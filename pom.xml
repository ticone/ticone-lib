<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>dk.sdu.imada</groupId>
	<artifactId>ticone-lib</artifactId>
	<version>${ticone.version}</version>

	<properties>
		<!-- for compatibility with surefire, don't use 5.3.x -->
		<junit.jupiter.version>5.2.0</junit.jupiter.version>
		<junit.vintage.version>4.12.3</junit.vintage.version>
		<junit.platform.version>1.2.0</junit.platform.version>
		<ticone.version>2.0.0-RC4-SNAPSHOT</ticone.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.2.3</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-runner</artifactId>
			<version>${junit.platform.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-api</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-params</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>dk.sdu.imada</groupId>
			<artifactId>TransClust</artifactId>
			<version>1.0</version>
		</dependency>

		<dependency>
			<groupId>dk.sdu.imada</groupId>
			<artifactId>ticone-kpm</artifactId>
			<version>4.0</version>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.8</version>
		</dependency>

		<!-- KPM Dependencies -->
		<dependency>
			<groupId>org.mvel</groupId>
			<artifactId>mvel2</artifactId>
			<version>2.4.2.Final</version>
		</dependency>

		<dependency>
			<groupId>net.sf.jung</groupId>
			<artifactId>jung-graph-impl</artifactId>
			<version>2.1.1</version>
		</dependency>

		<dependency>
			<groupId>net.sf.jung</groupId>
			<artifactId>jung-algorithms</artifactId>
			<version>2.1.1</version>
		</dependency>

		<dependency>
			<groupId>de.wiwie</groupId>
			<artifactId>Wiutils</artifactId>
			<version>1.4-SNAPSHOT</version>
		</dependency>

		<!-- textplots -->
		<dependency>
			<groupId>com.github.dvdmllr</groupId>
			<artifactId>textplots</artifactId>
			<version>b6dd78a001048633c51dbc7b75611665aed944b5</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>dk.sdu.imada</groupId>
			<artifactId>ticone-api</artifactId>
			<version>${ticone.version}</version>
		</dependency>

		<!-- BitMatrix -->
		<dependency>
			<groupId>colt</groupId>
			<artifactId>colt</artifactId>
			<version>1.2.0</version>
		</dependency>

		<dependency>
			<groupId>it.unimi.dsi</groupId>
			<artifactId>fastutil</artifactId>
			<version>8.2.2</version>
			<scope>test</scope>
		</dependency>


	</dependencies>

	<build>
		<sourceDirectory>src/main/java</sourceDirectory>
		<testSourceDirectory>src/test/java</testSourceDirectory>
		<plugins>
			<!-- ensure up-to-date version of maven -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
				<version>3.0.0-M2</version>
				<executions>
					<execution>
						<id>enforce-maven</id>
						<goals>
							<goal>enforce</goal>
						</goals>
						<configuration>
							<rules>
								<requireMavenVersion>
									<version>3.0.5</version>
								</requireMavenVersion>
							</rules>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<!-- On each build print a report with outdated dependencies and plugins 
				we are using -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>versions-maven-plugin</artifactId>
				<version>2.7</version>
				<executions>
					<execution>
						<id>check-for-updates</id>
						<phase>compile</phase>
						<goals>
							<goal>display-dependency-updates</goal>
							<goal>display-plugin-updates</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.0</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<!-- include the maven artifact version into the manifest file -->
			<!-- see http://stackoverflow.com/questions/2712970/get-maven-artifact-version-at-runtime -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<manifest>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
							<addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
						</manifest>
					</archive>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>3.0.0-M3</version>
				<dependencies>
					<dependency>
						<groupId>org.junit.platform</groupId>
						<artifactId>junit-platform-surefire-provider</artifactId>
						<version>${junit.platform.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<groups>!long-running</groups>
					<useSystemClassLoader>false</useSystemClassLoader>
				</configuration>
			</plugin>
		</plugins>

		<extensions>
			<!-- <extension> <groupId>org.apache.maven.wagon</groupId> <artifactId>wagon-ssh</artifactId> 
				<version>2.4</version> </extension> -->
			<extension>
				<groupId>org.apache.maven.wagon</groupId>
				<artifactId>wagon-ssh-external</artifactId>
				<version>1.0-beta-6</version>
			</extension>
		</extensions>
	</build>

	<repositories>
		<repository>
			<id>compbio internal</id>
			<releases>
			</releases>
			<name>Internal Release Repository</name>
			<url>http://maven.compbio.sdu.dk/repository/internal/</url>
		</repository>
		<repository>
			<id>compbio_maven</id>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
			</releases>
			<name>CompBio Maven Repository</name>
			<url>http://maven.compbio.sdu.dk/repository/snapshots/</url>
		</repository>
		<repository>
			<id>central_maven</id>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
			</releases>
			<name>Central Maven</name>
			<url>http://central.maven.org/</url>
		</repository>
		<repository>
			<id>jitpack.io</id>
			<url>https://jitpack.io</url>
		</repository>
	</repositories>

	<distributionManagement>
		<repository>
			<id>compbio_maven</id>
			<name>Internal Release Repository</name>
			<url>https://maven.compbio.sdu.dk/repository/internal/</url>
		</repository>
		<snapshotRepository>
			<uniqueVersion>true</uniqueVersion>
			<id>archiva.snapshots</id>
			<name>Snapshots</name>
			<url>https://maven.compbio.sdu.dk/repository/snapshots/</url>
			<layout>default</layout>
		</snapshotRepository>

	</distributionManagement>


</project>
