package dk.sdu.imada.ticone;

import java.io.File;

/**
 * Created by christian on 9/23/15.
 */
public class Constants {

	public static final String file_sep = File.separator;
	public static final String path_to_testfiles = file_sep + "test_files" + file_sep;
}
