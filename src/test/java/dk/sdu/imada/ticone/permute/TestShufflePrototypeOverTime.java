/**
 * 
 */
package dk.sdu.imada.ticone.permute;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ShuffleTimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public class TestShufflePrototypeOverTime {

	@Test
	public void test() throws Exception {
		ShuffleTimeSeries s = new ShuffleTimeSeries();

		double[] pt = new double[] { 1, 2, 3, 4, 5 };

		TimeSeries timeSeries = new TimeSeries(pt);
		double[] shuffled = ((ITimeSeries) s.shuffle(timeSeries, 42).getShuffled()).asArray();

		assertArrayEquals(pt, timeSeries.asArray(), 0.0);

		Arrays.sort(pt);
		Arrays.sort(shuffled);
		assertArrayEquals(pt, shuffled, 0.0);

	}

	@Test
	public void test2() throws Exception {
		ShuffleTimeSeries s = new ShuffleTimeSeries();

		double[] pt = new double[] { 1, 1, 1, 1, 1 };

		TimeSeries timeSeries = new TimeSeries(pt);
		double[] shuffled = ((ITimeSeries) s.shuffle(timeSeries, 42).getShuffled()).asArray();

		assertArrayEquals(pt, timeSeries.asArray(), 0.0);

		Arrays.sort(pt);
		Arrays.sort(shuffled);
		assertArrayEquals(pt, shuffled, 0.0);
	}

	@Test
	public void test3() throws Exception {
		ShuffleTimeSeries s = new ShuffleTimeSeries();

		double[] pt = new double[] {};

		TimeSeries timeSeries = new TimeSeries(pt);
		double[] shuffled = ((ITimeSeries) s.shuffle(timeSeries, 42).getShuffled()).asArray();

		assertArrayEquals(pt, timeSeries.asArray(), 0.0);

		Arrays.sort(pt);
		Arrays.sort(shuffled);
		assertArrayEquals(pt, shuffled, 0.0);
	}

	@Test
	public void testShufflingNotTheSameArray() throws Exception {
		ShuffleTimeSeries s = new ShuffleTimeSeries();

		double[] pt = new double[] { 1, 2, 3, 4, 5 };
		Arrays.sort(pt);

		TimeSeries timeSeries = new TimeSeries(pt);

		boolean anyNotEqual = false;

		for (int i = 0; i < 100; i++) {
			if (anyNotEqual)
				break;

			double[] shuffled = ((ITimeSeries) s.shuffle(timeSeries, 42).getShuffled()).asArray();

			anyNotEqual |= !Arrays.equals(pt, shuffled);

			assertArrayEquals(pt, timeSeries.asArray(), 0.0);
			Arrays.sort(shuffled);
			assertArrayEquals(pt, shuffled, 0.0);
		}
		assertTrue("Shuffled array shouldn't be always the same as the original array", anyNotEqual);
	}

}
