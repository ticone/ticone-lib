package dk.sdu.imada.ticone.discretize;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePatternFromZeroToMinMax;

public class TestDiscretizePatternFromZeroToMinMax {

	private double[] expectedDiscretizedValues01 = new double[] { -3, -1.5, 0, 1.75, 3.5, 5.25, 7 };
	private double[] expectedDiscretizedValues02 = new double[] { -3, -1.5, 0, 1.5, 3 };
	private double[] expectedDiscretizedValues03 = new double[] { 0, 2, 4, 6, 8 };
	private double[] expectedDiscretizedValues04 = new double[] { -8, -6, -4, -2, 0 };

	@Test
	public void testInitializeValues() {
		IDiscretizeTimeSeries discretizeFunction = new DiscretizePatternFromZeroToMinMax(-3, 7, 5);

		// Does not contain 0, and uneven above and below, and negative min, and
		// positive max
		double[] discretizedValues01 = discretizeFunction.getDiscretizationValues();
		for (int i = 0; i < discretizedValues01.length; i++) {
			assertEquals(expectedDiscretizedValues01[i], discretizedValues01[i], 0);
		}

		// Contains 0
		discretizeFunction = new DiscretizePatternFromZeroToMinMax(-3, 3, 4);
		double[] discretizedValues02 = discretizeFunction.getDiscretizationValues();
		for (int i = 0; i < discretizedValues02.length; i++) {
			assertEquals(expectedDiscretizedValues02[i], discretizedValues02[i], 0);
		}

		// Positive min
		discretizeFunction = new DiscretizePatternFromZeroToMinMax(2, 8, 4);
		double[] discretizedValues03 = discretizeFunction.getDiscretizationValues();
		for (int i = 0; i < discretizedValues03.length; i++) {
			assertEquals(expectedDiscretizedValues03[i], discretizedValues03[i], 0);
		}

		// Negative max
		discretizeFunction = new DiscretizePatternFromZeroToMinMax(-8, -2, 4);
		double[] discretizedValues04 = discretizeFunction.getDiscretizationValues();
		for (int i = 0; i < discretizedValues04.length; i++) {
			assertEquals(expectedDiscretizedValues04[i], discretizedValues04[i], 0);
		}
	}

	private ITimeSeries expectedDiscretizedPattern = new TimeSeries(new double[] { -3, -3, 0, 0, 1.5, 3 });

	@Test
	public void testDiscretizePattern() {
		DiscretizePatternFromZeroToMinMax discretizeFunction = new DiscretizePatternFromZeroToMinMax(-3, 3, 4);

		ITimeSeries pattern = new TimeSeries(new double[] { -3, -2.25, -0.2, 0.75, 2.25, 2.3 });

		TimeSeries discretizedPattern = discretizeFunction.discretizeObjectTimeSeries(pattern);

		for (int i = 0; i < discretizedPattern.getNumberTimePoints(); i++) {
			assertEquals(expectedDiscretizedPattern.get(i), discretizedPattern.get(i), 0);
		}
	}

}
