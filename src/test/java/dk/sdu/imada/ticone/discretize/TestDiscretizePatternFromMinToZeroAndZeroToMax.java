package dk.sdu.imada.ticone.discretize;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;

/**
 * Created by christian on 9/22/15.
 */
public class TestDiscretizePatternFromMinToZeroAndZeroToMax {

	private double[] expectedValues = new double[] { -3, -2, -1, 0, 1, 2, 3 };

	@Test
	public void testInitializeValues() {
		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(-3, 3, 3,
				3);
		double[] values = discretizePattern.getDiscretizationValues();
		for (int i = 0; i < values.length; i++) {
			assertEquals(values[i], expectedValues[i], 0);
		}
	}

	private ITimeSeries inputPattern = new TimeSeries(new double[] { -3, -2.5, -0.5, 0, 0.49, 2.51 });
	private ITimeSeries expectedDiscretizedPattern = new TimeSeries(new double[] { -3, -3, -1, 0, 0, 3 });

	@Test
	public void testDiscretizePattern() {
		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(-3, 3, 3,
				3);
		ITimeSeries discretizedPattern = discretizePattern.discretizeObjectTimeSeries(inputPattern);
		for (int i = 0; i < discretizedPattern.getNumberTimePoints(); i++) {
			assertEquals(discretizedPattern.get(i), expectedDiscretizedPattern.get(i), 0);
		}
	}
}
