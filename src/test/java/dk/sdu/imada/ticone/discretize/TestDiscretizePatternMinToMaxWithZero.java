package dk.sdu.imada.ticone.discretize;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePatternMinToMaxWithZero;

public class TestDiscretizePatternMinToMaxWithZero {

	private double[] expectedDiscretizedValues01 = new double[] { -3, -0.5, 0, 2, 4.5 };
	private double[] expectedDiscretizedValues02 = new double[] { -3, -1.5, 0, 1.5, 3 };

	@Test
	public void testInitializeValues() {

		IDiscretizeTimeSeries discretizeFunction = new DiscretizePatternMinToMaxWithZero(-3, 4.5, 3);
		double[] discretizedValues01 = discretizeFunction.getDiscretizationValues();
		for (int i = 0; i < discretizedValues01.length; i++) {
			assertEquals(expectedDiscretizedValues01[i], discretizedValues01[i], 0);
		}

		discretizeFunction = new DiscretizePatternMinToMaxWithZero(-3, 3, 4);
		double[] discretizedValues02 = discretizeFunction.getDiscretizationValues();
		for (int i = 0; i < discretizedValues02.length; i++) {
			assertEquals(expectedDiscretizedValues02[i], discretizedValues02[i], 0);
		}
	}

	private ITimeSeries expectedDiscretizedPattern = new TimeSeries(new double[] { -3, -3, 0, 0, 1.5, 3 });

	@Test
	public void testDiscretizePattern() {
		IDiscretizeTimeSeries discretizeFunction = new DiscretizePatternMinToMaxWithZero(-3, 3, 4);
		ITimeSeries pattern = new TimeSeries(new double[] { -3, -2.25, -0.2, 0.75, 2.25, 2.3 });

		ITimeSeries discretizedPattern = discretizeFunction.discretizeObjectTimeSeries(pattern);

		for (int i = 0; i < discretizedPattern.getNumberTimePoints(); i++) {
			assertEquals(expectedDiscretizedPattern.get(i), discretizedPattern.get(i), 0);
		}
	}

}
