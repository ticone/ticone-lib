package dk.sdu.imada.ticone.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.Constants;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.preprocessing.calculation.FoldChangeCalculator;

/**
 * Created by christian on 6/12/15.
 */
public class TestPatternCalculationFoldChange {

	final String file_sep = Constants.file_sep;
	final String path_to_testfiles = Constants.path_to_testfiles;

	double[][] absoluteValues = { { 0.0, 1.0, 1.0, -2.0 },
			{ 0.0, 0.5849625007211562, 2.15200309344505, -1.7369655941662063 },
			{ 0.0, 2.3219280948873626, -4.321928094887363, -1.0 } };

	@Test
	public void testCalculations() throws Exception {
		FoldChangeCalculator patternCalculationFoldChange = new FoldChangeCalculator();
		File timeSeriesDataFile = new File(
				getClass().getResource(path_to_testfiles + "calculation" + file_sep + "timeseries.test").getFile());
		try {
			Scanner scan = new Scanner(timeSeriesDataFile);
			ITimeSeriesObjectList timeSeriesDatas = Parser.parseObjects(scan);
			scan.close();
			patternCalculationFoldChange.initializeObjects(timeSeriesDatas);
		} catch (FileNotFoundException fnfe) {
			fail("File was not found");
		}
		patternCalculationFoldChange.process();
		ITimeSeriesObjectList timeSeriesDataArrayList = patternCalculationFoldChange.getObjects();
		for (int i = 0; i < timeSeriesDataArrayList.size(); i++) {
			ITimeSeries pattern = timeSeriesDataArrayList.get(i).getPreprocessedTimeSeriesList()[0];
			for (int j = 0; j < pattern.getNumberTimePoints(); j++) {
				assertEquals(absoluteValues[i][j], pattern.get(j), 0);
			}
		}
	}

	@Test
	public void testFileNotFound() throws Exception {
		FoldChangeCalculator patternCalculationFoldChange = new FoldChangeCalculator();
		File timeSeriesDataFile = new File("not a file");
		try {
			Scanner scan = new Scanner(timeSeriesDataFile);
			ITimeSeriesObjectList timeSeriesDatas = Parser.parseObjects(scan);
			scan.close();
			patternCalculationFoldChange.initializeObjects(timeSeriesDatas);
		} catch (FileNotFoundException fnfe) {
			return;
		}
		fail("FileNotFoundException was not thrown.");
	}

	//
	// @Test
	// public void testFoldChangeCalculations() {
	// FoldChangeCalculator patternCalculationFoldChange = new
	// FoldChangeCalculator();
	// File timeSeriesDataFile = new File(path_to_testfiles + "calculation" +
	// file_sep + "VSD_top1500_avgRepl_woHeader.txt");
	// try {
	// List<TimeSeriesData> timeSeriesDatas =
	// Parser.parseExpressions(timeSeriesDataFile);
	// patternCalculationFoldChange.initializeTimeSeriesData(timeSeriesDatas);
	//
	// for (TimeSeriesData tsData : timeSeriesDatas)
	// if (tsData.getName().equals("NM_001243780")) {
	// System.out.println(Arrays.toString(tsData.getSignal()));
	// System.out.println(Arrays.toString(tsData.getPattern()));
	// }
	// } catch (FileNotFoundException fnfe) {
	// fail("File was not found");
	// }
	// patternCalculationFoldChange.calculatePatterns();
	//
	// for (TimeSeriesData tsData :
	// patternCalculationFoldChange.getTimeSeriesDatas())
	// if (tsData.getName().equals("NM_001243780")) {
	// System.out.println(Arrays.toString(tsData.getSignal()));
	// System.out.println(Arrays.toString(tsData.getExactPattern()));
	// }
	//
	// List<TimeSeriesData> timeSeriesDataArrayList =
	// patternCalculationFoldChange.getTimeSeriesDatas();
	// for (int i = 0; i < timeSeriesDataArrayList.size(); i++) {
	// double[] pattern = timeSeriesDataArrayList.get(i).getExactPattern();
	// for (int j = 0; j < pattern.length; j++) {
	// assertEquals(absoluteValues[i][j], pattern[j], 0);
	// }
	// }
	// }

}
