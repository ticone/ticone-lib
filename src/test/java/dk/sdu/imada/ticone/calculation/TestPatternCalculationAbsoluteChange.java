package dk.sdu.imada.ticone.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.Constants;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.preprocessing.calculation.TimeSeriesMinMaxNormalizer;

/**
 *
 * Created by christian on 6/12/15.
 */
public class TestPatternCalculationAbsoluteChange {

	final String file_sep = Constants.file_sep;
	final String path_to_testfiles = Constants.path_to_testfiles;

	double[][] absoluteValues = { { 0.0005, 0.0015, 0.0035, 0.0005 }, { 0.0010, 0.00175, 0.0095, 0.0025 },
			{ 0.199599, 1.0, 0.04952, 0.02451 } };

	@Test
	public void testCalculations() throws Exception {
		TimeSeriesMinMaxNormalizer patternCalculationAbsoluteChange = new TimeSeriesMinMaxNormalizer();
		File timeSeriesDataFile = new File(
				getClass().getResource(path_to_testfiles + "calculation" + file_sep + "timeseries.test").getFile());
		try {
			Scanner scan = new Scanner(timeSeriesDataFile);
			ITimeSeriesObjectList timeSeriesDatas = Parser.parseObjects(scan);
			scan.close();
			patternCalculationAbsoluteChange.initializeObjects(timeSeriesDatas);
		} catch (FileNotFoundException fnfe) {
			fail("File was not found");
		}
		patternCalculationAbsoluteChange.process();
		ITimeSeriesObjectList timeSeriesDataArrayList = patternCalculationAbsoluteChange.getObjects();
		for (int i = 0; i < timeSeriesDataArrayList.size(); i++) {
			ITimeSeries pattern = timeSeriesDataArrayList.get(i).getPreprocessedTimeSeriesList()[0];
			for (int j = 0; j < pattern.getNumberTimePoints(); j++) {
				assertEquals(absoluteValues[i][j], pattern.get(j), 0.0001);
			}
		}
	}

	@Test
	public void testFileNotFound() throws Exception {
		TimeSeriesMinMaxNormalizer patternCalculationAbsoluteChange = new TimeSeriesMinMaxNormalizer();
		File timeSeriesDataFile = new File("not a file");
		try {
			Scanner scan = new Scanner(timeSeriesDataFile);
			ITimeSeriesObjectList timeSeriesDatas = Parser.parseObjects(scan);
			scan.close();
			patternCalculationAbsoluteChange.initializeObjects(timeSeriesDatas);
		} catch (FileNotFoundException fnfe) {
			return;
		}
		fail("FileNotFoundException was not thrown.");
	}

}
