package dk.sdu.imada.ticone.comparison;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.BasicClusteringProcessBuilder;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethod;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.IShuffleClustering;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.PrespecifiedInitialClustering;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingDataset;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringWithRandomPrototypeTimeSeries;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResultFactory;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureCommonObjectsOverPrototypeSimilarity;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.statistics.IPvalue;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

public class TestTiconeComparisonTask {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries());
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testNotSignificant() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();

		ClusterObjectMapping pom1 = demoData(true, new Random(41)), pom2 = demoData(true, new Random(43));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(pom1.getAssignedObjects());
		av.process();

		av = new AbsoluteValues();
		av.initializeObjects(pom2.getAssignedObjects());
		av.process();

		final BasicClusteringProcessBuilder<TiconeClusteringResult> processBuilder = new BasicClusteringProcessBuilder<>();

		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process1 = processBuilder
				.build(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
						new CLARAClusteringMethodBuilder().setSimilarityFunction(similarityFunction)
								.setPrototypeBuilder(f),
						new PrespecifiedInitialClustering(pom1), new LoadDataFromCollection(pom1.getAssignedObjects()),
						new PreprocessingSummary(), f, similarityFunction, TimePointWeighting.NONE,
						new AbsoluteValues(), 41, new ArrayList<>());
		process1.start();
		process1.doIteration();
		pom1 = process1.getLatestClustering();
		TiconeClusteringResult clusteringResult1 = process1.getClusteringResult();

		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process2 = processBuilder
				.build(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
						new CLARAClusteringMethodBuilder().setSimilarityFunction(similarityFunction)
								.setPrototypeBuilder(f),
						new PrespecifiedInitialClustering(pom2), new LoadDataFromCollection(pom2.getAssignedObjects()),
						new PreprocessingSummary(), f, similarityFunction, TimePointWeighting.NONE,
						new AbsoluteValues(), 42, new ArrayList<>());
		process2.start();
		process2.doIteration();
		pom2 = process2.getLatestClustering();
		TiconeClusteringResult clusteringResult2 = process2.getClusteringResult();

		IShuffleDataset s1 = new ShuffleDatasetRowwise();
		IShuffleDataset s2 = new ShuffleDatasetRowwise();

		IShuffleClustering shuffleClustering1 = new ShuffleClusteringByShufflingDataset(
				clusteringResult1.getClusteringMethodBuilder(),
				clusteringResult1.getClusterHistory().getClusterObjectMapping().getClusters().size(), s1);

		IShuffleClustering shuffleClustering2 = new ShuffleClusteringByShufflingDataset(
				clusteringResult2.getClusteringMethodBuilder(),
				clusteringResult2.getClusterHistory().getClusterObjectMapping().getClusters().size(), s2);

		int numberPermutations = 100;
		TiconeComparisonTask ticoneComparisonTask = new TiconeComparisonTask(clusteringResult1, clusteringResult2,
				new ClusteringComparisonShuffleClusteringPair(shuffleClustering1, shuffleClustering2), 1, 1,
				pom1.getClusters(), pom2.getClusters(), similarityFunction, numberPermutations);

		ClusteringComparisonResult calculateClusterComparisonMatrix = ticoneComparisonTask
				.calculateClusterComparisonMatrix();
		System.out.println(ticoneComparisonTask.clusterPairFeatureStore
				.keySet(new ClusterPairFeatureCommonObjectsOverPrototypeSimilarity()));
		System.out.println(ticoneComparisonTask.clusterPairFeatureStore
				.valuesRaw(new ClusterPairFeatureCommonObjectsOverPrototypeSimilarity()));
		PValueCalculationResult pvalues = calculateClusterComparisonMatrix.getPvalues();

		System.out.println(pvalues.getPValues(ObjectType.CLUSTER_PAIR));

		for (IClusterPair pair : pvalues.getObjects(ObjectType.CLUSTER_PAIR)) {
			IPvalue p = pvalues.getPValue(pair);
			System.out.println(String.format("%s\t%s", pair, p));
			assertTrue(p.getDouble() >= 0.1);
		}
	}

	@Test
	public void testComparison2() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();

		// 1st data set
		Random random42 = new Random(42);
		ClusterObjectMapping pom1 = demoData(true, random42);
		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(pom1.getAssignedObjects());
		av.process();

		// 2nd data set
		Random random43 = new Random(43);
		ClusterObjectMapping pom2 = demoData(true, random43);
		av = new AbsoluteValues();
		av.initializeObjects(pom2.getAssignedObjects());
		av.process();

		final BasicClusteringProcessBuilder<TiconeClusteringResult> processBuilder = new BasicClusteringProcessBuilder<>();
		// 1st clustering
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = processBuilder
				.build(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
						new CLARAClusteringMethodBuilder().setSimilarityFunction(similarityFunction)
								.setPrototypeBuilder(f),
						new PrespecifiedInitialClustering(pom1), new LoadDataFromCollection(pom1.getAssignedObjects()),
						new PreprocessingSummary(), f, similarityFunction, TimePointWeighting.NONE,
						new AbsoluteValues(), 42, new ArrayList<>());
		bla.start();
		bla.doIteration();
		pom1 = bla.getLatestClustering();
		TiconeClusteringResult clusteringResult1 = bla.getClusteringResult();

		// 2nd clustering
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla2 = processBuilder
				.build(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
						new CLARAClusteringMethodBuilder().setSimilarityFunction(similarityFunction)
								.setPrototypeBuilder(f),
						new PrespecifiedInitialClustering(pom2), new LoadDataFromCollection(pom2.getAssignedObjects()),
						new PreprocessingSummary(), f, similarityFunction, TimePointWeighting.NONE,
						new AbsoluteValues(), 43, new ArrayList<>());
		bla2.start();
		bla2.doIteration();
		pom2 = bla2.getLatestClustering();
		TiconeClusteringResult clusteringResult2 = bla2.getClusteringResult();

		IShuffleDataset s1 = new ShuffleDatasetRowwise();
		IShuffleDataset s2 = new ShuffleDatasetRowwise();

		IShuffleClustering shuffleClustering1 = new ShuffleClusteringByShufflingDataset(
				clusteringResult1.getClusteringMethodBuilder(), pom1.getClusters().size(), s1);

		IShuffleClustering shuffleClustering2 = new ShuffleClusteringByShufflingDataset(
				clusteringResult2.getClusteringMethodBuilder(), pom2.getClusters().size(), s2);

		int numberPermutations = 100;
		TiconeComparisonTask ticoneComparisonTask = new TiconeComparisonTask(clusteringResult1, clusteringResult2,
				new ClusteringComparisonShuffleClusteringPair(shuffleClustering1, shuffleClustering2), 1, 1,
				pom1.getClusters(), pom2.getClusters(), similarityFunction, numberPermutations);

		ClusteringComparisonResult calculateClusterComparisonMatrix = ticoneComparisonTask
				.calculateClusterComparisonMatrix();
		PValueCalculationResult pvalues = calculateClusterComparisonMatrix.getPvalues();

		boolean anyLargerZero = false;
		boolean anySmallerOne = false;

		for (IClusterPair c : pvalues.getObjects(ObjectType.CLUSTER_PAIR)) {
			double p = pvalues.getPValue(c).getDouble();
			anyLargerZero |= p > 0;
			anySmallerOne |= p < 1;
		}

		assertTrue(anyLargerZero);
		assertTrue(anySmallerOne);
	}

	@Test
	@Tag("long-running")
	public void testComparisonInfluenza() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();

		ITimeSeriesObjectList objs = TestDataUtility.parseInfluenzaExpressionData();
		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		IInitialClusteringProvider<ClusterObjectMapping> initialClustering = new InitialClusteringMethod(
				initialClusteringInterface, 500, 42);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		ClusterObjectMapping pom = bla.doIteration();
		TiconeClusteringResult clusteringResult1 = bla.getClusteringResult();

		IShuffleClustering shuffleClustering1 = new ShuffleClusteringWithRandomPrototypeTimeSeries(similarityFunction,
				f, new CreateRandomTimeSeries(pom.getAssignedObjects()));
		IShuffleClustering shuffleClustering2 = new ShuffleClusteringWithRandomPrototypeTimeSeries(similarityFunction,
				f, new CreateRandomTimeSeries(pom.getAssignedObjects()));
		IShuffle shuffleClusteringPair = new ClusteringComparisonShuffleClusteringPair(shuffleClustering1,
				shuffleClustering2);

		int numberPermutations = 100;
		TiconeComparisonTask ticoneComparisonTask = new TiconeComparisonTask(clusteringResult1, clusteringResult1,
				new ClusteringComparisonShuffleClusteringPair(shuffleClustering1, shuffleClustering2), 1, 1,
				pom.getClusters(), pom.getClusters(), similarityFunction, numberPermutations);

		ClusteringComparisonResult compResult = ticoneComparisonTask.calculateClusterComparisonMatrix();
		System.out.println(compResult);
	}

	/**
	 * Generates 3 clusters with random prototype time series, containing 3 random
	 * objects each.
	 * 
	 * @param isDirected
	 * @param r
	 * @return
	 * @throws Exception
	 */
	protected ClusterObjectMapping demoData(boolean isDirected, Random r) throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);

		final IntList availableObjectIds = new IntArrayList(IntStream.range(0, 9).iterator());
		Collections.shuffle(availableObjectIds, r);

		// generate 3 clusters with 3 objects each;
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + availableObjectIds.removeInt(0);
				double[] ts = new double[5];
				for (int i = 0; i < ts.length; i++) {
					ts[i] = r.nextDouble();
				}
				ITimeSeriesObject data = new TimeSeriesObject(objId, 1, TimeSeries.of(ts));
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
		}
		return demoPom;
	}
}
