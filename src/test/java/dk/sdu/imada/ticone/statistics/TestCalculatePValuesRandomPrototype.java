/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.BasicClusteringProcessBuilder;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethod;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.IShuffleClustering;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.LocalSearchClustering;
import dk.sdu.imada.ticone.clustering.PAMKClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingPrototypeTimeSeries;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringWithRandomPrototypeTimeSeries;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResultFactory;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ShuffleDatasetGlobally;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.data.ShuffleTimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.SimilarityFeatureValue;
import dk.sdu.imada.ticone.feature.scale.IScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.TanhNormalizerBuilder;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.generate.TimeSeriesTestClass;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.io.LoadDataFromFile;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PValues;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TimePointWeighting;
import it.unimi.dsi.fastutil.objects.ObjectList;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 10, 2017
 *
 */
public class TestCalculatePValuesRandomPrototype {

	private IPrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries());
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testZeroWeights() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));

		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 0));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 0.0, 0.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffleClustering shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(sim, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));
		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		for (IPvalue d : calculatePValues.values(ObjectType.CLUSTER))

			assertEquals(1.0, d.getDouble(), 0.0001);
	}

	@Test
	public void testOnlyAvgSimilarity() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 20));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 0.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffleClustering shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(sim, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));
		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		assertTrue(calculatePValues.get(c1).getDouble() < calculatePValues.get(c2).getDouble());
	}

	@Test
	public void testClustersEquallySimilarDifferentSizes() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 1));
		ITimeSeriesObject o3 = new TimeSeriesObject("obj3", "s1", TimeSeries.of(1, 2, 1));
		ITimeSeriesObject o4 = new TimeSeriesObject("obj4", "s1", TimeSeries.of(1, 2, 1));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));

		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o3, c2, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o4, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffleClustering shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(sim, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));
		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		System.out.println(calculatePValues.get(c1));
		System.out.println(calculatePValues.get(c2));

		assertTrue(calculatePValues.get(c1).getDouble() > calculatePValues.get(c2).getDouble());
	}

	@Test
	public void testClustersEquallyLargeAndSimilar2() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 4));

		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 0));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffleClustering shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(sim, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));
		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		assertEquals(calculatePValues.get(c1).getDouble(), calculatePValues.get(c2).getDouble(), 0.0001);
	}

	@Test
	public void testClustersEquallyLargeAndSimilar3() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o3 = new TimeSeriesObject("obj3", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 1));
		ITimeSeriesObject o4 = new TimeSeriesObject("obj4", "s1", TimeSeries.of(1, 2, 1));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o3, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o4, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffleClustering shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(sim, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));
		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		assertEquals(calculatePValues.get(c1).getDouble(), calculatePValues.get(c2).getDouble(), 0.0001);
	}

	@Test
	public void testClustersBalancedSizeVsSimilarity() throws Exception {
		// size of cluster 1 is half is large
		// avg sim of cluster 1 is twice is large
		// => it should be balanced out with equal feature weights
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		tsf.setTimeSeries(new double[] { 1, 2, 8 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 4));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 0));
		ITimeSeriesObject o3 = new TimeSeriesObject("obj3", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o4 = new TimeSeriesObject("obj4", "s1", TimeSeries.of(1, 2, 2));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		com.addMapping(o3, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o4, c3, TestSimpleSimilarityValue.of(1.0));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fno, fas };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffleClustering shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(sim, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));
		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
//		ClusterObjectMapping.resetClusterCount();
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		assertEquals(calculatePValues.get(c1).getDouble(), calculatePValues.get(c2).getDouble(), 0.0001);
	}

	@Test
	public void testSeed42() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();

		new ShuffleDatasetGlobally();
		int numberOfInitialClusters = 20;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/easyDataset-hardcodedPatterns-objects40-seed1342-bg240-samples2-identicaltrue.txt.2.norm")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		IInitialClusteringProvider<ClusterObjectMapping> initialClustering = new InitialClusteringMethod(
				initialClusteringInterface, numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		ClusterObjectMapping com = initialClustering.getInitialClustering(objs);

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffleClustering shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(similarityFunction, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));
		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
				similarityFunction).calculate(com)).getValue();

	}

	@Test
	public void testGeneratedCluster() throws Exception {
		List<ICluster> cluster = new ArrayList<>();
		ClusterObjectMapping com = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 0, 0, 0, 0, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 1, 0, 0, 0, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 1, 0, 0, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 1, 0, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 0, 1, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 0, 0, 1 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 3, 0, 5, 1, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());

		TimeSeriesTestClass.generateObjectsForClusterPrototypes(com, TestSimpleSimilarityValue.of(-1), 0, 10);

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5, 1, 1 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("x5", "s1", TimeSeries.of(6, 2, 0, 1, 1));
		ITimeSeriesObject o6 = new TimeSeriesObject("x6", "s1", TimeSeries.of(1, 7, 0, 1, 1));
		ITimeSeriesObject o7 = new TimeSeriesObject("x7", "s1", TimeSeries.of(1, 7, 13, 1, 1));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		ITimeSeriesObjectList objs = com.getAssignedObjects();

		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffle shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(similarityFunction, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));

		BasicCalculatePValues calcPvals = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvals.setFeatureStore(featureStore);

		PValueCalculationResult calculatePValues = calcPvals.calculatePValues(com, 42);
		PValues pvalues = calculatePValues.pValues;

		ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
				similarityFunction).calculate(com)).getValue();

	}

	@Tag("long-running")
	@Test
	public void testNoExceptionOnRandom2500_1() throws Exception {
		final ITimeSeriesObjectList objs = Parser.parseObjects(new Scanner(
				new File(getClass().getResource("/test_files/statistics/random_n2500_t11_1.tsv").getFile())));

		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		ShuffleClusteringWithRandomPrototypeTimeSeries shuffle = new ShuffleClusteringWithRandomPrototypeTimeSeries(
				similarityFunction, f, new CreateRandomTimeSeries(objs));
		int numberOfInitialClusters = 10;

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 10, 10);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 1, Collections.emptySet());
		bla.start();

		ClusterObjectMapping pom = bla.doIteration();

		ClusterFeatureNumberObjects cfNo = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity cfAs = new ClusterFeatureAverageSimilarity(similarityFunction);

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { cfAs };

		ICalculatePValues calculatePValues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER), shuffle,
				Arrays.asList(new BasicFitnessScore(ObjectType.CLUSTER, features)), new List[] { Arrays.asList(cfNo) },
				Arrays.asList(false), new MultiplyPValues(), 1000);

		bla.calculatePValues(calculatePValues, 1);

		IPValueCalculationResult clusterPValues = bla.getPValues();
//
//		ICluster constantCluster = null;
//		for (ICluster c : bla.getLatestClustering().getClusters()) {
//			// the cluster with constant prototype
//			if (c.getClusterNumber() == 10) {
//				constantCluster = c;
//			} else if (c.getClusterNumber() == 6) {
//			}
//
//			// double l = prior.calculate(pom, c).asDouble();
//			// System.out.println(c + "\t" + l);
//		}
//		assertArrayEquals(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, PrototypeComponentType.TIME_SERIES
//				.getComponent(constantCluster.getPrototype()).getTimeSeries().asArray(), 0.0);
//		// the cluster with constant signal should not be significant
//		for (ICluster c : bla.getLatestClustering().getClusters())
//			System.out.println(c + ": " + c.getObjects().size() + "\t" + c.getAvgSimilarity() + "\t"
//					+ clusterPValues.getPValue(c));
//		assertTrue(clusterPValues.getPValue(constantCluster).getDouble() > 0.05);
//
//		NegativeEuclideanSimilarityFunction similarityFunction = new NegativeEuclideanSimilarityFunction();
//
//		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
//		preprocessor.initializeObjects(objs);
//		preprocessor.process();
//
//		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
//		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);
//
//		Random random = new Random(42);
//
//		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, random),
//				new TanhNormalizerBuilder<>(fas, com, 1000, random) };
//
//		double[] weights = new double[] { 1.0, 1.0 };
//
//		final IFeatureStore featureStore = new BasicFeatureStore();
//		for (ICluster c : com.getClusters()) {
//			featureStore.setFeatureValue(c, fno, fno.calculate(c));
//			featureStore.setFeatureValue(c, fas, fas.calculate(c));
//		}
//
//		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
//				new ClusterFeatureNumberObjects(),
//				new ClusterFeatureAverageSimilarity(new NegativeEuclideanSimilarityFunction()) };
//
//		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);
//
//		IShuffle shuffleClustering = new ShuffleClusteringWithRandomTimeSeries(similarityFunction, f,
//				new CreateRandomTimeSeries(com.getAllObjects()));
//
//		BasicCalculatePValues calcPvals = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
//				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
//				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
//		calcPvals.setFeatureStore(featureStore);
//
//		PValueCalculationResult calculatePValues = calcPvals.calculatePValues(com, 42);
//		PValues pvalues = calculatePValues.pValues;
//
//		ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureAverageObjectClusterSimilarity(
//				similarityFunction).calculate(com)).getValue();
//
//		for (ICluster c : pvalues.keySet(ObjectType.CLUSTER)) {
//			System.out.println(String.format("%s\t%d\t%.3f\t%.3f\t%.3f", c.getName(), c.getObjects().size(),
//					c.getAvgSimilarity().get(), calculatePValues.getOriginalFitness(fitness, c).getValue(),
//					pvalues.get(c).getDouble()));
//		}

	}

	@Tag("long-running")
	@Test
	public void testNoOOMOnRandom2500_1() throws Exception {
		final ITimeSeriesObjectList objs = Parser.parseObjects(new Scanner(
				new File(getClass().getResource("/test_files/statistics/random_n2500_t11_1.tsv").getFile())));

		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		ShuffleDatasetRowwise shuffle = new ShuffleDatasetRowwise();
		int numberOfInitialClusters = 10;

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 10, 10);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 1, Collections.emptySet());
		bla.start();

		ClusterObjectMapping pom = bla.doIteration();

		ClusterFeatureNumberObjects cfNo = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity cfAs = new ClusterFeatureAverageSimilarity(similarityFunction);

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { cfAs };

		ICalculatePValues calculatePValues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER), shuffle,
				Arrays.asList(new BasicFitnessScore(ObjectType.CLUSTER, features)), new List[] { Arrays.asList(cfNo) },
				Arrays.asList(false), new MultiplyPValues(), 1000);

		bla.calculatePValues(calculatePValues, 1);

		IPValueCalculationResult clusterPValues = bla.getPValues();
		final ObjectList<IPvalue> pValues = clusterPValues.getPValues(ObjectType.CLUSTER);
		System.out.println(pValues);
	}

	@Test
	public void testPValuesRandom250_2() throws Exception {
		final LoadDataFromFile loadData = new LoadDataFromFile();
		loadData.setColumnMapping(ImportColumnMapping.DEFAULT_FILE_COLUMNS_WO_REPLICATES);
		loadData.setImportFile(
				new File(getClass().getResource("/test_files/statistics/random_n250_t11.tsv").getFile()));

		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		ShuffleClusteringByShufflingPrototypeTimeSeries shuffle = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				similarityFunction, f, new ShuffleTimeSeries());
		int numberOfInitialClusters = 10;

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		loadData.loadData(preprocessor);

		preprocessor.process();

		// verify that the data is correctly parsed
		final ITimeSeriesObjectList allObjects = preprocessor.getObjects();
		assertEquals(250, allObjects.size());
		final ITimeSeriesObject object1 = allObjects.get("1");
		assertEquals(1, object1.getOriginalTimeSeriesList().length);
		assertEquals(new TimeSeries(0.24388799071312, 0.921154742594808, 0.724684497341514, 0.391379598760977,
				0.277459135046229, 0.897312479093671, 0.934674707939848, 0.623950281180441, 0.765127265360206,
				0.979493221268058, 0.570401303237304), object1.getOriginalTimeSeriesList()[0]);

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 10, 10);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> clusteringMethod = new CLARAClusteringMethodBuilder()
				.setSamples(5).setSampleSize(60)
				.setPamkBuilder(new PAMKClusteringMethodBuilder().setSwapIterations(50).setNstart(5))
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(clusteringMethod,
				numberOfInitialClusters, 1);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, clusteringMethod, initialClustering, loadData,
				new PreprocessingSummary(), f, similarityFunction, TimePointWeighting.NONE, preprocessor, 1,
				Collections.emptySet());
		bla.start();

		bla.doIteration();
		new LocalSearchClustering().computeIterationsUntilConvergence(bla);

		assertEquals(9, bla.getCurrentIteration());

		assertEquals(11, bla.getLatestClustering().getCluster(1).size());
		assertEquals(32, bla.getLatestClustering().getCluster(2).size());
		assertEquals(32, bla.getLatestClustering().getCluster(3).size());
		assertEquals(31, bla.getLatestClustering().getCluster(4).size());
		assertEquals(23, bla.getLatestClustering().getCluster(5).size());
		assertEquals(37, bla.getLatestClustering().getCluster(6).size());
		assertEquals(22, bla.getLatestClustering().getCluster(7).size());
		assertEquals(7, bla.getLatestClustering().getCluster(8).size());
		assertEquals(15, bla.getLatestClustering().getCluster(9).size());
		assertEquals(40, bla.getLatestClustering().getCluster(10).size());

		ClusterFeatureNumberObjects cfNo = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity cfAs = new ClusterFeatureAverageSimilarity(similarityFunction);

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { cfAs };

		ICalculatePValues calculatePValues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER), shuffle,
				Arrays.asList(new BasicFitnessScore(ObjectType.CLUSTER, features)), new List[] { Arrays.asList(cfNo) },
				Arrays.asList(false), new MultiplyPValues(), 1000);

		bla.calculatePValues(calculatePValues, 1);

		IPValueCalculationResult clusterPValues = bla.getPValues();

		final ClusterObjectMapping clustering = bla.getLatestClustering();

		assertEquals(0.0084, clusterPValues.getPValue(clustering.getCluster(1)).getDouble(), 0.001);
		assertEquals(1.0, clusterPValues.getPValue(clustering.getCluster(2)).getDouble(), 0.001);
		assertEquals(416, ((EmpiricalPvalue) clusterPValues.getPValue(clustering.getCluster(3))).totalObservations);
		assertEquals(6, ((EmpiricalPvalue) clusterPValues.getPValue(clustering.getCluster(3))).betterObservations);
		assertEquals(0.0144, clusterPValues.getPValue(clustering.getCluster(3)).getDouble(), 0.001);
		assertEquals(0.327, clusterPValues.getPValue(clustering.getCluster(4)).getDouble(), 0.001);
		assertEquals(0.00434, clusterPValues.getPValue(clustering.getCluster(5)).getDouble(), 0.001);
		assertEquals(0.0887, clusterPValues.getPValue(clustering.getCluster(6)).getDouble(), 0.001);
		assertEquals(0.0331, clusterPValues.getPValue(clustering.getCluster(7)).getDouble(), 0.001);
		assertEquals(0.0452, clusterPValues.getPValue(clustering.getCluster(8)).getDouble(), 0.001);
		assertEquals(0.126, clusterPValues.getPValue(clustering.getCluster(9)).getDouble(), 0.001);
		assertEquals(0.218, clusterPValues.getPValue(clustering.getCluster(10)).getDouble(), 0.001);
	}

}
