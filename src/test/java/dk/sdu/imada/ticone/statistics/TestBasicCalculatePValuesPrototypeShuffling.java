/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.CLARAClusteringMethod;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingPrototypeTimeSeries;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringWithRandomPrototypeTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ShuffleTimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.SimilarityFeatureValue;
import dk.sdu.imada.ticone.feature.scale.IScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.TanhNormalizerBuilder;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessValue;
import dk.sdu.imada.ticone.generate.TimeSeriesTestClass;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PValues;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 7, 2017
 *
 */
public class TestBasicCalculatePValuesPrototypeShuffling {

	private IPrototypeBuilder f;
	private TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f = new PrototypeBuilder().setPrototypeComponentBuilders(tsf);
		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries());
	}

	@Test
	public void testZeroWeights() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 0));
		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 0.0, 0.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		ShuffleClusteringByShufflingPrototypeTimeSeries shuffleClustering = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				sim, f, new ShuffleTimeSeries());

		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		calcPvalues.setForceDisableObjectSpecificDistribution(true);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;
		for (IPvalue d : calculatePValues.values(ObjectType.CLUSTER))
			assertEquals(1.0, d.getDouble(), 0.0);
	}

	@Test
	public void testOnlyAvgSimilarity() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o12 = new TimeSeriesObject("obj12", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 20));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o12, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		ShuffleClusteringByShufflingPrototypeTimeSeries shuffleClustering = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				sim, f, new ShuffleTimeSeries());

		System.out.println(fno);
		System.out.println(featureStore.keySet(fno));
		System.out.println(featureStore.valuesRaw(fno));
		System.out.println(fas);
		System.out.println(featureStore.keySet(fas));
		System.out.println(featureStore.valuesRaw(fas));
		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setStorePermutedFeatureValues(true);
		calcPvalues.setStorePermutedFitnessValues(true);
		calcPvalues.setFeatureStore(featureStore);
		calcPvalues.setForceDisableObjectSpecificDistribution(true);

		PValueCalculationResult calculatePValues = calcPvalues.calculatePValues(com, 42);
		PValues pvals = calculatePValues.pValues;

		System.out.println(c1);
		System.out.println(calcPvalues.featureStore.getFeatureValue(c1, fno) + " "
				+ calcPvalues.featureStore.getFeatureValue(c1, fas));

		System.out.println(c2);
		System.out.println(calcPvalues.featureStore.getFeatureValue(c2, fno) + " "
				+ calcPvalues.featureStore.getFeatureValue(c2, fas));

		List<IFeatureValue<Integer>> fnoArray = calculatePValues.permutedFeatureValuesObjectSpecific.get(fno);
		List<IFeatureValue<ISimilarityValue>> fasArray = calculatePValues.permutedFeatureValuesObjectSpecific.get(fas);
		List<IFitnessValue> permutedFitnessValues = calculatePValues.permutedFitnessValues.values(ObjectType.CLUSTER)
				.iterator().next();
		System.out.println(permutedFitnessValues);

		assertTrue(pvals.get(c1).getDouble() < pvals.get(c2).getDouble());
	}

	@Test
	public void testClustersEquallySimilarButDifferentSizes() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 1));
		ITimeSeriesObject o3 = new TimeSeriesObject("obj3", "s1", TimeSeries.of(1, 2, 1));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o3, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		ShuffleClusteringByShufflingPrototypeTimeSeries shuffleClustering = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				sim, f, new ShuffleTimeSeries());

		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		calcPvalues.setStorePermutedFeatureValues(true);
		calcPvalues.setStorePermutedFitnessValues(true);
		PValueCalculationResult calculatePValues2 = calcPvalues.calculatePValues(com, 42);
		PValues pvalues = calculatePValues2.pValues;
		System.out.println(calculatePValues2.originalFitnessValues.values(ObjectType.CLUSTER));
		calculatePValues2.permutedFitnessValues.values(ObjectType.CLUSTER).forEach(v -> System.out.println(v));
//		calcPvalues.permutedFeatureValues.forEach((k, v) -> System.out.println(Arrays.toString(v)));
		double p1 = pvalues.get(c1).getDouble();
		double p2 = pvalues.get(c2).getDouble();
		System.out.println(p1);
		System.out.println(p2);
		assertTrue(p1 > p2);
	}

	@Test
	public void testClustersEquallyLargeAndSimilar2() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 4));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 0));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o3 = new TimeSeriesObject("obj3", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o4 = new TimeSeriesObject("obj4", "s1", TimeSeries.of(1, 7, 0));
		com.addMapping(o3, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o4, c3, TestSimpleSimilarityValue.of(1.0));

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		ShuffleClusteringByShufflingPrototypeTimeSeries shuffleClustering = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				sim, f, new ShuffleTimeSeries());

		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;
		assertEquals(calculatePValues.get(c1).getDouble(), calculatePValues.get(c2).getDouble(), 0.0);
	}

	@Test
	public void testClustersEquallyLargeAndSimilar3() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o3 = new TimeSeriesObject("obj3", "s1", TimeSeries.of(1, 2, 3));

		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 1));
		ITimeSeriesObject o4 = new TimeSeriesObject("obj4", "s1", TimeSeries.of(1, 2, 1));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o3, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o4, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		ShuffleClusteringByShufflingPrototypeTimeSeries shuffleClustering = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				sim, f, new ShuffleTimeSeries());

		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setForceDisableObjectSpecificDistribution(true);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;
		assertEquals(calculatePValues.get(c1).getDouble(), calculatePValues.get(c2).getDouble(), 0.0);
	}

	@Test
	public void testClustersBalancedSizeVsSimilarity() throws Exception {
		// size of cluster 1 is half is large
		// avg sim of cluster 1 is twice is large
		// => it should be balanced out with equal feature weights
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		tsf.setTimeSeries(new double[] { 1, 2, 8 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();

		InverseEuclideanSimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 4));

		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 0));

		ITimeSeriesObject o3 = new TimeSeriesObject("obj3", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o4 = new TimeSeriesObject("obj4", "s1", TimeSeries.of(1, 2, 2));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		com.addMapping(o3, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o4, c3, TestSimpleSimilarityValue.of(1.0));

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		ShuffleClusteringByShufflingPrototypeTimeSeries shuffleClustering = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				sim, f, new ShuffleTimeSeries());

		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);

		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		assertEquals(calculatePValues.get(c1).getDouble(), calculatePValues.get(c2).getDouble(), 0.0);
	}

	@Test
	public void testSeed42() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();

		int numberOfInitialClusters = 20;
		Random random = new Random(42);
		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/easyDataset-hardcodedPatterns-objects40-seed1342-bg240-samples2-identicaltrue.txt.2.norm")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		IInitialClusteringProvider<ClusterObjectMapping> initialClustering = new InitialClusteringMethod(
				initialClusteringInterface, numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		ClusterObjectMapping com = initialClustering.getInitialClustering(objs);

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		ShuffleClusteringByShufflingPrototypeTimeSeries shuffleClustering = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				similarityFunction, f, new ShuffleTimeSeries());

		BasicCalculatePValues calcPvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvalues.setFeatureStore(featureStore);
		PValues calculatePValues = calcPvalues.calculatePValues(com, 42).pValues;

		ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
				similarityFunction).calculate(com)).getValue();

		for (ICluster c : calculatePValues.keySet(ObjectType.CLUSTER)) {
			System.out.println(String.format("%s\t%d\t%.3f", c.getName(), c.getObjects().size(),
					calculatePValues.get(c).getDouble()));
		}

	}

	@Test
	public void testGeneratedCluster() throws Exception {
		List<ICluster> cluster = new ArrayList<>();
		ClusterObjectMapping com = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 0, 0, 0, 0, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 1, 0, 0, 0, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 1, 0, 0, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 1, 0, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 0, 1, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 0, 0, 1 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 3, 0, 5, 1, 0 });
		cluster.add(com.getClusterBuilder().setPrototype(f.build()).build());

		TimeSeriesTestClass.generateObjectsForClusterPrototypes(com, TestSimpleSimilarityValue.of(-1), 0, 10);

		tsf.setTimeSeries(new double[] { 0, 2, 5, 1, 1 });
		// we need a 3rd cluster for the scalers
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("x5", "s1", TimeSeries.of(6, 2, 0, 1, 1));
		ITimeSeriesObject o6 = new TimeSeriesObject("x6", "s1", TimeSeries.of(1, 7, 0, 1, 1));
		ITimeSeriesObject o7 = new TimeSeriesObject("x7", "s1", TimeSeries.of(1, 7, 13, 1, 1));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		ITimeSeriesObjectList objs = com.getAssignedObjects();

		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

		Random random = new Random(42);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(),
				new ClusterFeatureAverageSimilarity(new InverseEuclideanSimilarityFunction()) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);

		IShuffle shuffleClustering = new ShuffleClusteringWithRandomPrototypeTimeSeries(similarityFunction, f,
				new CreateRandomTimeSeries(com.getAssignedObjects()));

		BasicCalculatePValues calcPvals = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 100);
		calcPvals.setFeatureStore(featureStore);

		PValueCalculationResult calculatePValues = calcPvals.calculatePValues(com, 42);
		PValues pvalues = calculatePValues.pValues;

		ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
				similarityFunction).calculate(com)).getValue();

		for (ICluster c : pvalues.keySet(ObjectType.CLUSTER)) {
			System.out.println(String.format("%s\t%d\t%.3f\t%.3f", c.getName(), c.getObjects().size(),
					calculatePValues.getOriginalFitness(fitness, c).getValue(), pvalues.get(c).getDouble()));
		}

	}

}
