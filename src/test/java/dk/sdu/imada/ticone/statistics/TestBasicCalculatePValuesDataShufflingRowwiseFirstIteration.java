/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.CLARAClusteringMethod;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingDataset;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.SimilarityFeatureValue;
import dk.sdu.imada.ticone.feature.scale.IScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.TanhNormalizerBuilder;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PValues;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 7, 2017
 *
 */
public class TestBasicCalculatePValuesDataShufflingRowwiseFirstIteration {

	@Test
	@Tag("long-running")
	public void testSeed42() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();

		int numberOfInitialClusters = 20;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		Scanner scan = new Scanner(new File(
				"/test_files/clustering/easyDataset-hardcodedPatterns-objects40-seed1342-bg240-samples2-identicaltrue.txt.2.norm"));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		IInitialClusteringProvider<ClusterObjectMapping> initialClustering = new InitialClusteringMethod(
				initialClusteringInterface, numberOfInitialClusters, 42);

		AbsoluteValues preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		ClusterObjectMapping com = initialClustering.getInitialClustering(objs);

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

		FeatureValueSampler fvSampler = new FeatureValueSampler();
		IFeatureValueSample<Integer> fnoSample = fvSampler.sampleFrom(fno, com, 100, 42l);
		IFeatureValueSample<ISimilarityValue> fasSample = fvSampler.sampleFrom(fas, com, 100, 42l);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(fno, com, 1000, 42l),
				new TanhNormalizerBuilder<>(fas, com, 1000, 42l) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();
		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fno, fas };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);
		fitness.setObjectsAndFeatureStore(com, featureStore);

		prototypeFactory = new PrototypeBuilder().setPrototypeComponentBuilders(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
								preprocessor.minValue, preprocessor.maxValue, 10, 10)));

		IShuffle shuffleClustering = new ShuffleClusteringByShufflingDataset(new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory),
				com.getClusters().size(), new ShuffleDatasetRowwise());

		BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				shuffleClustering, Arrays.asList(fitness), new List[] { new ArrayList<>() },
				Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 1000);

//		ClusterObjectMapping.resetClusterCount();

		PValues calculateClusterPValues = calculatePvalues.calculatePValues(com, 42).pValues;

		ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
				similarityFunction).calculate(com)).getValue();

		for (ICluster c : calculateClusterPValues.keySet(ObjectType.CLUSTER)) {
			System.out.println(
					String.format("%s\t%d\t%.3f", c.getName(), c.getObjects().size(), calculateClusterPValues.get(c)));
		}

	}

}
