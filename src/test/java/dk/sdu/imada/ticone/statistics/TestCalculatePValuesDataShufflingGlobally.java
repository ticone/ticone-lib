/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.Cluster;
import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.PAMKClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingDataset;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingNetwork;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.CreateRandomDataset;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ShuffleDatasetGlobally;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.scale.DummyScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.feature.scale.MinMaxScalerBuilder;
import dk.sdu.imada.ticone.feature.ObjectPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.ObjectPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.SimilarityFeatureValue;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.network.CreateRandomNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.ShuffleNetworkWithEdgeCrossover;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction;
import dk.sdu.imada.ticone.similarity.INetworkBasedSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ITimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.InverseShortestPathSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.WeightedAverageCompositeSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction.MISSING_CHILD_HANDLING;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PValues;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PermutedFeatureValues;
import dk.sdu.imada.ticone.util.Iterables;
import dk.sdu.imada.ticone.util.TestDataUtility;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleList;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Mar 12, 2019
 *
 */
public class TestCalculatePValuesDataShufflingGlobally {

	private static IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder;
	private static PrototypeBuilder prototypeBuilder;
	private static int numberOfInitialClusters;

	@BeforeAll
	public static void init() throws IncompatibleSimilarityFunctionException {
		prototypeBuilder = new PrototypeBuilder();
		clusteringMethodBuilder = new CLARAClusteringMethodBuilder()
				.setPamkBuilder(new PAMKClusteringMethodBuilder().setNstart(15)).setPrototypeBuilder(prototypeBuilder);
		numberOfInitialClusters = 10;
	}

	private static Stream<Arguments> randomTimeSeriesArgumentProvider() throws IncompatibleSimilarityFunctionException {
		final ISimilarityFunction[] timeSeriesSF = new ISimilarityFunction[] { new PearsonCorrelationFunction(),
				new InverseEuclideanSimilarityFunction() };

		final Object2ObjectMap<IShuffle, ISimilarityFunction[]> shuffleToSimilarityFunctions = new Object2ObjectOpenHashMap<>();
		ShuffleClusteringByShufflingDataset shuffle1 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new CreateRandomDataset());
		shuffle1.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle1, timeSeriesSF);
		ShuffleClusteringByShufflingDataset shuffle2 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new ShuffleDatasetRowwise());
		shuffle2.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle2, timeSeriesSF);
		ShuffleClusteringByShufflingDataset shuffle3 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new ShuffleDatasetGlobally());
		shuffle3.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle3, timeSeriesSF);

		final int[] numbersTimepoints = new int[] { 5, 30 };

		final int[] numbersObjects = new int[] { 250 };

		final Int2ObjectMap<int[]> numbersClusters = new Int2ObjectArrayMap<>();
		numbersClusters.put(250, new int[] { 10, 30 });

		final List<Arguments> list = new ArrayList<>();
		for (IShuffle shuffle : shuffleToSimilarityFunctions.keySet()) {
			for (ISimilarityFunction similarityFunction : shuffleToSimilarityFunctions.get(shuffle)) {
				for (int numberObjects : numbersObjects) {
					for (int numberTimepoints : numbersTimepoints) {
						for (int numberClusters : numbersClusters.get(numberObjects)) {
							list.add(Arguments.of(shuffle, similarityFunction, numberObjects, numberTimepoints,
									numberClusters));
						}
					}
				}
			}
		}
		return list.stream();
	}

	public static void main(String[] args) throws IncompatibleSimilarityFunctionException {
		if (args.length == 0)
			return;

		init();
		final TestCalculatePValuesDataShufflingGlobally test = new TestCalculatePValuesDataShufflingGlobally();
		if (args[0].equals("-uniform")) {
			Stream<Arguments> arguments = args[1].equals("edgeCrossover") || args[2].equals("shortestpath")
					? randomNetworkClustersArgumentProvider()
					: randomTimeSeriesArgumentProvider();
			arguments.forEach(a -> {
				Object[] objects = a.get();

				IShuffle shuffle = (IShuffle) objects[0];
				ISimilarityFunction similarityFunction = (ISimilarityFunction) objects[1];

				if ((args[1].equals("*")
						|| (args[1].equals("createRandomDataSet")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof CreateRandomDataset)
						|| (args[1].equals("shuffleObjectsIndividually")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof ShuffleDatasetRowwise)
						|| (args[1].equals("shuffleObjectsGlobally")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof ShuffleDatasetGlobally))
						&& (args[2].equals("*")
								|| (args[2].equals("pearson")
										&& similarityFunction instanceof PearsonCorrelationFunction)
								|| (args[2].equals("euclidean")
										&& similarityFunction instanceof InverseEuclideanSimilarityFunction)
								|| (args[2].equals("shortestpath")
										&& similarityFunction instanceof InverseShortestPathSimilarityFunction)))

					try {
						if (args[1].equals("edgeCrossover") || args[2].equals("shortestpath"))
							test.testUniformRandomSubnetworksPValues((ShuffleClusteringByShufflingNetwork) objects[0],
									similarityFunction, (Integer) objects[2], (Double) objects[3],
									(Integer) objects[4]);
						else
							test.testUniformRandomTimeSeriesPValues((ShuffleClusteringByShufflingDataset) objects[0],
									(ITimeSeriesSimilarityFunction) similarityFunction, (Integer) objects[2],
									(Integer) objects[3], (Integer) objects[4]);
					} catch (Throwable e) {
						e.printStackTrace();
					}
			});
		} else if (args[0].equals("-random")) {
			Stream<Arguments> arguments;
			if (args[1].equals("edgeCrossover") || args[2].equals("shortestpath"))
				arguments = randomNetworkClustersArgumentProvider();
			else if (args[2].equals("composite"))
				throw new IllegalArgumentException();
//				arguments = r
			else
				arguments = randomTimeSeriesArgumentProvider();
			arguments.forEach(a -> {
				Object[] objects = a.get();

				IShuffle shuffle = (IShuffle) objects[0];
				ISimilarityFunction similarityFunction = (ISimilarityFunction) objects[1];

				if ((args[1].equals("*")
						|| (args[1].equals("createRandomDataSet")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof CreateRandomDataset)
						|| (args[1].equals("shuffleObjectsIndividually")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof ShuffleDatasetRowwise)
						|| (args[1].equals("shuffleObjectsGlobally")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof ShuffleDatasetGlobally))
						&& (args[2].equals("*")
								|| (args[2].equals("pearson")
										&& similarityFunction instanceof PearsonCorrelationFunction)
								|| (args[2].equals("euclidean")
										&& similarityFunction instanceof InverseEuclideanSimilarityFunction)
								|| (args[2].equals("shortestpath")
										&& similarityFunction instanceof InverseShortestPathSimilarityFunction)))

					try {
						if (args[1].equals("edgeCrossover") || args[2].equals("shortestpath"))
							test.testRandomSubnetworksNotSignificant((ShuffleClusteringByShufflingNetwork) objects[0],
									similarityFunction, (Integer) objects[2], (Double) objects[3],
									(Integer) objects[4]);
						else
							test.testRandomTimeSeriesNotSignificant((ShuffleClusteringByShufflingDataset) objects[0],
									(ITimeSeriesSimilarityFunction) similarityFunction, (Integer) objects[2],
									(Integer) objects[3], (Integer) objects[4]);
					} catch (Throwable e) {
						e.printStackTrace();
					}
			});
		} else {
			Stream<Arguments> arguments;
			if (args[0].equals("edgeCrossover") || args[1].equals("shortestpath"))
				arguments = hiddenNetworkClustersArgumentProvider();
			else if (args[1].equals("composite"))
				arguments = hiddenCompositeClustersArgumentProvider();
			else
				arguments = hiddenTimeSeriesClustersArgumentProvider();
			arguments.forEach(a -> {
				Object[] objects = a.get();

				IShuffle shuffle = (IShuffle) objects[0];
				ISimilarityFunction similarityFunction = (ISimilarityFunction) objects[1];

				if ((args[0].equals("*")
						|| (args[0].equals("createRandomDataSet")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof CreateRandomDataset)
						|| (args[0].equals("shuffleObjectsIndividually")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof ShuffleDatasetRowwise)
						|| (args[0].equals("shuffleObjectsGlobally")
								&& shuffle instanceof ShuffleClusteringByShufflingDataset
								&& ((ShuffleClusteringByShufflingDataset) shuffle)
										.getShuffleDataset() instanceof ShuffleDatasetGlobally)
						|| (args[0].equals("edgeCrossover") && shuffle instanceof ShuffleClusteringByShufflingNetwork))
						&& (args[1].equals("*")
								|| (args[1].equals("pearson")
										&& similarityFunction instanceof PearsonCorrelationFunction)
								|| (args[1].equals("euclidean")
										&& similarityFunction instanceof InverseEuclideanSimilarityFunction)
								|| (args[1].equals("shortestpath")
										&& similarityFunction instanceof InverseShortestPathSimilarityFunction)
								|| (args[1].equals("composite")
										&& similarityFunction instanceof ICompositeSimilarityFunction)))

					try {
						if (args[0].equals("edgeCrossover") || args[1].equals("shortestpath"))
							test.testEnrichedSubnetworksInRandom((ShuffleClusteringByShufflingNetwork) objects[0],
									(INetworkBasedSimilarityFunction) similarityFunction, (Integer) objects[2],
									(int[]) objects[3], (Integer) objects[4]);
						else if (args[1].equals("composite"))
							test.testEnrichedCompositeInRandom((ShuffleClusteringByShufflingDataset) objects[0],
									(ICompositeSimilarityFunction) similarityFunction, (Integer) objects[2],
									(Integer) objects[3], (int[]) objects[4], (Integer) objects[5]);
						else
							test.testEnrichedTimeSeriesInRandom((ShuffleClusteringByShufflingDataset) objects[0],
									(ITimeSeriesSimilarityFunction) similarityFunction, (Integer) objects[2],
									(Integer) objects[3], (int[]) objects[4], (Integer) objects[5]);
					} catch (Throwable e) {
						e.printStackTrace();
					}
			});
		}
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "randomTimeSeriesArgumentProvider" })
	public void testRandomTimeSeriesNotSignificant(final ShuffleClusteringByShufflingDataset shuffle,
			final ITimeSeriesSimilarityFunction similarityFunction, final int numberObjects, final int numberTimepoints,
			final int numberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		long startTime = System.currentTimeMillis();
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		numberOfInitialClusters = numberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		final double alpha = 0.05;

		long totalCount = 0, fp_total = 0;
		for (final long dsSeed : LongStream.range(0, 25).toArray()) {
			logger.debug(String.format("### DS seed: %d", dsSeed));
			ITimeSeriesObjectList objs = TestDataUtility
					.randomObjects(new Random(dsSeed), numberObjects, numberTimepoints).asList();

			AbsoluteValues preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(objs);
			preprocessor.process();

			similarityFunction.setTimeSeriesPreprocessor(preprocessor);

			IDiscretizeTimeSeries discretizeTimeSeries = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
					preprocessor.minValue, preprocessor.maxValue, 15, 15);
			for (ITimeSeriesObject o : objs)
				for (int s = 0; s < o.getPreprocessedTimeSeriesList().length; s++)
					o.addPreprocessedTimeSeries(s,
							discretizeTimeSeries.discretizeObjectTimeSeries(o.getPreprocessedTimeSeriesList()[s]));

			IShuffleDataset shuffleDataset = shuffle.getShuffleDataset();
			if (shuffleDataset instanceof CreateRandomDataset)
				((CreateRandomDataset) shuffleDataset).setDiscretizeTimeSeries(discretizeTimeSeries);

			IAggregateCluster<ITimeSeries> aggregateCluster = new AggregateClusterMeanTimeSeries();

			prototypeBuilder.setPrototypeComponentBuilders(new TimeSeriesPrototypeComponentBuilder()
					.setAggregationFunction(aggregateCluster).setDiscretizeFunction(discretizeTimeSeries));

			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
					clusteringMethodBuilder, numberOfInitialClusters, dsSeed);

			ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(objs);

			ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
			ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

			double[] weights = new double[] { 1.0 };

			final IFeatureStore featureStore = new BasicFeatureStore();
			for (ICluster c : com.getClusters()) {
				featureStore.setFeatureValue(c, fno, fno.calculate(c));
				featureStore.setFeatureValue(c, fas, fas.calculate(c));
			}

			IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

			IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
			fitness.setObjectsAndFeatureStore(com, featureStore);
			BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
					shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
					Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 1000);
			calculatePvalues.setStorePermutedFeatureValues(false);
			calculatePvalues.setFeatureStore(featureStore);

			ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
					similarityFunction).calculate(com)).getValue();

			PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(com, dsSeed);
			PValues calculateClusterPValues = calculatePValues2.pValues;

			DoubleList pvalues = new DoubleArrayList();

			Set<ICluster> clusters = calculateClusterPValues.keySet(ObjectType.CLUSTER);
			for (ICluster c : clusters) {
				IPvalue pvalue = calculateClusterPValues.get(c);
				int n = c.getObjects().size();
				double sim = featureStore.getFeatureValue(c, fas).getValue().get();
				if (pvalue.getDouble() <= alpha)
					logger.debug(String.format("FP:\t%s\t%d\t%.3f\t%.3f\t%s", c.getName(), n, sim, pvalue.getDouble(),
							pvalue));

				pvalues.add(pvalue.getDouble());

				System.out.println(String.format("%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t%.5f", shuffle.toString(),
						similarityFunction, dsSeed, numberObjects, numberTimepoints, numberClusters,
						c.getClusterNumber(), c.size(), pvalue.getDouble()));
			}
			long fp = pvalues.stream().filter(p -> p <= alpha).count();
			fp_total += fp;
			int count = pvalues.size();
			totalCount += count;
		}
		// allow + 0.01
		double fpr = fp_total / (double) totalCount;
		assertTrue(fpr <= alpha + 0.01, String.format("FPR=%.3f was larger than alpha + 0.01=%.3f + 0.01", fpr, alpha));
	}

	private TiconeNetworkImpl createRandomNetwork(final int numberObjects, final long dsSeed,
			final double backgroundEdgeProbability) throws InterruptedException {
		return TestDataUtility.randomNetwork(new Random(dsSeed), numberObjects, false, backgroundEdgeProbability);
	}

	private TiconeNetworkImpl createEnrichedNetwork(final List<ITimeSeriesObjects> objects, final long dsSeed,
			final double foregroundEdgeProbability, final double backgroundEdgeProbability)
			throws InterruptedException {
		return TestDataUtility.enrichedSubnetworksInRandom(new Random(dsSeed), objects, false,
				backgroundEdgeProbability, foregroundEdgeProbability);
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "hiddenTimeSeriesClustersArgumentProvider" })
	public void testEnrichedTimeSeriesInRandom(final ShuffleClusteringByShufflingDataset shuffle,
			final ITimeSeriesSimilarityFunction similarityFunction, final int numberObjects, final int numberTimepoints,
			final int[] numberObjectsEnrichedTimeSeries, final int totalNumberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		long startTime = System.currentTimeMillis();
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		numberOfInitialClusters = totalNumberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		final double alpha = 0.05;

		long totalCount = 0, fp_total = 0;
		for (final long dsSeed : LongStream.range(0, 25).toArray()) {
			logger.debug(String.format("### DS seed: %d", dsSeed));
			List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility.enrichedTimeSeriesInRandom(
					new Random(dsSeed), numberObjects, numberTimepoints, numberObjectsEnrichedTimeSeries, 0.0);
			ITimeSeriesObjectList allObjects = new TimeSeriesObjectList();
			for (ITimeSeriesObjects os : enrichedTimeSeriesInRandom)
				allObjects.addAll(os);
			final ITimeSeriesObjectList randomObjs = enrichedTimeSeriesInRandom
					.get(enrichedTimeSeriesInRandom.size() - 1).asList();

			AbsoluteValues preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(allObjects);
			preprocessor.process();

			similarityFunction.setTimeSeriesPreprocessor(preprocessor);

			IDiscretizeTimeSeries discretizeTimeSeries = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
					preprocessor.minValue, preprocessor.maxValue, 15, 15);
			for (ITimeSeriesObject o : allObjects)
				for (int s = 0; s < o.getPreprocessedTimeSeriesList().length; s++)
					o.addPreprocessedTimeSeries(s,
							discretizeTimeSeries.discretizeObjectTimeSeries(o.getPreprocessedTimeSeriesList()[s]));

			IShuffleDataset shuffleDataset = shuffle.getShuffleDataset();
			if (shuffleDataset instanceof CreateRandomDataset)
				((CreateRandomDataset) shuffleDataset).setDiscretizeTimeSeries(discretizeTimeSeries);

			IAggregateCluster<ITimeSeries> aggregateCluster = new AggregateClusterMeanTimeSeries();

			prototypeBuilder.setPrototypeComponentBuilders(new TimeSeriesPrototypeComponentBuilder()
					.setAggregationFunction(aggregateCluster).setDiscretizeFunction(discretizeTimeSeries));

			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
					clusteringMethodBuilder, totalNumberClusters - enrichedTimeSeriesInRandom.size() + 1, dsSeed);

			final Set<ICluster> enrichedClusters = new HashSet<>();

			ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(randomObjs);
			for (int e = 0; e < enrichedTimeSeriesInRandom.size() - 1; e++)
				enrichedClusters.add(com.addCluster(enrichedTimeSeriesInRandom.get(e), similarityFunction));

			ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
			ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

			double[] weights = new double[] { 1.0 };

			final IFeatureStore featureStore = new BasicFeatureStore();
			for (ICluster c : com.getClusters()) {
				featureStore.setFeatureValue(c, fno, fno.calculate(c));
				featureStore.setFeatureValue(c, fas, fas.calculate(c));
			}

			IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

			IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
			fitness.setObjectsAndFeatureStore(com, featureStore);
			BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
					shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
					Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 1000);
			calculatePvalues.setStorePermutedFeatureValues(false);
			calculatePvalues.setFeatureStore(featureStore);

			ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
					similarityFunction).calculate(com)).getValue();

			PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(com, dsSeed);
			PValues calculateClusterPValues = calculatePValues2.pValues;

			DoubleList nonEnrichedClusterPvalues = new DoubleArrayList();

			Set<ICluster> clusters = calculateClusterPValues.keySet(ObjectType.CLUSTER);
			for (ICluster c : clusters) {
				IPvalue pvalue = calculateClusterPValues.get(c);
				int n = c.getObjects().size();
				double sim = featureStore.getFeatureValue(c, fas).getValue().get();
				if (pvalue.getDouble() <= alpha)
					logger.debug(String.format("FP:\t%s\t%d\t%.3f\t%.3f\t%s", c.getName(), n, sim, pvalue.getDouble(),
							pvalue));

				final boolean isEnrichedCluster = enrichedClusters.contains(c);
				if (!isEnrichedCluster)
					nonEnrichedClusterPvalues.add(pvalue.getDouble());

				System.out.println(String.format("%s\t%s\t%d\t%d\t%d\t%s\t%d\t%d\t%d\t%b\t%.5f\t%s", shuffle.toString(),
						similarityFunction, dsSeed, numberObjects, numberTimepoints,
						Arrays.toString(numberObjectsEnrichedTimeSeries), totalNumberClusters, c.getClusterNumber(),
						c.size(), isEnrichedCluster, pvalue.getDouble(), pvalue));
			}
			long fp = nonEnrichedClusterPvalues.stream().filter(p -> p <= alpha).count();
			fp_total += fp;
			int count = nonEnrichedClusterPvalues.size();
			totalCount += count;
		}

		// allow + 0.01
		double fpr = fp_total / (double) totalCount;

		assertTrue(fpr <= alpha + 0.01, String.format("FPR=%.3f was larger than alpha + 0.01=%.3f + 0.01", fpr, alpha));
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "hiddenNetworkClustersArgumentProvider" })
	public void testEnrichedSubnetworksInRandom(final ShuffleClusteringByShufflingNetwork shuffle,
			final INetworkBasedSimilarityFunction similarityFunction, final int numberObjects,
			final int[] numberObjectsEnrichedClusters, final int totalNumberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		long startTime = System.currentTimeMillis();
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		numberOfInitialClusters = totalNumberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		final double alpha = 0.05;
		final double backgroundEdgeProbability = 0.01;

		long totalCount = 0, fp_total = 0;
		for (final long dsSeed : LongStream.range(0, 25).toArray()) {
			logger.debug(String.format("### DS seed: %d", dsSeed));
			List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility.enrichedTimeSeriesInRandom(
					new Random(dsSeed), numberObjects, 5, numberObjectsEnrichedClusters, 0.0);

			INetworkBasedSimilarityFunction networkSF = similarityFunction;
			TiconeNetworkImpl randomNetwork = createEnrichedNetwork(enrichedTimeSeriesInRandom, dsSeed, 1.0,
					backgroundEdgeProbability);
			networkSF.setNetwork(randomNetwork);

			ITimeSeriesObjectList allObjects = new TimeSeriesObjectList();
			for (ITimeSeriesObjects os : enrichedTimeSeriesInRandom)
				allObjects.addAll(os.mapToNetwork(randomNetwork));
			final ITimeSeriesObjectList randomObjs = enrichedTimeSeriesInRandom
					.get(enrichedTimeSeriesInRandom.size() - 1).asList();

			networkSF.setNetwork(randomNetwork);
			shuffle.setNetwork(randomNetwork);
			randomNetwork.initializeForFeature(new ObjectPairFeatureShortestDistance(false));

			AbsoluteValues preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(allObjects);
			preprocessor.process();

			prototypeBuilder.setPrototypeComponentBuilders(new NetworkLocationPrototypeComponentBuilder()
					.setAggregationFunction(new AggregateClusterMedoidNode(randomNetwork, networkSF)));

			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
					clusteringMethodBuilder, totalNumberClusters - enrichedTimeSeriesInRandom.size() + 1, dsSeed);

			final Set<ICluster> enrichedClusters = new HashSet<>();

			ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(randomObjs);
			for (int e = 0; e < enrichedTimeSeriesInRandom.size() - 1; e++)
				enrichedClusters.add(com.addCluster(enrichedTimeSeriesInRandom.get(e), similarityFunction));

			ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
			ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

			double[] weights = new double[] { 1.0 };

			final IFeatureStore featureStore = new BasicFeatureStore();
			for (ICluster c : com.getClusters()) {
				featureStore.setFeatureValue(c, fno, fno.calculate(c));
				featureStore.setFeatureValue(c, fas, fas.calculate(c));
			}

			IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

			IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
			fitness.setObjectsAndFeatureStore(com, featureStore);
			BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
					shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
					Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 500);
			calculatePvalues.setStorePermutedFeatureValues(false);
			calculatePvalues.setFeatureStore(featureStore);

			ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
					similarityFunction).calculate(com)).getValue();

			PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(com, dsSeed);
			PValues calculateClusterPValues = calculatePValues2.pValues;

			DoubleList nonEnrichedClusterPvalues = new DoubleArrayList();

			final List<ICluster> clusters = new ClusterList(calculateClusterPValues.keySet(ObjectType.CLUSTER));
			Collections.sort(clusters, new Comparator<ICluster>() {
				@Override
				public int compare(ICluster o1, ICluster o2) {
					return o1.getClusterNumber() - o2.getClusterNumber();
				}
			});
			for (ICluster c : clusters) {
				IPvalue pvalue = calculateClusterPValues.get(c);
				int n = c.getObjects().size();
				double sim = featureStore.getFeatureValue(c, fas).getValue().get();
				if (pvalue.getDouble() <= alpha)
					logger.debug(String.format("FP:\t%s\t%d\t%.3f\t%.3f\t%s", c.getName(), n, sim, pvalue.getDouble(),
							pvalue));

				final boolean isEnrichedCluster = enrichedClusters.contains(c);
				if (!isEnrichedCluster)
					nonEnrichedClusterPvalues.add(pvalue.getDouble());

				System.out.println(String.format("%s\t%s\t%d\t%d\t%.3f/%.3f\t%s\t%d\t%d\t%d\t%b\t%.5f\t%s",
						shuffle.toString(), similarityFunction, dsSeed, numberObjects, 1.0, backgroundEdgeProbability,
						Arrays.toString(numberObjectsEnrichedClusters), totalNumberClusters, c.getClusterNumber(),
						c.size(), isEnrichedCluster, pvalue.getDouble(), pvalue));
			}
			long fp = nonEnrichedClusterPvalues.stream().filter(p -> p <= alpha).count();
			fp_total += fp;
			int count = nonEnrichedClusterPvalues.size();
			totalCount += count;

			long currTime = System.currentTimeMillis();
//			System.out.println(currTime - startTime);
			startTime = currTime;

			randomNetwork.clearThreadPool();
		}
		// allow + 0.01
		double fpr = fp_total / (double) totalCount;
		assertTrue(fpr <= alpha + 0.01, String.format("FPR=%.3f was larger than alpha + 0.01=%.3f + 0.01", fpr, alpha));
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "randomNetworkClustersArgumentProvider" })
	public void testRandomSubnetworksNotSignificant(final ShuffleClusteringByShufflingNetwork shuffle,
			final ISimilarityFunction similarityFunction, final int numberObjects,
			final double backgroundEdgeProbability, final int numberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		long startTime = System.currentTimeMillis();
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		numberOfInitialClusters = numberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		final double alpha = 0.05;

		long totalCount = 0, fp_total = 0;
		for (final long dsSeed : LongStream.range(0, 25).toArray()) {
			logger.debug(String.format("### DS seed: %d", dsSeed));
			ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(dsSeed), numberObjects, 5).asList();

			INetworkBasedSimilarityFunction networkSF = null;
			TiconeNetworkImpl randomNetwork = null;
			if (similarityFunction instanceof INetworkBasedSimilarityFunction) {
				networkSF = (INetworkBasedSimilarityFunction) similarityFunction;
				randomNetwork = createRandomNetwork(numberObjects, dsSeed, backgroundEdgeProbability);
				objs = objs.mapToNetwork(randomNetwork);
				networkSF.setNetwork(randomNetwork);
			} else if (similarityFunction instanceof ICompositeSimilarityFunction) {
				for (ISimilarityFunction childSF : ((ICompositeSimilarityFunction) similarityFunction)
						.getSimilarityFunctions())
					if (childSF instanceof INetworkBasedSimilarityFunction) {
						networkSF = (INetworkBasedSimilarityFunction) childSF;
						if (randomNetwork == null) {
							randomNetwork = createRandomNetwork(numberObjects, dsSeed, backgroundEdgeProbability);
							objs = objs.mapToNetwork(randomNetwork);
						}
					}
			}
			networkSF.setNetwork(randomNetwork);
			shuffle.setNetwork(randomNetwork);
			randomNetwork.performEdgeCrossovers(16.12, false, dsSeed);
			networkSF.initialize();

			AbsoluteValues preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(objs);
			preprocessor.process();

			AggregateClusterMedoidNode aggregateCluster = new AggregateClusterMedoidNode(randomNetwork, networkSF);
			aggregateCluster.setNetwork(randomNetwork);
			prototypeBuilder.setPrototypeComponentBuilders(
					new NetworkLocationPrototypeComponentBuilder().setAggregationFunction(aggregateCluster));

			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
					clusteringMethodBuilder, numberOfInitialClusters, dsSeed);

			ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(objs);

			ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
			ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

			double[] weights = new double[] { 1.0 };

			final IFeatureStore featureStore = new BasicFeatureStore();
			for (ICluster c : com.getClusters()) {
				featureStore.setFeatureValue(c, fno, fno.calculate(c));
				featureStore.setFeatureValue(c, fas, fas.calculate(c));
			}

			IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

			IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
			fitness.setObjectsAndFeatureStore(com, featureStore);
			BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
					shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
					Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 1000);
//			calculatePvalues.addChangeListener(new ChangeListener() {
//				
//				@Override
//				public void stateChanged(ChangeEvent e) {
//					System.out.println(((PermutationTestChangeEvent)e).getPercentage());
//				}
//			});
			calculatePvalues.setStorePermutedFeatureValues(false);
			calculatePvalues.setFeatureStore(featureStore);

			ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
					similarityFunction).calculate(com)).getValue();

			PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(com, dsSeed);
			PValues calculateClusterPValues = calculatePValues2.pValues;

			DoubleList pvalues = new DoubleArrayList();

			final List<ICluster> clusters = new ClusterList(calculateClusterPValues.keySet(ObjectType.CLUSTER));
			Collections.sort(clusters, new Comparator<ICluster>() {
				@Override
				public int compare(ICluster o1, ICluster o2) {
					return o1.getClusterNumber() - o2.getClusterNumber();
				}
			});
			for (ICluster c : clusters) {
				IPvalue pvalue = calculateClusterPValues.get(c);
				int n = c.getObjects().size();
				double sim = featureStore.getFeatureValue(c, fas).getValue().get();
				if (pvalue.getDouble() <= alpha)
					logger.debug(String.format("FP:\t%s\t%d\t%.3f\t%.3f\t%s", c.getName(), n, sim, pvalue.getDouble(),
							pvalue));

				pvalues.add(pvalue.getDouble());

				System.out.println(String.format("%s\t%s\t%d\t%d\t%.3f\t%d\t%d\t%d\t%.5f", shuffle.toString(),
						similarityFunction, dsSeed, numberObjects, backgroundEdgeProbability, numberClusters,
						c.getClusterNumber(), c.size(), pvalue.getDouble()));
			}
			long fp = pvalues.stream().filter(p -> p <= alpha).count();
			fp_total += fp;
			int count = pvalues.size();
			totalCount += count;
		}
		// allow + 0.01
		double fpr = fp_total / (double) totalCount;
		assertTrue(fpr <= alpha + 0.01, String.format("FPR=%.3f was larger than alpha + 0.01=%.3f + 0.01", fpr, alpha));
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "randomNetworkClustersArgumentProvider" })
	public void testRandomSubnetworksOriginalFeaturesReproducible(final ShuffleClusteringByShufflingNetwork shuffle,
			final ISimilarityFunction similarityFunction, final int numberObjects,
			final double backgroundEdgeProbability, final int numberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		numberOfInitialClusters = numberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		List<int[]> featureValuesNO = new ArrayList<>();
		List<double[]> featureValuesAS = new ArrayList<>();

		final long dsSeed = 0;
		for (int i = 0; i < 5; i++) {
			ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(dsSeed), numberObjects, 5).asList();

			INetworkBasedSimilarityFunction networkSF = null;
			TiconeNetworkImpl randomNetwork = null;
			if (similarityFunction instanceof INetworkBasedSimilarityFunction) {
				networkSF = (INetworkBasedSimilarityFunction) similarityFunction;
				randomNetwork = createRandomNetwork(numberObjects, dsSeed, backgroundEdgeProbability);
				randomNetwork.performEdgeCrossovers(16.42, false, dsSeed);
				objs.mapToNetwork(randomNetwork);
				networkSF.setNetwork(randomNetwork);
			} else if (similarityFunction instanceof ICompositeSimilarityFunction) {
				for (ISimilarityFunction childSF : ((ICompositeSimilarityFunction) similarityFunction)
						.getSimilarityFunctions())
					if (childSF instanceof INetworkBasedSimilarityFunction) {
						networkSF = (INetworkBasedSimilarityFunction) childSF;
						if (randomNetwork == null) {
							randomNetwork = createRandomNetwork(numberObjects, dsSeed, backgroundEdgeProbability);
							objs.mapToNetwork(randomNetwork);
						}
					}
			}
			networkSF.setNetwork(randomNetwork);
			shuffle.setNetwork(randomNetwork);
			randomNetwork.initializeForFeature(new ObjectPairFeatureShortestDistance(false));

			AbsoluteValues preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(objs);
			preprocessor.process();

			prototypeBuilder.setPrototypeComponentBuilders(new NetworkLocationPrototypeComponentBuilder()
					.setAggregationFunction(new AggregateClusterMedoidNode(randomNetwork, networkSF)));

			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
					clusteringMethodBuilder, numberOfInitialClusters, dsSeed);

			ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(objs);

			ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
			ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

			final IFeatureStore featureStore = new BasicFeatureStore();
			for (ICluster c : com.getClusters()) {
				featureStore.setFeatureValue(c, fno, fno.calculate(c));
				featureStore.setFeatureValue(c, fas, fas.calculate(c));
			}

			featureValuesNO.add(IntStream.range(1, com.getClusters().size() + 1)
					.map(cN -> featureStore.getFeatureValue(com.getCluster(cN), fno).getValue().intValue()).toArray());
			featureValuesAS.add(IntStream.range(1, com.getClusters().size() + 1).mapToDouble(cN -> {
				try {
					return featureStore.getFeatureValue(com.getCluster(cN), fas).getValue().get();
				} catch (SimilarityCalculationException e) {
					return Double.NaN;
				}
			}).toArray());
		}
		for (int i = 0; i < featureValuesNO.size() - 1; i++) {
			assertArrayEquals(featureValuesNO.get(i), featureValuesNO.get(i + 1));
			assertArrayEquals(featureValuesAS.get(i), featureValuesAS.get(i + 1));
		}
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "randomNetworkClustersArgumentProvider" })
	public void testRandomSubnetworksPermutedFeaturesReproducible(final ShuffleClusteringByShufflingNetwork shuffle,
			final ISimilarityFunction similarityFunction, final int numberObjects,
			final double backgroundEdgeProbability, final int numberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		shuffle.setRandomNumberClusters(false);

		numberOfInitialClusters = numberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		List<IFeatureValue<Integer>[]> featureValuesNO = new ArrayList<>();
		List<IFeatureValue<ISimilarityValue>[]> featureValuesAS = new ArrayList<>();

		final long dsSeed = 0;
		for (int i = 0; i < 5; i++) {
			ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(dsSeed), numberObjects, 5).asList();

			INetworkBasedSimilarityFunction networkSF = null;
			TiconeNetworkImpl randomNetwork = null;
			if (similarityFunction instanceof INetworkBasedSimilarityFunction) {
				networkSF = (INetworkBasedSimilarityFunction) similarityFunction;
				randomNetwork = createRandomNetwork(numberObjects, dsSeed, backgroundEdgeProbability);
				randomNetwork.performEdgeCrossovers(16.42, false, dsSeed);
				objs.mapToNetwork(randomNetwork);
				networkSF.setNetwork(randomNetwork);
			} else if (similarityFunction instanceof ICompositeSimilarityFunction) {
				for (ISimilarityFunction childSF : ((ICompositeSimilarityFunction) similarityFunction)
						.getSimilarityFunctions())
					if (childSF instanceof INetworkBasedSimilarityFunction) {
						networkSF = (INetworkBasedSimilarityFunction) childSF;
						if (randomNetwork == null) {
							randomNetwork = createRandomNetwork(numberObjects, dsSeed, backgroundEdgeProbability);
							objs.mapToNetwork(randomNetwork);
						}
					}
			}
			networkSF.setNetwork(randomNetwork);
			shuffle.setNetwork(randomNetwork);
			randomNetwork.initializeForFeature(new ObjectPairFeatureShortestDistance(false));

			AbsoluteValues preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(objs);
			preprocessor.process();

			prototypeBuilder.setPrototypeComponentBuilders(new NetworkLocationPrototypeComponentBuilder()
					.setAggregationFunction(new AggregateClusterMedoidNode(randomNetwork, networkSF)));

			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
					clusteringMethodBuilder, numberOfInitialClusters, dsSeed);

			ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(objs);

			ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
			ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

			final IFeatureStore featureStore = new BasicFeatureStore();
			for (ICluster c : com.getClusters()) {
				featureStore.setFeatureValue(c, fno, fno.calculate(c));
				featureStore.setFeatureValue(c, fas, fas.calculate(c));
			}

			IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

			double[] weights = new double[] { 1.0 };

			IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
			fitness.setObjectsAndFeatureStore(com, featureStore);
			BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
					shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
					Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 2);
			calculatePvalues.setStorePermutedFeatureValues(true);
			calculatePvalues.setStorePermutedObjectInFeatureValues(true);
			calculatePvalues.setFeatureStore(featureStore);

			ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
					similarityFunction).calculate(com)).getValue();

			PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(com, dsSeed);
			final PermutedFeatureValues permutedFeatureValues = calculatePValues2.permutedFeatureValues;

			featureValuesNO.add(permutedFeatureValues.get(fno).toArray(new IFeatureValue[0]));
			featureValuesAS.add(permutedFeatureValues.get(fas).toArray(new IFeatureValue[0]));

			if (i == 0) {
				System.out.println("fno:");
				int[] fnosOrig = Iterables.toList(featureStore.getFeatureValues(fno)).stream()
						.mapToInt(v -> v.getValue().intValue()).toArray();
				int[] fnosPerm = Arrays.stream(featureValuesNO.get(0)).mapToInt(v -> v.getValue().intValue()).toArray();
				System.out.println("orig:\t" + ArraysExt.mean(fnosOrig) + "\t" + Arrays.toString(fnosOrig));
				System.out.println("perm:\t" + ArraysExt.mean(fnosPerm) + "\t" + Arrays.toString(fnosPerm));

				System.out.println("fas:");
				double[] fassOrig = Iterables.toList(featureStore.getFeatureValues(fas)).stream().mapToDouble(v -> {
					try {
						return v.getValue().get();
					} catch (SimilarityCalculationException e) {
						return Double.NaN;
					}
				}).toArray();
				double[] fassPerm = Arrays.stream(featureValuesAS.get(0)).mapToDouble(v -> {
					try {
						return v.getValue().get();
					} catch (SimilarityCalculationException e) {
						return Double.NaN;
					}
				}).toArray();
				System.out.println("orig:\t" + ArraysExt.mean(fassOrig) + "\t" + Arrays.toString(fassOrig));
				System.out.println("perm:\t" + ArraysExt.mean(fassPerm) + "\t" + Arrays.toString(fassPerm));
			}
		}
		for (int i = 0; i < featureValuesNO.size() - 1; i++) {
			IFeatureValue<Integer>[] fno1 = featureValuesNO.get(i);
			IFeatureValue<Integer>[] fno2 = featureValuesNO.get(i + 1);
			for (int j = 0; j < fno1.length; j++)
				try {
					assertEquals(fno1[j], fno2[j]);
				} catch (Throwable e) {
					System.out.println(Arrays.toString(fno1));
					System.out.println(Arrays.toString(fno2));
					Cluster c1 = (Cluster) fno1[j].getObject();
					Cluster c2 = (Cluster) fno2[j].getObject();
					System.out.println(c1);
					System.out.println(c2);
					System.out.println(c1.getClustering().getClusters());
					System.out.println(c2.getClustering().getClusters());
					throw e;
				}
			IFeatureValue<ISimilarityValue>[] fas1 = featureValuesAS.get(i);
			IFeatureValue<ISimilarityValue>[] fas2 = featureValuesAS.get(i + 1);
			for (int j = 0; j < fas1.length; j++)
				try {
					assertEquals(fas1[j], fas2[j]);
				} catch (Throwable e) {
					System.out.println(Arrays.toString(fas1));
					System.out.println(Arrays.toString(fas2));
					System.out.println(fas1[j].getObject());
					System.out.println(fas2[j].getObject());
					throw e;
				}
		}
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "randomNetworkClustersArgumentProvider" })
	public void testRandomSubnetworksPermutedClusteringsReproducible(final ShuffleClusteringByShufflingNetwork shuffle,
			final ISimilarityFunction similarityFunction, final int numberObjects,
			final double backgroundEdgeProbability, final int numberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		shuffle.setRandomNumberClusters(false);

		numberOfInitialClusters = numberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		List<Cluster[]> permutedClusters = new ArrayList<>();

		final long dsSeed = 0;
		for (int i = 0; i < 5; i++) {
			ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(dsSeed), numberObjects, 5).asList();

			INetworkBasedSimilarityFunction networkSF = null;
			TiconeNetworkImpl randomNetwork = null;
			if (similarityFunction instanceof INetworkBasedSimilarityFunction) {
				networkSF = (INetworkBasedSimilarityFunction) similarityFunction;
				randomNetwork = createRandomNetwork(numberObjects, dsSeed, backgroundEdgeProbability);
				randomNetwork.performEdgeCrossovers(16.42, false, dsSeed);
				objs.mapToNetwork(randomNetwork);
				networkSF.setNetwork(randomNetwork);
			} else if (similarityFunction instanceof ICompositeSimilarityFunction) {
				for (ISimilarityFunction childSF : ((ICompositeSimilarityFunction) similarityFunction)
						.getSimilarityFunctions())
					if (childSF instanceof INetworkBasedSimilarityFunction) {
						networkSF = (INetworkBasedSimilarityFunction) childSF;
						if (randomNetwork == null) {
							randomNetwork = createRandomNetwork(numberObjects, dsSeed, backgroundEdgeProbability);
							objs.mapToNetwork(randomNetwork);
						}
					}
			}
			networkSF.setNetwork(randomNetwork);
			shuffle.setNetwork(randomNetwork);
			randomNetwork.initializeForFeature(new ObjectPairFeatureShortestDistance(false));

			AbsoluteValues preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(objs);
			preprocessor.process();

			prototypeBuilder.setPrototypeComponentBuilders(new NetworkLocationPrototypeComponentBuilder()
					.setAggregationFunction(new AggregateClusterMedoidNode(randomNetwork, networkSF)));

			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
					clusteringMethodBuilder, numberOfInitialClusters, dsSeed);

			ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(objs);

			ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
			ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

			final IFeatureStore featureStore = new BasicFeatureStore();
			for (ICluster c : com.getClusters()) {
				featureStore.setFeatureValue(c, fno, fno.calculate(c));
				featureStore.setFeatureValue(c, fas, fas.calculate(c));
			}

			IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

			double[] weights = new double[] { 1.0 };

			IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
			fitness.setObjectsAndFeatureStore(com, featureStore);
			BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
					shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
					Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 2);
			calculatePvalues.setStorePermutedFeatureValues(true);
			calculatePvalues.setStorePermutedObjectInFeatureValues(true);
			calculatePvalues.setFeatureStore(featureStore);

			ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
					similarityFunction).calculate(com)).getValue();

			PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(com, dsSeed);
			final PermutedFeatureValues permutedFeatureValues = calculatePValues2.permutedFeatureValues;

			permutedClusters.add(permutedFeatureValues.get(fno).stream().map(v -> ((Cluster) v.getObject()))
					.toArray(Cluster[]::new));
		}
		for (int i = 0; i < permutedClusters.size() - 1; i++) {
			Cluster[] clusters1 = permutedClusters.get(i);
			for (int j = 0; j < clusters1.length; j++) {
				Cluster[] clusters2 = permutedClusters.get(i + 1);
				Map cObjs1 = clusters1[j].getSimilarities();
				Map cObjs2 = clusters2[j].getSimilarities();
				try {
					assertEquals(cObjs1.keySet(), cObjs2.keySet());
				} catch (Throwable e) {
					System.out.println(clusters1[j]);
					System.out.println(clusters2[j]);
					System.out.println(cObjs1.keySet());
					System.out.println(cObjs2.keySet());
					throw e;
				}
				try {
					assertEquals(cObjs1, cObjs2);
				} catch (Throwable e) {
					System.out.println(cObjs1);
					System.out.println(cObjs2);
					System.out.println(clusters1[j].getPrototype());
					System.out.println(clusters2[j].getPrototype());
					throw e;
				}
			}
		}
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "randomTimeSeriesArgumentProvider" })
	public void testUniformRandomTimeSeriesPValues(final ShuffleClusteringByShufflingDataset shuffle,
			final ITimeSeriesSimilarityFunction similarityFunction, final int numberObjects, final int numberTimepoints,
			final int numberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		long startTime = System.currentTimeMillis();
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		numberOfInitialClusters = numberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		// create initial clustering
		long seed = 0;
		ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(seed), numberObjects, numberTimepoints)
				.asList();

		AbsoluteValues preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		similarityFunction.setTimeSeriesPreprocessor(preprocessor);

		IDiscretizeTimeSeries discretizeTimeSeries = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 15, 15);
		for (ITimeSeriesObject o : objs)
			for (int s = 0; s < o.getPreprocessedTimeSeriesList().length; s++)
				o.addPreprocessedTimeSeries(s,
						discretizeTimeSeries.discretizeObjectTimeSeries(o.getPreprocessedTimeSeriesList()[s]));

		IShuffleDataset shuffleDataset = shuffle.getShuffleDataset();
		if (shuffleDataset instanceof CreateRandomDataset)
			((CreateRandomDataset) shuffleDataset).setDiscretizeTimeSeries(discretizeTimeSeries);

		IAggregateCluster<ITimeSeries> aggregateCluster = new AggregateClusterMeanTimeSeries();

		prototypeBuilder.setPrototypeComponentBuilders(new TimeSeriesPrototypeComponentBuilder()
				.setAggregationFunction(aggregateCluster).setDiscretizeFunction(discretizeTimeSeries));

		IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
				clusteringMethodBuilder, numberOfInitialClusters, seed);

		ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(objs);
		// end create initial clustering

		for (seed = 1; seed < 25; seed++) {
			try {
				logger.debug(String.format("### seed: %d", seed));

				final IShuffleResult shuffleResult = shuffle.shuffle(com, seed);
				final ClusterObjectMapping shuffledClustering = (ClusterObjectMapping) shuffleResult.getShuffled();

				ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
				ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

				double[] weights = new double[] { 1.0 };

				final IFeatureStore featureStore = new BasicFeatureStore();
				for (ICluster c : shuffledClustering.getClusters()) {
					featureStore.setFeatureValue(c, fno, fno.calculate(c));
					featureStore.setFeatureValue(c, fas, fas.calculate(c));
				}

				IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

				IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
				fitness.setObjectsAndFeatureStore(shuffledClustering, featureStore);
				BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
						shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
						Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 1000);
				calculatePvalues.setStorePermutedFeatureValues(false);
				calculatePvalues.setFeatureStore(featureStore);

				ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
						similarityFunction).calculate(shuffledClustering)).getValue();

				PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(shuffledClustering, seed);
				PValues calculateClusterPValues = calculatePValues2.pValues;

				DoubleList pvalues = new DoubleArrayList();

				Set<ICluster> clusters = calculateClusterPValues.keySet(ObjectType.CLUSTER);
				for (ICluster c : clusters) {
					IPvalue pvalue = calculateClusterPValues.get(c);
					int n = c.getObjects().size();

					pvalues.add(pvalue.getDouble());

					System.out.println(String.format("%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t%.5f", shuffle.toString(),
							similarityFunction, seed, numberObjects, numberTimepoints, numberClusters,
							c.getClusterNumber(), c.size(), pvalue.getDouble()));
				}
			} catch (Exception e) {
			}
		}
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "randomNetworkClustersArgumentProvider" })
	public void testUniformRandomSubnetworksPValues(final ShuffleClusteringByShufflingNetwork shuffle,
			final ISimilarityFunction similarityFunction, final int numberObjects,
			final double backgroundEdgeProbability, final int numberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.INFO);
		long startTime = System.currentTimeMillis();
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		numberOfInitialClusters = numberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		long seed = 0;

		ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(seed), numberObjects, 5).asList();

		INetworkBasedSimilarityFunction networkSF = null;
		TiconeNetworkImpl randomNetwork = null;
		if (similarityFunction instanceof INetworkBasedSimilarityFunction) {
			networkSF = (INetworkBasedSimilarityFunction) similarityFunction;
			randomNetwork = createRandomNetwork(numberObjects, seed, backgroundEdgeProbability);
			objs = objs.mapToNetwork(randomNetwork);
			networkSF.setNetwork(randomNetwork);
		} else if (similarityFunction instanceof ICompositeSimilarityFunction) {
			for (ISimilarityFunction childSF : ((ICompositeSimilarityFunction) similarityFunction)
					.getSimilarityFunctions())
				if (childSF instanceof INetworkBasedSimilarityFunction) {
					networkSF = (INetworkBasedSimilarityFunction) childSF;
					if (randomNetwork == null) {
						randomNetwork = createRandomNetwork(numberObjects, seed, backgroundEdgeProbability);
						objs = objs.mapToNetwork(randomNetwork);
					}
				}
		}
		networkSF.setNetwork(randomNetwork);
		shuffle.setNetwork(randomNetwork);
		randomNetwork.performEdgeCrossovers(16.12, false, seed);
		networkSF.initialize();

		AbsoluteValues preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		AggregateClusterMedoidNode aggregateCluster = new AggregateClusterMedoidNode(randomNetwork, networkSF);
		aggregateCluster.setNetwork(randomNetwork);
		prototypeBuilder.setPrototypeComponentBuilders(
				new NetworkLocationPrototypeComponentBuilder().setAggregationFunction(aggregateCluster));

		IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
				clusteringMethodBuilder, numberOfInitialClusters, seed);

		ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(objs);

		for (seed = 1; seed < 25; seed++) {
			try {
				logger.debug(String.format("### seed: %d", seed));

				shuffle.setNetwork(randomNetwork);
				networkSF.setNetwork(randomNetwork);

				final IShuffleResult shuffleResult = shuffle.shuffle(com, seed);
				final ClusterObjectMapping shuffledClustering = (ClusterObjectMapping) shuffleResult.getShuffled();
				shuffle.setNetwork((ITiconeNetwork<?, ?>) shuffleResult.getShuffledSupport().get(randomNetwork));

				ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
				ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

				double[] weights = new double[] { 1.0 };

				final IFeatureStore featureStore = new BasicFeatureStore();
				for (ICluster c : shuffledClustering.getClusters()) {
					featureStore.setFeatureValue(c, fno, fno.calculate(c));
					featureStore.setFeatureValue(c, fas, fas.calculate(c));
				}

				IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

				IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
				fitness.setObjectsAndFeatureStore(shuffledClustering, featureStore);
				BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
						shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
						Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 1000);
				calculatePvalues.setStorePermutedFeatureValues(false);
				calculatePvalues.setFeatureStore(featureStore);

				ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
						similarityFunction).calculate(shuffledClustering)).getValue();

				PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(shuffledClustering, seed);
				PValues calculateClusterPValues = calculatePValues2.pValues;

				DoubleList pvalues = new DoubleArrayList();

				Set<ICluster> clusters = calculateClusterPValues.keySet(ObjectType.CLUSTER);
				for (ICluster c : clusters) {
					IPvalue pvalue = calculateClusterPValues.get(c);
					int n = c.getObjects().size();

					pvalues.add(pvalue.getDouble());

					System.out.println(String.format("%s\t%s\t%d\t%d\t%.3f\t%d\t%d\t%d\t%.5f", shuffle.toString(),
							similarityFunction, seed, numberObjects, backgroundEdgeProbability, numberClusters,
							c.getClusterNumber(), c.size(), pvalue.getDouble()));
				}
			} catch (Exception e) {
			}
		}
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "hiddenCompositeClustersArgumentProvider" })
	public void testEnrichedCompositeInRandom(final ShuffleClusteringByShufflingDataset shuffle,
			final ICompositeSimilarityFunction similarityFunction, final int numberObjects, final int numberTimepoints,
			final int[] numberObjectsEnrichedTimeSeries, final int totalNumberClusters) throws Exception {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.setLevel(Level.WARN);
		long startTime = System.currentTimeMillis();
		logger.debug(String.format("Shuffle: %s", shuffle.toString()));

		numberOfInitialClusters = totalNumberClusters;
		shuffle.setSimilarityFunction(similarityFunction);
		clusteringMethodBuilder.setSimilarityFunction(similarityFunction);

		final double alpha = 0.05;
		final double backgroundEdgeProbability = 0.01;
		final double foregroundEdgeProbability = 1.0;

		long totalCount = 0, fp_total = 0;
		for (final long dsSeed : LongStream.range(0, 25).toArray()) {
			TiconeNetworkImpl randomNetwork = null;
			try {
				logger.debug(String.format("### DS seed: %d", dsSeed));
				List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility.enrichedTimeSeriesInRandom(
						new Random(dsSeed), numberObjects, numberTimepoints, numberObjectsEnrichedTimeSeries, 0.0);

				ITimeSeriesSimilarityFunction tsSF = null;
				INetworkBasedSimilarityFunction networkSF = null;
				for (ISimilarityFunction childSF : similarityFunction.getSimilarityFunctions())
					if (childSF instanceof INetworkBasedSimilarityFunction) {
						networkSF = (INetworkBasedSimilarityFunction) childSF;
						if (randomNetwork == null) {
							randomNetwork = createEnrichedNetwork(enrichedTimeSeriesInRandom, dsSeed,
									foregroundEdgeProbability, backgroundEdgeProbability);
						}
					} else if (childSF instanceof ITimeSeriesSimilarityFunction)
						tsSF = (ITimeSeriesSimilarityFunction) childSF;

				ITimeSeriesObjectList allObjects = new TimeSeriesObjectList();
				for (ITimeSeriesObjects os : enrichedTimeSeriesInRandom)
					allObjects.addAll(os.mapToNetwork(randomNetwork));
				final ITimeSeriesObjectList randomObjs = enrichedTimeSeriesInRandom
						.get(enrichedTimeSeriesInRandom.size() - 1).asList();

				if (networkSF != null) {
					networkSF.setNetwork(randomNetwork);
					randomNetwork.initializeForFeature(new ObjectPairFeatureShortestDistance(false));
				}

				AbsoluteValues preprocessor = new AbsoluteValues();
				preprocessor.initializeObjects(allObjects);
				preprocessor.process();

				if (tsSF != null)
					tsSF.setTimeSeriesPreprocessor(preprocessor);

				IDiscretizeTimeSeries discretizeTimeSeries = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
						preprocessor.minValue, preprocessor.maxValue, 15, 15);
				for (ITimeSeriesObject o : allObjects)
					for (int s = 0; s < o.getPreprocessedTimeSeriesList().length; s++)
						o.addPreprocessedTimeSeries(s,
								discretizeTimeSeries.discretizeObjectTimeSeries(o.getPreprocessedTimeSeriesList()[s]));

				IShuffleDataset shuffleDataset = shuffle.getShuffleDataset();
				if (shuffleDataset instanceof CreateRandomDataset)
					((CreateRandomDataset) shuffleDataset).setDiscretizeTimeSeries(discretizeTimeSeries);

				IAggregateCluster<ITimeSeries> aggregateCluster = new AggregateClusterMeanTimeSeries();

				prototypeBuilder.setPrototypeComponentBuilders(
						new NetworkLocationPrototypeComponentBuilder()
								.setAggregationFunction(new AggregateClusterMedoidNode(randomNetwork, networkSF)),
						new TimeSeriesPrototypeComponentBuilder()
								.setAggregationFunction(new AggregateClusterMeanTimeSeries())
								.setDiscretizeFunction(discretizeTimeSeries));

				similarityFunction.setMissingChildHandling(MISSING_CHILD_HANDLING.TREAT_AS_ZERO);

				IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
						clusteringMethodBuilder, totalNumberClusters - enrichedTimeSeriesInRandom.size() + 1, dsSeed);

				final Set<ICluster> enrichedClusters = new HashSet<>();

				ClusterObjectMapping com = initialClusteringProvider.getInitialClustering(randomObjs);
				for (int e = 0; e < enrichedTimeSeriesInRandom.size() - 1; e++)
					enrichedClusters.add(com.addCluster(enrichedTimeSeriesInRandom.get(e), similarityFunction));

				ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
				ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(similarityFunction);

				double[] weights = new double[] { 1.0 };

				final IFeatureStore featureStore = new BasicFeatureStore();
				for (ICluster c : com.getClusters()) {
					featureStore.setFeatureValue(c, fno, fno.calculate(c));
					featureStore.setFeatureValue(c, fas, fas.calculate(c));
				}

				IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fas };

				IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, null, weights);
				fitness.setObjectsAndFeatureStore(com, featureStore);
				BasicCalculatePValues calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
						shuffle, Arrays.asList(fitness), new List[] { Arrays.asList(fno) },
						Arrays.asList(new Boolean[] { false }), new MultiplyPValues(), 10);
				calculatePvalues.setStorePermutedFeatureValues(false);
				calculatePvalues.setFeatureStore(featureStore);

				ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
						similarityFunction).calculate(com)).getValue();

				PValueCalculationResult calculatePValues2 = calculatePvalues.calculatePValues(com, dsSeed);

				PValues calculateClusterPValues = calculatePValues2.pValues;

				DoubleList nonEnrichedClusterPvalues = new DoubleArrayList();

				Set<ICluster> clusters = calculateClusterPValues.keySet(ObjectType.CLUSTER);
				for (ICluster c : clusters) {
					IPvalue pvalue = calculateClusterPValues.get(c);
					int n = c.getObjects().size();
					double sim = featureStore.getFeatureValue(c, fas).getValue().get();
					if (pvalue.getDouble() <= alpha)
						logger.debug(String.format("FP:\t%s\t%d\t%.3f\t%.3f\t%s", c.getName(), n, sim,
								pvalue.getDouble(), pvalue));

					final boolean isEnrichedCluster = enrichedClusters.contains(c);
					if (!isEnrichedCluster)
						nonEnrichedClusterPvalues.add(pvalue.getDouble());

					System.out.println(String.format("%s\t%s\t%d\t%d\t%d\t%s\t%d\t%d\t%d\t%b\t%.5f\t%s",
							shuffle.toString(), similarityFunction, dsSeed, numberObjects, numberTimepoints,
							Arrays.toString(numberObjectsEnrichedTimeSeries), totalNumberClusters, c.getClusterNumber(),
							c.size(), isEnrichedCluster, pvalue.getDouble(), pvalue));
				}
				long fp = nonEnrichedClusterPvalues.stream().filter(p -> p <= alpha).count();
				fp_total += fp;
				int count = nonEnrichedClusterPvalues.size();
				totalCount += count;
			} finally {
				if (randomNetwork != null)
					randomNetwork.clearThreadPool();
			}
		}

		// allow + 0.01
		double fpr = fp_total / (double) totalCount;

		assertTrue(fpr <= alpha + 0.01, String.format("FPR=%.3f was larger than alpha + 0.01=%.3f + 0.01", fpr, alpha));
	}

	private static Stream<Arguments> hiddenTimeSeriesClustersArgumentProvider()
			throws IncompatibleSimilarityFunctionException {
		final ISimilarityFunction[] timeSeriesSF = new ISimilarityFunction[] { new PearsonCorrelationFunction(),
				new InverseEuclideanSimilarityFunction() };

		final Object2ObjectMap<IShuffle, ISimilarityFunction[]> shuffleToSimilarityFunctions = new Object2ObjectOpenHashMap<>();

		ShuffleClusteringByShufflingDataset shuffle1 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new CreateRandomDataset());
		shuffle1.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle1, timeSeriesSF);
		ShuffleClusteringByShufflingDataset shuffle2 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new ShuffleDatasetRowwise());
		shuffle2.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle2, timeSeriesSF);
		ShuffleClusteringByShufflingDataset shuffle3 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new ShuffleDatasetGlobally());
		shuffle3.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle3, timeSeriesSF);

		final int[] numbersTimepoints = new int[] { 5, 30 };

		final int[] numbersObjects = new int[] { 250 };

		final Int2ObjectMap<int[][]> numbersObjectsEnrichedTimeSeries = new Int2ObjectArrayMap<>();
		numbersObjectsEnrichedTimeSeries.put(250, new int[][] { new int[] { 10, 10, 10 }, new int[] { 25, 25, 25 },
				new int[] { 10, 10, 10, 10, 10 }, new int[] { 25, 25, 25, 25, 25 } });

		final Int2ObjectMap<int[]> numbersClusters = new Int2ObjectArrayMap<>();
		numbersClusters.put(250, new int[] { 10, 30 });

		final List<Arguments> list = new ArrayList<>();
		for (IShuffle shuffle : shuffleToSimilarityFunctions.keySet()) {
			for (ISimilarityFunction similarityFunction : shuffleToSimilarityFunctions.get(shuffle)) {
				for (int numberObjects : numbersObjects) {
					for (int numberTimepoints : numbersTimepoints) {
						for (int[] numberObjectsEnrichedTimeSeries : numbersObjectsEnrichedTimeSeries
								.get(numberObjects)) {
							for (int numberClusters : numbersClusters.get(numberObjects)) {
								list.add(Arguments.of(shuffle, similarityFunction, numberObjects, numberTimepoints,
										numberObjectsEnrichedTimeSeries, numberClusters));
							}
						}
					}
				}
			}
		}
		return list.stream();
	}

	private static Stream<Arguments> randomNetworkClustersArgumentProvider()
			throws IncompatibleSimilarityFunctionException {
		final Object2ObjectMap<IShuffle, ISimilarityFunction[]> shuffleToSimilarityFunctions = new Object2ObjectOpenHashMap<>();

		ShuffleClusteringByShufflingNetwork shuffle1 = new ShuffleClusteringByShufflingNetwork(clusteringMethodBuilder,
				new ShuffleNetworkWithEdgeCrossover(false));
		shuffle1.setRandomNumberClusters(true);
		ShuffleClusteringByShufflingNetwork shuffle2 = new ShuffleClusteringByShufflingNetwork(clusteringMethodBuilder,
				new CreateRandomNetwork(false));
		shuffle2.setRandomNumberClusters(true);
		InverseShortestPathSimilarityFunction simFunction = new InverseShortestPathSimilarityFunction();
		simFunction.setEnsureKnownObjects(true);
		shuffleToSimilarityFunctions.put(shuffle1, new ISimilarityFunction[] { simFunction });
		shuffleToSimilarityFunctions.put(shuffle2, new ISimilarityFunction[] { simFunction });

		final double[] backgroundEdgeProbabilities = new double[] { 0.001, 0.01 };

		final int[] numbersObjects = new int[] { 250 };

		final Int2ObjectMap<int[]> numbersClusters = new Int2ObjectArrayMap<>();
		numbersClusters.put(250, new int[] { 10, 30 });

		final List<Arguments> list = new ArrayList<>();
		for (IShuffle shuffle : shuffleToSimilarityFunctions.keySet()) {
			for (ISimilarityFunction similarityFunction : shuffleToSimilarityFunctions.get(shuffle)) {
				for (int numberObjects : numbersObjects) {
					for (double backgroundEdgeProbability : backgroundEdgeProbabilities) {
						for (int numberClusters : numbersClusters.get(numberObjects)) {
							list.add(Arguments.of(shuffle, similarityFunction, numberObjects, backgroundEdgeProbability,
									numberClusters));
						}
					}
				}
			}
		}
		return list.stream();
	}

	private static Stream<Arguments> hiddenCompositeClustersArgumentProvider()
			throws IncompatibleSimilarityFunctionException {
		final InverseShortestPathSimilarityFunction shortestPathSF = new InverseShortestPathSimilarityFunction();
		final PearsonCorrelationFunction pearsonSF = new PearsonCorrelationFunction();
		final InverseEuclideanSimilarityFunction euclideanSF = new InverseEuclideanSimilarityFunction();
		final ICompositeSimilarityFunction[] simFuncs = new ICompositeSimilarityFunction[] {
				new WeightedAverageCompositeSimilarityFunction(pearsonSF, shortestPathSF),
				new WeightedAverageCompositeSimilarityFunction(euclideanSF, shortestPathSF) };

		final Object2ObjectMap<IShuffle, ICompositeSimilarityFunction[]> shuffleToSimilarityFunctions = new Object2ObjectOpenHashMap<>();

		ShuffleClusteringByShufflingDataset shuffle1 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new CreateRandomDataset());
		shuffle1.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle1, simFuncs);
		ShuffleClusteringByShufflingDataset shuffle2 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new ShuffleDatasetRowwise());
		shuffle2.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle2, simFuncs);
		ShuffleClusteringByShufflingDataset shuffle3 = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new ShuffleDatasetGlobally());
		shuffle3.setRandomNumberClusters(true);
		shuffleToSimilarityFunctions.put(shuffle3, simFuncs);

		final int[] numbersTimepoints = new int[] { /* 5, */ 30 };

		final int[] numbersObjects = new int[] { 250 };

		final Int2ObjectMap<int[][]> numbersObjectsEnrichedTimeSeries = new Int2ObjectArrayMap<>();
		numbersObjectsEnrichedTimeSeries.put(250, new int[][] { new int[] { 10, 10, 10 }, new int[] { 25, 25, 25 },
				new int[] { 10, 10, 10, 10, 10 }, new int[] { 25, 25, 25, 25, 25 } });

		final Int2ObjectMap<int[]> numbersClusters = new Int2ObjectArrayMap<>();
		numbersClusters.put(250, new int[] { 10, 30 });

		final List<Arguments> list = new ArrayList<>();
		for (IShuffle shuffle : shuffleToSimilarityFunctions.keySet()) {
			for (ICompositeSimilarityFunction similarityFunction : shuffleToSimilarityFunctions.get(shuffle)) {
				for (int numberObjects : numbersObjects) {
					for (int numberTimepoints : numbersTimepoints) {
						for (int[] numberObjectsEnrichedTimeSeries : numbersObjectsEnrichedTimeSeries
								.get(numberObjects)) {
							for (int numberClusters : numbersClusters.get(numberObjects)) {
								list.add(Arguments.of(shuffle, similarityFunction, numberObjects, numberTimepoints,
										numberObjectsEnrichedTimeSeries, numberClusters));
							}
						}
					}
				}
			}
		}
		return list.stream();
	}

	private static Stream<Arguments> hiddenNetworkClustersArgumentProvider()
			throws IncompatibleSimilarityFunctionException {
		final Object2ObjectMap<IShuffle, ISimilarityFunction[]> shuffleToSimilarityFunctions = new Object2ObjectOpenHashMap<>();

		ShuffleClusteringByShufflingNetwork shuffle1 = new ShuffleClusteringByShufflingNetwork(clusteringMethodBuilder,
				new ShuffleNetworkWithEdgeCrossover(false));
		shuffle1.setRandomNumberClusters(true);
		ShuffleClusteringByShufflingNetwork shuffle2 = new ShuffleClusteringByShufflingNetwork(clusteringMethodBuilder,
				new CreateRandomNetwork(false));
		shuffle2.setRandomNumberClusters(true);
		InverseShortestPathSimilarityFunction simFunction = new InverseShortestPathSimilarityFunction();
		simFunction.setEnsureKnownObjects(true);
		shuffleToSimilarityFunctions.put(shuffle1, new ISimilarityFunction[] { simFunction });
		shuffleToSimilarityFunctions.put(shuffle2, new ISimilarityFunction[] { simFunction });

		final int[] numbersObjects = new int[] { 250 };

		final Int2ObjectMap<int[][]> numbersObjectsEnrichedTimeSeries = new Int2ObjectArrayMap<>();
		numbersObjectsEnrichedTimeSeries.put(250, new int[][] { new int[] { 10, 10, 10 }, new int[] { 25, 25, 25 },
				new int[] { 10, 10, 10, 10, 10 }, new int[] { 25, 25, 25, 25, 25 } });

		final Int2ObjectMap<int[]> numbersClusters = new Int2ObjectArrayMap<>();
		numbersClusters.put(250, new int[] { 10, 30 });

		final List<Arguments> list = new ArrayList<>();
		for (IShuffle shuffle : shuffleToSimilarityFunctions.keySet()) {
			for (ISimilarityFunction similarityFunction : shuffleToSimilarityFunctions.get(shuffle)) {
				for (int numberObjects : numbersObjects) {
					for (int[] numberObjectsEnrichedTimeSeries : numbersObjectsEnrichedTimeSeries.get(numberObjects)) {
						for (int numberClusters : numbersClusters.get(numberObjects)) {
							list.add(Arguments.of(shuffle, similarityFunction, numberObjects,
									numberObjectsEnrichedTimeSeries, numberClusters));
						}
					}
				}
			}
		}
		return list.stream();
	}

}
