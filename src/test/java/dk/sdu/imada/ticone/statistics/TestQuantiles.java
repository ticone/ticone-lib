/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.feature.AbstractFeature;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.FeatureValue;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.INumericFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
public class TestQuantiles {

	class DummyFeature extends AbstractFeature<Double> implements INumericFeature<Double> {

		/**
		 * @param objectType
		 * @param valueType
		 */
		public DummyFeature(ObjectType<? extends IObjectWithFeatures> objectType) {
			super(objectType, Double.class);
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -4021654257577248100L;

		@Override
		public String getName() {
			return null;
		}

		@Override
		public IFeature<Double> copy() {
			return null;
		}

		@Override
		protected IFeatureValue<Double> doCalculate(IObjectWithFeatures object)
				throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
			return null;
		}

	}

	class DummyFeatureValue extends FeatureValue<Double> {

		/**
		 * @param featureType
		 * @param valueType
		 * @param value
		 */
		protected DummyFeatureValue(Double value) {
			super(DummyFeature.class, Double.class, value);
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -2974731750497908778L;

	}

	@Test
	public void testOneValue() throws Exception {
		List<DummyFeatureValue> values = Arrays.asList(0.5).stream().map(d -> new DummyFeatureValue(d))
				.collect(Collectors.toList());
		double[] quantiles = new double[] { 0.0, 0.1, 0.5, 1.0 };

		double[] expected = new double[] { 0.5, 0.5, 0.5, 0.5 };
		double[] result = Quantiles.calculate(values, quantiles);
		assertArrayEquals(expected, result, 0.00001);
	}

	@Test
	public void testQuantiles() throws Exception {
		List<DummyFeatureValue> values = Arrays.asList(0.7, 0.3, 0.6, 0.1, 0.9, 0.2, 0.2, 0.7, 0.3, 0.0, 1.0, 0.9)
				.stream().map(d -> new DummyFeatureValue(d)).collect(Collectors.toList());

		double[] quantiles = new double[] { 0.0, 0.1, 0.5, 1.0 };

		// taken from R as reference
		double[] expected = new double[] { 0.0, 0.1, 0.45, 1.0 };
		double[] result = Quantiles.calculate(values, quantiles);
		assertArrayEquals(expected, result, 0.00001);
	}

	@Test
	public void testInvalidQuantiles() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			List<DummyFeatureValue> values = Arrays.asList(0.7, 0.3, 0.6, 0.1, 0.9, 0.2, 0.2, 0.7, 0.3, 0.0, 1.0, 0.9)
					.stream().map(d -> new DummyFeatureValue(d)).collect(Collectors.toList());
			double[] quantiles = new double[] { 0.0, 0.1, -0.1, 0.5, 1.0 };

			Quantiles.calculate(values, quantiles);
		});
	}

	@Test
	public void testInvalidQuantiles2() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			List<DummyFeatureValue> values = Arrays.asList(0.7, 0.3, 0.6, 0.1, 0.9, 0.2, 0.2, 0.7, 0.3, 0.0, 1.0, 0.9)
					.stream().map(d -> new DummyFeatureValue(d)).collect(Collectors.toList());
			double[] quantiles = new double[] { 0.0, 0.1, 1.1, 0.5, 1.0 };

			Quantiles.calculate(values, quantiles);
		});
	}

}
