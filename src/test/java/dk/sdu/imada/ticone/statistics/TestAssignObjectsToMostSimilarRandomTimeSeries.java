/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.Random;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.TestDataUtility;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 12, 2019
 *
 */
class TestAssignObjectsToMostSimilarRandomTimeSeries {

//	@Test
	void test() throws Exception {
		Random random = new Random(42);
		int numberClusters = 10;
		final PrototypeBuilder prototypeFactory = new PrototypeBuilder();
		// TODO
		prototypeFactory.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder());
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		similarityFunction.initialize();
		ITiconeNetwork<ITiconeNetworkNode, ?> network = null;
		AssignObjectsToMostSimilarRandomCluster randomF = new AssignObjectsToMostSimilarRandomCluster(42l,
				prototypeFactory, numberClusters, similarityFunction, network);
		ClusterObjectMapping randomClustering = randomF.generateFrom(TestDataUtility.randomObjects(random, 10));
		randomClustering.getClusters().forEach(c -> {
			try {
				System.out.println(c.getPrototype().getPrototypeComponent(PrototypeComponentType.TIME_SERIES));
				System.out.println(c.getObjects());
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				e.printStackTrace();
			}
		});
		;
	}

}
