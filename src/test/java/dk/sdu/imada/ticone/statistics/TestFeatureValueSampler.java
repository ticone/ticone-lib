/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.IClusterPairFeatureShortestPath;
import dk.sdu.imada.ticone.feature.IObjectPairFeature;
import dk.sdu.imada.ticone.feature.ObjectPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.ObjectPairSimilarityFeature;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.InverseShortestPathSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.statistics.IFeatureValueSample.Entry;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 8, 2018
 *
 */
public class TestFeatureValueSampler {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries());
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testCorrectSampleSize() throws Exception {
		ISimilarityFunction simFunc = new PearsonCorrelationFunction();
		FeatureValueSampler s = new FeatureValueSampler();

		ITimeSeriesObjects dataSet = new TimeSeriesObjectList();

		TimeSeriesObject o1 = new TimeSeriesObject("o1", 1);
		o1.addPreprocessedTimeSeries(0, TimeSeries.of(1, 2, 3, 4, 5));
		TimeSeriesObject o2 = new TimeSeriesObject("o2", 1);
		o2.addPreprocessedTimeSeries(0, TimeSeries.of(1, 2, 3, 4, 5));
		TimeSeriesObject o3 = new TimeSeriesObject("o3", 1);
		o3.addPreprocessedTimeSeries(0, TimeSeries.of(5, 4, 3, 2, 1));

		dataSet.add(o1);
		dataSet.add(o2);
		dataSet.add(o3);

		IObjectPairFeature<ISimilarityValue> f = new ObjectPairSimilarityFeature(simFunc);

		IFeatureValueSample<ISimilarityValue> sampleFrom = s.sampleFrom(f, dataSet, 100, 42l);
		assertEquals(100, sampleFrom.size());
	}

	@Test
	public void testClusterPairNetworkDistance() throws Exception {
		FeatureValueSampler s = new FeatureValueSampler();

		ITimeSeriesObjects dataSet = new TimeSeriesObjectList();

		TimeSeriesObject o1 = new TimeSeriesObject("o1", 1);
		o1.addPreprocessedTimeSeries(0, TimeSeries.of(1, 2, 3, 4, 5));
		TimeSeriesObject o2 = new TimeSeriesObject("o2", 1);
		o2.addPreprocessedTimeSeries(0, TimeSeries.of(1, 2, 3, 4, 5));
		TimeSeriesObject o3 = new TimeSeriesObject("o3", 1);
		o3.addPreprocessedTimeSeries(0, TimeSeries.of(5, 4, 3, 2, 1));

		dataSet.add(o1);
		dataSet.add(o2);
		dataSet.add(o3);

		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("o1");
		network.addNode("o2");
		network.addNode("o3");
		network.addEdge("o1", "o2", false);
		network.addEdge("o2", "o3", false);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		f.addPrototypeComponentFactory(nlf);
		InverseShortestPathSimilarityFunction negativeShortestPathSimilarityFunction = new InverseShortestPathSimilarityFunction(
				network);
		negativeShortestPathSimilarityFunction.initialize();
		nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, negativeShortestPathSimilarityFunction));

		ClusterObjectMapping clustering = new ClusterObjectMapping(f);
		f.setObjects(new TimeSeriesObjectList(o1, o2));
		IPrototype prototype = f.build();
		System.out.println(prototype);
		ICluster c1 = clustering.getClusterBuilder().setPrototype(prototype).build();
		clustering.addMapping(o1, c1, AbstractSimilarityValue.MAX);
		clustering.addMapping(o2, c1, AbstractSimilarityValue.MAX);
		f.setObjects(new TimeSeriesObjectList(o3));
		prototype = f.build();
		System.out.println(prototype);
		ICluster c2 = clustering.getClusterBuilder().setPrototype(prototype).build();
		clustering.addMapping(o3, c2, AbstractSimilarityValue.MAX);

		IClusterPairFeatureShortestPath feature = new ClusterPairFeatureShortestDistance(false);
		feature.setFeatureValueProvider(network);

		IFeatureValueSample<Double> sampleFrom = s.sampleFrom(feature, clustering, 100, 42l);
		assertEquals(100, sampleFrom.size());

		assertEquals("o1", PrototypeComponentType.NETWORK_LOCATION.getComponent(c1.getPrototype()).getNetworkLocation()
				.iterator().next().getName());
		assertEquals("o3", PrototypeComponentType.NETWORK_LOCATION.getComponent(c2.getPrototype()).getNetworkLocation()
				.iterator().next().getName());

		for (Entry<Double> p : sampleFrom) {
			if (p.getObject().equals(new ClusterPair(c1, c1))) {
				assertEquals(Double.NEGATIVE_INFINITY, p.getValue().getValue().doubleValue(), 0.0000001);
			} else if (p.getObject().equals(new ClusterPair(c1, c2))) {
				assertEquals(2.0, p.getValue().getValue().doubleValue(), 0.0000001);
			} else if (p.getObject().equals(new ClusterPair(c2, c2))) {
				assertEquals(Double.NEGATIVE_INFINITY, p.getValue().getValue().doubleValue(), 0.0000001);
			}
		}
	}

	@Test
	public void testObjectPairNetworkDistance() throws Exception {
		FeatureValueSampler s = new FeatureValueSampler();

		ITimeSeriesObjects dataSet = new TimeSeriesObjectList();

		TimeSeriesObject o1 = new TimeSeriesObject("o1", 1);
		o1.addPreprocessedTimeSeries(0, TimeSeries.of(1, 2, 3, 4, 5));
		TimeSeriesObject o2 = new TimeSeriesObject("o2", 1);
		o2.addPreprocessedTimeSeries(0, TimeSeries.of(1, 2, 3, 4, 5));
		TimeSeriesObject o3 = new TimeSeriesObject("o3", 1);
		o3.addPreprocessedTimeSeries(0, TimeSeries.of(5, 4, 3, 2, 1));

		dataSet.add(o1);
		dataSet.add(o2);
		dataSet.add(o3);

		ClusterObjectMapping clustering = new ClusterObjectMapping(f);
		tsf.setTimeSeries(TimeSeries.of(1, 2, 3, 4, 5));
		ICluster c1 = clustering.getClusterBuilder().setPrototype(f.build()).build();
		clustering.addMapping(o1, c1, AbstractSimilarityValue.MAX);
		clustering.addMapping(o2, c1, AbstractSimilarityValue.MAX);
		tsf.setTimeSeries(TimeSeries.of(5, 4, 3, 2, 1));
		ICluster c2 = clustering.getClusterBuilder().setPrototype(f.build()).build();
		clustering.addMapping(o3, c2, AbstractSimilarityValue.MAX);

		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("o1");
		network.addNode("o2");
		network.addNode("o3");
		network.addEdge("o1", "o2", false);
		network.addEdge("o2", "o3", false);

		ObjectPairFeatureShortestDistance f = new ObjectPairFeatureShortestDistance(false);
		f.setFeatureValueProvider(network);

		network.initializeForFeature(f);

		IFeatureValueSample<Double> sampleFrom = s.sampleFrom(f, dataSet, 100, 42l);
		assertEquals(100, sampleFrom.size());
		System.out.println(sampleFrom);

		for (Entry<Double> p : sampleFrom) {
			if (p.getObject().equals(new ObjectPair(o1, o1)) || p.getObject().equals(new ObjectPair(o2, o2))
					|| p.getObject().equals(new ObjectPair(o3, o3))) {
				assertEquals(Double.NEGATIVE_INFINITY, p.getValue().getValue().doubleValue(), 0.0000001);
			} else if (p.getObject().equals(new ObjectPair(o1, o2)) || p.getObject().equals(new ObjectPair(o2, o1))) {
				assertEquals(1.0, p.getValue().getValue().doubleValue(), 0.0000001);
			} else if (p.getObject().equals(new ObjectPair(o1, o3)) || p.getObject().equals(new ObjectPair(o3, o1))) {
				assertEquals(2.0, p.getValue().getValue().doubleValue(), 0.0000001);
			} else if (p.getObject().equals(new ObjectPair(o2, o3)) || p.getObject().equals(new ObjectPair(o3, o2))) {
				assertEquals(1.0, p.getValue().getValue().doubleValue(), 0.0000001);
			}
		}
	}

}
