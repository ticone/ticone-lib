package dk.sdu.imada.ticone.variance;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.preprocessing.PreprocessingException;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;

public class TestStandardVariance {

	@Test
	public void test() throws PreprocessingException, InterruptedException {
		ITimeSeriesObject tsData = new TimeSeriesObject("bla", 1, new TimeSeries());

		IObjectSetVariance var = new StandardVariance();

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(tsData);
		av.process();

		Assert.assertEquals(Double.NaN, var.calculateObjectSetVariance(tsData), 0.0);
	}

	@Test
	public void test2() throws Exception {
		ITimeSeriesObject tsData = new TimeSeriesObject("bla", 1);
		tsData.addOriginalTimeSeriesToList(0, TimeSeries.of(1, 2, 3), "s1");

		IObjectSetVariance var = new StandardVariance();

		Assert.assertEquals(0.81649, var.calculateObjectSetVariance(tsData), 0.00001);
	}

	@Test
	public void test3() throws Exception {
		ITimeSeriesObject tsData = new TimeSeriesObject("bla", 1);
		tsData.addOriginalTimeSeriesToList(0, TimeSeries.of(1, 2, 3), "s1");
		tsData.addOriginalTimeSeriesToList(0, TimeSeries.of(1, 2, 4), "s2");

		IObjectSetVariance var = new StandardVariance();

		Assert.assertEquals(1.247219, var.calculateObjectSetVariance(tsData), 0.00001);
	}

}
