package dk.sdu.imada.ticone.connectivity;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.rules.ErrorCollector;

import de.davidm.textplots.Plot;
import de.davidm.textplots.Scatterplot;
import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.BasicClusteringProcessBuilder;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethod;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringRandomlyWithSameNumberClusters;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringWithRandomPrototypeTimeSeries;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResultFactory;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.mergeclusters.MergeSelectedClusters;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.pair.ShuffleFirstClusteringOfClusteringPair;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ShuffleDatasetGlobally;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberDirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberUndirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IClusterPairFeatureNumberDirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.io.LoadDataFromFile;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.network.ShuffleNetworkWithEdgeCrossover;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImplFactory;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimpleSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.statistics.IPvalue;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;
import it.unimi.dsi.fastutil.ints.IntList;
import junitx.framework.ArrayAssert;

public class TestTiconeClusterConnectivityTask {

	PearsonCorrelationFunction similarityFunction;

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;
	private TiconeClusteringResult clusteringResult;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries());
		f.setPrototypeComponentBuilders(tsf);
		similarityFunction = new PearsonCorrelationFunction();
		clusteringResult = new TiconeClusteringResult(42, null, null, 0, null, null, null, null, null, null, f);

	}

	@Rule
	public ErrorCollector collector = new ErrorCollector();

	@Test
	public void testDirectedConnectivity() throws Exception {
		new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0, 1, 4, 4);
		new AbsoluteValues();
		new AggregateClusterMeanTimeSeries();
		new ShuffleDatasetGlobally();

		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(true);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();
		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ticoneConnectivityTask.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.calculatePValues(network, true);

	}

	@Test
	@Tag("long-running")
	public void testEnrichmentVSPvalues2() throws Exception {
		TiconeNetworkImpl network = TiconeNetworkImpl.parseFromFile("blubb", true, new File(
				"data/generated/difficultDataset-hardcodedPatterns-objects40-seed42-bg240-network0.01-samePatternEdgeProb0.2.txt"));

		// System.out.println(network.getEdgeCount());

		// ClusterObjectMapping pom = IClusterObjectMapping.parseFromFile(new
		// File(
		// "data/generated/difficultDataset-hardcodedPatterns-objects40-seed42-bg240-object-to-pattern.txt"));
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		int numberClusters = 15;
		for (int i = 0; i < numberClusters; i++) {
			tsf.setTimeSeries(new double[0]);
			pom.getClusterBuilder().setPrototype(f.build()).setClusterNumber(i).setInternalClusterNumber((long) i)
					.setIsKeep(true).build();
		}
		IClusterList clusters = new ClusterList(pom.getClusters());
		Random r = new Random(42);
		for (TiconeNetworkNodeImpl n : network.getNodeSet()) {
			pom.addMapping(new TimeSeriesObject(n.getName(), 1, TimeSeries.of(new double[0])),
					clusters.get(r.nextInt(numberClusters)), TestSimpleSimilarityValue.of(1.0));
		}
		int numberPermutations = 1000;
		boolean isTwoSidedPvalue = true;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, isTwoSidedPvalue,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ticoneConnectivityTask.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);

		ticoneConnectivityTask.calculatePValues(network, true);

		// TODO: what should this test?
		// scatterPlotEnrichmentVsPvalues("Undirected", "Enrichment", "p",
		// undirectedConnectivityResult.edgeCountEnrichment,
		// edgeCrossoverConnectivityResult.pvaluesUndirected);
		// scatterPlotEnrichmentVsPvalues("Directed", "Enrichment", "p",
		// directedConnectivityResult.edgeCountEnrichment,
		// edgeCrossoverConnectivityResult.pvaluesDirected);
	}

	protected void scatterPlotEnrichmentVsPvalues(final String title, final String xlab, final String ylab,
			final Map<IClusterPair, Double> r, final Map<IClusterPair, IPvalue> e) {
		double[] newEnrichments = new double[r.size()];
		double[] newPvalues = new double[r.size()];

		int i = 0;
		for (IClusterPair clusterPair : r.keySet()) {
			double newEnr = r.get(clusterPair);
			double pvalNew = e.get(clusterPair).getDouble();

			// System.out.print(newEnr + "\t");
			// System.out.println(pvalNew);

			if (!Double.isFinite(newEnr))
				continue;
			newEnrichments[i] = newEnr;
			newPvalues[i] = pvalNew;
			i++;
		}

		Plot plot;
		System.out.println("SCATTER: " + title);
		plot = new Scatterplot.ScatterplotBuilder(org.apache.commons.math3.util.Pair.create(xlab, newEnrichments),
				org.apache.commons.math3.util.Pair.create(ylab, newPvalues)).setSize(25, 10).plotObject();
		plot.printPlot(true);
	}

	// @Test
	// public void testConstantEdgeCountEdgeCrossoverDirected() throws
	// IOException, ClusterConnectivityException {
	// TiCoNENetworkImpl network = TiCoNENetworkImpl.parseFromFile("blubb",
	// true, new File(
	// "data/generated/difficultDataset-hardcodedPatterns-objects40-seed42-bg240-network0.01-samePatternEdgeProb0.2.txt"));
	//
	// ClusterObjectMapping pom = new ClusterObjectMapping(f);
	// int numberClusters = 15;
	// for (int i = 0; i < numberClusters; i++)
	// pom.addCluster(new Cluster(new double[0], i, i, true));
	// IClusters clusters = new Clusters(pom.clusterSet());
	// Random r = new Random(42);
	// for (TiCoNENetworkNodeImpl n : network.getNodeSet()) {
	// pom.addMapping(new TimeSeriesObject(n.getName(), 1, new double[0]),
	// clusters.get(r.nextInt(numberClusters)),
	// 1.0);
	// }
	// int numberPermutations = 1000;
	// boolean isTwoSidedPvalue = true;
	// TiCoNEClusterConnectivityTask ticoneConnectivityTask = new
	// TiCoNEClusterConnectivityTask(network,
	// clusteringResult, pom, clusters, numberPermutations, isTwoSidedPvalue,
	// 16.12,
	// new ShuffleClusteringPair(new CreateRandomClustering()));
	//
	// network.initNodeDegreesUndirected();
	// ticoneConnectivityTask.initEntrezToNetworks();
	//
	// ticoneConnectivityTask.removeObjectsNotInNetwork(network);
	// ticoneConnectivityTask.removeNodesNotInDataset(network);
	//
	// final ConnectivityResult directedConnectivityResult =
	// ticoneConnectivityTask
	// .getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network),
	// undirectedConnectivityResult = ticoneConnectivityTask
	// .getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
	//
	// EdgeCrossoverConnectivityResult edgeCrossoverConnectivityResult =
	// ticoneConnectivityTask
	// .edgeCrossoverLogOddsPValues(network, true);
	//
	// Map<Pair<ICluster, ICluster>, List<Integer>>
	// permutationEdgeCountsDirected = ticoneConnectivityTask
	// .getPermutationEdgeCountsDirected();
	// for (int p = 0; p < numberPermutations; p++) {
	// long sum = 0;
	// for (Pair<ICluster, ICluster> k : permutationEdgeCountsDirected.keySet())
	// sum += permutationEdgeCountsDirected.get(k).get(p);
	// assertEquals(network.getEdgeCount(), sum);
	// }
	// }

	@Test
	@Tag("long-running")
	public void testConstantEdgeCountEdgeCrossoverDirected2() throws Exception {
		TiconeNetworkImpl network = TiconeNetworkImpl.parseFromFile("blubb", true, new File(
				"data/generated/difficultDataset-hardcodedPatterns-objects40-seed42-bg240-network0.01-samePatternEdgeProb0.2.txt"));

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		int numberClusters = 60;
		for (int i = 0; i < numberClusters; i++) {
			tsf.setTimeSeries(new double[0]);
			pom.getClusterBuilder().setPrototype(f.build()).setClusterNumber(i).setInternalClusterNumber((long) i)
					.setIsKeep(true).build();
		}
		IClusterList clusters = new ClusterList(pom.getClusters());
		Random r = new Random(42);
		for (TiconeNetworkNodeImpl n : network.getNodeSet()) {
			pom.addMapping(new TimeSeriesObject(n.getName(), 1, TimeSeries.of(new double[0])),
					clusters.get(r.nextInt(numberClusters)), TestSimpleSimilarityValue.of(1.0));
		}
		int numberPermutations = 100;
		boolean isTwoSidedPvalue = true;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, isTwoSidedPvalue,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ticoneConnectivityTask.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);

		ConnectivityPValueResult calculatePValues = ticoneConnectivityTask.calculatePValues(network, true);

		Map<IClusterPair, IntList> permutationEdgeCountsDirected = calculatePValues.permutationEdgeCountsDirected;
		for (int p = 0; p < numberPermutations; p++) {
			long sum = 0;
			for (IClusterPair k : permutationEdgeCountsDirected.keySet())
				sum += permutationEdgeCountsDirected.get(k).get(p);
			assertEquals(network.getEdgeCount(), sum);
		}
	}

	// @Test
	// public void testConstantEdgeCountEdgeCrossoverUndirected() throws
	// IOException, ClusterConnectivityException {
	// TiCoNENetworkImpl network = TiCoNENetworkImpl.parseFromFile("blubb",
	// true, new File(
	// "data/generated/difficultDataset-hardcodedPatterns-objects40-seed42-bg240-network0.01-samePatternEdgeProb0.2.txt"));
	// network.removeDuplicateEdges();
	// ClusterObjectMapping pom = new ClusterObjectMapping(f);
	// int numberClusters = 15;
	// for (int i = 0; i < numberClusters; i++)
	// pom.addCluster(new Cluster(new double[0], i, i, true));
	// IClusters clusters = new Clusters(pom.clusterSet());
	// Random r = new Random(42);
	// for (TiCoNENetworkNodeImpl n : network.getNodeSet()) {
	// pom.addMapping(new TimeSeriesObject(n.getName(), 1, new double[0]),
	// clusters.get(r.nextInt(numberClusters)),
	// 1.0);
	// }
	// int numberPermutations = 100;
	// boolean isTwoSidedPvalue = true;
	// TiCoNEClusterConnectivityTask ticoneConnectivityTask = new
	// TiCoNEClusterConnectivityTask(network,
	// clusteringResult, pom, clusters, numberPermutations, isTwoSidedPvalue,
	// 16.12,
	// new ShuffleClusteringPair(new CreateRandomClustering()));
	//
	// network.initNodeDegreesUndirected();
	// ticoneConnectivityTask.initEntrezToNetworks();
	//
	// ticoneConnectivityTask.removeObjectsNotInNetwork(network);
	// ticoneConnectivityTask.removeNodesNotInDataset(network);
	//
	// final ConnectivityResult directedConnectivityResult =
	// ticoneConnectivityTask
	// .getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network),
	// undirectedConnectivityResult = ticoneConnectivityTask
	// .getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
	//
	// EdgeCrossoverConnectivityResult edgeCrossoverConnectivityResult =
	// ticoneConnectivityTask
	// .edgeCrossoverLogOddsPValues(network, true);
	//
	// Map<Pair<ICluster, ICluster>, List<Integer>>
	// permutationEdgeCountsUndirected = ticoneConnectivityTask
	// .getPermutationEdgeCountsUndirected();
	// for (int p = 0; p < numberPermutations; p++) {
	// long sum = 0;
	// for (Pair<ICluster, ICluster> k :
	// permutationEdgeCountsUndirected.keySet())
	// if (k.getLeft().getClusterNumber() <= k.getRight().getClusterNumber())
	// sum += permutationEdgeCountsUndirected.get(k).get(p);
	// assertEquals(network.getEdgeCount(), sum);
	// }
	// }

	@Test
	public void testMedian() {
		double[] bla = new double[] { 1, 2, 3 };
		assertEquals(2.0, median(bla), 0.0001);

		bla = new double[] { 1, 2, 2, 3 };
		assertEquals(2.0, median(bla), 0.0001);

		bla = new double[] { 1, 2, 3, 3 };
		assertEquals(2.5, median(bla), 0.0001);

		bla = new double[] { 3, 2, 3, 1 };
		assertEquals(2.5, median(bla), 0.0001);
	}

	public double median(double[] arr) {
		double[] arrCopy = java.util.Arrays.copyOf(arr, arr.length);
		java.util.Arrays.sort(arrCopy);
		return arrCopy.length % 2 == 1 ? arrCopy[arrCopy.length / 2]
				: (arrCopy[arrCopy.length / 2 - 1] + arrCopy[arrCopy.length / 2]) / 2.0;
	}

	@Test
	public void testDirectedConnectivitySumEnrichmentScores() throws Exception {
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(true);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();

		int numberPermutations = 0;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult directedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);

		for (IClusterPair cp : directedConnectivityResult.edgeCountEnrichment.keySet()) {
			Math.pow(10, directedConnectivityResult.edgeCountEnrichment.get(cp).doubleValue());
		}
	}

	@Test
	public void testTotalExpectedEdgesSummingUpToPresentEdgesDirectedConnectivity() throws Exception {
		new PearsonCorrelationFunction();
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(true);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();
		int numberPermutations = 1000;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult directedConnectivityResult = null;

		directedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.calculatePValues(network);

		int edgeCount = network.getEdgeCount();

		double sumExpectedEdgeCounts = 0.0;
		for (IClusterPair cp : directedConnectivityResult.expectedEdgeCount.keySet()) {
			sumExpectedEdgeCounts += directedConnectivityResult.expectedEdgeCount.get(cp);
		}

		assertEquals(edgeCount, sumExpectedEdgeCounts, 0.1);
	}

	@Test
	public void testUndirectedConnectivity() throws Exception {
		new PearsonCorrelationFunction();

		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();
		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult directedConnectivityResult = null, undirectedConnectivityResult = null;
		ConnectivityPValueResult edgeCrossoverConnectivityResult = null;

		undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		directedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network);

		new ConnectivityResultWrapper(clusteringResult, directedConnectivityResult, undirectedConnectivityResult,
				edgeCrossoverConnectivityResult, 1);

	}

	@Test
	public void testUndirectedEdgesSmallerDirectedEdges() throws Exception {
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();
		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult directedConnectivityResult = null, undirectedConnectivityResult = null;

		undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		directedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);

		for (IClusterPair clusterPair : undirectedConnectivityResult.getEdgeCount().keySet()) {
			int undirected = undirectedConnectivityResult.getEdgeCount().get(clusterPair);
			int directed1 = directedConnectivityResult.getEdgeCount().get(clusterPair);
			int directed2 = directedConnectivityResult.getEdgeCount()
					.get(new ClusterPair(clusterPair.getSecond(), clusterPair.getFirst()));

			// assertTrue("undirected edge count is larger than directed edge
			// count", undirected > directed1);
			// assertTrue("undirected edge count is larger than directed edge
			// count", undirected > directed2);

			assertTrue("undirected edge count is larger than summed directed edge count",
					undirected <= directed1 + directed2);
		}
	}

	public ClusterObjectMapping parseFromFile(final File file, final int objectIdColumnIdx,
			final int clusterIdColumnIdx, final ISimpleSimilarityFunction simFunc) throws Exception {
		Scanner s = new Scanner(file);
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		Map<String, ICluster> idToCluster = new HashMap<>();
		while (s.hasNextLine()) {
			String[] lineSplit = s.nextLine().split("\t");
			if (!idToCluster.containsKey(lineSplit[clusterIdColumnIdx])) {
				tsf.setTimeSeries(new double[0]);
				// try to parse a cluster number
				try {
					String idS = lineSplit[clusterIdColumnIdx];
					idS.replaceAll("Cluster", "");
					idS.replace(" ", "");
					int id = Integer.valueOf(lineSplit[clusterIdColumnIdx]);

					ICluster newC = pom.getClusterBuilder().setPrototype(f.build()).setClusterNumber(id).build();
					idToCluster.put(lineSplit[clusterIdColumnIdx], newC);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			pom.addMapping(new TimeSeriesObject(lineSplit[objectIdColumnIdx], 1, TimeSeries.of(new double[0])),
					idToCluster.get(lineSplit[clusterIdColumnIdx]), simFunc.value(1.0, null));
		}
		s.close();
		return pom;
	}

	protected Triple<ClusterObjectMapping, TiconeNetworkImpl, IClusterList> demoDataConstantEdgeProbabilityUndirected()
			throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		IClusterList demoClusters = new ClusterList();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, 1);
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 undirected edges within cluster
			String node1 = p.getObjects().get(0).getName();
			String node2 = p.getObjects().get(1).getName();
			network.addEdge(node1, node2, false);
			network.addEdge(node1, node1, false);
		}

		// create 3 undirected edges between each pair of clusters
		for (int c1 = 0; c1 < demoClusters.size(); c1++) {
			ICluster p1 = demoClusters.get(c1);
			for (int c2 = c1 + 1; c2 < demoClusters.size(); c2++) {
				ICluster p2 = demoClusters.get(c2);
				if (p1.equals(p2))
					continue;
				String node11 = p1.getObjects().get(0).getName();
				String node12 = p1.getObjects().get(1).getName();
				String node21 = p2.getObjects().get(0).getName();
				String node22 = p2.getObjects().get(1).getName();
				network.addEdge(node11, node21, false);
				network.addEdge(node12, node21, false);
				network.addEdge(node12, node22, false);
			}
		}
		// doesn't work for such small data sets
		// double edgeProbability = 0.5;
		//
		// // generate global edges with equal global probability
		// List<TiCoNENetworkNodeImpl> nodeList = network.getNodeList();
		// for (int i = 0; i < network.getNodeCount(); i++) {
		// for (int j = i; j < network.getNodeCount(); j++) {
		// if (r.nextDouble() <= edgeProbability) {
		// network.addEdge(nodeList.get(i), nodeList.get(j), false);
		// }
		// }
		// }
//		ClusterObjectMapping.resetClusterCount();

		return Triple.of(demoPom, network, (IClusterList) new ClusterList(demoClusters));
	}

	@Test
	public void testDirectedConnectivityNoIntraClusterEdges() throws Exception {
		new PearsonCorrelationFunction();

		Triple<ClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataNoIntraClusterEdges(true);

		TiconeNetworkImpl network = pair.getMiddle();
		ClusterObjectMapping pom = pair.getLeft();
		IClusterList clusters = pair.getRight();
		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult directedConnectivityResult = null;
		directedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.calculatePValues(network);
		for (ICluster c : clusters) {
			assertTrue(directedConnectivityResult.getEdgeCount().containsKey(new ClusterPair(c, c)));
			assertTrue(directedConnectivityResult.getEdgeCountEnrichment().containsKey(new ClusterPair(c, c)));
			assertTrue(directedConnectivityResult.getExpectedEdgeCount().containsKey(new ClusterPair(c, c)));
		}
	}

	@Test
	public void testUndirectedConnectivityNoIntraClusterEdges() throws Exception {
		new PearsonCorrelationFunction();

		Triple<ClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataNoIntraClusterEdges(true);

		TiconeNetworkImpl network = pair.getMiddle();
		ClusterObjectMapping pom = pair.getLeft();
		IClusterList clusters = pair.getRight();
		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult undirectedConnectivityResult = null;
		undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.calculatePValues(network);
		for (ICluster c : clusters) {
			assertTrue(undirectedConnectivityResult.getEdgeCount().containsKey(new ClusterPair(c, c)));
			assertTrue(undirectedConnectivityResult.getEdgeCountEnrichment().containsKey(new ClusterPair(c, c)));
			assertTrue(undirectedConnectivityResult.getExpectedEdgeCount().containsKey(new ClusterPair(c, c)));
		}
	}

	// we are not using k-nearest neighbor anymore for joined node degrees at
	// the moment; thus this would always fail;
	// TODO: should we reintroduce k-nn again? makes performance much slower.
	// @Test
	// public void
	// testUndirectedConnectivityNoIntraClusterEdgesExpectedEdgesLarger0()
	// throws FileNotFoundException, LoadDataException,
	// TooFewObjectsClusteringException {
	// ISimilarity similarityFunction = new PearsonCorrelation();
	// IDiscretizePrototype discretizePattern = new
	// DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0, 1, 4, 4);
	// AbstractTimeSeriesPreprocessor timeSeriesData = new AbsoluteValues();
	// IAggregateCluster refinePattern = new AggregateClusterMean();
	// IPermutateDataset permFunction = new PermutateDatasetGlobally();
	//
	// // parse time series data
	// LoadDataFromFile loadData = new LoadDataFromFile();
	// loadData.getColumnMapping().setObjectIdColumn(2, "objectId");
	// loadData.getColumnMapping().setReplicateColumn(1, "replicate");
	// loadData.getColumnMapping().setTimePointsColumns(new int[] { 3, 4, 5, 6,
	// 7 },
	// new String[] { "tp1", "tp2", "tp3", "tp4", "tp5", "tp6", "tp7" });
	// loadData.setImportFile(
	// new
	// File("/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5.txt"));
	// loadData.loadData(clusteringResult);
	//
	// ITimeSeriesObjectSet objects =
	// clusteringResult.getAbstractTimeSeriesPreprocessor().getTimeSeriesDatas();
	//
	// // parse network
	// Scanner scanner = new Scanner(new File(
	// "/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5-seed42-samePatternEdgeProb0.0.txt"));
	// TiCoNENetworkImpl network = NetworkUtil.parseNetworkFromScanner(new
	// TiCoNENetworkImplFactory(), scanner, "bla",
	// false);
	// System.out.println(network);
	//
	// // perform clustering into 3 clusters
	// Random random = new Random(42);
	// IClustering initialClusteringInterface = new
	// CLARAClustering(similarityFunction, refinePattern,
	// discretizePattern, random);
	// ClusterObjectMapping pom =
	// initialClusteringInterface.findClusters(objects, 3);
	//
	// IClusters clusters = new Clusters(pom.clusterSet());
	// int numberPermutations = 100;
	// TiCoNEClusterConnectivityTask ticoneConnectivityTask = new
	// TiCoNEClusterConnectivityTask(network,
	// clusteringResult, pom, clusters, numberPermutations, false
	//
	// network.initNodeDegreesUndirected();
	// ticoneConnectivityTask.initEntrezToNetworks();
	//
	// ticoneConnectivityTask.removeObjectsNotInNetwork(network);
	// ticoneConnectivityTask.removeNodesNotInDataset(network);
	//
	// ConnectivityResult directedConnectivityResult = null,
	// undirectedConnectivityResult = null;
	// EdgeCrossoverConnectivityResult edgeCrossoverConnectivityResult = null;
	//
	// undirectedConnectivityResult = ticoneConnectivityTask
	// .getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
	// System.out.println(undirectedConnectivityResult.getExpectedEdgeCount());
	// for (ICluster c : clusters) {
	// Pair<ICluster, ICluster> p = Pair.of(c, c);
	// double val = undirectedConnectivityResult.getExpectedEdgeCount().get(p);
	// assertTrue("expected edges not larger than " + 0 + " for " + p, val > 0);
	// }
	// }

	@Test
	public void testEdgeCrossoverNoIntraClusterEdges() throws Exception {
		new PearsonCorrelationFunction();

		Triple<ClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataNoIntraClusterEdges(true);

		TiconeNetworkImpl network = pair.getMiddle();
		ClusterObjectMapping pom = pair.getLeft();
		IClusterList clusters = pair.getRight();
		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false, new ShuffleNetworkWithEdgeCrossover(true));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityPValueResult edgeCrossoverConnectivityResult = null;

		ticoneConnectivityTask.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network);
		for (ICluster c : clusters) {
			assertTrue(edgeCrossoverConnectivityResult.getDirectedEdgeCount().containsKey(new ClusterPair(c, c)));
			assertTrue(
					edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues().containsKey(new ClusterPair(c, c)));
			assertTrue(edgeCrossoverConnectivityResult.getUndirectedEdgeCount().containsKey(new ClusterPair(c, c)));
			assertTrue(
					edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues().containsKey(new ClusterPair(c, c)));
		}

		System.out.println(edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues());
		System.out.println(edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues());
	}

	@Test
	public void testUndirectedConnectivityNoIntraClusterEdgesExpectedNotInfinity() throws Exception {
		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0, 1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePattern);

		IClusteringMethodBuilder<CLARAClusteringMethod> clusteringMethod = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		// parse time series data
		LoadDataFromFile loadData = new LoadDataFromFile();
		loadData.getColumnMapping().setObjectIdColumn(2, "objectId");
		loadData.getColumnMapping().setReplicateColumn(1, "replicate");
		loadData.getColumnMapping().setTimePointsColumns(new int[] { 3, 4, 5, 6, 7 },
				new String[] { "tp1", "tp2", "tp3", "tp4", "tp5", "tp6", "tp7" });
		loadData.setImportFile(new File(getClass()
				.getResource("/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5.txt")
				.getFile()));

		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();

		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, clusteringMethod,
				new InitialClusteringMethod(clusteringMethod, 3, 42), loadData, new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		// parse network
		Scanner scanner = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5-seed42-samePatternEdgeProb0.4.txt")
				.getFile()));
		TiconeNetworkImpl network = NetworkUtil.parseNetworkFromScanner(new TiconeNetworkImplFactory(), scanner, "bla",
				null, false);
		System.out.println(network);

		// perform clustering into 3 clusters
		ClusterObjectMapping pom = bla.doIteration();

		IClusterList clusters = new ClusterList(pom.getClusters());
		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		System.out.println(ticoneConnectivityTask.removeObjectsNotInNetwork(network));
		System.out.println(ticoneConnectivityTask.removeNodesNotInDataset(network));

		ConnectivityResult undirectedConnectivityResult = null;
		undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.calculatePValues(network);
		System.out.println(undirectedConnectivityResult.getExpectedEdgeCount());
		for (ICluster c : clusters) {
			ClusterPair p = new ClusterPair(c, c);
			double val = undirectedConnectivityResult.getExpectedEdgeCount().getDouble(p);
			assertTrue("expected edges is infinity for " + p, Double.isFinite(val));
		}
	}

	@Test
	public void testMergeClustersAfterConnectivity() throws Exception {
		new ShuffleDatasetRowwise();
		int numberOfInitialClusters = 10;
		int numberOfRefinementPermutations = 100;

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5_mergeTestCase.txt")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		int expectedNumberClusters = numberOfInitialClusters;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		List<IArithmeticFeature<? extends Comparable<?>>> featureList = new ArrayList<>();
		featureList.add(new ClusterFeatureNumberObjects());
		featureList.add(new ClusterFeatureAverageSimilarity(similarityFunction));

		// parse network
		Scanner scanner = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5-seed42-samePatternEdgeProb0.0.txt")
				.getFile()));
		TiconeNetworkImpl network = NetworkUtil.parseNetworkFromScanner(new TiconeNetworkImplFactory(), scanner, "bla",
				null, false);

		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, bla.getLatestClustering(), new ClusterList(bla.getLatestClustering().getClusters()),
				numberOfRefinementPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ticoneConnectivityTask.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);

		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		bla.doIteration();

		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 8
		bla.mergeClusters(new int[] { 1, 4 });

		assertEquals(3, bla.getHistory().getIterationNumber());
		expectedNumberClusters--;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());
	}

	protected Triple<ClusterObjectMapping, TiconeNetworkImpl, IClusterList> demoDataNoIntraClusterEdges(
			boolean isDirected) throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, 1);
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}
//		ClusterObjectMapping.resetClusterCount();
		return Triple.of(demoPom, network, (IClusterList) new ClusterList(demoClusters));
	}

	@Test
	public void testTotalNodeDegreeCounts() throws Exception {

		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(true);

		network.initNodeDegreesUndirected();

		int[] totalNodeDegreeCounts = network.getTotalNodeDegreeCounts();
		// System.out.println(Arrays.toString(totalNodeDegreeCounts));
		assertArrayEquals(new int[] { 0, 0, 3, 0, 3, 0, 0, 0, 0, 0, 3 }, totalNodeDegreeCounts);
	}

	@Test
	public void testTotalNodeDegreeCounts2() throws Exception {

		Triple<ClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataConstantEdgeProbabilityUndirected();

		TiconeNetworkImpl network = pair.getMiddle();

		network.initNodeDegreesUndirected();

		network.getTotalNodeDegreeCounts();
	}

	// failing since removal of knn TODO
	// @Test
	// public void testConnectivityWithConstantEdgeProbabilitySmall() throws
	// TooFewObjectsClusteringException,
	// FileNotFoundException, InterruptedException,
	// TimePointSignalParseEmptyStringException,
	// ObjectIdParseEmptyStringException, SampleNameParseEmptyStringException,
	// MultipleTimeSeriesSignalsForSameSampleParseException,
	// TimePointSignalNotANumberException {
	// ISimilarity similarityFunction = new PearsonCorrelation();
	// IDiscretizePrototype discretizePattern = new
	// DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0, 1, 4, 4);
	// AbstractTimeSeriesPreprocessor timeSeriesData = new AbsoluteValues();
	// IAggregateCluster refinePattern = new AggregateClusterMean();
	// IPermutateDataset permFunction = new PermutateDatasetGlobally();
	//
	// Triple<ClusterObjectMapping, TiCoNENetworkImpl, IClusters> pair =
	// demoDataConstantEdgeProbabilityUndirected();
	//
	// TiCoNENetworkImpl network = network;
	// ClusterObjectMapping pom = pair.getLeft();
	//
	// System.out.println(pom);
	//
	// IClusters clusters = pair.getRight();
	// int numberPermutations = 1000;
	// TiCoNEClusterConnectivityTask ticoneConnectivityTask = new
	// TiCoNEClusterConnectivityTask(network,
	// clusteringResult, pom, clusters, numberPermutations, false,
	//
	// network.initNodeDegreesUndirected();
	// ticoneConnectivityTask.initEntrezToNetworks();
	// network.performEdgeCrossovers(16.12,
	//
	// int[] degrees = network.getNodeDegreesUndirected();
	// System.out.println(degrees);
	//
	// ticoneConnectivityTask.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
	//
	// // none of the cluster pairs should be significantly enriched
	// EdgeCrossoverConnectivityResult edgeCrossoverConnectivityResult =
	// ticoneConnectivityTask
	// .edgeCrossoverLogOddsPValues(network);
	// //
	// System.out.println(edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues());
	// System.out.println(edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues());
	// for (Pair<ICluster, ICluster> clusterPair :
	// edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues()
	// .keySet()) {
	// assertTrue(edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues().get(clusterPair)
	// .doubleValue() > 0.0);
	// }
	// }

	@Test
	public void testConnectivityWithConstantEdgeProbability() throws Exception {
		Scanner scan = new Scanner(new File(getClass().getResource("/test_files/connectivity/"
				+ "qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar0.8-minObjSim0.6-hardcodedProbs-randomEdgeProb.txt"
		// "qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.75-hardcodedProbs.txt"
		).getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[5]);
		ICluster c1 = pom.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[5]);
		ICluster c2 = pom.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[5]);
		ICluster c3 = pom.getClusterBuilder().setPrototype(f.build()).build();
		for (int i = 0; i < 80; i++) {
			pom.addMapping(objs.get(i), c1, TestSimpleSimilarityValue.of(1.0));
		}
		for (int i = 80; i < 160; i++) {
			pom.addMapping(objs.get(i), c2, TestSimpleSimilarityValue.of(1.0));
		}
		for (int i = 160; i < 240; i++) {
			pom.addMapping(objs.get(i), c3, TestSimpleSimilarityValue.of(1.0));
		}

		// parse network
		Scanner scanner = new Scanner(new File(getClass().getResource(
				// "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.75-hardcodedProbs-seed42-samePatternEdgeProb0.8.txt"));
				"/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs0.01-randomEdgeProb-seed42-samePatternEdgeProb0.9.txt")
				.getFile()));
		// "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs-randomEdgeProb0.2-seed42-samePatternEdgeProb0.8.txt"));
		TiconeNetworkImpl network = NetworkUtil.parseNetworkFromScanner(new TiconeNetworkImplFactory(), scanner, "bla",
				null, false);
		network = network.copy();

		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, new ClusterList(pom.getClusters()), 100, true,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		ticoneConnectivityTask.removeNodesNotInDataset(network);

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		System.out.println(
				"ticoneConnectivityTask.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network).edgeCountEnrichment");
		System.out.println(ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network).edgeCountEnrichment);
		System.out.println(
				"ticoneConnectivityTask.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network).edgeCountEnrichment");
		System.out.println(ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network).edgeCountEnrichment);

		// none of the cluster pairs should be significantly enriched
		ConnectivityPValueResult edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network);
		System.out.println("edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues()");
		System.out.println(edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues());
		System.out.println("edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues()");
		System.out.println(edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues());
		boolean all1 = true, all0 = true;
		for (IClusterPair clusterPair : edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues().keySet()) {
			double pval = edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues().get(clusterPair).getDouble();
			all0 &= pval == 0.0;
			all1 &= pval == 1.0;
		}
		assertTrue("undirected p-values should not all be 0.0", !all0);
		assertTrue("undirected p-values should not all be 1.0", !all1);
		all1 = true;
		all0 = true;
		for (IClusterPair clusterPair : edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues().keySet()) {
			double pval = edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues().get(clusterPair).getDouble();
			all0 &= pval == 0.0;
			all1 &= pval == 1.0;
		}
		assertTrue("directed p-values should not all be 0.0", !all0);
		assertTrue("directed p-values should not all be 1.0", !all1);
	}

	@Test
	public void testConnectivityWithConstantEdgeProbability2() throws Exception {
		new ShuffleDatasetRowwise();
		Scanner scan = new Scanner(new File(getClass().getResource("/test_files/connectivity/"
				+ "qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar0.8-minObjSim0.6-hardcodedProbs-randomEdgeProb.txt"
		// "qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.75-hardcodedProbs.txt"
		).getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.setPrototypeComponentBuilders(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		new CLARAClusteringMethodBuilder().setSimilarityFunction(similarityFunction)
				.setPrototypeBuilder(prototypeFactory).build();

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[5]);
		ICluster c1 = pom.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[5]);
		ICluster c2 = pom.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[5]);
		ICluster c3 = pom.getClusterBuilder().setPrototype(f.build()).build();
		for (int i = 0; i < 80; i++) {
			pom.addMapping(objs.get(i), c1, TestSimpleSimilarityValue.of(1.0));
		}
		for (int i = 80; i < 160; i++) {
			pom.addMapping(objs.get(i), c2, TestSimpleSimilarityValue.of(1.0));
		}
		for (int i = 160; i < 240; i++) {
			pom.addMapping(objs.get(i), c3, TestSimpleSimilarityValue.of(1.0));
		}

		// parse network
		Scanner scanner = new Scanner(new File(getClass().getResource(
				// "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.75-hardcodedProbs-seed42-samePatternEdgeProb0.8.txt"));
				"/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs0.05-randomEdgeProb-seed42-samePatternEdgeProb0.9.txt")
				.getFile()));
		// "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs-randomEdgeProb0.2-seed42-samePatternEdgeProb0.8.txt"));
		TiconeNetworkImpl network = NetworkUtil.parseNetworkFromScanner(new TiconeNetworkImplFactory(), scanner, "bla",
				null, false);
		network = network.copy();

		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, new ClusterList(pom.getClusters()), 100, true,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		ticoneConnectivityTask.removeNodesNotInDataset(network);

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		int[] degrees = network.getNodeDegreesUndirected();
		System.out.println(degrees);

		System.out.println(ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network).edgeCountEnrichment);
		System.out.println(ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network).edgeCountEnrichment);

		// none of the cluster pairs should be significantly enriched
		ConnectivityPValueResult edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network);
		System.out.println("edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues()");
		System.out.println(edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues());
		System.out.println("edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues()");
		System.out.println(edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues());
		boolean all1 = true, all0 = true;
		for (IClusterPair clusterPair : edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues().keySet()) {
			double pval = edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues().get(clusterPair).getDouble();
			all0 &= pval == 0.0;
			all1 &= pval == 1.0;
		}
		assertTrue("undirected p-values should not all be 0.0", !all0);
		assertTrue("undirected p-values should not all be 1.0", !all1);
		all1 = true;
		all0 = true;
		for (IClusterPair clusterPair : edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues().keySet()) {
			double pval = edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues().get(clusterPair).getDouble();
			all0 &= pval == 0.0;
			all1 &= pval == 1.0;
		}
		assertTrue("directed p-values should not all be 0.0", !all0);
		assertTrue("directed p-values should not all be 1.0", !all1);
	}

	// @Test
	// public void testConnectivityWithConstantEdgeProbabilitySeed43() throws
	// TooFewObjectsClusteringException,
	// FileNotFoundException, InterruptedException,
	// TimePointSignalParseEmptyStringException,
	// ObjectIdParseEmptyStringException, SampleNameParseEmptyStringException,
	// MultipleTimeSeriesSignalsForSameSampleParseException,
	// TimePointSignalNotANumberException {
	// ISimilarity similarityFunction = new PearsonCorrelation();
	// IPermutateDataset permutationFunction = new PermutateDatasetRowwise();
	// int numberOfPermutations = 0;
	// int numberOfInitialClusters = 4;
	// int numberOfRefinementPermutations = 100;
	//
	// Scanner scan = new Scanner(new File(
	// "/test_files/connectivity/qual-patterns3-objects80-seed43-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.75-hardcodedProbs.txt"));
	// ITimeSeriesObjectSet objs = Parser.parseObjectSets(scan, new
	// ImportColumnMapping(2, "Column 2", 1, "Column 1",
	// new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4",
	// "tp5" }));
	// AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
	// preprocessor.initializeTimeSeriesData(objs);
	// preprocessor.calculatePatterns();
	//
	// IDiscretizePrototype discretizePatternFunction = new
	// DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
	// preprocessor.minValue, preprocessor.maxValue, 4, 4);
	// IAggregateCluster refinePattern = new AggregateClusterMean();
	// Random random = new Random(14);
	// IClustering initialClusteringInterface = new
	// CLARAClustering(similarityFunction, refinePattern,
	// discretizePatternFunction, random);
	//
	// ClusterObjectMapping pom = new ClusterObjectMapping(f);
	// ICluster c1 = new Cluster(new double[5]);
	// ICluster c2 = new Cluster(new double[5]);
	// ICluster c3 = new Cluster(new double[5]);
	// pom.addPattern(c1);
	// pom.addPattern(c2);
	// pom.addPattern(c3);
	// for (int i = 0; i < 120; i++) {
	// pom.addMapping(objs.get(i), c1, 1.0);
	// }
	// for (int i = 120; i < 240; i++) {
	// pom.addMapping(objs.get(i), c2, 1.0);
	// }
	// for (int i = 240; i < 360; i++) {
	// pom.addMapping(objs.get(i), c3, 1.0);
	// }
	//
	// // parse network
	// Scanner scanner = new Scanner(new File(
	// "/test_files/connectivity/qual-patterns3-objects80-seed43-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.75-hardcodedProbs-seed43-samePatternEdgeProb0.5.txt"));
	// TiCoNENetwork<TiCoNENetworkNodeImpl,TiCoNENetworkEdge<TiCoNENetworkNodeImpl>>
	// network = NetworkUtil.parseNetworkFromScanner(new
	// TiCoNENetworkImplFactory(), scanner, "bla",
	// false);
	//
	// TiCoNEClusterConnectivityTask ticoneConnectivityTask = new
	// TiCoNEClusterConnectivityTask(network,
	// clusteringResult, pom, new Clusters(pom.clusterSet()), 100,
	// true);
	//
	// ticoneConnectivityTask.initNodeDegreesUndirected();
	// ticoneConnectivityTask.initEntrezToNetworks();
	//
	//
	// Map<TiCoNENetworkNodeImpl, Integer> degrees =
	// ticoneConnectivityTask.getNodeDegreesUndirected().values().iterator()
	// .next();
	// System.out.println(degrees);
	//
	// ConnectivityResult undirectedConnectivityResult = ticoneConnectivityTask
	// .getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
	//
	// // none of the cluster pairs should be significantly enriched
	// EdgeCrossoverConnectivityResult edgeCrossoverConnectivityResult =
	// ticoneConnectivityTask.edgeCrossoverLogOddsPValues();
	// System.out.println("getUndirectedEdgeCount()");
	// System.out.println(edgeCrossoverConnectivityResult.getUndirectedEdgeCount());
	// System.out.println("getUndirectedEdgeCountPvalues()");
	// System.out.println(edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues());
	// for (Pair<ICluster, ICluster> clusterPair :
	// edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues()
	// .keySet()) {
	// assertTrue(edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues().get(clusterPair)
	// .doubleValue() > 0.0);
	// }
	// }

	@Test
	public void testRemoveNodesNotInDataset() throws Exception {
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();

		TiconeNetworkNodeImpl node = network.addNode("testObjectNotInDataset");

		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		Set<TiconeNetworkNodeImpl> removedNodes = ticoneConnectivityTask.removeNodesNotInDataset(network);
		assertEquals(1, removedNodes.size());
		assertEquals(node, removedNodes.iterator().next());
	}

	@Test
	public void testRemoveNodesNotInDataset2() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		Map<Integer, ICluster> idToCluster = new HashMap<>();
		BufferedReader r = new BufferedReader(
				new FileReader(getClass().getResource("/test_files/clustering/clustering_influenza.txt").getFile()));
		while (r.ready()) {
			String nextLine = r.readLine();
			String[] split = nextLine.split("\t");
			int id = Integer.valueOf(split[0]);
			if (!idToCluster.containsKey(id)) {
				tsf.setTimeSeries(new double[0]);
				ICluster cluster = pom.getClusterBuilder().setPrototype(f.build()).setClusterNumber(id)
						.setInternalClusterNumber((long) id).setIsKeep(true).build();
				idToCluster.put(id, cluster);
			}
			pom.addMapping(new TimeSeriesObject(split[1], 1, TimeSeries.of(new double[0])), idToCluster.get(id),
					TestSimpleSimilarityValue.of(1.0));
		}
		r.close();
		IClusterList clusters = new ClusterList(pom.getClusters());

		TiconeNetworkImpl network = TestDataUtility.parseI2DPPINetwork();

		int numberPermutations = 1000;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, true,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringWithRandomPrototypeTimeSeries(
						similarityFunction, f, new CreateRandomTimeSeries(pom.getAssignedObjects()))));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);
	}

	@Test
	public void testConnectivityWithRemoveNodesNotInDataset() throws Exception {
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();

		TiconeNetworkNodeImpl node = network.addNode("testObjectNotInDataset");

		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		Set<TiconeNetworkNodeImpl> removedNodes = ticoneConnectivityTask.removeNodesNotInDataset(network);
		ticoneConnectivityTask.initEntrezToNetworks();
		assertEquals(1, removedNodes.size());
		assertEquals(node, removedNodes.iterator().next());

		ConnectivityResult directedConnectivityResult = null, undirectedConnectivityResult = null;
		ConnectivityPValueResult edgeCrossoverConnectivityResult = null;

		undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		directedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network);

		new ConnectivityResultWrapper(clusteringResult, directedConnectivityResult, undirectedConnectivityResult,
				edgeCrossoverConnectivityResult, 1);
	}

	@Test
	public void testUndirectedConnectivity1ClusterOnly() throws Exception {
		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0, 1, 4, 4);
		ITimeSeriesPreprocessor timeSeriesData = new AbsoluteValues();
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		ClusterObjectMapping pom = pair.getFirst();

		timeSeriesData.initializeObjects(pom.getAssignedObjects());
		timeSeriesData.process();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePattern);

		new MergeSelectedClusters(new ClusterList(pom.getClusters()), similarityFunction).doMerge(pom);

		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, new ClusterList(pom.getClusters()), numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult undirectedConnectivityResult = null;
		undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.calculatePValues(network, true);

		// we have 18 undirected edges
		assertEquals(18, undirectedConnectivityResult.edgeCount.values().iterator().next().intValue());
		assertEquals(18.0, undirectedConnectivityResult.expectedEdgeCount.values().iterator().next(), 0.0001);
		assertEquals(0.0, undirectedConnectivityResult.edgeCountEnrichment.values().iterator().next(), 0.0001);
	}

	@Test
	public void testDirectedConnectivity1ClusterOnly() throws Exception {
		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0, 1, 4, 4);
		ITimeSeriesPreprocessor timeSeriesData = new AbsoluteValues();
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		new ShuffleDatasetGlobally();

		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		ClusterObjectMapping pom = pair.getFirst();

		timeSeriesData.initializeObjects(pom.getAssignedObjects());
		timeSeriesData.process();
		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePattern);

		new MergeSelectedClusters(new ClusterList(pom.getClusters()), similarityFunction).doMerge(pom);

		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, new ClusterList(pom.getClusters()), numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult directedConnectivityResult = null;
		directedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.calculatePValues(network);

		// we have 18 directed edges
		assertEquals(18, directedConnectivityResult.edgeCount.values().iterator().next().intValue());
		assertEquals(18.0, directedConnectivityResult.expectedEdgeCount.values().iterator().next(), 0.0001);
		assertEquals(0.0, directedConnectivityResult.edgeCountEnrichment.values().iterator().next(), 0.0001);
	}

	@Test
	public void testUndirectedConnectivity2Clusters() throws Exception {
		new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0, 1, 4, 4);
		new AbsoluteValues();
		new AggregateClusterMeanTimeSeries();

		Triple<ClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoData2Clusters();

		TiconeNetworkImpl network = pair.getMiddle();

		ClusterObjectMapping pom = pair.getLeft();

		int numberPermutations = 0;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, new ClusterList(pom.getClusters()), numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult undirectedConnectivityResult = null;
		double[][] totalDirectedEdgeProbabilityNodeDegrees = network.getTotalUndirectedEdgeProbabilityNodeDegrees();
		System.out.println(ArraysExt.toSeparatedString(totalDirectedEdgeProbabilityNodeDegrees));

		undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);

		System.out.println(undirectedConnectivityResult.getExpectedEdgeCount());
		System.out.println(undirectedConnectivityResult.getEdgeCount());
		System.out.println(undirectedConnectivityResult.getEdgeCountEnrichment());
	}

	@Test
	public void testDirectedEdgeProbabilities() throws Exception {
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(true);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();
		int numberPermutations = 0;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		network.initNodeDegreesUndirected();

		double[][] expectedEdgeProbabilities = new double[][] {
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						1 / 3.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						0.0 },
				new double[] { 0.000000, 0.000000, 2 / 3.0, 0.000000, 1.0, 0.000000, 0.000000, 0.000000, 0.0, 0.0,
						2 / 3.0 } };

		double[][] totalDirectedEdgeProbabilityNodeDegrees = network.getTotalDirectedEdgeProbabilityNodeDegrees();

		for (int i = 0; i < expectedEdgeProbabilities.length; i++)
			ArrayAssert.assertEquals(expectedEdgeProbabilities[i], totalDirectedEdgeProbabilityNodeDegrees[i], 0.0001);
	}

	@Test
	public void testUndirectedEdgeProbabilities() throws Exception {
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();
		int numberPermutations = 0;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		network.initNodeDegreesUndirected();

		double[][] expectedEdgeProbabilities = new double[][] {
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.666667 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000 },
				new double[] { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.500000 } };

		double[][] totalUndirectedEdgeProbabilityNodeDegrees = network.getTotalUndirectedEdgeProbabilityNodeDegrees();

		for (int i = 0; i < expectedEdgeProbabilities.length; i++)
			ArrayAssert.assertEquals(expectedEdgeProbabilities[i], totalUndirectedEdgeProbabilityNodeDegrees[i],
					0.0001);
	}

	@Test
	public void testTotalExpectedEdgesSummingUpToPresentEdgesUndirectedConnectivity() throws Exception {
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();
		int numberPermutations = 1000;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		network.initNodeDegreesUndirected();

		ConnectivityResult undirectedConnectivityResult = null;

		undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.calculatePValues(network);

		int edgeCount = network.getEdgeCount();

		double sumExpectedEdgeCounts = 0.0;
		for (IClusterPair cp : undirectedConnectivityResult.expectedEdgeCount.keySet()) {
			if (cp.getFirst().getInternalClusterId() <= cp.getSecond().getInternalClusterId())
				sumExpectedEdgeCounts += undirectedConnectivityResult.expectedEdgeCount.getDouble(cp);
		}

		assertEquals(edgeCount, sumExpectedEdgeCounts, 0.0001);
	}

	@Test
	@Tag("long-running")
	public void testDirectedConnectivityAlexMetatargetomeThreshold20() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		Map<Integer, ICluster> idToCluster = new HashMap<>();
		Scanner r = new Scanner(new File("/media/chris/zfs/Dropbox/Dropbox/TiCoNE/clustering.txt"));
		while (r.hasNextLine()) {
			String nextLine = r.nextLine();
			String[] split = nextLine.split("\t");
			int id = Integer.valueOf(split[0]);
			if (!idToCluster.containsKey(id)) {
				tsf.setTimeSeries(new double[0]);
				ICluster cluster = pom.getClusterBuilder().setPrototype(f.build()).setClusterNumber(id)
						.setInternalClusterNumber((long) id).setIsKeep(true).build();
				idToCluster.put(id, cluster);
			}
			pom.addMapping(new TimeSeriesObject(split[1], 1, TimeSeries.of(new double[0])), idToCluster.get(id),
					TestSimpleSimilarityValue.of(1.0));
		}
		r.close();
		IClusterList clusters = new ClusterList(pom.getClusters());

		TiconeNetworkImpl network = TiconeNetworkImpl.parseFromFile("alex_metatargetome_thres20", true,
				new File("/media/chris/zfs/Dropbox/Dropbox/TiCoNE/alex_metatargetome_thres20.txt.tab"));

		int numberPermutations = 1000;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, true,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult directedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		ConnectivityResult undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		ConnectivityPValueResult edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network);
		// System.out.println(directedConnectivityResult.getEdgeCountEnrichment());
		// System.out.println(edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues());

		// directedConnectivityResult.getEdgeCountEnrichment().keySet().stream()
		// .forEach(k ->
		// System.out.println(String.format("Cluster_%d\tCluster_%d\t%.3f\t%d\t%.3f\t%.3f",
		// k.getLeft().getClusterNumber(), k.getRight().getClusterNumber(),
		// directedConnectivityResult.getExpectedEdgeCount().get(k),
		// directedConnectivityResult.getEdgeCount().get(k),
		// directedConnectivityResult.getEdgeCountEnrichment().get(k),
		// edgeCrossoverConnectivityResult.getDirectedEdgeCountPvalues().get(k))));

		scatterPlotEnrichmentVsPvalues("Undirected", "Enrichment", "p",
				undirectedConnectivityResult.edgeCountEnrichment, edgeCrossoverConnectivityResult.pvaluesUndirected);
		scatterPlotEnrichmentVsPvalues("Directed", "Enrichment", "p", directedConnectivityResult.edgeCountEnrichment,
				edgeCrossoverConnectivityResult.pvaluesDirected);
	}

	@Test
	@Tag("long-running")
	public void testUndirectedConnectivityInfluenza() throws Exception {
		int numberOfInitialClusters = 20;
		ITimeSeriesObjectList objs = TestDataUtility.parseInfluenzaExpressionData();

		TiconeNetworkImpl network = TestDataUtility.parseI2DPPINetwork();

		objs = objs.mapToNetwork(network);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();
		ClusterObjectMapping pom = bla.doIteration();

		List<IArithmeticFeature<? extends Comparable<?>>> featureList = new ArrayList<>();
		featureList.add(new ClusterFeatureNumberObjects());
		featureList.add(new ClusterFeatureAverageSimilarity(similarityFunction));

		IClusterList clusters = pom.getClusters().asList();

		int numberPermutations = 1000;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, true,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringWithRandomPrototypeTimeSeries(
						similarityFunction, f, new CreateRandomTimeSeries(pom.getAssignedObjects()))));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		// ConnectivityResult directedConnectivityResult =
		// ticoneConnectivityTask
		// .getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		ConnectivityResult undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		ConnectivityPValueResult edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network,
				true);
		// System.out.println(undirectedConnectivityResult.getEdgeCountEnrichment());
		// System.out.println(edgeCrossoverConnectivityResult.getUndirectedEdgeCountPvalues());

		Map<IClusterPair, IntList> permutationEdgeCountsUndirected = edgeCrossoverConnectivityResult.permutationEdgeCountsUndirected;
		for (int p = 0; p < numberPermutations; p++) {
			long sum = 0;
			for (IClusterPair k : permutationEdgeCountsUndirected.keySet())
				if (k.getFirst().getClusterNumber() <= k.getSecond().getClusterNumber())
					sum += permutationEdgeCountsUndirected.get(k).get(p);
			assertEquals(network.getEdgeCount(), sum);
		}
		// System.out.println(permutationEdgeCountsUndirected);

		scatterPlotEnrichmentVsPvalues("Undirected", "Enrichment", "p",
				undirectedConnectivityResult.edgeCountEnrichment, edgeCrossoverConnectivityResult.pvaluesUndirected);
		// scatterPlotEnrichmentVsPvalues("Directed", "Enrichment", "p",
		// directedConnectivityResult.edgeCountEnrichment,
		// edgeCrossoverConnectivityResult.pvaluesDirected);
	}

	@Test
	public void testUndirectedConnectivitySumEnrichmentScores() throws Exception {
		new PearsonCorrelationFunction();
		new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0, 1, 4, 4);
		new AbsoluteValues();
		new AggregateClusterMeanTimeSeries();
		new ShuffleDatasetGlobally();

		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);
		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();

		int numberPermutations = 0;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, pom, clusters, numberPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityResult undirectedConnectivityResult = ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);

		for (IClusterPair cp : undirectedConnectivityResult.edgeCountEnrichment.keySet()) {
			Math.pow(10, undirectedConnectivityResult.edgeCountEnrichment.get(cp).doubleValue());
		}
	}

	protected Triple<ClusterObjectMapping, TiconeNetworkImpl, IClusterList> demoData2Clusters() throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		Random r = new Random(14);

		tsf.setTimeSeries(r.doubles(5).toArray());
		ICluster c = demoPom.getClusterBuilder().setPrototype(f.build()).build();
		demoClusters.add(c);

		String objId = "Object 1";
		demoNodes.add(objId);
		network.addNode(objId);
		demoEntrezToNetworks.put(objId, c);
		ITimeSeriesObject data = new TimeSeriesObject(objId, 1);
		demoPom.addMapping(data, c, TestSimpleSimilarityValue.of(r.nextDouble()));

		objId = "Object 2";
		demoNodes.add(objId);
		network.addNode(objId);
		demoEntrezToNetworks.put(objId, c);
		data = new TimeSeriesObject(objId, 1);
		demoPom.addMapping(data, c, TestSimpleSimilarityValue.of(r.nextDouble()));

		tsf.setTimeSeries(r.doubles(5).toArray());
		c = demoPom.getClusterBuilder().setPrototype(f.build()).build();
		demoClusters.add(c);

		objId = "Object 3";
		demoNodes.add(objId);
		network.addNode(objId);
		demoEntrezToNetworks.put(objId, c);
		data = new TimeSeriesObject(objId, 1);
		demoPom.addMapping(data, c, TestSimpleSimilarityValue.of(r.nextDouble()));

		objId = "Object 4";
		demoNodes.add(objId);
		network.addNode(objId);
		demoEntrezToNetworks.put(objId, c);
		data = new TimeSeriesObject(objId, 1);
		demoPom.addMapping(data, c, TestSimpleSimilarityValue.of(r.nextDouble()));

		tsf.setTimeSeries(r.doubles(5).toArray());
		c = demoPom.getClusterBuilder().setPrototype(f.build()).build();
		demoClusters.add(c);

		objId = "Object 5";
		demoNodes.add(objId);
		network.addNode(objId);
		demoEntrezToNetworks.put(objId, c);
		data = new TimeSeriesObject(objId, 1);
		demoPom.addMapping(data, c, TestSimpleSimilarityValue.of(r.nextDouble()));

		objId = "Object 6";
		demoNodes.add(objId);
		network.addNode(objId);
		demoEntrezToNetworks.put(objId, c);
		data = new TimeSeriesObject(objId, 1);
		demoPom.addMapping(data, c, TestSimpleSimilarityValue.of(r.nextDouble()));

		demoEdges.put("Object 1", new ArrayList<String>());
		demoEdges.get("Object 1").add("Object 2");
		network.addEdge("Object 1", "Object 2", false);
		demoEdges.put("Object 2", new ArrayList<String>());
		demoEdges.get("Object 2").add("Object 2");
		network.addEdge("Object 2", "Object 2", false);
		demoEdges.get("Object 2").add("Object 3");
		network.addEdge("Object 2", "Object 3", false);
		demoEdges.get("Object 2").add("Object 4");
		network.addEdge("Object 2", "Object 4", false);
		demoEdges.put("Object 4", new ArrayList<String>());
		demoEdges.get("Object 4").add("Object 4");
		network.addEdge("Object 4", "Object 4", false);
		demoEdges.get("Object 4").add("Object 5");
		network.addEdge("Object 4", "Object 5", false);
		demoEdges.get("Object 4").add("Object 6");
		network.addEdge("Object 4", "Object 6", false);
		demoEdges.put("Object 5", new ArrayList<String>());
		demoEdges.get("Object 5").add("Object 6");
		network.addEdge("Object 5", "Object 6", false);

//		ClusterObjectMapping.resetClusterCount();
		return Triple.of(demoPom, network, (IClusterList) new ClusterList(demoClusters));
	}

	@Test
	@Tag("long-running")
	public void testCompareDirectedTwoSidedEdgeCrossoverToOldImplementation() throws Exception {
		String networkPath = "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs-randomEdgeProb-seed42-samePatternEdgeProb0.9_randomClusterToHaveEqualNodeDegrees.txt";
		String clusteringPath = "/test_files/clustering/2017_05_07_seed42_clustering.txt";
		ClusterObjectMapping clustering = parseFromFile(new File(clusteringPath), 1, 0,
				new PearsonCorrelationFunction());

		Map<Integer, ICluster> clusters = new HashMap<>();
		for (ICluster c : clustering.getClusters())
			clusters.put(c.getClusterNumber(), c);

		TiconeNetworkImpl network = TiconeNetworkImpl.parseFromFile("seed42", true, new File(networkPath));

		int numberPermutations = 100;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, clustering, new ClusterList(clusters.values()), numberPermutations, true,
				new ShuffleNetworkWithEdgeCrossover(true));

		network.initNodeDegreesUndirected();

		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ConnectivityPValueResult edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network);

		// directed edge counts from ticone version 1.2.75-SNAPSHOT using the
		// old edge crossover implementation
		List<int[]> edgeCountHelper = new ArrayList<>();
		edgeCountHelper.add(new int[] { 1, 1, 139 });
		edgeCountHelper.add(new int[] { 1, 5, 99 });
		edgeCountHelper.add(new int[] { 1, 6, 71 });
		edgeCountHelper.add(new int[] { 1, 7, 70 });
		edgeCountHelper.add(new int[] { 1, 11, 458 });
		edgeCountHelper.add(new int[] { 1, 12, 399 });
		edgeCountHelper.add(new int[] { 1, 13, 952 });
		edgeCountHelper.add(new int[] { 5, 1, 104 });
		edgeCountHelper.add(new int[] { 5, 5, 95 });
		edgeCountHelper.add(new int[] { 5, 6, 60 });
		edgeCountHelper.add(new int[] { 5, 7, 63 });
		edgeCountHelper.add(new int[] { 5, 11, 348 });
		edgeCountHelper.add(new int[] { 5, 12, 377 });
		edgeCountHelper.add(new int[] { 5, 13, 802 });
		edgeCountHelper.add(new int[] { 6, 1, 56 });
		edgeCountHelper.add(new int[] { 6, 5, 52 });
		edgeCountHelper.add(new int[] { 6, 6, 41 });
		edgeCountHelper.add(new int[] { 6, 7, 61 });
		edgeCountHelper.add(new int[] { 6, 11, 259 });
		edgeCountHelper.add(new int[] { 6, 12, 246 });
		edgeCountHelper.add(new int[] { 6, 13, 573 });
		edgeCountHelper.add(new int[] { 7, 1, 91 });
		edgeCountHelper.add(new int[] { 7, 5, 65 });
		edgeCountHelper.add(new int[] { 7, 6, 51 });
		edgeCountHelper.add(new int[] { 7, 7, 84 });
		edgeCountHelper.add(new int[] { 7, 11, 333 });
		edgeCountHelper.add(new int[] { 7, 12, 320 });
		edgeCountHelper.add(new int[] { 7, 13, 749 });
		edgeCountHelper.add(new int[] { 11, 1, 436 });
		edgeCountHelper.add(new int[] { 11, 5, 395 });
		edgeCountHelper.add(new int[] { 11, 6, 285 });
		edgeCountHelper.add(new int[] { 11, 7, 377 });
		edgeCountHelper.add(new int[] { 11, 11, 3855 });
		edgeCountHelper.add(new int[] { 11, 12, 3625 });
		edgeCountHelper.add(new int[] { 11, 13, 2046 });
		edgeCountHelper.add(new int[] { 12, 1, 446 });
		edgeCountHelper.add(new int[] { 12, 5, 324 });
		edgeCountHelper.add(new int[] { 12, 6, 274 });
		edgeCountHelper.add(new int[] { 12, 7, 346 });
		edgeCountHelper.add(new int[] { 12, 11, 3612 });
		edgeCountHelper.add(new int[] { 12, 12, 3659 });
		edgeCountHelper.add(new int[] { 12, 13, 870 });
		edgeCountHelper.add(new int[] { 13, 1, 926 });
		edgeCountHelper.add(new int[] { 13, 5, 784 });
		edgeCountHelper.add(new int[] { 13, 6, 557 });
		edgeCountHelper.add(new int[] { 13, 7, 769 });
		edgeCountHelper.add(new int[] { 13, 11, 1909 });
		edgeCountHelper.add(new int[] { 13, 12, 861 });
		edgeCountHelper.add(new int[] { 13, 13, 3664 });

		Map<Pair<ICluster, ICluster>, Integer> expectedNumberEdges = transformClusterEdgeCounts(clusters,
				edgeCountHelper);

		// expected edge counts from ticone version 1.2.75-SNAPSHOT using the
		// old
		// edge crossover implementation
		Map<int[], Double> expectedHelper = new HashMap<>();
		expectedHelper.put(new int[] { 1, 7 }, 103.535);
		expectedHelper.put(new int[] { 6, 12 }, 328.857);
		expectedHelper.put(new int[] { 6, 13 }, 337.314);
		expectedHelper.put(new int[] { 13, 6 }, 344.285);
		expectedHelper.put(new int[] { 12, 6 }, 344.611);
		expectedHelper.put(new int[] { 6, 11 }, 376.242);
		expectedHelper.put(new int[] { 11, 6 }, 399.065);
		expectedHelper.put(new int[] { 7, 12 }, 432.971);
		expectedHelper.put(new int[] { 7, 13 }, 444.356);
		expectedHelper.put(new int[] { 13, 7 }, 455.008);
		expectedHelper.put(new int[] { 12, 7 }, 455.097);
		expectedHelper.put(new int[] { 13, 5 }, 465.565);
		expectedHelper.put(new int[] { 12, 5 }, 466.214);
		expectedHelper.put(new int[] { 5, 12 }, 472.839);
		expectedHelper.put(new int[] { 5, 13 }, 484.973);
		expectedHelper.put(new int[] { 7, 11 }, 493.862);
		expectedHelper.put(new int[] { 11, 7 }, 528.282);
		expectedHelper.put(new int[] { 5, 11 }, 539.274);
		expectedHelper.put(new int[] { 11, 5 }, 542.02);
		expectedHelper.put(new int[] { 1, 12 }, 560.821);
		expectedHelper.put(new int[] { 13, 1 }, 563.966);
		expectedHelper.put(new int[] { 12, 1 }, 564.192);
		expectedHelper.put(new int[] { 1, 13 }, 572.897);
		expectedHelper.put(new int[] { 1, 11 }, 637.542);
		expectedHelper.put(new int[] { 11, 1 }, 656.643);
		expectedHelper.put(new int[] { 13, 12 }, 2427.116);
		expectedHelper.put(new int[] { 12, 12 }, 2440.546);
		expectedHelper.put(new int[] { 13, 13 }, 2462.721);
		expectedHelper.put(new int[] { 12, 13 }, 2486.121);
		expectedHelper.put(new int[] { 13, 11 }, 2751.339);
		expectedHelper.put(new int[] { 12, 11 }, 2774.219);
		expectedHelper.put(new int[] { 11, 12 }, 2823.85);
		expectedHelper.put(new int[] { 11, 13 }, 2867.618);
		expectedHelper.put(new int[] { 11, 11 }, 3201.522);
		expectedHelper.put(new int[] { 5, 7 }, 87.065);
		expectedHelper.put(new int[] { 6, 1 }, 75.622);
		expectedHelper.put(new int[] { 7, 5 }, 81.793);
		expectedHelper.put(new int[] { 6, 5 }, 62.62);
		expectedHelper.put(new int[] { 7, 6 }, 60.445);
		expectedHelper.put(new int[] { 7, 1 }, 99.823);
		expectedHelper.put(new int[] { 1, 1 }, 128.944);
		expectedHelper.put(new int[] { 1, 6 }, 78.394);
		expectedHelper.put(new int[] { 5, 6 }, 66.118);
		expectedHelper.put(new int[] { 6, 6 }, 46.082);
		expectedHelper.put(new int[] { 1, 5 }, 105.867);
		expectedHelper.put(new int[] { 5, 5 }, 89.921);
		expectedHelper.put(new int[] { 7, 7 }, 79.75);
		expectedHelper.put(new int[] { 5, 1 }, 108.81);
		expectedHelper.put(new int[] { 6, 7 }, 61.263);
		Map<Pair<ICluster, ICluster>, Double> oldEE = transformClusterPValues(clusters, expectedHelper);

		// directed p-values from ticone version 1.2.75-SNAPSHOT using the old
		// edge crossover implementation
		Map<int[], Double> helper = new HashMap<>();
		helper.put(new int[] { 1, 1 }, 0.290);
		helper.put(new int[] { 1, 5 }, 0.431);
		helper.put(new int[] { 1, 6 }, 0.315);
		helper.put(new int[] { 1, 7 }, 0.000);
		helper.put(new int[] { 1, 11 }, 0.000);
		helper.put(new int[] { 1, 12 }, 0.000);
		helper.put(new int[] { 1, 13 }, 0.000);
		helper.put(new int[] { 5, 1 }, 0.584);
		helper.put(new int[] { 5, 5 }, 0.539);
		helper.put(new int[] { 5, 6 }, 0.385);
		helper.put(new int[] { 5, 7 }, 0.003);
		helper.put(new int[] { 5, 11 }, 0.000);
		helper.put(new int[] { 5, 12 }, 0.000);
		helper.put(new int[] { 5, 13 }, 0.000);
		helper.put(new int[] { 6, 1 }, 0.005);
		helper.put(new int[] { 6, 5 }, 0.098);
		helper.put(new int[] { 6, 6 }, 0.393);
		helper.put(new int[] { 6, 7 }, 0.966);
		helper.put(new int[] { 6, 11 }, 0.000);
		helper.put(new int[] { 6, 12 }, 0.000);
		helper.put(new int[] { 6, 13 }, 0.000);
		helper.put(new int[] { 7, 1 }, 0.277);
		helper.put(new int[] { 7, 5 }, 0.028);
		helper.put(new int[] { 7, 6 }, 0.159);
		helper.put(new int[] { 7, 7 }, 0.574);
		helper.put(new int[] { 7, 11 }, 0.000);
		helper.put(new int[] { 7, 12 }, 0.000);
		helper.put(new int[] { 7, 13 }, 0.000);
		helper.put(new int[] { 11, 1 }, 0.000);
		helper.put(new int[] { 11, 5 }, 0.000);
		helper.put(new int[] { 11, 6 }, 0.000);
		helper.put(new int[] { 11, 7 }, 0.000);
		helper.put(new int[] { 11, 11 }, 0.000);
		helper.put(new int[] { 11, 12 }, 0.000);
		helper.put(new int[] { 11, 13 }, 0.000);
		helper.put(new int[] { 12, 1 }, 0.000);
		helper.put(new int[] { 12, 5 }, 0.000);
		helper.put(new int[] { 12, 6 }, 0.000);
		helper.put(new int[] { 12, 7 }, 0.000);
		helper.put(new int[] { 12, 11 }, 0.000);
		helper.put(new int[] { 12, 12 }, 0.000);
		helper.put(new int[] { 12, 13 }, 0.000);
		helper.put(new int[] { 13, 1 }, 0.000);
		helper.put(new int[] { 13, 5 }, 0.000);
		helper.put(new int[] { 13, 6 }, 0.000);
		helper.put(new int[] { 13, 7 }, 0.000);
		helper.put(new int[] { 13, 11 }, 0.000);
		helper.put(new int[] { 13, 12 }, 0.000);
		helper.put(new int[] { 13, 13 }, 0.000);
		Map<Pair<ICluster, ICluster>, Double> oldPvalues = transformClusterPValues(clusters, helper);

		// compare edge counts
		IFeatureStore featureStore = ticoneConnectivityTask.calculatePvalues.getFeatureStore();
		IClusterPairFeatureNumberDirectedConnectingEdges feature = new ClusterPairFeatureNumberDirectedConnectingEdges();
		Map<IClusterPair, IFeatureValue<Double>> featureValueMap = featureStore.getFeatureValueMap(feature,
				ObjectType.CLUSTER_PAIR);
		for (IClusterPair p : featureValueMap.keySet()) {
			ClusterPair pair = new ClusterPair(p.getFirst(), p.getSecond());
			int oldCount = expectedNumberEdges.get(pair);
			int newCount = (int) featureValueMap.get(p).getValue().doubleValue();
			assertEquals(oldCount, newCount);
		}

		System.out.println("PERMUTED EDGE COUNTS and EXPECTED EDGE COUNT");
		// check out expected edge counts
		Map<IClusterPair, IntList> permutationEdgeCountsDirected = edgeCrossoverConnectivityResult
				.getPermutationEdgeCountsDirected();
		Map<IClusterPair, IntList> permutationEdgeCountsUndirected = edgeCrossoverConnectivityResult
				.getPermutationEdgeCountsUndirected();
		for (IClusterPair p : permutationEdgeCountsDirected.keySet()) {
			int[] ds = permutationEdgeCountsDirected.get(p).toIntArray();
			System.out.println(p + "\t" + ArraysExt.mean(ds) + "\t" + Arrays.toString(ds));
		}

		// compare expected edge counts
		System.out.println("##### COMPARE EXPECTED EDGE COUNT");
		for (IClusterPair p : permutationEdgeCountsDirected.keySet()) {
			int[] ds = permutationEdgeCountsDirected.get(p).toIntArray();
			double EE = ArraysExt.mean(ds);
			double oldExpected = oldEE.get(p);
			System.out.println(p + "\t" + EE + "\t" + oldExpected + "\t" + (EE - oldExpected));
		}

		Map<IClusterPair, IPvalue> pvaluesDirected = edgeCrossoverConnectivityResult.pvaluesDirected;
		System.out.println("##### P-VALUE COMPARISON");
		for (Pair<ICluster, ICluster> p : oldPvalues.keySet()) {
			double oldP = oldPvalues.get(p);
			double newP = pvaluesDirected.get(p).getDouble();
			String.format("P-value for cluster pair %s is not the same", p);
			// assertEquals(String.format("P-value for cluster pair %s is not
			// the same", p), oldP, newP, 0.05);
			// collector.checkThat(msg, newP, CoreMatchers.equalTo(oldP));
			System.out.println(p + "\t" + newP + "\t" + oldP + "\t" + (newP - oldP));
		}
	}

	private Map<Pair<ICluster, ICluster>, Double> transformClusterPValues(final Map<Integer, ICluster> clusters,
			final Map<int[], Double> helper) {
		Map<Pair<ICluster, ICluster>, Double> result = new HashMap<>();

		for (int[] clusterNumbers : helper.keySet()) {
			result.put(Pair.of(clusters.get(clusterNumbers[0]), clusters.get(clusterNumbers[1])),
					helper.get(clusterNumbers));
		}

		return result;
	}

	private Map<Pair<ICluster, ICluster>, Integer> transformClusterEdgeCounts(final Map<Integer, ICluster> clusters,
			final List<int[]> helper) {
		Map<Pair<ICluster, ICluster>, Integer> result = new HashMap<>();

		for (int[] clusterNumbers : helper) {
			result.put(Pair.of(clusters.get(clusterNumbers[0]), clusters.get(clusterNumbers[1])), clusterNumbers[2]);
		}

		return result;
	}

//	@Test
	public void testCompareUndirectedTwoSidedEdgeCrossoverToOldImplementation() throws Exception {
		String networkPath = "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs-randomEdgeProb-seed42-samePatternEdgeProb0.9_randomClusterToHaveEqualNodeDegrees.txt";
		String clusteringPath = "/test_files/clustering/2017_05_07_seed42_clustering.txt";
		ClusterObjectMapping clustering = parseFromFile(new File(clusteringPath), 1, 0,
				new PearsonCorrelationFunction());

		Map<Integer, ICluster> clusters = new HashMap<>();
		for (ICluster c : clustering.getClusters())
			clusters.put(c.getClusterNumber(), c);

		// directed edge counts from ticone version 1.2.75-SNAPSHOT using the
		// old edge crossover implementation
		List<int[]> edgeCountHelper = new ArrayList<>();
		edgeCountHelper.add(new int[] { 11, 12, 7237 });
		edgeCountHelper.add(new int[] { 12, 11, 7237 });
		edgeCountHelper.add(new int[] { 11, 13, 3955 });
		edgeCountHelper.add(new int[] { 13, 11, 3955 });
		edgeCountHelper.add(new int[] { 11, 11, 3855 });
		edgeCountHelper.add(new int[] { 13, 13, 3664 });
		edgeCountHelper.add(new int[] { 12, 12, 3659 });
		edgeCountHelper.add(new int[] { 1, 13, 1878 });
		edgeCountHelper.add(new int[] { 13, 1, 1878 });
		edgeCountHelper.add(new int[] { 12, 13, 1731 });
		edgeCountHelper.add(new int[] { 13, 12, 1731 });
		edgeCountHelper.add(new int[] { 5, 13, 1586 });
		edgeCountHelper.add(new int[] { 13, 5, 1586 });
		edgeCountHelper.add(new int[] { 7, 13, 1518 });
		edgeCountHelper.add(new int[] { 13, 7, 1518 });
		edgeCountHelper.add(new int[] { 6, 13, 1130 });
		edgeCountHelper.add(new int[] { 13, 6, 1130 });
		edgeCountHelper.add(new int[] { 1, 11, 894 });
		edgeCountHelper.add(new int[] { 11, 1, 894 });
		edgeCountHelper.add(new int[] { 1, 12, 845 });
		edgeCountHelper.add(new int[] { 12, 1, 845 });
		edgeCountHelper.add(new int[] { 5, 11, 743 });
		edgeCountHelper.add(new int[] { 11, 5, 743 });
		edgeCountHelper.add(new int[] { 7, 11, 710 });
		edgeCountHelper.add(new int[] { 11, 7, 710 });
		edgeCountHelper.add(new int[] { 5, 12, 701 });
		edgeCountHelper.add(new int[] { 12, 5, 701 });
		edgeCountHelper.add(new int[] { 7, 12, 666 });
		edgeCountHelper.add(new int[] { 12, 7, 666 });
		edgeCountHelper.add(new int[] { 6, 11, 544 });
		edgeCountHelper.add(new int[] { 11, 6, 544 });
		edgeCountHelper.add(new int[] { 6, 12, 520 });
		edgeCountHelper.add(new int[] { 12, 6, 520 });
		edgeCountHelper.add(new int[] { 1, 5, 203 });
		edgeCountHelper.add(new int[] { 5, 1, 203 });
		edgeCountHelper.add(new int[] { 1, 7, 161 });
		edgeCountHelper.add(new int[] { 7, 1, 161 });
		edgeCountHelper.add(new int[] { 1, 1, 139 });
		edgeCountHelper.add(new int[] { 5, 7, 128 });
		edgeCountHelper.add(new int[] { 7, 5, 128 });
		edgeCountHelper.add(new int[] { 1, 6, 127 });
		edgeCountHelper.add(new int[] { 6, 1, 127 });
		edgeCountHelper.add(new int[] { 5, 6, 112 });
		edgeCountHelper.add(new int[] { 6, 5, 112 });
		edgeCountHelper.add(new int[] { 6, 7, 112 });
		edgeCountHelper.add(new int[] { 7, 6, 112 });
		edgeCountHelper.add(new int[] { 5, 5, 95 });
		edgeCountHelper.add(new int[] { 7, 7, 84 });
		edgeCountHelper.add(new int[] { 6, 6, 41 });

		Map<Pair<ICluster, ICluster>, Integer> expectedNumberEdges = transformClusterEdgeCounts(clusters,
				edgeCountHelper);

		// expected edge counts from ticone version 1.2.75-SNAPSHOT using the
		// old
		// edge crossover implementation
		Map<int[], Double> expectedHelper = new HashMap<>();
		expectedHelper.put(new int[] { 11, 12 }, 5593.117);
		expectedHelper.put(new int[] { 12, 11 }, 5593.117);
		expectedHelper.put(new int[] { 11, 13 }, 5585.452);
		expectedHelper.put(new int[] { 13, 11 }, 5585.452);
		expectedHelper.put(new int[] { 11, 11 }, 3201.309);
		expectedHelper.put(new int[] { 13, 13 }, 2461.768);
		expectedHelper.put(new int[] { 12, 12 }, 2449.049);
		expectedHelper.put(new int[] { 1, 13 }, 1149.235);
		expectedHelper.put(new int[] { 13, 1 }, 1149.235);
		expectedHelper.put(new int[] { 12, 13 }, 4911.573);
		expectedHelper.put(new int[] { 13, 12 }, 4911.573);
		expectedHelper.put(new int[] { 5, 13 }, 959.734);
		expectedHelper.put(new int[] { 13, 5 }, 959.734);
		expectedHelper.put(new int[] { 7, 13 }, 907.998);
		expectedHelper.put(new int[] { 13, 7 }, 907.998);
		expectedHelper.put(new int[] { 6, 13 }, 688.472);
		expectedHelper.put(new int[] { 13, 6 }, 688.472);
		expectedHelper.put(new int[] { 1, 11 }, 1306.628);
		expectedHelper.put(new int[] { 11, 1 }, 1306.628);
		expectedHelper.put(new int[] { 1, 12 }, 1121.199);
		expectedHelper.put(new int[] { 12, 1 }, 1121.199);
		expectedHelper.put(new int[] { 5, 11 }, 1091.442);
		expectedHelper.put(new int[] { 11, 5 }, 1091.442);
		expectedHelper.put(new int[] { 7, 11 }, 1031.785);
		expectedHelper.put(new int[] { 11, 7 }, 1031.785);
		expectedHelper.put(new int[] { 5, 12 }, 936.358);
		expectedHelper.put(new int[] { 12, 5 }, 936.358);
		expectedHelper.put(new int[] { 7, 12 }, 885.714);
		expectedHelper.put(new int[] { 12, 7 }, 885.714);
		expectedHelper.put(new int[] { 6, 11 }, 781.958);
		expectedHelper.put(new int[] { 11, 6 }, 781.958);
		expectedHelper.put(new int[] { 6, 12 }, 671.941);
		expectedHelper.put(new int[] { 12, 6 }, 671.941);
		expectedHelper.put(new int[] { 1, 5 }, 208.699);
		expectedHelper.put(new int[] { 5, 1 }, 208.699);
		expectedHelper.put(new int[] { 1, 7 }, 197.565);
		expectedHelper.put(new int[] { 7, 1 }, 197.565);
		expectedHelper.put(new int[] { 1, 1 }, 126.15);
		expectedHelper.put(new int[] { 5, 7 }, 164.52);
		expectedHelper.put(new int[] { 7, 5 }, 164.52);
		expectedHelper.put(new int[] { 1, 6 }, 150.374);
		expectedHelper.put(new int[] { 6, 1 }, 150.374);
		expectedHelper.put(new int[] { 5, 6 }, 125.001);
		expectedHelper.put(new int[] { 6, 5 }, 125.001);
		expectedHelper.put(new int[] { 6, 7 }, 118.232);
		expectedHelper.put(new int[] { 7, 6 }, 118.232);
		expectedHelper.put(new int[] { 5, 5 }, 88.623);
		expectedHelper.put(new int[] { 7, 7 }, 78.593);
		expectedHelper.put(new int[] { 6, 6 }, 45.511);

		transformClusterPValues(clusters, expectedHelper);

		// undirected p-values from ticone version 1.2.75-SNAPSHOT using the old
		// edge crossover implementation
		Map<int[], Double> helper = new HashMap<>();
		helper.put(new int[] { 11, 11 }, 0.0);
		helper.put(new int[] { 11, 12 }, 0.0);
		helper.put(new int[] { 11, 13 }, 0.0);
		helper.put(new int[] { 1, 1 }, 0.113);
		helper.put(new int[] { 1, 5 }, 0.584);
		helper.put(new int[] { 1, 7 }, 0.002);
		helper.put(new int[] { 1, 6 }, 0.01);
		helper.put(new int[] { 1, 12 }, 0.0);
		helper.put(new int[] { 1, 11 }, 0.0);
		helper.put(new int[] { 1, 13 }, 0.0);
		helper.put(new int[] { 12, 12 }, 0.0);
		helper.put(new int[] { 12, 13 }, 0.0);
		helper.put(new int[] { 13, 13 }, 0.0);
		helper.put(new int[] { 5, 5 }, 0.367);
		helper.put(new int[] { 5, 7 }, 0.0);
		helper.put(new int[] { 5, 6 }, 0.114);
		helper.put(new int[] { 5, 12 }, 0.0);
		helper.put(new int[] { 5, 11 }, 0.0);
		helper.put(new int[] { 5, 13 }, 0.0);
		helper.put(new int[] { 6, 6 }, 0.373);
		helper.put(new int[] { 6, 7 }, 0.438);
		helper.put(new int[] { 6, 13 }, 0.0);
		helper.put(new int[] { 6, 11 }, 0.0);
		helper.put(new int[] { 6, 12 }, 0.0);
		helper.put(new int[] { 7, 7 }, 0.407);
		helper.put(new int[] { 7, 13 }, 0.0);
		helper.put(new int[] { 7, 12 }, 0.0);
		helper.put(new int[] { 7, 11 }, 0.0);

		Map<Pair<ICluster, ICluster>, Double> oldPvalues = transformClusterPValues(clusters, helper);

		TiconeNetworkImpl network = TiconeNetworkImpl.parseFromFile("seed42", false, new File(networkPath));

		int numberPermutations = 1000;
		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, clustering, clustering.getClusters(), numberPermutations, true,
				new ShuffleNetworkWithEdgeCrossover(true));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		ticoneConnectivityTask.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(network);
		ticoneConnectivityTask.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(network);
		ConnectivityPValueResult edgeCrossoverConnectivityResult = ticoneConnectivityTask.calculatePValues(network);

		// compare edge counts
		IFeatureStore featureStore = ticoneConnectivityTask.calculatePvalues.getFeatureStore();
		ClusterPairFeatureNumberUndirectedConnectingEdges feature = new ClusterPairFeatureNumberUndirectedConnectingEdges();
		Map<IClusterPair, IFeatureValue<Double>> featureValueMap = featureStore.getFeatureValueMap(feature,
				ObjectType.CLUSTER_PAIR);
		for (IClusterPair p : featureValueMap.keySet()) {
			IClusterPair pair = new ClusterPair(p.getFirst(), p.getSecond());
			int oldCount = expectedNumberEdges.get(pair);
			int newCount = (int) featureStore.getFeatureValue(p, feature).getValue().doubleValue();
			assertEquals(oldCount, newCount);
		}

		// System.out.println("PERMUTED EDGE COUNTS and EXPECTED EDGE COUNT");
		// // check out expected edge counts
		// Map<IClusterPair, Map<INumberFeature<IClusterPair>,
		// double[]>> permutedFeatureValuesObjectSpecific =
		// ticoneConnectivityTask.calculatePvalues
		// .getPermutedFeatureValuesObjectSpecific();
		// for (IClusterPair p : permutedFeatureValuesObjectSpecific.keySet()) {
		// double[] ds =
		// permutedFeatureValuesObjectSpecific.get(p).get(feature);
		// System.out.println(p + "\t" + ArraysExt.mean(ds) + "\t" +
		// Arrays.toString(ds));
		// }

		// // compare expected edge counts
		// System.out.println("##### COMPARE EXPECTED EDGE COUNT");
		// for (IClusterPair p : permutedFeatureValuesObjectSpecific.keySet()) {
		// double[] ds =
		// permutedFeatureValuesObjectSpecific.get(p).get(feature);
		// double EE = ArraysExt.mean(ds);
		// double oldExpected = oldEE.get(Pair.of(p.getFirst(), p.getSecond()));
		// System.out.println(p + "\t" + (EE - oldExpected));
		// }

		Map<IClusterPair, IPvalue> pvaluesDirected = edgeCrossoverConnectivityResult.pvaluesUndirected;
		System.out.println("##### P-VALUE COMPARISON");
		for (Pair<ICluster, ICluster> p : oldPvalues.keySet()) {
			double oldP = oldPvalues.get(p);
			double newP = pvaluesDirected.get(p).getDouble();
			String msg = String.format("P-value for cluster pair %s is not the same", p);
			System.out.println(p + "\t" + newP + "\t" + oldP + "\t" + (newP - oldP));
			assertEquals(String.format(msg), oldP, newP, 0.07);
		}
	}

//	@Test
	public void testMergeClustersAfterConnectivityClustering1() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		new ShuffleDatasetRowwise();
		int numberOfInitialClusters = 10;
		Scanner scan = new Scanner(new File(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5_mergeTestCase.txt"));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();
		IClusterObjectMapping clustering = bla.doIteration();

		Map<Integer, List<String>> clusterToObjectIds = new HashMap<>();
		for (ICluster c : clustering.getClusters()) {
			List<String> ids = c.getObjects().stream().map(o -> o.getName()).collect(Collectors.toList());
			clusterToObjectIds.put(c.getClusterNumber(), ids);
		}

		Map<Integer, List<String>> expectedObjectIds = new HashMap<>();
		expectedObjectIds.put(1,
				Arrays.asList("obj81", "obj85", "obj94", "obj105", "obj109", "obj112", "obj129", "obj130", "obj132",
						"obj133", "obj134", "obj136", "obj137", "obj141", "obj146", "obj149", "obj150", "obj241",
						"obj276", "obj289", "obj294", "obj308", "obj311", "obj324", "obj342", "obj345", "obj350",
						"obj351"));
		expectedObjectIds.put(2,
				Arrays.asList("obj245", "obj248", "obj251", "obj254", "obj271", "obj283", "obj284", "obj287", "obj290",
						"obj297", "obj298", "obj301", "obj306", "obj307", "obj314", "obj316", "obj327", "obj330",
						"obj339", "obj340", "obj341", "obj344", "obj357"));
		expectedObjectIds.put(3, Arrays.asList("obj266", "obj269", "obj273", "obj275", "obj285", "obj299", "obj303",
				"obj317", "obj318", "obj322", "obj323", "obj347", "obj354", "obj355", "obj358"));
		expectedObjectIds.put(4,
				Arrays.asList("obj0", "obj1", "obj2", "obj4", "obj5", "obj6", "obj7", "obj8", "obj9", "obj10", "obj11",
						"obj12", "obj13", "obj14", "obj16", "obj17", "obj18", "obj19", "obj20", "obj21", "obj23",
						"obj24", "obj25", "obj28", "obj29", "obj30", "obj31", "obj32", "obj33", "obj35", "obj36",
						"obj37", "obj38", "obj39", "obj40", "obj43", "obj45", "obj46", "obj47", "obj48", "obj49",
						"obj50", "obj51", "obj52", "obj54", "obj55", "obj56", "obj57", "obj58", "obj59", "obj60",
						"obj62", "obj63", "obj66", "obj67", "obj68", "obj69", "obj70", "obj71", "obj72", "obj73",
						"obj74", "obj75", "obj77", "obj78", "obj79", "obj243", "obj246", "obj257", "obj260", "obj261",
						"obj264", "obj292", "obj296", "obj312"));
		expectedObjectIds.put(5, Arrays.asList("obj80", "obj82", "obj83", "obj84", "obj87", "obj88", "obj89", "obj90",
				"obj91", "obj92", "obj93", "obj95", "obj96", "obj97", "obj98", "obj99", "obj100", "obj101", "obj102",
				"obj103", "obj106", "obj107", "obj108", "obj110", "obj111", "obj113", "obj114", "obj115", "obj116",
				"obj117", "obj118", "obj119", "obj121", "obj123", "obj124", "obj125", "obj126", "obj127", "obj128",
				"obj131", "obj135", "obj139", "obj140", "obj142", "obj143", "obj144", "obj147", "obj148", "obj152",
				"obj153", "obj154", "obj155", "obj156", "obj157", "obj158", "obj159", "obj247", "obj329", "obj359"));
		expectedObjectIds.put(6, Arrays.asList("obj242", "obj249", "obj256", "obj262", "obj277", "obj280", "obj288",
				"obj291", "obj300", "obj302", "obj313", "obj321", "obj332", "obj335", "obj343", "obj349"));
		expectedObjectIds.put(7,
				Arrays.asList("obj161", "obj162", "obj163", "obj165", "obj166", "obj167", "obj169", "obj170", "obj171",
						"obj172", "obj173", "obj174", "obj175", "obj176", "obj177", "obj178", "obj179", "obj180",
						"obj181", "obj182", "obj183", "obj185", "obj186", "obj187", "obj188", "obj189", "obj190",
						"obj191", "obj193", "obj194", "obj197", "obj198", "obj199", "obj200", "obj202", "obj203",
						"obj206", "obj207", "obj208", "obj209", "obj210", "obj212", "obj213", "obj215", "obj216",
						"obj217", "obj218", "obj219", "obj220", "obj221", "obj222", "obj223", "obj224", "obj225",
						"obj227", "obj229", "obj230", "obj232", "obj233", "obj234", "obj235", "obj236", "obj237",
						"obj238", "obj255", "obj265", "obj272", "obj274", "obj333", "obj336", "obj337", "obj346"));
		expectedObjectIds.put(8,
				Arrays.asList("obj86", "obj104", "obj120", "obj122", "obj138", "obj145", "obj151", "obj240", "obj244",
						"obj250", "obj252", "obj253", "obj258", "obj268", "obj315", "obj319", "obj320", "obj325",
						"obj326", "obj353", "obj356"));
		expectedObjectIds.put(9,
				Arrays.asList("obj160", "obj164", "obj168", "obj184", "obj192", "obj195", "obj196", "obj201", "obj204",
						"obj205", "obj211", "obj214", "obj226", "obj228", "obj231", "obj239", "obj263", "obj270",
						"obj278", "obj293", "obj309", "obj310", "obj328", "obj334"));
		expectedObjectIds.put(10,
				Arrays.asList("obj3", "obj15", "obj22", "obj26", "obj27", "obj34", "obj41", "obj42", "obj44", "obj53",
						"obj61", "obj64", "obj65", "obj76", "obj259", "obj267", "obj279", "obj281", "obj282", "obj286",
						"obj295", "obj304", "obj305", "obj331", "obj338", "obj348", "obj352"));

		assertEquals(expectedObjectIds, clusterToObjectIds);
	}

	@Test
	public void testMergeClustersAfterConnectivityRemoveObjectsNotInNetwork() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 10;
		int numberOfRefinementPermutations = 100;

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5_mergeTestCase.txt")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		int expectedNumberClusters = numberOfInitialClusters;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		List<IArithmeticFeature<? extends Comparable<?>>> featureList = new ArrayList<IArithmeticFeature<? extends Comparable<?>>>();
		featureList.add(new ClusterFeatureNumberObjects());
		featureList.add(new ClusterFeatureAverageSimilarity(similarityFunction));

		// parse network
		Scanner scanner = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5-seed42-samePatternEdgeProb0.0.txt")
				.getFile()));
		TiconeNetworkImpl network = NetworkUtil.parseNetworkFromScanner(new TiconeNetworkImplFactory(), scanner, "bla",
				null, false);

		TiconeClusterConnectivityTask ticoneConnectivityTask = new TiconeClusterConnectivityTask(network,
				clusteringResult, bla.getLatestClustering(), bla.getLatestClustering().getClusters(),
				numberOfRefinementPermutations, false,
				new ShuffleFirstClusteringOfClusteringPair(new ShuffleClusteringRandomlyWithSameNumberClusters()));

		network.initNodeDegreesUndirected();
		ticoneConnectivityTask.initEntrezToNetworks();

		ticoneConnectivityTask.removeObjectsNotInNetwork(network);
		ticoneConnectivityTask.removeNodesNotInDataset(network);

		List<String> objIds = new ArrayList<>();
		for (ITimeSeriesObject o : bla.getLatestClustering().getAssignedObjects())
			objIds.add(o.getName());

		List<String> expectedObjIds = Arrays.asList("obj81", "obj85", "obj94", "obj105", "obj109", "obj112", "obj129",
				"obj130", "obj132", "obj133", "obj134", "obj136", "obj137", "obj141", "obj146", "obj149", "obj150",
				"obj241", "obj276", "obj289", "obj294", "obj308", "obj311", "obj324", "obj342", "obj345", "obj350",
				"obj351", "obj245", "obj248", "obj251", "obj254", "obj271", "obj283", "obj284", "obj287", "obj290",
				"obj297", "obj298", "obj301", "obj306", "obj307", "obj314", "obj316", "obj327", "obj330", "obj339",
				"obj340", "obj341", "obj344", "obj357", "obj266", "obj269", "obj273", "obj275", "obj285", "obj299",
				"obj303", "obj317", "obj318", "obj322", "obj323", "obj347", "obj354", "obj355", "obj358", "obj0",
				"obj1", "obj2", "obj4", "obj5", "obj6", "obj7", "obj8", "obj9", "obj10", "obj11", "obj12", "obj13",
				"obj14", "obj16", "obj17", "obj18", "obj19", "obj20", "obj21", "obj23", "obj24", "obj25", "obj28",
				"obj29", "obj30", "obj31", "obj32", "obj33", "obj35", "obj36", "obj37", "obj38", "obj39", "obj40",
				"obj43", "obj45", "obj46", "obj47", "obj48", "obj49", "obj50", "obj51", "obj52", "obj54", "obj55",
				"obj56", "obj57", "obj58", "obj59", "obj60", "obj62", "obj63", "obj66", "obj67", "obj68", "obj69",
				"obj70", "obj71", "obj72", "obj73", "obj74", "obj75", "obj77", "obj78", "obj79", "obj243", "obj246",
				"obj257", "obj260", "obj261", "obj264", "obj292", "obj296", "obj312", "obj80", "obj82", "obj83",
				"obj84", "obj87", "obj88", "obj89", "obj90", "obj91", "obj92", "obj93", "obj95", "obj96", "obj97",
				"obj98", "obj99", "obj100", "obj101", "obj102", "obj103", "obj106", "obj107", "obj108", "obj110",
				"obj111", "obj113", "obj114", "obj115", "obj116", "obj117", "obj118", "obj119", "obj121", "obj123",
				"obj124", "obj125", "obj126", "obj127", "obj128", "obj131", "obj135", "obj139", "obj140", "obj142",
				"obj143", "obj144", "obj147", "obj148", "obj152", "obj153", "obj154", "obj155", "obj156", "obj157",
				"obj158", "obj159", "obj247", "obj329", "obj359", "obj242", "obj249", "obj256", "obj262", "obj277",
				"obj280", "obj288", "obj291", "obj300", "obj302", "obj313", "obj321", "obj332", "obj335", "obj343",
				"obj349", "obj161", "obj162", "obj163", "obj165", "obj166", "obj167", "obj169", "obj170", "obj171",
				"obj172", "obj173", "obj174", "obj175", "obj176", "obj177", "obj178", "obj179", "obj180", "obj181",
				"obj182", "obj183", "obj185", "obj186", "obj187", "obj188", "obj189", "obj190", "obj191", "obj193",
				"obj194", "obj197", "obj198", "obj199", "obj200", "obj202", "obj203", "obj206", "obj207", "obj208",
				"obj209", "obj210", "obj212", "obj213", "obj215", "obj216", "obj217", "obj218", "obj219", "obj220",
				"obj221", "obj222", "obj223", "obj224", "obj225", "obj227", "obj229", "obj230", "obj232", "obj233",
				"obj234", "obj235", "obj236", "obj237", "obj238", "obj255", "obj265", "obj272", "obj274", "obj333",
				"obj336", "obj337", "obj346", "obj86", "obj104", "obj120", "obj122", "obj138", "obj145", "obj151",
				"obj240", "obj244", "obj250", "obj252", "obj253", "obj258", "obj268", "obj315", "obj319", "obj320",
				"obj325", "obj326", "obj353", "obj356", "obj160", "obj164", "obj168", "obj184", "obj192", "obj195",
				"obj196", "obj201", "obj204", "obj205", "obj211", "obj214", "obj226", "obj228", "obj231", "obj239",
				"obj263", "obj270", "obj278", "obj293", "obj309", "obj310", "obj328", "obj334", "obj3", "obj15",
				"obj22", "obj26", "obj27", "obj34", "obj41", "obj42", "obj44", "obj53", "obj61", "obj64", "obj65",
				"obj76", "obj259", "obj267", "obj279", "obj281", "obj282", "obj286", "obj295", "obj304", "obj305",
				"obj331", "obj338", "obj348", "obj352");

		assertEquals(new HashSet<>(expectedObjIds), new HashSet<>(objIds));
	}
}
