package dk.sdu.imada.ticone.splitpattern;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Random;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.Cluster;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.PAMKClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.PseudoClusteringMethodOneCluster;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterBasedOnTwoMostDissimilarObjects;
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterContainer;
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterWithClustering;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;

/**
 * Created by christian on 16-11-15.
 */
public class TestSplitPattern {

	double[][] timeSeriesData = new double[][] { { 0, -2, 1, -2, 3 }, { 0, -1, 0.5, -1.5, 2.5 },
			{ 0, -1.2, 1.3, -1.7, 2.8 }, { 0, -1.7, 0.8, -1.8, 2.4 }, { 0, 1, 2, 0, -2 }, { 0, 1.5, 2.1, 1, -3 },
			{ 0, 1.2, 1.9, -0.5, -1.5 }, { 0, 2, 2.5, 1, -2 } };

	private ClusterObjectMapping createTempMapping() throws Exception {

		PrototypeBuilder prototypeFactory = new PrototypeBuilder().setPrototypeComponentBuilders(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(-3, 3, 3, 3)));

		IClusteringMethod<ClusterObjectMapping> clustering = new PseudoClusteringMethodOneCluster(prototypeFactory,
				new PearsonCorrelationFunction(), 5, 0.5);
		ITimeSeriesObjectList timeSeriesDataList = new TimeSeriesObjectList();
		for (int i = 0; i < timeSeriesData.length; i++) {
			ITimeSeriesObject tsd = new TimeSeriesObject("obj" + i, 1);
			tsd.addPreprocessedTimeSeries(0, TimeSeries.of(timeSeriesData[i]));
			timeSeriesDataList.add(tsd);
		}
		ClusterObjectMapping patternObjectMapping = clustering.findClusters(timeSeriesDataList, 2, 42);
		return patternObjectMapping;
	}

	@Test
	public void TestSplitPatternBasedOnTwoMostDissimilarObjects() throws Exception {
		ClusterObjectMapping patternObjectMapping = createTempMapping();

		PrototypeBuilder prototypeFactory = new PrototypeBuilder().setPrototypeComponentBuilders(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(-3, 3, 3, 3)));

		SplitClusterBasedOnTwoMostDissimilarObjects splitPattern = new SplitClusterBasedOnTwoMostDissimilarObjects(
				new PearsonCorrelationFunction(), prototypeFactory);
		ICluster patternToSplit = patternObjectMapping.getClusters().toArray(new Cluster[0])[0];
		SplitClusterContainer splitPatternContainer = splitPattern.splitCluster(patternToSplit);

		assertEquals("Old pattern test if equals to splitted pattern", patternToSplit,
				splitPatternContainer.getOldPattern());

		ClusterObjectMapping newClustering = splitPatternContainer.getNewClusters();

		int count = newClustering.getClusters().size();

		assertEquals("Test if new mappings contains 2 patterns", 2, count);

		ITimeSeriesObjects candidates = splitPatternContainer.getCandidates();
		ITimeSeriesObjects expectedCandidates = new TimeSeriesObjectList();
		for (ITimeSeriesObject obj : patternObjectMapping.getAssignedObjects()) {
			if (obj.getName().equals("obj1") || obj.getName().equals("obj5"))
				expectedCandidates.add(obj);
		}
		assertEquals(new HashSet<>(expectedCandidates), new HashSet<>(candidates));

		splitPattern.setClustering(patternObjectMapping);
		IClusterObjectMapping newPom = splitPattern.applyNewClusters(splitPatternContainer);
		// TODO: think of how to validate this one
	}

	@Test
	public void TestSplitPatternBasedOnNewClustering() throws Exception {
		ClusterObjectMapping patternObjectMapping = createTempMapping();
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(-3, 3, 3,
				3);
		Random random = new Random(42);

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePattern));

		SplitClusterWithClustering splitPattern = new SplitClusterWithClustering(2, new PAMKClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory), 42);

		ICluster patternToSplit = patternObjectMapping.getClusters().toArray(new Cluster[0])[0];

		SplitClusterContainer splitPatternContainer = splitPattern.splitCluster(patternToSplit);

		assertEquals("Old pattern test if equals to splitted pattern", patternToSplit,
				splitPatternContainer.getOldPattern());

		ClusterObjectMapping newClustering = splitPatternContainer.getNewClusters();

		int count = newClustering.getClusters().size();

		assertEquals("Test if new mappings contains 2 patterns", 2, count);

		ITimeSeriesObjects candidates = splitPatternContainer.getCandidates();
		assertEquals(null, candidates);

		splitPattern.setClustering(patternObjectMapping);
		IClusterObjectMapping newPom = splitPattern.applyNewClusters(splitPatternContainer);
		// TODO: think of how to validate this one
	}
}
