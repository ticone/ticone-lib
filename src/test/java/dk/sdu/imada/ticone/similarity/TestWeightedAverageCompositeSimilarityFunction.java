/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import de.wiwie.wiutils.utils.ArraysExt;
import de.wiwie.wiutils.utils.Pair;
import dk.sdu.imada.ticone.clustering.BasicClusteringProcessBuilder;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethod;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterUtility;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResultFactory;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.ObjectClusterPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.ObjectPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.scale.DummyScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.feature.scale.MinMaxScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.QuantileScalerBuilder;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkEdgeImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.statistics.RandomlyAssignObjectsToClusters;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 26, 2018
 *
 */
public class TestWeightedAverageCompositeSimilarityFunction {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testOnlyPearsonEqualObjects() throws Exception {
		PearsonCorrelationFunction p = new PearsonCorrelationFunction();
		ICompositeSimilarityFunction f = new WeightedAverageCompositeSimilarityFunction(p);

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		assertEquals(1.0, f.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get(), 0.0);
	}

	@Test
	public void testOnlyPearsonTwoDifferentObjects() throws Exception {
		PearsonCorrelationFunction p = new PearsonCorrelationFunction();
		ICompositeSimilarityFunction f = new WeightedAverageCompositeSimilarityFunction(p);

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		assertEquals(0.0, f.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get(), 0.0);
	}

	@Test
	public void testTwoPearsonEqualObjects() throws Exception {
		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction(), p2 = new PearsonCorrelationFunction();
		ICompositeSimilarityFunction f = new WeightedAverageCompositeSimilarityFunction(p1, p2);

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ICompositeSimilarityValue v = f.calculateSimilarity(new ObjectPair(tsd1, tsd2));

		assertEquals(1.0, v.calculate().get(), 0.0);
		ISimpleSimilarityValue[] expected = new ISimpleSimilarityValue[] { p1.value(1.0, null), p2.value(1.0, null) };
		ISimpleSimilarityValue[] observed = v.getChildValues();
		assertArrayEquals(expected, observed);
	}

	@Test
	public void testTwoPearsonDifferentObjects() throws Exception {
		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction(), p2 = new PearsonCorrelationFunction();
		ICompositeSimilarityFunction f = new WeightedAverageCompositeSimilarityFunction(p1, p2);

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ICompositeSimilarityValue v = f.calculateSimilarity(new ObjectPair(tsd1, tsd2));

		assertEquals(0.0, v.calculate().get(), 0.0);
		ISimpleSimilarityValue[] expected = new ISimpleSimilarityValue[] { p1.value(0.0, null), p2.value(0.0, null) };
		ISimpleSimilarityValue[] observed = v.getChildValues();
		assertArrayEquals(expected, observed);
	}

	@Test
	public void testNegativeEuclideanEqualObjects() throws Exception {
		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		InverseEuclideanSimilarityFunction p2 = new InverseEuclideanSimilarityFunction();
		ICompositeSimilarityFunction f = new WeightedAverageCompositeSimilarityFunction(p1, p2);

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ICompositeSimilarityValue v = f.calculateSimilarity(new ObjectPair(tsd1, tsd2));

		assertEquals((1.0 + 0.0) / 2, v.calculate().get(), 0.0);
		ISimpleSimilarityValue[] expected = new ISimpleSimilarityValue[] { p1.value(1.0, null), p2.value(0.0, null) };
		ISimpleSimilarityValue[] observed = v.getChildValues();
		assertArrayEquals(expected, observed);
	}

	@Test
	public void testNegativeEuclideanAndPearsonEqualObjectsWithScalers() throws Exception {
		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		p1.setNormalizeByNumberTimepoints(false);
		InverseEuclideanSimilarityFunction p2 = new InverseEuclideanSimilarityFunction();
		p2.setNormalizeByNumberTimepoints(false);
		ICompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, p2);
		simFunc.initialize();

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(p1);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(p2);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42).build(),
				s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42).build();
		simFunc.setScalerForAnyPair(0, s1);
		simFunc.setScalerForAnyPair(1, s2);

		ICompositeSimilarityValue v = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd2));
		double value = v.calculate().get();

		assertEquals((1.0 + 1.0) / 2, value, 0.0);
		ISimpleSimilarityValue[] expected = new ISimpleSimilarityValue[] { p1.value(1.0, null), p2.value(0.0, null) };
		ISimpleSimilarityValue[] observed = v.getChildValues();
		assertArrayEquals(expected, observed);
	}

//	@Test
//	public void testNegativeEuclideanAndPearsonEqualObjectsWithScalers2() throws SimilarityCalculationException,
//			MultipleTimeSeriesSignalsForSameSampleParseException, CreateInstanceFactoryException {
//		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
//		NegativeEuclideanSimilarityFunction p2 = new NegativeEuclideanSimilarityFunction();
//		ICompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, p2);
//
//		double[] data = new double[] { 0, 1, 2, 3, 4 };
//		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
//		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
//		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
//		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));
//
//		data = new double[] { 0, 1, 2, 3, 4 };
//		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
//		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
//		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
//		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));
//
//		data = new double[] { 4, 3, 2, 1, 0 };
//		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
//		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
//		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
//		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));
//
//		ITimeSeriesObjects ds = new TimeSeriesObjectList();
//		ds.add(tsd1);
//		ds.add(tsd2);
//		ds.add(tsd3);
//
//		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(p1);
//		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(p2);
//
//		MinMaxScalerFactory<Double, ?, ?>.MinMaxScaler s1 = new MinMaxScalerFactory<>(f1, ds).createInstance(),
//				s2 = new MinMaxScalerFactory<>(f2, ds).createInstance();
//		simFunc.setScaler(p1, s1);
//		simFunc.setScaler(p2, s2);
//
//		ICompositeSimilarityValue v1 = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd2));
//		ICompositeSimilarityValue v2 = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd2));
//		double value = v1.calculate().get();
//
//		assertTrue(Double.isNaN(value));
//		ISimpleSimilarityValue[] expected = new HashMap<>(), observed = v1.getChildValues();
//		expected.put(p1, p1.value(1.0));
//		expected.put(p2, p2.value(0.0));
//		assertEquals(expected, observed);
//	}

	@Test
	public void testNegativeEuclideanAndPearsonDifferentObjectsWithScalers3() throws Exception {
		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		p1.setNormalizeByNumberTimepoints(false);
		InverseEuclideanSimilarityFunction p2 = new InverseEuclideanSimilarityFunction();
		p2.setNormalizeByNumberTimepoints(false);
		ICompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, p2);
		simFunc.initialize();

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(p1);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(p2);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42).build(),
				s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42).build();
		simFunc.setScalerForAnyPair(0, s1);
		simFunc.setScalerForAnyPair(1, s2);

		ICompositeSimilarityValue v1 = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd2));
		ICompositeSimilarityValue v2 = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd3));
		ICompositeSimilarityValue v3 = simFunc.calculateSimilarity(new ObjectPair(tsd2, tsd3));
		double value1 = v1.get();
		double value2 = v2.get();
		double value3 = v3.get();
//
//		assertEquals(0.0, s1.getMin(), 0.0);
//		assertEquals(1.0, s1.getMax(), 0.0);
//
//		assertEquals(-6.324555320336759, s2.getMin(), 0.0);
//		assertEquals(0.0, s2.getMax(), 0.0);

		assertEquals(1.0, value1, 0.0);
		assertEquals(0.0, value2, 0.0);
		assertEquals(0.0, value3, 0.0);
		ISimpleSimilarityValue[] expected = new ISimpleSimilarityValue[] { p1.value(1.0, null), p2.value(0.0, null) };
		ISimpleSimilarityValue[] observed = v1.getChildValues();
		assertArrayEquals(expected, observed);
	}

	@Test
	public void testNegativeEuclideanAndPearsonDifferentObjectsWithScalers4() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		pearson.setNormalizeByNumberTimepoints(false);
		InverseEuclideanSimilarityFunction euclidean = new InverseEuclideanSimilarityFunction();
		euclidean.setNormalizeByNumberTimepoints(false);
		ICompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(pearson, euclidean);
		simFunc.initialize();

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(pearson);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(euclidean);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42).build(),
				s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42).build();
		simFunc.setScalerForAnyPair(0, s1);
		simFunc.setScalerForAnyPair(1, s2);

		ICompositeSimilarityValue v1 = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd2));
		ICompositeSimilarityValue v2 = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd3));
		ICompositeSimilarityValue v3 = simFunc.calculateSimilarity(new ObjectPair(tsd2, tsd3));
		double value1 = v1.calculate().get();
		double value2 = v2.calculate().get();
		double value3 = v3.calculate().get();
//
//		assertEquals(0.0, s1.getMin(), 0.0);
//		assertEquals(1.0, s1.getMax(), 0.0);
//
//		assertEquals(-6.324555320336759, s2.getMin(), 0.0);
//		assertEquals(0.0, s2.getMax(), 0.0);

		assertEquals(0.5, v1.getChildSimilarityValue(0).calculate().get(), 0.0);
		assertEquals(0.0, v2.getChildSimilarityValue(0).calculate().get(), 0.0);
		assertEquals(0.5, v3.getChildSimilarityValue(0).calculate().get(), 0.0);

		assertEquals(-4.47213595499958, v1.getChildSimilarityValue(1).calculate().get(), 0.0);
		assertEquals(-6.324555320336759, v2.getChildSimilarityValue(1).calculate().get(), 0.0);
		assertEquals(-4.47213595499958, v3.getChildSimilarityValue(1).calculate().get(), 0.0);

		assertEquals(0.3964466, value1, 0.000001);
		assertEquals(0.0, value2, 0.0);
		assertEquals(0.3964466, value3, 0.000001);
	}

	@Test
	public void testNegativeEuclideanAndPearsonDifferentObjectsWithScalersAndEqualWeights() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		InverseEuclideanSimilarityFunction euclidean = new InverseEuclideanSimilarityFunction();
		WeightedAverageCompositeSimilarityFunction simFunc1 = new WeightedAverageCompositeSimilarityFunction(pearson,
				euclidean), simFunc2 = new WeightedAverageCompositeSimilarityFunction(pearson, euclidean);

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(pearson);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(euclidean);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42).build(),
				s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42).build();
		simFunc1.setScalerForAnyPair(0, s1);
		simFunc1.setScalerForAnyPair(1, s2);
		simFunc2.setScalerForAnyPair(0, s1);
		simFunc2.setScalerForAnyPair(1, s2);

		// set only weights on first function
		simFunc1.setWeight(0, 1.0);
		simFunc1.setWeight(1, 1.0);

		ICompositeSimilarityValue v11 = simFunc1.calculateSimilarity(new ObjectPair(tsd1, tsd2)),
				v12 = simFunc2.calculateSimilarity(new ObjectPair(tsd1, tsd2));
		ICompositeSimilarityValue v21 = simFunc1.calculateSimilarity(new ObjectPair(tsd1, tsd3)),
				v22 = simFunc2.calculateSimilarity(new ObjectPair(tsd1, tsd3));
		ICompositeSimilarityValue v31 = simFunc1.calculateSimilarity(new ObjectPair(tsd2, tsd3)),
				v32 = simFunc2.calculateSimilarity(new ObjectPair(tsd2, tsd3));

		assertEquals(v11.calculate().get(), v12.calculate().get(), 0.0);
		assertEquals(v21.calculate().get(), v22.calculate().get(), 0.0);
		assertEquals(v31.calculate().get(), v32.calculate().get(), 0.0);
	}

	@Test
	public void testNegativeEuclideanAndPearsonDifferentObjectsWithScalersAndDifferentWeights() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		pearson.setNormalizeByNumberTimepoints(false);
		InverseEuclideanSimilarityFunction euclidean = new InverseEuclideanSimilarityFunction();
		euclidean.setNormalizeByNumberTimepoints(false);
		WeightedAverageCompositeSimilarityFunction f = new WeightedAverageCompositeSimilarityFunction(pearson,
				euclidean);

		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(pearson);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(euclidean);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42).build(),
				s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42).build();
		f.setScalerForAnyPair(0, s1);
		f.setScalerForAnyPair(1, s2);
		f.setWeight(0, 1.0);
		f.setWeight(1, 0.0);

		ICompositeSimilarityValue v1 = f.calculateSimilarity(new ObjectPair(tsd1, tsd2));
		ICompositeSimilarityValue v2 = f.calculateSimilarity(new ObjectPair(tsd1, tsd3));
		ICompositeSimilarityValue v3 = f.calculateSimilarity(new ObjectPair(tsd2, tsd3));
		double value1 = v1.calculate().get();
		double value2 = v2.calculate().get();
		double value3 = v3.calculate().get();
//
//		assertEquals(0.0, s1.getMin(), 0.0);
//		assertEquals(1.0, s1.getMax(), 0.0);
//
//		assertEquals(-6.324555320336759, s2.getMin(), 0.0);
//		assertEquals(0.0, s2.getMax(), 0.0);

		assertEquals(0.5, v1.getChildSimilarityValue(0).calculate().get(), 0.0);
		assertEquals(0.0, v2.getChildSimilarityValue(0).calculate().get(), 0.0);
		assertEquals(0.5, v3.getChildSimilarityValue(0).calculate().get(), 0.0);

		assertEquals(-4.47213595499958, v1.getChildSimilarityValue(1).calculate().get(), 0.0);
		assertEquals(-6.324555320336759, v2.getChildSimilarityValue(1).calculate().get(), 0.0);
		assertEquals(-4.47213595499958, v3.getChildSimilarityValue(1).calculate().get(), 0.0);

		assertEquals(0.5, value1, 0.0);
		assertEquals(0.0, value2, 0.0);
		assertEquals(0.5, value3, 0.0);
	}

	@Test
	public void testNegativeEuclideanAndPearsonDifferentObjectsWithScalersAndOnlyZeroWeights() throws Exception {
		Assertions.assertThrows(SimilarityCalculationException.class, () -> {
			PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
			InverseEuclideanSimilarityFunction euclidean = new InverseEuclideanSimilarityFunction();
			WeightedAverageCompositeSimilarityFunction f = new WeightedAverageCompositeSimilarityFunction(pearson,
					euclidean);

			double[] data = new double[] { 0, 1, 2, 3, 4 };
			TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
			tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
			tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
			tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

			data = new double[] { 4, 3, 2, 3, 4 };
			TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
			tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
			tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
			tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

			data = new double[] { 4, 3, 2, 1, 0 };
			TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
			tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
			tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
			tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

			ITimeSeriesObjects ds = new TimeSeriesObjectList();
			ds.add(tsd1);
			ds.add(tsd2);
			ds.add(tsd3);

			ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(pearson);
			ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(euclidean);

			Random random = new Random(42);

			IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42).build(),
					s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42).build();
			f.setScalerForAnyPair(0, s1);
			f.setScalerForAnyPair(1, s2);
			f.setWeight(0, 0.0);
			f.setWeight(1, 0.0);

			ICompositeSimilarityValue v1 = f.calculateSimilarity(new ObjectPair(tsd1, tsd2));
			ICompositeSimilarityValue v2 = f.calculateSimilarity(new ObjectPair(tsd1, tsd3));
			ICompositeSimilarityValue v3 = f.calculateSimilarity(new ObjectPair(tsd2, tsd3));
			double value1 = v1.calculate().get();
			double value2 = v2.calculate().get();
			double value3 = v3.calculate().get();
//
//			assertEquals(0.0, s1.getMin(), 0.0);
//			assertEquals(0.5, s1.getMax(), 0.0);
//
//			assertEquals(-6.324555320336759, s2.getMin(), 0.0);
//			assertEquals(-4.47213595499958, s2.getMax(), 0.0);

			assertEquals(0.5, v1.getChildSimilarityValue(0).calculate().get(), 0.0);
			assertEquals(0.0, v2.getChildSimilarityValue(0).calculate().get(), 0.0);
			assertEquals(0.5, v3.getChildSimilarityValue(0).calculate().get(), 0.0);

			assertEquals(-4.47213595499958, v1.getChildSimilarityValue(1).calculate().get(), 0.0);
			assertEquals(-6.324555320336759, v2.getChildSimilarityValue(1).calculate().get(), 0.0);
			assertEquals(-4.47213595499958, v3.getChildSimilarityValue(1).calculate().get(), 0.0);

			assertEquals(1.0, value1, 0.0);
			assertEquals(0.0, value2, 0.0);
			assertEquals(1.0, value3, 0.0);
		});
	}

	@Test
	public void testPearsonCorrelationAndShortestPathObj1_2() throws Exception {
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("obj1");
		network.addNode("obj2");
		network.addNode("obj3");
		network.addEdge("obj1", "obj2", false);
		network.addEdge("obj2", "obj3", false);

		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		p1.setNormalizeByNumberTimepoints(false);
		InverseShortestPathSimilarityFunction sp = new InverseShortestPathSimilarityFunction(network);
		ICompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, sp);
		simFunc.initialize();

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(p1);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(sp);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42).build(),
				s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42).build();
		simFunc.setScalerForAnyPair(0, s1);
		simFunc.setScalerForAnyPair(1, s2);

		ICompositeSimilarityValue v = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd2));
		double value = v.calculate().get();

		assertEquals((1.0 + 1.0) / 2, value, 0.0);
		ISimpleSimilarityValue[] expected = new ISimpleSimilarityValue[] { p1.value(1.0, null), sp.value(-1.0, null) };
		ISimpleSimilarityValue[] observed = v.getChildValues();
		assertArrayEquals(expected, observed);
	}

	@Test
	public void testPearsonCorrelationAndShortestPathObj1_3() throws Exception {
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("obj1");
		network.addNode("obj2");
		network.addNode("obj3");
		network.addEdge("obj1", "obj2", false);
		network.addEdge("obj2", "obj3", false);

		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		InverseShortestPathSimilarityFunction sp = new InverseShortestPathSimilarityFunction(network);
		ICompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, sp);
		simFunc.initialize();

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(p1);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(sp);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42).build(),
				s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42).build();
		simFunc.setScalerForAnyPair(0, s1);
		simFunc.setScalerForAnyPair(1, s2);

		ICompositeSimilarityValue v = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd3));
		double value = v.calculate().get();

		assertEquals(0.0, value, 0.0);
		ISimpleSimilarityValue[] expected = new ISimpleSimilarityValue[] { p1.value(0.0, null), sp.value(-2.0, null) };
		ISimpleSimilarityValue[] observed = v.getChildValues();
		assertArrayEquals(expected, observed);
	}

	@Test
	public void testPearsonCorrelationAndShortestPathObj1_1() throws Exception {
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("obj1");
		network.addNode("obj2");
		network.addNode("obj3");
		network.addEdge("obj1", "obj2", false);
		network.addEdge("obj2", "obj3", false);

		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		p1.setNormalizeByNumberTimepoints(false);
		InverseShortestPathSimilarityFunction sp = new InverseShortestPathSimilarityFunction(network);
		ICompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, sp);
		simFunc.initialize();

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(p1);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(sp);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, ds, 1000, 42l).build(),
				s2 = new MinMaxScalerBuilder<>(f2, ds, 1000, 42l).build();
		simFunc.setScalerForAnyPair(0, s1);
		simFunc.setScalerForAnyPair(1, s2);

		ICompositeSimilarityValue v = simFunc.calculateSimilarity(new ObjectPair(tsd1, tsd1));
		double value = v.calculate().get();

		assertEquals(1.0, value, 0.0);
		ISimpleSimilarityValue[] expected = new ISimpleSimilarityValue[] { p1.value(1.0, null), sp.maxValue() };
		ISimpleSimilarityValue[] observed = v.getChildValues();
		assertArrayEquals(expected, observed);
	}

	@Test
	public void testPearsonCorrelationAndShortestPathCluster1_1() throws Exception {

		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);
		network.addEdge("Object 1", "Object 4", false);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		f.addPrototypeComponentFactory(nlf);
		InverseShortestPathSimilarityFunction negativeShortestPathSimilarityFunction = new InverseShortestPathSimilarityFunction(
				network);
		negativeShortestPathSimilarityFunction.initialize();
		nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, negativeShortestPathSimilarityFunction));

		Pair<ClusterObjectMapping, IClusterList> data = TestDataUtility.demoData(f, true);

		ClusterObjectMapping pom = data.getFirst();
		IClusterList clusters = data.getSecond();

		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		p1.setNormalizeByNumberTimepoints(false);
		InverseShortestPathSimilarityFunction sp = new InverseShortestPathSimilarityFunction(network);
		ICompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, sp);
		simFunc.initialize();

		ClusterPairSimilarityFeature f1 = new ClusterPairSimilarityFeature(p1);
		ClusterPairSimilarityFeature f2 = new ClusterPairSimilarityFeature(sp);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, pom, 1000, 42l).build(),
				s2 = new MinMaxScalerBuilder<>(f2, pom, 1000, 42l).build();
		simFunc.setScalerForClusterPair(0, s1);
		simFunc.setScalerForClusterPair(1, s2);

		ICompositeSimilarityValue calculateSimilarity = simFunc
				.calculateSimilarity(new ClusterPair(clusters.get(0), clusters.get(0)));
		assertEquals(1.0, calculateSimilarity.get(), 0.0);
	}

	@Test
	public void testPearsonCorrelationAndShortestPathCluster1_2() throws Exception {
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);
		network.addEdge("Object 1", "Object 4", false);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		f.addPrototypeComponentFactory(nlf);
		InverseShortestPathSimilarityFunction negativeShortestPathSimilarityFunction = new InverseShortestPathSimilarityFunction(
				network);
		negativeShortestPathSimilarityFunction.initialize();
		nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, negativeShortestPathSimilarityFunction));

		Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, true);

		ClusterObjectMapping pom = pair.getFirst();
		IClusterList clusters = pair.getSecond();

		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		p1.setNormalizeByNumberTimepoints(false);
		InverseShortestPathSimilarityFunction sp = new InverseShortestPathSimilarityFunction(network);
		WeightedAverageCompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, sp);
		simFunc.initialize();

		ClusterPairSimilarityFeature f1 = new ClusterPairSimilarityFeature(p1);
		ClusterPairSimilarityFeature f2 = new ClusterPairSimilarityFeature(sp);

		Random random = new Random(42);

		IScaler s1 = new MinMaxScalerBuilder<>(f1, pom, 1000, 42).build(),
				s2 = new MinMaxScalerBuilder<>(f2, pom, 1000, 42).build();
		simFunc.setScalerForClusterPair(0, s1);
		simFunc.setScalerForClusterPair(1, s2);

		ICompositeSimilarityValue calculateSimilarity = simFunc
				.calculateSimilarity(new ClusterPair(clusters.get(0), clusters.get(1)));
		assertEquals(0.5, calculateSimilarity.get(), 0.00001);
	}

	@Test
	@Tag("long-running")
	public void testShortestDistanceInfluenzaPPI() throws Exception {
		ITimeSeriesObjectList objs = new TimeSeriesObjectList(TestDataUtility.parseInfluenzaExpressionData());

		TiconeNetworkImpl network = TestDataUtility.parseI2DPPINetwork();

		int numberOfInitialClusters = 500;

		// only keep objects also in network
		objs = new TimeSeriesObjectList(
				objs.stream().filter(o -> network.containsNode(o.getName())).collect(Collectors.toList()));

		// remove nodes not in data set
		network.removeNodesNotInDataset(objs);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		InverseShortestPathSimilarityFunction sp = new InverseShortestPathSimilarityFunction(network);
		WeightedAverageCompositeSimilarityFunction similarityFunction = new WeightedAverageCompositeSimilarityFunction(
				p1, sp);
		similarityFunction.initialize();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		f.addPrototypeComponentFactory(nlf);
		nlf.setAggregationFunction(
				new AggregateClusterMedoidNode(network, new InverseShortestPathSimilarityFunction(network)));

		ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(
				similarityFunction.getSimilarityFunctions()[0]);
		ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(
				similarityFunction.getSimilarityFunctions()[1]);
		IScaler s1;
		try {
			s1 = new MinMaxScalerBuilder<>(f1, objs, 1000, 42l).setMapNanToZero(true).build();
		} catch (Exception e) {
			s1 = new DummyScalerBuilder<>().build();
		}
		IScaler s2;
		try {
			s2 = new MinMaxScalerBuilder<>(f2, objs, 1000, 42l).setMapNanToZero(true).build();
		} catch (Exception e) {
			s2 = new DummyScalerBuilder<>().build();
		}
		similarityFunction.setScalerForAnyPair(0, s1);
		similarityFunction.setScalerForAnyPair(1, s2);

		CLARAClusteringMethod initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f).build();

		System.out.println("performing clustering");
		ClusterObjectMapping findClusters = initialClusteringInterface.findClusters(objs, numberOfInitialClusters, 42);
		System.out.println(findClusters);
		IClusterList patternSingleLinkage = ClusterUtility.getPatternSingleLinkage(findClusters.getClusters().asList(),
				similarityFunction);
		System.out.println(patternSingleLinkage);
	}

	@Test
	@Tag("long-running")
	public void testShortestDistanceInfluenzaPPI_2iterations() throws Exception {
		ITimeSeriesObjectList objs = new TimeSeriesObjectList(
				TestDataUtility.parseInfluenzaExpressionData().subList(0, 500));

		TiconeNetworkImpl network = TestDataUtility.parseI2DPPINetwork();

		int numberOfInitialClusters = 50;

		// only keep objects also in network
		objs = new TimeSeriesObjectList(
				objs.stream().filter(o -> network.containsNode(o.getName())).collect(Collectors.toList()));

		Set<String> objIds = objs.stream().map(o -> o.getName()).collect(Collectors.toSet());

		// remove nodes not in data set
		network.removeNodesByIds(network.getNodeSet().stream().filter(n -> !objIds.contains(n.getName()))
				.map(n -> n.getName()).toArray(String[]::new));

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		InverseShortestPathSimilarityFunction sp = new InverseShortestPathSimilarityFunction(network);
		WeightedAverageCompositeSimilarityFunction similarityFunction = new WeightedAverageCompositeSimilarityFunction(
				p1, sp);
		similarityFunction.initialize();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		f.addPrototypeComponentFactory(nlf);
		nlf.setAggregationFunction(
				new AggregateClusterMedoidNode(network, new InverseShortestPathSimilarityFunction(network)));

		IScaler scaler1 = new QuantileScalerBuilder<>(new ObjectClusterPairSimilarityFeature(p1),
				new RandomlyAssignObjectsToClusters(42l, f, p1, numberOfInitialClusters).generateFrom(objs), 100, 42l)
						.setQuantiles(ArraysExt.range(0.0, 1.0, 0.01)).build();
		similarityFunction.setScalerForAnyPair(0, scaler1);

		IScaler scaler2 = new QuantileScalerBuilder<>(new ObjectClusterPairSimilarityFeature(sp),
				new RandomlyAssignObjectsToClusters(42l, f, sp, numberOfInitialClusters).generateFrom(objs), 100, 42l)
						.setQuantiles(ArraysExt.range(0.0, 1.0, 0.01)).build();
		similarityFunction.setScalerForAnyPair(1, scaler2);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		IInitialClusteringProvider<ClusterObjectMapping> initialClustering = new InitialClusteringMethod(
				initialClusteringInterface, numberOfInitialClusters, 42);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		process.start();

		System.out.println("performing 1st iteration");
		ClusterObjectMapping findClusters = process.doIteration();
		assertTrue(findClusters.getClusters().size() > 1);
		IClusterList patternSingleLinkage = ClusterUtility.getPatternSingleLinkage(findClusters.getClusters().asList(),
				similarityFunction);
		System.out.println(patternSingleLinkage);

		System.out.println("performing 2nd iteration");
		findClusters = process.doIteration();
		assertTrue(findClusters.getClusters().size() > 1);
	}

	@Test
	public void testPearsonCorrelationAndShortestPath_AvgClusterSimilarity() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("test");
		// cluster1
		TiconeNetworkNodeImpl n1 = network.addNode("n1");
		network.addNode("n2");
		network.addNode("n3");

		// cluster2
		TiconeNetworkNodeImpl n4 = network.addNode("n4");
		network.addNode("n5");
		network.addNode("n6");

		// cluster3
		network.addNode("n7");
		network.addNode("n8");
		network.addNode("n9");

		// cluster1
		network.addEdge("n1", "n2", false);
		network.addEdge("n1", "n3", false);
		network.addEdge("n2", "n3", false);

		// cluster2
		network.addEdge("n4", "n5", false);
		network.addEdge("n4", "n6", false);

		// cluster3: not connected

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		f.addPrototypeComponentFactory(nlf);
		InverseShortestPathSimilarityFunction negativeShortestPathSimilarityFunction = new InverseShortestPathSimilarityFunction(
				network);
		negativeShortestPathSimilarityFunction.initialize();
		nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, negativeShortestPathSimilarityFunction));
		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries());

		double[] ts = new double[] { 0, 1, 2 };

		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		// cluster1
		TimeSeriesObject o1 = new TimeSeriesObject("n1", 1), o2 = new TimeSeriesObject("n2", 1),
				o3 = new TimeSeriesObject("n3", 1);
		o1.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		o2.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		o3.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		f.setObjects(new TimeSeriesObjectList(o1, o2, o3));
		ICluster c1 = pom.getClusterBuilder().setPrototype(f.build()).build();
		c1.addObject(o1, AbstractSimilarityValue.MAX);
		c1.addObject(o2, AbstractSimilarityValue.MAX);
		c1.addObject(o3, AbstractSimilarityValue.MAX);

		// cluster2
		TimeSeriesObject o4 = new TimeSeriesObject("n4", 1), o5 = new TimeSeriesObject("n5", 1),
				o6 = new TimeSeriesObject("n6", 1);
		o4.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		o5.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		o6.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		f.setObjects(new TimeSeriesObjectList(o4, o5, o6));
		ICluster c2 = pom.getClusterBuilder().setPrototype(f.build()).build();
		c2.addObject(o4, AbstractSimilarityValue.MAX);
		c2.addObject(o5, AbstractSimilarityValue.MAX);
		c2.addObject(o6, AbstractSimilarityValue.MAX);

		// cluster3
		TimeSeriesObject o7 = new TimeSeriesObject("n7", 1), o8 = new TimeSeriesObject("n8", 1),
				o9 = new TimeSeriesObject("n9", 1);
		o7.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		o8.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		o9.addPreprocessedTimeSeries(0, TimeSeries.of(ts));
		f.setObjects(new TimeSeriesObjectList(o7, o8, o9));
		ICluster c3 = pom.getClusterBuilder().setPrototype(f.build()).build();
		c3.addObject(o7, AbstractSimilarityValue.MAX);
		c3.addObject(o8, AbstractSimilarityValue.MAX);
		c3.addObject(o9, AbstractSimilarityValue.MAX);

		PearsonCorrelationFunction p1 = new PearsonCorrelationFunction();
		InverseShortestPathSimilarityFunction sp = new InverseShortestPathSimilarityFunction(network);
		WeightedAverageCompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(p1, sp);
		simFunc.initialize();

		ClusterFeatureAverageSimilarity avgSim = new ClusterFeatureAverageSimilarity(simFunc);
		for (ICluster c : pom.getClusters()) {
			System.out.println(c.getPrototype());
			IFeatureValue<ISimilarityValue> val = avgSim.calculate(c);
			ICompositeSimilarityValue similarityValue = (ICompositeSimilarityValue) val.getValue();
			System.out.println(c + "\t" + Arrays.toString(similarityValue.getChildValues()));
		}

		Set<TiconeNetwork<TiconeNetworkNodeImpl, TiconeNetworkEdgeImpl>.ConnectedComponent> connectedComponents = network
				.getConnectedComponents();
		connectedComponents.forEach(c -> System.out.println(c.getNodes()));
	}
}
