package dk.sdu.imada.ticone.similarity;

import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import de.wiwie.wiutils.utils.Pair;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPairList;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.IObjectPairs;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairsFactory;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.TestDataUtility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 16, 2018
 *
 */
public class TestNegativeShortestNetworkDistanceSimilarityFunction {
	private PrototypeBuilder prototypeFactory;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		prototypeFactory = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		prototypeFactory.setPrototypeComponentBuilders(tsf);
	}

	@Test
	@Tag("long-running")
	public void testShortestDistanceObjectPairs() throws Exception {
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		prototypeFactory.addPrototypeComponentFactory(nlf);
		nlf.setAggregationFunction(
				new AggregateClusterMedoidNode(network, new InverseShortestPathSimilarityFunction(network)));

		Pair<ClusterObjectMapping, IClusterList> demoData = TestDataUtility.demoData(prototypeFactory, true);

		ClusterObjectMapping clustering = demoData.getFirst();
		InverseShortestPathSimilarityFunction simFunc = new InverseShortestPathSimilarityFunction(network);
		simFunc.setDirected(false);
		// add a completely disconnected node
		network.addNode("testObj");
		TimeSeriesObject disconnectedObject = new TimeSeriesObject("testObj");
		clustering.addMapping(disconnectedObject, clustering.getClusters().asList().get(0),
				AbstractSimilarityValue.MAX);

		TimeSeriesObjectList allObjects = clustering.getAssignedObjects();

		IObjectPairs objectPairs = clustering.getObjectPairs();

		ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> calculateSimilarity = simFunc

				.calculateSimilarities(objectPairs.asList(), ObjectType.OBJECT_PAIR);

		// TODO
//		// self edge
//		ObjectPair p = new ObjectPair(allObjects.get(0), allObjects.get(0));
//		System.out.println(p);
//		assertEquals(0.0, calculateSimilarity.get(p).get(), 0.0);
//		// direct neighbors
//		p = new ObjectPair(allObjects.get(7), allObjects.get(1));
//		System.out.println(p);
//		assertEquals(-1.0, calculateSimilarity.get(p).get(), 0.0);
//		// not connected to anything
//		p = new ObjectPair(disconnectedObject, allObjects.get(0));
//		System.out.println(p);
//		assertEquals(-Double.POSITIVE_INFINITY, calculateSimilarity.get(p).get(), 0.0);
	}

	@Test
	@Tag("long-running")
	public void testShortestDistanceObjectPairs2() throws Exception {
		TiconeNetworkImpl network = TestDataUtility.parseI2DPPINetwork();

//		for (TiconeNetworkNodeImpl n : new ArrayList<>(network.getNodeSet()).subList(10000, network.getNodeCount()))
//			network.removeNode(n);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		prototypeFactory.addPrototypeComponentFactory(nlf);
		nlf.setAggregationFunction(
				new AggregateClusterMedoidNode(network, new InverseShortestPathSimilarityFunction(network)));

		ITimeSeriesObjectList allObjects = TestDataUtility.parseInfluenzaExpressionData();

		network.removeNodesNotInDataset(allObjects);
		allObjects.removeIf(o -> !network.containsNode(o.getName()));
		System.out.println(allObjects.size());
		network.reinitializeNetworkWideNodeIds();

		allObjects = new TimeSeriesObjectList(allObjects.stream()
				.map(o -> o.mapToNetworkNode(network, network.getNode(o.getName()))).collect(Collectors.toList()));

		InverseShortestPathSimilarityFunction simFunc = new InverseShortestPathSimilarityFunction(network);
		simFunc.setDirected(false);

		ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> calculateSimilarities = simFunc
				.calculateSimilarities(allObjects.toArray(), allObjects.toArray(), new ObjectPair.ObjectPairsFactory(),
						ObjectType.OBJECT_PAIR);
		System.out.println("bla");
	}

	@Test
	public void testShortestUndirectedDistanceObjectClusterPair() throws Exception {
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		prototypeFactory.addPrototypeComponentFactory(nlf);
		InverseShortestPathSimilarityFunction negativeShortestPathSimilarityFunction = new InverseShortestPathSimilarityFunction(
				network);
		negativeShortestPathSimilarityFunction.initialize();
		nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, negativeShortestPathSimilarityFunction));

		Pair<ClusterObjectMapping, IClusterList> demoData = TestDataUtility.demoData(prototypeFactory, true);
		ClusterObjectMapping clustering = demoData.getFirst();
		InverseShortestPathSimilarityFunction simFunc = new InverseShortestPathSimilarityFunction(network);
		simFunc.setDirected(false);
		TimeSeriesObjectList allObjects = clustering.getAssignedObjects();

		assertEquals("Object 1",
				PrototypeComponentType.NETWORK_LOCATION.getComponent(clustering.getCluster(1).getPrototype())
						.getNetworkLocation().iterator().next().getName());
		assertEquals("Object 4",
				PrototypeComponentType.NETWORK_LOCATION.getComponent(clustering.getCluster(2).getPrototype())
						.getNetworkLocation().iterator().next().getName());
		assertEquals("Object 7",
				PrototypeComponentType.NETWORK_LOCATION.getComponent(clustering.getCluster(3).getPrototype())
						.getNetworkLocation().iterator().next().getName());

		System.out.println(network);

		final IObjectClusterPairList pairs = new ObjectClusterPairsFactory().createList(allObjects,
				clustering.getClusters());

		for (IObjectClusterPair pair : pairs) {
			final ICluster cluster = pair.getCluster();
			final ITimeSeriesObject o = pair.getFirst();

			double expectedSim = Double.NEGATIVE_INFINITY;
			switch (o.getName()) {
			case "Object 1":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = Double.POSITIVE_INFINITY;
					break;
				case 2:
					expectedSim = -2.0;
					break;
				case 3:
					expectedSim = -2.0;
					break;
				}
				break;
			case "Object 2":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -1.0;
					break;
				case 2:
					expectedSim = -1.0;
					break;
				case 3:
					expectedSim = -1.0;
					break;
				}
				break;
			case "Object 3":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -2.0;
					break;
				case 2:
					expectedSim = -2.0;
					break;
				case 3:
					expectedSim = -2.0;
					break;
				}
				break;
			case "Object 4":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -2.0;
					break;
				case 2:
					expectedSim = Double.POSITIVE_INFINITY;
					break;
				case 3:
					expectedSim = -2.0;
					break;
				}
				break;
			case "Object 5":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -1.0;
					break;
				case 2:
					expectedSim = -1.0;
					break;
				case 3:
					expectedSim = -1.0;
					break;
				}
				break;
			case "Object 6":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -2.0;
					break;
				case 2:
					expectedSim = -2.0;
					break;
				case 3:
					expectedSim = -2.0;
					break;
				}
				break;
			case "Object 7":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -2.0;
					break;
				case 2:
					expectedSim = -2.0;
					break;
				case 3:
					expectedSim = Double.POSITIVE_INFINITY;
					break;
				}
				break;
			case "Object 8":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -1.0;
					break;
				case 2:
					expectedSim = -1.0;
					break;
				case 3:
					expectedSim = -1.0;
					break;
				}
				break;
			case "Object 9":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -2.0;
					break;
				case 2:
					expectedSim = -2.0;
					break;
				case 3:
					expectedSim = -2.0;
					break;
				}
				break;
			}

			double observed = simFunc.calculateSimilarity(pair).get();
			assertEquals(pair.toString(), expectedSim, observed, 0.0);
		}

	}

	@Test
	public void testShortestDirectedDistanceObjectClusterPair() throws Exception {
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(true);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		prototypeFactory.addPrototypeComponentFactory(nlf);
		InverseShortestPathSimilarityFunction negativeShortestPathSimilarityFunction = new InverseShortestPathSimilarityFunction(
				network);
		negativeShortestPathSimilarityFunction.setDirected(true);
		negativeShortestPathSimilarityFunction.initialize();
		nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, negativeShortestPathSimilarityFunction));

		Pair<ClusterObjectMapping, IClusterList> demoData = TestDataUtility.demoData(prototypeFactory, true);
		ClusterObjectMapping clustering = demoData.getFirst();
		InverseShortestPathSimilarityFunction simFunc = new InverseShortestPathSimilarityFunction(network);
		simFunc.setDirected(true);
		TimeSeriesObjectList allObjects = clustering.getAssignedObjects();

		assertEquals("Object 2",
				PrototypeComponentType.NETWORK_LOCATION.getComponent(clustering.getCluster(1).getPrototype())
						.getNetworkLocation().iterator().next().getName());
		assertEquals("Object 5",
				PrototypeComponentType.NETWORK_LOCATION.getComponent(clustering.getCluster(2).getPrototype())
						.getNetworkLocation().iterator().next().getName());
		assertEquals("Object 8",
				PrototypeComponentType.NETWORK_LOCATION.getComponent(clustering.getCluster(3).getPrototype())
						.getNetworkLocation().iterator().next().getName());

		System.out.println(network);

		final IObjectClusterPairList pairs = new ObjectClusterPairsFactory().createList(allObjects,
				clustering.getClusters());

		for (IObjectClusterPair pair : pairs) {
			final ICluster cluster = pair.getCluster();
			final ITimeSeriesObject o = pair.getFirst();

			double expectedSim = Double.NEGATIVE_INFINITY;
			switch (o.getName()) {
			case "Object 1":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -1;
					break;
				case 2:
					expectedSim = -2;
					break;
				case 3:
					expectedSim = -2;
					break;
				}
				break;
			case "Object 2":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -2;
					break;
				case 2:
					expectedSim = -1;
					break;
				case 3:
					expectedSim = -1;
					break;
				}
				break;
			case "Object 3":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = 0;
					break;
				case 2:
					expectedSim = Double.NEGATIVE_INFINITY;
					break;
				case 3:
					expectedSim = Double.NEGATIVE_INFINITY;
					break;
				}
				break;
			case "Object 4":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -2;
					break;
				case 2:
					expectedSim = -3;
					break;
				case 3:
					expectedSim = -2;
					break;
				}
				break;
			case "Object 5":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -1;
					break;
				case 2:
					expectedSim = -2;
					break;
				case 3:
					expectedSim = -1;
					break;
				}
				break;
			case "Object 6":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = Double.NEGATIVE_INFINITY;
					break;
				case 2:
					expectedSim = 0;
					break;
				case 3:
					expectedSim = Double.NEGATIVE_INFINITY;
					break;
				}
				break;
			case "Object 7":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -2;
					break;
				case 2:
					expectedSim = -2;
					break;
				case 3:
					expectedSim = -3;
					break;
				}
				break;
			case "Object 8":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = -1;
					break;
				case 2:
					expectedSim = -1;
					break;
				case 3:
					expectedSim = -2;
					break;
				}
				break;
			case "Object 9":
				switch (cluster.getClusterNumber()) {
				case 1:
					expectedSim = Double.NEGATIVE_INFINITY;
					break;
				case 2:
					expectedSim = Double.NEGATIVE_INFINITY;
					break;
				case 3:
					expectedSim = 0;
					break;
				}
				break;
			}

			double observed = simFunc.calculateSimilarity(pair).get();
			assertEquals(pair.toString(), expectedSim, observed, 0.0);
		}

	}

	@Test
	public void testShortestDistanceClusterPair() throws Exception {
		TiconeNetworkImpl network = TestDataUtility.demoNetwork(false);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		prototypeFactory.addPrototypeComponentFactory(nlf);
		InverseShortestPathSimilarityFunction negativeShortestPathSimilarityFunction = new InverseShortestPathSimilarityFunction(
				network);
		negativeShortestPathSimilarityFunction.initialize();
		nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, negativeShortestPathSimilarityFunction));

		Pair<ClusterObjectMapping, IClusterList> demoData = TestDataUtility.demoData(prototypeFactory, true);
		System.out.println(network);
		ClusterObjectMapping clustering = demoData.getFirst();
		InverseShortestPathSimilarityFunction simFunc = new InverseShortestPathSimilarityFunction(network);
		simFunc.setDirected(false);

		ICluster cluster1 = clustering.getCluster(1);
		ICluster cluster2 = clustering.getCluster(2);
		System.out.println(cluster1.getPrototype());
		System.out.println(cluster2.getPrototype());

		ISimilarityValue sim = simFunc.calculateSimilarity(new ClusterPair(cluster1, cluster2));

		assertEquals(-2.0, sim.get(), 0.00001);
	}
}
