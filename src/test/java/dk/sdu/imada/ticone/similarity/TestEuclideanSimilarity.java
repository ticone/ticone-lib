package dk.sdu.imada.ticone.similarity;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * Created by christian on 6/12/15.
 */
public class TestEuclideanSimilarity {

	@Test
	public void testAllPairs() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		final TimeSeriesObjectList objects = new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4);

		InverseEuclideanSimilarityFunction pearson = new InverseEuclideanSimilarityFunction();
		ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(objects.toArray(), objects.toArray(), new ObjectPair.ObjectPairsFactory(),
						ObjectType.OBJECT_PAIR);
		System.out.println(similarities);
		Double observedMin = similarities.min().calculate().get();
		double expectedMin = -1.2649;
		assertEquals(expectedMin, observedMin, 0.0001);
	}

	@Test
	public void testAllPairsWithTemplate() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		final TimeSeriesObjectList objects = new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4);

		InverseEuclideanSimilarityFunction pearson = new InverseEuclideanSimilarityFunction();

		ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> sims = pearson.emptySimilarities(
				objects.toArray(), objects.toArray(), new ObjectPair.ObjectPairsFactory(), ObjectType.OBJECT_PAIR);
		System.out.println(sims);
		pearson.calculateSimilarities(new int[] { 0 }, new int[] { 2 }, sims);
		System.out.println(sims);
		pearson.calculateSimilarities(sims.firstObjectIndices(), sims.secondObjectIndices(), sims);
		System.out.println(sims);
		Double observedMin = sims.min().calculate().get();
		double expectedMin = -1.2649;
		assertEquals(expectedMin, observedMin, 0.0001);
	}

}
