/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairsFactory;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 30, 2018
 *
 */
public class TestSimilarityValues {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@Test
	public void testMinAndWhichMin() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		ISimilarityValuesSomePairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(
						new ObjectPair.ObjectPairsFactory().createList(new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4),
								new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4)),
						ObjectType.OBJECT_PAIR);
		Double observedMin = similarities.min().calculate().get();
		double expectedMin = 0.0;
		assertEquals(expectedMin, observedMin, 0.0);
		ISimilarityValue whichMin = similarities.min();
		assertEquals(expectedMin, whichMin.calculate().get(), 0.0);
	}

	@Test
	public void testMaxAndWhichMax() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		ISimilarityValuesSomePairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(
						new ObjectPair.ObjectPairsFactory().createList(new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4),
								new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4)),
						ObjectType.OBJECT_PAIR);
		Double observedMax = similarities.max().calculate().get();
		double expectedMax = 1.0;
		assertEquals(expectedMax, observedMax, 0.0);
		ISimilarityValue whichMax = similarities.max();
		assertEquals(expectedMax, whichMax.calculate().get(), 0.0);
	}

	@Test
	public void testMean() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(
						new ObjectPair.ObjectPairsFactory().createList(new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4),
								new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4)),
						ObjectType.OBJECT_PAIR);
		Double observedMean = similarities.mean().calculate().get();
		double expectedMean = 9.0 / 16;
		assertEquals(expectedMean, observedMean, 0.0);
	}

	@Test
	public void testSum() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(
						new ObjectPair.ObjectPairsFactory().createList(new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4),
								new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4)),
						ObjectType.OBJECT_PAIR);
		Double observedSum = similarities.sum().calculate().get();
		double expectedSum = 9.0;
		assertEquals(expectedSum, observedSum, 0.0);
	}

	@Test
	public void testFilterByFirstObject() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		ISimilarityValuesSomePairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(
						new ObjectPair.ObjectPairsFactory().createList(new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4),
								new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4)),
						ObjectType.OBJECT_PAIR);
		ISimilarityValuesSomePairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> subset = similarities
				.filter((o1, o2, s) -> o1 == tsd1);
		assertEquals(4, subset.size());
		Set<String> expectedIds = new HashSet<>(Arrays.asList("obj1", "obj2", "obj3", "obj4"));
		Set<String> foundIds = new HashSet<>();
		subset.getPairs().forEach(p -> foundIds.add(p.getSecond().getName()));
		assertEquals(expectedIds, foundIds);
	}

	@Test
	public void testFilterBySecondObject() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(
						new ObjectPair.ObjectPairsFactory().createList(new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4),
								new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4)),
						ObjectType.OBJECT_PAIR);
		ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> subset = similarities
				.filter((o1, o2, s) -> o2 == tsd1);
		assertEquals(4, subset.size());
		Set<String> expectedIds = new HashSet<>(Arrays.asList("obj1", "obj2", "obj3", "obj4"));
		Set<String> foundIds = new HashSet<>();
		subset.getPairs().forEach(p -> foundIds.add(p.getFirst().getName()));
		assertEquals(expectedIds, foundIds);
	}

	@Test
	public void testFilterBySimilarityValue() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(
						new ObjectPair.ObjectPairsFactory().createList(new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4),
								new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4)),
						ObjectType.OBJECT_PAIR);
		ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> subset = similarities
				.filter((o1, o2, s) -> {
					try {
						return s.calculate().get() == 0.0;
					} catch (SimilarityCalculationException e) {
						return false;
					}
				});
		assertEquals(4, subset.size());
		Set<IObjectPair> expectedPairs = new HashSet<>(Arrays.asList(new ObjectPair(tsd1, tsd3),
				new ObjectPair(tsd2, tsd3), new ObjectPair(tsd3, tsd1), new ObjectPair(tsd3, tsd2)));
		Set<IObjectPair> observed = new HashSet<>();
		subset.getPairs().forEach(p -> observed.add(p));
		assertEquals(expectedPairs, observed);
	}

	@Test
	public void testGroupByFirstObject() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.calculateSimilarities(
						new ObjectPair.ObjectPairsFactory().createList(new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4),
								new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4)),
						ObjectType.OBJECT_PAIR);
		Map<?, ? extends ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>> subsets = similarities
				.partitionByFirstObject();
		assertEquals(4, subsets.size());
		for (ISimilarityValues<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> subset : subsets.values()) {
			assertEquals(4, subset.size());

			Set<String> expectedIds = new HashSet<>(Arrays.asList("obj1", "obj2", "obj3", "obj4"));
			Set<String> foundIds = new HashSet<>();
			subset.getPairs().forEach(p -> foundIds.add(p.getSecond().getName()));
			assertEquals(expectedIds, foundIds);
		}
	}

	@Test
	public void testSortedByValues3Objects() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		tsf.setTimeSeries(new double[] { 0.1, 0.3, 0.7 });
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		ICluster p = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject obj1 = new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) });

		ITimeSeriesObject obj2 = new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 0, 2), TimeSeries.of(1, 0, 1) });

		ITimeSeriesObject obj3 = new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 1, 3), TimeSeries.of(1, 2, 4) });

		pom.addMapping(obj1, p, TestSimpleSimilarityValue.of(0.7));
		pom.addMapping(obj2, p, TestSimpleSimilarityValue.of(0.8));
		pom.addMapping(obj3, p, TestSimpleSimilarityValue.of(0.6));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(pom.getAssignedObjects());
		absValues.process();

		final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = similarityFunction
				.calculateSimilarities(p.getObjects().toArray(), p.asSingletonList().toArray(),
						new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR);
		assertArrayEquals(new double[] { .9909902530309829, .7109089768031478, .986227795630767 },
				sims.values().stream().mapToDouble(s -> {
					try {
						return s.get();
					} catch (SimilarityCalculationException e) {
						return Double.NEGATIVE_INFINITY;
					}
				}).toArray(), 0.00001);

		final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sortedByValue = sims
				.sortedByValue();
		assertArrayEquals(new double[] { .9909902530309829, .986227795630767, .7109089768031478 },
				sortedByValue.values().stream().mapToDouble(s -> {
					try {
						return s.get();
					} catch (SimilarityCalculationException e) {
						return Double.NEGATIVE_INFINITY;
					}
				}).toArray(), 0.00001);
	}

	@Test
	public void testSortedByValues4Objects() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		tsf.setTimeSeries(new double[] { 0.1, 0.3, 0.7 });
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		ICluster p = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject obj1 = new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) });

		ITimeSeriesObject obj2 = new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 0, 2), TimeSeries.of(1, 0, 1) });

		ITimeSeriesObject obj3 = new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 1, 3), TimeSeries.of(1, 2, 4) });

		ITimeSeriesObject obj4 = new TimeSeriesObject("test4", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 1, 3), TimeSeries.of(1, 3, 4) });

		pom.addMapping(obj1, p, TestSimpleSimilarityValue.of(0.7));
		pom.addMapping(obj2, p, TestSimpleSimilarityValue.of(0.8));
		pom.addMapping(obj3, p, TestSimpleSimilarityValue.of(0.6));
		pom.addMapping(obj4, p, TestSimpleSimilarityValue.of(0.6));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(pom.getAssignedObjects());
		absValues.process();

		final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = similarityFunction
				.calculateSimilarities(p.getObjects().toArray(), p.asSingletonList().toArray(),
						new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR);
		double[] observed = sims.values().stream().mapToDouble(s -> {
			try {
				return s.get();
			} catch (SimilarityCalculationException e) {
				return Double.NEGATIVE_INFINITY;
			}
		}).toArray();
		assertArrayEquals(new double[] { .9909902530309829, .7109089768031478, .986227795630767, 0.9683706527736242 },
				observed, 0.00001);

		final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sortedByValue = sims
				.sortedByValue();
		observed = sortedByValue.values().stream().mapToDouble(s -> {
			try {
				return s.get();
			} catch (SimilarityCalculationException e) {
				return Double.NEGATIVE_INFINITY;
			}
		}).toArray();
		assertArrayEquals(new double[] { .9909902530309829, .986227795630767, 0.9683706527736242, .7109089768031478 },
				observed, 0.00001);
	}

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testAllCalculated() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		TimeSeriesObjectList objects = new TimeSeriesObjectList(tsd1, tsd2, tsd3, tsd4);
		ObjectPair.ObjectPairsFactory factory = new ObjectPair.ObjectPairsFactory();
		final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = pearson
				.emptySimilarities(objects.toArray(), objects.toArray(), factory, ObjectType.OBJECT_PAIR);

		for (int x = 0; x < objects.size(); x++) {
			for (int y = 0; y < objects.size(); y++) {
				assertFalse(similarities.isSet(x, y));
			}
		}

		pearson.calculateSimilarities(IntStream.range(0, objects.size()).toArray(),
				IntStream.range(0, objects.size()).toArray(), similarities);

		for (int x = 0; x < objects.size(); x++) {
			for (int y = 0; y < objects.size(); y++) {
				assertTrue(similarities.isSet(x, y));
				assertFalse(similarities.isMissing(x, y));
			}
		}
	}
}
