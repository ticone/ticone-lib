package dk.sdu.imada.ticone.similarity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IIterableWithSize;
import dk.sdu.imada.ticone.util.TimePointWeighting;

/**
 * Created by christian on 6/12/15.
 */
public class TestPearson {

	@Test
	public void testPearson() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] data1 = { 0, 1, 2, 3, 4 };
		double[] data2 = { 0, -1, -2, -3, -4 };
		double similarity = pearson
				.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(data1), TimeSeries.of(data2), null).calculate()
				.get();
		assertEquals(0, similarity, 0.01);

		data2 = new double[] { 0, 2, 4, 6, 8 };
		similarity = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(data1), TimeSeries.of(data2), null)
				.calculate().get();
		assertEquals(1, similarity, 0.01);

		data2 = new double[] { 0, 2, 0, 2, 0 };
		similarity = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(data1), TimeSeries.of(data2), null)
				.calculate().get();
		assertEquals(0.5, similarity, 0.01);
	}

	@Test
	public void testZeroVariance() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] data1 = { 0, 0, 0, 0 };
		double[] data2 = { 0, 0, 0, 0 };
		double similarity = pearson
				.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(data1), TimeSeries.of(data2), null).calculate()
				.get();
		assertTrue(Double.isNaN(similarity));

	}

	@Test
	public void testMultiplePearson() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(1, similarity, 0.01);

		data = new double[] { 0, 1, 2, 3, 4 };
		tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, -1, -2, -3, -4 };
		tsd2 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(0, similarity, 0.01);

		data = new double[] { 0, 1, 2, 3, 4 };
		tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, -1, -2, -3, -4 };
		tsd2 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		data = new double[] { 0, 1, 2, 3, 4 };
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(0.5, similarity, 0.01);
	}

	@Test
	public void testCalculateUnweightedCovarianceWithoutWeights() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();

		double[] x = new double[] { 1, 2, 3 };
		double[] y = new double[] { 2, 3, 4 };

		double mean1 = ArraysExt.mean(x);
		double mean2 = ArraysExt.mean(y);

		assertEquals(2 / 3.0, pearson.calculateUnweightedCovariance(TimeSeries.of(x), mean1, TimeSeries.of(y), mean2),
				0.0);
	}

	@Test
	public void testCalculateWeightedCovarianceWithoutWeights() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();

		double[] x = new double[] { 1, 2, 3 };
		double[] y = new double[] { 2, 3, 4 };

		double mean1 = ArraysExt.mean(x);
		double mean2 = ArraysExt.mean(y);

		assertEquals(2 / 3.0, pearson.calculateWeightedCovariance(x, y, mean1, mean2), 0.0);
	}

	@Test
	public void testTimePointWeighting() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(1, similarity, 0.01);

	}

	@Test
	public void testIdenticalTimeSeriesWithWeighting() throws Exception {
		TimePointWeighting weights = new TimePointWeighting(5);
		for (int i = 0; i < 5; i++)
			weights.setWeightForTimePointColumn(i, 1.0);
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction(weights);
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		TimeSeriesObject tsd2 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(1, similarity, 0.01);
	}

	@Test
	public void testDifferentTimeSeriesWithEqualWeights() throws Exception {
		TimePointWeighting weights = new TimePointWeighting(5);
		for (int i = 0; i < weights.getTimePointColumns(); i++)
			weights.setWeightForTimePointColumn(i, 1 / (double) weights.getTimePointColumns());
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction(weights);
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 5, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(.5, similarity, .01);
	}

	@Test
	public void testDifferentTimeSeriesWithEqualWeights2() throws Exception {
		TimePointWeighting weights = new TimePointWeighting(5);
		for (int i = 0; i < weights.getTimePointColumns(); i++)
			weights.setWeightForTimePointColumn(i, 1 / (double) weights.getTimePointColumns());
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction(weights);
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 5, 1, 4, 1, 1 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(0.1755572, similarity, 0.01);
	}

	@Test
	public void testDifferentTimeSeriesWithEqualWeights3() throws Exception {
		TimePointWeighting weights = new TimePointWeighting(3);
		for (int i = 0; i < weights.getTimePointColumns(); i++)
			weights.setWeightForTimePointColumn(i, 1 / (double) weights.getTimePointColumns());
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction(weights);
		double[] data = new double[] { 0, 1, 2 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 2, 1, 0 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(0.0, similarity, 0.0);
	}

	@Test
	public void testDifferentTimeSeriesWithEqualWeights4() throws Exception {
		TimePointWeighting weights = new TimePointWeighting(3);
		for (int i = 0; i < weights.getTimePointColumns(); i++)
			weights.setWeightForTimePointColumn(i, 1 / (double) weights.getTimePointColumns());
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction(weights);
		double[] data = new double[] { 0, 3, 0 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 1.5, 0, 1.5 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(0.0, similarity, 0.0);
	}

	@Test
	public void testDifferentTimeSeriesWithEqualWeights6() throws Exception {
		TimePointWeighting weights = new TimePointWeighting(3);
		for (int i = 0; i < weights.getTimePointColumns(); i++)
			weights.setWeightForTimePointColumn(i, 1 / (double) weights.getTimePointColumns());
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction(weights);
		double[] data = new double[] { 0, 3, 0 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 2, 1, 0 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(0.5, similarity, 0.0);
	}

	@Test
	public void testDifferentTimeSeriesWithEqualWeights5() throws Exception {
		TimePointWeighting weights = new TimePointWeighting(3);
		for (int i = 0; i < weights.getTimePointColumns(); i++)
			weights.setWeightForTimePointColumn(i, i / (double) weights.getTimePointColumns());
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction(weights);
		double[] data = new double[] { 0, 3, 0 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 3, 0, 0 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		double similarity = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertTrue(Double.isNaN(similarity));
	}

	@Test
	public void testNormalization() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] data1 = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data1));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data1), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data1));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data1));

		double[] data2 = new double[] { 0, 1, 3, 2, 1 };
		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data2));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data2), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data2));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data2));

		double similarity1 = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(0.7080125735844609, similarity1, 0.00001);

		// normalize between 0 and 1 and check that we get the same correlation

		data1 = normalize(data1);
		tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data1));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data1), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data1));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data1));

		data2 = normalize(data2);
		tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data2));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data2), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data2));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data2));

		double similarity2 = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(similarity1, similarity2, 0.00001);
	}

	@Test
	public void testNormalization2() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] data1 = { -4, -3, -2, -1, 0 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data1));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data1), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data1));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data1));

		double[] data2 = new double[] { -4, -3, -1, -2, -3 };
		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data2));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data2), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data2));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data2));

		double similarity1 = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(0.7080125735844609, similarity1, 0.00001);

		// normalize between 0 and 1 and check that we get the same correlation

		data1 = normalize(data1);
		tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data1));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data1), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data1));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data1));

		data2 = normalize(data2);
		tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data2));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data2), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data2));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data2));

		double similarity2 = pearson.calculateSimilarity(new ObjectPair(tsd1, tsd2)).calculate().get();
		assertEquals(similarity1, similarity2, 0.00001);
	}

	@Test
	public void testSimilarityValuesAllPairs() throws Exception {
		PearsonCorrelationFunction p = new PearsonCorrelationFunction();

		double[] data1 = { -4, -3, -2, -1, 0 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data1));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data1), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data1));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data1));

		double[] data2 = new double[] { -4, -3, -1, -2, -3 };
		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data2));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data2), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data2));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data2));

		ITimeSeriesObjects objects = new TimeSeriesObjectList(tsd1, tsd2);

		ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> sims = p.calculateSimilarities(
				objects.toArray(), objects.toArray(), new ObjectPair.ObjectPairsFactory(), ObjectType.OBJECT_PAIR);

		Iterable<int[]> pairIndices = sims.pairIndices();
		assertTrue(pairIndices instanceof IIterableWithSize);
		assertEquals(4l, ((IIterableWithSize) pairIndices).longSize());

		Iterator<int[]> it = pairIndices.iterator();
		assertTrue(it.hasNext());
		assertArrayEquals(new int[] { 0, 0 }, it.next());
		assertTrue(it.hasNext());
		assertArrayEquals(new int[] { 0, 1 }, it.next());
		assertTrue(it.hasNext());
		assertArrayEquals(new int[] { 1, 0 }, it.next());
		assertTrue(it.hasNext());
		assertArrayEquals(new int[] { 1, 1 }, it.next());
		assertFalse(it.hasNext());
	}

	public double[] normalize(double[] arr) {
		double[] result = new double[arr.length];

		double min = min(arr);
		double max = max(arr);

		for (int i = 0; i < arr.length; i++) {
			result[i] = (arr[i] - min) / (max - min);
		}
		return result;
	}

	public double min(double[] arr) {
		double result = Double.POSITIVE_INFINITY;

		for (double d : arr)
			if (d < result)
				result = d;

		return result;
	}

	public double max(double[] arr) {
		double result = Double.NEGATIVE_INFINITY;

		for (double d : arr)
			if (d > result)
				result = d;

		return result;
	}
}
