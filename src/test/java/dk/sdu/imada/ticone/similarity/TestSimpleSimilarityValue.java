/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 18, 2018
 *
 */
public class TestSimpleSimilarityValue extends AbstractSimilarityValue implements ISimpleSimilarityValue {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2830465687337544208L;

	public static TestSimpleSimilarityValue of(final double value) {
		return new TestSimpleSimilarityValue(value);
	}

	/**
	 * 
	 */
	TestSimpleSimilarityValue(final double value) {
		super(null);
		this.value = value;
		this.calculated = true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ISimilarityValue)
			return compareTo((ISimilarityValue) obj) == 0;
		return false;
	}

	@Override
	public int hashCode() {
		return Double.hashCode(value);
	}

	@Override
	public TestSimpleSimilarityValue doCalculate() {
		this.calculated = true;
		return this;
	}

	@Override
	public ISimpleSimilarityValue plus(double value, int cardinality) throws SimilarityCalculationException {
		if (Double.isNaN(value))
			return this;
		if (Double.isNaN(this.value))
			return new TestSimpleSimilarityValue(value);
		return new TestSimpleSimilarityValue(this.get() + value);
	}

	@Override
	public ISimpleSimilarityValue minus(double value) throws SimilarityCalculationException {
		if (Double.isNaN(value))
			return this;
		if (Double.isNaN(this.value))
			return new TestSimpleSimilarityValue(-value);
		return new TestSimpleSimilarityValue(this.get() - value);
	}

	@Override
	public ISimpleSimilarityValue times(double value) throws SimilarityCalculationException {
		return new TestSimpleSimilarityValue(this.get() * value);
	}

	@Override
	public ISimpleSimilarityValue divideBy(double value) throws SimilarityCalculationException {
		return new TestSimpleSimilarityValue(this.get() / value);
	}

	@Override
	public ISimpleSimilarityValue divideBy(ISimilarityValue value) throws SimilarityCalculationException {
		return (ISimpleSimilarityValue) super.divideBy(value);
	}

	@Override
	public ISimpleSimilarityValue minus(ISimilarityValue value) throws SimilarityCalculationException {
		return (ISimpleSimilarityValue) super.minus(value);
	}

	@Override
	public ISimpleSimilarityValue plus(ISimilarityValue value) throws SimilarityCalculationException {
		return (ISimpleSimilarityValue) super.plus(value);
	}

	@Override
	public ISimpleSimilarityValue times(ISimilarityValue value) throws SimilarityCalculationException {
		return (ISimpleSimilarityValue) super.times(value);
	}
}
