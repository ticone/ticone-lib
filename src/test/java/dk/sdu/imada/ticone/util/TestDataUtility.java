package dk.sdu.imada.ticone.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.PrimitiveIterator.OfDouble;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import de.wiwie.wiutils.utils.Pair;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

public class TestDataUtility {

	public static ITimeSeriesObjects randomObjects(final Random r, final int numberObjects) {
		return randomObjects(r, numberObjects, 5);
	}

	public static ITimeSeriesObjects randomObjects(final Random r, final int numberObjects,
			final int numberTimepoints) {
		ITimeSeriesObjects objects = new TimeSeriesObjectList();

		for (int o = 0; o < numberObjects; o++) {
			String objId = "Object " + (o + 1);
			double[] signal = r.doubles(numberTimepoints).toArray();
			TimeSeries timeSeries = new TimeSeries(signal);
			ITimeSeriesObject data = new TimeSeriesObject(objId, 1, timeSeries);
			data.addPreprocessedTimeSeries(0, timeSeries);
			objects.add(data);
		}

		return objects;
	}

	private static double getProbabilityOfSeeingRandomTimeSeries(final int numberTimepoints,
			final int numberValueChoices) {
		return Math.pow(1.0 / numberValueChoices, numberTimepoints);
	}

	public static List<ITimeSeriesObjects> enrichedTimeSeriesInRandom(final Random r, final int totalNumberObjects,
			final int numberTimepoints, final int[] numberObjectsEnrichedTimeSeries, final double timeSeriesVariance) {
		if (Arrays.stream(numberObjectsEnrichedTimeSeries).sum() > totalNumberObjects)
			throw new IllegalArgumentException("Number of enriched time-series exceeds total number of objects");
		// generate enriched random time series
		final Set<TimeSeries> enrichedTimeSeriesSet = new HashSet<>();
		while (enrichedTimeSeriesSet.size() < numberObjectsEnrichedTimeSeries.length) {
			final TimeSeries enriched = new TimeSeries(r.doubles(numberTimepoints).toArray());
			// TODO: only take it, if it is different enough from the other enriched time
			// series
			enrichedTimeSeriesSet.add(enriched);
		}
		final List<TimeSeries> enrichedTimeSeries = new ArrayList<>(enrichedTimeSeriesSet);

		int currentCount = 0;
		final List<ITimeSeriesObjects> result = new ArrayList<>();
		for (int e = 0; e < numberObjectsEnrichedTimeSeries.length; e++) {
			final ITimeSeriesObjects objects = new TimeSeriesObjectList();
			for (int o = 0; o < numberObjectsEnrichedTimeSeries[e]; o++) {
				final double[] withNoise = Arrays.stream(enrichedTimeSeries.get(e).asArray())
						.map(v -> Math.max(0.0, Math.min(1.0, r.nextGaussian() * timeSeriesVariance + v))).toArray();
				objects.add(new TimeSeriesObject(String.format("Object %d", ++currentCount), "sample1",
						TimeSeries.of(withNoise)));
			}
			result.add(objects);
		}

		// random time-series
		final ITimeSeriesObjects randomObjects = new TimeSeriesObjectList();
		for (int o = currentCount; o < totalNumberObjects; o++) {
			randomObjects.add(new TimeSeriesObject(String.format("Object %d", o + 1), "sample1",
					new TimeSeries(r.doubles(numberTimepoints).toArray())));
		}
		result.add(randomObjects);
		return result;
	}

	public static void main(String[] args) {
		enrichedTimeSeriesInRandom(new Random(42), 10, 5, new int[] { 2, 2, 2 }, 0.0);
	}

	public static Pair<ClusterObjectMapping, IClusterList> demoData(PrototypeBuilder f,
			boolean setRandomPrototypeTimeSeries) throws Exception {
		ITimeSeriesPrototypeComponentBuilder tsf = PrototypeComponentType.TIME_SERIES.getComponentFactory(f);
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);

		List<String> demoNodes = new ArrayList<>();
		Set<ITimeSeriesObjects> demoClusters = new HashSet<>();

		Random r = new Random(14);

		// generate 3 clusters with 3 objects each;
		for (int c = 0; c < 3; c++) {
			ITimeSeriesObjects p = new TimeSeriesObjectList();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				final TimeSeries timeSeries = new TimeSeries(r.doubles(5).toArray());
				ITimeSeriesObject data = new TimeSeriesObject(objId, 1, timeSeries);
				data.addPreprocessedTimeSeries(0, timeSeries);
				p.add(data);
			}
		}

		for (ITimeSeriesObjects objs : demoClusters) {
			if (setRandomPrototypeTimeSeries)
				tsf.setTimeSeries(r.doubles(5).toArray());
			f.setObjects(objs);
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();

			for (ITimeSeriesObject o : objs)
				p.addObject(o, TestSimpleSimilarityValue.of(r.nextDouble()));
		}

//		ClusterObjectMapping.resetClusterCount();
		return Pair.getPair(demoPom, demoPom.getClusters().asList());
	}

	public static TiconeNetworkImpl demoNetwork(boolean isDirected) throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");
		network.setMultiGraph(false);

		List<String> demoNodes = new ArrayList<>();
		Set<List<String>> demoClusters = new HashSet<>();

		// generate 3 clusters with 3 objects each;
		for (int c = 0; c < 3; c++) {
			List<String> p = new ArrayList<>();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				p.add(objId);
			}
			// create 2 directed edges within cluster
			String edgeSource = p.get(0);
			String edgeTarget = p.get(1);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (List<String> p1 : demoClusters) {
			for (List<String> p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.get(1);
				String edgeTarget = p2.get(0);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.get(1);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.get(2);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}
		return network;
	}

	public static TiconeNetworkImpl parseI2DPPINetwork() throws Exception {
		return TiconeNetworkImpl.parseFromFile("PPI entrez", false, new File(TiconeNetworkImpl.class
				.getResource("/test_files/connectivity/PPI_Human.2col.entrez.unique.tab").getFile()));
	}

	public static ITimeSeriesObjectList parseInfluenzaExpressionData() throws Exception {
		Scanner scan = new Scanner(new File(TestDataUtility.class.getResource(
				"/test_files/clustering/GSE71766_series_matrix_entrez.ticone.fc.cy.txtinfluenza_ctrlCorrected.txt")
				.getFile()));
		return Parser.parseObjects(scan,
				new ImportColumnMapping(2, "Column 2", 1, "Column 1", new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11 }, null),
				true);
	}

	public static ITimeSeriesObjectList parseRhinoExpressionData() throws Exception {
		Scanner scan = new Scanner(new File(TestDataUtility.class
				.getResource(
						"/test_files/clustering/GSE71766_series_matrix_entrez.ticone.fc.cy.txtrhino_ctrlCorrected.txt")
				.getFile()));
		return Parser.parseObjects(scan,
				new ImportColumnMapping(2, "Column 2", 1, "Column 1", new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11 }, null),
				true);
	}

	public static TiconeNetworkImpl randomNetwork(final Random r, final int numberNodes, final boolean isDirected,
			final double edgeProbability) {
		final TiconeNetworkImpl network = new TiconeNetworkImpl(
				String.format("random-network-%d-%b-%3f", numberNodes, isDirected, edgeProbability));

		for (int o = 0; o < numberNodes; o++)
			network.addNode(String.format("Object %d", o + 1));

		if (isDirected) {
			final double[] doubles = r.doubles(numberNodes * numberNodes).toArray();
			int nIdx1 = 0;
			for (final TiconeNetworkNodeImpl n1 : network.getNodeList()) {
				int nIdx2 = 0;
				for (final TiconeNetworkNodeImpl n2 : network.getNodeList()) {
					if (doubles[nIdx1 * nIdx2 + nIdx2] < edgeProbability) {
						network.addEdge(n1, n2, isDirected);
					}
					nIdx2++;
				}
				nIdx1++;
			}
		} else {
			final OfDouble doubles = r.doubles(numberNodes * (numberNodes - 1) / 2 + numberNodes).iterator();

			final List<TiconeNetworkNodeImpl> nodeList = network.getNodeList();
			for (int nIdx1 = 0; nIdx1 < nodeList.size(); nIdx1++) {
				for (int nIdx2 = nIdx1; nIdx2 < nodeList.size(); nIdx2++) {
					if (doubles.next() < edgeProbability) {
						network.addEdge(nodeList.get(nIdx1), nodeList.get(nIdx2), isDirected);
					}
				}
			}
		}

		return network;
	}

	public static TiconeNetworkImpl enrichedSubnetworksInRandom(final Random r, final List<ITimeSeriesObjects> objects,
			final boolean isDirected, final double backgroundEdgeProbability, final double foregroundEdgeProbability) {
		final TiconeNetworkImpl network = new TiconeNetworkImpl(String.format("enriched-subnetworks-%s-%b-%3f-%3f",
				Arrays.toString(objects.stream().mapToInt(objs -> objs.size()).toArray()), isDirected,
				foregroundEdgeProbability, backgroundEdgeProbability));
		// add all nodes
		final List<TiconeNetworkNodeImpl[]> nodes = new ArrayList<>();
		objects.forEach(os -> nodes.add(network.addNodes(os.stream().map(o -> o.getName()).toArray(String[]::new))));

		// generate enriched subnetworks
		for (int os = 0; os < nodes.size() - 1; os++) {
			final TiconeNetworkNodeImpl[] subnetworkNodes = nodes.get(os);

			final int numberNodes = subnetworkNodes.length;
			if (isDirected) {
				final double[] doubles = r.doubles(numberNodes * numberNodes).toArray();
				int nIdx1 = 0;
				for (final TiconeNetworkNodeImpl n1 : subnetworkNodes) {
					int nIdx2 = 0;
					for (final TiconeNetworkNodeImpl n2 : network.getNodeList()) {
						if (doubles[nIdx1 * nIdx2 + nIdx2] < foregroundEdgeProbability) {
							network.addEdge(n1, n2, isDirected);
						}
						nIdx2++;
					}
					nIdx1++;
				}
			} else {
				final OfDouble doubles = r.doubles(numberNodes * (numberNodes - 1) / 2 + numberNodes).iterator();

				for (int nIdx1 = 0; nIdx1 < subnetworkNodes.length; nIdx1++) {
					for (int nIdx2 = nIdx1; nIdx2 < subnetworkNodes.length; nIdx2++) {
						if (doubles.next() < foregroundEdgeProbability) {
							network.addEdge(subnetworkNodes[nIdx1], subnetworkNodes[nIdx2], isDirected);
						}
					}
				}
			}

		}

		// random edges
		for (int os1 = 0; os1 < nodes.size(); os1++) {
			final TiconeNetworkNodeImpl[] nodes1 = nodes.get(os1);
			for (int os2 = isDirected ? 0 : os1; os2 < nodes.size(); os2++) {
				if (os1 == os2 && os1 != nodes.size() - 1)
					continue;

				final TiconeNetworkNodeImpl[] nodes2 = nodes.get(os2);

				for (int nIdx1 = 0; nIdx1 < nodes1.length; nIdx1++) {
					for (int nIdx2 = isDirected ? 0 : nIdx1; nIdx2 < nodes2.length; nIdx2++) {
						if (r.nextDouble() < backgroundEdgeProbability) {
							network.addEdge(nodes1[nIdx1], nodes2[nIdx2], isDirected);
						}
					}
				}
			}
		}
		return network;
	}
}
