/**
 * 
 */
package dk.sdu.imada.ticone.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.SimilarityFeatureValue;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 1, 2018
 *
 */
public class TestStatsCalculation {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testCalculateRSSForPattern() throws Exception {
		double[] data = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 0, 1, 2, 3, 4 });
		ICluster c1 = pom.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 2, 2, 2, 1, 0 });
		ICluster c2 = pom.getClusterBuilder().setPrototype(f.build()).build();

		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();

		pom.addMapping(tsd1, c1, pearson.value(1.0, null));
		pom.addMapping(tsd2, c1, pearson.value(1.0, null));
		pom.addMapping(tsd3, c2, pearson.value(1.0, null));
		pom.addMapping(tsd4, c2, pearson.value(1.0, null));

		ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
				pearson).calculate(pom)).getValue();
	}
}
