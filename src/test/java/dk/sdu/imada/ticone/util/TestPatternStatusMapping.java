package dk.sdu.imada.ticone.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;

public class TestPatternStatusMapping {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testAddPattern() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		IClusterStatusMapping m = new ClusterStatusMapping(new BasicFeatureStore());
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObjectList objs = new TimeSeriesObjectList();
		m.addCluster(c, objs, true, false, false);

		assertTrue(m.getNew().contains(c));

		tsf.setTimeSeries(new double[] { 2, 3, 4, 5 });
		c.setParent(com.getClusterBuilder().setPrototype(f.build()).build());
		m = new ClusterStatusMapping(new BasicFeatureStore());
		m.addCluster(c, objs, true, false, false);
		assertTrue(m.getNew().contains(c));
		assertFalse(m.getDeleted().contains(c));
		assertFalse(m.getSplitClusters().contains(c));

		IClusters children = m.getChildren(c);
		assertTrue(children == null);

		children = m.getClustersToShow();
		assertEquals(new ClusterSet(c), children);
	}

}
