/**
 * 
 */
package dk.sdu.imada.ticone.util;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.feature.AbstractFeature;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.INumericFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.feature.scale.QuantileScalerBuilder;
import dk.sdu.imada.ticone.statistics.FeatureValueSample;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
public class TestQuantileScaler {

	class DummyIntegerFeature extends AbstractFeature<Integer> implements INumericFeature<Integer> {

		/**
		 * @param objectType
		 * @param valueType
		 */
		public DummyIntegerFeature() {
			super(null, Integer.class);
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 7972555631670565762L;

		@Override
		public String getName() {
			return null;
		}

		@Override
		public IFeature<Integer> copy() {
			return null;
		}

		@Override
		protected IFeatureValue<Integer> doCalculate(IObjectWithFeatures object)
				throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
			return null;
		}
	}

	class DummyDoubleFeature extends AbstractFeature<Double> implements INumericFeature<Double> {

		/**
		 * @param objectType
		 * @param valueType
		 */
		public DummyDoubleFeature() {
			super(null, Double.class);
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 6386701079970580466L;

		@Override
		public String getName() {
			return null;
		}

		@Override
		public IFeature<Double> copy() {
			return null;
		}

		@Override
		protected IFeatureValue<Double> doCalculate(IObjectWithFeatures object)
				throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
			return null;
		}
	}

	@Test
	public void test1() throws Exception {
		List<Integer> values = Arrays.asList(1, 3, 7);
		DummyIntegerFeature f = new DummyIntegerFeature();
		FeatureValueSample<Integer> sample = new FeatureValueSample<>(f);
		for (int i = 0; i < values.size(); i++)
			sample.setValue(new TimeSeriesObject("o" + (i + 1)), f.value(values.get(i)));

		double[] quantiles = new double[] { 0.0, 0.5, 1.0 };
		IScaler quantileScaler = new QuantileScalerBuilder<>(f, sample).setQuantiles(quantiles).build();

		assertEquals(0.0, quantileScaler.scale(1), 0.0);
	}

	@Test
	public void test2() throws Exception {
		List<Integer> values = Arrays.asList(1, 3, 7);
		DummyIntegerFeature f = new DummyIntegerFeature();
		FeatureValueSample<Integer> sample = new FeatureValueSample<>(f);
		for (int i = 0; i < values.size(); i++)
			sample.setValue(new TimeSeriesObject("o" + (i + 1)), f.value(values.get(i)));

		double[] quantiles = new double[] { 0.0, 0.5, 1.0 };

		QuantileScalerBuilder<Integer, TimeSeriesObject, DummyIntegerFeature> factory = new QuantileScalerBuilder<>(f,
				sample);
		factory.setQuantiles(quantiles);
		IScaler quantileScaler = factory.build();

		assertEquals(0.0, quantileScaler.scale(2), 0.0);
	}

	@Test
	public void test3() throws Exception {
		List<Integer> values = Arrays.asList(1, 3, 7);
		DummyIntegerFeature f = new DummyIntegerFeature();
		FeatureValueSample<Integer> sample = new FeatureValueSample<>(f);
		for (int i = 0; i < values.size(); i++)
			sample.setValue(new TimeSeriesObject("o" + (i + 1)), f.value(values.get(i)));

		double[] quantiles = new double[] { 0.0, 0.5, 1.0 };

		QuantileScalerBuilder<Integer, TimeSeriesObject, DummyIntegerFeature> factory = new QuantileScalerBuilder<>(f,
				sample);
		factory.setQuantiles(quantiles);
		IScaler quantileScaler = factory.build();

		assertEquals(0.5, quantileScaler.scale(3), 0.0);
	}

	@Test
	public void test4() throws Exception {
		List<Integer> values = Arrays.asList(1, 3, 7);
		DummyIntegerFeature f = new DummyIntegerFeature();
		FeatureValueSample<Integer> sample = new FeatureValueSample<>(f);
		for (int i = 0; i < values.size(); i++)
			sample.setValue(new TimeSeriesObject("o" + (i + 1)), f.value(values.get(i)));

		double[] quantiles = new double[] { 0.0, 0.5, 1.0 };

		QuantileScalerBuilder<Integer, TimeSeriesObject, DummyIntegerFeature> factory = new QuantileScalerBuilder<>(f,
				sample);
		factory.setQuantiles(quantiles);
		IScaler quantileScaler = factory.build();

		assertEquals(1.0, quantileScaler.scale(7), 0.0);
	}

	@Test
	public void test5() throws Exception {
		List<Double> values = Arrays.asList(0.1182647, 0.9745081, 0.8512887, 0.3549522, 0.9336519, 0.2841757, 0.8865038,
				0.8664198, 0.9203799, 0.5184761);
		DummyDoubleFeature f = new DummyDoubleFeature();
		FeatureValueSample<Double> sample = new FeatureValueSample<>(f);
		for (int i = 0; i < values.size(); i++)
			sample.setValue(new TimeSeriesObject("o" + (i + 1)), f.value(values.get(i)));

		double[] quantiles = new double[] { 0.0, 0.5, 1.0 };

		QuantileScalerBuilder<Double, TimeSeriesObject, DummyDoubleFeature> factory = new QuantileScalerBuilder<>(f,
				sample);
		factory.setQuantiles(quantiles);
		IScaler quantileScaler = factory.build();

//		assertArrayEquals(new double[] { 0.1182647, 0.85885425, 0.9745081 }, quantileScaler.icdf, 0.00000001);

		assertEquals(0.0, quantileScaler.scale(0.1182647), 0.0);
		assertEquals(0.5, quantileScaler.scale(0.85885425), 0.0);
		assertEquals(1.0, quantileScaler.scale(0.9745081), 0.0);
	}

	@Test
	public void testValueOutOfRange() throws Exception {
		List<Integer> values = Arrays.asList(1, 3, 7);
		DummyIntegerFeature f = new DummyIntegerFeature();
		FeatureValueSample<Integer> sample = new FeatureValueSample<>(f);
		for (int i = 0; i < values.size(); i++)
			sample.setValue(new TimeSeriesObject("o" + (i + 1)), f.value(values.get(i)));

		double[] quantiles = new double[] { 0.0, 0.5, 1.0 };

		QuantileScalerBuilder<Integer, TimeSeriesObject, DummyIntegerFeature> factory = new QuantileScalerBuilder<>(f,
				sample);
		factory.setQuantiles(quantiles);
		IScaler quantileScaler = factory.build();

		assertEquals(0.0, quantileScaler.scale(0), 0.0);
	}

	@Test
	public void testValueOutOfRange2() throws Exception {
		List<Integer> values = Arrays.asList(1, 3, 7);
		DummyIntegerFeature f = new DummyIntegerFeature();
		FeatureValueSample<Integer> sample = new FeatureValueSample<>(f);
		for (int i = 0; i < values.size(); i++)
			sample.setValue(new TimeSeriesObject("o" + (i + 1)), f.value(values.get(i)));

		double[] quantiles = new double[] { 0.0, 0.5, 1.0 };

		QuantileScalerBuilder<Integer, TimeSeriesObject, DummyIntegerFeature> factory = new QuantileScalerBuilder<>(f,
				sample);
		factory.setQuantiles(quantiles);
		IScaler quantileScaler = factory.build();

		assertEquals(1.0, quantileScaler.scale(8), 0.0);
	}

	@Test
	public void testNaN() throws Exception {
		List<Double> values = Arrays.asList(1.0, 3.0, 7.0);
		DummyDoubleFeature f = new DummyDoubleFeature();
		FeatureValueSample<Double> sample = new FeatureValueSample<>(f);
		for (int i = 0; i < values.size(); i++)
			sample.setValue(new TimeSeriesObject("o" + (i + 1)), f.value(values.get(i)));

		double[] quantiles = new double[] { 0.0, 0.5, 1.0 };

		QuantileScalerBuilder<Double, TimeSeriesObject, DummyDoubleFeature> factory = new QuantileScalerBuilder<>(f,
				sample);
		factory.setQuantiles(quantiles);
		IScaler quantileScaler = factory.build();

		assertEquals(Double.NaN, quantileScaler.scale(Double.NaN), 0.0);
	}

}
