/**
 * 
 */
package dk.sdu.imada.ticone.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 26, 2019
 *
 */
class TestTestDataUtility {

	@ParameterizedTest
	@ValueSource(ints = { 2, 3, 4, 5 })
	public void testThreeEnrichedTimeSeriesInRandom(final int numberEnrichedObjects) {
		List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility.enrichedTimeSeriesInRandom(new Random(42),
				100, 5, new int[] { numberEnrichedObjects, numberEnrichedObjects, numberEnrichedObjects }, 0.0);

		assertEquals(4, enrichedTimeSeriesInRandom.size());

		assertTrue(enrichedTimeSeriesInRandom.get(0).size() >= numberEnrichedObjects);
		assertTrue(enrichedTimeSeriesInRandom.get(1).size() >= numberEnrichedObjects);
		assertTrue(enrichedTimeSeriesInRandom.get(2).size() >= numberEnrichedObjects);
	}

	@ParameterizedTest
	@ValueSource(ints = { 2, 3, 4, 5 })
	public void testFourEnrichedTimeSeriesInRandom(final int numberEnrichedObjects) {
		List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility.enrichedTimeSeriesInRandom(new Random(42),
				100, 5, new int[] { numberEnrichedObjects, numberEnrichedObjects, numberEnrichedObjects,
						numberEnrichedObjects },
				0.0);

		assertEquals(5, enrichedTimeSeriesInRandom.size());

		assertTrue(enrichedTimeSeriesInRandom.get(0).size() >= numberEnrichedObjects);
		assertTrue(enrichedTimeSeriesInRandom.get(1).size() >= numberEnrichedObjects);
		assertTrue(enrichedTimeSeriesInRandom.get(2).size() >= numberEnrichedObjects);
		assertTrue(enrichedTimeSeriesInRandom.get(3).size() >= numberEnrichedObjects);
	}

	@ParameterizedTest
	@ValueSource(ints = { 2, 3, 4, 5, 10, 20 })
	public void testThreeEnrichedTimeSeriesFifteenTimepointsInRandom(final int numberEnrichedObjects) {
		List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility.enrichedTimeSeriesInRandom(new Random(42),
				100, 15, new int[] { numberEnrichedObjects, numberEnrichedObjects, numberEnrichedObjects }, 0.0);

		assertEquals(4, enrichedTimeSeriesInRandom.size());

		assertTrue(enrichedTimeSeriesInRandom.get(0).size() >= numberEnrichedObjects);
		assertTrue(enrichedTimeSeriesInRandom.get(1).size() >= numberEnrichedObjects);
		assertTrue(enrichedTimeSeriesInRandom.get(2).size() >= numberEnrichedObjects);
	}

}
