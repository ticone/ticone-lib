package dk.sdu.imada.ticone.util;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.tuple.Triple;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

public class TestExtractData {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testFindLeastConservedObjectSetsEmptyList() throws Exception {
		ITimeSeriesObjectList datas = new TimeSeriesObjectList();

		ITimeSeriesObjects leastConservedObjects = ExtractData.findLeastConservedObjects(datas, 10,
				new PearsonCorrelationFunction());
		Assert.assertEquals(new TimeSeriesObjectSet(), leastConservedObjects);
	}

	@Test
	public void testFindLeastConservedObjectSets() throws Exception {
		ITimeSeriesObjectList datas = new TimeSeriesObjectList();

		datas.add(new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test4", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test5", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test6", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test7", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test8", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test9", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test10", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 5) }));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(datas);
		absValues.process();

		ITimeSeriesObjects leastConservedObjects = ExtractData.findLeastConservedObjects(datas, 10,
				new PearsonCorrelationFunction());
		Assert.assertEquals(1, leastConservedObjects.size());
		Assert.assertEquals("test10", leastConservedObjects.iterator().next().getName());
	}

	@Test
	public void testFindLeastConservedObjectSets2() throws Exception {
		ITimeSeriesObjectList datas = new TimeSeriesObjectList();

		datas.add(new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test4", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test5", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test6", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test7", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test8", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test9", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 5) }));
		datas.add(new TimeSeriesObject("test10", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 5) }));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(datas);
		absValues.process();

		ITimeSeriesObjects leastConservedObjects = ExtractData.findLeastConservedObjects(datas, 10,
				new PearsonCorrelationFunction());
		Assert.assertEquals(1, leastConservedObjects.size());
		Assert.assertEquals("test9", leastConservedObjects.iterator().next().getName());
	}

	@Test
	public void testFindLeastConservedObjectSets3() throws Exception {
		ITimeSeriesObjectList datas = new TimeSeriesObjectList();

		datas.add(new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test4", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test5", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test6", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test7", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test8", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test9", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test10", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test11", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test12", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test13", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test14", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test15", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test16", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test17", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test18", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test19", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 5) }));
		datas.add(new TimeSeriesObject("test20", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 6) }));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(datas);
		absValues.process();

		ITimeSeriesObjects leastConservedObjects = ExtractData.findLeastConservedObjects(datas, 10,
				new PearsonCorrelationFunction());
		Assert.assertEquals(2, leastConservedObjects.size());
		Iterator<ITimeSeriesObject> it = leastConservedObjects.iterator();
		Assert.assertEquals("test20", it.next().getName());
		Assert.assertEquals("test19", it.next().getName());
	}

	@Test
	public void testGetSimilarityHistogram() throws Exception {
		tsf.setTimeSeries(new double[] { 0.1, 0.3, 0.7 });
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		ICluster p = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObjects datas = new TimeSeriesObjectList();

		pom.addMapping(
				new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
						new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }),
				p, TestSimpleSimilarityValue.of(0.8));
		pom.addMapping(
				new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
						new TimeSeries[] { TimeSeries.of(1, 0, 2), TimeSeries.of(1, 0, 1) }),
				p, TestSimpleSimilarityValue.of(0.8));
		pom.addMapping(
				new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
						new TimeSeries[] { TimeSeries.of(1, 1, 3), TimeSeries.of(1, 2, 4) }),
				p, TestSimpleSimilarityValue.of(0.8));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(pom.getAssignedObjects());
		absValues.process();

		Pair<double[], int[]> histogram = ExtractData.getSimilarityHistogram(pom, null, 10,
				new PearsonCorrelationFunction());
	}

	@Test
	public void testGetLeastFittingDataFromPattern4Objects() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		tsf.setTimeSeries(new double[] { 0.1, 0.3, 0.7 });
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		ICluster p = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject obj1 = new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) });

		ITimeSeriesObject obj2 = new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 0, 2), TimeSeries.of(1, 0, 1) });

		ITimeSeriesObject obj3 = new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 1, 3), TimeSeries.of(1, 2, 4) });

		ITimeSeriesObject obj4 = new TimeSeriesObject("test4", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 1, 3), TimeSeries.of(1, 3, 4) });

		pom.addMapping(obj1, p, TestSimpleSimilarityValue.of(0.7));
		pom.addMapping(obj2, p, TestSimpleSimilarityValue.of(0.8));
		pom.addMapping(obj3, p, TestSimpleSimilarityValue.of(0.6));
		pom.addMapping(obj4, p, TestSimpleSimilarityValue.of(0.6));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(pom.getAssignedObjects());
		absValues.process();

		ITimeSeriesObjects leastFitting = ExtractData.getLeastFittingDataFromPattern(pom, p, 50, similarityFunction);

		assertEquals(2, leastFitting.size());
		assertEquals(obj2, leastFitting.iterator().next());
	}

	@Test
	public void testGetLeastFittingDataFromPattern3Objects() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		tsf.setTimeSeries(new double[] { 0.1, 0.3, 0.7 });
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		ICluster p = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject obj1 = new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) });

		ITimeSeriesObject obj2 = new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 0, 2), TimeSeries.of(1, 0, 1) });

		ITimeSeriesObject obj3 = new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 1, 3), TimeSeries.of(1, 2, 4) });

		pom.addMapping(obj1, p, TestSimpleSimilarityValue.of(0.7));
		pom.addMapping(obj2, p, TestSimpleSimilarityValue.of(0.8));
		pom.addMapping(obj3, p, TestSimpleSimilarityValue.of(0.6));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(pom.getAssignedObjects());
		absValues.process();

		ITimeSeriesObjects leastFitting = ExtractData.getLeastFittingDataFromPattern(pom, p, 50, similarityFunction);

		assertEquals(1, leastFitting.size());
		assertEquals(obj2, leastFitting.iterator().next());
	}

	@Test
	public void testLeastConservedThresholdTwoSamples() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> pair = demoData(true, 2);

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(pair.getLeft().getAssignedObjects());
		absValues.process();

		ITimeSeriesObjectList allObjs = pair.getLeft().getAssignedObjects();

		List<Pair<ITimeSeriesObject, ISimilarityValue>> varianceList = new ArrayList<>();
		for (ITimeSeriesObject timeSeriesData : allObjs) {
			ISimilarityValue var = ExtractData.findObjectConformity(timeSeriesData, similarityFunction);
			varianceList.add(Pair.of(timeSeriesData, var));
		}
		Collections.sort(varianceList, new Comparator<Pair<ITimeSeriesObject, ISimilarityValue>>() {
			@Override
			public int compare(Pair<ITimeSeriesObject, ISimilarityValue> o1,
					Pair<ITimeSeriesObject, ISimilarityValue> o2) {
				return o1.getRight().compareTo(o2.getRight());
			}
		});
		ISimilarityValue median = varianceList.get(varianceList.size() / 2).getRight();

		ITimeSeriesObjects findLeastConservedObjectsWithThreshold = ExtractData
				.findLeastConservedObjectsWithThreshold(allObjs, median, similarityFunction);
		ITimeSeriesObjects filtered = findLeastConservedObjectsWithThreshold;
		assertEquals(varianceList.size() / 2, filtered.size());
	}

	protected Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> demoData(boolean isDirected,
			int numberSamples) throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetwork<?, ?> network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, numberSamples);
				for (int s = 0; s < numberSamples; s++) {
					double[] ts = new double[5];
					for (int i = 0; i < ts.length; i++) {
						ts[i] = r.nextDouble();
					}
					data.addOriginalTimeSeriesToList(s, TimeSeries.of(ts), "Sample " + s);
				}
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}
		return Triple.of(demoPom, network, (IClusterList) new ClusterList(demoClusters));
	}

	@Test
	public void testGetLeastFittingObjectsFromWholeDataset() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		tsf.setTimeSeries(new double[] { 0.1, 0.3, 0.7 });
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		ICluster p = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject obj1 = new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) });

		ITimeSeriesObject obj2 = new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 0, 2), TimeSeries.of(1, 0, 1) });

		ITimeSeriesObject obj3 = new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 1, 3), TimeSeries.of(1, 2, 4) });

		pom.addMapping(obj1, p, TestSimpleSimilarityValue.of(0.7));
		pom.addMapping(obj2, p, TestSimpleSimilarityValue.of(0.8));
		pom.addMapping(obj3, p, TestSimpleSimilarityValue.of(0.6));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(pom.getAssignedObjects());
		absValues.process();

		ITimeSeriesObjects leastFitting = ExtractData.getLeastFittingObjectsFromWholeDataset(pom, 50,
				similarityFunction);
		assertEquals(1, leastFitting.size());
		assertEquals(obj2, leastFitting.iterator().next());
	}
}
