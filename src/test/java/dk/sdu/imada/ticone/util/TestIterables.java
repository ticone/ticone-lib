/**
 * 
 */
package dk.sdu.imada.ticone.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.util.MyParallel.BatchCount;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 30, 2018
 *
 */
public class TestIterables {

	@Test
	public void testBoundIterable() {
		BoundIterable<Long> iterable = new TestBoundIterable(0, 0);

		BoundIterable<Long>.BoundIterator it = iterable.iterator();
		assertFalse(it.hasNext());
	}

	@Test
	public void testBoundIterable2() {
		BoundIterable<Long> iterable = new TestBoundIterable(0, 1);

		BoundIterable<Long>.BoundIterator it = iterable.iterator();
		assertTrue(it.hasNext());
		it.next();
		assertFalse(it.hasNext());
	}

	@Test
	public void testBatchify1() {
		BoundIterable<Long> iterable = new TestBoundIterable(0, 0);

		Iterable<Iterable<Long>> batches = Iterables.batchify(iterable, new BatchCount(1));
		Iterator<Iterable<Long>> it = batches.iterator();
		assertFalse(it.hasNext());
	}

	@Test
	public void testBatchify2() {
		BoundIterable<Long> iterable = new TestBoundIterable(0, 1);

		Iterable<Iterable<Long>> batches = Iterables.batchify(iterable, new BatchCount(1));
		Iterator<Iterable<Long>> it = batches.iterator();
		assertTrue(it.hasNext());
		assertEquals(Arrays.asList(0l), Iterables.toList(it.next()));
		assertFalse(it.hasNext());
	}
}

class TestBoundIterable extends BoundIterable<Long> {

	/**
	 * @param start
	 * @param end
	 */
	public TestBoundIterable(long start, long end) {
		super(start, end);
	}

	@Override
	public BoundIterable<Long> range(long start, long end) {
		return new TestBoundIterable(start, end);
	}

	@Override
	public BoundIterable<Long>.BoundIterator iterator() {
		return new Iterator();
	}

	class Iterator extends BoundIterator {

		long c = start;

		@Override
		public Long next() {
			return c++;
		}

		@Override
		public boolean hasNext() {
			return c < end;
		}
	}
}
