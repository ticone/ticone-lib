/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.similarity.InverseShortestPathSimilarityFunction;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 7, 2018
 *
 */
class TestCompositePrototype {

	@Test
	public void test3Objects2ConnectedComponents() throws Exception {
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("obj1");
		network.addNode("obj2");
		network.addNode("obj3");
		network.addEdge("obj1", "obj2", false);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(ds.asList());
		preprocessor.process();

		TimeSeriesPrototypeComponentBuilder f1 = new TimeSeriesPrototypeComponentBuilder();
		InverseShortestPathSimilarityFunction sf2 = new InverseShortestPathSimilarityFunction(network);
		sf2.initialize();

		f1.setAggregationFunction(new AggregateClusterMeanTimeSeries());
		NetworkLocationPrototypeComponentBuilder f2 = new NetworkLocationPrototypeComponentBuilder();
		f2.setAggregationFunction(new AggregateClusterMedoidNode(network, sf2));
		PrototypeBuilder prototypeFactory = new PrototypeBuilder().setPrototypeComponentBuilders(f1, f2);

		ITimeSeriesObjects cluster = new TimeSeriesObjectList();
		cluster.add(tsd1);
		cluster.add(tsd2);
		cluster.add(tsd3);

		IPrototype prototype = prototypeFactory.setObjects(cluster).build();
		System.out.println(prototype);
		assertArrayEquals(
				new double[] { (0 + 0 + 4) / 3.0, (1 + 1 + 3) / 3.0, 2.0, (3 + 3 + 1) / 3.0, (4 + 4 + 0) / 3.0 },
				PrototypeComponentType.TIME_SERIES.getComponent(prototype).getTimeSeries().asArray(), 0.000001);

//		assertEquals("obj1",
//				PrototypeComponentType.NETWORK_LOCATION.getComponent(prototype).getNetworkLocation().getName());
	}

	@Test
	public void test5Objects1ConnectedComponent() throws Exception {
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		TimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		TimeSeriesObject tsd5 = new TimeSeriesObject("obj5", 2, "p1", TimeSeries.of(data));
		tsd5.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd5.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd5.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("obj1");
		network.addNode("obj2");
		network.addNode("obj3");
		network.addNode("obj4");
		network.addNode("obj5");
		network.addEdge("obj1", "obj2", false);
		network.addEdge("obj2", "obj3", false);
		network.addEdge("obj3", "obj4", false);
		network.addEdge("obj4", "obj5", false);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(ds.asList());
		preprocessor.process();

		TimeSeriesPrototypeComponentBuilder f1 = new TimeSeriesPrototypeComponentBuilder();
		InverseShortestPathSimilarityFunction sf2 = new InverseShortestPathSimilarityFunction(network);
		sf2.initialize();

		f1.setAggregationFunction(new AggregateClusterMeanTimeSeries());
		NetworkLocationPrototypeComponentBuilder f2 = new NetworkLocationPrototypeComponentBuilder();
		f2.setAggregationFunction(new AggregateClusterMedoidNode(network, sf2));
		PrototypeBuilder prototypeFactory = new PrototypeBuilder().setPrototypeComponentBuilders(f1, f2);

		ITimeSeriesObjects cluster = new TimeSeriesObjectList();
		cluster.add(tsd1);
		cluster.add(tsd2);
		cluster.add(tsd3);
		cluster.add(tsd4);
		cluster.add(tsd5);

		IPrototype prototype = prototypeFactory.setObjects(cluster).build();
		System.out.println(prototype);
		assertArrayEquals(
				new double[] { (2 * 0 + 3 * 4) / 5.0, (2 * 1 + 3 * 3) / 5.0, 2.0, (2 * 3 + 3 * 1) / 5.0,
						(2 * 4 + 3 * 0) / 5.0 },
				PrototypeComponentType.TIME_SERIES.getComponent(prototype).getTimeSeries().asArray(), 0.000001);

//		assertEquals("obj3",
//				PrototypeComponentType.NETWORK_LOCATION.getComponent(prototype).getNetworkLocation().getName());
	}

	@Test
	public void test5Objects2ConnectedComponents() throws Exception {
		double[] data = new double[] { 0, 1, 2, 3, 4 };
		TimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data));
		tsd1.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 1, 2, 3, 4 };
		TimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data));
		tsd2.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		data = new double[] { 4, 3, 2, 1, 0 };
		TimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data));
		tsd3.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		TimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data));
		tsd4.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		TimeSeriesObject tsd5 = new TimeSeriesObject("obj5", 2, "p1", TimeSeries.of(data));
		tsd5.addOriginalTimeSeriesToList(1, TimeSeries.of(data), "p2");
		tsd5.addPreprocessedTimeSeries(0, TimeSeries.of(data));
		tsd5.addPreprocessedTimeSeries(1, TimeSeries.of(data));

		ITimeSeriesObjects ds = new TimeSeriesObjectList();
		ds.add(tsd1);
		ds.add(tsd2);
		ds.add(tsd3);

		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("obj1");
		network.addNode("obj2");
		network.addNode("obj3");
		network.addNode("obj4");
		network.addNode("obj5");
		network.addEdge("obj1", "obj2", false);
		network.addEdge("obj2", "obj3", false);
		network.addEdge("obj4", "obj5", false);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(ds.asList());
		preprocessor.process();

		TimeSeriesPrototypeComponentBuilder f1 = new TimeSeriesPrototypeComponentBuilder();
		InverseShortestPathSimilarityFunction sf2 = new InverseShortestPathSimilarityFunction(network);
		sf2.initialize();

		f1.setAggregationFunction(new AggregateClusterMeanTimeSeries());
		NetworkLocationPrototypeComponentBuilder f2 = new NetworkLocationPrototypeComponentBuilder();
		f2.setAggregationFunction(new AggregateClusterMedoidNode(network, sf2));
		PrototypeBuilder prototypeFactory = new PrototypeBuilder().setPrototypeComponentBuilders(f1, f2);

		ITimeSeriesObjects cluster = new TimeSeriesObjectList();
		cluster.add(tsd1);
		cluster.add(tsd2);
		cluster.add(tsd3);
		cluster.add(tsd4);
		cluster.add(tsd5);

		IPrototype prototype = prototypeFactory.setObjects(cluster).build();
		System.out.println(prototype);
		assertArrayEquals(
				new double[] { (0 + 4 * 4) / 5.0, (2 * 1 + 3 * 3) / 5.0, 2.0, (2 * 3 + 3 * 1) / 5.0,
						(2 * 4 + 3 * 0) / 5.0 },
				PrototypeComponentType.TIME_SERIES.getComponent(prototype).getTimeSeries().asArray(), 0.000001);
//
//		assertEquals("obj2",
//				PrototypeComponentType.NETWORK_LOCATION.getComponent(prototype).getNetworkLocation().getName());
	}

}
