package dk.sdu.imada.ticone.parsing;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.MultipleTimeSeriesSignalsForSameSampleParseException;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.ObjectIdParseEmptyStringException;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.io.SampleNameParseEmptyStringException;
import dk.sdu.imada.ticone.io.TimePointSignalParseEmptyStringException;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;

/**
 * Created by christian on 6/12/15.
 */
public class TestParser {
	@Test
	public void testWriteObjectSetsToOutputStream() throws Exception {
		ITimeSeriesObjectList datas = new TimeSeriesObjectList();

		datas.add(new TimeSeriesObject("test1", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test2", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 3) }));
		datas.add(new TimeSeriesObject("test3", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test4", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test5", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test6", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test7", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test8", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test9", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 3, 4) }));
		datas.add(new TimeSeriesObject("test10", new String[] { "sample1", "sample2" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3), TimeSeries.of(1, 2, 5) }));

		ITimeSeriesPreprocessor absValues = new AbsoluteValues();
		absValues.initializeObjects(datas);
		absValues.process();
		File f = new File(getClass().getResource("/test_files/parsing/test_write_tsdata.txt").getFile());
		f.createNewFile();
		Writer w = new FileWriter(f);
		Parser.writeObjectSetsToOutputStream(datas, w);
		w.close();

		Scanner scan = new Scanner(f);
		ITimeSeriesObjects parsedData = Parser.parseObjects(scan);
		scan.close();

		assertEquals(datas, parsedData);
	}

	@Test
	public void testParseObjectSetsWithMissingEntries() throws Exception {
		Assertions.assertThrows(TimePointSignalParseEmptyStringException.class, () -> {
			File f = new File(getClass().getResource("/test_files/parsing/test_tsdata_missing_values.txt").getFile());

			Parser.parseObjects(new Scanner(f), new ImportColumnMapping(1, "", 0, "", new int[] { 2, 3, 4 }, null));
		});
	}

	@Test
	public void testParseObjectSetsWithObjectIdEmpty() throws Exception {
		Assertions.assertThrows(ObjectIdParseEmptyStringException.class, () -> {
			File f = new File(getClass().getResource("/test_files/parsing/test_tsdata_empty_object_id.txt").getFile());

			Parser.parseObjects(new Scanner(f), new ImportColumnMapping(1, "", 0, "", new int[] { 2, 3, 4 }, null));
		});
	}

	@Test
	public void testParseObjectSetsWithSampleNameEmpty() throws Exception {
		Assertions.assertThrows(SampleNameParseEmptyStringException.class, () -> {
			File f = new File(
					getClass().getResource("/test_files/parsing/test_tsdata_empty_sample_name.txt").getFile());

			Parser.parseObjects(new Scanner(f), new ImportColumnMapping(1, "", 0, "", new int[] { 2, 3, 4 }, null));
		});
	}

	@Test
	public void testParseObjectSetsWithSampleDuplicates() throws Exception {
		Assertions.assertThrows(MultipleTimeSeriesSignalsForSameSampleParseException.class, () -> {
			File f = new File(
					getClass().getResource("/test_files/parsing/test_tsdata_with_duplicate_sample.txt").getFile());

			Parser.parseObjects(new Scanner(f), new ImportColumnMapping(1, "", 0, "", new int[] { 2, 3, 4 }, null));
		});
	}
}
