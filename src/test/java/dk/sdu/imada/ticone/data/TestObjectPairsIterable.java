/**
 * 
 */
package dk.sdu.imada.ticone.data;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public class TestObjectPairsIterable {
	@Test
	public void testEmpty() {
		TimeSeriesObjectList objects = new TimeSeriesObjectList(), otherObjects = new TimeSeriesObjectList();
		IObjectPairs objectPairsIterable = new ObjectPair.ObjectPairsFactory().createIterable(objects.toArray(),
				otherObjects.toArray());
		Iterator<IObjectPair> it = objectPairsIterable.iterator();
		assertFalse(it.hasNext());
	}

	@Test
	public void test1Each() {
		TimeSeriesObjectList objects = new TimeSeriesObjectList(new TimeSeriesObject("o1")),
				otherObjects = new TimeSeriesObjectList(new TimeSeriesObject("o2"));
		IObjectPairs objectPairsIterable = new ObjectPair.ObjectPairsFactory().createIterable(objects.toArray(),
				otherObjects.toArray());
		Iterator<IObjectPair> it = objectPairsIterable.iterator();
		assertTrue(it.hasNext());
		it.next();
		assertFalse(it.hasNext());
	}
}
