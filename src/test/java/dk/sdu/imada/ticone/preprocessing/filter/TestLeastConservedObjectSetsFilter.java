package dk.sdu.imada.ticone.preprocessing.filter;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

public class TestLeastConservedObjectSetsFilter {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testOnlyOneSample() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> pair = demoData(true, 1);

		LeastConservedObjectSetsFilter filter = new LeastConservedObjectSetsFilter(100, similarityFunction);
		ITimeSeriesObjects allObjs = pair.getLeft().getAssignedObjects();
		ITimeSeriesObjects filtered = filter.filterObjectSets(allObjs);
		assertEquals(allObjs, filtered);
	}

	@Test
	public void testTwoSamples100Percent() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> pair = demoData(true, 2);

		LeastConservedObjectSetsFilter filter = new LeastConservedObjectSetsFilter(100, similarityFunction);
		ITimeSeriesObjects allObjs = pair.getLeft().getAssignedObjects();
		ITimeSeriesObjects filtered = filter.filterObjectSets(allObjs);
		assertEquals(0, filtered.size());
	}

	@Test
	public void testTwoSamples0Percent() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		Random random = new Random(42);
		Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> pair = demoData(true, 2);

		LeastConservedObjectSetsFilter filter = new LeastConservedObjectSetsFilter(0, similarityFunction);
		ITimeSeriesObjects allObjs = pair.getLeft().getAssignedObjects();
		ITimeSeriesObjects filtered = filter.filterObjectSets(allObjs);
		assertEquals(allObjs.size(), filtered.size());
	}

	@Test
	public void testTwoSamples50Percent() throws Exception {
		int percentageToRemove = 50;
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> pair = demoData(true, 2);

		LeastConservedObjectSetsFilter filter = new LeastConservedObjectSetsFilter(percentageToRemove,
				similarityFunction);
		ITimeSeriesObjects allObjs = pair.getLeft().getAssignedObjects();
		ITimeSeriesObjects filtered = filter.filterObjectSets(allObjs);
		assertEquals((int) Math.ceil(allObjs.size() * (100 - percentageToRemove) / 100.0), filtered.size());
	}

	@Test
	public void testTwoSamples75Percent() throws Exception {
		int percentageToRemove = 75;
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> pair = demoData(true, 2);

		LeastConservedObjectSetsFilter filter = new LeastConservedObjectSetsFilter(percentageToRemove,
				similarityFunction);
		ITimeSeriesObjects allObjs = pair.getLeft().getAssignedObjects();
		ITimeSeriesObjects filtered = filter.filterObjectSets(allObjs);
		assertEquals((int) Math.ceil(allObjs.size() * (100 - percentageToRemove) / 100.0), filtered.size());
	}

	protected Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> demoData(boolean isDirected,
			int numberSamples) throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetwork<?, ?> network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, numberSamples);
				for (int s = 0; s < numberSamples; s++) {
					double[] ts = new double[5];
					for (int i = 0; i < ts.length; i++) {
						ts[i] = r.nextDouble();
					}
					data.addOriginalTimeSeriesToList(s, TimeSeries.of(ts), "Sample " + s);
				}
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		ITimeSeriesPreprocessor pre = new AbsoluteValues();
		pre.initializeObjects(demoPom.getAssignedObjects());
		pre.process();

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}
		return Triple.of(demoPom, network, (IClusterList) new ClusterList(demoClusters));
	}

}
