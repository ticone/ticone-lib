/**
 * 
 */
package dk.sdu.imada.ticone;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 6, 2018
 *
 */
@RunWith(JUnitPlatform.class)
@IncludeEngines("junit-jupiter")
@SelectPackages("dk.sdu.imada.ticone")
@IncludeClassNamePatterns(".*\\.Test.*")
public class TiconeAllTests {

}
