package dk.sdu.imada.ticone.network;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.tuple.Triple;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import cern.colt.bitvector.BitMatrix;
import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureShortestDistance;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkEdgeImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.InverseShortestPathSimilarityFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

public class TestTiconeNetworkImpl {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testAddNode() {
		TiconeNetworkImpl net = new TiconeNetworkImpl("test");
		TiconeNetworkNodeImpl node = net.addNode("Bla");
		Assert.assertEquals("Bla", node.name);
		Assert.assertTrue(net.nodes.contains(node));
	}

	// @Test
	// public void testAddEdge() {
	// TiCoNENetworkImpl net = new TiCoNENetworkImpl("test");
	// TiCoNENetworkNodeImpl n1 = net.addNode("n1"), n2 = net.addNode("n2");
	// TiCoNENetworkEdgeImpl e = net.addEdge(n1, n2, true);
	// Assert.assertTrue(net.containsEdge(n1, n2));
	// Assert.assertFalse(net.containsEdge(n2, n1));
	// }

	// @Test
	// public void testGetUndirectedEdge() {
	// TiCoNENetworkImpl net = new TiCoNENetworkImpl("test");
	// TiCoNENetworkNodeImpl n1 = net.addNode("n1"), n2 = net.addNode("n2");
	// TiCoNENetworkEdgeImpl e = net.addEdge(n1, n2, false);
	//
	// TiCoNENetworkEdgeImpl edge = net.getEdge(n1, n2, false);
	// assertEquals(e, edge);
	// }

	// @Test
	// public void testGetUndirectedEdge2() {
	// TiCoNENetworkImpl net = new TiCoNENetworkImpl("test");
	// TiCoNENetworkNodeImpl n1 = net.addNode("n1"), n2 = net.addNode("n2");
	// TiCoNENetworkEdgeImpl e = net.addEdge(n1, n2, false);
	//
	// TiCoNENetworkEdgeImpl edge = net.getEdge(n2, n1, false);
	// assertEquals(e, edge);
	// }

	// @Test
	// public void testGetUndirectedEdge3() {
	// // if we add a directed edge and we want to retrieve an undirected edge
	// TiCoNENetworkImpl net = new TiCoNENetworkImpl("test");
	// TiCoNENetworkNodeImpl n1 = net.addNode("n1"), n2 = net.addNode("n2");
	// TiCoNENetworkEdgeImpl e = net.addEdge(n1, n2, true);
	//
	// TiCoNENetworkEdgeImpl edge = net.getEdge(n2, n1, false);
	// assertEquals(e, edge);
	// }

	@Test
	public void testGetAdjacentEdgeList() {
		TiconeNetworkImpl net = new TiconeNetworkImpl("test");
		TiconeNetworkNodeImpl n1 = net.addNode("n1"), n2 = net.addNode("n2");
		TiconeNetworkEdgeImpl e = net.addEdge(n1, n2, true);
		List<TiconeNetworkEdgeImpl> expectedEdges = new ArrayList<>();
		expectedEdges.add(e);
		Assert.assertEquals(expectedEdges, net.getAdjacentEdgeList(n1, TiconeEdgeType.OUTGOING));

		expectedEdges = new ArrayList<>();
		Assert.assertEquals(expectedEdges, net.getAdjacentEdgeList(n1, TiconeEdgeType.INCOMING));

		expectedEdges = new ArrayList<>();
		expectedEdges.add(e);
		Assert.assertEquals(expectedEdges, net.getAdjacentEdgeList(n1, TiconeEdgeType.ANY));
	}

	@Test
	public void testGetNeighborList() {
		TiconeNetworkImpl net = new TiconeNetworkImpl("test");
		TiconeNetworkNodeImpl n1 = net.addNode("n1"), n2 = net.addNode("n2");
		net.addEdge(n1, n2, true);
		List<TiconeNetworkNodeImpl> expectedNodes = new ArrayList<>();
		expectedNodes.add(n2);
		Assert.assertEquals(expectedNodes, net.getNeighbors(n1, TiconeEdgeType.OUTGOING));

		expectedNodes = new ArrayList<>();
		Assert.assertEquals(expectedNodes, net.getNeighbors(n1, TiconeEdgeType.INCOMING));

		expectedNodes = new ArrayList<>();
		expectedNodes.add(n2);
		Assert.assertEquals(expectedNodes, net.getNeighbors(n1, TiconeEdgeType.ANY));
	}

	@Test
	public void testAddNodeAttribute() {
		TiconeNetworkImpl net = new TiconeNetworkImpl("test");
		TiconeNetworkNodeImpl node = net.addNode("Bla");

		net.createNodeAttribute("testAttr", Integer.class, false);

		Assert.assertTrue(net.nodeAttributes.containsKey("testAttr"));
		Assert.assertTrue(net.hasNodeAttribute("testAttr"));

		net.setNodeAttribute(node, "testAttr", 3);
		Assert.assertEquals(3l, (long) net.getValue(node, "testAttr", Integer.class));
	}

	@Test
	public void testAddEdgeAttribute() {
		TiconeNetworkImpl net = new TiconeNetworkImpl("test");
		TiconeNetworkNodeImpl n1 = net.addNode("n1"), n2 = net.addNode("n2");
		TiconeNetworkEdgeImpl e = net.addEdge(n1, n2, true);

		net.createEdgeAttribute("testAttr", Integer.class, false);

		Assert.assertTrue(net.edgeAttributes.containsKey("testAttr"));
		Assert.assertTrue(net.hasEdgeAttribute("testAttr"));

		net.setEdgeAttribute(e, "testAttr", 3);
		Assert.assertEquals(3l, (long) net.getValue(e, "testAttr", Integer.class));
	}

	@Test
	public void testGetTotalDirectedEdgeCountNodeDegrees() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataConstantEdgeProbabilityUndirected();
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		int expectedCountSum = network.getEdgeCount();
		int[][] counts = network.getTotalDirectedEdgeCountNodeDegrees();
		int countSum = 0;
		for (int[] p : counts)
			for (int d : p) {
				countSum += d;
			}
		assertEquals(expectedCountSum, countSum);
	}

	@Test
	public void testGetTotalDirectedEdgeProbabilityNodeDegrees() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataConstantEdgeProbabilityUndirected();
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		double[][] probs = network.getTotalDirectedEdgeProbabilityNodeDegrees();
		for (double[] p : probs)
			for (double d : p) {
				assertTrue("Directed edge probabilities have to be within [0,1]", d >= 0.0 && d <= 1.0);
			}

	}

	@Test
	public void testGetConnectedNodesArrayUndirected() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataConstantEdgeProbabilityUndirected();
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		int expectedCountSum = network.getEdgeCount();
		BitMatrix connectedNodes = network.getConnectedNodesArray(true);
		int countSum = 0;
		for (int i = 0; i < connectedNodes.rows(); i++)
			for (int j = 0; j < connectedNodes.columns(); j++) {
				if (connectedNodes.get(i, j))
					countSum++;
			}
		assertEquals(expectedCountSum, countSum);
	}

	@Test
	public void testConnectedComponentOne() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("n1");
		network.addNode("n2");
		network.addNode("n3");
		network.addEdge("n1", "n2", false);
		network.addEdge("n2", "n3", false);
		network.calculateConnectedComponents();

		for (TiconeNetworkNodeImpl n : network.nodes)
			assertEquals(0, n.connectedComponent.getNetworkWideId());
	}

	@Test
	public void testConnectedComponentOneWithLoop() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("n1");
		network.addNode("n2");
		network.addNode("n3");
		network.addEdge("n1", "n2", false);
		network.addEdge("n2", "n3", false);
		network.addEdge("n3", "n1", false);
		network.calculateConnectedComponents();

		for (TiconeNetworkNodeImpl n : network.nodes)
			assertEquals(0, n.connectedComponent.getNetworkWideId());
	}

	@Test
	public void testConnectedComponentOneWithDirectedLoop() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("n1");
		network.addNode("n2");
		network.addNode("n3");
		network.addEdge("n1", "n2", true);
		network.addEdge("n2", "n3", true);
		network.addEdge("n3", "n1", true);
		network.calculateConnectedComponents();

		for (TiconeNetworkNodeImpl n : network.nodes)
			assertEquals(0, n.connectedComponent.getNetworkWideId());
	}

	@Test
	public void testConnectedComponentsTwo() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("n1");
		network.addNode("n2");
		network.addNode("n3");
		network.addEdge("n1", "n2", false);
		network.calculateConnectedComponents();

		assertEquals(0, network.getNode("n1").connectedComponent.getNetworkWideId());
		assertEquals(0, network.getNode("n2").connectedComponent.getNetworkWideId());
		assertEquals(1, network.getNode("n3").connectedComponent.getNetworkWideId());
	}

	@Test
	public void testConnectedComponentsTwoWithLoop() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("n1");
		network.addNode("n2");
		network.addNode("n3");
		network.addEdge("n1", "n2", false);
		network.addEdge("n2", "n1", false);
		network.calculateConnectedComponents();

		assertEquals(0, network.getNode("n1").connectedComponent.getNetworkWideId());
		assertEquals(0, network.getNode("n2").connectedComponent.getNetworkWideId());
		assertEquals(1, network.getNode("n3").connectedComponent.getNetworkWideId());
	}

	@Test
	public void testConnectedComponentsTwoWithDirectedLoop() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("n1");
		network.addNode("n2");
		network.addNode("n3");
		network.addEdge("n1", "n2", true);
		network.addEdge("n2", "n1", true);
		network.calculateConnectedComponents();

		assertEquals(0, network.getNode("n1").connectedComponent.getNetworkWideId());
		assertEquals(0, network.getNode("n2").connectedComponent.getNetworkWideId());
		assertEquals(1, network.getNode("n3").connectedComponent.getNetworkWideId());
	}

	@Test
	public void testConnectedComponentsThree() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("n1");
		network.addNode("n2");
		network.addNode("n3");
		network.calculateConnectedComponents();

		assertEquals(0, network.getNode("n1").connectedComponent.getNetworkWideId());
		assertEquals(1, network.getNode("n2").connectedComponent.getNetworkWideId());
		assertEquals(2, network.getNode("n3").connectedComponent.getNetworkWideId());
	}

	@Test
	public void testConnectedComponentsTwo2() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("obj1");
		network.addNode("obj2");
		network.addNode("obj3");
		network.addNode("obj4");
		network.addNode("obj5");
		network.addEdge("obj1", "obj2", false);
		network.addEdge("obj2", "obj3", false);
		network.addEdge("obj4", "obj5", false);
		network.calculateConnectedComponents();

		assertEquals(0, network.getNode("obj1").connectedComponent.getNetworkWideId());
		assertEquals(0, network.getNode("obj2").connectedComponent.getNetworkWideId());
		assertEquals(0, network.getNode("obj3").connectedComponent.getNetworkWideId());
		assertEquals(1, network.getNode("obj4").connectedComponent.getNetworkWideId());
		assertEquals(1, network.getNode("obj5").connectedComponent.getNetworkWideId());
	}

	@Test
	public void testConnectedComponentEmptyNetwork() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.calculateConnectedComponents();
	}

	@Test
	public void testGetTotalUndirectedEdgeCountNodeDegrees() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataConstantEdgeProbabilityUndirected();
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		int expectedCountSum = 15;
		int[][] counts = network.getTotalUndirectedEdgeCountNodeDegrees();
		int countSum = 0;
		for (int[] p : counts)
			for (int d : p) {
				countSum += d;
			}
		assertEquals(expectedCountSum, countSum);
	}

	@Test
	public void testGetTotalUndirectedEdgeProbabilityNodeDegrees() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoDataConstantEdgeProbabilityUndirected();
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		double[][] probs = network.getTotalUndirectedEdgeProbabilityNodeDegrees();
		for (double[] p : probs)
			for (double d : p) {
				assertTrue("Undirected edge probabilities have to be within [0,1]: " + d, d >= 0.0 && d <= 1.0);
			}

	}

	protected Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> demoDataConstantEdgeProbabilityUndirected()
			throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		IClusterList demoClusters = new ClusterList();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, 1);
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 undirected edges within cluster
			String node1 = p.getObjects().get(0).getName();
			String node2 = p.getObjects().get(1).getName();
			network.addEdge(node1, node2, false);
			network.addEdge(node1, node1, false);
		}

		// create 3 undirected edges between each pair of clusters
		for (int c1 = 0; c1 < demoClusters.size(); c1++) {
			ICluster p1 = demoClusters.get(c1);
			for (int c2 = c1 + 1; c2 < demoClusters.size(); c2++) {
				ICluster p2 = demoClusters.get(c2);
				if (p1.equals(p2))
					continue;
				String node11 = p1.getObjects().get(0).getName();
				String node12 = p1.getObjects().get(1).getName();
				String node21 = p2.getObjects().get(0).getName();
				String node22 = p2.getObjects().get(1).getName();
				network.addEdge(node11, node21, false);
				network.addEdge(node12, node21, false);
				network.addEdge(node12, node22, false);
			}
		}

		// // generate global edges with equal global probability
		// List<TiCoNENetworkNodeImpl> nodeList = network.getNodeList();
		// for (int i = 0; i < network.getNodeCount(); i++) {
		// for (int j = i; j < network.getNodeCount(); j++) {
		// if (r.nextDouble() <= edgeProbability) {
		// network.addEdge(nodeList.get(i), nodeList.get(j), false);
		// }
		// }
		// }

		return Triple.of(demoPom, network, (IClusterList) new ClusterList(demoClusters));
	}

	@Test
	public void testConstantNumberEdgesWhenEdgeCrossver() throws Exception {
		TiconeNetworkImpl network = TiconeNetworkImpl.parseFromFile("test", false,
				new File("network_test_node_degrees_total_graph.txt"));

		for (int i = 0; i < 100; i++) {
			TiconeNetworkImpl copy = network.copy();
			copy.initNodeDegreesUndirected();
			copy.performEdgeCrossovers(16.12, true, 42);

			assertEquals(network.getEdgeCount(), copy.getEdgeCount());
		}
	}

	@Test
	public void testConstantUndirectedNodeDegreesWhenEdgeCrossver() throws Exception {
		TiconeNetworkImpl network = TiconeNetworkImpl.parseFromFile("test", false,
				new File("network_test_node_degrees_total_graph.txt"));
		network.initNodeDegreesUndirected();
		int[] ndu = network.getNodeDegreesUndirected();

		for (int i = 0; i < 100; i++) {
			TiconeNetworkImpl copy = network.copy();
			copy.initNodeDegreesUndirected();
			copy.performEdgeCrossovers(16.12, true, 42);
			int[] ndu2 = copy.getNodeDegreesUndirected();

			assertArrayEquals(ndu, ndu2);
		}
	}

	@Test
	public void testConstantNumberEdgesWhenEdgeCrossver2() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoData(true);
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();

		for (int i = 0; i < 100; i++) {
			TiconeNetworkImpl copy = network.copy();
			copy.initNodeDegreesUndirected();
			copy.performEdgeCrossovers(16.12, true, 42);

			assertEquals(network.getEdgeCount(), copy.getEdgeCount());
		}
	}

	@Test
	public void testConstantUndirectedNodeDegreesWhenEdgeCrossver2() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoData(true);
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		int[] ndu = network.getNodeDegreesUndirected();

		for (int i = 0; i < 100; i++) {
			TiconeNetworkImpl copy = network.copy();
			copy.initNodeDegreesUndirected();
			copy.performEdgeCrossovers(16.12, true, 42);
			int[] ndu2 = copy.getNodeDegreesUndirected();

			assertArrayEquals(ndu, ndu2);
		}
	}

	@Test
	public void testConstantNumberEdgesWhenEdgeCrossver3() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoData(true);
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		long sum = ArraysExt.sum(network.getTotalDirectedEdgeCountNodeDegrees());

		for (int i = 0; i < 100; i++) {
			TiconeNetworkImpl copy = network.copy();
			copy.initNodeDegreesUndirected();
			copy.performEdgeCrossovers(16.12, true, 42);

			assertEquals(sum, ArraysExt.sum(copy.getTotalDirectedEdgeCountNodeDegrees()));
		}
	}

	@Test
	public void testConstantUndirectedNodeDegreesWhenEdgeCrossver3() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoData(true);
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		long sum = ArraysExt.sum(network.getTotalUndirectedEdgeCountNodeDegrees());

		for (int i = 0; i < 100; i++) {
			TiconeNetworkImpl copy = network.copy();
			copy.initNodeDegreesUndirected();
			copy.performEdgeCrossovers(16.12, true, 42);

			assertEquals(sum, ArraysExt.sum(copy.getTotalUndirectedEdgeCountNodeDegrees()));
		}
	}

	@Test
	public void testConstantNumberEdgesWhenEdgeCrossver4() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoData(true);
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		long connectedNodesArray = network.getConnectedNodesArray(false).cardinality();

		for (int i = 0; i < 100; i++) {
			TiconeNetworkImpl copy = network.copy();
			copy.initNodeDegreesUndirected();
			copy.performEdgeCrossovers(16.12, true, 42);

			assertEquals(connectedNodesArray, copy.getConnectedNodesArray(false).cardinality());
		}
	}

	@Test
	public void testConstantUndirectedNodeDegreesWhenEdgeCrossver4() throws Exception {
		Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> pair = demoData(false);
		TiconeNetworkImpl network = pair.getMiddle();
		network.initNodeDegreesUndirected();
		long connectedNodesArray = network.getConnectedNodesArray(true).cardinality();

		for (int i = 0; i < 100; i++) {
			TiconeNetworkImpl copy = network.copy();
			copy.initNodeDegreesUndirected();
			copy.performEdgeCrossovers(16.12, false, 42);

			assertEquals(connectedNodesArray, copy.getConnectedNodesArray(true).cardinality());
		}
	}

	protected Triple<IClusterObjectMapping, TiconeNetworkImpl, IClusterList> demoData(boolean isDirected)
			throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");
		network.setMultiGraph(false);

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, 1);
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			String edgeTarget = p.getObjects().get(1).getName();
			network.addEdge(edgeSource, edgeTarget, isDirected);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				String edgeTarget = p2.getObjects().get(0).getName();
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}
		return Triple.of(demoPom, network, (IClusterList) new ClusterList(demoClusters));
	}

	@Test
	public void testRemoveDuplicateEdges() {
		TiconeNetworkImpl net = new TiconeNetworkImpl("blubb");
		net.isMultiGraph = true;

		net.addNode("obj1");
		net.addNode("obj2");
		net.addNode("obj3");

		// one should be removed
		net.addEdge("obj1", "obj2", true);
		net.addEdge("obj1", "obj2", true);
		net.addEdge("obj1", "obj3", true);

		net.initNodeDegreesUndirected();
		assertEquals(3, net.getNode("obj1").undirectedDegree);
		assertEquals(2, net.getNode("obj2").undirectedDegree);
		assertEquals(1, net.getNode("obj3").undirectedDegree);
		assertEquals(3, net.maxUndirectedNodeDegree);
		assertArrayEquals(new int[][] { new int[] { 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0 },
				new int[] { 0, 1, 2, 0 } }, net.getTotalDirectedEdgeCountNodeDegrees());
		assertArrayEquals(new double[][] { new double[] { 0, 0, 0, 0 }, new double[] { 0, 0, 0, 0 },
				new double[] { 0, 0, 0, 0 }, new double[] { 0, 1, 2, 0 } },
				net.getTotalDirectedEdgeProbabilityNodeDegrees());

		net.isMultiGraph = false;

		net.removeDuplicateEdges();

		assertEquals(2, net.getEdgeCount());
		assertEquals(2, net.getNode("obj1").undirectedDegree);
		assertEquals(1, net.getNode("obj2").undirectedDegree);
		assertEquals(1, net.getNode("obj3").undirectedDegree);
		assertEquals(2, net.maxUndirectedNodeDegree);
		assertArrayEquals(new int[][] { new int[] { 0, 0, 0 }, new int[] { 0, 0, 0 }, new int[] { 0, 2, 0 } },
				net.getTotalDirectedEdgeCountNodeDegrees());
		assertArrayEquals(
				new double[][] { new double[] { 0, 0, 0 }, new double[] { 0, 0, 0 }, new double[] { 0, 1, 0 } },
				net.getTotalDirectedEdgeProbabilityNodeDegrees());
	}
//
//	@Test
//	public void testConsistentEdgeCountsAfterParse() throws Exception {
//		String networkPath = "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs-randomEdgeProb-seed42-samePatternEdgeProb0.9_randomClusterToHaveEqualNodeDegrees.txt";
//		TiCoNENetworkImpl network = TiCoNENetworkImpl.parseFromFile("seed42", true, new File(networkPath));
//
//		int oldEdgeCount = network.edges.size();
//		assertEquals(oldEdgeCount, network.edgeWrapperToEdge.size());
//		assertEquals(oldEdgeCount, countTotalNumberIncoming(network).size() + countTotalNumberSelf(network).size());
//		assertEquals(oldEdgeCount, countTotalNumberOutgoing(network).size() + countTotalNumberSelf(network).size());
//	}
//
//	@Test
//	public void testSameEdgeCountAfterCopy() throws Exception {
//		String networkPath = "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs-randomEdgeProb-seed42-samePatternEdgeProb0.9_randomClusterToHaveEqualNodeDegrees.txt";
//		TiCoNENetworkImpl network = TiCoNENetworkImpl.parseFromFile("seed42", true, new File(networkPath));
//
//		int oldEdgeCount = network.edges.size();
//
//		TiCoNENetworkImpl networkCopy = network.getCopy();
//
//		// same edge count after copy?
//		assertEquals(oldEdgeCount, networkCopy.edges.size());
//		assertEquals(oldEdgeCount, networkCopy.edgeWrapperToEdge.size());
//		assertEquals(oldEdgeCount, networkCopy.edgeToEdgeWrapper.size());
//	}
//
//	@Test
//	public void testSameEdgeCountAfterEdgeCrossover() throws Exception {
//		String networkPath = "/test_files/connectivity/qual-patterns3-objects80-seed42-bg120-samples2-conffalse-tp5-maxVar1.8-minObjSim0.6-hardcodedProbs-randomEdgeProb-seed42-samePatternEdgeProb0.9_randomClusterToHaveEqualNodeDegrees.txt";
//		TiCoNENetworkImpl network = TiCoNENetworkImpl.parseFromFile("seed42", true, new File(networkPath));
//
//		int oldEdgeCount = network.edges.size();
//
//		TiCoNENetworkImpl networkCopy = network.getCopy();
//
//		networkCopy.performEdgeCrossovers(16.12, true);
//
//		assertEquals(oldEdgeCount, networkCopy.edges.size());
//		assertEquals(oldEdgeCount, networkCopy.edgeWrapperToEdge.size());
//		assertEquals(oldEdgeCount, networkCopy.edgeToEdgeWrapper.size());
//
//		Set<TiCoNENetworkEdgeImpl> missingEdges = new HashSet<>(networkCopy.edges);
//		missingEdges.removeAll(countTotalNumberIncoming(networkCopy));
//		System.out.println(missingEdges);
//
//		assertEquals(oldEdgeCount, countTotalNumberIncoming(networkCopy).size() + countTotalNumberSelf(networkCopy).size());
//		assertEquals(oldEdgeCount, countTotalNumberOutgoing(networkCopy).size() + countTotalNumberSelf(networkCopy).size());
//	}
//	
	//
//	@Test
//	public void testNodeDegreesOfTotalGraph() throws Exception {
//		TiCoNENetworkImpl network = TiCoNENetworkImpl.parseFromFile("test", false,
//				new File("network_test_node_degrees_total_graph.txt"));
//
//		int expectedNodes = 20;
//		assertEquals(expectedNodes, network.getNodeCount());
//		int expectedNumberEdges = 210;
//
//		assertEquals(expectedNumberEdges, network.getEdgeCount());
//		assertEquals(expectedNumberEdges, network.getEdgeSet().size());
//
//		// do we find all inserted edges SOMEHWHERE in the incomingEdges map?
//		// (not necessarily at the right position)
//		Set<TiCoNENetworkEdgeImpl> missingEdges = new HashSet<TiCoNENetworkEdgeImpl>(network.getEdgeSet());
//		for (TiCoNENetworkNodeImpl node : network.getIncomingEdges().keySet()) {
//			for (TiCoNENetworkNodeImpl node2 : network.getIncomingEdges().get(node).keySet())
//				missingEdges.removeAll(network.getIncomingEdges().get(node).get(node2));
//		}
//		for (TiCoNENetworkNodeImpl node : network.getSelfEdges().keySet()) {
//			missingEdges.removeAll(network.getSelfEdges().get(node));
//		}
//		assertEquals(0, missingEdges.size());
//
//		// do we find all inserted edges SOMEHWHERE in the outgoingEdges map?
//		// (not necessarily at the right position)
//		missingEdges = new HashSet<TiCoNENetworkEdgeImpl>(network.getEdgeSet());
//		for (TiCoNENetworkNodeImpl node : network.getOutgoingEdges().keySet()) {
//			for (TiCoNENetworkNodeImpl node2 : network.getOutgoingEdges().get(node).keySet())
//				missingEdges.removeAll(network.getOutgoingEdges().get(node).get(node2));
//		}
//		for (TiCoNENetworkNodeImpl node : network.getSelfEdges().keySet()) {
//			missingEdges.removeAll(network.getSelfEdges().get(node));
//		}
//		assertEquals(0, missingEdges.size());
//
//		for (TiCoNENetworkNodeImpl node : network.getIncomingEdges().keySet()) {
//			for (TiCoNENetworkNodeImpl node2 : network.getIncomingEdges().get(node).keySet())
//				assertTrue(network.getIncomingEdges().get(node).get(node2).size() <= 1);
//		}
//
//		assertEquals(expectedNodes, network.getSelfEdges().size());
//
//		for (TiCoNENetworkNodeImpl node : network.getNodeSet())
//			assertTrue(network.getSelfEdges().containsKey(node));
//
//		for (TiCoNENetworkNodeImpl node : network.getNodeSet())
//			assertEquals(expectedNodes,
//					(network.getIncomingEdges().containsKey(node) ? network.getIncomingEdges().get(node).size() : 0)
//							+ (network.getOutgoingEdges().containsKey(node)
//									? network.getOutgoingEdges().get(node).size() : 0)
//							+ network.getSelfEdges().get(node).size());
//
//		Set<TiCoNENetworkNodeImpl> edges = new HashSet<TiCoNENetworkNodeImpl>(network.getOutgoingEdges().keySet());
//		edges.addAll(network.getIncomingEdges().keySet());
//		assertEquals(expectedNodes, edges.size());
//
//		network.initNodeDegreesUndirected();
//
//		int[] nodeDegreesUndirected = network.getNodeDegreesUndirected();
//		int expectedDegree = 20;
//		for (int i = 0; i < nodeDegreesUndirected.length; i++)
//			assertEquals(expectedDegree, nodeDegreesUndirected[i]);
//	}
//
//	private Set<TiCoNENetworkEdgeImpl> countTotalNumberSelf(final TiCoNENetworkImpl network) {
//		Set<TiCoNENetworkEdgeImpl> result = new HashSet<>();
//		for (TiCoNENetworkNodeImpl n : network.getSelfEdges().keySet())
//			result.addAll(network.getSelfEdges().get(n));
//
//		return result;
//	}
//
//	private Set<TiCoNENetworkEdgeImpl> countTotalNumberIncoming(final TiCoNENetworkImpl network) {
//		Set<TiCoNENetworkEdgeImpl> result = new HashSet<>();
//		for (TiCoNENetworkNodeImpl n : network.getIncomingEdges().keySet())
//			for (TiCoNENetworkNodeImpl n2 : network.getIncomingEdges().get(n).keySet())
//				result.addAll(network.getIncomingEdges().get(n).get(n2));
//
//		return result;
//	}
//
//	private Set<TiCoNENetworkEdgeImpl> countTotalNumberOutgoing(final TiCoNENetworkImpl network) {
//		Set<TiCoNENetworkEdgeImpl> result = new HashSet<>();
//		for (TiCoNENetworkNodeImpl n : network.getOutgoingEdges().keySet())
//			for (TiCoNENetworkNodeImpl n2 : network.getOutgoingEdges().get(n).keySet())
//				result.addAll(network.getOutgoingEdges().get(n).get(n2));
//		return result;
//	}

	@Test
	public void testShortestPathCircle() throws Exception {
		Assertions.assertTimeout(Duration.ofSeconds(5), () -> {
			TiconeNetworkImpl network = new TiconeNetworkImpl("network");
			network.addNode("o1");
			network.addNode("o2");
			network.addEdge("o1", "o2", true);

			NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
			f.addPrototypeComponentFactory(nlf);
			InverseShortestPathSimilarityFunction negativeShortestPathSimilarityFunction = new InverseShortestPathSimilarityFunction(
					network);
			negativeShortestPathSimilarityFunction.initialize();
			nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, negativeShortestPathSimilarityFunction));

			TimeSeriesObject o1 = new TimeSeriesObject("o1");
			TimeSeriesObject o2 = new TimeSeriesObject("o2");

			ClusterObjectMapping pom = new ClusterObjectMapping(f);

			tsf.setTimeSeries(new double[] { 1, 2, 3 });
			ICluster c1 = pom.getClusterBuilder().setPrototype(f.setObjects(new TimeSeriesObjectSet(o1)).build())
					.build();
			c1.addObject(o1, TestSimpleSimilarityValue.of(1.0));
			tsf.setTimeSeries(new double[] { 3, 2, 3 });
			ICluster c2 = pom.getClusterBuilder().setPrototype(f.setObjects(new TimeSeriesObjectSet(o2)).build())
					.build();
			c2.addObject(o2, TestSimpleSimilarityValue.of(1.0));

			ClusterPairFeatureShortestDistance sp = new ClusterPairFeatureShortestDistance(false);

			network.getFeatureValue(sp, new ClusterPair(c1, c2));
		});
	}
}
