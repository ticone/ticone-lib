package dk.sdu.imada.ticone.network;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkEdgeImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.IdMapMethod;

public class TestNetworkUtil {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testCreateNewNetwork() throws Exception {
		boolean isDirected = true;
		int numberSamples = 2;
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, numberSamples);
				for (int s = 0; s < numberSamples; s++) {
					double[] ts = new double[5];
					for (int i = 0; i < ts.length; i++) {
						ts[i] = r.nextDouble();
					}
					data.addOriginalTimeSeriesToList(s, TimeSeries.of(ts), "Sample " + s);
				}
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}

		List<TiconeNetworkNodeImpl> nodes = NetworkUtil.getAllNodes(network);
		List<String> nodeIdList = new ArrayList<>();
		for (TiconeNetworkNodeImpl node : nodes)
			nodeIdList.add(node.getName());
		List<Pair<String, String>> edges = NetworkUtil.getEdgeList(network);
		Map<String, List<String>> edgeMap = new HashMap<>();
		for (Pair<String, String> e : edges) {
			if (!edgeMap.containsKey(e.getLeft()))
				edgeMap.put(e.getLeft(), new ArrayList<String>());
			edgeMap.get(e.getLeft()).add(e.getRight());
		}
		TiconeNetworkImpl createdNetwork = NetworkUtil.createNewNetwork(new TiconeNetworkImplFactory(), "bla", null,
				nodeIdList, edgeMap, false, null, null);

		assertEquals(network.nodes, createdNetwork.nodes);
		assertEquals(network.edgeWrapperToEdge.keySet(), network.edgeWrapperToEdge.keySet());
	}

//	@Test
	public void testCreateNewNetworkWithAttributes() throws Exception {
		boolean isDirected = true;
		int numberSamples = 2;
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");
		network.setMultiGraph(true);

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		String[] nodeAttrs = new String[] { "nodeAttr1" };
		for (String nodeAttr : nodeAttrs)
			network.createNodeAttribute(nodeAttr, String.class, false);
		String[] edgeAttrs = new String[] { "edgeAttr1" };
		for (String edgeAttr : edgeAttrs)
			network.createEdgeAttribute(edgeAttr, String.class, false);

		Map<String, Map<String, Object>> nodeAttributes = new HashMap<>();
		Map<String, Map<String, Map<String, Object>>> edgeAttributes = new HashMap<>();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				TiconeNetworkNodeImpl node = network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, numberSamples);
				for (int s = 0; s < numberSamples; s++) {
					double[] ts = new double[5];
					for (int i = 0; i < ts.length; i++) {
						ts[i] = r.nextDouble();
					}
					data.addOriginalTimeSeriesToList(s, TimeSeries.of(ts), "Sample " + s);
				}
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
				nodeAttributes.put(objId, new HashMap<String, Object>());
				for (String nodeAttr : nodeAttrs) {
					nodeAttributes.get(objId).put(nodeAttr, "blubb");
					network.setNodeAttribute(node, nodeAttr, "blubb");
				}
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			TiconeNetworkEdgeImpl edge = network.addEdge(edgeSource, edgeTarget, isDirected);
			edgeAttributes.putIfAbsent(edge.source.getName(), new HashMap<String, Map<String, Object>>());
			edgeAttributes.get(edge.source.getName()).putIfAbsent(edge.target.getName(), new HashMap<String, Object>());
			for (String edgeAttr : edgeAttrs) {
				String attrVal = String.format("blubb %s", edge.toString());
				edgeAttributes.get(edge.source.getName()).get(edge.target.getName()).put(edgeAttr, attrVal);
				network.setEdgeAttribute(edge, edgeAttr, attrVal);
			}
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}

		List<TiconeNetworkNodeImpl> nodes = NetworkUtil.getAllNodes(network);
		List<String> nodeIdList = new ArrayList<>();
		for (TiconeNetworkNodeImpl node : nodes)
			nodeIdList.add(node.getName());
		List<Pair<String, String>> edges = NetworkUtil.getEdgeList(network);
		Map<String, List<String>> edgeMap = new HashMap<>();
		for (Pair<String, String> e : edges) {
			if (!edgeMap.containsKey(e.getLeft()))
				edgeMap.put(e.getLeft(), new ArrayList<String>());
			edgeMap.get(e.getLeft()).add(e.getRight());
		}
		TiconeNetworkImpl createdNetwork = NetworkUtil.createNewNetwork(new TiconeNetworkImplFactory(true), "bla", null,

				nodeIdList, edgeMap, false, nodeAttributes, edgeAttributes);
		createdNetwork.setMultiGraph(true);

		network.edgeAttributes.forEach((a, m) -> {
			System.out.print(a);
			m.forEach((e, v) -> System.out.print("\t" + e.edge.networkWideId + "=" + v));
			System.out.println();
		});
		createdNetwork.edgeAttributes.forEach((a, m) -> {
			System.out.print(a);
			m.forEach((e, v) -> System.out.print("\t" + e.edge.networkWideId + "=" + v));
			System.out.println();
		});
		assertEquals(network, createdNetwork);
	}

	@Test
	public void testCloneNetwork() throws Exception {
		boolean isDirected = true;
		int numberSamples = 2;
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, numberSamples);
				for (int s = 0; s < numberSamples; s++) {
					double[] ts = new double[5];
					for (int i = 0; i < ts.length; i++) {
						ts[i] = r.nextDouble();
					}
					data.addOriginalTimeSeriesToList(s, TimeSeries.of(ts), "Sample " + s);
				}
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}

		TiconeNetworkImpl createdNetwork = NetworkUtil.cloneTiCoNENetwork(new TiconeNetworkImplFactory(), network)
				.getLeft();
		createdNetwork.setName(network.getName());

		assertEquals(network.nodes, createdNetwork.nodes);
		assertEquals(network.edgeWrapperToEdge.keySet(), network.edgeWrapperToEdge.keySet());
	}

	@Test
	public void testGetNodeInducedNetworkEmpty() throws Exception {
		boolean isDirected = true;
		int numberSamples = 2;
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, numberSamples);
				for (int s = 0; s < numberSamples; s++) {
					double[] ts = new double[5];
					for (int i = 0; i < ts.length; i++) {
						ts[i] = r.nextDouble();
					}
					data.addOriginalTimeSeriesToList(s, TimeSeries.of(ts), "Sample " + s);
				}
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}

		Map<String, Map<String, Object>> nodeMap = new HashMap<>();

		TiconeNetworkImpl createdNetwork = NetworkUtil.getNodeInducedNetwork(new TiconeNetworkImplFactory(), network,
				"blubb", null, nodeMap, false, new IdMapMethod() {

					/**
					 * 
					 */
					private static final long serialVersionUID = -3195595849673004543L;

					@Override
					public String getAlternativeId(String objectId) {
						return null;
					}

					@Override
					public boolean isActive() {
						return false;
					}

					@Override
					public IIdMapMethod copy() {
						return null;
					}
				});

		TiconeNetworkImpl expectedNetwork = new TiconeNetworkImpl(createdNetwork.name);
		assertEquals(expectedNetwork.nodes, createdNetwork.nodes);
		assertEquals(expectedNetwork.edges, createdNetwork.edges);
	}

	@Test
	public void testGetNodeInducedNetworkOneNode() throws Exception {
		boolean isDirected = true;
		int numberSamples = 2;
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, numberSamples);
				for (int s = 0; s < numberSamples; s++) {
					double[] ts = new double[5];
					for (int i = 0; i < ts.length; i++) {
						ts[i] = r.nextDouble();
					}
					data.addOriginalTimeSeriesToList(s, TimeSeries.of(ts), "Sample " + s);
				}
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}

		Map<String, Map<String, Object>> nodeMap = new HashMap<>();
		nodeMap.put("Object 1", new HashMap<String, Object>());

		TiconeNetworkImpl createdNetwork = NetworkUtil.getNodeInducedNetwork(new TiconeNetworkImplFactory(), network,
				"blubb", null, nodeMap, false, IdMapMethod.NONE);

		TiconeNetworkImpl expectedNetwork = new TiconeNetworkImpl(createdNetwork.name);
		TiconeNetworkNodeImpl node = expectedNetwork.addNode("Object 1");
		expectedNetwork.createNodeAttribute("inOriginalNetwork", String.class, false);
		expectedNetwork.setNodeAttribute(node, "inOriginalNetwork", true);

		assertEquals(expectedNetwork.nodes, createdNetwork.nodes);
		assertEquals(expectedNetwork.edges, createdNetwork.edges);
	}

	@Test
	public void testGetNodeInducedNetworkTwoNodes() throws Exception {
		boolean isDirected = true;
		int numberSamples = 2;
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetworkImpl network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				ITimeSeriesObject data = new TimeSeriesObject(objId, numberSamples);
				for (int s = 0; s < numberSamples; s++) {
					double[] ts = new double[5];
					for (int i = 0; i < ts.length; i++) {
						ts[i] = r.nextDouble();
					}
					data.addOriginalTimeSeriesToList(s, TimeSeries.of(ts), "Sample " + s);
				}
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}

		Map<String, Map<String, Object>> nodeMap = new HashMap<>();
		nodeMap.put("Object 1", new HashMap<String, Object>());
		nodeMap.put("Object 2", new HashMap<String, Object>());

		TiconeNetworkImpl createdNetwork = NetworkUtil.getNodeInducedNetwork(new TiconeNetworkImplFactory(), network,
				"blubb", null, nodeMap, false, IdMapMethod.NONE);

		TiconeNetworkImpl expectedNetwork = new TiconeNetworkImpl(createdNetwork.name);
		TiconeNetworkNodeImpl node = expectedNetwork.addNode("Object 1");
		expectedNetwork.createNodeAttribute("inOriginalNetwork", String.class, false);
		expectedNetwork.setNodeAttribute(node, "inOriginalNetwork", true);
		node = expectedNetwork.addNode("Object 2");
		expectedNetwork.setNodeAttribute(node, "inOriginalNetwork", true);
		expectedNetwork.addEdge("Object 1", "Object 2", true);
		expectedNetwork.addEdge("Object 2", "Object 1", true);
		assertEquals(expectedNetwork.nodes, createdNetwork.nodes);
		assertEquals(expectedNetwork.edgeWrapperToEdge.keySet(), createdNetwork.edgeWrapperToEdge.keySet());
	}

}
