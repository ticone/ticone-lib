package dk.sdu.imada.ticone.network.kpm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.jupiter.api.Test;

public class TestAlgo {

	@Test
	public void test() {
		Map<String, Map<String, int[]>> expressionIdToNodeMap = new HashMap<String, Map<String, int[]>>();
		Map<String, int[]> blubb = new HashMap<String, int[]>();
		blubb.put("bla", new int[] { 1, 2, 3, 4, 5 });
		expressionIdToNodeMap.put("bla", blubb);
		blubb.put("bla2", new int[] { 1, 2, 1, 0, 4 });
		expressionIdToNodeMap.put("bla2", blubb);
		LinkedList<String[]> edges = new LinkedList<String[]>();
		edges.add(new String[] { "bla", "bla2" });
		Map<String, String> nodeIdToSymbol = new HashMap<String, String>();
		KPMGraph g = new KPMGraph(expressionIdToNodeMap, edges, nodeIdToSymbol);
		List<Result> results = Algo.LCG.run(g);
		results = Algo.GREEDY.run(g);
		results = Algo.EXCEPTIONSUMACO.run(g);
		results = Algo.EXCEPTIONSUMGREEDY.run(g);
		results = Algo.EXCEPTIONSUMOPTIMAL.run(g);
		results = Algo.OPTIMAL.run(g);
	}

	@Test
	public void testWithBenRemoving() throws Exception {
		Map<String, Map<String, int[]>> expressionIdToNodeMap = new HashMap<String, Map<String, int[]>>();
		Map<String, int[]> blubb = new HashMap<String, int[]>();
		blubb.put("bla", new int[] { 1, 2, 3, 4, 5 });
		expressionIdToNodeMap.put("bla", blubb);
		blubb.put("bla2", new int[] { 1, 2, 1, 0, 4 });
		expressionIdToNodeMap.put("bla2", blubb);
		LinkedList<String[]> edges = new LinkedList<String[]>();
		edges.add(new String[] { "bla", "bla2" });
		Map<String, String> nodeIdToSymbol = new HashMap<String, String>();
		KPMGraph g = new KPMGraph(expressionIdToNodeMap, edges, nodeIdToSymbol);
		List<Result> results = Algo.LCG.run(g);
		results = Algo.GREEDY.run(g);
		results = Algo.EXCEPTIONSUMACO.run(g);
		results = Algo.EXCEPTIONSUMGREEDY.run(g);
		results = Algo.EXCEPTIONSUMOPTIMAL.run(g);
		results = Algo.OPTIMAL.run(g);

//		System.out.println("REMOVING BENs...");
		List<Result> benFiltered = new ArrayList<Result>();

		ExecutorService pool = Executors.newFixedThreadPool(Globals.NUMBER_OF_PROCESSORS);
		List<Future<Result>> futures = new ArrayList<Future<Result>>(results.size());
		for (final Result result : results) {
			final BenRemover benr = new BenRemover(g);
			futures.add(pool.submit(new Callable<Result>() {
				@Override
				public Result call() throws Exception {
					return benr.filterBENs(result);
				}
			}));
		}
		for (Future<Result> future : futures) {
			try {
				Result filtered = future.get();
				if (!benFiltered.contains(filtered) && filtered.getVisitedNodes().size() >= Globals.MIN_PATH_SIZE) {
					benFiltered.add(filtered);
				}
			} catch (ExecutionException ex) {
			}
		}
		pool.shutdown();
		results = benFiltered;
	}
}
