package dk.sdu.imada.ticone.refinement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.Constants;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.io.Parser;

/**
 * Created by christian on 9/22/15.
 */
public class TestRefinePatternMean {

	final String file_sep = Constants.file_sep;
	final String path_to_testfiles = Constants.path_to_testfiles;

	double[] patternValues = new double[] { 0, 0, 0, 0, 0 };
	double[] expectedRefinedPattern = new double[] { 0, -1, 1.75, -2, 1.3 };

	@Test
	public void testRefinePattern() throws Exception {
		ITimeSeriesObjectList timeSeriesDatas = null;
		try {
			File timeSeriesFile = new File(getClass()
					.getResource(path_to_testfiles + "refinement" + file_sep + "refinement-testfile.test").getFile());
			Scanner scan = new Scanner(timeSeriesFile);
			timeSeriesDatas = Parser.parseObjects(scan);
			scan.close();
		} catch (FileNotFoundException fnfe) {
			fail("File not found.");
		}
		addPreprocessedTimeSeries(timeSeriesDatas);

		IAggregateCluster<ITimeSeries> IRefinePattern = new AggregateClusterMeanTimeSeries();
		double[] refinedPattern = IRefinePattern.aggregate(timeSeriesDatas).asArray();
		for (int i = 0; i < refinedPattern.length; i++) {
			assertEquals(expectedRefinedPattern[i], refinedPattern[i], 0);
		}
	}

	private void addPreprocessedTimeSeries(ITimeSeriesObjectList timeSeriesDatas) {
		for (int i = 0; i < timeSeriesDatas.size(); i++) {
			timeSeriesDatas.get(i).addPreprocessedTimeSeries(0, timeSeriesDatas.get(i).getOriginalTimeSeries());
		}
	}
}
