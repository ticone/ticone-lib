/**
 * 
 */
package dk.sdu.imada.ticone.refinement;

import static org.junit.Assert.assertArrayEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 1, 2018
 *
 */
public class TestAggregateClusterMostSimilar {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void test() throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();

		double[] data1 = { 0, 1, 2, 3, 4 };
		ITimeSeriesObject tsd1 = new TimeSeriesObject("obj1", 2, "p1", TimeSeries.of(data1));
		tsd1.addOriginalTimeSeriesToList(0, TimeSeries.of(data1), "p2");
		tsd1.addPreprocessedTimeSeries(0, TimeSeries.of(data1));
		tsd1.addPreprocessedTimeSeries(1, TimeSeries.of(data1));

		ITimeSeriesObject tsd2 = new TimeSeriesObject("obj2", 2, "p1", TimeSeries.of(data1));
		tsd2.addOriginalTimeSeriesToList(0, TimeSeries.of(data1), "p2");
		tsd2.addPreprocessedTimeSeries(0, TimeSeries.of(data1));
		tsd2.addPreprocessedTimeSeries(1, TimeSeries.of(data1));

		double[] data2 = new double[] { 4, 3, 2, 1, 0 };
		ITimeSeriesObject tsd3 = new TimeSeriesObject("obj3", 2, "p1", TimeSeries.of(data2));
		tsd3.addOriginalTimeSeriesToList(0, TimeSeries.of(data2), "p2");
		tsd3.addPreprocessedTimeSeries(0, TimeSeries.of(data2));
		tsd3.addPreprocessedTimeSeries(1, TimeSeries.of(data2));

		double[] data3 = new double[] { 0, 1, 2, 1, 0 };
		ITimeSeriesObject tsd4 = new TimeSeriesObject("obj4", 2, "p1", TimeSeries.of(data3));
		tsd4.addOriginalTimeSeriesToList(0, TimeSeries.of(data3), "p2");
		tsd4.addPreprocessedTimeSeries(0, TimeSeries.of(data3));
		tsd4.addPreprocessedTimeSeries(1, TimeSeries.of(data3));

		double[] data4 = new double[] { 0, 1, 1, 1, 0 };
		ITimeSeriesObject tsd5 = new TimeSeriesObject("obj5", 2, "p1", TimeSeries.of(data4));
		tsd5.addOriginalTimeSeriesToList(0, TimeSeries.of(data4), "p2");
		tsd5.addPreprocessedTimeSeries(0, TimeSeries.of(data4));
		tsd5.addPreprocessedTimeSeries(1, TimeSeries.of(data4));

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		double[] p1 = new double[] { 0, 1, 2, 3, 4 };
		tsf.setTimeSeries(p1);
		ICluster c1 = pom.getClusterBuilder().setPrototype(f.build()).build();
		double[] p2 = new double[] { 2, 2, 2, 1, 0 };
		tsf.setTimeSeries(p2);
		ICluster c2 = pom.getClusterBuilder().setPrototype(f.build()).build();

		pom.addMapping(tsd1, c1, pearson.value(1.0, null));
		pom.addMapping(tsd2, c1, pearson.value(1.0, null));
		pom.addMapping(tsd3, c2, pearson.value(1.0, null));
		pom.addMapping(tsd4, c2, pearson.value(1.0, null));
		pom.addMapping(tsd5, c2, pearson.value(1.0, null));

		AggregateClusterMedoidTimeSeries a = new AggregateClusterMedoidTimeSeries(pearson);
		double[] aggregateCluster = a.aggregate(c1.getObjects()).asArray();
		assertArrayEquals(data1, aggregateCluster, 0.0);

		aggregateCluster = a.aggregate(c2.getObjects()).asArray();
		assertArrayEquals(data3, aggregateCluster, 0.0);
	}

}
