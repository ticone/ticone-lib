package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wiwie.wiutils.utils.Pair;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;

public class TestKMeans {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void test() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<KMeansClusteringMethod> initialClusteringInterface = new KMeansClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(5, bla.getLatestClustering().getClusters().size());

		int numberIterations = bla.getHistory().getIterationNumber();
		assertEquals(1, numberIterations);
	}

}
