/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureEntropy;
import dk.sdu.imada.ticone.feature.IClusterFeatureEntropy;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.generate.TimeSeriesTestClass;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.util.AggregationException;
import dk.sdu.imada.ticone.util.TestDataUtility;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 11, 2017
 *
 */
public class TestClusterFeatureEntropy {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testEmptyCluster() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		IFeatureValue<Double> calculate = new ClusterFeatureEntropy(10).calculate(c);
		assertEquals(0.0, calculate.getValue(), 0.0);
	}

	@Test
	public void blubb() throws Exception {
		List<ICluster> cluster = new ArrayList<>();
		ClusterObjectMapping clustering = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 0, 0, 0, 0, 0 });
		cluster.add(clustering.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 1, 0, 0, 0, 0 });
		cluster.add(clustering.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 1, 0, 0, 0 });
		cluster.add(clustering.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 1, 0, 0 });
		cluster.add(clustering.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 0, 1, 0 });
		cluster.add(clustering.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 0, 0, 1 });
		cluster.add(clustering.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 3, 0, 5, 1, 0 });
		cluster.add(clustering.getClusterBuilder().setPrototype(f.build()).build());

		IClusterObjectMapping com = TimeSeriesTestClass.generateObjectsForClusterPrototypes(clustering,
				TestSimpleSimilarityValue.of(-1), 3, 10);
		ITimeSeriesObjectList objs = com.getAssignedObjects();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IClusterFeatureEntropy prior = new ClusterFeatureEntropy(100);

		for (ICluster c : com.getClusters()) {
			IFeatureValue<Double> calculate = prior.calculate(c);
			System.out.println(c + "\t" + calculate.getValue());
		}
	}

	@Test
	public void testEntropyConstantClusterInfluenza() throws Exception {
		ITimeSeriesObjectList objs = TestDataUtility.parseInfluenzaExpressionData();
		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(objs);
		av.process();

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		Map<Integer, ICluster> idToCluster = new HashMap<>();
		BufferedReader r = new BufferedReader(
				new FileReader(getClass().getResource("/test_files/clustering/clustering_influenza.txt").getFile()));

		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				av.getMinValue(), av.getMaxValue(), 10, 10);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePattern);

		while (r.ready()) {
			String nextLine = r.readLine();
			String[] split = nextLine.split("\t");
			int id = Integer.valueOf(split[0]);
			if (!idToCluster.containsKey(id)) {
				tsf.setTimeSeries(new double[0]);
				ICluster cluster = pom.clusterBuilder.setPrototype(f.build()).setClusterNumber(id)
						.setInternalClusterNumber((long) id).setIsKeep(false).build();
				idToCluster.put(id, cluster);
			}
			pom.addMapping(idToObj.get(split[1]), idToCluster.get(id), TestSimpleSimilarityValue.of(1.0));
		}
		r.close();

		for (ICluster p : pom.getClusters()) {
			ITimeSeriesObjects patternsData = p.getObjects();
			ITimeSeries patternArr;
			try {
				patternArr = refinePattern.aggregate(patternsData);
			} catch (AggregationException e) {
				throw new ClusterOperationException(e);
			}
			patternArr = discretizePattern.discretizeObjectTimeSeries(patternArr);
			tsf.setTimeSeries(patternArr);
			p.updatePrototype(f.build());
		}

		// remove objects not in the clustering
		objs.retainAll(pom.getAssignedObjects());

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IClusterFeatureEntropy prior = new ClusterFeatureEntropy(100);

		for (ICluster c : pom.getClusters()) {
			double l = prior.calculate(c).getValue();
			System.out.println(c + "\t" + l);
		}
	}

}
