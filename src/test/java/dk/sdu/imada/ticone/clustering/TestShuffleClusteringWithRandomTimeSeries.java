/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.clustering.validity.DunnIndex;
import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ShuffleDatasetGlobally;
import dk.sdu.imada.ticone.data.ShuffleTimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusteringFeatureNumberClusters;
import dk.sdu.imada.ticone.feature.ClusteringFeatureValidity;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.statistics.BasicCalculatePValues;
import dk.sdu.imada.ticone.statistics.MultiplyPValues;
import dk.sdu.imada.ticone.util.TestDataUtility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 22, 2019
 *
 */
public class TestShuffleClusteringWithRandomTimeSeries {

	static int[] numberObjectsFactory() {
		return new int[] { 10, 20, 50, 100, 150, 200, 250, 300, 500, 1000, 2000, 5000, 10000, 15000 };
	}

	@Tag("long-running")
	@ParameterizedTest
	@MethodSource(value = { "numberObjectsFactory" })
	public void testShuffleRandomTimeSeries(int numberObjects) throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();

		ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(), numberObjects).asList();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 10, 10);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> clusteringMethod = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(clusteringMethod, 10, 42);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcess bla = new BasicClusteringProcess(new TiconeClusteringResultFactory(), null,
				clusteringMethod, initialClustering, null, null, prototypeFactory, similarityFunction, null,
				preprocessor, 42);
		bla.start();
		ClusterObjectMapping pom = Objects.requireNonNull(bla.doIteration());

		final CreateRandomTimeSeries createRandomTimeSeries = new CreateRandomTimeSeries();
		createRandomTimeSeries.setAllObjects(preprocessor.getObjects());

		ShuffleClusteringWithRandomPrototypeTimeSeries shuffle = new ShuffleClusteringWithRandomPrototypeTimeSeries(
				similarityFunction, prototypeFactory, createRandomTimeSeries);
		shuffle.setRandomNumberClusters(true);

		long startTime = System.currentTimeMillis();
		bla.calculatePValues(
				new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER, ObjectType.CLUSTERING), shuffle,
						Arrays.asList(
								new BasicFitnessScore(ObjectType.CLUSTER,
										new IArithmeticFeature[] {
												new ClusterFeatureAverageSimilarity(similarityFunction) },
										null, null),
								new BasicFitnessScore(ObjectType.CLUSTERING,
										new IArithmeticFeature[] {
												new ClusteringFeatureValidity(new DunnIndex(), similarityFunction) },
										null, null)),
						new List[] { Arrays.asList(new ClusterFeatureNumberObjects()),
								Arrays.asList(new ClusteringFeatureNumberClusters()) },
						Arrays.asList(false, false), new MultiplyPValues(), 1000),
				42);
		System.out.println(System.currentTimeMillis() - startTime);
	}

	@Tag("long-running")
	@ParameterizedTest
	@MethodSource(value = { "numberObjectsFactory" })
	public void testPermutePrototypeTimeSeries(int numberObjects) throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		int numberOfInitialClusters = 10;

		ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(), numberObjects).asList();
		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> clusteringMethod = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(clusteringMethod,
				numberOfInitialClusters, 42);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcess bla = new BasicClusteringProcess(new TiconeClusteringResultFactory(), null,
				clusteringMethod, initialClustering, null, null, prototypeFactory, similarityFunction, null,
				preprocessor, 42);
		bla.start();
		ClusterObjectMapping pom = Objects.requireNonNull(bla.doIteration());

		List<IArithmeticFeature<? extends Comparable<?>>> featureList = new ArrayList<>();
		featureList.add(new ClusterFeatureNumberObjects());
		featureList.add(new ClusterFeatureAverageSimilarity(similarityFunction));

		final ShuffleTimeSeries createRandomTimeSeries = new ShuffleTimeSeries();

		ShuffleClusteringByShufflingPrototypeTimeSeries shuffle = new ShuffleClusteringByShufflingPrototypeTimeSeries(
				similarityFunction, prototypeFactory, createRandomTimeSeries);

		long startTime = System.currentTimeMillis();
		bla.calculatePValues(
				new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER, ObjectType.CLUSTERING), shuffle,
						Arrays.asList(
								new BasicFitnessScore(ObjectType.CLUSTER,
										new IArithmeticFeature[] {
												new ClusterFeatureAverageSimilarity(similarityFunction) },
										null, null),
								new BasicFitnessScore(ObjectType.CLUSTERING,
										new IArithmeticFeature[] {
												new ClusteringFeatureValidity(new DunnIndex(), similarityFunction) },
										null, null)),
						new List[] { Arrays.asList(new ClusterFeatureNumberObjects()),
								Arrays.asList(new ClusteringFeatureNumberClusters()) },
						Arrays.asList(false, false), new MultiplyPValues(), 1000),
				42);
		System.out.println(System.currentTimeMillis() - startTime);
	}

	static final int[] blubb = {};

	@Tag("long-running")
	@ParameterizedTest
	@MethodSource(value = { "numberObjectsFactory" })
	public void testShuffleDatasetGlobally(int numberObjects) throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		int numberOfInitialClusters = 10;

		ITimeSeriesObjectList objs = TestDataUtility.randomObjects(new Random(42), numberObjects).asList();
		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> clusteringMethodBuilder = new CLARAClusteringMethodBuilder()
				.setPamkBuilder(new PAMKClusteringMethodBuilder().setNstart(5))
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(clusteringMethodBuilder,
				numberOfInitialClusters, 42);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcess bla = new BasicClusteringProcess(new TiconeClusteringResultFactory(), null,
				clusteringMethodBuilder, initialClustering, null, null, prototypeFactory, similarityFunction, null,
				preprocessor, 42);
		bla.start();
		ClusterObjectMapping pom = Objects.requireNonNull(bla.doIteration());

		List<IArithmeticFeature<? extends Comparable<?>>> featureList = new ArrayList<>();
		featureList.add(new ClusterFeatureNumberObjects());
		featureList.add(new ClusterFeatureAverageSimilarity(similarityFunction));

		ShuffleClusteringByShufflingDataset shuffle = new ShuffleClusteringByShufflingDataset(clusteringMethodBuilder,
				numberOfInitialClusters, new ShuffleDatasetGlobally());
		shuffle.setRandomNumberClusters(true);

		long startTime = System.currentTimeMillis();
		bla.calculatePValues(
				new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER, ObjectType.CLUSTERING), shuffle,
						Arrays.asList(
								new BasicFitnessScore(ObjectType.CLUSTER,
										new IArithmeticFeature[] {
												new ClusterFeatureAverageSimilarity(similarityFunction) },
										null, null),
								new BasicFitnessScore(ObjectType.CLUSTERING,
										new IArithmeticFeature[] {
												new ClusteringFeatureValidity(new DunnIndex(), similarityFunction) },
										null, null)),
						new List[] { Arrays.asList(new ClusterFeatureNumberObjects()),
								Arrays.asList(new ClusteringFeatureNumberClusters()) },
						Arrays.asList(false, false), new MultiplyPValues(), 1000),
				42);
		System.out.println(System.currentTimeMillis() - startTime);
	}

}
