package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitClusterContainer;
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterBasedOnTwoMostDissimilarObjects;
import dk.sdu.imada.ticone.clustering.suggestclusters.IClusterSuggestion;
import dk.sdu.imada.ticone.clustering.suggestclusters.ISuggestNewCluster;
import dk.sdu.imada.ticone.clustering.suggestclusters.SuggestClusterException;
import dk.sdu.imada.ticone.clustering.suggestclusters.SuggestClustersBasedOnLeastFitting;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ShuffleDatasetGlobally;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.preprocessing.filter.LeastVaryingObjectSetsFilter;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.statistics.BasicCalculatePValues;
import dk.sdu.imada.ticone.statistics.ICalculatePValues;
import dk.sdu.imada.ticone.statistics.IPValueCalculationResult;
import dk.sdu.imada.ticone.statistics.MultiplyPValues;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;
import dk.sdu.imada.ticone.variance.IObjectSetVariance;
import dk.sdu.imada.ticone.variance.StandardVariance;

public class TestBasicClusteringProcess {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
//		ClusterObjectMapping.resetClusterCount();
	}

	@Test
	public void testDoFirstIteration() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42l);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(5, bla.getLatestClustering().getClusters().size());
	}

	@Test
	public void testDoTwoIterations() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);
		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(5, bla.getLatestClustering().getClusters().size());
		bla.doIteration();
		assertTrue(5 >= bla.getLatestClustering().getClusters().size());
	}

	@Test
	public void testConstructor() throws Exception {
		Assertions.assertThrows(ClusterOperationException.class, () -> {
			PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
			IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
					0, 1, 4, 4);
			IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
			Random random = new Random(42);
			tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

			IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
					.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
			de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

			IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface, 0,
					42);

			ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
			preprocessor.process();

			tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
					.setDiscretizeFunction(discretizePatternFunction);

			BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
			IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
					new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface,
					initialClustering, new LoadDataFromCollection(pair.getFirst().getAssignedObjects()),
					new PreprocessingSummary(), f, similarityFunction, TimePointWeighting.NONE, preprocessor, 42l,
					Collections.emptySet());
			bla.start();

		});
	}

	@Test
	public void testAddCluster() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		new ShuffleDatasetGlobally();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(5, bla.getLatestClustering().getClusters().size());
		tsf.setTimeSeries(new double[] { 1, 2, 3, 5, 23, 41, 23 });
		ICluster addCluster = bla.addCluster(f.build());
		assertTrue(addCluster != null);
		assertEquals(6, bla.getLatestClustering().getClusters().size());
	}

	public void testSuggestClusterLeastFittingZeroObjects() throws Exception {
		Assertions.assertThrows(SuggestClusterException.class, () -> {
			PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
			int numberOfInitialClusters = 5;
			IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
					0, 1, 4, 4);
			IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
			Random random = new Random(42);

			tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

			IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
					.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
			de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

			IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
					numberOfInitialClusters, 42);

			ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
			preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
			preprocessor.process();

			tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
					.setDiscretizeFunction(discretizePatternFunction);

			BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
			IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
					new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface,
					initialClustering, new LoadDataFromCollection(pair.getFirst().getAssignedObjects()),
					new PreprocessingSummary(), f, similarityFunction, TimePointWeighting.NONE, preprocessor, 42l,
					Collections.emptySet());
			bla.start();

			bla.doIteration();
			ISuggestNewCluster suggest = new SuggestClustersBasedOnLeastFitting(1, 5, initialClusteringInterface,
					similarityFunction, 42);
			assertEquals(5, bla.getLatestClustering().getClusters().size());
			bla.suggestNewClusters(suggest);
		});
	}

	@Test
	public void testSuggestClusterLeastFitting() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(5, bla.getLatestClustering().getClusters().size());
		ISuggestNewCluster suggest = new SuggestClustersBasedOnLeastFitting(1, 50, initialClusteringInterface,
				similarityFunction, 42);
		IClusterSuggestion trans = bla.suggestNewClusters(suggest);
		assertEquals(4, trans.getObjectsToDelete().size());
		bla.applySuggestedClusters(suggest, trans);
		assertEquals(6, bla.getLatestClustering().getClusters().size());
	}

	@Test
	public void testSplitCluster() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		new ShuffleDatasetGlobally();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		pair.getFirst();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(5, bla.getLatestClustering().getClusters().size());
		tsf.setTimeSeries(new double[] { 1, 2, 3, 5, 23 });
		ICluster newCluster = bla.addCluster(f.build());
		TimeSeriesObject o1 = new TimeSeriesObject("test1", 1, TimeSeries.of(1, 2, 3, 4, 5));
		o1.addPreprocessedTimeSeries(0, TimeSeries.of(1, 2, 3, 4, 5));
		newCluster.addObject(o1, similarityFunction.value(1.0, o1.getObjectType()));
		TimeSeriesObject o2 = new TimeSeriesObject("test2", 1, TimeSeries.of(1, 2, 3, 2, 0));
		o2.addPreprocessedTimeSeries(0, TimeSeries.of(1, 2, 3, 2, 0));
		newCluster.addObject(o2, similarityFunction.value(1.0, o2.getObjectType()));
		preprocessor.initializeObjects(bla.getLatestClustering().getAssignedObjects());
		preprocessor.process();

		assertEquals(6, bla.getLatestClustering().getClusters().size());
		ISplitCluster splitCluster = new SplitClusterBasedOnTwoMostDissimilarObjects(similarityFunction, f);
		ISplitClusterContainer cont = bla.splitCluster(splitCluster, newCluster.getClusterNumber());
		assertEquals(6, bla.getLatestClustering().getClusters().size());
		bla.applySplitClusters(splitCluster, cont);
		assertEquals(7, bla.getLatestClustering().getClusters().size());
	}

	@Test
	public void testMerge2Cluster() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		process.start();

		process.doIteration();
		int clusters = process.getLatestClustering().getClusters().size();
		Iterator<ICluster> it = process.getLatestClustering().getClusters().iterator();
		process.mergeClusters(new int[] { it.next().getClusterNumber(), it.next().getClusterNumber() });
		assertEquals(clusters - 1, process.getLatestClustering().getClusters().size());

	}

	@Test
	public void testDeleteObjectsFromClusters() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);
		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> clusteringMethod = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, clusteringMethod,
				new PrespecifiedInitialClustering(pair.getFirst()),
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		final int initialNumberObjects = pair.getFirst().getAssignedObjects().size();
		int expectedNumberObjects = initialNumberObjects - 5;
		bla.removeObjectsFromClusters(
				new TimeSeriesObjectList(bla.getLatestClustering().getAssignedObjects().subList(0, 5)));
		assertEquals(expectedNumberObjects, bla.getLatestClustering().getAssignedObjects().size());

		assertEquals(initialNumberObjects, bla.getObjects().size());
		bla.doIteration();
		assertEquals(initialNumberObjects, bla.getLatestClustering().getAssignedObjects().size());
		assertEquals(initialNumberObjects, bla.getObjects().size());
	}

	@Test
	public void testDeleteDataBoth() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);
		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(5, bla.getLatestClustering().getClusters().size());
		ICluster cluster = bla.getLatestClustering().getClusters().iterator().next();
		ITimeSeriesObjects datas = cluster.getObjects();
		int expectedNumberObjects = bla.getLatestClustering().getAssignedObjects().size() - datas.size();
		bla.deleteData(cluster.getClusterNumber(), IClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);
		assertEquals(4, bla.getLatestClustering().getClusters().size());
		assertEquals(expectedNumberObjects, bla.getObjects().size());
	}

	@Test
	public void testDeleteOnlyCluster() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		new ShuffleDatasetGlobally();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);
		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		process.start();

		process.doIteration();
		assertEquals(5, process.getLatestClustering().getClusters().size());
		ICluster cluster = process.getLatestClustering().getClusters().iterator().next();
		int previousNumberObjects = process.getLatestClustering().getAssignedObjects().size();
		process.deleteData(cluster.getClusterNumber(), IClusterObjectMapping.DELETE_METHOD.ONLY_PROTOTYPE);
		// perform 1 iteration to reassign objects to most similar prototypes
		process.doIteration();
		assertEquals(4, process.getLatestClustering().getClusters().size());
		assertEquals(previousNumberObjects, process.getLatestClustering().getAssignedObjects().size());
		assertEquals(previousNumberObjects, process.getObjects().size());
	}

	@Test
	public void testDeleteOnlyObjects() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);
		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> clusteringMethod = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		de.wiwie.wiutils.utils.Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		assertEquals(9, pair.getFirst().getAssignedObjects().size());

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(clusteringMethod,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, clusteringMethod, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		process.start();

		process.doIteration();
		assertEquals(5, process.getLatestClustering().getClusters().size());
		ICluster cluster = process.getLatestClustering().getClusters().iterator().next();
		ITimeSeriesObjects datas = cluster.getObjects();
		final int initialNumberObjects = process.getLatestClustering().getAssignedObjects().size();
		int expectedNumberObjects = initialNumberObjects - datas.size();
		process.deleteData(cluster.getClusterNumber(), IClusterObjectMapping.DELETE_METHOD.ONLY_OBJECTS);
		assertEquals(5, process.getLatestClustering().getClusters().size());
		assertEquals(expectedNumberObjects, process.getObjects().size());
		assertEquals(expectedNumberObjects, process.getLatestClustering().getAssignedObjects().size());
		process.doIteration();
		assertEquals(expectedNumberObjects, process.getObjects().size());
		assertEquals(expectedNumberObjects, process.getLatestClustering().getAssignedObjects().size());
	}

	protected Pair<IClusterObjectMapping, IClusterList> demoDataConstantCluster() throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);

		List<String> demoNodes = new ArrayList<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.clusterBuilder.setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				double[] ts = new double[5];
				for (int i = 0; i < ts.length; i++) {
					if (c == 0)
						ts[i] = 1;
					else
						ts[i] = r.nextDouble();
				}
				ITimeSeriesObject data = new TimeSeriesObject(objId, 1, TimeSeries.of(ts));
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
		}

		return Pair.of(demoPom, (IClusterList) new ClusterList(demoClusters));
	}

	@Test
	public void testNonNormalizedDataDoFirstIteration() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 10;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 15, 15);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/easyDataset-hardcodedPatterns-objects40-seed1342-bg240-samples2-identicaltrue.txt.2")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(0, bla.getLatestClustering().getClusters().size());
	}

	@Test
	public void testNormalizedDataDoFirstIteration() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		new ShuffleDatasetGlobally();
		int numberOfInitialClusters = 10;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/easyDataset-hardcodedPatterns-objects40-seed1342-bg240-samples2-identicaltrue.txt.2.norm")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		assertEquals(numberOfInitialClusters, bla.getLatestClustering().getClusters().size());
	}

	@Test
	public void testDoFirstIterationPvalues() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		IShuffleDataset permutationFunction = new ShuffleDatasetGlobally();
		int numberOfInitialClusters = 10;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/easyDataset-hardcodedPatterns-objects40-seed1342-bg240-samples2-identicaltrue.txt.2.norm")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();

		ClusterFeatureNumberObjects cfNo = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity cfAs = new ClusterFeatureAverageSimilarity(similarityFunction);

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { cfNo, cfAs };

		ICalculatePValues calculatePValues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				permutationFunction, Arrays.asList(new BasicFitnessScore(ObjectType.CLUSTER, features)),
				new List[] { new ArrayList<>() }, Arrays.asList(false), new MultiplyPValues(), 100);

		bla.calculatePValues(calculatePValues, 42);

		IPValueCalculationResult clusterPValues = bla.getPValues();

		boolean anyIsLargerPearson = false;
		boolean anyIsLargerRss = false;
		for (ICluster c : bla.getLatestClustering().getClusters()) {
			anyIsLargerPearson = anyIsLargerPearson || clusterPValues.getPValue(c).getDouble() > 0.0;
			anyIsLargerRss = anyIsLargerRss || clusterPValues.getPValue(c).getDouble() > 0.0;

			if (anyIsLargerRss && anyIsLargerPearson)
				break;
		}
		assertTrue(anyIsLargerPearson);
		assertTrue(anyIsLargerRss);
	}

	@Test
	public void testPvaluesConstantCluster() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		new ShuffleDatasetRowwise();
		int numberOfInitialClusters = 3;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		Pair<IClusterObjectMapping, IClusterList> demo = demoDataConstantCluster();
		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(demo.getLeft().getAssignedObjects());
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(demo.getLeft().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		// for (ICluster c : bla.getClusterObjectMapping().clusterSet()) {
		// System.out.println(Arrays.toString(c.getPrototype()));
		// System.out.println(c.getPearsonPValue());
		// System.out.println(c.getRssPValue());
		// }
	}

	@Test
	public void testDoSecondIterationPvalues() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		IShuffleDataset permutationFunction = new ShuffleDatasetGlobally();
		int numberOfInitialClusters = 20;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/easyDataset-hardcodedPatterns-objects40-seed1342-bg240-samples2-identicaltrue.txt.2.norm")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		process.start();

		process.doIteration();
		process.doIteration();

		ClusterFeatureNumberObjects cfNo = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity cfAs = new ClusterFeatureAverageSimilarity(similarityFunction);

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { cfNo, cfAs };

		ICalculatePValues calculatePValues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				permutationFunction, Arrays.asList(new BasicFitnessScore(ObjectType.CLUSTER, features)),
				new List[] { new ArrayList<>() }, Arrays.asList(false), new MultiplyPValues(), 100);
		process.calculatePValues(calculatePValues, 42);

		IPValueCalculationResult clusterPValues = process.getPValues();
		boolean anyIsLargerPearson = false;
		boolean anyIsLargerRss = false;
		for (ICluster c : clusterPValues.getObjects(ObjectType.CLUSTER)) {
			anyIsLargerPearson = anyIsLargerPearson || clusterPValues.getPValue(c).getDouble() > 0.0;
			anyIsLargerRss = anyIsLargerRss || clusterPValues.getPValue(c).getDouble() > 0.0;

			if (anyIsLargerRss && anyIsLargerPearson)
				break;
		}
		assertTrue(anyIsLargerPearson);
		assertTrue(anyIsLargerRss);
	}

	// @Test
	public void testRhinoPerform1000Iterations() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		int numberOfInitialClusters = 3;

		ITimeSeriesObjectList objs = TestDataUtility.parseRhinoExpressionData();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 14);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		int iteration = 1000;
		while (iteration-- > 0)
			bla.doIteration();
		//
		// PValueCalculationResult clusterPValues =
		// bla.getClusterPValues();
		//
		// boolean anyIsLargerPearson = false;
		// boolean anyIsLargerRss = false;
		// for (ICluster c : bla.getClusterObjectMapping().clusterSet()) {
		// anyIsLargerPearson = anyIsLargerPearson || clusterPValues.getPValue(c) > 0.0;
		// anyIsLargerRss = anyIsLargerRss || clusterPValues.getPValue(c) > 0.0;
		//
		// // System.out.println(c.getClusterNumber() + "\t" +
		// // c.getResidualSumOfSquares() + "\t"
		// // + ArraysExt.toSeparatedString(c.getPrototype()));
		//
		// if (anyIsLargerRss && anyIsLargerPearson)
		// break;
		// }
		// assertTrue(anyIsLargerPearson);
		// assertTrue(anyIsLargerRss);
		//
		// boolean anyIsSmallerPearson = false;
		// boolean anyIsSmallerRss = false;
		// for (ICluster c : bla.getClusterObjectMapping().clusterSet()) {
		// anyIsSmallerPearson = anyIsSmallerPearson || clusterPValues.getPValue(c) <
		// 1.0;
		// anyIsSmallerRss = anyIsSmallerRss || clusterPValues.getPValue(c) < 1.0;
		//
		// if (anyIsSmallerPearson && anyIsSmallerRss)
		// break;
		// }
		// assertTrue(anyIsSmallerPearson);
		// assertTrue(anyIsSmallerRss);
	}

	// @Test
	// This test is obsolete;
	public void testCalculatePValues2ConstantClusterInfluenza() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		IShuffleDataset permutationFunction = new ShuffleDatasetRowwise();
		int numberOfInitialClusters = 20;
		ITimeSeriesObjectList objs = TestDataUtility.parseInfluenzaExpressionData();
		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		Map<Integer, ICluster> idToCluster = new HashMap<>();
		BufferedReader r = new BufferedReader(new FileReader("/test_files/clustering/clustering_influenza.txt"));
		while (r.ready()) {
			String nextLine = r.readLine();
			String[] split = nextLine.split("\t");
			int id = Integer.valueOf(split[0]);

			if (!idToCluster.containsKey(id)) {
				tsf.setTimeSeries(new double[9]);
				ICluster cluster = pom.clusterBuilder.setPrototype(f.build()).setClusterNumber(id)
						.setInternalClusterNumber((long) id).setIsKeep(false).build();
				idToCluster.put(id, cluster);
			}
			pom.addMapping(idToObj.get(split[1]), idToCluster.get(id),
					similarityFunction.value(1.0, ObjectType.OBJECT_CLUSTER_PAIR));
		}
		r.close();

		// remove objects not in the clustering
		objs.retainAll(pom.getAssignedObjects());

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		IInitialClusteringProvider initialClustering = new PrespecifiedInitialClustering(pom);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		pom = bla.doIteration();

		ClusterFeatureNumberObjects cfNo = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity cfAs = new ClusterFeatureAverageSimilarity(similarityFunction);

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { cfNo, cfAs };

		ICalculatePValues calculatePValues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER),
				permutationFunction, Arrays.asList(new BasicFitnessScore(ObjectType.CLUSTER, features)),
				new List[] { new ArrayList<>() }, Arrays.asList(false), new MultiplyPValues(), 100);

		bla.calculatePValues(calculatePValues, 42);

		IPValueCalculationResult clusterPValues = bla.getPValues();

		ICluster constantCluster = null;
		for (ICluster c : bla.getLatestClustering().getClusters()) {
			// the cluster with constant prototype
			if (c.getClusterNumber() == 10) {
				constantCluster = c;
			} else if (c.getClusterNumber() == 6) {
			}

			// double l = prior.calculate(pom, c).asDouble();
			// System.out.println(c + "\t" + l);
		}
		assertArrayEquals(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, PrototypeComponentType.TIME_SERIES
				.getComponent(constantCluster.getPrototype()).getTimeSeries().asArray(), 0.0);
		// the cluster with constant signal should not be significant
		assertTrue(clusterPValues.getPValue(constantCluster).getDouble() > 0.05);
	}

	@Test
	public void testSeed42() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		new ShuffleDatasetRowwise();
		int numberOfInitialClusters = 10;
		Scanner scan = new Scanner(new File(getClass().getResource("/test_files/clustering/seed42.txt").getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 10, 10);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		// System.out.println(idToObj.size());

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		Map<Integer, ICluster> idToCluster = new HashMap<>();
		BufferedReader r = new BufferedReader(
				new FileReader(getClass().getResource("/test_files/clustering/seed42_clustering.txt").getFile()));
		while (r.ready()) {
			String nextLine = r.readLine();
			String[] split = nextLine.split("\t");
			int id = Integer.valueOf(split[0]);
			if (!idToCluster.containsKey(id)) {
				tsf.setTimeSeries(new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 });
				ICluster cluster = pom.clusterBuilder.setPrototype(f.build()).setClusterNumber(id)
						.setInternalClusterNumber((long) id).setIsKeep(false).build();
				idToCluster.put(id, cluster);
			}
			pom.addMapping(idToObj.get(split[1]), idToCluster.get(id),
					similarityFunction.value(1.0, ObjectType.OBJECT_CLUSTER_PAIR));
		}
		r.close();

		// is the object part of cluster 1?
		ITimeSeriesObject o299 = idToObj.get("obj299");
		assertEquals(o299.getName() + " is assigned to the wrong cluster", idToCluster.get(1),
				pom.getSimilarities(o299).keySet().iterator().next());

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		IInitialClusteringProvider initialClustering = new PrespecifiedInitialClustering(pom);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		// perform iterations until we arrive at the optimal clustering
		new LocalSearchClustering(0.00001, 100).computeIterationsUntilConvergence(bla);
		List<IClusterHistory> hist = new ArrayList<>();
		IClusterHistory ph = bla.getHistory();
		hist.add(0, ph);
		while (ph.getParent() != null) {
			ph = ph.getParent();
			hist.add(0, ph);
		}
	}

	// @Test
	public void testCalculatePValues2Influenza500Clusters() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		new ShuffleDatasetRowwise();
		int numberOfInitialClusters = 500;
		ITimeSeriesObjectList objs = TestDataUtility.parseInfluenzaExpressionData();
		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

//		TiconeNetworkImpl network = TestUtility.parseI2DPPINetwork();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);
		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 14);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
	}

	@Test
	public void testUntilConvergencePvalues() throws Exception {
		System.gc();
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		int numberOfInitialClusters = 3;

		ITimeSeriesObjectList objs = TestDataUtility.parseRhinoExpressionData();

		IObjectSetVariance variance = new StandardVariance();
		LeastVaryingObjectSetsFilter filter = new LeastVaryingObjectSetsFilter(99, variance);
		ITimeSeriesObjectList filtered = filter.filterObjectSets(objs).asList();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(filtered);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 14);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		new LocalSearchClustering(0.001, 100).computeIterationsUntilConvergence(bla);

		List<IClusterHistory> hist = new ArrayList<>();
		IClusterHistory ph = bla.getHistory();
		hist.add(0, ph);
		while (ph.getParent() != null) {
			ph = ph.getParent();
			hist.add(0, ph);
		}
		//
		// PValueCalculationResult clusterPValues =
		// bla.getClusterPValues();
		//
		// boolean anyIsLargerPearson = false;
		// boolean anyIsLargerRss = false;
		// for (ICluster c : bla.getClusterObjectMapping().clusterSet()) {
		// anyIsLargerPearson = anyIsLargerPearson || clusterPValues.getPValue(c) > 0.0;
		// anyIsLargerRss = anyIsLargerRss || clusterPValues.getPValue(c) > 0.0;
		//
		// // System.out.println(c.getClusterNumber() + "\t" +
		// // c.getResidualSumOfSquares() + "\t"
		// // + ArraysExt.toSeparatedString(c.getPrototype()));
		//
		// if (anyIsLargerRss && anyIsLargerPearson)
		// break;
		// }
		// assertTrue(anyIsLargerPearson);
		// assertTrue(anyIsLargerRss);
		//
		// boolean anyIsSmallerPearson = false;
		// boolean anyIsSmallerRss = false;
		// for (ICluster c : bla.getClusterObjectMapping().clusterSet()) {
		// anyIsSmallerPearson = anyIsSmallerPearson || clusterPValues.getPValue(c) <
		// 1.0;
		// anyIsSmallerRss = anyIsSmallerRss || clusterPValues.getPValue(c) < 1.0;
		//
		// if (anyIsSmallerPearson && anyIsSmallerRss)
		// break;
		// }
		// assertTrue(anyIsSmallerPearson);
		// assertTrue(anyIsSmallerRss);
	}
}
