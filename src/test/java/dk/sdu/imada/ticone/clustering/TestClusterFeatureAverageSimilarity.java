/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public class TestClusterFeatureAverageSimilarity {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;
	private InverseEuclideanSimilarityFunction sim;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);

		sim = new InverseEuclideanSimilarityFunction();
		sim.setNormalizeByNumberTimepoints(false);
	}

	@Test
	public void test() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject o = new TimeSeriesObject("obj1", new String[] { "s1" },
				new TimeSeries[] { TimeSeries.of(1, 2, 3) });
		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(new TimeSeriesObjectList(Arrays.asList(o)));
		av.process();

		pom.addMapping(o, c, TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<ISimilarityValue> calculate = new ClusterFeatureAverageSimilarity(sim).calculate(c);
		assertEquals(0.0, calculate.getValue().get(), 0.0);
	}

	@Test
	public void test2() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject o = new TimeSeriesObject("obj1", new String[] { "s1" },
				new TimeSeries[] { TimeSeries.of(1, 2, 4) });
		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(new TimeSeriesObjectList(Arrays.asList(o)));
		av.process();

		pom.addMapping(o, c, TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<ISimilarityValue> calculate = new ClusterFeatureAverageSimilarity(sim).calculate(c);
		assertEquals(-1.0, calculate.getValue().get(), 0.0);
	}

	@Test
	public void test3() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject o = new TimeSeriesObject("obj1", new String[] { "s1" },
				new TimeSeries[] { TimeSeries.of(1, 2, 5) });
		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(new TimeSeriesObjectList(Arrays.asList(o)));
		av.process();

		pom.addMapping(o, c, TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<ISimilarityValue> calculate = new ClusterFeatureAverageSimilarity(sim).calculate(c);
		assertEquals(-2.0, calculate.getValue().get(), 0.0);
	}

	@Test
	public void test4() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject o = new TimeSeriesObject("obj1", new String[] { "s1" },
				new TimeSeries[] { TimeSeries.of(1, 1, 4) });
		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(new TimeSeriesObjectList(Arrays.asList(o)));
		av.process();

		pom.addMapping(o, c, TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<ISimilarityValue> calculate = new ClusterFeatureAverageSimilarity(sim).calculate(c);
		assertEquals(-Math.sqrt(2), calculate.getValue().get(), 0.0);
	}

	@Test
	public void testEmptyCluster() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		IFeatureValue<ISimilarityValue> calculate = new ClusterFeatureAverageSimilarity(sim).calculate(c);
		assertEquals(Double.NaN, calculate.getValue().get(), 0.0);
	}

}
