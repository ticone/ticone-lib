package dk.sdu.imada.ticone.clustering.mergeclusters;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.BasicClusteringProcessBuilder;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethod;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.LocalSearchClustering;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResultFactory;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TimePointWeighting;

public class TestMergeClusters {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries());
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testMergeClusters() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 10;
		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5_mergeTestCase.txt")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 25, 25);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 14);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		int expectedNumberClusters = numberOfInitialClusters;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iterations 2-3
		new LocalSearchClustering(0.001, 100).computeIterationsUntilConvergence(bla);

		assertEquals(3, bla.getHistory().getIterationNumber());
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 4
		bla.mergeClusters(new int[] { 7, 9 });

		assertEquals(4, bla.getHistory().getIterationNumber());
		expectedNumberClusters--;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 5
		bla.mergeClusters(new int[] { 4, 10 });

		assertEquals(5, bla.getHistory().getIterationNumber());
		expectedNumberClusters--;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 6
		bla.doIteration();
		assertEquals(6, bla.getHistory().getIterationNumber());
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 7
		new LocalSearchClustering(0.01, 100).computeIterationsUntilConvergence(bla);
		assertEquals(8, bla.getHistory().getIterationNumber());
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 8
		bla.mergeClusters(new int[] { 1, 2 });
		assertEquals(9, bla.getHistory().getIterationNumber());
		expectedNumberClusters--;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 9
		bla.mergeClusters(new int[] { 11, 12 });
		assertEquals(10, bla.getHistory().getIterationNumber());
		expectedNumberClusters--;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 10
		bla.mergeClusters(new int[] { 3, 14 });
		assertEquals(11, bla.getHistory().getIterationNumber());
		expectedNumberClusters--;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 11
		bla.mergeClusters(new int[] { 13, 15 });
		assertEquals(12, bla.getHistory().getIterationNumber());
		expectedNumberClusters--;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 12
		bla.doIteration();
		assertEquals(13, bla.getHistory().getIterationNumber());
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

		// iteration 13
		bla.mergeClusters(new int[] { 6, 8 });
		assertEquals(14, bla.getHistory().getIterationNumber());
		expectedNumberClusters--;
		assertEquals(expectedNumberClusters, bla.getLatestClustering().getClusters().size());

	}

	@Test
	public void testTotalRSS() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		similarityFunction.setNormalizeByNumberTimepoints(false);
		int numberOfInitialClusters = 10;
		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5_mergeTestCase.txt")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 14);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();

		ISimilarityValue totalRSS = bla.getAverageObjectClusterSimilarity();
		assertEquals(0.9157, totalRSS.get(), 0.0001);
	}
}
