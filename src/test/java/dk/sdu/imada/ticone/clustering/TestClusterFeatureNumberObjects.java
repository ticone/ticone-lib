/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public class TestClusterFeatureNumberObjects {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void test() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject o = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		pom.addMapping(o, c, TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<Integer> calculate = new ClusterFeatureNumberObjects().calculate(c);
		assertEquals(1.0, calculate.getValue(), 0.0);
	}

	@Test
	public void test2() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		pom.addMapping(new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 4)), c,
				TestSimpleSimilarityValue.of(1.0));
		pom.addMapping(new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 4)), c,
				TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<Integer> calculate = new ClusterFeatureNumberObjects().calculate(c);
		assertEquals(2.0, calculate.getValue(), 0.0);
	}

	@Test
	public void test3() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject o = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 5));
		pom.addMapping(o, c, TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<Integer> calculate = new ClusterFeatureNumberObjects().calculate(c);
		assertEquals(1.0, calculate.getValue(), 0.0);
	}

	@Test
	public void test4() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ITimeSeriesObject o = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 1, 4));
		pom.addMapping(o, c, TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<Integer> calculate = new ClusterFeatureNumberObjects().calculate(c);
		assertEquals(1.0, calculate.getValue(), 0.0);
	}

}
