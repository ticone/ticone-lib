/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;

/**
 * @author Christian Wiwie
 * 
 * @since May 10, 2017
 *
 */
public class TestShuffleClusteringByShufflingClustersDegreePreserving {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

//	@Test
	public void test() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 20;

		ITimeSeriesObjectList objs = TestDataUtility.parseInfluenzaExpressionData();
		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction));

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeFactory);

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(discretizePatternFunction));

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		ClusterObjectMapping pom = bla.doIteration();

		List<IArithmeticFeature<? extends Comparable<?>>> featureList = new ArrayList<>();
		featureList.add(new ClusterFeatureNumberObjects());
		featureList.add(new ClusterFeatureAverageSimilarity(similarityFunction));

		TiconeNetworkImpl network = TestDataUtility.parseI2DPPINetwork();

		pom.removeObjectsNotInNetwork(network);
		network.removeNodesNotInDataset(pom.getAssignedObjects());

		ShuffleClusteringByShufflingClustersDegreePreserving shuffle = new ShuffleClusteringByShufflingClustersDegreePreserving();
		shuffle.setNetwork(network);

		IShuffleResult shuffle2 = shuffle.shuffle(pom, 42);
	}

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

}
