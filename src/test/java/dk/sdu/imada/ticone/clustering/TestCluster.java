package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

public class TestCluster {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testSerialize() throws Exception {
		Cluster p = generateRandomPattern();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(p);
		oos.close();

		byte[] arr = baos.toByteArray();

		ByteArrayInputStream bais = new ByteArrayInputStream(arr);
		ObjectInputStream ois = new ObjectInputStream(bais);
		Cluster p_deserialized = (Cluster) ois.readObject();
		Assert.assertEquals(p_deserialized.keep, p.keep);
		Assert.assertArrayEquals(
				PrototypeComponentType.TIME_SERIES.getComponent(p_deserialized.prototype).getTimeSeries().asArray(),
				PrototypeComponentType.TIME_SERIES.getComponent(p.prototype).getTimeSeries().asArray(), 0.00001);
		Assert.assertEquals(p_deserialized.clusterNumber, p.clusterNumber);
	}

	public Cluster generateRandomPattern() throws Exception {
		Random r = new Random(14);
		tsf.setTimeSeries(
				new double[] { r.nextDouble(), r.nextDouble(), r.nextDouble(), r.nextDouble(), r.nextDouble() });
		Cluster p = (Cluster) new ClusterObjectMapping(f).addCluster(f.build());
		p.keep = r.nextBoolean();
		p.clusterNumber = r.nextInt();
		return p;
	}

	@Test
	public void testGetClusterNumber() throws Exception {
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = new ClusterObjectMapping(f).addCluster(f.build());
		assertEquals(1, c.getClusterNumber());
	}

	// @Test
	// public void testGetPearsonPValue() {
	// ICluster c = new Cluster(new PrototypeFactory().createInstance(
	// tsf.createInstance(new TimeSeries(new double[] { 1, 2, 3 }))));
	// c.setPearsonPValue(0.5);
	// assertEquals(0.5, c.getPearsonPValue(), 0.000000001);
	// }
	//
	// @Test
	// public void testGetRSSPValue() {
	// ICluster c = new Cluster(new PrototypeFactory().createInstance(
	// tsf.createInstance(new TimeSeries(new double[] { 1, 2, 3 }))));
	// c.setRssPValue(0.5);
	// assertEquals(0.5, c.getRssPValue(), 0.000000001);
	// }
	//
	// @Test
	// public void testGetPValue() {
	// ICluster c = new Cluster(new PrototypeFactory().createInstance(
	// tsf.createInstance(new TimeSeries(new double[] { 1, 2, 3 }))));
	// c.setPValue(0.5);
	// assertEquals(0.5, c.getPValue(), 0.000000001);
	// }
	//
	// @Test
	// public void testGetClustPValue() {
	// ICluster c = new Cluster(new PrototypeFactory().createInstance(
	// tsf.createInstance(new TimeSeries(new double[] { 1, 2, 3 }))));
	// c.setClustPValue(0.5);
	// assertEquals(0.5, c.getClustPValue(), 0.000000001);
	// }

	@Test
	public void testGetKeep() throws Exception {
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = new ClusterObjectMapping(f).addCluster(f.build());
		c.setKeep(true);
		assertTrue(c.isKeep());
	}

	@Test
	public void testGetParent() throws Exception {
		ClusterObjectMapping clustering = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = clustering.addCluster(f.build());
		tsf.setTimeSeries(new double[] { 4, 3, 2 });
		ICluster parent = clustering.addCluster(f.build());
		c.setParent(parent);
		assertEquals(parent, c.getParent());
	}

	@Test
	public void testEquals() throws Exception {
		ClusterObjectMapping clustering = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = clustering.addCluster(f.build());
		assertEquals(c, clustering.clusterBuilder.copy(c, true));
	}

	@Test
	public void testHashCode() throws Exception {
		ClusterObjectMapping clustering = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ClusterBuilder clusterBuilder = clustering.getClusterBuilder();
		ICluster c = clusterBuilder.setPrototype(f.build()).build();
		assertEquals(c.hashCode(), clusterBuilder.copy(c, true).hashCode());
	}
}
