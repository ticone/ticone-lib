package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.InverseShortestPathSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.similarity.WeightedAverageCompositeSimilarityFunction;

public class TestClusterUtility {

	protected IClusterBuilder cf;
	protected PrototypeBuilder pf;
	protected TimeSeriesPrototypeComponentBuilder tspcf;

	@BeforeEach
	public void setUp() {
		pf = new PrototypeBuilder();
		cf = new ClusterObjectMapping(pf).getClusterBuilder();
		cf.setPrototypeBuilder(pf);
		tspcf = new TimeSeriesPrototypeComponentBuilder();
		pf.setPrototypeComponentBuilders(tspcf);
//		ClusterObjectMapping.resetClusterCount();
	}

	@Test
	public void testWithOnePattern() throws Exception {
		IClusterList patterns = new ClusterList();
		tspcf.setTimeSeries(new double[] { 0.1, 0.3, 0.7 });
		ICluster p = cf.build();
		patterns.add(p);

		IClusterList result = ClusterUtility.getPatternSingleLinkage(patterns, new PearsonCorrelationFunction());
		Assert.assertEquals(1, result.size());
		Assert.assertEquals(p, result.get(0));
	}

	@Test
	public void testWithMorePatterns() throws Exception {
		IClusterList patterns = new ClusterList();
		tspcf.setTimeSeries(new double[] { 0.1, 0.3, 0.7 });
		ICluster p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.2, 0.3, 0.7 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.2, 0.1, 0.7 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.1, 0.4, 0.7 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.1, 0.4, 0.9 });
		p = cf.build();
		patterns.add(p);

		IClusterList result = ClusterUtility.getPatternSingleLinkage(patterns, new PearsonCorrelationFunction());
		Assert.assertEquals(5, result.size());
		// Assert.assertEquals(p, result.get(0));
	}

	@Test
	public void testWith10Patterns() throws Exception {
		IClusterList patterns = new ClusterList();
		// 0: should be close
		tspcf.setTimeSeries(new double[] { 0.0, 1.0, 0.0, 1.0, 1.0 });
		ICluster p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.0, 0.25, 0.25, 1.0, 0.25 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 1.0, 0.75, 0.25, 0.0, 0.0 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 1.0, 0.0, 1.0, 0.0, 0.0 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.25, 0.25, 1.0, 0.25, 0.0 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 1.0, 0.25, 0.0, 0.0, 0.0 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.25, 1.0, 0.5, 0.25, 0.0 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 1.0, 0.25, 0.0, 0.25, 0.75 });
		p = cf.build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.0, 0.25, 0.25, 0.25, 1.0 });
		p = cf.build();
		patterns.add(p);
		// 9: should be close
		tspcf.setTimeSeries(new double[] { 0.0, 1.0, 0.0, 0.75, 1.0 });
		p = cf.build();
		patterns.add(p);

		IClusterList result = ClusterUtility.getPatternSingleLinkage(patterns, new PearsonCorrelationFunction());
		Assert.assertEquals(10, result.size());

		assertEquals(Arrays.asList(7, 5, 4, 8, 6, 3, 9, 10, 1, 2),
				result.stream().map(c -> c.getClusterNumber()).collect(Collectors.toList()));
	}

	@Test
	public void testWithCompositeSimilarityFunction() throws Exception {
		TiconeNetworkImpl network = new TiconeNetworkImpl("network");
		network.addNode("o1");
		network.addNode("o2");
		network.addNode("o3");
		network.addNode("o4");
		network.addNode("o5");
		network.addNode("o6");
		network.addNode("o7");
		network.addNode("o8");
		network.addNode("o9");
		network.addNode("o10");

		// add all possible edges, such that network topology should not affect the
		// result
		for (TiconeNetworkNodeImpl n1 : network.getNodeSet())
			for (TiconeNetworkNodeImpl n2 : network.getNodeSet())
				network.addEdge(n1, n2, false);

		InverseShortestPathSimilarityFunction nsf = new InverseShortestPathSimilarityFunction(network);
		nsf.initialize();
		AggregateClusterMedoidNode medoidNode = new AggregateClusterMedoidNode(network, nsf);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		pf.addPrototypeComponentFactory(nlf);
		nlf.setAggregationFunction(medoidNode);

		IClusterList patterns = new ClusterList();
		// 0: should be close
		tspcf.setTimeSeries(new double[] { 0.0, 1.0, 0.0, 1.0, 1.0 });
		ICluster p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o1")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.0, 0.25, 0.25, 1.0, 0.25 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o2")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 1.0, 0.75, 0.25, 0.0, 0.0 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o3")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 1.0, 0.0, 1.0, 0.0, 0.0 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o4")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.25, 0.25, 1.0, 0.25, 0.0 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o5")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 1.0, 0.25, 0.0, 0.0, 0.0 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o6")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.25, 1.0, 0.5, 0.25, 0.0 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o7")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 1.0, 0.25, 0.0, 0.25, 0.75 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o8")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		tspcf.setTimeSeries(new double[] { 0.0, 0.25, 0.25, 0.25, 1.0 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o9")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);
		// 9: should be close
		tspcf.setTimeSeries(new double[] { 0.0, 1.0, 0.0, 0.75, 1.0 });
		p = cf.setObjects(new TimeSeriesObjectList(new TimeSeriesObject("o10")),
				Arrays.asList(TestSimpleSimilarityValue.of(1.0))).build();
		patterns.add(p);

		WeightedAverageCompositeSimilarityFunction sf = new WeightedAverageCompositeSimilarityFunction(nsf,
				new PearsonCorrelationFunction());

		IClusterList result = ClusterUtility.getPatternSingleLinkage(patterns, sf);
		Assert.assertEquals(10, result.size());

		assertEquals(Arrays.asList(7, 5, 4, 8, 6, 3, 9, 10, 1, 2),
				result.stream().map(c -> c.getClusterNumber()).collect(Collectors.toList()));
	}
}
