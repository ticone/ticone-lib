/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeaturePrior;
import dk.sdu.imada.ticone.feature.IClusterFeaturePrior;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.generate.TimeSeriesTestClass;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 10, 2017
 *
 */
public class TestClusterFeaturePrior {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testEmptyCluster() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ClusterFeaturePrior f = new ClusterFeaturePrior(10);
		f.setFeatureValueProvider(pom);
		IFeatureValue<Double> calculate = f.calculate(c);
		assertEquals(Double.NaN, calculate.getValue(), 0.0);
	}

	@Test
	public void blubb() throws Exception {
		List<ICluster> cluster = new ArrayList<>();
		ClusterObjectMapping pom = new ClusterObjectMapping(f);

		tsf.setTimeSeries(new double[] { 0, 0, 0, 0, 0 });
		cluster.add(pom.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 1, 0, 0, 0, 0 });
		cluster.add(pom.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 1, 0, 0, 0 });
		cluster.add(pom.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 1, 0, 0 });
		cluster.add(pom.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 0, 1, 0 });
		cluster.add(pom.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 0, 0, 0, 0, 1 });
		cluster.add(pom.getClusterBuilder().setPrototype(f.build()).build());
		tsf.setTimeSeries(new double[] { 3, 0, 5, 1, 0 });
		cluster.add(pom.getClusterBuilder().setPrototype(f.build()).build());

		IClusterObjectMapping com = TimeSeriesTestClass.generateObjectsForClusterPrototypes(pom,
				TestSimpleSimilarityValue.of(-1), 3, 10);
		ITimeSeriesObjectList objs = com.getAssignedObjects();

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IClusterFeaturePrior prior = new ClusterFeaturePrior(100);
		prior.setFeatureValueProvider(com);

		for (ICluster c : com.getClusters()) {
			IFeatureValue<Double> calculate = prior.calculate(c);
			System.out.println(c + "\t" + calculate.getValue());
		}
	}

}
