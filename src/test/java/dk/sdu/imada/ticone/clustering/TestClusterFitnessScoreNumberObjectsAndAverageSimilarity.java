/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.scale.IScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.TanhNormalizerBuilder;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public class TestClusterFitnessScoreNumberObjectsAndAverageSimilarity {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testWrongNumberArguments() throws Exception {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			tsf.setTimeSeries(new double[] { 1, 2, 3 });
			ClusterObjectMapping pom = new ClusterObjectMapping(f);
			ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

			ISimilarityFunction sim = new InverseEuclideanSimilarityFunction();

			ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
			ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

			IFeatureValue<Integer> noObjects = fno.calculate(c);
			IFeatureValue<ISimilarityValue> avgSim = fas.calculate(c);

			IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(0.0, 1.0),
					new TanhNormalizerBuilder<>(0.0, 1.0) };

			double[] weights = new double[] { 1.0 };

			IFeatureStore featureValues = new BasicFeatureStore();
			featureValues.setFeatureValue(c, fno, noObjects);
			featureValues.setFeatureValue(c, fas, avgSim);

			IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
					new ClusterFeatureNumberObjects(), new ClusterFeatureAverageSimilarity(sim) };

			IFitnessScore fs = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);
			fs.setObjectsAndFeatureStore(pom, featureValues);

			double calculateClusterFitness = fs.calculateFitness(c).getValue();
			assertEquals(0.5, calculateClusterFitness, 0.0);
		});
	}

	@Test
	public void testEmptyCluster() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ISimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		IFeatureValue<Integer> noObjects = fno.calculate(c);
		IFeatureValue<ISimilarityValue> avgSim = fas.calculate(c);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(0.0, 1.0),
				new TanhNormalizerBuilder<>(0.0, 1.0) };

		double[] weights = new double[] { 1.0, 1.0 };

		IFeatureStore featureValues = new BasicFeatureStore();
		featureValues.setFeatureValue(c, fno, noObjects);
		featureValues.setFeatureValue(c, fas, avgSim);

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(), new ClusterFeatureAverageSimilarity(sim) };

		IFitnessScore fs = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);
		fs.setObjectsAndFeatureStore(pom, featureValues);

		double calculateClusterFitness = fs.calculateFitness(c).getValue();
		assertEquals(0.5, calculateClusterFitness, 0.0);
	}

	@Test
	public void testOneCluster() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c = pom.getClusterBuilder().setPrototype(f.build()).build();

		ISimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		ITimeSeriesObject o = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(new TimeSeriesObjectList(Arrays.asList(o)));
		av.process();

		pom.addMapping(o, c, TestSimpleSimilarityValue.of(1.0));

		IFeatureValue<Integer> noObjects = fno.calculate(c);
		IFeatureValue avgSim = fas.calculate(c);

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(1.0, 1.0),
				new TanhNormalizerBuilder<>(0.0, 1.0) };

		double[] weights = new double[] { 1.0, 1.0 };

		IFeatureStore featureValues = new BasicFeatureStore();
		featureValues.setFeatureValue(c, fno, noObjects);
		featureValues.setFeatureValue(c, fas, avgSim);

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] { fno, fas };

		IFitnessScore fs = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);
		fs.setObjectsAndFeatureStore(pom, featureValues);

		double calculateClusterFitness = fs.calculateFitness(c).getValue();
		assertEquals(0.5, calculateClusterFitness, 0.0);
	}

	@Test
	public void testSameClusters() throws Exception {
		ClusterObjectMapping com = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = com.getClusterBuilder().setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 1 });
		ICluster c2 = com.getClusterBuilder().setPrototype(f.build()).build();

		ISimilarityFunction sim = new InverseEuclideanSimilarityFunction();

		ClusterFeatureNumberObjects fno = new ClusterFeatureNumberObjects();
		ClusterFeatureAverageSimilarity fas = new ClusterFeatureAverageSimilarity(sim);

		ITimeSeriesObject o1 = new TimeSeriesObject("obj1", "s1", TimeSeries.of(1, 2, 3));
		ITimeSeriesObject o2 = new TimeSeriesObject("obj2", "s1", TimeSeries.of(1, 2, 1));

		com.addMapping(o1, c1, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o2, c2, TestSimpleSimilarityValue.of(1.0));

		// we need a 3rd cluster for the scalers
		tsf.setTimeSeries(new double[] { 0, 2, 5 });
		ICluster c3 = com.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject o5 = new TimeSeriesObject("obj5", "s1", TimeSeries.of(6, 2, 0));
		ITimeSeriesObject o6 = new TimeSeriesObject("obj6", "s1", TimeSeries.of(1, 7, 0));
		ITimeSeriesObject o7 = new TimeSeriesObject("obj7", "s1", TimeSeries.of(1, 7, 13));
		com.addMapping(o5, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o6, c3, TestSimpleSimilarityValue.of(1.0));
		com.addMapping(o7, c3, TestSimpleSimilarityValue.of(1.0));

		AbsoluteValues av = new AbsoluteValues();
		av.initializeObjects(com.getAssignedObjects());
		av.process();

		IScalerBuilder[] scaler = new IScalerBuilder[] { new TanhNormalizerBuilder<>(1.0, 1.0),
				new TanhNormalizerBuilder<>(0.0, 1.0) };

		double[] weights = new double[] { 1.0, 1.0 };

		final IFeatureStore featureStore = new BasicFeatureStore();

		for (ICluster c : com.getClusters()) {
			featureStore.setFeatureValue(c, fno, fno.calculate(c));
			featureStore.setFeatureValue(c, fas, fas.calculate(c));
		}

		IArithmeticFeature<? extends Comparable<?>>[] features = new IArithmeticFeature[] {
				new ClusterFeatureNumberObjects(), new ClusterFeatureAverageSimilarity(sim) };

		IFitnessScore fitness = new BasicFitnessScore(ObjectType.CLUSTER, features, scaler, weights);
		fitness.setObjectsAndFeatureStore(com, featureStore);

		for (ICluster c : com.getClusters()) {
			if (c.getClusterNumber() > 2)
				continue;
			IFeatureValue<Integer> noObjects = fno.calculate(c);
			IFeatureValue avgSim = fas.calculate(c);

			featureStore.setFeatureValue(c, fno, noObjects);
			featureStore.setFeatureValue(c, fas, avgSim);

			double calculateClusterFitness = fitness.calculateFitness(c).getValue();
			assertEquals(0.5, calculateClusterFitness, 0.0);
		}
	}

}
