package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.SimilarityFeatureValue;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimpleSimilarityValue;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.WeightedAverageCompositeSimilarityFunction;

public class TestClusterObjectMapping {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testGetCoefficientsObjects() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 0, 1, 2 });
		ICluster cluster1 = pom.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject test1 = new TimeSeriesObject("test1");
		pom.addMapping(test1, cluster1, new PearsonCorrelationFunction().value(1.0, null));

		tsf.setTimeSeries(new double[] { 0, 1, 5 });
		ICluster cluster2 = pom.getClusterBuilder().setPrototype(f.build()).build();
		pom.addMapping(test1, cluster2, new PearsonCorrelationFunction().value(0.5, null));

		Map<ICluster, ISimilarityValue> expectedCoeffs = new HashMap<>();
		expectedCoeffs.put(cluster1, new PearsonCorrelationFunction().value(1.0, null));
		expectedCoeffs.put(cluster2, new PearsonCorrelationFunction().value(0.5, null));

		Map<ICluster, ISimilarityValue> coeffs = pom.getSimilarities(test1);

		assertEquals(expectedCoeffs, coeffs);
	}

	@Test
	public void testGetCoefficientsCluster() throws Exception {
		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 0, 1, 2 });
		ICluster cluster1 = pom.getClusterBuilder().setPrototype(f.build()).build();
		ITimeSeriesObject test1 = new TimeSeriesObject("test1"), test2 = new TimeSeriesObject("test2");
		pom.addMapping(test1, cluster1, new PearsonCorrelationFunction().value(1.0, null));

		pom.addMapping(test2, cluster1, new PearsonCorrelationFunction().value(0.5, null));

		Map<ITimeSeriesObject, ISimilarityValue> expectedCoeffs = new HashMap<>();
		expectedCoeffs.put(test1, new PearsonCorrelationFunction().value(1.0, null));
		expectedCoeffs.put(test2, new PearsonCorrelationFunction().value(0.5, null));

		Map<ITimeSeriesObject, ISimilarityValue> coeffs = cluster1.getSimilarities();

		assertEquals(expectedCoeffs, coeffs);
	}
//
//	@Test
//	public void testGetCoefficient() throws Exception {
//		ClusterObjectMapping pom = new ClusterObjectMapping(f);
//		ICluster cluster1 = new Cluster(new double[] { 0, 1, 2 });
//		pom.addCluster(cluster1);
//		pom.addMapping("test1", cluster1, new PearsonCorrelationFunction().value(1.0));
//
//		pom.addCluster(cluster1);
//		pom.addMapping("test2", cluster1, new PearsonCorrelationFunction().value(0.5));
//
//		ISimilarityValue coeffs = pom.getCoefficient("test1", cluster1);
//
//		assertEquals(new PearsonCorrelationFunction().value(1.0), coeffs);
//	}

	@Test
	public void testEqualPartitionLarge2() throws Exception {
		ClusterObjectMapping com1 = new ClusterObjectMapping(f);
		for (int i = 0; i < 500; i++) {
			tsf.setTimeSeries(new double[] { 1, 2, 3 });
			ICluster c = com1.getClusterBuilder().setPrototype(f.build()).setClusterNumber(i)
					.setInternalClusterNumber((long) i).setIsKeep(true).build();

			for (int o = 0; o < 30; o++)
				c.addObject(new TimeSeriesObject("obj" + i + " " + o), AbstractSimilarityValue.MAX);
		}

		ClusterObjectMapping com2 = new ClusterObjectMapping(f);
		for (int i = 0; i < 500; i++) {
			tsf.setTimeSeries(new double[] { 1, 2, 3 });
			ICluster c = com2.getClusterBuilder().setPrototype(f.build()).setClusterNumber(i)
					.setInternalClusterNumber((long) i).setIsKeep(true).build();

			for (int o = 0; o < 30; o++)
				c.addObject(new TimeSeriesObject("obj" + i + " " + o), AbstractSimilarityValue.MAX);
		}

		assertTrue(com1.equalObjectPartition(com2));
	}

	@Test
	public void testEquals() throws Exception {
		ClusterObjectMapping clustering = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ClusterBuilder clusterBuilder = clustering.getClusterBuilder();
		clusterBuilder.setPrototype(f.build()).build();
		assertEquals(clustering.asObjectPartition(), clustering.getCopy().copy.asObjectPartition());
	}

	@Test
	public void testEqualsAfterSerialize() throws Exception {
		ClusterObjectMapping clustering = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ClusterBuilder clusterBuilder = clustering.getClusterBuilder();
		clusterBuilder.setPrototype(f.build()).build();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(clustering);
		oos.close();

		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

		assertEquals(clustering.asObjectPartition(),
				((ClusterObjectMapping) new ObjectInputStream(bais).readObject()).asObjectPartition());
	}

	@Test
	public void testEqualsAfterSerialize2() throws Exception {
		PearsonCorrelationFunction pcf = new PearsonCorrelationFunction();

		ClusterObjectMapping clustering = new ClusterObjectMapping(f);
		ClusterBuilder clusterBuilder = clustering.getClusterBuilder();
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c1 = clusterBuilder.setPrototype(f.build()).build();
		tsf.setTimeSeries(new double[] { 1, 2, 3 });
		ICluster c2 = clusterBuilder.setPrototype(f.build()).build();

		TimeSeriesObject o1 = new TimeSeriesObject("o1");
		TimeSeriesObject o2 = new TimeSeriesObject("o2");

		c1.addObject(o1, pcf.value(1.0, null));
		c2.addObject(o2, pcf.missingValuePlaceholder());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(clustering);
		oos.close();

		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		ClusterObjectMapping first = (ClusterObjectMapping) new ObjectInputStream(bais).readObject();

		baos = new ByteArrayOutputStream();
		oos = new ObjectOutputStream(baos);
		oos.writeObject(clustering);
		oos.close();

		bais = new ByteArrayInputStream(baos.toByteArray());
		ClusterObjectMapping second = (ClusterObjectMapping) new ObjectInputStream(bais).readObject();

		assertEquals(first.asObjectPartition(), second.asObjectPartition());
	}

	@Test
	public void testEqualPartitionLarge() throws Exception {
		ClusterObjectMapping com1 = new ClusterObjectMapping(f);
		for (int i = 0; i < 500; i++) {
			tsf.setTimeSeries(new double[] { 1, 2, 3 });
			ICluster c = com1.getClusterBuilder().setPrototype(f.build()).setClusterNumber(i)
					.setInternalClusterNumber((long) i).setIsKeep(true).build();

			for (int o = 0; o < 30; o++)
				c.addObject(new TimeSeriesObject("obj" + i + " " + o), AbstractSimilarityValue.MAX);
		}

		ClusterObjectMapping com2 = new ClusterObjectMapping(f);
		for (int i = 0; i < 500; i++) {
			tsf.setTimeSeries(new double[] { 1, 2, 3 });
			ICluster c = com2.getClusterBuilder().setPrototype(f.build()).setClusterNumber(i)
					.setInternalClusterNumber((long) i).setIsKeep(true).build();

			for (int o = 0; o < 30; o++)
				c.addObject(new TimeSeriesObject("obj" + i + " " + o), AbstractSimilarityValue.MAX);
		}

		assertTimeout(Duration.ofSeconds(1), () -> assertTrue(com1.equalObjectPartition(com2)));
	}

	@Test
	public void testEqualPartitionLarge3() throws Exception {
		ClusterObjectMapping com1 = new ClusterObjectMapping(f);
		for (int i = 0; i < 5000; i++) {
			tsf.setTimeSeries(new double[] { 1, 2, 3 });
			ICluster c = com1.getClusterBuilder().setPrototype(f.build()).setClusterNumber(i)
					.setInternalClusterNumber((long) i).setIsKeep(true).build();

			for (int o = 0; o < 30; o++)
				c.addObject(new TimeSeriesObject("obj" + i + " " + o), AbstractSimilarityValue.MAX);
		}

		ClusterObjectMapping com2 = new ClusterObjectMapping(f);
		for (int i = 0; i < 5000; i++) {
			tsf.setTimeSeries(new double[] { 1, 2, 3 });
			ICluster c = com2.getClusterBuilder().setPrototype(f.build()).setClusterNumber(i)
					.setInternalClusterNumber((long) i).setIsKeep(true).build();

			for (int o = 0; o < 30; o++)
				c.addObject(new TimeSeriesObject("obj" + i + " " + o), AbstractSimilarityValue.MAX);
		}

		assertTimeout(Duration.ofSeconds(2), () -> assertTrue(com1.equalObjectPartition(com2)));
	}

	@Test
	public void testTotalSimilarity() throws Exception {
		PearsonCorrelationFunction sf1 = new PearsonCorrelationFunction();
		InverseEuclideanSimilarityFunction sf2 = new InverseEuclideanSimilarityFunction();
		WeightedAverageCompositeSimilarityFunction similarityFunction = new WeightedAverageCompositeSimilarityFunction(
				sf1, sf2);

		ITimeSeriesObject test1 = new TimeSeriesObject("test1", 1);
		test1.addPreprocessedTimeSeries(0, TimeSeries.of(0, 2, 1));
		ITimeSeriesObject test2 = new TimeSeriesObject("test2", 1);
		test2.addPreprocessedTimeSeries(0, TimeSeries.of(1, 0, 7));

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		tsf.setTimeSeries(new double[] { 0, 1, 2 });
		ICluster cluster1 = pom.getClusterBuilder().setPrototype(f.build()).build();
		ISimpleSimilarityValue[] sims = new ISimpleSimilarityValue[2];
		sims[0] = sf1.value(1.0, null);
		sims[1] = sf2.value(-0.5, null);
		pom.addMapping(test1, cluster1, similarityFunction.value(sims, ObjectType.OBJECT_CLUSTER_PAIR));

		tsf.setTimeSeries(new double[] { 0, 1, 5 });
		ICluster cluster2 = pom.getClusterBuilder().setPrototype(f.build()).build();

		sims = new ISimpleSimilarityValue[2];
		sims[0] = sf1.value(0.7, null);
		sims[1] = sf2.value(-2.3, null);
		pom.addMapping(test2, cluster2, similarityFunction.value(sims, ObjectType.OBJECT_CLUSTER_PAIR));

		Map<ICluster, ISimilarityValue> expectedCoeffs = new HashMap<>();
		expectedCoeffs.put(cluster1, new PearsonCorrelationFunction().value(1.0, null));
		expectedCoeffs.put(cluster2, new PearsonCorrelationFunction().value(0.5, null));

		ISimilarityValue totalSim = ((SimilarityFeatureValue) new ClusteringFeatureTotalObjectClusterSimilarity(
				similarityFunction).calculate(pom)).getValue();
		assertTrue(totalSim instanceof ICompositeSimilarityValue);
	}
}
