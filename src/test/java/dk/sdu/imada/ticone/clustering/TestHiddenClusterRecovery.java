/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.ObjectPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.ObjectPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.scale.DummyScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.feature.scale.MinMaxScalerBuilder;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction.MISSING_CHILD_HANDLING;
import dk.sdu.imada.ticone.similarity.INetworkBasedSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ITimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.InverseShortestPathSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.WeightedAverageCompositeSimilarityFunction;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.MyParallel;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 2, 2019
 *
 */
class TestHiddenClusterRecovery {

	private static CLARAClusteringMethodBuilder clusteringMethodBuilder;

	@BeforeAll
	public static void init() throws IncompatibleSimilarityFunctionException {
		clusteringMethodBuilder = new CLARAClusteringMethodBuilder();
		clusteringMethodBuilder.setSamples(1).setPamkBuilder(new PAMKClusteringMethodBuilder().setNstart(50))
				.setPrototypeBuilder(new PrototypeBuilder());
	}

	private static Stream<Arguments> hiddenTimeSeriesClustersArgumentProvider()
			throws IncompatibleSimilarityFunctionException {
		final ISimilarityFunction[] timeSeriesSF = new ISimilarityFunction[] { new PearsonCorrelationFunction(),
				new InverseEuclideanSimilarityFunction() };

		final double[] timeSeriesVariances = ArraysExt.rev(new double[] { 0.8, 0.4, 0.2, 0.1, 0.05, 0.0 });

		int[] nstarts = new int[] { 50/* , 125, 250, 500, 1000 */ };
		final int[] numbersObjects = new int[] { 250 };

		final Int2ObjectMap<int[][]> numbersObjectsEnrichedTimeSeries = new Int2ObjectArrayMap<>();
		numbersObjectsEnrichedTimeSeries.put(250, new int[][] { new int[] { 10, 10, 10 }, new int[] { 25, 25, 25 },
				new int[] { 10, 10, 10, 10, 10 }, new int[] { 25, 25, 25, 25, 25 } });

		final Int2ObjectMap<int[]> numbersClusters = new Int2ObjectArrayMap<>();
		numbersClusters.put(250, new int[] { 10, 30 });

		final List<Arguments> list = new ArrayList<>();
		for (ISimilarityFunction similarityFunction : timeSeriesSF) {
			for (int numberObjects : numbersObjects) {
				for (double timeSeriesVariance : timeSeriesVariances) {
					for (int[] numberObjectsEnrichedTimeSeries : numbersObjectsEnrichedTimeSeries.get(numberObjects)) {
						for (int numberClusters : numbersClusters.get(numberObjects)) {
							for (int nstart : nstarts) {
								list.add(Arguments.of(similarityFunction, numberObjects, timeSeriesVariance,
										numberObjectsEnrichedTimeSeries, numberClusters, nstart));
							}
						}
					}
				}
			}
		}
		return list.stream();
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "hiddenTimeSeriesClustersArgumentProvider" })
	public void testEnrichedTimeSeriesInRandom(final ITimeSeriesSimilarityFunction simFunc, final int numberObjects,
			final double timeSeriesVariance, final int[] numberObjectsEnrichedTimeSeries, final int totalNumberClusters,
			final int nstart) throws Exception {
		try {
			ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
					.getLogger(Logger.ROOT_LOGGER_NAME);
			logger.setLevel(Level.WARN);

			clusteringMethodBuilder.setSimilarityFunction(simFunc);
			clusteringMethodBuilder.getPamkBuilder().setNstart(nstart);

			final List<Future<Void>> futures = new MyParallel<Void>(forPool)
					.For(LongArrayList.wrap(LongStream.range(0, 25).toArray()), new MyParallel.Operation<Long, Void>() {
						@Override
						public Void perform(Long dsSeed) throws Exception {
							final CLARAClusteringMethodBuilder clusteringMethodBuilder = TestHiddenClusterRecovery.clusteringMethodBuilder
									.copy();
							final ITimeSeriesSimilarityFunction similarityFunction = (ITimeSeriesSimilarityFunction) simFunc
									.copy();
							clusteringMethodBuilder.setSimilarityFunction(similarityFunction);
							final PrototypeBuilder prototypeBuilder = (PrototypeBuilder) clusteringMethodBuilder.prototypeBuilder;
							long startTime = System.currentTimeMillis();
							logger.debug(String.format("### DS seed: %d", dsSeed));

							List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility
									.enrichedTimeSeriesInRandom(new Random(dsSeed), numberObjects, 15,
											numberObjectsEnrichedTimeSeries, timeSeriesVariance);

							ITimeSeriesObjectList allObjects = new TimeSeriesObjectList();
							for (ITimeSeriesObjects os : enrichedTimeSeriesInRandom)
								allObjects.addAll(os);
							final ITimeSeriesObjectList randomObjects = enrichedTimeSeriesInRandom
									.get(enrichedTimeSeriesInRandom.size() - 1).asList();

							AbsoluteValues preprocessor = new AbsoluteValues();
							preprocessor.initializeObjects(allObjects);
							preprocessor.process();

							similarityFunction.setTimeSeriesPreprocessor(preprocessor);

							IDiscretizeTimeSeries discretizeTimeSeries = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
									preprocessor.minValue, preprocessor.maxValue, 15, 15);
							for (ITimeSeriesObject o : allObjects)
								for (int s = 0; s < o.getPreprocessedTimeSeriesList().length; s++)
									o.addPreprocessedTimeSeries(s, discretizeTimeSeries
											.discretizeObjectTimeSeries(o.getPreprocessedTimeSeriesList()[s]));

							prototypeBuilder.setPrototypeComponentBuilders(new TimeSeriesPrototypeComponentBuilder()
									.setAggregationFunction(new AggregateClusterMeanTimeSeries())
									.setDiscretizeFunction(discretizeTimeSeries));

							IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
									clusteringMethodBuilder, totalNumberClusters, dsSeed);

							BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
							IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process = basicClusteringProcessBuilder
									.build(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
											clusteringMethodBuilder, initialClusteringProvider,
											new LoadDataFromCollection(allObjects), new PreprocessingSummary(),
											prototypeBuilder, similarityFunction, TimePointWeighting.NONE, preprocessor,
											dsSeed, Collections.emptySet());
							process.start();

							process.doIteration();

							final long runtime = System.currentTimeMillis() - startTime;

							final LocalSearchClustering localSearchClustering = new LocalSearchClustering();
							localSearchClustering.computeIterationsUntilConvergence(process);

							final TiconeClusteringResult clusteringResult = process.getClusteringResult();
							final List<Integer> iterations = clusteringResult.getIterations();

							final ClusteringFeatureTotalObjectClusterSimilarity fas = new ClusteringFeatureTotalObjectClusterSimilarity(
									similarityFunction);

							for (int iteration : iterations) {
								final IClusterObjectMapping clustering = clusteringResult
										.getClusteringOfIteration(iteration);
								clustering.getClusters().forEach(c -> c.removeObjects(randomObjects));
								clustering.removeEmptyClusters();
//					System.out.println(clustering.asObjectPartition());
//					System.out.println(clustering.getUnassignedObjects());
								double jaccard = calculateJaccardIndex(
										enrichedTimeSeriesInRandom.subList(0, enrichedTimeSeriesInRandom.size() - 1),
										clustering.getClusters().stream().map(c -> c.getObjects())
												.collect(Collectors.toList()));
								assertTrue(jaccard >= 0.0 && jaccard <= 1.0);
								final IFeatureStore featureStore = clusteringResult.getFeatureStore(iteration);
								double swapProbability = clusteringMethodBuilder.getPamkBuilder().getSwapProbability();
								if (swapProbability == -1)
									swapProbability = PAMKClusteringMethod.recommendedSwapProbability(numberObjects,
											totalNumberClusters);
								synchronized (TestHiddenClusterRecovery.this) {
									System.out.println(String.format(
											"%s\t%d\t%.3f\t%s\t%d\t%d\t%d\t%s\t%.5f\t%d\t%d\t%.3f\t%d",
											similarityFunction, numberObjects, timeSeriesVariance,
											Arrays.toString(numberObjectsEnrichedTimeSeries), totalNumberClusters,
											dsSeed, iteration, "Object-Cluster Similarity",
											featureStore.getFeatureValue(clustering, fas).getValue().get(), runtime,
											clusteringMethodBuilder.getPamkBuilder().getNstart(), swapProbability,
											clusteringMethodBuilder.getPamkBuilder().getSwapIterations()));
									System.out.println(String.format(
											"%s\t%d\t%.3f\t%s\t%d\t%d\t%d\t%s\t%.5f\t%d\t%d\t%.3f\t%d",
											similarityFunction, numberObjects, timeSeriesVariance,
											Arrays.toString(numberObjectsEnrichedTimeSeries), totalNumberClusters,
											dsSeed, iteration, "Jaccard Index", jaccard, runtime,
											clusteringMethodBuilder.getPamkBuilder().getNstart(), swapProbability,
											clusteringMethodBuilder.getPamkBuilder().getSwapIterations()));
								}

							}
							return null;
						}
					});
			for (Future f : futures)
				f.get();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private TiconeNetworkImpl createEnrichedNetwork(final List<ITimeSeriesObjects> objects, final long dsSeed,
			final double foregroundEdgeProbability, final double backgroundEdgeProbability)
			throws InterruptedException {
		return TestDataUtility.enrichedSubnetworksInRandom(new Random(dsSeed), objects, false,
				backgroundEdgeProbability, foregroundEdgeProbability);
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "hiddenCompositeClustersArgumentProvider" })
	public void testEnrichedCompositeInRandom(final ICompositeSimilarityFunction simFunc, final int numberObjects,
			final double timeSeriesVariance, final double backgroundEdgeProbability,
			final int[] numberObjectsEnrichedTimeSeries, final int totalNumberClusters, final int nstart)
			throws Exception {
		try {
			final DecimalFormat format = new DecimalFormat("#.###");
			ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
					.getLogger(Logger.ROOT_LOGGER_NAME);
			logger.setLevel(Level.WARN);

			clusteringMethodBuilder.setSimilarityFunction(simFunc);
			clusteringMethodBuilder.getPamkBuilder().setNstart(nstart);

			final double foregroundEdgeProbability = 1.0;

			final List<Future<Void>> futures = new MyParallel<Void>(forPool)
					.For(LongArrayList.wrap(LongStream.range(0, 25).toArray()), new MyParallel.Operation<Long, Void>() {
						@Override
						public Void perform(Long dsSeed) throws Exception {
							TiconeNetworkImpl randomNetwork = null;
							try {
								// for (long dsSeed: LongStream.range(0, 25).toArray()) {
								final CLARAClusteringMethodBuilder clusteringMethodBuilder = TestHiddenClusterRecovery.clusteringMethodBuilder
										.copy();
								final ICompositeSimilarityFunction similarityFunction = (ICompositeSimilarityFunction) simFunc
										.copy();
								clusteringMethodBuilder.setSimilarityFunction(similarityFunction);
								final PrototypeBuilder prototypeBuilder = (PrototypeBuilder) clusteringMethodBuilder.prototypeBuilder;
								long startTime = System.currentTimeMillis();
								logger.debug(String.format("### DS seed: %d", dsSeed));

								List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility
										.enrichedTimeSeriesInRandom(new Random(dsSeed), numberObjects, 15,
												numberObjectsEnrichedTimeSeries, timeSeriesVariance);

								ITimeSeriesSimilarityFunction tsSF = null;
								INetworkBasedSimilarityFunction networkSF = null;
								for (ISimilarityFunction childSF : similarityFunction.getSimilarityFunctions())
									if (childSF instanceof INetworkBasedSimilarityFunction) {
										networkSF = (INetworkBasedSimilarityFunction) childSF;
										if (randomNetwork == null) {
											randomNetwork = createEnrichedNetwork(enrichedTimeSeriesInRandom, dsSeed,
													foregroundEdgeProbability, backgroundEdgeProbability);
										}
									} else if (childSF instanceof ITimeSeriesSimilarityFunction)
										tsSF = (ITimeSeriesSimilarityFunction) childSF;

								ITimeSeriesObjectList allObjects = new TimeSeriesObjectList();
								for (ITimeSeriesObjects os : enrichedTimeSeriesInRandom)
									allObjects.addAll(os.mapToNetwork(randomNetwork));
								final ITimeSeriesObjectList randomObjects = enrichedTimeSeriesInRandom
										.get(enrichedTimeSeriesInRandom.size() - 1).asList();

								if (networkSF != null) {
									networkSF.setNetwork(randomNetwork);
									randomNetwork.initializeForFeature(new ObjectPairFeatureShortestDistance(false));
								}

								AbsoluteValues preprocessor = new AbsoluteValues();
								preprocessor.initializeObjects(allObjects);
								preprocessor.process();

								if (tsSF != null)
									tsSF.setTimeSeriesPreprocessor(preprocessor);

								IDiscretizeTimeSeries discretizeTimeSeries = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
										preprocessor.minValue, preprocessor.maxValue, 15, 15);
								for (ITimeSeriesObject o : allObjects)
									for (int s = 0; s < o.getPreprocessedTimeSeriesList().length; s++)
										o.addPreprocessedTimeSeries(s, discretizeTimeSeries
												.discretizeObjectTimeSeries(o.getPreprocessedTimeSeriesList()[s]));

								prototypeBuilder.setPrototypeComponentBuilders(
										new NetworkLocationPrototypeComponentBuilder().setAggregationFunction(
												new AggregateClusterMedoidNode(randomNetwork, networkSF)),
										new TimeSeriesPrototypeComponentBuilder()
												.setAggregationFunction(new AggregateClusterMeanTimeSeries())
												.setDiscretizeFunction(discretizeTimeSeries));

								similarityFunction.setMissingChildHandling(MISSING_CHILD_HANDLING.RETURN_MISSING);

								// initialize scalers of similarity functions
								ObjectPairSimilarityFeature f1 = new ObjectPairSimilarityFeature(
										similarityFunction.getSimilarityFunctions()[0]);
								ObjectPairSimilarityFeature f2 = new ObjectPairSimilarityFeature(
										similarityFunction.getSimilarityFunctions()[1]);
								IScaler s1;
								try {
									s1 = new MinMaxScalerBuilder<>(f1, allObjects, 1000, dsSeed).setMapNanToZero(true)
											.build();
								} catch (Exception e) {
									s1 = new DummyScalerBuilder<>().build();
								}
								IScaler s2;
								try {
									s2 = new MinMaxScalerBuilder<>(f2, allObjects, 1000, dsSeed).setMapNanToZero(true)
											.build();
								} catch (Exception e) {
									s2 = new DummyScalerBuilder<>().build();
								}
								similarityFunction.setScalerForAnyPair(0, s1);
								similarityFunction.setScalerForAnyPair(1, s2);

								IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
										clusteringMethodBuilder, totalNumberClusters, dsSeed);

								BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
								IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process = basicClusteringProcessBuilder
										.build(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
												clusteringMethodBuilder, initialClusteringProvider,
												new LoadDataFromCollection(allObjects), new PreprocessingSummary(),
												prototypeBuilder, similarityFunction, TimePointWeighting.NONE,
												preprocessor, dsSeed, Collections.emptySet());
								process.start();

								process.doIteration();
								final LocalSearchClustering localSearchClustering = new LocalSearchClustering();
								localSearchClustering.computeIterationsUntilConvergence(process);

								final TiconeClusteringResult clusteringResult = process.getClusteringResult();
								final List<Integer> iterations = clusteringResult.getIterations();

								final ClusteringFeatureTotalObjectClusterSimilarity fas = new ClusteringFeatureTotalObjectClusterSimilarity(
										similarityFunction);

								for (int iteration : iterations) {
									final IClusterObjectMapping clustering = clusteringResult
											.getClusteringOfIteration(iteration);
									clustering.getClusters().forEach(c -> c.removeObjects(randomObjects));
									clustering.removeEmptyClusters();
									double jaccard = calculateJaccardIndex(
											enrichedTimeSeriesInRandom.subList(0,
													enrichedTimeSeriesInRandom.size() - 1),
											clustering.getClusters().stream().map(c -> c.getObjects())
													.collect(Collectors.toList()));
									assertTrue(jaccard >= 0.0 && jaccard <= 1.0);
									final IFeatureStore featureStore = clusteringResult.getFeatureStore(iteration);
									final long runtime = System.currentTimeMillis() - startTime;
									double swapProbability = clusteringMethodBuilder.getPamkBuilder()
											.getSwapProbability();
									if (swapProbability == -1)
										swapProbability = PAMKClusteringMethod.recommendedSwapProbability(numberObjects,
												totalNumberClusters);
									synchronized (TestHiddenClusterRecovery.this) {
										System.out.println(String.format(
												"%s\t%d\t%s\t%s/%s\t%s\t%d\t%d\t%d\t%s\t%.5f\t%d\t%d\t%.3f\t%d",
												similarityFunction, numberObjects, format.format(timeSeriesVariance),
												format.format(foregroundEdgeProbability),
												format.format(backgroundEdgeProbability),
												Arrays.toString(numberObjectsEnrichedTimeSeries), totalNumberClusters,
												dsSeed, iteration, "Object-Cluster Similarity",
												featureStore.getFeatureValue(clustering, fas).getValue().get(), runtime,
												clusteringMethodBuilder.getPamkBuilder().getNstart(), swapProbability,
												clusteringMethodBuilder.getPamkBuilder().getSwapIterations()));
										System.out.println(String.format(
												"%s\t%d\t%s\t%s/%s\t%s\t%d\t%d\t%d\t%s\t%.5f\t%d\t%d\t%.3f\t%d",
												similarityFunction, numberObjects, format.format(timeSeriesVariance),
												format.format(foregroundEdgeProbability),
												format.format(backgroundEdgeProbability),
												Arrays.toString(numberObjectsEnrichedTimeSeries), totalNumberClusters,
												dsSeed, iteration, "Jaccard Index", jaccard, runtime,
												clusteringMethodBuilder.getPamkBuilder().getNstart(), swapProbability,
												clusteringMethodBuilder.getPamkBuilder().getSwapIterations()));
									}
								}
								return null;
							} finally {
								if (randomNetwork != null) {
									randomNetwork.clearThreadPool();
								}
							}
						}
					});
			for (Future f : futures)
				f.get();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@ParameterizedTest
	@Tag("long-running")
	@MethodSource(value = { "hiddenSubnetworkClustersArgumentProvider" })
	public void testEnrichedSubnetworksInRandom(final INetworkBasedSimilarityFunction simFunc, final int numberObjects,
			final double backgroundEdgeProbability, final int[] numberObjectsEnrichedTimeSeries,
			final int totalNumberClusters, final int nstart) throws Exception {
		final BufferedWriter bw = new BufferedWriter(new FileWriter(new File("shortestpaths.csv"), true));

		try {
			final DecimalFormat format = new DecimalFormat("#.###");
			ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
					.getLogger(Logger.ROOT_LOGGER_NAME);
			logger.setLevel(Level.WARN);

			clusteringMethodBuilder.setSimilarityFunction(simFunc);
			clusteringMethodBuilder.getPamkBuilder().setNstart(nstart);

			final double foregroundEdgeProbability = 1.0;

			final List<Future<Void>> futures = new MyParallel<Void>(forPool)
					.For(LongArrayList.wrap(LongStream.range(0, 25).toArray()), new MyParallel.Operation<Long, Void>() {
						@Override
						public Void perform(Long dsSeed) throws Exception {
							final CLARAClusteringMethodBuilder clusteringMethodBuilder = TestHiddenClusterRecovery.clusteringMethodBuilder
									.copy();
							final INetworkBasedSimilarityFunction similarityFunction = (INetworkBasedSimilarityFunction) simFunc
									.copy();
							clusteringMethodBuilder.setSimilarityFunction(similarityFunction);
							final PrototypeBuilder prototypeBuilder = (PrototypeBuilder) clusteringMethodBuilder.prototypeBuilder;
							long startTime = System.currentTimeMillis();
							logger.debug(String.format("### DS seed: %d", dsSeed));

							List<ITimeSeriesObjects> enrichedTimeSeriesInRandom = TestDataUtility
									.enrichedTimeSeriesInRandom(new Random(dsSeed), numberObjects, 15,
											numberObjectsEnrichedTimeSeries, 0.0);

							INetworkBasedSimilarityFunction networkSF = null;
							TiconeNetworkImpl randomNetwork = null;
							networkSF = similarityFunction;
							randomNetwork = createEnrichedNetwork(enrichedTimeSeriesInRandom, dsSeed,
									foregroundEdgeProbability, backgroundEdgeProbability);
							networkSF.setNetwork(randomNetwork);

							ITimeSeriesObjectList allObjects = new TimeSeriesObjectList();
							for (ITimeSeriesObjects os : enrichedTimeSeriesInRandom)
								allObjects.addAll(os.mapToNetwork(randomNetwork));
							final ITimeSeriesObjectList randomObjects = enrichedTimeSeriesInRandom
									.get(enrichedTimeSeriesInRandom.size() - 1).asList();

							networkSF.setNetwork(randomNetwork);
							ObjectPairFeatureShortestDistance fsd = new ObjectPairFeatureShortestDistance(false);
							randomNetwork.initializeForFeature(fsd);

//							for (int i = 0; i < enrichedTimeSeriesInRandom.size(); i++) {
//								ITimeSeriesObjects os1 = enrichedTimeSeriesInRandom.get(i);
//								for (int j = i; j < enrichedTimeSeriesInRandom.size(); j++) {
//									ITimeSeriesObjects os2 = enrichedTimeSeriesInRandom.get(j);
//									for (ITimeSeriesObject o1 : os1) {
//										for (ITimeSeriesObject o2 : os2) {
//											Double dist = randomNetwork.getFeatureValue(fsd, new ObjectPair(o1, o2))
//													.getValue();
//											if (dist.isNaN() || dist.isInfinite())
//												continue;
//											synchronized (bw) {
//												bw.write(String.format("%d\t%d\t%d\t%s\t%d\t%d\t%d\t%d\n",
//														numberObjectsEnrichedTimeSeries.length,
//														numberObjectsEnrichedTimeSeries[0], totalNumberClusters,
//														format.format(backgroundEdgeProbability), dsSeed, i, j,
//														dist.intValue()));
//											}
//										}
//									}
//								}
//							}

							AbsoluteValues preprocessor = new AbsoluteValues();
							preprocessor.initializeObjects(allObjects);
							preprocessor.process();

							IDiscretizeTimeSeries discretizeTimeSeries = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
									preprocessor.minValue, preprocessor.maxValue, 15, 15);
							for (ITimeSeriesObject o : allObjects)
								for (int s = 0; s < o.getPreprocessedTimeSeriesList().length; s++)
									o.addPreprocessedTimeSeries(s, discretizeTimeSeries
											.discretizeObjectTimeSeries(o.getPreprocessedTimeSeriesList()[s]));

							prototypeBuilder.setPrototypeComponentBuilders(
									new NetworkLocationPrototypeComponentBuilder().setAggregationFunction(
											new AggregateClusterMedoidNode(randomNetwork, networkSF)));

							// initialize scaler of similarity function
							ObjectPairSimilarityFeature f = new ObjectPairSimilarityFeature(similarityFunction);

							IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = new InitialClusteringMethod(
									clusteringMethodBuilder, totalNumberClusters, dsSeed);

							BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
							IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> process = basicClusteringProcessBuilder
									.build(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
											clusteringMethodBuilder, initialClusteringProvider,
											new LoadDataFromCollection(allObjects), new PreprocessingSummary(),
											prototypeBuilder, similarityFunction, TimePointWeighting.NONE, preprocessor,
											dsSeed, Collections.emptySet());
							process.start();

							process.doIteration();
							final LocalSearchClustering localSearchClustering = new LocalSearchClustering();
							localSearchClustering.computeIterationsUntilConvergence(process);

							final TiconeClusteringResult clusteringResult = process.getClusteringResult();
							final List<Integer> iterations = clusteringResult.getIterations();

							final ClusteringFeatureTotalObjectClusterSimilarity fas = new ClusteringFeatureTotalObjectClusterSimilarity(
									similarityFunction);
							final ClusterFeatureAverageSimilarity cfas = new ClusterFeatureAverageSimilarity(
									similarityFunction);
							cfas.setUseCache(false);

							final long runtime = System.currentTimeMillis() - startTime;

							for (int iteration : iterations) {
								final IClusterObjectMapping clustering = clusteringResult
										.getClusteringOfIteration(iteration);
								clustering.getClusters().forEach(c -> c.removeObjects(randomObjects));
								clustering.removeEmptyClusters();
								double jaccard = calculateJaccardIndex(
										enrichedTimeSeriesInRandom.subList(0, enrichedTimeSeriesInRandom.size() - 1),
										clustering.getClusters().stream().map(c -> c.getObjects())
												.collect(Collectors.toList()));
								assertTrue(jaccard >= 0.0 && jaccard <= 1.0);
								final IFeatureStore featureStore = clusteringResult.getFeatureStore(iteration);
								double swapProbability = clusteringMethodBuilder.getPamkBuilder().getSwapProbability();
								if (swapProbability == -1)
									swapProbability = PAMKClusteringMethod.recommendedSwapProbability(numberObjects,
											totalNumberClusters);
								synchronized (TestHiddenClusterRecovery.this) {
									System.out.println(String.format(
											"%s\t%d\t%s/%s\t%s\t%d\t%d\t%d\t%s\t%.5f\t%d\t%d\t%.3f\t%d",
											similarityFunction, numberObjects, format.format(foregroundEdgeProbability),
											format.format(backgroundEdgeProbability),
											Arrays.toString(numberObjectsEnrichedTimeSeries), totalNumberClusters,
											dsSeed, iteration, "Object-Cluster Similarity",
											featureStore.getFeatureValue(clustering, fas).getValue().get(), runtime,
											clusteringMethodBuilder.getPamkBuilder().getNstart(), swapProbability,
											clusteringMethodBuilder.getPamkBuilder().getSwapIterations()));
									System.out.println(String.format(
											"%s\t%d\t%s/%s\t%s\t%d\t%d\t%d\t%s\t%.5f\t%d\t%d\t%.3f\t%d",
											similarityFunction, numberObjects, format.format(foregroundEdgeProbability),
											format.format(backgroundEdgeProbability),
											Arrays.toString(numberObjectsEnrichedTimeSeries), totalNumberClusters,
											dsSeed, iteration, "Jaccard Index", jaccard, runtime,
											clusteringMethodBuilder.getPamkBuilder().getNstart(), swapProbability,
											clusteringMethodBuilder.getPamkBuilder().getSwapIterations()));
								}
							}

							return null;
						}
					});
			for (Future f : futures)
				f.get();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}

	public static void main(String[] args) throws IncompatibleSimilarityFunctionException {
		if (args.length == 0)
			return;

		init();
		final TestHiddenClusterRecovery test = new TestHiddenClusterRecovery();
		if (args[0].equals("-timeSeries")) {
			Stream<Arguments> arguments = hiddenTimeSeriesClustersArgumentProvider();
			arguments.forEach(a -> {
				Object[] objects = a.get();

				try {
					test.testEnrichedTimeSeriesInRandom((ITimeSeriesSimilarityFunction) objects[0],
							(Integer) objects[1], (Double) objects[2], (int[]) objects[3], (Integer) objects[4],
							(Integer) objects[5]);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			});
		} else if (args[0].equals("-shortestpath")) {
			Stream<Arguments> arguments = hiddenSubnetworkClustersArgumentProvider();
			arguments.forEach(a -> {
				Object[] objects = a.get();

				try {
					test.testEnrichedSubnetworksInRandom((INetworkBasedSimilarityFunction) objects[0],
							(Integer) objects[1], (Double) objects[2], (int[]) objects[3], (Integer) objects[4],
							(Integer) objects[5]);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			});
		} else {
			Stream<Arguments> arguments = hiddenCompositeClustersArgumentProvider();
			arguments.forEach(a -> {
				Object[] objects = a.get();

				try {
					test.testEnrichedCompositeInRandom((ICompositeSimilarityFunction) objects[0], (Integer) objects[1],
							(Double) objects[2], (Double) objects[3], (int[]) objects[4], (Integer) objects[5],
							(Integer) objects[6]);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			});
		}
	}

	private static Stream<Arguments> hiddenCompositeClustersArgumentProvider()
			throws IncompatibleSimilarityFunctionException {
		final InverseShortestPathSimilarityFunction shortestPathSF = new InverseShortestPathSimilarityFunction();
		final PearsonCorrelationFunction pearsonSF = new PearsonCorrelationFunction();
		final InverseEuclideanSimilarityFunction euclideanSF = new InverseEuclideanSimilarityFunction();
		WeightedAverageCompositeSimilarityFunction weightedAverageCompositeSimilarityFunction = new WeightedAverageCompositeSimilarityFunction(
				pearsonSF, shortestPathSF);
		WeightedAverageCompositeSimilarityFunction weightedAverageCompositeSimilarityFunction2 = new WeightedAverageCompositeSimilarityFunction(
				euclideanSF, shortestPathSF);
		final ICompositeSimilarityFunction[] simFuncs = new ICompositeSimilarityFunction[] {
				weightedAverageCompositeSimilarityFunction, weightedAverageCompositeSimilarityFunction2 };

		final double[] timeSeriesVariances = ArraysExt.rev(new double[] { 0.8, 0.4, 0.2, 0.1, 0.05, 0.0 });

		final double[] backgroundEdgeProbabilities = ArraysExt
				.rev(new double[] { 1.0, 0.8, 0.5, 0.3, 0.2, 0.1, 0.05, 0.01, 0.001, 0.0 });

		int[] nstarts = new int[] { 50/* , 125, 250, 500, 1000 */ };
		final int[] numbersObjects = new int[] { 250 };

		final Int2ObjectMap<int[][]> numbersObjectsEnrichedTimeSeries = new Int2ObjectArrayMap<>();
		numbersObjectsEnrichedTimeSeries.put(250, new int[][] { new int[] { 10, 10, 10 }, new int[] { 25, 25, 25 },
				new int[] { 10, 10, 10, 10, 10 }, new int[] { 25, 25, 25, 25, 25 } });
		final Int2ObjectMap<int[]> numbersClusters = new Int2ObjectArrayMap<>();
		numbersClusters.put(250, new int[] { 10, 30 });

		final List<Arguments> list = new ArrayList<>();
		for (ICompositeSimilarityFunction similarityFunction : simFuncs) {
			for (int numberObjects : numbersObjects) {
				for (double timeSeriesVariance : timeSeriesVariances) {
					for (double bgEdgeProbability : backgroundEdgeProbabilities) {
						for (int[] numberObjectsEnrichedTimeSeries : numbersObjectsEnrichedTimeSeries
								.get(numberObjects)) {
							for (int numberClusters : numbersClusters.get(numberObjects)) {
								for (int nstart : nstarts) {
									list.add(Arguments.of(similarityFunction, numberObjects, timeSeriesVariance,
											bgEdgeProbability, numberObjectsEnrichedTimeSeries, numberClusters,
											nstart));
								}
							}
						}
					}
				}
			}
		}
		return list.stream();
	}

	private static Stream<Arguments> hiddenSubnetworkClustersArgumentProvider()
			throws IncompatibleSimilarityFunctionException {
		final ISimilarityFunction[] timeSeriesSF = new ISimilarityFunction[] {
				new InverseShortestPathSimilarityFunction() };

		int[] nstarts = new int[] { 50/* , 125, 250, 500, 1000 */ };
		final double[] backgroundEdgeProbabilities = ArraysExt
				.rev(new double[] { 1.0, 0.8, 0.5, 0.3, 0.2, 0.1, 0.05, 0.01, 0.001, 0.0 });

		final int[] numbersObjects = new int[] { 250 };

		final Int2ObjectMap<int[][]> numbersObjectsEnrichedTimeSeries = new Int2ObjectArrayMap<>();
		numbersObjectsEnrichedTimeSeries.put(250, new int[][] { new int[] { 10, 10, 10 }, new int[] { 25, 25, 25 },
				new int[] { 10, 10, 10, 10, 10 }, new int[] { 25, 25, 25, 25, 25 } });

		final Int2ObjectMap<int[]> numbersClusters = new Int2ObjectArrayMap<>();
		numbersClusters.put(250, new int[] { 10, 30 });

		final List<Arguments> list = new ArrayList<>();
		for (ISimilarityFunction similarityFunction : timeSeriesSF) {
			for (int numberObjects : numbersObjects) {
				for (double bgEdgeProbability : backgroundEdgeProbabilities) {
					for (int[] numberObjectsEnrichedTimeSeries : numbersObjectsEnrichedTimeSeries.get(numberObjects)) {
						for (int numberClusters : numbersClusters.get(numberObjects)) {
							for (int nstart : nstarts) {
								list.add(Arguments.of(similarityFunction, numberObjects, bgEdgeProbability,
										numberObjectsEnrichedTimeSeries, numberClusters, nstart));
							}
						}
					}
				}
			}
		}
		return list.stream();
	}

	private static double calculateJaccardIndex(final List<ITimeSeriesObjects> plantedClusters,
			final List<ITimeSeriesObjects> observedClusters) {
		final Object2IntOpenHashMap<ITimeSeriesObject> objectToPlantedCluster = new Object2IntOpenHashMap<>();
		for (int c = 0; c < plantedClusters.size(); c++) {
			for (ITimeSeriesObject o : plantedClusters.get(c)) {
				objectToPlantedCluster.put(o, c);
			}
		}

		final Object2IntOpenHashMap<ITimeSeriesObject> objectToObservedCluster = new Object2IntOpenHashMap<>();
		for (int c = 0; c < observedClusters.size(); c++) {
			for (ITimeSeriesObject o : observedClusters.get(c)) {
				objectToObservedCluster.put(o, c);
			}
		}

		final ITimeSeriesObjectList allObjects = plantedClusters.stream()
				.reduce(new TimeSeriesObjectSet(), (objs1, objs2) -> {
					objs1.addAll(objs2);
					return objs1;
				}).asList();

		int tp = 0, fp = 0, tn = 0, fn = 0;

		for (int oIdx1 = 0; oIdx1 < allObjects.size(); oIdx1++) {
			final ITimeSeriesObject o1 = allObjects.get(oIdx1);
			final int pc1 = objectToPlantedCluster.getInt(o1);
			final int oc1 = objectToObservedCluster.getInt(o1);
			for (int oIdx2 = oIdx1 + 1; oIdx2 < allObjects.size(); oIdx2++) {
				final ITimeSeriesObject o2 = allObjects.get(oIdx2);
				final int pc2 = objectToPlantedCluster.getInt(o2);
				final int oc2 = objectToObservedCluster.getInt(o2);

				if (pc1 == pc2) {
					if (oc1 == oc2) {
						tp++;
					} else {
						fn++;
					}
				} else {
					if (oc1 == oc2) {
						fp++;
					} else {
						tn++;
					}
				}
			}
		}

		return (double) tp / (tp + fp + fn);
	}

	private static final ScheduledThreadPoolExecutor forPool = new ScheduledThreadPoolExecutor(
			Runtime.getRuntime().availableProcessors());

}
