package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import de.wiwie.wiutils.utils.Pair;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.InverseShortestPathSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.WeightedAverageCompositeSimilarityFunction;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;

public class TestLocalSearchClustering {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void test() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		Pair<ClusterObjectMapping, IClusterList> pair = TestDataUtility.demoData(f, false);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getFirst().getAssignedObjects());
		preprocessor.process();

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();
		bla.doIteration();
		assertEquals(5, bla.getLatestClustering().getClusters().size());

		int numberIterations = bla.getHistory().getIterationNumber();
		assertEquals(1, numberIterations);

		new LocalSearchClustering(0.001, 100).computeIterationsUntilConvergence(bla);
		numberIterations = bla.getHistory().getIterationNumber();
		assertTrue(2 <= numberIterations);
	}

	@Test
	@Tag("long-running")
	public void testWithInfluenzaAndRhino() throws Exception {
		long seed = 42;
		TiconeNetworkImpl network = TestDataUtility.parseI2DPPINetwork();

//		for (TiconeNetworkNodeImpl n : new ArrayList<>(network.getNodeSet()).subList(1200, network.getNodeCount()))
//			network.removeNode(n);

		ITimeSeriesObjectList objectsInfluenza = TestDataUtility.parseInfluenzaExpressionData().mapToNetwork(network);
		ITimeSeriesObjectList objectsRhino = TestDataUtility.parseRhinoExpressionData().mapToNetwork(network);

		network.removeNodesNotInDataset(objectsInfluenza);
		network.removeNodesNotInDataset(objectsRhino);
		objectsInfluenza.removeIf(o -> !network.containsNode(o.getName()));
		objectsRhino.removeIf(o -> !network.containsNode(o.getName()));

		// influenza specific stuff
		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objectsInfluenza);
		preprocessor.process();

		PearsonCorrelationFunction psf = new PearsonCorrelationFunction();
		InverseShortestPathSimilarityFunction spsf = new InverseShortestPathSimilarityFunction(network);
		spsf.setEnsureKnownObjects(false);
		WeightedAverageCompositeSimilarityFunction simFunc = new WeightedAverageCompositeSimilarityFunction(psf, spsf);

		NetworkLocationPrototypeComponentBuilder nlf = new NetworkLocationPrototypeComponentBuilder();
		f.addPrototypeComponentFactory(nlf);
		nlf.setAggregationFunction(new AggregateClusterMedoidNode(network, spsf));

		int numberOfInitialClusters = 500;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> clusteringMethod = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(simFunc).setPrototypeBuilder(f);

		BasicClusteringProcess bla = new BasicClusteringProcess(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
				clusteringMethod, new InitialClusteringMethod(clusteringMethod, numberOfInitialClusters, 42),
				new LoadDataFromCollection(objectsInfluenza), new PreprocessingSummary(), f, psf,
				TimePointWeighting.NONE, preprocessor, seed);
		new LocalSearchClustering(0.001, 100).computeIterationsUntilConvergence(bla);
		ClusterObjectMapping clusteringInfluenza = bla.getLatestClustering();

		// rhino specific stuff

		preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objectsRhino);
		preprocessor.process();

		discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.getMinValue(), preprocessor.getMaxValue(), 20, 20);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		bla = new BasicClusteringProcess(new TiconeClusteringResultFactory(), IdMapMethod.NONE, clusteringMethod,
				new InitialClusteringMethod(clusteringMethod, numberOfInitialClusters, 42),
				new LoadDataFromCollection(objectsRhino), new PreprocessingSummary(), f, psf, TimePointWeighting.NONE,
				preprocessor, seed);
		new LocalSearchClustering(0.001, 100).computeIterationsUntilConvergence(bla);
		ClusterObjectMapping clusteringRhino = bla.getLatestClustering();
	}
}
