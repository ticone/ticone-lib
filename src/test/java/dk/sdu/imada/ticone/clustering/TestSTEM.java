package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.network.TiconeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TimePointWeighting;

public class TestSTEM {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void test() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<STEMClusteringMethod> initialClusteringInterface = new STEMClusteringMethodBuilder()
				.setTimepoints(5).setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> pair = demoData(true);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair.getLeft().getAssignedObjects());
		preprocessor.process();

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair.getLeft().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		// TODO: don't know how to validate the number of clusters for STEM
		// assertEquals(2, bla.getClusterObjectMapping().clusterSet().size());

		int numberIterations = bla.getHistory().getIterationNumber();
		assertEquals(1, numberIterations);
	}

	protected Triple<ClusterObjectMapping, TiconeNetwork<?, ?>, IClusterList> demoData(boolean isDirected)
			throws Exception {
		ClusterObjectMapping demoPom = new ClusterObjectMapping(f);
		TiconeNetwork<?, ?> network = new TiconeNetworkImpl("bla");

		Map<String, ICluster> demoEntrezToNetworks = new HashMap<>();
		List<String> demoNodes = new ArrayList<>();
		Map<String, List<String>> demoEdges = new HashMap<>();
		IClusters demoClusters = new ClusterSet();

		// generate 3 clusters with 3 objects each;
		Random r = new Random(14);
		for (int c = 0; c < 3; c++) {
			tsf.setTimeSeries(r.doubles(5).toArray());
			ICluster p = demoPom.getClusterBuilder().setPrototype(f.build()).build();
			demoClusters.add(p);

			for (int o = 0; o < 3; o++) {
				String objId = "Object " + (c * 3 + o + 1);
				demoNodes.add(objId);
				network.addNode(objId);
				demoEntrezToNetworks.put(objId, p);
				double[] ts = new double[5];
				for (int i = 0; i < ts.length; i++) {
					ts[i] = r.nextDouble();
				}
				ITimeSeriesObject data = new TimeSeriesObject(objId, 1, TimeSeries.of(ts));
				demoPom.addMapping(data, p, TestSimpleSimilarityValue.of(r.nextDouble()));
			}
			// create 2 directed edges within cluster
			String edgeSource = p.getObjects().get(0).getName();
			if (!demoEdges.containsKey(edgeSource))
				demoEdges.put(edgeSource, new ArrayList<String>());
			String edgeTarget = p.getObjects().get(1).getName();
			demoEdges.get(edgeSource).add(edgeTarget);
			network.addEdge(edgeSource, edgeTarget, isDirected);
			if (!demoEdges.containsKey(edgeTarget))
				demoEdges.put(edgeTarget, new ArrayList<String>());
			demoEdges.get(edgeTarget).add(edgeSource);
			network.addEdge(edgeTarget, edgeSource, isDirected);
		}

		// create 3 directed edges from each cluster to every other cluster
		for (ICluster p1 : demoClusters) {
			for (ICluster p2 : demoClusters) {
				if (p1.equals(p2))
					continue;
				String edgeSource = p1.getObjects().get(1).getName();
				if (!demoEdges.containsKey(edgeSource))
					demoEdges.put(edgeSource, new ArrayList<String>());
				String edgeTarget = p2.getObjects().get(0).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(1).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
				edgeTarget = p2.getObjects().get(2).getName();
				demoEdges.get(edgeSource).add(edgeTarget);
				network.addEdge(edgeSource, edgeTarget, isDirected);
			}
		}
		return Triple.of(demoPom, network, (IClusterList) new ClusterList(demoClusters));
	}

}
