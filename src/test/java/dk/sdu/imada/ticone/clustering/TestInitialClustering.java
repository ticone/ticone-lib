package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import dk.sdu.imada.ticone.Constants;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.preprocessing.calculation.FoldChangeCalculator;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.IProgress;
import dk.sdu.imada.ticone.util.Utility;

/**
 * @author Christian Nørskov
 */
public class TestInitialClustering {

	final String file_sep = Constants.file_sep;
	final String path_to_testfiles = Constants.path_to_testfiles;

	/*
	 * @Test public void testFindClusters() {
	 * 
	 * File timeseriesFile = new File(path_to_testfiles + "clustering" + file_sep +
	 * "easyDataset-patterns5-objects10-seed42.txt"); List<TimeSeriesData>
	 * timeSeriesDatas = null;
	 * 
	 * try { timeSeriesDatas = Parser.parseExpressions(timeseriesFile); } catch
	 * (FileNotFoundException fnfe) { fail("File not found"); } FoldChangeCalculator
	 * patternCalculationFoldChange = new FoldChangeCalculator();
	 * patternCalculationFoldChange.initializeTimeSeriesData(timeSeriesDatas);
	 * patternCalculationFoldChange.calculatePatterns(); timeSeriesDatas =
	 * patternCalculationFoldChange.getTimeSeriesDatas();
	 * 
	 * Progress progress = new Progress(); Utility.setProgress(progress);
	 * 
	 * DiscretizePatternWithStepsFromMinToZeroAndZeroToMax
	 * discretizePatternInterface = new
	 * DiscretizePatternWithStepsFromMinToZeroAndZeroToMax(
	 * patternCalculationFoldChange.getMinValue(),
	 * patternCalculationFoldChange.getMaxValue(), 4, 4); ISimilarity similarityFunc
	 * = new Pearson(); IRefinePattern refinePattern = new RefinePatternMean();
	 * double pairwiseSimiliarityThreshold = 0.5;
	 * 
	 * IClustering clustering = new TransClustClustering(
	 * discretizePatternInterface, similarityFunc, pairwiseSimiliarityThreshold,
	 * refinePattern); IClusterObjectMapping patternObjectMapping =
	 * clustering.findClusters( timeSeriesDatas, 5); assertNotNull(
	 * "IClusterObjectMapping is null", patternObjectMapping);
	 * 
	 * Iterator<Pattern> patternIterator = patternObjectMapping .patternIterator();
	 * int numberOfPatterns = 0; while (patternIterator.hasNext()) {
	 * numberOfPatterns++; patternIterator.next(); } assertEquals(5,
	 * numberOfPatterns, 1); }
	 */

	@Test
	public void testFindClustersPAMK() throws Exception {
		File timeseriesFile = new File(getClass()
				.getResource(path_to_testfiles + "clustering" + file_sep + "easyDataset-patterns5-objects10-seed42.txt")
				.getFile());
		ITimeSeriesObjectList timeSeriesDatas = null;

		try {
			Scanner scan = new Scanner(timeseriesFile);
			timeSeriesDatas = Parser.parseObjects(scan);
			scan.close();
		} catch (FileNotFoundException fnfe) {
			fail("File not found");
		}
		FoldChangeCalculator patternCalculationFoldChange = new FoldChangeCalculator();
		patternCalculationFoldChange.initializeObjects(timeSeriesDatas);
		patternCalculationFoldChange.process();
		timeSeriesDatas = patternCalculationFoldChange.getObjects();

		PrototypeBuilder prototypeFactory = new PrototypeBuilder().addPrototypeComponentFactory(
				new TimeSeriesPrototypeComponentBuilder().setAggregationFunction(new AggregateClusterMeanTimeSeries())
						.setDiscretizeFunction(new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(-3, 3, 4, 4)));

		PAMKClusteringMethod PAMKClustering = new PAMKClusteringMethodBuilder()
				.setSimilarityFunction(new PearsonCorrelationFunction()).setPrototypeBuilder(prototypeFactory).build();
		IClusterObjectMapping patternObjectMapping = PAMKClustering.findClusters(timeSeriesDatas, 5, 42);

		int numberOfClusters = 0;
		int numberOfObjects = 0;
		for (ICluster pattern : patternObjectMapping.getClusters()) {
			numberOfObjects += pattern.getObjects().size();
			numberOfClusters++;
		}
		assertEquals("Number of clusters", 5, numberOfClusters, 1);
		assertEquals("Number of objects", timeSeriesDatas.size(), numberOfObjects);
	}

	// @Test
	public void testReset() throws Exception {
		File timeseriesFile = new File(getClass()
				.getResource(path_to_testfiles + "clustering" + file_sep + "easyDataset-patterns5-objects10-seed42.txt")
				.getFile());
		ITimeSeriesObjectList timeSeriesDatas = null;

		try {
			Scanner scan = new Scanner(timeseriesFile);
			timeSeriesDatas = Parser.parseObjects(scan);
			scan.close();
		} catch (FileNotFoundException fnfe) {
			fail("File not found");
		}
		FoldChangeCalculator patternCalculationFoldChange = new FoldChangeCalculator();
		patternCalculationFoldChange.initializeObjects(timeSeriesDatas);
		patternCalculationFoldChange.process();
		timeSeriesDatas = patternCalculationFoldChange.getObjects();

		IProgress progress = Utility.getProgress();

		DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax discretizePatternInterface = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				patternCalculationFoldChange.getMinValue(), patternCalculationFoldChange.getMaxValue(), 4, 4);
		PearsonCorrelationFunction similarityFunc = new PearsonCorrelationFunction();
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		double pairwiseSimiliarityThreshold = 0.5;

		PrototypeBuilder prototypeFactory = new PrototypeBuilder()
				.addPrototypeComponentFactory(new TimeSeriesPrototypeComponentBuilder()
						.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternInterface));

		IClusteringMethod clustering = new TransClustClusteringMethod(prototypeFactory, similarityFunc,
				pairwiseSimiliarityThreshold);
		IClusterObjectMapping patternObjectMapping = clustering.findClusters(timeSeriesDatas, 5, 42);

		/*
		 * ITimeSeriesClusteringWithOverrepresentedPatterns bla = new
		 * BasicTimeSeriesClusteringWithOverrepresentedPatterns(
		 * patternCalculationFoldChange, similarityFunc, new PermutateDatasetGlobally(),
		 * 0, 5, discretizePatternInterface, clustering);
		 * bla.reset(patternObjectMapping);
		 */
	}
}
