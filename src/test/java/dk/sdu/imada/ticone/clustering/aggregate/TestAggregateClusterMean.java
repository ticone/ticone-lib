package dk.sdu.imada.ticone.clustering.aggregate;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.BasicClusteringProcessBuilder;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethod;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.PrespecifiedInitialClustering;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResultFactory;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TimePointWeighting;

public class TestAggregateClusterMean {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
	}

	@Test
	public void testSeed42() throws Exception {
		Scanner scan = new Scanner(new File(getClass().getResource("/test_files/clustering/seed42.txt").getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		Map<Integer, ICluster> idToCluster = new HashMap<>();
		BufferedReader r = new BufferedReader(
				new FileReader(getClass().getResource("/test_files/clustering/seed42_clustering.txt").getFile()));
		while (r.ready()) {
			String nextLine = r.readLine();
			String[] split = nextLine.split("\t");
			int id = Integer.valueOf(split[0]);
			if (!idToCluster.containsKey(id)) {
				tsf.setTimeSeries(new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 });
				ICluster cluster = pom.getClusterBuilder().setPrototype(f.build()).setClusterNumber(id)
						.setInternalClusterNumber((long) id).setIsKeep(false).build();
				idToCluster.put(id, cluster);
			}
			pom.addMapping(idToObj.get(split[1]), idToCluster.get(id), AbstractSimilarityValue.MAX);
		}
		r.close();

		IAggregateCluster<ITimeSeries> agg = new AggregateClusterMeanTimeSeries();

		for (ICluster c : pom.getClusters()) {
			ITimeSeriesObjects cos = c.getObjects();
			double[] a = agg.aggregate(cos).asArray();

			System.out.println(ArraysExt.toSeparatedString(a));

			for (int tp = 0; tp < a.length; tp++) {
				double min = Double.MAX_VALUE;
				double max = -Double.MAX_VALUE;

				for (ITimeSeriesObject o : cos) {
					for (ITimeSeries s : o.getPreprocessedTimeSeriesList()) {
						min = Math.min(min, s.get(tp));
						max = Math.max(max, s.get(tp));
					}
				}
				assertTrue(a[tp] >= min);
				assertTrue(a[tp] <= max);
			}
		}
	}

	@Test
	public void testSeed42_2() throws Exception {
		InverseEuclideanSimilarityFunction similarityFunction = new InverseEuclideanSimilarityFunction();
		int numberOfInitialClusters = 10;
		Scanner scan = new Scanner(new File(getClass().getResource("/test_files/clustering/seed42.txt").getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan,
				new ImportColumnMapping(1, "Column 2", 0, "Column 1", null, null));

		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		Map<String, ITimeSeriesObject> idToObj = new HashMap<>();
		for (ITimeSeriesObject o : objs)
			idToObj.put(o.getName(), o);

		ClusterObjectMapping pom = new ClusterObjectMapping(f);
		Map<Integer, ICluster> idToCluster = new HashMap<>();
		BufferedReader r = new BufferedReader(
				new FileReader(getClass().getResource("/test_files/clustering/seed42_clustering.txt").getFile()));
		while (r.ready()) {
			String nextLine = r.readLine();
			String[] split = nextLine.split("\t");
			int id = Integer.valueOf(split[0]);
			if (!idToCluster.containsKey(id)) {
				tsf.setTimeSeries(new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 });
				ICluster cluster = pom.getClusterBuilder().setPrototype(f.build()).setClusterNumber(id)
						.setInternalClusterNumber((long) id).setIsKeep(false).build();
				idToCluster.put(id, cluster);
			}
			pom.addMapping(idToObj.get(split[1]), idToCluster.get(id), AbstractSimilarityValue.MAX);
		}
		r.close();

		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 10, 10);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);

		List<IArithmeticFeature<? extends Comparable<?>>> featureList = new ArrayList<>();
		featureList.add(new ClusterFeatureNumberObjects());
		featureList.add(new ClusterFeatureAverageSimilarity(similarityFunction));

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		IInitialClusteringProvider initialClustering = new PrespecifiedInitialClustering(pom);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(objs), new PreprocessingSummary(), f, similarityFunction,
				TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		bla.doIteration();
		bla.doIteration();

		// // perform iterations until we arrive at the optimal clustering
		// LocalSearchClustering.computeIterationsUntilConvergence(bla);
		// List<PatternHistory> hist = new ArrayList<PatternHistory>();
		// PatternHistory ph = bla.getHistory();
		// hist.add(0, ph);
		// while (ph.getParent() != null) {
		// ph = ph.getParent();
		// hist.add(0, ph);
		// }

		pom = bla.getLatestClustering();

		for (ICluster c : pom.getClusters()) {
			ITimeSeriesObjects cos = c.getObjects();
			double[] a = PrototypeComponentType.TIME_SERIES.getComponent(c.getPrototype()).getTimeSeries().asArray();

			double[] mins = new double[a.length];
			double[] maxs = new double[a.length];

			for (int tp = 0; tp < a.length; tp++) {
				double min = Double.MAX_VALUE;
				double max = -Double.MAX_VALUE;

				for (ITimeSeriesObject o : cos) {
					for (ITimeSeries s : o.getPreprocessedTimeSeriesList()) {
						min = Math.min(min, s.get(tp));
						max = Math.max(max, s.get(tp));
					}
				}
				mins[tp] = min;
				maxs[tp] = max;
			}

			// prototype
			for (int tp = 0; tp < a.length; tp++) {
				assertTrue(a[tp] >= mins[tp]);
				assertTrue(a[tp] <= maxs[tp]);
			}

		}
	}

	@Test
	public void test() throws Exception {
		ITimeSeriesObjectList objs = new TimeSeriesObjectList();

		ITimeSeriesObject obj = new TimeSeriesObject("obj1", 1, new TimeSeries(new double[] { 1, 2, 3 }));
		obj.addPreprocessedTimeSeries(0, obj.getOriginalTimeSeriesList()[0]);
		objs.add(obj);

		IAggregateCluster<ITimeSeries> agg = new AggregateClusterMeanTimeSeries();

		double[] aggregateCluster = agg.aggregate(objs).asArray();

		assertArrayEquals(new double[] { 1, 2, 3 }, aggregateCluster, 0.0);
	}

	@Test
	public void test_2identical_samples() throws Exception {
		ITimeSeriesObjectList objs = new TimeSeriesObjectList();

		ITimeSeriesObject obj = new TimeSeriesObject("obj1", 2, new TimeSeries(new double[] { 1, 2, 3 }));
		obj.addOriginalTimeSeriesToList(1, new TimeSeries(new double[] { 1, 2, 3 }), "2");
		obj.addPreprocessedTimeSeries(0, obj.getOriginalTimeSeriesList()[0]);
		obj.addPreprocessedTimeSeries(1, obj.getOriginalTimeSeriesList()[1]);
		objs.add(obj);

		IAggregateCluster<ITimeSeries> agg = new AggregateClusterMeanTimeSeries();

		double[] aggregateCluster = agg.aggregate(objs).asArray();

		assertArrayEquals(new double[] { 1, 2, 3 }, aggregateCluster, 0.0);
	}

	@Test
	public void test_2diff_samples() throws Exception {
		ITimeSeriesObjectList objs = new TimeSeriesObjectList();

		ITimeSeriesObject obj = new TimeSeriesObject("obj1", 2, new TimeSeries(new double[] { 1, 2, 3 }));
		obj.addOriginalTimeSeriesToList(1, new TimeSeries(new double[] { 3, 2, 1 }), "2");
		obj.addPreprocessedTimeSeries(0, obj.getOriginalTimeSeriesList()[0]);
		obj.addPreprocessedTimeSeries(1, obj.getOriginalTimeSeriesList()[1]);
		objs.add(obj);

		IAggregateCluster<ITimeSeries> agg = new AggregateClusterMeanTimeSeries();

		double[] aggregateCluster = agg.aggregate(objs).asArray();

		assertArrayEquals(new double[] { 2, 2, 2 }, aggregateCluster, 0.0);
	}

}
