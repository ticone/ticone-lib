package dk.sdu.imada.ticone.clustering;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wiwie.wiutils.utils.Pair;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.io.ImportColumnMapping;
import dk.sdu.imada.ticone.io.LoadDataFromCollection;
import dk.sdu.imada.ticone.io.Parser;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.IdMapMethod;
import dk.sdu.imada.ticone.util.TestDataUtility;
import dk.sdu.imada.ticone.util.TimePointWeighting;

public class TestCLARAClustering {

	protected PrototypeBuilder f;
	protected TimeSeriesPrototypeComponentBuilder tsf;

	@BeforeEach
	public void setUp() {
		f = new PrototypeBuilder();
		tsf = new TimeSeriesPrototypeComponentBuilder();
		f.setPrototypeComponentBuilders(tsf);
//		ClusterObjectMapping.resetClusterCount();
		ClusterObjectMapping.nextClusteringId = 0;
	}

	@Test
	public void testDeterministic() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 5;
		IDiscretizeTimeSeries discretizePatternFunction = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(0,
				1, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		IClusteringMethodBuilder<CLARAClusteringMethod> initialClusteringInterface = new CLARAClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f);
		Pair<ClusterObjectMapping, IClusterList> pair1 = TestDataUtility.demoData(f, false);

		ITimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair1.getFirst().getAssignedObjects());
		preprocessor.process();

		IInitialClusteringProvider initialClustering = new InitialClusteringMethod(initialClusteringInterface,
				numberOfInitialClusters, 42);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		BasicClusteringProcessBuilder<TiconeClusteringResult> basicClusteringProcessBuilder = new BasicClusteringProcessBuilder<>();
		IClusteringProcess<ClusterObjectMapping, TiconeClusteringResult> bla = basicClusteringProcessBuilder.build(
				new TiconeClusteringResultFactory(), IdMapMethod.NONE, initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair1.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		IClusterObjectMapping pom1 = bla.doIteration();
		assertEquals(5, pom1.getClusters().size());

		int numberIterations = bla.getHistory().getIterationNumber();
		assertEquals(1, numberIterations);

		List<IArithmeticFeature<? extends Comparable<?>>> featureList = new ArrayList<>();
		featureList.add(new ClusterFeatureNumberObjects());
		featureList.add(new ClusterFeatureAverageSimilarity(similarityFunction));

		// perform same procedure again
//		ClusterObjectMapping.resetClusterCount();
		ClusterObjectMapping.nextClusteringId = 0;
		random = new Random(42);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePatternFunction);

		initialClusteringInterface = new CLARAClusteringMethodBuilder().setSimilarityFunction(similarityFunction)
				.setPrototypeBuilder(f);
		Pair<ClusterObjectMapping, IClusterList> pair2 = TestDataUtility.demoData(f, false);

		assertEquals(pair1.getFirst().getAssignedObjects(), pair2.getFirst().getAssignedObjects());

		preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(pair2.getFirst().getAssignedObjects());
		preprocessor.process();

		initialClustering = new InitialClusteringMethod(initialClusteringInterface, numberOfInitialClusters, 42);

		tsf.setAggregationFunction(new AggregateClusterMeanTimeSeries())
				.setDiscretizeFunction(discretizePatternFunction);

		bla = basicClusteringProcessBuilder.build(new TiconeClusteringResultFactory(), IdMapMethod.NONE,
				initialClusteringInterface, initialClustering,
				new LoadDataFromCollection(pair2.getFirst().getAssignedObjects()), new PreprocessingSummary(), f,
				similarityFunction, TimePointWeighting.NONE, preprocessor, 42l, Collections.emptySet());
		bla.start();

		IClusterObjectMapping pom2 = bla.doIteration();
		assertEquals(5, pom2.getClusters().size());

		numberIterations = bla.getHistory().getIterationNumber();
		assertEquals(1, numberIterations);

		assertEquals(pom1.getClusters().size(), pom2.getClusters().size());

		assertEquals(pom1.asObjectPartition(), pom2.asObjectPartition());
	}

	@Test
	public void testMergeTestDataSampleData() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 10;

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5_mergeTestCase.txt")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePattern);

		CLARAClusteringMethod clara = new CLARAClusteringMethodBuilder().setSampleSize(60)
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f).build();
		ITimeSeriesObjects sampleData = clara.findSampleData(objs, 60, new Random(14));

		List<String> expectedSampleNames = Arrays.asList("obj0", "obj15", "obj18", "obj35", "obj37", "obj43", "obj46",
				"obj63", "obj64", "obj65", "obj67", "obj69", "obj70", "obj84", "obj94", "obj97", "obj98", "obj99",
				"obj104", "obj105", "obj108", "obj109", "obj123", "obj126", "obj134", "obj138", "obj140", "obj150",
				"obj153", "obj156", "obj161", "obj174", "obj183", "obj185", "obj193", "obj199", "obj208", "obj209",
				"obj213", "obj217", "obj219", "obj223", "obj234", "obj238", "obj241", "obj260", "obj264", "obj281",
				"obj293", "obj295", "obj300", "obj306", "obj308", "obj310", "obj311", "obj326", "obj328", "obj350",
				"obj351", "obj359");
		ITimeSeriesObjectList expectedSampleData = new TimeSeriesObjectList();
		for (ITimeSeriesObject o : objs)
			if (expectedSampleNames.contains(o.getName()))
				expectedSampleData.add(o);
		assertEquals(expectedSampleData, sampleData);
	}

//	@Test
	public void testMergeTestDataInitialPamKClustering() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		int numberOfInitialClusters = 10;

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5_mergeTestCase.txt")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePattern);

		CLARAClusteringMethod clara = new CLARAClusteringMethodBuilder().setSampleSize(60)
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f).build();
		ITimeSeriesObjects sampleData = clara.findSampleData(objs, 60, new Random(14));
		PAMKClusteringMethod pamkClustering = new PAMKClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f).build();

		IClusterObjectMapping pom = pamkClustering.findClusters(sampleData, numberOfInitialClusters, 42);
		System.out.println(pom);

		Map<Integer, Set<String>> clusterObjectIds = new HashMap<>(), expectedClusterObjectIds = new HashMap<>();
		for (ICluster c : pom.getClusters())
			clusterObjectIds.putIfAbsent(c.getClusterNumber(),
					c.getObjects().stream().map(o -> o.getName()).collect(Collectors.toSet()));

		expectedClusterObjectIds.put(1, new HashSet<>(Arrays.asList("obj123", "obj310", "obj293", "obj326", "obj359")));
		expectedClusterObjectIds.put(2, new HashSet<>(Arrays.asList("obj69", "obj46", "obj18", "obj70", "obj63")));
		expectedClusterObjectIds.put(3,
				new HashSet<>(Arrays.asList("obj37", "obj67", "obj35", "obj264", "obj0", "obj260", "obj43")));
		expectedClusterObjectIds.put(4,
				new HashSet<>(Arrays.asList("obj234", "obj185", "obj193", "obj208", "obj219", "obj328")));
		expectedClusterObjectIds.put(5, new HashSet<>(Arrays.asList("obj15")));
		expectedClusterObjectIds.put(6, new HashSet<>(Arrays.asList("obj295", "obj281", "obj65", "obj64")));
		expectedClusterObjectIds.put(7, new HashSet<>(Arrays.asList("obj213", "obj223", "obj199", "obj174", "obj209")));
		expectedClusterObjectIds.put(8, new HashSet<>(Arrays.asList("obj161", "obj183", "obj217", "obj238")));
		expectedClusterObjectIds.put(9, new HashSet<>(Arrays.asList("obj105", "obj126", "obj300", "obj311", "obj156",
				"obj153", "obj241", "obj150", "obj94", "obj109", "obj108", "obj306", "obj97")));
		expectedClusterObjectIds.put(10, new HashSet<>(Arrays.asList("obj138", "obj104", "obj134", "obj351", "obj350",
				"obj84", "obj140", "obj308", "obj98", "obj99")));

		assertEquals(expectedClusterObjectIds, clusterObjectIds);
	}

	@Test
	public void testMergeTestDataInitialTotalSimilarity() throws Exception {
		PearsonCorrelationFunction similarityFunction = new PearsonCorrelationFunction();
		similarityFunction.setNormalizeByNumberTimepoints(false);
		int numberOfInitialClusters = 10;

		Scanner scan = new Scanner(new File(getClass().getResource(
				"/test_files/clustering/qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5_mergeTestCase.txt")
				.getFile()));
		ITimeSeriesObjectList objs = Parser.parseObjects(scan, new ImportColumnMapping(2, "Column 2", 1, "Column 1",
				new int[] { 3, 4, 5, 6, 7 }, new String[] { "tp1", "tp2", "tp3", "tp4", "tp5" }));
		AbstractTimeSeriesPreprocessor preprocessor = new AbsoluteValues();
		preprocessor.initializeObjects(objs);
		preprocessor.process();

		IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				preprocessor.minValue, preprocessor.maxValue, 4, 4);
		IAggregateCluster<ITimeSeries> refinePattern = new AggregateClusterMeanTimeSeries();
		Random random = new Random(14);

		tsf.setAggregationFunction(refinePattern).setDiscretizeFunction(discretizePattern);

		CLARAClusteringMethod clara = new CLARAClusteringMethodBuilder().setSampleSize(60)
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f).build();
		ITimeSeriesObjects sampleData = clara.findSampleData(objs, 60, new Random(14));
		PAMKClusteringMethod pamkClustering = new PAMKClusteringMethodBuilder()
				.setSimilarityFunction(similarityFunction).setPrototypeBuilder(f).build();

		ClusterObjectMapping pom = pamkClustering.findClusters(sampleData, numberOfInitialClusters, 42);
		ISimilarityValue simSum = pom.calculateAverageObjectClusterSimilarity(similarityFunction);
		assertEquals(0.939, simSum.get(), 0.01);
	}
}
