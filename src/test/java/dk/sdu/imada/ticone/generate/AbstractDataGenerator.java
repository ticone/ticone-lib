package dk.sdu.imada.ticone.generate;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.Cluster;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since May 11, 2017
 *
 */
public class AbstractDataGenerator {

	// Specifies the number of samples of each object
	private int totalObjects;
	private int numberOfTimepoints;
	private int sampleNumber;
	private boolean identicalSamples;
	private boolean hardcodedPatterns;
	private boolean hardcodedEdgeProbabilities;
	private double hardcodedEdgeProbability;
	private int numberOfPatterns;
	private int numberOfObjects;
	private double minFoldChange;
	private double maxFoldChange;
	private int bgObjects;

	private ISimilarityValue globalMinSimilarity;

	private List<double[]> patternList;

	// For quality testing
	private double maxVariance;
	private ISimilarityValue minSimilarity;

	private int seed;
	private Random random;
	private Random networkRandom;

	// Network density
	// private final double NETWORK_DENSITY = 0.01;
	private double edgeProbabilityReductionFactor;// 0.02;
	private double samePatternEdgeProbability;
	// private final double NOT_IN_CLUSTER_EDGE_PROBABILITY = 0.2 *
	// EDGE_PROBABILITY_REDUCTION_FACTOR;
	private boolean undirectedNetwork;
	private double[][] interClusterEdgeProbabilityMap = new double[numberOfPatterns][numberOfPatterns];
	// first index is background to itself
	private double[] clusterToBackgroundEdgeProbabilities;

	public AbstractDataGenerator(final String dataDirectory, int totalObjects, int numberOfTimepoints, int sampleNumber,
			boolean identicalSamples, boolean hardcodedPatterns, boolean hardcodedEdgeProbabilities,
			double hardcodedEdgeProbability, int numberOfPatterns, int numberOfObjects, double minFoldChange,
			double maxFoldChange, int bgObjects, ISimilarityValue globalMinSimilarity, List<double[]> patternList,
			double maxVariance, ISimilarityValue minSimilarity, int seed, double edgeProbabilityReductionFactor,
			double samePatternEdgeProbability, boolean undirectedNetwork) {
		super();
		this.dataDirectory = dataDirectory;
		this.totalObjects = totalObjects;
		this.numberOfTimepoints = numberOfTimepoints;
		this.sampleNumber = sampleNumber;
		this.identicalSamples = identicalSamples;
		this.hardcodedPatterns = hardcodedPatterns;
		this.hardcodedEdgeProbabilities = hardcodedEdgeProbabilities;
		this.hardcodedEdgeProbability = hardcodedEdgeProbability;
		this.numberOfPatterns = numberOfPatterns;
		this.numberOfObjects = numberOfObjects;
		this.minFoldChange = minFoldChange;
		this.maxFoldChange = maxFoldChange;
		this.bgObjects = bgObjects;
		this.globalMinSimilarity = globalMinSimilarity;
		this.patternList = patternList;
		this.maxVariance = maxVariance;
		this.minSimilarity = minSimilarity;
		this.seed = seed;
		this.edgeProbabilityReductionFactor = edgeProbabilityReductionFactor;
		this.samePatternEdgeProbability = samePatternEdgeProbability;
		this.undirectedNetwork = undirectedNetwork;
		this.interClusterEdgeProbabilityMap = new double[numberOfPatterns][numberOfPatterns];

		random = new Random(seed);
		networkRandom = new Random(seed);

		if (!hardcodedEdgeProbabilities) {
			random.nextDouble();
			for (int i = 0; i < interClusterEdgeProbabilityMap.length; i++) {
				for (int j = undirectedNetwork ? i : 0; j < interClusterEdgeProbabilityMap[i].length; j++) {
					double prob;
					if (i == j) {
						prob = samePatternEdgeProbability;
					} else {
						prob = random.nextDouble() * edgeProbabilityReductionFactor;
						if (seed == 42) {
							if (i == 0 && j == 1)
								prob = 0.8;
							else if (i == 0 && j == 2)
								prob = 0.2;
							else if (i == 1 && j == 2)
								prob = 0.05;
						}
						// else if (seed == 43 && i == 0 && j == 2)
						// prob = 0.95;
						// else if (seed == 43 && i == 0 && j == 1)
						// prob = 0.2;
					}
					interClusterEdgeProbabilityMap[i][j] = prob;
					if (undirectedNetwork)
						interClusterEdgeProbabilityMap[j][i] = prob;
					System.out.println("Edge Probability cluster " + i + " <-> cluster " + j + ": " + prob);
				}
			}
			if (seed == 42) {
				clusterToBackgroundEdgeProbabilities = new double[] { 0.2, 0.2, 0.2, 0.7 };
			}
		} else {
			// equal probabilities to all
			for (int i = 0; i < interClusterEdgeProbabilityMap.length; i++) {
				for (int j = 0; j < interClusterEdgeProbabilityMap.length; j++) {
					interClusterEdgeProbabilityMap[i][j] = hardcodedEdgeProbability;
				}
			}
			if (seed == 42) {
				clusterToBackgroundEdgeProbabilities = new double[] { 0.2, 0.2, 0.2, 0.2 };
			}
		}

	}

	private final String dataDirectory;

	private Map<String, Integer> objectsPatternMap;

	public static void main(String[] args) throws Exception {
		double[] variances = new double[] { 0.4, 0.8, 1.2, 1.8 };
		ISimilarityValue[] minSimilarities = new ISimilarityValue[] { TestSimpleSimilarityValue.of(0.9),
				TestSimpleSimilarityValue.of(0.75), TestSimpleSimilarityValue.of(0.6) };

		AbstractDataGenerator gen = new AbstractDataGenerator(
				"data" + File.separator + "generated" + File.separator + "2017_05_11" + File.separator, 360, 5, 2,
				false, false, false, 0.01, 3, 80, -3, 3, 0, TestSimpleSimilarityValue.of(2), null, 0.7,
				TestSimpleSimilarityValue.of(0.7), 42, 1.0, 0.9, true);
		gen.generateQualityDataSet(variances, minSimilarities);
		System.out.println("Global min similarity: " + gen.globalMinSimilarity);
	}

	public static void recomb2017(String[] args) throws Exception {
		double[] variances = new double[] { 0.4, 0.8, 1.2, 1.8 };
		ISimilarityValue[] minSimilarities = new ISimilarityValue[] { TestSimpleSimilarityValue.of(0.9),
				TestSimpleSimilarityValue.of(0.75), TestSimpleSimilarityValue.of(0.6) };

		AbstractDataGenerator gen = new AbstractDataGenerator(
				"data" + File.separator + "generated" + File.separator + "RECOMB17" + File.separator, 360, 5, 2, false,
				false, false, 0.01, 3, 80, -3, 3, 0, TestSimpleSimilarityValue.of(2), null, 0.7,
				TestSimpleSimilarityValue.of(0.7), 42, 1.0, 0.9, true);
		gen.generateQualityDataSet(variances, minSimilarities);
		System.out.println("Global min similarity: " + gen.globalMinSimilarity);
	}

	private void generateQualityDataSet(double[] variances, ISimilarityValue[] minSimilarities) throws Exception {
		for (int i = 0; i < variances.length; i++) {
			double variance = variances[i];
			for (int j = 0; j < minSimilarities.length; j++) {
				ISimilarityValue minSim = minSimilarities[j];
				bgObjects = totalObjects - numberOfObjects * numberOfPatterns;
				System.out.println("Starting.");
				this.minSimilarity = minSim;
				this.maxVariance = variance;

				String options = "-patterns" + numberOfPatterns + "-objects" + numberOfObjects + "-seed" + seed + "-bg"
						+ bgObjects + "-samples" + sampleNumber + "-conf" + identicalSamples + "-tp"
						+ numberOfTimepoints + "-maxVar" + maxVariance + "-minObjSim" + minSimilarity;

				if (hardcodedPatterns) {
					options = "-hardcodedPatterns" + "-objects" + numberOfObjects + "-seed" + seed + "-bg" + bgObjects
							+ "-samples" + sampleNumber
							// + "-identical" + IDENTICAl_SAMPLES
							+ "-tp" + numberOfTimepoints + "-maxVar" + maxVariance + "-minObjSim" + minSimilarity;
				}
				options += hardcodedEdgeProbabilities ? "-hardcodedProbs" : "-randomProbs";
				options += "-randomEdgeProb";// +
												// NOT_IN_CLUSTER_EDGE_PROBABILITY;

				System.out.println("Number of patterns: " + numberOfPatterns);
				System.out.println("Number of objects: " + numberOfObjects);
				System.out.println("Number of time points: " + numberOfTimepoints);
				System.out.println("Number of samples: " + sampleNumber);
				// System.out.println("Identical samples: " +
				// IDENTICAl_SAMPLES);
				System.out.println("Max fold change: " + maxFoldChange);
				System.out.println("Min fold change: " + minFoldChange);
				System.out.println("Variance: " + maxVariance);
				System.out.println("Min similarity: " + minSimilarity);
				System.out.println("Seed: " + seed);
				/*
				 * System.out.println("Network NETWORK_DENSITY: " + NETWORK_DENSITY);
				 * System.out.println("Same pattern edge probability: " +
				 * SAME_PATTERN_EDGE_PROBABILITY);
				 */

				System.out.println("Starting generating easy dataset.");
				System.out.println("#################################");
				generateEasyData(numberOfPatterns, numberOfObjects, dataDirectory + "qual" + options);
				// generateEasyData(NUMBER_OF_PATTERNS, NUMBER_OF_OBJECTS,
				// dataDirectory
				// + "easyDataset" + options);

				/**
				 * CURRENTLY EXITING AFTER CREATING ONE SET
				 */
				if (true) {
					continue;
				}

				System.out.println("Starting generating medium dataset.");
				System.out.println("#################################");
				generateMediumData(numberOfPatterns, numberOfObjects, dataDirectory + "mediumDataset" + options);

				System.out.println("Starting generating difficult dataset.");
				System.out.println("#################################");
				generateDifficultData(numberOfPatterns, numberOfObjects, dataDirectory + "difficultDataset" + options);

				System.out.println("Done.");
			}
		}
	}

	public void generateEasyData(int numberOfPatterns, int numberOfObjectsPerPattern, String filename)
			throws Exception {
		// For generated patterns only
		ISimilarityValue maxPatternSimilarity = TestSimpleSimilarityValue.of(0.7); // Not used for quality
																					// testing
																					// as
		// predefined patterns are used

		// For objects assigned to specific patterns
		ISimilarityValue minObjectSimilarity = minSimilarity;

		generateDatset(numberOfPatterns, numberOfObjectsPerPattern, maxPatternSimilarity, minObjectSimilarity, filename,
				maxVariance);

	}

	public void generateMediumData(int numberOfPatterns, int numberOfObjectsPerPattern, String filename)
			throws Exception {

		generateDatset(numberOfPatterns, numberOfObjectsPerPattern, TestSimpleSimilarityValue.of(0.80),
				TestSimpleSimilarityValue.of(0.7), filename, 0.3);
	}

	public void generateDifficultData(int numberOfPatterns, int numberOfObjectsPerPattern, String filename)
			throws Exception {

		generateDatset(numberOfPatterns, numberOfObjectsPerPattern, TestSimpleSimilarityValue.of(0.9),
				TestSimpleSimilarityValue.of(0.5), filename, 0.6);
	}

	private void generateDatset(int numberOfPatterns, int numberOfObjectsPerPattern,
			ISimilarityValue maxPatternSimilarity, ISimilarityValue simpleSimilarityValue, String filename,
			double maxVariance) throws Exception {
		List<double[]> patterns;
		if (!hardcodedPatterns) {
			System.out.println("Generating " + numberOfPatterns + " clusters");
			patterns = generatePatterns(numberOfPatterns, maxPatternSimilarity);
		} else {
			patterns = hardcodedPatterns();
		}
		objectsPatternMap = new HashMap<>();

		System.out.println(String.format("Generating objects: %d per cluster", numberOfObjectsPerPattern));
		List<List<double[]>> objectSets = generateObjectSetsForPatterns(patterns, simpleSimilarityValue, maxVariance,
				numberOfObjectsPerPattern);
		// List<double[]> objects = generateObjectsForPattern(patterns,
		// minObjectSimilarity, maxVariance, numberOfObjectsPerPattern);

		System.out.println("Adding background data");

		// addBackgroundData(objects);
		addBackgroundDataSets(objectSets, maxVariance);

		System.out.println("Writing objects to file: " + filename + ".txt");
		/*
		 * writeObjectsToFile(filename + ".txt.cy", objects, true);
		 * writeObjectsToFile(filename + ".txt", objects, false);
		 * writeObjectsToFile(filename + ".txt.norm", objects, false, true);
		 */
		writeObjectSetsToFile(filename + ".txt", objectSets);

		System.out.println("Writing patterns to file");
		writePatternsToFile(filename + "-patterns.txt", patterns);
		generateGraphs(patterns, filename);

		System.out.println("Writing object pattern mapping to file");
		writeObjectPatternMappingToFile(filename + "-object-to-pattern.txt", objectsPatternMap);

		// if (0 == 0) {
		// return;
		// }
		filename += "-seed" + seed + "-samePatternEdgeProb" + samePatternEdgeProbability + "-Undirected"
				+ undirectedNetwork + ".txt";
		System.out.println("Generating a network for the objects: " + filename);
		generateNetworkForObjects(filename, objectSets.size());
		// generateNetworkForObjects(filename + "-network" + NETWORK_DENSITY +
		// "-samePatternEdgeProb" + SAME_PATTERN_EDGE_PROBABILITY + ".txt",
		// objects.size());
	}

	private List<double[]> hardcodedPatterns() {
		List<double[]> patterns = new ArrayList<>();
		if (numberOfTimepoints == 5) {
			double[] pattern1 = new double[] { 0, 2, -2, 2, 0 };
			double[] pattern2 = new double[] { 1, 2, -2, 1, 0 };
			double[] pattern3 = new double[] { 0, -2, 2, -2, 0 };
			double[] pattern4 = new double[] { 0, -1, 3, 0, 0.5 };
			double[] pattern5 = new double[] { 0, -3, -3, 3, 3 };
			double[] pattern6 = new double[] { -1, -2, -1.5, 0, 0 };
			patterns.add(pattern1);
			patterns.add(pattern2);
			patterns.add(pattern3);
			patterns.add(pattern4);
			patterns.add(pattern5);
			patterns.add(pattern6);
		} else if (numberOfTimepoints == 10) {
			double[] pattern1 = new double[] { 0, 2, -2, 2, 0, 0, 2, -2, 2, 0 };
			double[] pattern2 = new double[] { 0, -2, 2, -2, 0, 0, -2, 2, -2, 0 };
			double[] pattern3 = new double[] { 0, -3, -3, 3, 3, 0, -3, -3, 3, 3 };
			patterns.add(pattern1);
			patterns.add(pattern2);
			patterns.add(pattern3);
		} else {
			System.out.println("Use 5 or 10 timepoints for hardcoded patterns");
			System.exit(0);
		}
		return patterns;
	}

	private void generateGraphs(List<double[]> patterns, String filename) {

		// JPanel patternGraphPanel = new JPanel(new GridBagLayout());
		// GridBagConstraints constraints = new GridBagConstraints();
		// constraints.anchor = GridBagConstraints.CENTER;
		// constraints.gridx = 0;
		// constraints.gridy = 0;
		// for (int i = 0; i < patterns.size(); i++) {
		// GeneratePatternGraphs generatePatternGraphs = new
		// GeneratePatternGraphs(patterns.get(i), 4);
		// patternGraphPanel.add(generatePatternGraphs.createChartPanel(),
		// constraints);
		// constraints.gridy++;
		// }
		// JFrame f = new JFrame();
		// f.add(patternGraphPanel);
		// f.setVisible(true);
		// f.pack();
		// BufferedImage bi = new BufferedImage(f.getSize().width,
		// f.getSize().height, BufferedImage.TYPE_INT_ARGB);
		// Graphics g = bi.createGraphics();
		// f.paint(g);
		// g.dispose();
		// try{
		// ImageIO.write(bi, "png", new File(filename + ".png"));
		// } catch (Exception e) {
		// } finally {
		// f.dispose();
		// }
	}

	private List<double[]> generatePatterns(int numberOfPatterns, ISimilarityValue maxPatternSimilarity)
			throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] pattern;
		List<double[]> patterns = new ArrayList<>();
		boolean addPattern = true;
		while (patterns.size() < numberOfPatterns) {
			pattern = generatePattern(numberOfTimepoints, minFoldChange, maxFoldChange);
			for (int i = 0; i < patterns.size(); i++) {
				ISimilarityValue similarity = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(pattern),
						ArraysExt.mean(pattern), TimeSeries.of(patterns.get(i)), ArraysExt.mean(patterns.get(i)), null);
				if (similarity.compareTo(maxPatternSimilarity) > 0) {
					addPattern = false;
					break;
				}
			}
			if (addPattern) {
				System.out.println("Cluster " + patterns.size() + ": " + Arrays.toString(pattern));
				patterns.add(pattern);
			}
			addPattern = true;
		}
		return patterns;
	}

	public IClusterObjectMapping generateObjectsForClusterPrototypes(final List<Cluster> cluster,
			ISimilarityValue minObjectSimilarity, double maxVariance, int numberOfObjects) throws Exception {
		List<double[]> patterns = new ArrayList<>();
		for (Cluster c : cluster)
			patterns.add(PrototypeComponentType.TIME_SERIES.getComponent(c.getPrototype()).getTimeSeries().asArray());

		List<List<double[]>> tmp = generateObjectSetsForPatterns(patterns, minObjectSimilarity, maxVariance,
				numberOfObjects);
		IClusterObjectMapping res = new ClusterObjectMapping(cluster.get(0).getClustering().getPrototypeBuilder());
		int oi = 0;
		ICluster currentCluster = null;
		for (List<double[]> ts : tmp) {
			if (oi % numberOfObjects == 0)
				currentCluster = cluster.get(oi / numberOfObjects);
			TimeSeriesObject obj = new TimeSeriesObject("obj" + oi, 2);
			int si = 0;
			for (double[] t : ts) {
				obj.addOriginalTimeSeriesToList(TimeSeries.of(t), "s" + si);
				si++;
			}
			res.addMapping(obj, currentCluster, TestSimpleSimilarityValue.of(1.0));
			oi++;
		}
		return res;
	}

	private List<List<double[]>> generateObjectSetsForPatterns(List<double[]> patterns,
			ISimilarityValue simpleSimilarityValue, double maxVariance, int numberOfObjects) throws Exception {
		List<List<double[]>> objectSets = new ArrayList<>();
		int totalObjects = 0;
		boolean predefinedPattern = true;
		patternList = patterns;
		for (int i = 0; i < patterns.size(); i++) {
			double[] pattern = patterns.get(i);
			for (int j = 0; j < numberOfObjects; j++) {
				objectSets.add(
						generateObjectSetForPattern(pattern, simpleSimilarityValue, maxVariance, predefinedPattern));
				if (objectsPatternMap != null)
					objectsPatternMap.put("obj" + totalObjects, i);
				totalObjects++;
			}
		}
		return objectSets;
	}

	private List<double[]> generateObjectSetForPattern(double[] pattern, ISimilarityValue simpleSimilarityValue,
			double maxVariance, boolean predefinedPattern) throws Exception {
		List<double[]> objectSet = new ArrayList<>();
		ISimilarityValue similarity;
		// double upperVariance = 1 + maxVariance;
		// double lowerVariance = 1 - maxVariance;
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] object = null;
		int maxTries;
		for (int j = 0; j < sampleNumber; j++) {
			maxTries = 100;

			similarity = simpleSimilarityValue.minus(TestSimpleSimilarityValue.of(1));
			while (similarity.lessThan(simpleSimilarityValue)) {
				object = new double[pattern.length];
				// object[0] = random.nextDouble() * (MAX_FOLD_CHANGE -
				// MIN_FOLD_CHANGE) + MIN_FOLD_CHANGE;
				for (int i = 0; i < object.length; i++) {
					// double multiplier = lowerVariance + (random.nextDouble()
					// * (upperVariance - lowerVariance));
					double variance = random.nextDouble() * maxVariance * 2 - maxVariance;
					// double val = pattern[i] * multiplier ;
					double val = pattern[i] + variance;
					object[i] = val;
				}
				similarity = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(object),
						ArraysExt.mean(object), TimeSeries.of(pattern), ArraysExt.mean(pattern), null);
				if (predefinedPattern) {
					for (int i = 0; i < patternList.size(); i++) {
						double[] p = patternList.get(i);
						if (p == pattern) {
							continue;
						}
						ISimilarityValue oSim = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(object),
								ArraysExt.mean(object), TimeSeries.of(p), ArraysExt.mean(p), null);
						if (oSim.greaterThan(similarity)) {
							similarity = TestSimpleSimilarityValue.of(-1);
						}
					}
				}
				maxTries--;
				if (maxTries < 0)
					break;
			}
			if (predefinedPattern && similarity.lessThan(globalMinSimilarity)) {
				globalMinSimilarity = similarity;
			}
			objectSet.add(object);
		}

		return objectSet;
	}

	private List<double[]> generateObjectsForPattern(List<double[]> patterns, ISimilarityValue minObjectSimilarity,
			double maxVariance, double numberOfObjects) throws Exception {
		List<double[]> objects = new ArrayList<double[]>();
		int totalObjects = 0;
		for (int i = 0; i < patterns.size(); i++) {
			int count = 0;
			while (count < numberOfObjects) {
				double[] object = generateObjectForPattern(patterns.get(i), minObjectSimilarity, maxVariance);
				if (object != null) {
					objects.add(object);
					count++;
					totalObjects++;
					objectsPatternMap.put("obj" + totalObjects, i);
				}
			}
		}
		return objects;
	}

	private void writePatternsToFile(String filename, List<double[]> patterns) {
		try {
			PrintWriter printWriter = new PrintWriter(filename);
			for (int i = 0; i < patterns.size(); i++) {
				String line = "";
				for (int j = 0; j < patterns.get(i).length; j++) {
					line += "\t" + patterns.get(i)[j];
				}
				printWriter.println(line);
			}
			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
		}
	}

	private void writeObjectsToFile(String filename, List<double[]> objects, boolean writeObjectId) {
		writeObjectsToFile(filename, objects, writeObjectId, false);
	}

	private void writeObjectSetsToFile(String filename, List<List<double[]>> objectSets) {
		try {
			PrintWriter printWriter = new PrintWriter(filename);
			int id = 0;
			for (int k = 0; k < sampleNumber; k++) {
				for (int i = 0; i < objectSets.size(); i++) {
					double[] object = objectSets.get(i).get(k);
					StringBuilder sid = new StringBuilder("");
					sid.append("id" + id + "\t");
					id++;
					sid.append("sample");
					sid.append(k);
					sid.append("\tobj");
					sid.append(i);
					sid.append("\t");
					for (int j = 0; j < object.length; j++) {
						sid.append(object[j]);
						sid.append("\t");
					}
					sid.deleteCharAt(sid.length() - 1);
					printWriter.println(sid.toString());
				}
			}
			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
		}
	}

	private void writeObjectsToFile(String filename, List<double[]> objects, boolean writeObjectId,
			boolean scaleZeroAndOne) {
		try {
			PrintWriter printWriter = new PrintWriter(filename);
			int id = 0;
			List<Integer> indexList = new ArrayList<>();
			for (int k = 0; k < sampleNumber; k++) {
				if (!identicalSamples) {
					indexList = generateIndexList(objects.size());
				}
				for (int i = 0; i < objects.size(); i++) {
					double[] object = null;
					if (identicalSamples) {
						double[] tmp = objects.get(i);
						object = new double[tmp.length];
						for (int x = 0; x < tmp.length; x++) {
							object[x] = tmp[x];
						}
					} else {
						int index = getObject(indexList);
						double[] tmp = objects.get(index);
						object = new double[tmp.length];
						for (int x = 0; x < tmp.length; x++) {
							object[x] = tmp[x];
						}
					}
					if (scaleZeroAndOne) {
						double max = Double.MIN_VALUE;
						double min = Double.MAX_VALUE;
						for (int x = 0; x < object.length; x++) {
							if (object[x] > max) {
								max = object[x];
							}
							if (object[x] < min) {
								min = object[x];
							}
						}
						for (int x = 0; x < object.length; x++) {
							object[x] = (object[x] - min) / (max - min);
						}
					}

					StringBuilder sid = new StringBuilder("");
					if (writeObjectId) {
						sid.append("id" + id + "\t");
					}
					id++;
					sid.append("sample");
					sid.append(k);
					sid.append("\tobj");
					sid.append(i);
					sid.append("\t");
					for (int j = 0; j < object.length; j++) {
						sid.append(object[j]);
						sid.append("\t");
					}
					sid.deleteCharAt(sid.length() - 1);
					printWriter.println(sid.toString());
				}
			}
			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
		}

	}

	private List<Integer> generateIndexList(int objects) {
		List<Integer> indexList = new ArrayList<>();
		for (int i = 0; i < objects; i++) {
			indexList.add(i);
		}
		return indexList;
	}

	private int getObject(List<Integer> indexList) {
		int index = random.nextInt(indexList.size());
		int objIndex = indexList.get(index);
		indexList.remove(index);
		return objIndex;
	}

	private void writeObjectPatternMappingToFile(String filename, Map<String, Integer> patternObjectMapping) {
		try {
			PrintWriter printWriter = new PrintWriter(filename);
			for (Map.Entry<String, Integer> e : patternObjectMapping.entrySet()) {
				printWriter.println(String.format("%s\t%d", e.getKey(), e.getValue()));
			}
			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
		}

	}

	private double[] generatePattern(int timePoints, double max, double min) {
		double[] pattern = new double[timePoints];
		for (int i = 0; i < timePoints; i++) {
			pattern[i] = min + random.nextDouble() * (max - min);
		}
		return pattern;
	}

	private double[] generateObjectForPattern(double[] pattern, ISimilarityValue minSimilarity, double maxVariance)
			throws Exception {

		ISimilarityValue similarity = minSimilarity.minus(1);
		double upperVariance = 1 + maxVariance;
		double lowerVariance = 1 - maxVariance;
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] object = null;
		int maxTries = 1000;
		while (similarity.lessThan(minSimilarity)) {
			object = new double[pattern.length];
			object[0] = random.nextInt(201);
			for (int i = 1; i < object.length; i++) {
				// double val = Math.pow(2, pattern[i]) * object[i - 1];
				double val = pattern[i] * (lowerVariance + random.nextDouble()) * (upperVariance - lowerVariance);
				// object[i] = (int) (val * (lowerVariance + random.nextDouble()
				// * (upperVariance - lowerVariance)));
				object[i] = val;
			}
			// double[] objPattern = calculatePattern(object);
			// similarity = pearson.calculateDataSimilarity(objPattern,
			// pattern);
			similarity = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(object), ArraysExt.mean(object),
					TimeSeries.of(pattern), ArraysExt.mean(pattern), null);
			maxTries--;
			if (maxTries < 0) {
				break;
			}
		}

		return object;
	}

	private double[] calculatePattern(int[] data) {
		double[] fChange = new double[data.length];
		fChange[0] = 0;
		for (int i = 1; i < data.length; i++) {
			if (data[i - 1] == 0 || data[i] == 0) {
				fChange[i] = 0;
			} else {
				fChange[i] = (Math.log10(data[i] / data[i - 1]) / Math.log10(2));
			}
		}
		return fChange;
	}

	private void addBackgroundData(List<double[]> objects) throws Exception {

		for (int i = 0; i < bgObjects; i++) {
			double[] pattern = generatePattern(numberOfTimepoints, minFoldChange, maxFoldChange);
			objects.add(generateObjectForPattern(pattern, TestSimpleSimilarityValue.of(0.95), 0));
		}

	}

	private void addBackgroundDataSets(List<List<double[]>> objectSets, double maxVariance) throws Exception {
		boolean predefinedPattern = false;
		for (int i = 0; i < bgObjects; i++) {
			double[] pattern = generatePattern(numberOfTimepoints, minFoldChange, maxFoldChange);
			objectSets.add(generateObjectSetForPattern(pattern, TestSimpleSimilarityValue.of(0.9), maxVariance,
					predefinedPattern));
		}
	}

	private void generateNetworkForObjects(String filename, int totalObjects) {
		try {
			Map<String, Integer> nodeDegrees = new HashMap<String, Integer>();
			PrintWriter printWriter = new PrintWriter(filename);

			Set<Pair<Integer, Integer>> edges = new HashSet<Pair<Integer, Integer>>();

			for (int i = 0; i < totalObjects; i++) {
				for (int j = 0; j < totalObjects; j++) {
					String object1 = "obj" + i;
					String object2 = "obj" + j;

					// do not insert the same edge twice
					Pair<Integer, Integer> pair = Pair.of(Math.min(i, j), Math.max(i, j));
					if (edges.contains(pair))
						continue;

					// 50% change to flip source and target to not introduce
					// bias
					if (networkRandom.nextDouble() >= 0.5) {
						String tmp = object1;
						object1 = object2;
						object2 = tmp;
					}

					Integer cluster1 = objectsPatternMap.get(object1);
					Integer cluster2 = objectsPatternMap.get(object2);

					double probability;
					if (hardcodedEdgeProbabilities) {
						probability = hardcodedEdgeProbability;
					} else {
						if (object1.equals(object2))
							probability = 1.0;
						else if (cluster1 == null || cluster2 == null) {
							if (cluster1 == null && cluster2 == null)
								probability = clusterToBackgroundEdgeProbabilities[0];
							else if (cluster1 == null)
								probability = clusterToBackgroundEdgeProbabilities[cluster2 + 1];
							else
								probability = clusterToBackgroundEdgeProbabilities[cluster1 + 1];
						} else
							probability = interClusterEdgeProbabilityMap[cluster1][cluster2];
					}
					if (networkRandom.nextDouble() <= probability) {
						String line = object1 + "\t" + object2;
						printWriter.println(line);

						edges.add(pair);
						if (!nodeDegrees.containsKey(object1))
							nodeDegrees.put(object1, 0);
						if (!nodeDegrees.containsKey(object2))
							nodeDegrees.put(object2, 0);
						nodeDegrees.put(object1, nodeDegrees.get(object1) + 1);
						nodeDegrees.put(object2, nodeDegrees.get(object2) + 1);
					}
				}
			}

			// System.out.println(nodeDegrees);
			//
			// // the last one is for the random objects
			// List<List<Integer>> clusterNodeDegrees = new
			// ArrayList<List<Integer>>();
			// for (int i = 0; i < NUMBER_OF_PATTERNS + 1; i++)
			// clusterNodeDegrees.add(new ArrayList<Integer>());
			// double[] clusterAvgNodeDegree = new double[NUMBER_OF_PATTERNS +
			// 1];
			// for (String objId : nodeDegrees.keySet()) {
			// Integer cluster = objectsPatternMap.get(objId);
			// int d = nodeDegrees.get(objId);
			// if (cluster != null) {
			// clusterAvgNodeDegree[cluster] += d;
			// clusterNodeDegrees.get(cluster).add(d);
			// } else {
			// clusterAvgNodeDegree[NUMBER_OF_PATTERNS] += d;
			// clusterNodeDegrees.get(NUMBER_OF_PATTERNS).add(d);
			// }
			// }
			// for (int i = 0; i < clusterAvgNodeDegree.length - 1; i++)
			// clusterAvgNodeDegree[i] /= NUMBER_OF_OBJECTS;
			// clusterAvgNodeDegree[NUMBER_OF_PATTERNS] /= (NUMBER_OF_OBJECTS *
			// NUMBER_OF_PATTERNS / 2);
			//
			// System.out.println(Arrays.toString(clusterAvgNodeDegree));
			//
			// // the last one is for the random objects
			// List<double[]> clusterNodeDegreeHists = new
			// ArrayList<double[]>();
			// for (List<Integer> degrees : clusterNodeDegrees) {
			// int sum = 0;
			// int maxDegree = Collections.max(degrees);
			// double[] degreeHist = new double[maxDegree + 1];
			// for (int d : degrees) {
			// degreeHist[d]++;
			// sum++;
			// }
			// for (int i = 0; i < degreeHist.length; i++)
			// degreeHist[i] /= sum;
			// clusterNodeDegreeHists.add(degreeHist);
			// }
			//
			// System.out.println("Node degree histograms per cluster: ");
			// for (double[] degreeHist : clusterNodeDegreeHists)
			// System.out.println(Arrays.toString(degreeHist));

			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}