package dk.sdu.imada.ticone.generate;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.TestSimpleSimilarityValue;

/**
 * Class to generate test data.
 *
 * @author Christian Nørskov
 */
public class TimeSeriesTestClass {

	// Specifies the number of samples of each object
	private static int totalObjects = 360;
	private static final int NUMBER_OF_TIMEPOINTS = 5;
	private static final int SAMPLE_NUMBER = 2;
	private static final boolean IDENTICAl_SAMPLES = false;
	private static final boolean HARDCODED_PATTERNS = false;
	private static boolean HARDCODED_EDGE_PROBABILITIES = false;
	private static double HARDCODED_EDGE_PROBABILITY = 0.01;
	private static final int NUMBER_OF_PATTERNS = 3;
	private static final int NUMBER_OF_OBJECTS = 80;
	private static final double MIN_FOLD_CHANGE = -3;
	private static final double MAX_FOLD_CHANGE = 3;
	private static int bgObjects;

	private static ISimilarityValue GLOBAL_MIN_SIMILARITY = TestSimpleSimilarityValue.of(2);

	private static List<double[]> patternList;

	// For quality testing
	private static double MAX_VARIANCE = 0.7;
	private static ISimilarityValue MIN_SIMILARITY = TestSimpleSimilarityValue.of(0.7);

	private static final int seed = 42;
	private static Random random = new Random(seed);
	private static Random networkRandom = new Random(seed);

	// Network density
	// private static final double NETWORK_DENSITY = 0.01;
	private static final double EDGE_PROBABILITY_REDUCTION_FACTOR = 1.0;// 0.02;
	private static final double SAME_PATTERN_EDGE_PROBABILITY = 0.9 * EDGE_PROBABILITY_REDUCTION_FACTOR;
	// private static final double NOT_IN_CLUSTER_EDGE_PROBABILITY = 0.2 *
	// EDGE_PROBABILITY_REDUCTION_FACTOR;
	private static final boolean UNDIRECTED_NETWORK = true;
	private static final double[][] interClusterEdgeProbabilityMap = new double[NUMBER_OF_PATTERNS][NUMBER_OF_PATTERNS];
	// first index is background to itself
	private static final double[] clusterToBackgroundEdgeProbabilities;

	static {
		if (!HARDCODED_EDGE_PROBABILITIES) {
			random.nextDouble();
			for (int i = 0; i < interClusterEdgeProbabilityMap.length; i++) {
				for (int j = UNDIRECTED_NETWORK ? i : 0; j < interClusterEdgeProbabilityMap[i].length; j++) {
					double prob;
					if (i == j) {
						prob = SAME_PATTERN_EDGE_PROBABILITY;
					} else {
						prob = random.nextDouble() * EDGE_PROBABILITY_REDUCTION_FACTOR;
						if (seed == 42) {
							if (i == 0 && j == 1)
								prob = 0.8;
							else if (i == 0 && j == 2)
								prob = 0.2;
							else if (i == 1 && j == 2)
								prob = 0.05;
						}
						// else if (seed == 43 && i == 0 && j == 2)
						// prob = 0.95;
						// else if (seed == 43 && i == 0 && j == 1)
						// prob = 0.2;
					}
					interClusterEdgeProbabilityMap[i][j] = prob;
					if (UNDIRECTED_NETWORK)
						interClusterEdgeProbabilityMap[j][i] = prob;
					System.out.println("Edge Probability ICluster " + i + " <-> ICluster " + j + ": " + prob);
				}
			}
			if (seed == 42) {
				clusterToBackgroundEdgeProbabilities = new double[] { 0.2, 0.2, 0.2, 0.7 };
			}
		} else {
			// equal probabilities to all
			for (int i = 0; i < interClusterEdgeProbabilityMap.length; i++) {
				for (int j = 0; j < interClusterEdgeProbabilityMap.length; j++) {
					interClusterEdgeProbabilityMap[i][j] = HARDCODED_EDGE_PROBABILITY;
				}
			}
			if (seed == 42) {
				clusterToBackgroundEdgeProbabilities = new double[] { 0.2, 0.2, 0.2, 0.2 };
			}
		}
		random = new Random(seed);
	}

	private static final String dataDirectory = "data" + File.separator + "generated" + File.separator + "RECOMB17"
			+ File.separator;

	private static Map<String, Integer> objectsPatternMap;

	public static void main(String[] args) throws Exception {
		// testAbsoluteValues();
		// System.exit(0);
		double[] variances = new double[] { 0.4, 0.8, 1.2, 1.8 };
		ISimilarityValue[] minSimilarities = new ISimilarityValue[] { TestSimpleSimilarityValue.of(0.9),
				TestSimpleSimilarityValue.of(0.75), TestSimpleSimilarityValue.of(0.6) };
		for (int i = 0; i < variances.length; i++) {
			double variance = variances[i];
			for (int j = 0; j < minSimilarities.length; j++) {
				ISimilarityValue minSim = minSimilarities[j];
				GLOBAL_MIN_SIMILARITY = TestSimpleSimilarityValue.of(2);
				generateQualityDataSet(variance, minSim);
				System.out.println("Global min similarity: " + GLOBAL_MIN_SIMILARITY);
			}
		}
	}

	private static void generateQualityDataSet(double maxVariance, ISimilarityValue minSimilarity) throws Exception {
		bgObjects = totalObjects - NUMBER_OF_OBJECTS * NUMBER_OF_PATTERNS;
		System.out.println("Starting.");
		MIN_SIMILARITY = minSimilarity;
		MAX_VARIANCE = maxVariance;

		String options = "-patterns" + NUMBER_OF_PATTERNS + "-objects" + NUMBER_OF_OBJECTS + "-seed" + seed + "-bg"
				+ bgObjects + "-samples" + SAMPLE_NUMBER + "-conf" + IDENTICAl_SAMPLES + "-tp" + NUMBER_OF_TIMEPOINTS
				+ "-maxVar" + maxVariance + "-minObjSim" + minSimilarity;

		if (HARDCODED_PATTERNS) {
			options = "-hardcodedPatterns" + "-objects" + NUMBER_OF_OBJECTS + "-seed" + seed + "-bg" + bgObjects
					+ "-samples" + SAMPLE_NUMBER
					// + "-identical" + IDENTICAl_SAMPLES
					+ "-tp" + NUMBER_OF_TIMEPOINTS + "-maxVar" + maxVariance + "-minObjSim" + minSimilarity;
		}
		options += HARDCODED_EDGE_PROBABILITIES ? "-hardcodedProbs" : "-randomProbs";
		options += "-randomEdgeProb";// + NOT_IN_CLUSTER_EDGE_PROBABILITY;

		System.out.println("Number of patterns: " + NUMBER_OF_PATTERNS);
		System.out.println("Number of objects: " + NUMBER_OF_OBJECTS);
		System.out.println("Number of time points: " + NUMBER_OF_TIMEPOINTS);
		System.out.println("Number of samples: " + SAMPLE_NUMBER);
		// System.out.println("Identical samples: " + IDENTICAl_SAMPLES);
		System.out.println("Max fold change: " + MAX_FOLD_CHANGE);
		System.out.println("Min fold change: " + MIN_FOLD_CHANGE);
		System.out.println("Variance: " + maxVariance);
		System.out.println("Min similarity: " + minSimilarity);
		System.out.println("Seed: " + seed);
		/*
		 * System.out.println("Network NETWORK_DENSITY: " + NETWORK_DENSITY);
		 * System.out.println("Same pattern edge probability: " +
		 * SAME_PATTERN_EDGE_PROBABILITY);
		 */

		System.out.println("Starting generating easy dataset.");
		System.out.println("#################################");
		generateEasyData(NUMBER_OF_PATTERNS, NUMBER_OF_OBJECTS, dataDirectory + "qual" + options);
		// generateEasyData(NUMBER_OF_PATTERNS, NUMBER_OF_OBJECTS, dataDirectory
		// + "easyDataset" + options);

		/**
		 * CURRENTLY EXITING AFTER CREATING ONE SET
		 */
		if (true) {
			return;
		}

		System.out.println("Starting generating medium dataset.");
		System.out.println("#################################");
		generateMediumData(NUMBER_OF_PATTERNS, NUMBER_OF_OBJECTS, dataDirectory + "mediumDataset" + options);

		System.out.println("Starting generating difficult dataset.");
		System.out.println("#################################");
		generateDifficultData(NUMBER_OF_PATTERNS, NUMBER_OF_OBJECTS, dataDirectory + "difficultDataset" + options);

		System.out.println("Done.");

	}

	public static void generateEasyData(int numberOfPatterns, int numberOfObjectsPerPattern, String filename)
			throws Exception {
		// For generated patterns only
		ISimilarityValue maxPatternSimilarity = TestSimpleSimilarityValue.of(0.7); // Not used for quality
																					// testing as
		// predefined patterns are used

		// For objects assigned to specific patterns
		ISimilarityValue minObjectSimilarity = MIN_SIMILARITY;

		// How much variance is allowed from actual pattern
		double maxVariance = MAX_VARIANCE;

		generateDatset(numberOfPatterns, numberOfObjectsPerPattern, maxPatternSimilarity, minObjectSimilarity, filename,
				maxVariance);

	}

	public static void generateMediumData(int numberOfPatterns, int numberOfObjectsPerPattern, String filename)
			throws Exception {

		generateDatset(numberOfPatterns, numberOfObjectsPerPattern, TestSimpleSimilarityValue.of(0.80),
				TestSimpleSimilarityValue.of(0.7), filename, 0.3);
	}

	public static void generateDifficultData(int numberOfPatterns, int numberOfObjectsPerPattern, String filename)
			throws Exception {

		generateDatset(numberOfPatterns, numberOfObjectsPerPattern, TestSimpleSimilarityValue.of(0.9),
				TestSimpleSimilarityValue.of(0.5), filename, 0.6);
	}

	private static void generateDatset(int numberOfPatterns, int numberOfObjectsPerPattern,
			ISimilarityValue maxPatternSimilarity, ISimilarityValue simpleSimilarityValue, String filename,
			double maxVariance) throws Exception {
		List<double[]> patterns;
		if (!HARDCODED_PATTERNS) {
			System.out.println("Generating " + numberOfPatterns + " clusters");
			patterns = generatePatterns(numberOfPatterns, maxPatternSimilarity);
		} else {
			patterns = hardcodedPatterns();
		}
		objectsPatternMap = new HashMap<>();

		System.out.println(String.format("Generating objects: %d per cluster", numberOfObjectsPerPattern));
		List<List<double[]>> objectSets = generateObjectSetsForPatterns(patterns, simpleSimilarityValue, maxVariance,
				numberOfObjectsPerPattern);
		// List<double[]> objects = generateObjectsForPattern(patterns,
		// minObjectSimilarity, maxVariance, numberOfObjectsPerPattern);

		System.out.println("Adding background data");

		// addBackgroundData(objects);
		addBackgroundDataSets(objectSets, maxVariance);

		System.out.println("Writing objects to file: " + filename + ".txt");
		/*
		 * writeObjectsToFile(filename + ".txt.cy", objects, true);
		 * writeObjectsToFile(filename + ".txt", objects, false);
		 * writeObjectsToFile(filename + ".txt.norm", objects, false, true);
		 */
		writeObjectSetsToFile(filename + ".txt", objectSets);

		System.out.println("Writing patterns to file");
		writePatternsToFile(filename + "-patterns.txt", patterns);
		generateGraphs(patterns, filename);

		System.out.println("Writing object pattern mapping to file");
		writeObjectPatternMappingToFile(filename + "-object-to-pattern.txt", objectsPatternMap);

		// if (0 == 0) {
		// return;
		// }
		filename += "-seed" + seed + "-samePatternEdgeProb" + SAME_PATTERN_EDGE_PROBABILITY + "-Undirected"
				+ UNDIRECTED_NETWORK + ".txt";
		System.out.println("Generating a network for the objects: " + filename);
		generateNetworkForObjects(filename, objectSets.size());
		// generateNetworkForObjects(filename + "-network" + NETWORK_DENSITY +
		// "-samePatternEdgeProb" + SAME_PATTERN_EDGE_PROBABILITY + ".txt",
		// objects.size());
	}

	private static List<double[]> hardcodedPatterns() {
		List<double[]> patterns = new ArrayList<>();
		if (NUMBER_OF_TIMEPOINTS == 5) {
			double[] pattern1 = new double[] { 0, 2, -2, 2, 0 };
			double[] pattern2 = new double[] { 1, 2, -2, 1, 0 };
			double[] pattern3 = new double[] { 0, -2, 2, -2, 0 };
			double[] pattern4 = new double[] { 0, -1, 3, 0, 0.5 };
			double[] pattern5 = new double[] { 0, -3, -3, 3, 3 };
			double[] pattern6 = new double[] { -1, -2, -1.5, 0, 0 };
			patterns.add(pattern1);
			patterns.add(pattern2);
			patterns.add(pattern3);
			patterns.add(pattern4);
			patterns.add(pattern5);
			patterns.add(pattern6);
		} else if (NUMBER_OF_TIMEPOINTS == 10) {
			double[] pattern1 = new double[] { 0, 2, -2, 2, 0, 0, 2, -2, 2, 0 };
			double[] pattern2 = new double[] { 0, -2, 2, -2, 0, 0, -2, 2, -2, 0 };
			double[] pattern3 = new double[] { 0, -3, -3, 3, 3, 0, -3, -3, 3, 3 };
			patterns.add(pattern1);
			patterns.add(pattern2);
			patterns.add(pattern3);
		} else {
			System.out.println("Use 5 or 10 timepoints for hardcoded patterns");
			System.exit(0);
		}
		return patterns;
	}

	private static void generateGraphs(List<double[]> patterns, String filename) {

		// JPanel patternGraphPanel = new JPanel(new GridBagLayout());
		// GridBagConstraints constraints = new GridBagConstraints();
		// constraints.anchor = GridBagConstraints.CENTER;
		// constraints.gridx = 0;
		// constraints.gridy = 0;
		// for (int i = 0; i < patterns.size(); i++) {
		// GeneratePatternGraphs generatePatternGraphs = new
		// GeneratePatternGraphs(patterns.get(i), 4);
		// patternGraphPanel.add(generatePatternGraphs.createChartPanel(),
		// constraints);
		// constraints.gridy++;
		// }
		// JFrame f = new JFrame();
		// f.add(patternGraphPanel);
		// f.setVisible(true);
		// f.pack();
		// BufferedImage bi = new BufferedImage(f.getSize().width,
		// f.getSize().height, BufferedImage.TYPE_INT_ARGB);
		// Graphics g = bi.createGraphics();
		// f.paint(g);
		// g.dispose();
		// try{
		// ImageIO.write(bi, "png", new File(filename + ".png"));
		// } catch (Exception e) {
		// } finally {
		// f.dispose();
		// }
	}

	private static List<double[]> generatePatterns(int numberOfPatterns, ISimilarityValue maxPatternSimilarity)
			throws Exception {
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] pattern;
		List<double[]> patterns = new ArrayList<double[]>();
		boolean addPattern = true;
		while (patterns.size() < numberOfPatterns) {
			pattern = generatePattern(NUMBER_OF_TIMEPOINTS, MIN_FOLD_CHANGE, MAX_FOLD_CHANGE);
			for (int i = 0; i < patterns.size(); i++) {
				ISimilarityValue similarity;
				similarity = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(pattern),
						ArraysExt.mean(pattern), TimeSeries.of(patterns.get(i)), ArraysExt.mean(patterns.get(i)), null);
				if (similarity.greaterThan(maxPatternSimilarity)) {
					addPattern = false;
					break;
				}
			}
			if (addPattern) {
				System.out.println("Cluster " + patterns.size() + ": " + Arrays.toString(pattern));
				patterns.add(pattern);
			}
			addPattern = true;
		}
		return patterns;
	}

	public static ClusterObjectMapping generateObjectsForClusterPrototypes(final ClusterObjectMapping clustering,
			ISimilarityValue minObjectSimilarity, double maxVariance, int numberOfObjects) throws Exception {
		final ClusterObjectMapping res = new ClusterObjectMapping(clustering.getPrototypeBuilder());
		for (ICluster c : clustering.getClusters())
			res.getClusterBuilder().copy(c);
		final IClusterList cluster = res.getClusters().asList();

		List<double[]> patterns = new ArrayList<>();
		for (ICluster c : cluster)
			patterns.add(PrototypeComponentType.TIME_SERIES.getComponent(c.getPrototype()).getTimeSeries().asArray());

		List<List<double[]>> tmp = generateObjectSetsForPatterns(patterns, minObjectSimilarity, maxVariance,
				numberOfObjects);
		int oi = 0;
		ICluster currentCluster = null;
		for (List<double[]> ts : tmp) {
			if (oi % numberOfObjects == 0)
				currentCluster = cluster.get(oi / numberOfObjects);
			TimeSeriesObject obj = new TimeSeriesObject("obj" + oi, 2);
			int si = 0;
			for (double[] t : ts) {
				obj.addOriginalTimeSeriesToList(TimeSeries.of(t), "s" + si);
				si++;
			}
			res.addMapping(obj, currentCluster, TestSimpleSimilarityValue.of(1.0));
			oi++;
		}
		return res;
	}

	private static List<List<double[]>> generateObjectSetsForPatterns(List<double[]> patterns,
			ISimilarityValue simpleSimilarityValue, double maxVariance, int numberOfObjects) throws Exception {
		List<List<double[]>> objectSets = new ArrayList<>();
		int totalObjects = 0;
		boolean predefinedPattern = true;
		patternList = patterns;
		for (int i = 0; i < patterns.size(); i++) {
			double[] pattern = patterns.get(i);
			for (int j = 0; j < numberOfObjects; j++) {
				objectSets.add(
						generateObjectSetForPattern(pattern, simpleSimilarityValue, maxVariance, predefinedPattern));
				if (objectsPatternMap != null)
					objectsPatternMap.put("obj" + totalObjects, i);
				totalObjects++;
			}
		}
		return objectSets;
	}

	private static List<double[]> generateObjectSetForPattern(double[] pattern, ISimilarityValue simpleSimilarityValue,
			double maxVariance, boolean predefinedPattern) throws Exception {
		List<double[]> objectSet = new ArrayList<>();
		ISimilarityValue similarity;
		// double upperVariance = 1 + maxVariance;
		// double lowerVariance = 1 - maxVariance;
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] object = null;
		int maxTries;
		for (int j = 0; j < SAMPLE_NUMBER; j++) {
			maxTries = 100;

			similarity = simpleSimilarityValue.minus(1);
			while (similarity.lessThan(simpleSimilarityValue)) {
				object = new double[pattern.length];
				// object[0] = random.nextDouble() * (MAX_FOLD_CHANGE -
				// MIN_FOLD_CHANGE) + MIN_FOLD_CHANGE;
				for (int i = 0; i < object.length; i++) {
					// double multiplier = lowerVariance + (random.nextDouble()
					// * (upperVariance - lowerVariance));
					double variance = random.nextDouble() * maxVariance * 2 - maxVariance;
					// double val = pattern[i] * multiplier ;
					double val = pattern[i] + variance;
					object[i] = val;
				}
				similarity = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(object),
						ArraysExt.mean(object), TimeSeries.of(pattern), ArraysExt.mean(pattern), null);
				if (predefinedPattern) {
					for (int i = 0; i < patternList.size(); i++) {
						double[] p = patternList.get(i);
						if (p == pattern) {
							continue;
						}
						ISimilarityValue oSim = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(object),
								ArraysExt.mean(object), TimeSeries.of(p), ArraysExt.mean(p), null);
						if (oSim.greaterThan(similarity)) {
							similarity = TestSimpleSimilarityValue.of(-1);
						}
					}
				}
				maxTries--;
				if (maxTries < 0) {
					break;
				}
			}
			if (predefinedPattern && similarity.lessThan(GLOBAL_MIN_SIMILARITY)) {
				GLOBAL_MIN_SIMILARITY = similarity;
			}
			objectSet.add(object);
		}

		return objectSet;
	}

	private static List<double[]> generateObjectsForPattern(List<double[]> patterns,
			ISimilarityValue minObjectSimilarity, double maxVariance, double numberOfObjects) throws Exception {
		List<double[]> objects = new ArrayList<double[]>();
		int totalObjects = 0;
		for (int i = 0; i < patterns.size(); i++) {
			int count = 0;
			while (count < numberOfObjects) {
				double[] object = generateObjectForPattern(patterns.get(i), minObjectSimilarity, maxVariance);
				if (object != null) {
					objects.add(object);
					count++;
					totalObjects++;
					objectsPatternMap.put("obj" + totalObjects, i);
				}
			}
		}
		return objects;
	}

	private static void writePatternsToFile(String filename, List<double[]> patterns) {
		try {
			PrintWriter printWriter = new PrintWriter(filename);
			for (int i = 0; i < patterns.size(); i++) {
				String line = "";
				for (int j = 0; j < patterns.get(i).length; j++) {
					line += "\t" + patterns.get(i)[j];
				}
				printWriter.println(line);
			}
			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
		}
	}

	private static void writeObjectsToFile(String filename, List<double[]> objects, boolean writeObjectId) {
		TimeSeriesTestClass.writeObjectsToFile(filename, objects, writeObjectId, false);
	}

	private static void writeObjectSetsToFile(String filename, List<List<double[]>> objectSets) {
		try {
			PrintWriter printWriter = new PrintWriter(filename);
			int id = 0;
			for (int k = 0; k < SAMPLE_NUMBER; k++) {
				for (int i = 0; i < objectSets.size(); i++) {
					double[] object = objectSets.get(i).get(k);
					StringBuilder sid = new StringBuilder("");
					sid.append("id" + id + "\t");
					id++;
					sid.append("sample");
					sid.append(k);
					sid.append("\tobj");
					sid.append(i);
					sid.append("\t");
					for (int j = 0; j < object.length; j++) {
						sid.append(object[j]);
						sid.append("\t");
					}
					sid.deleteCharAt(sid.length() - 1);
					printWriter.println(sid.toString());
				}
			}
			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
		}
	}

	private static void writeObjectsToFile(String filename, List<double[]> objects, boolean writeObjectId,
			boolean scaleZeroAndOne) {
		try {
			PrintWriter printWriter = new PrintWriter(filename);
			int id = 0;
			List<Integer> indexList = new ArrayList<>();
			for (int k = 0; k < SAMPLE_NUMBER; k++) {
				if (!IDENTICAl_SAMPLES) {
					indexList = generateIndexList(objects.size());
				}
				for (int i = 0; i < objects.size(); i++) {
					double[] object = null;
					if (IDENTICAl_SAMPLES) {
						double[] tmp = objects.get(i);
						object = new double[tmp.length];
						for (int x = 0; x < tmp.length; x++) {
							object[x] = tmp[x];
						}
					} else {
						int index = getObject(indexList);
						double[] tmp = objects.get(index);
						object = new double[tmp.length];
						for (int x = 0; x < tmp.length; x++) {
							object[x] = tmp[x];
						}
					}
					if (scaleZeroAndOne) {
						double max = Double.MIN_VALUE;
						double min = Double.MAX_VALUE;
						for (int x = 0; x < object.length; x++) {
							if (object[x] > max) {
								max = object[x];
							}
							if (object[x] < min) {
								min = object[x];
							}
						}
						for (int x = 0; x < object.length; x++) {
							object[x] = (object[x] - min) / (max - min);
						}
					}

					StringBuilder sid = new StringBuilder("");
					if (writeObjectId) {
						sid.append("id" + id + "\t");
					}
					id++;
					sid.append("sample");
					sid.append(k);
					sid.append("\tobj");
					sid.append(i);
					sid.append("\t");
					for (int j = 0; j < object.length; j++) {
						sid.append(object[j]);
						sid.append("\t");
					}
					sid.deleteCharAt(sid.length() - 1);
					printWriter.println(sid.toString());
				}
			}
			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
		}

	}

	private static List<Integer> generateIndexList(int objects) {
		List<Integer> indexList = new ArrayList<>();
		for (int i = 0; i < objects; i++) {
			indexList.add(i);
		}
		return indexList;
	}

	private static int getObject(List<Integer> indexList) {
		int index = random.nextInt(indexList.size());
		int objIndex = indexList.get(index);
		indexList.remove(index);
		return objIndex;
	}

	private static void writeObjectPatternMappingToFile(String filename, Map<String, Integer> patternObjectMapping) {
		try {
			PrintWriter printWriter = new PrintWriter(filename);
			for (Map.Entry<String, Integer> e : patternObjectMapping.entrySet()) {
				printWriter.println(String.format("%s\t%d", e.getKey(), e.getValue()));
			}
			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
		}

	}

	private static double[] generatePattern(int timePoints, double max, double min) {
		double[] pattern = new double[timePoints];
		for (int i = 0; i < timePoints; i++) {
			pattern[i] = min + random.nextDouble() * (max - min);
		}
		return pattern;
	}

	private static double[] generateObjectForPattern(double[] pattern, ISimilarityValue minSimilarity,
			double maxVariance) throws Exception {

		ISimilarityValue similarity = minSimilarity.minus(1);
		double upperVariance = 1 + maxVariance;
		double lowerVariance = 1 - maxVariance;
		PearsonCorrelationFunction pearson = new PearsonCorrelationFunction();
		double[] object = null;
		int maxTries = 1000;
		while (similarity.lessThan(minSimilarity)) {
			object = new double[pattern.length];
			object[0] = random.nextInt(201);
			for (int i = 1; i < object.length; i++) {
				// double val = Math.pow(2, pattern[i]) * object[i - 1];
				double val = pattern[i] * (lowerVariance + random.nextDouble()) * (upperVariance - lowerVariance);
				// object[i] = (int) (val * (lowerVariance + random.nextDouble()
				// * (upperVariance - lowerVariance)));
				object[i] = val;
			}
			// double[] objPattern = calculatePattern(object);
			// similarity = pearson.calculateDataSimilarity(objPattern,
			// pattern);
			similarity = pearson.calculateUnweightedTimeSeriesSimilarity(TimeSeries.of(object), ArraysExt.mean(object),
					TimeSeries.of(pattern), ArraysExt.mean(pattern), null);
			maxTries--;
			if (maxTries < 0) {
				break;
			}
		}

		return object;
	}

	private static double[] calculatePattern(int[] data) {
		double[] fChange = new double[data.length];
		fChange[0] = 0;
		for (int i = 1; i < data.length; i++) {
			if (data[i - 1] == 0 || data[i] == 0) {
				fChange[i] = 0;
			} else {
				fChange[i] = (Math.log10(data[i] / data[i - 1]) / Math.log10(2));
			}
		}
		return fChange;
	}

	private static void addBackgroundData(List<double[]> objects) throws Exception {

		for (int i = 0; i < bgObjects; i++) {
			double[] pattern = generatePattern(NUMBER_OF_TIMEPOINTS, MIN_FOLD_CHANGE, MAX_FOLD_CHANGE);
			objects.add(generateObjectForPattern(pattern, TestSimpleSimilarityValue.of(0.95), 0));
			objects.add(generateObjectForPattern(pattern, TestSimpleSimilarityValue.of(0.95), 0));
		}

	}

	private static void addBackgroundDataSets(List<List<double[]>> objectSets, double maxVariance) throws Exception {
		boolean predefinedPattern = false;
		for (int i = 0; i < bgObjects; i++) {
			double[] pattern = generatePattern(NUMBER_OF_TIMEPOINTS, MIN_FOLD_CHANGE, MAX_FOLD_CHANGE);
			objectSets.add(generateObjectSetForPattern(pattern, TestSimpleSimilarityValue.of(0.9), maxVariance,
					predefinedPattern));
		}
	}

	private static void generateNetworkForObjects(String filename, int totalObjects) {
		try {
			Map<String, Integer> nodeDegrees = new HashMap<String, Integer>();
			PrintWriter printWriter = new PrintWriter(filename);

			Set<Pair<Integer, Integer>> edges = new HashSet<Pair<Integer, Integer>>();

			for (int i = 0; i < totalObjects; i++) {
				for (int j = 0; j < totalObjects; j++) {
					String object1 = "obj" + i;
					String object2 = "obj" + j;

					// do not insert the same edge twice
					Pair<Integer, Integer> pair = Pair.of(Math.min(i, j), Math.max(i, j));
					if (edges.contains(pair))
						continue;

					// 50% change to flip source and target to not introduce
					// bias
					if (networkRandom.nextDouble() >= 0.5) {
						String tmp = object1;
						object1 = object2;
						object2 = tmp;
					}

					Integer cluster1 = objectsPatternMap.get(object1);
					Integer cluster2 = objectsPatternMap.get(object2);

					double probability;
					if (HARDCODED_EDGE_PROBABILITIES) {
						probability = HARDCODED_EDGE_PROBABILITY;
					} else {
						if (object1.equals(object2))
							probability = 1.0;
						else if (cluster1 == null || cluster2 == null) {
							if (cluster1 == null && cluster2 == null)
								probability = clusterToBackgroundEdgeProbabilities[0];
							else if (cluster1 == null)
								probability = clusterToBackgroundEdgeProbabilities[cluster2 + 1];
							else
								probability = clusterToBackgroundEdgeProbabilities[cluster1 + 1];
						} else
							probability = interClusterEdgeProbabilityMap[cluster1][cluster2];
					}
					if (networkRandom.nextDouble() <= probability) {
						String line = object1 + "\t" + object2;
						printWriter.println(line);

						edges.add(pair);
						if (!nodeDegrees.containsKey(object1))
							nodeDegrees.put(object1, 0);
						if (!nodeDegrees.containsKey(object2))
							nodeDegrees.put(object2, 0);
						nodeDegrees.put(object1, nodeDegrees.get(object1) + 1);
						nodeDegrees.put(object2, nodeDegrees.get(object2) + 1);
					}
				}
			}

			// System.out.println(nodeDegrees);
			//
			// // the last one is for the random objects
			// List<List<Integer>> clusterNodeDegrees = new
			// ArrayList<List<Integer>>();
			// for (int i = 0; i < NUMBER_OF_PATTERNS + 1; i++)
			// clusterNodeDegrees.add(new ArrayList<Integer>());
			// double[] clusterAvgNodeDegree = new double[NUMBER_OF_PATTERNS +
			// 1];
			// for (String objId : nodeDegrees.keySet()) {
			// Integer cluster = objectsPatternMap.get(objId);
			// int d = nodeDegrees.get(objId);
			// if (cluster != null) {
			// clusterAvgNodeDegree[cluster] += d;
			// clusterNodeDegrees.get(cluster).add(d);
			// } else {
			// clusterAvgNodeDegree[NUMBER_OF_PATTERNS] += d;
			// clusterNodeDegrees.get(NUMBER_OF_PATTERNS).add(d);
			// }
			// }
			// for (int i = 0; i < clusterAvgNodeDegree.length - 1; i++)
			// clusterAvgNodeDegree[i] /= NUMBER_OF_OBJECTS;
			// clusterAvgNodeDegree[NUMBER_OF_PATTERNS] /= (NUMBER_OF_OBJECTS *
			// NUMBER_OF_PATTERNS / 2);
			//
			// System.out.println(Arrays.toString(clusterAvgNodeDegree));
			//
			// // the last one is for the random objects
			// List<double[]> clusterNodeDegreeHists = new
			// ArrayList<double[]>();
			// for (List<Integer> degrees : clusterNodeDegrees) {
			// int sum = 0;
			// int maxDegree = Collections.max(degrees);
			// double[] degreeHist = new double[maxDegree + 1];
			// for (int d : degrees) {
			// degreeHist[d]++;
			// sum++;
			// }
			// for (int i = 0; i < degreeHist.length; i++)
			// degreeHist[i] /= sum;
			// clusterNodeDegreeHists.add(degreeHist);
			// }
			//
			// System.out.println("Node degree histograms per cluster: ");
			// for (double[] degreeHist : clusterNodeDegreeHists)
			// System.out.println(Arrays.toString(degreeHist));

			printWriter.flush();
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testNormalityOfNodeDegrees() throws Exception {
		networkRandom = new Random(14);
		HARDCODED_EDGE_PROBABILITIES = true;
		HARDCODED_EDGE_PROBABILITY = 1.0;
		totalObjects = 20;
		objectsPatternMap = new HashMap<String, Integer>();
		for (int i = 0; i < totalObjects; i++) {
			objectsPatternMap.put("obj" + i, (i + 1) / 10);
		}
		generateNetworkForObjects("network_test_node_degrees_total_graph.txt", totalObjects);

		TiconeNetworkImpl network = TiconeNetworkImpl.parseFromFile("test", false,
				new File("network_test_node_degrees_total_graph.txt"));
		network.initNodeDegreesUndirected();

		for (int i : network.getNodeDegreesUndirected())
			assertEquals(totalObjects, i);
	}

}