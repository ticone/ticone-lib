/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.MeanTimeSeriesFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 14, 2019
 *
 */
public class VarianceTimeSeriesFeature extends AbstractFeature<Double> implements INumericFeature<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4024456395129052913L;

	VarianceTimeSeriesFeature(VarianceTimeSeriesFeature other) {
		super(other);
	}

	public VarianceTimeSeriesFeature() {
		super(ObjectType.TIME_SERIES, Double.class);
	}

	@Override
	public String getName() {
		return "Time-series Standard Variance";
	}

	@Override
	public IFeature<Double> copy() {
		return new VarianceTimeSeriesFeature(this);
	}

	@Override
	protected IFeatureValue<Double> doCalculate(IObjectWithFeatures object)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
		final ITimeSeries timeSeries = ObjectType.TIME_SERIES.getBaseType().cast(object);

		try {
			final MeanTimeSeriesFeature meanF = new MeanTimeSeriesFeature();
			final double mean = object.hasFeatureValueSet(meanF)
					? object.getFeatureValue(meanF).getValue().doubleValue()
					: meanF.calculate(timeSeries).getValue().doubleValue();

			final double[] asArray = timeSeries.asArray();
			final int n = asArray.length;
			double s = 0;
			for (int i = 0; i < n; i++) {
				s += Math.pow(asArray[i] - mean, 2);
			}
			return value(object, s / n);
		} catch (FeatureValueNotSetException | IncompatibleFeatureAndObjectException e) {
			throw new FeatureCalculationException(e);
		}
	}

}
