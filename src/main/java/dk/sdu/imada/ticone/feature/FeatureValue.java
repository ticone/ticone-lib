/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Objects;

import dk.sdu.imada.ticone.similarity.IMissingSimilarityValue;
import dk.sdu.imada.ticone.util.IConvertibleToNumber;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

public class FeatureValue<R> implements IFeatureValue<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -436894524451203510L;

	protected final Class<R> valueType;
	protected final Class<? extends IFeature<R>> featureType;
	protected IFeature<R> feature;
	protected IObjectWithFeatures object;
	protected final R value;

	/**
	 * 
	 */
	protected FeatureValue(final Class<? extends IFeature<R>> featureType, final Class<R> valueType, final R value) {
		this.valueType = valueType;
		this.featureType = featureType;
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FeatureValue)
			return Objects.equals(valueType, ((FeatureValue) obj).valueType)
					&& Objects.equals(featureType, ((FeatureValue) obj).featureType)
					&& Objects.equals(value, ((FeatureValue) obj).value);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(valueType, featureType, feature, value);
	}

	@Override
	public Class<R> getValueType() {
		return this.valueType;
	}

	/**
	 * @return the featureType
	 */
	@Override
	public Class<? extends IFeature<R>> getFeatureType() {
		return this.featureType;
	}

	@Override
	public IFeature<R> getFeature() {
		return this.feature;
	}

	/**
	 * @param feature the feature to set
	 */
	@Override
	public void setFeature(IFeature<R> feature) {
		if (!(this.getFeatureType().isAssignableFrom(feature.getClass())))
			throw new IllegalArgumentException("This feature value does not belong to the passed feature type");
		this.feature = feature;
	}

	@Override
	public IObjectWithFeatures getObject() {
		return this.object;
	}

	/**
	 * @param object the object to set
	 */
	@Override
	public void setObject(IObjectWithFeatures object) {
		this.object = object;
	}

	@Override
	public R getValue() {
		return this.value;
	}

	@Override
	public boolean isArithmetic() {
		return IArithmeticFeature.class.isAssignableFrom(featureType);
	}

	@Override
	public Number toNumber() throws NotAnArithmeticFeatureValueException, ToNumberConversionException {
		if (!isArithmetic())
			throw new NotAnArithmeticFeatureValueException();
		if (value instanceof Number)
			return ((Number) value);
		else if (value instanceof IConvertibleToNumber)
			return ((IConvertibleToNumber) value).toNumber();
		throw new NotAnArithmeticFeatureValueException();
	}

	@Override
	public boolean isNaN() {
		return (this.value instanceof Double && ((Double) this.value).isNaN())
				|| (this.value instanceof Float && ((Float) this.value).isNaN())
				|| (this.value instanceof IMissingSimilarityValue);
	}

	@Override
	public String toString() {
		return Objects.toString(value);
	}
}