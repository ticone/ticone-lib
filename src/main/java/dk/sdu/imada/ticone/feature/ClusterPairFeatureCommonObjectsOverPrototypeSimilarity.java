/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Collection;
import java.util.stream.Collectors;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.comparison.IClusterPairFeatureCommonObjectsOverPrototypeSimilarity;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

public class ClusterPairFeatureCommonObjectsOverPrototypeSimilarity extends AbstractFeature<Double>
		implements IClusterPairFeatureCommonObjectsOverPrototypeSimilarity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1304025268069617159L;

	protected ISimilarityFunction similarityFunction;

	public ClusterPairFeatureCommonObjectsOverPrototypeSimilarity() {
		super(ObjectType.CLUSTER_PAIR, Double.class);
	}

	public ClusterPairFeatureCommonObjectsOverPrototypeSimilarity(final ISimilarityFunction sim) {
		this();
		this.similarityFunction = sim;
	}

	@Override
	public boolean equals(final Object obj) {
		return (obj instanceof ClusterPairFeatureCommonObjectsOverPrototypeSimilarity);
	};

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public IFeature<Double> copy() {
		final ClusterPairFeatureCommonObjectsOverPrototypeSimilarity copy = new ClusterPairFeatureCommonObjectsOverPrototypeSimilarity();
		copy.similarityFunction = this.similarityFunction;
		return copy;
	}

	/**
	 * @param similarityFunction the similarityFunction to set
	 */
	public void setSimilarityFunction(final ISimilarityFunction similarityFunction) {
		this.similarityFunction = similarityFunction;
	}

	/**
	 * @return the similarityFunction
	 */
	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	@Override
	public String getName() {
		if (this.similarityFunction != null)
			return String.format("Number of Common Objects / Prototype Similarity (%s)",
					this.similarityFunction.toString());
		return "Number of Common Objects / Prototype Similarity";
	}

	@Override
	public boolean validateInitialized() throws IncorrectlyInitializedException {
		if (this.similarityFunction == null)
			throw new IncorrectlyInitializedException("similarityFunction");
		return true;
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures object)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
		final IClusterPair pair = (IClusterPair) object;
		final Collection<String> ids1 = pair.getFirst().getObjects().stream().map(o -> o.getName())
				.collect(Collectors.toSet());
		final Collection<String> ids2 = pair.getSecond().getObjects().stream().map(o -> o.getName())
				.collect(Collectors.toSet());

		ids1.retainAll(ids2);

		final int numberCommonObjects = ids1.size();

		double protoSim;
		try {
			protoSim = this.similarityFunction.calculateSimilarity(pair).calculate().get();
		} catch (final SimilarityCalculationException | IncompatibleObjectTypeException e) {
			throw new FeatureCalculationException(e);
		}
		final double pseudo = 0.000000001;
		return value(pair, numberCommonObjects / (protoSim + pseudo));
	}
}