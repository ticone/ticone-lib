/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Objects;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 11, 2018
 *
 */
public abstract class SimilarityFeature extends AbstractFeature<ISimilarityValue> implements ISimilarityFeature {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5047182428157754785L;
	/**
	 * 
	 */
	protected ISimilarityFunction similarityFunction;

	/**
	 * 
	 */
	public SimilarityFeature(final ObjectType<?> objectType) {
		super(objectType, ISimilarityValue.class);
	}

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	public SimilarityFeature(final ObjectType<? extends IObjectWithFeatures> objectType,
			final ISimilarityFunction simFunc) throws IncompatibleSimilarityFunctionException {
		super(objectType, ISimilarityValue.class);
		this.validateSimilarityFunction(simFunc);
		this.similarityFunction = simFunc;
	}

	protected SimilarityFeature(final SimilarityFeature other) {
		super(other);
		this.similarityFunction = other.similarityFunction.copy();
	}

	protected final void validateSimilarityFunction(final ISimilarityFunction similarityFunction)
			throws IncompatibleSimilarityFunctionException {
		if (!isValidSimilarityFunction(similarityFunction))
			throw new IncompatibleSimilarityFunctionException();
	}

	protected abstract boolean isValidSimilarityFunction(final ISimilarityFunction similarityFunction);

	/**
	 * @return the similarityFunction
	 */
	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	@Override
	public IFeatureValueProvider getFeatureValueProvider() {
		return this.similarityFunction;
	}

	@Override
	public void setFeatureValueProvider(IFeatureValueProvider provider)
			throws IncompatibleFeatureValueProviderException {
		if (!(provider.featuresProvidedValuesFor(this.objectType).contains(this))
				|| !(provider instanceof ISimilarityFunction))
			throw new IncompatibleFeatureValueProviderException(provider, this);
		this.similarityFunction = (ISimilarityFunction) provider;
	}

	/**
	 * @param similarityFunction the similarityFunction to set
	 * @throws IncompatibleSimilarityFunctionException
	 */
	@Override
	public void setSimilarityFunction(ISimilarityFunction similarityFunction)
			throws IncompatibleSimilarityFunctionException {
		this.validateSimilarityFunction(similarityFunction);
		this.similarityFunction = similarityFunction;
	}

	@Override
	public boolean validateInitialized() throws IncorrectlyInitializedException {
		if (this.similarityFunction == null)
			throw new IncorrectlyInitializedException("similarityFunction");
		return super.validateInitialized();
	}

	@Override
	public String getName() {
		return String.format("Similarity feature (%s)", this.similarityFunction);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof SimilarityFeature)
			return this.getSimilarityFunction().equals(((SimilarityFeature) obj).getSimilarityFunction());
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getClass(), this.similarityFunction);
	}

	@Override
	protected IFeatureValue<ISimilarityValue> doValue(ISimilarityValue rawValue) {
		return new SimilarityFeatureValue((Class<? extends ISimilarityFeature>) this.getClass(), rawValue);
	}

	@Override
	protected final IFeatureValue<ISimilarityValue> doCalculate(final IObjectWithFeatures object)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
		try {
			return this.similarityFunction.getFeatureValue(this, object);
		} catch (IncompatibleFeatureValueProviderException | UnknownObjectFeatureValueProviderException
				| IncompatibleFeatureAndObjectException e) {
			throw new FeatureCalculationException(e);
		}
	}

}