/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 4, 2019
 *
 */
public class ClusteringFeatureTotalObjectClusterSimilarity extends SimilarityFeature
		implements IClusteringFeature<ISimilarityValue> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6415508014583643573L;

	public ClusteringFeatureTotalObjectClusterSimilarity() {
		super(ObjectType.CLUSTERING);
	}

	public ClusteringFeatureTotalObjectClusterSimilarity(ISimilarityFunction simFunc)
			throws IncompatibleSimilarityFunctionException {
		super(ObjectType.CLUSTERING, simFunc);
	}

	ClusteringFeatureTotalObjectClusterSimilarity(ClusteringFeatureTotalObjectClusterSimilarity other) {
		super(other);
	}

	@Override
	public String getName() {
		return "Average object-cluster similarity";
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	public IFeature<ISimilarityValue> copy() {
		return new ClusteringFeatureTotalObjectClusterSimilarity(this);
	}
}
