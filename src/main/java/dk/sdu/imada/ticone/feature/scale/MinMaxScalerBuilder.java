/**
 * 
 */
package dk.sdu.imada.ticone.feature.scale;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.FeatureComparisonException;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.statistics.FeatureValueSampleException;
import dk.sdu.imada.ticone.statistics.IFeatureValueSample;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.IConvertibleToNumber;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ScalerInitializationException;
import dk.sdu.imada.ticone.util.ScalerNotInitializedException;
import dk.sdu.imada.ticone.util.ScalingException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
public class MinMaxScalerBuilder<V extends Comparable<V>, O extends IObjectWithFeatures, F extends IArithmeticFeature<V>>
		extends AbstractScalerBuilder<V, O, F> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6729876513522385336L;
	protected transient Double min, max;
	protected boolean mapNanToZero;

	/**
	 * 
	 */
	public MinMaxScalerBuilder() {
	}

	public MinMaxScalerBuilder(final Double min, final Double max) {
		super();
		this.min = min;
		this.max = max;
		this.mapNanToZero = false;
	}

	public MinMaxScalerBuilder(final IConvertibleToNumber<Double> min, final IConvertibleToNumber<Double> max)
			throws ToNumberConversionException {
		this(min.toNumber(), max.toNumber());
	}

	public MinMaxScalerBuilder(final F featureToScale, final IFeatureValueSample<V> featureValueSample) {
		super(featureToScale, featureValueSample);
	}

	public MinMaxScalerBuilder(final F featureToScale, final IObjectProvider provider, final int sampleSize,
			final long seed) {
		super(featureToScale, provider, sampleSize, seed);
	}

	public MinMaxScalerBuilder(final AbstractScalerBuilder<V, O, F> scaler) {
		super(scaler);
	}

	@Override
	public MinMaxScalerBuilder<V, O, F> copy() {
		return new MinMaxScalerBuilder<>(this);
	}

	/**
	 * @param nanMapping the nanMapping to set
	 */
	public MinMaxScalerBuilder<V, O, F> setMapNanToZero(final boolean mapNanToZero) {
		this.mapNanToZero = mapNanToZero;
		return this;
	}

	/**
	 * @param min the min to set
	 */
	public void setMin(final Double min) {
		this.min = min;
	}

	public void setMin(final IConvertibleToNumber<Double> min) throws ToNumberConversionException {
		this.setMin(min.toNumber());
	}

	/**
	 * @param max the max to set
	 */
	public void setMax(final Double max) {
		this.max = max;
	}

	public void setMax(final IConvertibleToNumber<Double> max) throws ToNumberConversionException {
		this.setMax(max.toNumber());
	}

	/**
	 * @return the min
	 */
	public Double getMin() {
		return this.min;
	}

	/**
	 * @return the max
	 */
	public Double getMax() {
		return this.max;
	}

	/**
	 * @return the mapNanToZero
	 */
	public boolean isMapNanToZero() {
		return this.mapNanToZero;
	}

	@Override
	public MinMaxScalerBuilder<V, O, F> setObjectProvider(final IObjectProvider forObject) {
		super.setObjectProvider(forObject);
		return this;
	}

	@Override
	public MinMaxScaler doBuild() throws CreateInstanceFactoryException, InterruptedException {
		return (MinMaxScaler) super.doBuild();
	}

	@Override
	protected void reset() {
		super.reset();
		max = null;
		min = null;
		mapNanToZero = false;
	}

	@Override
	protected void ensureFieldsForInitialization() throws FeatureValueSampleException, ScalerNotInitializedException,
			ScalerInitializationException, InterruptedException {
		if (this.min != null && this.max != null) {
			return;
		}
		super.ensureFieldsForInitialization();
	}

	@Override
	public MinMaxScaler doCreateInstance() throws CreateInstanceFactoryException {
		try {
			return new MinMaxScaler(this.min, this.max, this.mapNanToZero);
		} catch (ToNumberConversionException e) {
			throw new CreateInstanceFactoryException(e);
		}
	}

	@Override
	protected void initialize() throws SimilarityCalculationException, FeatureCalculationException,
			IncorrectlyInitializedException, IncompatibleObjectProviderException, ScalerInitializationException {
		try {
			final List<? extends IFeatureValue<V>> values = this.excludeInvalidSampleValuesForInitialization();
			if (values.size() < 2)
				throw new ScalerInitializationException(
						"MinMaxScaler requires at least two numerical (non-NaN) values for initialization");
			if (this.min == null)
				this.min = Collections.min(values, new Comparator<IFeatureValue<V>>() {

					@Override
					public int compare(IFeatureValue<V> o1, IFeatureValue<V> o2) {
						try {
							return Double.compare(o1.toNumber().doubleValue(), o2.toNumber().doubleValue());
						} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException e) {
							throw new FeatureComparisonException(e);
						}
					}

				}).toNumber().doubleValue();

			if (this.max == null)
				this.max = Collections.max(values, new Comparator<IFeatureValue<V>>() {

					@Override
					public int compare(IFeatureValue<V> o1, IFeatureValue<V> o2) {
						try {
							return Double.compare(o1.toNumber().doubleValue(), o2.toNumber().doubleValue());
						} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException e) {
							throw new FeatureComparisonException(e);
						}
					}

				}).toNumber().doubleValue();
		} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException e) {
			throw new ScalerInitializationException(e);
		}
	}

	@Override
	protected boolean isInitialized() {
		return this.min != null && this.max != null;
	}

	@Override
	protected void ensureValidFieldsForCreateInstance() throws IncorrectlyInitializedException {
		if (this.min.equals(this.max))
			throw new IncorrectlyInitializedException(
					"MinMaxScaler cannot be initialized with identical min and max values.");
	}

	@Override
	public String toString() {
		return String.format("Min Max Scaler (min=%.3f, max=%.3f)", this.min, this.max);
	}
}

class MinMaxScaler extends AbstractScaler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6519208500012192389L;
	protected final double min, max, range, nanMapping, offset;

	MinMaxScaler(final double min, final double max, final boolean mapNanToZero) throws ToNumberConversionException {
		super();
		this.min = min;
		this.max = max;
		this.range = max - min;
		if (mapNanToZero) {
			this.nanMapping = 0.0;
			this.offset = nanMapping + 0.5;
		} else {
			this.nanMapping = Double.NaN;
			this.offset = 0.0;
		}
	}

	MinMaxScaler(final MinMaxScaler scaler) {
		super(scaler);
		this.min = scaler.min;
		this.max = scaler.max;
		this.range = scaler.range;
		this.nanMapping = scaler.nanMapping;
		this.offset = scaler.offset;
	}

	@Override
	public boolean equals(final Object obj) {
		return super.equals(obj) && (obj instanceof MinMaxScaler) && this.min == ((MinMaxScaler) obj).min
				&& this.max == ((MinMaxScaler) obj).max && this.nanMapping == ((MinMaxScaler) obj).nanMapping
				&& this.offset == ((MinMaxScaler) obj).offset;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), this.min, this.max, this.nanMapping, this.offset);
	}

	/**
	 * @return the max
	 */
	public double getMax() {
		return this.max;
	}

	/**
	 * @return the min
	 */
	public double getMin() {
		return this.min;
	}

	/**
	 * @return the nanValue
	 */
	public double getNanMapping() {
		return this.nanMapping;
	}

	/**
	 * @return the offset
	 */
	public double getOffset() {
		return this.offset;
	}

	@Override
	public MinMaxScaler copy() {
		return new MinMaxScaler(this);
	}

	@Override
	public double scale(final Number value) throws ScalingException {
		final double doubleValue = value.doubleValue();
		if (Double.isNaN(doubleValue))
			return this.nanMapping;
//		else if (doubleValue > this.max)
//			return 1.0;
//		else if (doubleValue < this.min)
//			return this.offset;
//		if (doubleValue > this.max)
//			System.out.println("+" + (doubleValue - this.max));
//		if (doubleValue < this.min)
//			System.out.println((doubleValue - this.min));
		return this.offset + (1 - this.offset) * ((doubleValue - this.min) / (this.range));
	}

	@Override
	public String toString() {
		return String.format("%s: %.3f %.3f", this.getClass().getSimpleName(), this.min, this.max);
	}
}
