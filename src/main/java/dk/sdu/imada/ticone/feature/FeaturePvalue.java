/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.statistics.IPvalue;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public class FeaturePvalue extends AbstractFeature<IPvalue> implements IFeaturePvalue {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5371829071554514025L;
	protected IFeatureValueProvider provider;

	/**
	 * @param other
	 */
	public FeaturePvalue(final ObjectType<? extends IObjectWithFeatures> objectType) {
		super(objectType, IPvalue.class);
	}

	@Override
	public FeaturePvalue copy() {
		final FeaturePvalue copy = new FeaturePvalue(this.objectType);
		copy.provider = this.provider;
		return copy;
	}

	@Override
	public IFeatureValue<IPvalue> doCalculate(final IObjectWithFeatures cluster)
			throws IncorrectlyInitializedException, FeatureCalculationException, InterruptedException {
		this.validateInitialized();
		try {
			return this.provider.getFeatureValue(this, cluster);
		} catch (final UnknownObjectFeatureValueProviderException | IncompatibleFeatureValueProviderException
				| IncompatibleFeatureAndObjectException e) {
			throw new FeatureCalculationException(e);
		}
	}

	@Override
	public String getName() {
		return "P-value";
	}

	@Override
	public IFeatureValueProvider getFeatureValueProvider() {
		return this.provider;
	}

	@Override
	public void setFeatureValueProvider(final IFeatureValueProvider provider) {
		this.provider = provider;
	}

	@Override
	protected IFeatureValue<IPvalue> doValue(IPvalue rawValue) {
		return new FeaturePvalueValue(rawValue);
	}
}
