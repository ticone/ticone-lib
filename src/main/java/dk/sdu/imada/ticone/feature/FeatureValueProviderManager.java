/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Set;

import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.util.FeatureValueProviderChangeEvent;
import dk.sdu.imada.ticone.util.IFeatureValueProviderListener;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public class FeatureValueProviderManager implements IFeatureValueProviderManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -861950518353838352L;

	protected IFeatureValueProvider provider;

	protected Set<IFeatureValueProviderListener> fvProviderListener;

	/**
	 * 
	 */
	public FeatureValueProviderManager(final IFeatureValueProvider provider) {
		super();
		this.provider = provider;
	}

	/**
	 * @return the provider
	 */
	@Override
	public IFeatureValueProvider getFeatureValueProvider() {
		return this.provider;
	}

	@Override
	public void addFeatureValueProviderListener(final IFeatureValueProviderListener listener) {
		this.fvProviderListener.add(listener);
	}

	@Override
	public void fireFeatureValueProviderChanged() {
		final FeatureValueProviderChangeEvent e = new FeatureValueProviderChangeEvent(this.provider);
		for (final IFeatureValueProviderListener l : this.fvProviderListener)
			l.featureValueProviderChanged(e);
	}

	@Override
	public void removeFeatureValueProviderListener(final IFeatureValueProviderListener listener) {
		this.fvProviderListener.remove(listener);
	}

}
