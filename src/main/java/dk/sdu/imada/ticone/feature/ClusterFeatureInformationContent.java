/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Arrays;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 11, 2017
 *
 */
public class ClusterFeatureInformationContent extends AbstractFeature<Double>
		implements IClusterFeature<Double>, INumericFeature<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8482182121330645421L;
	protected int numberBins, numberTimepoints;
	protected double[][] bins;
	protected double[] max;
	protected double[] min;

	/**
	 * 
	 */
	public ClusterFeatureInformationContent() {
		super(ObjectType.CLUSTER, Double.class);
		this.numberBins = 100;
	}

	/**
	 * 
	 */
	public ClusterFeatureInformationContent(final int numberBins) {
		this();
		this.numberBins = numberBins;
	}

	@Override
	public ClusterFeatureInformationContent copy() {
		final ClusterFeatureInformationContent copy = new ClusterFeatureInformationContent();
		copy.numberBins = this.numberBins;
		return copy;
	}

	/**
	 * @param numberBins the numberBins to set
	 */
	public void setNumberBins(final int numberBins) {
		this.numberBins = numberBins;
	}

	/**
	 * @return the numberBins
	 */
	public int getNumberBins() {
		return this.numberBins;
	}

	protected void initBins(final double[] timeSeries, final ITimeSeriesObjects allObjects) {
		this.numberTimepoints = timeSeries.length;

		this.min = new double[this.numberTimepoints];
		this.max = new double[this.numberTimepoints];
		this.bins = new double[this.numberTimepoints][this.numberBins];
		for (int i = 0; i < this.numberTimepoints; i++) {
			this.min[i] = Double.MAX_VALUE;
			this.max[i] = -Double.MAX_VALUE;
		}

		logger.info("Recalculate density function");
		// new clustering
		// -> recalculate bins
		for (final ITimeSeriesObject o : allObjects) {
			final ITimeSeries[] samples = o.getPreprocessedTimeSeriesList();
			for (int s = 0; s < samples.length; s++) {
				for (int tp = 0; tp < this.numberTimepoints; tp++) {
					try {
						this.min[tp] = Math.min(this.min[tp], samples[s].get(tp));
						this.max[tp] = Math.max(this.max[tp], samples[s].get(tp));
					} catch (final ArrayIndexOutOfBoundsException e) {
						e.printStackTrace();
					}
				}
			}
		}

		final long[] totalCount = new long[this.numberTimepoints];

		// count bins
		for (final ITimeSeriesObject o : allObjects) {
			final ITimeSeries[] samples = o.getPreprocessedTimeSeriesList();
			for (int s = 0; s < samples.length; s++) {
				for (int tp = 0; tp < this.numberTimepoints; tp++) {
					final double v = samples[s].get(tp);
					final int bin = this.valueToBin(tp, v);
					this.bins[tp][bin]++;
					totalCount[tp]++;
				}
			}
		}

		for (int tp = 0; tp < this.numberTimepoints; tp++) {
			for (int b = 0; b < this.bins[tp].length; b++) {
				this.bins[tp][b] /= totalCount[tp];
				// avoid 0 probs
				this.bins[tp][b] += Double.MIN_VALUE;
			}
		}
		logger.debug(Arrays.toString(this.bins));
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures object)
			throws IncorrectlyInitializedException, FeatureCalculationException {
		final ICluster cluster = ObjectType.CLUSTER.getBaseType().cast(object);
		try {
			final ITimeSeriesObjects objects = cluster.getObjects();
			final double[] timeSeries = PrototypeComponentType.TIME_SERIES.getComponent(cluster.getPrototype())
					.getTimeSeries().asArray();
			this.initBins(timeSeries, objects);
			this.validateInitialized();
			int count = 0;
			double res = 0.0;
			for (final ITimeSeriesObject obj : cluster.getObjects()) {
				for (int s = 0; s < obj.getPreprocessedTimeSeriesList().length; s++) {
					for (int tp = 0; tp < timeSeries.length; tp++) {
						final double p = this.bins[tp][this.valueToBin(tp,
								obj.getPreprocessedTimeSeriesList()[s].get(tp))];
						res += -Math.log(p) / Math.log(2);
					}
					count++;
				}
			}
			res /= count;

			return value(cluster, res);
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			throw new FeatureCalculationException(e);
		}
	}

	@Override
	public String getName() {
		return "Information Content";
	}

	private int valueToBin(final int timepoint, final double value) {
		final int res = (int) ((value - this.min[timepoint]) / (this.max[timepoint] - this.min[timepoint])
				* (this.getNumberBins() - 1));
		return res;
	}
}
