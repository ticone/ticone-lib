/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Collection;
import java.util.stream.Collectors;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPairFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * @author Christian Wiwie
 * 
 * @since May 1, 2017
 *
 */
public class ClusterPairFeatureNumberCommonObjects extends AbstractFeature<Integer>
		implements IClusterPairFeature<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3582803525417225162L;

	/**
	 * 
	 */
	public ClusterPairFeatureNumberCommonObjects() {
		super(ObjectType.CLUSTER_PAIR, Integer.class);
	}

	@Override
	public ClusterPairFeatureNumberCommonObjects copy() {
		return new ClusterPairFeatureNumberCommonObjects();
	}

	@Override
	public String getName() {
		return "Number of Common Objects";
	}

	@Override
	public IFeatureValue<Integer> doCalculate(final IObjectWithFeatures object) throws FeatureCalculationException {
		final IClusterPair pair = (IClusterPair) object;
		final Collection<String> ids1 = pair.getFirst().getObjects().stream().map(o -> o.getName())
				.collect(Collectors.toSet());
		final Collection<String> ids2 = pair.getSecond().getObjects().stream().map(o -> o.getName())
				.collect(Collectors.toSet());

		ids1.retainAll(ids2);

		final int numberCommonObjects = ids1.size();

		return value(pair, numberCommonObjects);
	}
}