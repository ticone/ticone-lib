/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Collection;
import java.util.stream.Collectors;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPairFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Dec 14, 2018
 *
 */
public class ClusterPairFeaturePercentageCommonObjects extends AbstractFeature<Double>
		implements IClusterPairFeature<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1276450629996917712L;

	/**
	 * 
	 */
	public ClusterPairFeaturePercentageCommonObjects() {
		super(ObjectType.CLUSTER_PAIR, Double.class);
	}

	@Override
	public ClusterPairFeaturePercentageCommonObjects copy() {
		return new ClusterPairFeaturePercentageCommonObjects();
	}

	@Override
	public String getName() {
		return "Percentage of Common Objects";
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures object) throws FeatureCalculationException {
		final IClusterPair pair = (IClusterPair) object;
		final Collection<String> ids1 = pair.getFirst().getObjects().stream().map(o -> o.getName())
				.collect(Collectors.toSet());
		final Collection<String> ids2 = pair.getSecond().getObjects().stream().map(o -> o.getName())
				.collect(Collectors.toSet());

		ids1.retainAll(ids2);

		final int numberCommonObjects = ids1.size();

		return value(pair, (double) numberCommonObjects * 100
				/ (Math.min(pair.getFirst().getObjects().size(), pair.getSecond().getObjects().size())));
	}
}