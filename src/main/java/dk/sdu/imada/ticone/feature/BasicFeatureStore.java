/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.stream.Collectors;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.FeatureStoreChangedEvent;
import dk.sdu.imada.ticone.feature.store.FeatureValuesStoredEvent;
import dk.sdu.imada.ticone.feature.store.FeaturesAddedEvent;
import dk.sdu.imada.ticone.feature.store.FeaturesRemovedEvent;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.feature.store.IFeatureStoreChangedListener;
import dk.sdu.imada.ticone.feature.store.IFeatureStoreFeatureAddedListener;
import dk.sdu.imada.ticone.feature.store.IFeatureStoreFeatureRemovedListener;
import dk.sdu.imada.ticone.feature.store.IFeatureValueStoredListener;
import dk.sdu.imada.ticone.util.Pair;
import it.unimi.dsi.fastutil.ints.Int2LongMap;
import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import it.unimi.dsi.fastutil.objects.ObjectBigList;
import it.unimi.dsi.fastutil.objects.ObjectList;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 26, 2017
 *
 */
public class BasicFeatureStore implements IFeatureStore {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8124786089553500978L;

	public static final int DEFAULT_EXPECTED_NUMBER_FEATURES = 10;
	public static final int DEFAULT_EXPECTED_NUMBER_OBJECTS = 1000;

	private final ReadWriteLock rwLock = new ReentrantReadWriteLock();

	private final List<IFeature<?>> features;
	private final Object2IntMap<IFeature<?>> featureToIndex;

	protected final ObjectList<ObjectBigList<IFeatureValue<?>>> featureToValues;
	protected final Map<IObjectWithFeatures, Int2LongMap> objectToValueIndex;

	private transient Set<IFeatureStoreFeatureAddedListener> featureAddedListener;
	private transient Set<IFeatureStoreFeatureRemovedListener> featureRemovedListener;
	private transient Set<IFeatureStoreChangedListener> listener;
	private transient Set<IFeatureValueStoredListener> featureValueStoredListener;

	private final int expectedNumberFeatures;
	private final int expectedNumberObjects;

	public BasicFeatureStore() {
		this(DEFAULT_EXPECTED_NUMBER_FEATURES, DEFAULT_EXPECTED_NUMBER_OBJECTS);
	}

	public BasicFeatureStore(final int expectedNumberFeatures, final int expectedNumberObjects) {
		super();
		this.expectedNumberFeatures = expectedNumberFeatures;
		this.expectedNumberObjects = expectedNumberObjects;

		this.features = new ArrayList<>(expectedNumberFeatures);
		this.featureToIndex = new Object2IntOpenHashMap<>(expectedNumberFeatures * 2);
		this.featureToIndex.defaultReturnValue(-1);
		this.featureToValues = new ObjectArrayList<>(this.expectedNumberFeatures);
		this.objectToValueIndex = new HashMap<>(this.expectedNumberObjects * 2);

		this.featureAddedListener = new HashSet<>();
		this.featureValueStoredListener = new HashSet<>();
		this.listener = new HashSet<>();
	}

	public BasicFeatureStore(final BasicFeatureStore store) {
		this(store, null);
	}

	public BasicFeatureStore(final BasicFeatureStore store,
			final Function<IObjectWithFeatures, IObjectWithFeatures> replaceObjects) {
		this(store.expectedNumberFeatures, store.expectedNumberObjects);

		store.featureToValues.forEach(v -> this.featureToValues.add(new ObjectBigArrayBigList<>(v)));
		store.objectToValueIndex.forEach((k, v) -> this.objectToValueIndex
				.put(replaceObjects != null ? replaceObjects.apply(k) : k, new Int2LongOpenHashMap(v)));

		this.featureAddedListener.addAll(store.featureAddedListener);

//		if (store.featureValueStoredListener != null)
//			store.featureValueStoredListener.forEach((k, v) -> {
//				final Map<IObjectWithFeatures, Set<IFeatureValueStoredListener>> newMap = new HashMap<>();
//				final Map<IObjectWithFeatures, Set<IFeatureValueStoredListener>> m = store.featureValueStoredListenerPerFeature
//						.get(k);
//				m.forEach((k1, v1) -> {
//					newMap.put(k1, new HashSet<>(v1));
//				});
//				this.featureValueStoredListener.put(k, newMap);
//			});
//		this.featureValueStoredListener.putAll(store.featureValueStoredListener);
		this.featureValueStoredListener.addAll(store.featureValueStoredListener);
	}

	@Override
	public IFeatureStore copy() {

		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			return new BasicFeatureStore(this);
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public boolean contains(final IObjectWithFeatures object) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			return objectToValueIndex.containsKey(object);
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public boolean contains(IFeature<?> feature) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			return featureToIndex.containsKey(feature);
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public Set<IObjectWithFeatures> keySet() {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			return objectToValueIndex.keySet();
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public <O extends IObjectWithFeatures> Set<O> keySet(ObjectType<O> keyType) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			return (Set<O>) objectToValueIndex.keySet().stream().filter(o -> o.getObjectType() == keyType)
					.collect(Collectors.toCollection(() -> new HashSet<>()));
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public Collection<IObjectWithFeatures> keySet(final IFeature<?> feature) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			final int featureIdx = this.featureToIndex.getInt(feature);
			final ObjectBigList<IObjectWithFeatures> result = new ObjectBigArrayBigList<>();
			objectToValueIndex.keySet().stream().filter(o -> objectToValueIndex.get(o).containsKey(featureIdx))
					.forEach(o -> {
						final long idx = objectToValueIndex.get(o).get(featureIdx);
						if (idx >= result.size64())
							result.size(idx + 1);
						result.set(idx, o);
					});
			return result;
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public <O extends IObjectWithFeatures> Set<IFeature<?>> getFeaturesFor(final O object) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			if (!objectToValueIndex.containsKey(object))
				return Collections.emptySet();
			return objectToValueIndex.get(object).keySet().stream().map(i -> this.features.get(i))
					.collect(Collectors.toCollection(HashSet::new));
		} finally {
			readLock.unlock();
		}
	}

	private void createAndFireFeatureAddedEvent(final Collection<IFeature<?>> features) {
		final FeaturesAddedEvent e = new FeaturesAddedEvent(this);
		e.setAddedFeatures(features);
		this.fireFeatureAdded(e);
	}

	@Override
	public <O extends IObjectWithFeatures, V> void setFeatureValue(final O object, final IFeature<V> feature,
			final IFeatureValue<V> value) {
		this.setFeatureValues(Arrays.asList(Pair.getPair(object, feature)), Arrays.asList(value));
	}

	@Override
	public <O extends IObjectWithFeatures, V> void setFeatureValues(List<Pair<O, IFeature<V>>> objectFeaturePairs,
			List<IFeatureValue<V>> values) {
		Objects.requireNonNull(objectFeaturePairs);
		Objects.requireNonNull(values);
		Lock writeLock = rwLock.writeLock();
		writeLock.lock();
		try {
			final List<IFeature<?>> addedFeatures = new ArrayList<>();
			final Map<IFeature<?>, Collection<IObjectWithFeatures>> updatedFeatureValues = new HashMap<>();
			boolean changed = false;
			for (int i = 0; i < objectFeaturePairs.size(); i++) {
				final Pair<O, IFeature<V>> pair = objectFeaturePairs.get(i);
				final O object = pair.getFirst();
				final IFeature<V> feature = pair.getSecond();
				final int featureIdx;
				final IFeatureValue<V> value = values.get(i);
				Objects.requireNonNull(object);
				Objects.requireNonNull(feature);
				Objects.requireNonNull(value);
				if (!this.objectToValueIndex.containsKey(object)) {
					this.objectToValueIndex.put(object, new Int2LongOpenHashMap(this.expectedNumberFeatures * 2));
					changed = true;
				}
				if (!this.featureToIndex.containsKey(feature)) {
					featureIdx = this.features.size();
					this.features.add(feature);
					this.featureToIndex.put(feature, featureIdx);
					this.featureToValues.add(new ObjectBigArrayBigList<>(this.expectedNumberObjects * 2));
					addedFeatures.add(feature);
					changed = true;
				} else
					featureIdx = this.featureToIndex.getInt(feature);

				// find the index for the value
				final long idx;

				// if both object and feature were known, check whether the exact pair had been
				// already inserted
				if (!changed && this.objectToValueIndex.get(object).containsKey(featureIdx)) {
					idx = this.objectToValueIndex.get(object).get(featureIdx);
					this.featureToValues.get(featureIdx).set(idx, value);
				} else {
					idx = this.featureToValues.get(featureIdx).size64();
					this.featureToValues.get(featureIdx).add(value);
				}
				this.objectToValueIndex.get(object).put(featureIdx, idx);
				updatedFeatureValues.putIfAbsent(feature, new HashSet<>());
				updatedFeatureValues.get(feature).add(object);
			}

			if (!addedFeatures.isEmpty())
				createAndFireFeatureAddedEvent(addedFeatures);
			fireFeatureValuesStored(updatedFeatureValues);
//			if (changed)
//				fireFeatureStoreChanged(new FeatureStoreChangedEvent(this));
		} finally

		{
			writeLock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <O extends IObjectWithFeatures, V> IFeatureValue<V> getFeatureValue(final O object,
			final IFeature<V> feature) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			final int featureIdx = this.featureToIndex.getInt(feature);
			if (!this.objectToValueIndex.containsKey(object))
				throw new IllegalArgumentException(
						String.format("This store does not contain values for object '%s'.", object));
			if (!this.objectToValueIndex.get(object).containsKey(featureIdx))
				throw new IllegalArgumentException(String
						.format("This store does not hold a value of feature '%s' for object '%s'.", feature, object));
			return (IFeatureValue<V>) this.featureToValues.get(featureIdx)
					.get(this.objectToValueIndex.get(object).get(featureIdx));
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof BasicFeatureStore))
			return false;

		final BasicFeatureStore other = (BasicFeatureStore) obj;
		return this.objectToValueIndex.equals(other.objectToValueIndex)
				&& this.featureToValues.equals(other.featureToValues);
	}

	@Override
	public int hashCode() {
		return this.objectToValueIndex.hashCode() + this.featureToValues.hashCode();
	}

	/**
	 * @return the listener
	 */
	public Set<IFeatureStoreChangedListener> getListener() {
		if (this.listener == null)
			this.listener = new HashSet<>();
		return this.listener;
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		s.defaultReadObject();

		this.featureAddedListener = new HashSet<>();
		this.featureValueStoredListener = new HashSet<>();
		this.listener = new HashSet<>();
	}

	@Override
	public void addFeatureAddedListener(final IFeatureStoreFeatureAddedListener l) {
		this.featureAddedListener.add(l);
	}

	@Override
	public void removeFeatureAddedListener(final IFeatureStoreFeatureAddedListener l) {
		this.featureAddedListener.remove(l);
	}

	private void fireFeatureAdded(final FeaturesAddedEvent e) {
		for (final IFeatureStoreFeatureAddedListener l : this.featureAddedListener)
			l.storeFeatureAdded(e);
	}

	@Override
	public void addFeatureRemovedListener(final IFeatureStoreFeatureRemovedListener l) {
		this.featureRemovedListener.add(l);
	}

	@Override
	public void removeFeatureRemovedListener(final IFeatureStoreFeatureRemovedListener l) {
		this.featureRemovedListener.remove(l);
	}

	private void fireFeatureRemoved(final FeaturesRemovedEvent e) {
		for (final IFeatureStoreFeatureRemovedListener l : this.featureRemovedListener)
			l.storeFeatureRemoved(e);
	}

	public void fireFeatureStoreChanged(final FeatureStoreChangedEvent e) {
		for (final IFeatureStoreChangedListener l : this.listener)
			l.featureStoreChanged(e);
	}

	private void fireFeatureValuesStored(final Map<IFeature<?>, Collection<IObjectWithFeatures>> objectFeaturePairs) {
		final FeatureValuesStoredEvent e = new FeatureValuesStoredEvent(this, objectFeaturePairs);
		for (final IFeatureValueStoredListener l : this.featureValueStoredListener)
			l.featureValuesStored(e);
	}

	@Override
	public Set<IFeature<?>> featureSet() {
		return this.featureToIndex.keySet();
	}

	@Override
	public void addFeatureStoreChangeListener(final IFeatureStoreChangedListener l) {
		this.getListener().add(l);
	}

	@Override
	public void removeFeatureStoreChangeListener(final IFeatureStoreChangedListener l) {
		this.getListener().remove(l);
	}

	@Override
	public boolean addFeatureValueStoredListener(IFeatureValueStoredListener l) {
		return this.featureValueStoredListener.add(l);
	}

	@Override
	public boolean removeFeatureValueStoredListener(IFeatureValueStoredListener l) {
		return this.featureValueStoredListener.remove(l);
	}

	@Override
	public <O extends IObjectWithFeatures> boolean hasFeatureFor(IFeature<?> feature, O object) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			final int featureIdx = this.featureToIndex.getInt(feature);
			return this.objectToValueIndex.containsKey(object)
					&& this.objectToValueIndex.get(object).containsKey(featureIdx);
		} finally {
			readLock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <V> Collection<V> valuesRaw(final IFeature<V> feature) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			final int featureIdx = this.featureToIndex.getInt(feature);
			return Collections.unmodifiableCollection(this.featureToValues.get(featureIdx).stream()
					.map(v -> (V) v.getValue()).collect(Collectors.toList()));
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public <V> Iterable<IFeatureValue<V>> getFeatureValues(IFeature<V> feature) {
		return this.getFeatureValues(feature, null);
	}

	@Override
	public <V> Iterable<IFeatureValue<V>> getFeatureValues(IFeature<V> feature, ObjectType<?> objectType) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			final int featureIdx = this.featureToIndex.getInt(feature);
			return new Iterable<IFeatureValue<V>>() {
				@Override
				public Iterator<IFeatureValue<V>> iterator() {
					final Iterator<IObjectWithFeatures> it = BasicFeatureStore.this.objectToValueIndex.keySet()
							.iterator();
					return new Iterator<IFeatureValue<V>>() {
						private IObjectWithFeatures nextObjectWithFeature;

						@Override
						public boolean hasNext() {
							// burn objects for which this feature isn't present and which are not of the
							// specified type
							while (nextObjectWithFeature == null && it.hasNext()) {
								final IObjectWithFeatures tmp = it.next();
								if ((objectType == null || tmp.getObjectType() == objectType)
										&& objectToValueIndex.get(tmp).containsKey(featureIdx))
									nextObjectWithFeature = tmp;
							}
							return nextObjectWithFeature != null;
						}

						@Override
						public IFeatureValue<V> next() {
							final IObjectWithFeatures next = nextObjectWithFeature;
							this.nextObjectWithFeature = null;
							return (IFeatureValue<V>) featureToValues.get(featureIdx)
									.get(objectToValueIndex.get(next).get(featureIdx));
						}
					};
				}
			};
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public <V> Map<IObjectWithFeatures, IFeatureValue<V>> getFeatureValueMap(final IFeature<V> feature) {
		return this.getFeatureValueMap(feature, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <O extends IObjectWithFeatures, V> Map<O, IFeatureValue<V>> getFeatureValueMap(final IFeature<V> feature,
			final ObjectType<O> objectType) {
		Lock readLock = rwLock.readLock();
		readLock.lock();
		try {
			final int featureIdx = this.featureToIndex.getInt(feature);
			final Map<O, IFeatureValue<V>> res = new HashMap<>(this.expectedNumberObjects);

			for (final IObjectWithFeatures o : this.objectToValueIndex.keySet())
				if ((objectType == null || o.getObjectType() == objectType)
						&& this.objectToValueIndex.get(o).containsKey(featureIdx))
					res.put((O) o, (IFeatureValue<V>) this.featureToValues.get(featureIdx)
							.get(this.objectToValueIndex.get(o).get(featureIdx)));
			return res;
		} finally {
			readLock.unlock();
		}
	}

	/**
	 * 
	 */
	@Override
	public void clear() {
		Lock writeLock = rwLock.writeLock();
		writeLock.lock();
		try {
			this.objectToValueIndex.clear();
			this.featureToValues.clear();
		} finally {
			writeLock.unlock();
		}
	}

	/**
	 * @return
	 * 
	 */
	@Override
	public boolean removeFeature(final IFeature<?> feature) {
		Lock writeLock = rwLock.writeLock();
		writeLock.lock();
		try {
			if (!this.featureToIndex.containsKey(feature))
				return false;

			final int featureIdx = this.featureToIndex.removeInt(feature);
			this.features.set(featureIdx, null);
			this.objectToValueIndex.forEach((o, fmap) -> fmap.remove(featureIdx));

			FeaturesRemovedEvent event = new FeaturesRemovedEvent(this);
			event.setRemovedFeatures(Arrays.asList(feature));

			this.fireFeatureRemoved(event);
			return true;
		} finally {
			writeLock.unlock();
		}
	}
}
