/**
 * 
 */
package dk.sdu.imada.ticone.feature.scale;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.statistics.FeatureValueSampleException;
import dk.sdu.imada.ticone.statistics.IFeatureValueSample;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ScalerInitializationException;
import dk.sdu.imada.ticone.util.ScalerNotInitializedException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 2, 2018
 *
 */
public abstract class AbstractScalerBuilder<N, O extends IObjectWithFeatures, F extends IArithmeticFeature<N>> extends
		AbstractBuilder<IScaler, FactoryException, CreateInstanceFactoryException> implements IScalerBuilder<N, O, F> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2087980193970695661L;

	protected transient F featureToScale;

	protected transient IFeatureValueSample<N> featureValueSample;

	protected transient IObjectProvider provider;

	protected int sampleSize;

	protected long seed;

	protected Set<ChangeListener> samplingChangeListener;

	protected boolean excludeNaNValues, excludeInfinityValues;

	/**
	 * 
	 * Do not initialize anything but do it in the implementing sub class.
	 */
	public AbstractScalerBuilder() {
		super();
		this.samplingChangeListener = new HashSet<>();
		this.excludeInfinityValues = true;
		this.excludeNaNValues = true;
	}

	/**
	 * 
	 * Use the passed in feature and value sample to initialize the required fields
	 * of the created {@link IScaler}.
	 */
	public AbstractScalerBuilder(final F featureToScale, final IFeatureValueSample<N> featureValueSample) {
		this();
		this.featureToScale = featureToScale;
		this.featureValueSample = featureValueSample;
	}

	/**
	 * 
	 * Use the passed in feature and {@link IObjectProvider} to calculate a feature
	 * value sample and initialize the required fields of the created
	 * {@link IScaler}.
	 */
	public AbstractScalerBuilder(final F featureToScale, final IObjectProvider provider, final int sampleSize,
			final long seed) {
		this();
		this.featureToScale = featureToScale;
		this.provider = provider;
		this.sampleSize = sampleSize;
		this.seed = seed;
	}

	/**
	 * Copy the passed factory.
	 */
	public AbstractScalerBuilder(final AbstractScalerBuilder<N, O, F> scaler) {
		this();
		this.featureToScale = scaler.featureToScale;
		this.featureValueSample = scaler.featureValueSample;
		this.provider = scaler.provider;
		this.seed = scaler.seed;
	}

	/**
	 * @param excludeInfinityValues the excludeInfinityValues to set
	 */
	public void setExcludeInfinityValues(final boolean excludeInfinityValues) {
		this.excludeInfinityValues = excludeInfinityValues;
	}

	/**
	 * @param excludeNaNValues the excludeNaNValues to set
	 */
	public void setExcludeNaNValues(final boolean excludeNaNValues) {
		this.excludeNaNValues = excludeNaNValues;
	}

	/**
	 * @return the excludeInfinityValues
	 */
	public boolean isExcludeInfinityValues() {
		return this.excludeInfinityValues;
	}

	/**
	 * @return the excludeNaNValues
	 */
	public boolean isExcludeNaNValues() {
		return this.excludeNaNValues;
	}

	/**
	 * @param provider the provider to set
	 */
	@Override
	public IScalerBuilder<N, O, F> setObjectProvider(final IObjectProvider provider) {
		this.provider = provider;
		return this;
	}

	/**
	 * @param sampleSize the sampleSize to set
	 * @return
	 */
	@Override
	public IScalerBuilder<N, O, F> setSampleSize(int sampleSize) {
		this.sampleSize = sampleSize;
		return this;
	}

	/**
	 * @param random the random to set
	 * @return
	 */
	@Override
	public IScalerBuilder<N, O, F> setSeed(long seed) {
		this.seed = seed;
		return this;
	}

	/**
	 * @return the forObject
	 */
	@Override
	public IObjectProvider getObjectProvider() {
		return this.provider;
	}

	/**
	 * @param featureToScale the featureToScale to set
	 * @return
	 */
	@Override
	public IScalerBuilder<N, O, F> setFeatureToScale(final F featureToScale) {
		this.featureToScale = featureToScale;
		return this;
	}

	/**
	 * @return the featureToScale
	 */
	@Override
	public F getFeatureToScale() {
		return this.featureToScale;
	}

	/**
	 * @param featureValueSample the featureStore to set
	 */
	@Override
	public IScalerBuilder<N, O, F> setFeatureValueSample(final IFeatureValueSample<N> featureValueSample) {
		this.featureValueSample = featureValueSample;
		return this;
	}

	/**
	 * @return the featureStore
	 */
	@Override
	public IFeatureValueSample<N> getFeatureValueSample() {
		return this.featureValueSample;
	}

	@Override
	public IScaler doBuild() throws CreateInstanceFactoryException, InterruptedException {
		try {
			this.ensureFieldsForInitialization();
			if (!this.isInitialized())
				this.initialize();

			this.ensureValidFieldsForCreateInstance();
			final IScaler s = this.doCreateInstance();
			return s;
		} catch (ScalerNotInitializedException | FeatureValueSampleException | ScalerInitializationException
				| SimilarityCalculationException | FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleObjectProviderException e) {
			throw new CreateInstanceFactoryException(e);
		}
	}

	/**
	 * Is being called iff all required fields have been initialized.
	 * 
	 * @return
	 * @throws CreateInstanceFactoryException
	 */
	protected abstract AbstractScaler doCreateInstance()
			throws CreateInstanceFactoryException, ScalerInitializationException;

	@Override
	protected void reset() {
//		this.featureToScale = null;
//		this.featureValueSample = null;
//		this.provider = null;
//		this.samplingChangeListener.clear();
	}

	protected void ensureFieldsForInitialization() throws FeatureValueSampleException, ScalerNotInitializedException,
			InterruptedException, ScalerInitializationException {
		if (this.featureToScale == null)
			throw new ScalerNotInitializedException("featureToScale");
		if (this.featureValueSample == null) {
			if (this.provider == null)
				throw new ScalerNotInitializedException("provider");
			this.featureValueSample = this.provider.getFeatureValueSamples().get(this.featureToScale, this.sampleSize,
					this.seed);
		}
	}

	protected abstract void ensureValidFieldsForCreateInstance() throws IncorrectlyInitializedException;

	/**
	 * Initialize the fields that are passed on to the constructor of this factory's
	 * {@link IScaler} class.
	 * 
	 * @param values
	 * @throws ScalerInitializationException
	 * @throws IncompatibleObjectProviderException
	 * @throws IncorrectlyInitializedException
	 * @throws FeatureCalculationException
	 * @throws SimilarityCalculationException
	 */
	protected abstract void initialize() throws ScalerInitializationException, SimilarityCalculationException,
			FeatureCalculationException, IncorrectlyInitializedException, IncompatibleObjectProviderException;

	protected List<? extends IFeatureValue<N>> excludeInvalidSampleValuesForInitialization()
			throws NotAnArithmeticFeatureValueException, ToNumberConversionException {
		final List<? extends IFeatureValue<N>> values = this.featureValueSample.getValues();
		final List<IFeatureValue<N>> result = new ArrayList<>();

		for (IFeatureValue<N> fv : values) {
			final double doubleValue = fv.toNumber().doubleValue();
			if (!this.excludeNaNValues
					|| !Double.isNaN(doubleValue) && (!this.excludeInfinityValues || !Double.isInfinite(doubleValue)))
				result.add(fv);

		}

		return result;
	}

	protected abstract boolean isInitialized();

	/**
	 * Are retrieved iff this scaler is not yet initialized.
	 * 
	 * @return
	 */
	protected IFeatureValueSample<N> getValuesForInitialization() {
		return this.featureValueSample;
	}

	public boolean addSamplingChangeListener(final ChangeListener samplingChangeListener) {
		return this.samplingChangeListener.add(samplingChangeListener);
	}

	public boolean removeSamplingChangeListener(final ChangeListener samplingChangeListener) {
		return this.samplingChangeListener.remove(samplingChangeListener);
	}

	protected void fireSampleChanged() {
		final ChangeEvent e = new ChangeEvent(this);
		for (final ChangeListener l : this.samplingChangeListener) {
			l.stateChanged(e);
		}
	}
}
