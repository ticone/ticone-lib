/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

public class ObjectPairSimilarityFeature extends SimilarityFeature implements IObjectPairFeature<ISimilarityValue> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3748059824541121908L;

	/**
	 * @param simFunc
	 * @throws IncompatibleSimilarityFunctionException
	 */
	public ObjectPairSimilarityFeature(final ISimilarityFunction simFunc)
			throws IncompatibleSimilarityFunctionException {
		super(ObjectType.OBJECT_PAIR, simFunc);
	}

	public ObjectPairSimilarityFeature(ObjectPairSimilarityFeature other) {
		super(other);
	}

	@Override
	public ObjectPairSimilarityFeature copy() {
		return new ObjectPairSimilarityFeature(this);
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	public boolean validateInitialized() throws IncorrectlyInitializedException {
		if (this.similarityFunction == null)
			throw new IncorrectlyInitializedException("similarityFunction");
		return true;
	}

	@Override
	public String getName() {
		return "Object pair similarity";
	}
}