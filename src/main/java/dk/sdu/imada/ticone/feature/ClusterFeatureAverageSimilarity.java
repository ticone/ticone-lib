/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public class ClusterFeatureAverageSimilarity extends SimilarityFeature implements IClusterFeature<ISimilarityValue> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6617324328208015756L;

	/**
	 * 
	 */
	public ClusterFeatureAverageSimilarity() {
		super(ObjectType.CLUSTER);
	}

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	public ClusterFeatureAverageSimilarity(final ISimilarityFunction sim)
			throws IncompatibleSimilarityFunctionException {
		super(ObjectType.CLUSTER, sim);
	}

	protected ClusterFeatureAverageSimilarity(final ClusterFeatureAverageSimilarity other) {
		super(other);
	}

	@Override
	public ClusterFeatureAverageSimilarity copy() {
		return new ClusterFeatureAverageSimilarity(this);
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	public String getName() {
		if (this.similarityFunction != null)
			return String.format("Average Object-Prototype Similarity (%s)", this.similarityFunction.toString());
		return "Average Object-Prototype Similarity";
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ClusterFeatureAverageSimilarity))
			return false;

		final ClusterFeatureAverageSimilarity other = (ClusterFeatureAverageSimilarity) obj;
		return this.similarityFunction.getClass().equals(other.similarityFunction.getClass());
	};

	@Override
	public int hashCode() {
		if (this.similarityFunction != null)
			return super.hashCode() + this.similarityFunction.getClass().hashCode();
		return super.hashCode();
	}

}
