/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.pair.IClusterPairFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since May 1, 2017
 *
 */
public class ClusterPairFeaturePrototypeSimilarity extends SimilarityFeature
		implements IClusterPairFeature<ISimilarityValue> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5876608658474793274L;

	public ClusterPairFeaturePrototypeSimilarity() {
		super(ObjectType.CLUSTER_PAIR);
	}

	public ClusterPairFeaturePrototypeSimilarity(final ISimilarityFunction sim)
			throws IncompatibleSimilarityFunctionException {
		super(ObjectType.CLUSTER_PAIR, sim);
	}

	@Override
	public ClusterPairFeaturePrototypeSimilarity copy() {
		final ClusterPairFeaturePrototypeSimilarity copy = new ClusterPairFeaturePrototypeSimilarity();
		copy.similarityFunction = this.similarityFunction;
		return copy;
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	public boolean validateInitialized() throws IncorrectlyInitializedException {
		if (this.similarityFunction == null)
			throw new IncorrectlyInitializedException("similarityFunction");
		return true;
	}

	@Override
	public String getName() {
		if (this.similarityFunction != null)
			return String.format("Prototype Similarity (%s)", this.similarityFunction.toString());
		return "Prototype Similarity";
	}
}
