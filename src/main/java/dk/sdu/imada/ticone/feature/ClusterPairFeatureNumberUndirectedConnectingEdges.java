/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * @author Christian Wiwie
 * 
 * @since May 1, 2017
 *
 */
public class ClusterPairFeatureNumberUndirectedConnectingEdges extends AbstractFeature<Double>
		implements IClusterPairFeatureNumberUndirectedConnectingEdges {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7381259767438118860L;

	protected boolean scaleByClusterSizes;

	protected IFeatureValueProvider provider;

	public ClusterPairFeatureNumberUndirectedConnectingEdges() {
		super(ObjectType.CLUSTER_PAIR, Double.class);
	}

	@Override
	public IClusterPairFeatureNumberUndirectedConnectingEdges copy() {
		final ClusterPairFeatureNumberUndirectedConnectingEdges copy = new ClusterPairFeatureNumberUndirectedConnectingEdges();
		copy.provider = this.provider;
		copy.scaleByClusterSizes = this.scaleByClusterSizes;
		return copy;
	}

	/**
	 * @param scaleByClusterSizes the scaleByClusterSizes to set
	 */
	public void setScaleByClusterSizes(final boolean scaleByClusterSizes) {
		this.scaleByClusterSizes = scaleByClusterSizes;
	}

	/**
	 * @return the scaleByClusterSizes
	 */
	@Override
	public boolean isScaleByClusterSizes() {
		return this.scaleByClusterSizes;
	}

	@Override
	public String getName() {
		return "Number of Undirected Connecting Edges";
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures pair)
			throws FeatureCalculationException, InterruptedException {
		try {
			return this.provider.getFeatureValue(this, pair);
		} catch (final UnknownObjectFeatureValueProviderException | IncompatibleFeatureValueProviderException
				| IncompatibleFeatureAndObjectException e) {
			throw new FeatureCalculationException(e);
		}
	}

	@Override
	public IFeatureValueProvider getFeatureValueProvider() {
		return this.provider;
	}

	@Override
	public void setFeatureValueProvider(final IFeatureValueProvider provider) {
		this.provider = provider;
	}
}
