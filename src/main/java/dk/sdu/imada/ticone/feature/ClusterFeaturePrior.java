/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 10, 2017
 *
 */
public class ClusterFeaturePrior extends AbstractFeature<Double> implements IClusterFeaturePrior {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7544220056179535720L;

	protected int numberBins;
	protected IFeatureValueProvider provider;

	/**
	 * 
	 */
	public ClusterFeaturePrior() {
		super(ObjectType.CLUSTER, Double.class);
	}

	/**
	 * 
	 */
	public ClusterFeaturePrior(final int numberBins) {
		this();
		this.numberBins = numberBins;
	}

	@Override
	public IClusterFeaturePrior copy() {
		final ClusterFeaturePrior copy = new ClusterFeaturePrior();
		copy.numberBins = this.numberBins;
		return copy;
	}

	/**
	 * @param numberBins the numberBins to set
	 */
	public void setNumberBins(final int numberBins) {
		this.numberBins = numberBins;
	}

	/**
	 * @return the numberBins
	 */
	@Override
	public int getNumberBins() {
		return this.numberBins;
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures object)
			throws IncorrectlyInitializedException, FeatureCalculationException, InterruptedException {
		final ICluster cluster = ObjectType.CLUSTER.getBaseType().cast(object);
		try {
			return this.provider.getFeatureValue(this, cluster);
		} catch (final UnknownObjectFeatureValueProviderException | IncompatibleFeatureValueProviderException
				| IncompatibleFeatureAndObjectException e) {
			throw new FeatureCalculationException(e);
		}
	}

	@Override
	public String getName() {
		return "Prior Probability";
	}

	@Override
	public IFeatureValueProvider getFeatureValueProvider() {
		return this.provider;
	}

	@Override
	public void setFeatureValueProvider(final IFeatureValueProvider provider) {
		this.provider = provider;
	}
}
