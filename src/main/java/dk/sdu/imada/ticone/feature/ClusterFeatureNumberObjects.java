/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public class ClusterFeatureNumberObjects extends AbstractFeature<Integer>
		implements IClusterFeature<Integer>, INumericFeature<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8266354675638262958L;

	public ClusterFeatureNumberObjects() {
		super(ObjectType.CLUSTER, Integer.class);
	}

	@Override
	public ClusterFeatureNumberObjects copy() {
		return new ClusterFeatureNumberObjects();
	}

	@Override
	public String getName() {
		return "Number of Objects";
	}

	@Override
	public IFeatureValue<Integer> doCalculate(final IObjectWithFeatures cluster)
			throws IncorrectlyInitializedException, FeatureCalculationException {
		return value(cluster, ObjectType.CLUSTER.getBaseType().cast(cluster).getObjects().size());
	}
}
