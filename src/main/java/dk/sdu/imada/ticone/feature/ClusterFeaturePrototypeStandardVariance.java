/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.variance.IVariance;
import dk.sdu.imada.ticone.variance.StandardVariance;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 18, 2017
 *
 */
public class ClusterFeaturePrototypeStandardVariance extends AbstractFeature<Double>
		implements IClusterFeature<Double>, INumericFeature<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2765739812796564737L;

	protected IVariance varianceFunction = new StandardVariance();

	/**
	 * @param other
	 */
	public ClusterFeaturePrototypeStandardVariance() {
		super(ObjectType.CLUSTER, Double.class);
	}

	@Override
	public ClusterFeaturePrototypeStandardVariance copy() {
		final ClusterFeaturePrototypeStandardVariance copy = new ClusterFeaturePrototypeStandardVariance();
		copy.varianceFunction = this.varianceFunction;
		return copy;
	}

	@Override
	public boolean validateInitialized() throws IncorrectlyInitializedException {
		if (this.varianceFunction == null)
			throw new IncorrectlyInitializedException("stdVar");
		return true;
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures object)
			throws IncorrectlyInitializedException, FeatureCalculationException {
		final ICluster cluster = ObjectType.CLUSTER.getBaseType().cast(object);
		try {
			return value(cluster, this.varianceFunction.calculateVariance(
					PrototypeComponentType.TIME_SERIES.getComponent(cluster.getPrototype()).getTimeSeries().asArray()));
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			throw new FeatureCalculationException(e);
		}
	}

	@Override
	public String getName() {
		return "Prototype Standard Variance";
	}

	/**
	 * @return the varianceFunction
	 */
	public IVariance getVarianceFunction() {
		return this.varianceFunction;
	}
}
