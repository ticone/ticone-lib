/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPairFeature;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * @author Christian Wiwie
 * 
 * @since May 1, 2017
 *
 */
public class ClusterPairFeatureCommonObjectsList extends AbstractFeature<ITimeSeriesObjectList>
		implements IClusterPairFeature<ITimeSeriesObjectList> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7796656963203425766L;

	public ClusterPairFeatureCommonObjectsList() {
		super(ObjectType.CLUSTER_PAIR, ITimeSeriesObjectList.class);
	}

	@Override
	public ClusterPairFeatureCommonObjectsList copy() {
		final ClusterPairFeatureCommonObjectsList copy = new ClusterPairFeatureCommonObjectsList();
		return copy;
	}

	@Override
	public String getName() {
		return "List of Common Objects";
	}

	@Override
	public IFeatureValue<ITimeSeriesObjectList> doCalculate(final IObjectWithFeatures object) {
		final IClusterPair pair = (IClusterPair) object;
		final ITimeSeriesObjects ids1 = pair.getFirst().getObjects();
		final ITimeSeriesObjects ids2 = pair.getSecond().getObjects();

		ids1.retainAll(ids2);

		return value(pair, ids1.asList());
	}
}