/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

public class ObjectClusterPairSimilarityFeature extends SimilarityFeature
		implements IObjectClusterFeature<ISimilarityValue> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3230864020782453837L;

	/**
	 * @param simFunc
	 * @throws IncompatibleSimilarityFunctionException
	 */
	public ObjectClusterPairSimilarityFeature(final ISimilarityFunction simFunc)
			throws IncompatibleSimilarityFunctionException {
		super(ObjectType.OBJECT_CLUSTER_PAIR, simFunc);
	}

	protected ObjectClusterPairSimilarityFeature(ObjectClusterPairSimilarityFeature other) {
		super(other);
	}

	@Override
	public ObjectClusterPairSimilarityFeature copy() {
		return new ObjectClusterPairSimilarityFeature(this);
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction;
	}
}