/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Arrays;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 11, 2017
 *
 */
public class ClusterFeatureEntropy extends AbstractFeature<Double>
		implements IClusterFeatureEntropy, INumericFeature<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7868043692615233979L;
	protected int numberBins;
	double[][] bins;
	double[] max;
	double[] min;

	/**
	 * 
	 */
	public ClusterFeatureEntropy() {
		super(ObjectType.CLUSTER, Double.class);
	}

	/**
	 * 
	 */
	public ClusterFeatureEntropy(final int numberBins) {
		this();
		this.numberBins = numberBins;
	}

	@Override
	public IClusterFeatureEntropy copy() {
		final ClusterFeatureEntropy copy = new ClusterFeatureEntropy();
		copy.numberBins = this.numberBins;
		return copy;
	}

	/**
	 * @param numberBins the numberBins to set
	 */
	public void setNumberBins(final int numberBins) {
		this.numberBins = numberBins;
	}

	/**
	 * @return the numberBins
	 */
	@Override
	public int getNumberBins() {
		return this.numberBins;
	}

	@Override
	public String getName() {
		return "Shannon Entropy";
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures object)
			throws IncorrectlyInitializedException, FeatureCalculationException {
		try {
			final ICluster cluster = ObjectType.CLUSTER.getBaseType().cast(object);
			final ITimeSeriesObjects allObjects = cluster.getObjects();
			final double[] timeSeries = PrototypeComponentType.TIME_SERIES.getComponent(cluster.getPrototype())
					.getTimeSeries().asArray();
			this.initBins(timeSeries, allObjects);
			if (this.bins == null) {
				logger.info("Recalculate density function");
				// new clustering
				// -> recalculate bins

				for (final ITimeSeriesObject o : allObjects) {
					final ITimeSeries[] samples = o.getPreprocessedTimeSeriesList();
					for (int s = 0; s < samples.length; s++) {
						for (int tp = 0; tp < samples[s].getNumberTimePoints(); tp++) {
							this.min[tp] = Math.min(this.min[tp], samples[s].get(tp));
							this.max[tp] = Math.max(this.max[tp], samples[s].get(tp));
						}
					}
				}

				this.bins = new double[timeSeries.length][this.getNumberBins()];
				long totalCount = 0l;

				// count bins
				for (final ITimeSeriesObject o : allObjects) {
					final ITimeSeries[] samples = o.getPreprocessedTimeSeriesList();
					for (int s = 0; s < samples.length; s++) {
						for (int tp = 0; tp < samples[s].getNumberTimePoints(); tp++) {
							final double v = samples[s].get(tp);
							final int bin = this.valueToBin(tp, v);
							this.bins[tp][bin]++;
							totalCount++;
						}
					}
				}

				for (int tp = 0; tp < this.bins.length; tp++) {
					for (int b = 0; b < this.bins[tp].length; b++) {
						this.bins[tp][b] /= totalCount;
						// avoid 0 probs
						this.bins[tp][b] += Double.MIN_VALUE;
					}
				}
				logger.debug(Arrays.toString(this.bins));
			}

			double entropy = 0.0;
			for (final ITimeSeriesObject obj : allObjects) {
				for (int s = 0; s < obj.getPreprocessedTimeSeriesList().length; s++)
					for (int tp = 0; tp < obj.getPreprocessedTimeSeriesList()[s].getNumberTimePoints(); tp++) {
						final double p = this.bins[tp][this.valueToBin(tp,
								obj.getPreprocessedTimeSeriesList()[s].get(tp))];
						entropy += -p * Math.log(p);
					}
			}
			return value(cluster, entropy);
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			throw new FeatureCalculationException(e);
		}
	}

	private int valueToBin(final int timepoint, final double value) {
		final int res = (int) ((value - this.min[timepoint]) / (this.max[timepoint] - this.min[timepoint])
				* (this.getNumberBins() - 1));
		return res;
	}

	protected void initBins(final double[] timeSeries, final ITimeSeriesObjects allObjects) {
		final int numberTimepoints = timeSeries.length;

		this.min = new double[numberTimepoints];
		this.max = new double[numberTimepoints];
		this.bins = new double[numberTimepoints][this.numberBins];
		for (int i = 0; i < numberTimepoints; i++) {
			this.min[i] = Double.MAX_VALUE;
			this.max[i] = -Double.MAX_VALUE;
		}

		logger.info("Recalculate density function");
		// new clustering
		// -> recalculate bins

		for (final ITimeSeriesObject o : allObjects) {
			final ITimeSeries[] samples = o.getPreprocessedTimeSeriesList();
			for (int s = 0; s < samples.length; s++) {
				for (int tp = 0; tp < numberTimepoints; tp++) {
					try {
						this.min[tp] = Math.min(this.min[tp], samples[s].get(tp));
						this.max[tp] = Math.max(this.max[tp], samples[s].get(tp));
					} catch (final ArrayIndexOutOfBoundsException e) {
						e.printStackTrace();
					}
				}
			}
		}

		final long[] totalCount = new long[numberTimepoints];

		// count bins
		for (final ITimeSeriesObject o : allObjects) {
			final ITimeSeries[] samples = o.getPreprocessedTimeSeriesList();
			for (int s = 0; s < samples.length; s++) {
				for (int tp = 0; tp < numberTimepoints; tp++) {
					final double v = samples[s].get(tp);
					final int bin = this.valueToBin(tp, v);
					this.bins[tp][bin]++;
					totalCount[tp]++;
				}
			}
		}

		for (int tp = 0; tp < numberTimepoints; tp++) {
			for (int b = 0; b < this.bins[tp].length; b++) {
				this.bins[tp][b] /= totalCount[tp];
				// avoid 0 probs
				this.bins[tp][b] += Double.MIN_VALUE;
			}
		}
		logger.debug(Arrays.toString(this.bins));
	}

}
