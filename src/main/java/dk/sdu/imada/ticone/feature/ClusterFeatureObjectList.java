/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public class ClusterFeatureObjectList extends AbstractFeature<ITimeSeriesObjectList>
		implements IClusterFeatureObjectList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8266354675638262958L;

	public ClusterFeatureObjectList() {
		super(ObjectType.CLUSTER, ITimeSeriesObjectList.class);
	}

	@Override
	public ClusterFeatureObjectList copy() {
		return new ClusterFeatureObjectList();
	}

	@Override
	public String getName() {
		return "Object List";
	}

	@Override
	public IFeatureValue<ITimeSeriesObjectList> doCalculate(final IObjectWithFeatures cluster)
			throws IncorrectlyInitializedException, FeatureCalculationException {
		return value(cluster, ObjectType.CLUSTER.getBaseType().cast(cluster).getObjects().asList());
	}
}
