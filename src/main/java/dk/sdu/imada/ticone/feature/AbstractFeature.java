/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.io.IOException;
import java.io.ObjectInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since May 5, 2017
 *
 * @param <V>
 */
public abstract class AbstractFeature<R> implements IFeature<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7517806733993584713L;

	protected transient IFeatureStore cache;
	protected boolean useCache, storeFeatureInValues, storeObjectInValues;
	protected final ObjectType<? extends IObjectWithFeatures> objectType;
	protected final Class<R> valueType;
	protected transient Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 
	 */
	public AbstractFeature(final ObjectType<? extends IObjectWithFeatures> objectType, final Class<R> valueType) {
		super();
		this.useCache = true;
		this.cache = new BasicFeatureStore();
		this.objectType = objectType;
		this.valueType = valueType;
	}

	protected AbstractFeature(final AbstractFeature<R> other) {
		this.useCache = other.useCache;
		this.storeFeatureInValues = other.storeFeatureInValues;
		this.storeObjectInValues = other.storeObjectInValues;
		if (this.useCache)
			this.cache = new BasicFeatureStore();
		this.objectType = other.objectType;
		this.valueType = other.valueType;
	}

	@Override
	public void setUseCache(final boolean useCache) {
		this.useCache = useCache;
		if (useCache && this.cache == null)
			this.cache = new BasicFeatureStore();
		else if (!useCache && this.cache != null)
			this.cache = null;
	}

	@Override
	public void setStoreFeatureInValues(boolean storeFeature) {
		this.storeFeatureInValues = storeFeature;
	}

	@Override
	public void setStoreObjectInValues(boolean storeObject) {
		this.storeObjectInValues = storeObject;
	}

	@Override
	public boolean equals(final Object obj) {
		return this.getClass().equals(obj.getClass());
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	public ObjectType<? extends IObjectWithFeatures> supportedObjectType() {
		return this.objectType;
	}

	@Override
	public Class<R> getValueType() {
		return this.valueType;
	}

	@Override
	public boolean hasCache() {
		return this.cache != null;
	}

	@Override
	public boolean isStoreFeatureInValues() {
		return this.storeFeatureInValues;
	}

	@Override
	public boolean isStoreObjectInValues() {
		return this.storeObjectInValues;
	}

	@Override
	public IFeatureStore getCache() {
		return this.cache;
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		s.defaultReadObject();
		if (useCache)
			this.cache = new BasicFeatureStore();
		this.logger = LoggerFactory.getLogger(this.getClass());
	}

	/**
	 * 
	 * @throws InterruptedException
	 * @throws IncompatibleFeatureAndObjectException
	 */
	@Override
	public IFeatureValue<R> calculate(final IObjectWithFeatures object) throws FeatureCalculationException,
			IncorrectlyInitializedException, InterruptedException, IncompatibleFeatureAndObjectException {
		ensureCompatibleObjectType(object);
		final boolean objectStoresFeatureValues = object.storesFeatureValues();
		try {
			if (objectStoresFeatureValues && object.hasFeatureValueSet(this))
				return object.getFeatureValue(this);
		} catch (FeatureValueNotSetException e) {
		}
		final boolean hasCache = hasCache();
		if (hasCache) {
			final IFeatureStore cache = getCache();
			if (cache.contains(object))
				return cache.getFeatureValue(object, this);
		}
		validateInitialized();
		final IFeatureValue<R> result = doCalculate(object);
		if (objectStoresFeatureValues)
			object.setFeatureValue(this, result);
		if (hasCache)
			getCache().setFeatureValue(object, this, result);
		if (isStoreFeatureInValues())
			result.setFeature(this);
		if (isStoreObjectInValues())
			result.setObject(object);
		return result;
	}

	/**
	 * @throws IncompatibleFeatureAndObjectException
	 * 
	 */
	private void ensureCompatibleObjectType(final IObjectWithFeatures object)
			throws IncompatibleFeatureAndObjectException {
		if (this.supportedObjectType() != object.getObjectType())
			throw new IncompatibleFeatureAndObjectException();
	}

	/**
	 * <p>
	 * Implement this to define how a {@link IFeatureValue} is calculated for this
	 * feature.
	 * </p>
	 * <p>
	 * When this method is called it has been validated that the passed
	 * {@link IObjectWithFeatures} is of the correct type, i.e. it is equal to
	 * {@link #supportedObjectType()}. Thus, it is safe to type cast to the desired
	 * class.
	 * </p>
	 * 
	 * @throws InterruptedException
	 */
	protected abstract IFeatureValue<R> doCalculate(final IObjectWithFeatures object)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException;

	@Override
	public final IFeatureValue<R> value(IObjectWithFeatures object, R rawValue) {
		final IFeatureValue<R> value = value(rawValue);
		if (isStoreObjectInValues())
			value.setObject(object);
		return value;
	}

	@Override
	public final IFeatureValue<R> value(R rawValue) {
		final IFeatureValue<R> value = doValue(rawValue);
		if (isStoreFeatureInValues())
			value.setFeature(this);
		return value;
	}

	protected IFeatureValue<R> doValue(R rawValue) {
		return new FeatureValue<>((Class<? extends IFeature<R>>) this.getClass(), this.valueType, rawValue);
	}
}