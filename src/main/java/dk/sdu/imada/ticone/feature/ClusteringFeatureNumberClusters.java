/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 4, 2019
 *
 */
public class ClusteringFeatureNumberClusters extends AbstractFeature<Integer>
		implements IClusteringFeature<Integer>, INumericFeature<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6415508014583643573L;

	@Override
	public String getName() {
		return "Number of clusters";
	}

	public ClusteringFeatureNumberClusters() {
		super(ObjectType.CLUSTERING, Integer.class);
	}

	@Override
	public IFeature<Integer> copy() {
		return new ClusteringFeatureNumberClusters();
	}

	@Override
	protected IFeatureValue<Integer> doCalculate(final IObjectWithFeatures object)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
		return value(object, ((IClusterObjectMapping) object).getClusters().size());
	}

}
