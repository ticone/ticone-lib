/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 9, 2018
 *
 */
public class ObjectPairFeatureShortestDistance extends AbstractFeature<Double>
		implements IObjectPairFeatureShortestDistance {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2806494685093632375L;
	protected boolean isDirected, ensureKnownObjects;
	protected IFeatureValueProvider provider;

	public ObjectPairFeatureShortestDistance(final boolean isDirected) {
		super(ObjectType.OBJECT_PAIR, Double.class);
		this.isDirected = isDirected;
		this.ensureKnownObjects = true;
	}

	@Override
	public boolean isEnsureKnownObjects() {
		return this.ensureKnownObjects;
	}

	/**
	 * @param ensureKnownObjects the ensureKnownObjects to set
	 */
	public void setEnsureKnownObjects(final boolean ensureKnownObjects) {
		this.ensureKnownObjects = ensureKnownObjects;
	}

	@Override
	public boolean isDirected() {
		return this.isDirected;
	}

	@Override
	public ObjectPairFeatureShortestDistance copy() {
		return new ObjectPairFeatureShortestDistance(this.isDirected);
	}

	@Override
	public String getName() {
		if (this.isDirected)
			return "Average Shortest Directed Path";
		return "Average Shortest Undirected Path";
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures pair)
			throws FeatureCalculationException, InterruptedException {
		try {
			return this.provider.getFeatureValue(this, pair);
		} catch (final UnknownObjectFeatureValueProviderException | IncompatibleFeatureValueProviderException
				| IncompatibleFeatureAndObjectException e) {
			throw new FeatureCalculationException(e);
		}
	}

	@Override
	public IFeatureValueProvider getFeatureValueProvider() {
		return this.provider;
	}

	@Override
	public void setFeatureValueProvider(final IFeatureValueProvider provider) {
		this.provider = provider;
	}
}
