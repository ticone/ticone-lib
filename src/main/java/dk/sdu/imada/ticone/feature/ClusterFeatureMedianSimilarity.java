/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 5, 2019
 *
 */
public class ClusterFeatureMedianSimilarity extends SimilarityFeature implements IClusterFeature<ISimilarityValue> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7425160022011202738L;

	public ClusterFeatureMedianSimilarity() {
		super(ObjectType.CLUSTER);
	}

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	public ClusterFeatureMedianSimilarity(final ISimilarityFunction sim)
			throws IncompatibleSimilarityFunctionException {
		super(ObjectType.CLUSTER, sim);
	}

	protected ClusterFeatureMedianSimilarity(final ClusterFeatureMedianSimilarity other) {
		super(other);
	}

	@Override
	public ClusterFeatureMedianSimilarity copy() {
		return new ClusterFeatureMedianSimilarity(this);
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	public String getName() {
		if (this.similarityFunction != null)
			return String.format("Median Object-Prototype Similarity (%s)", this.similarityFunction.toString());
		return "Median Object-Prototype Similarity";
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ClusterFeatureMedianSimilarity))
			return false;

		final ClusterFeatureMedianSimilarity other = (ClusterFeatureMedianSimilarity) obj;
		return this.similarityFunction.getClass().equals(other.similarityFunction.getClass());
	};

	@Override
	public int hashCode() {
		if (this.similarityFunction != null)
			return super.hashCode() + this.similarityFunction.getClass().hashCode();
		return super.hashCode();
	}
}
