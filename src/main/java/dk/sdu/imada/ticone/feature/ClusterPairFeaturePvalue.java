/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Dec 14, 2018
 *
 */
public class ClusterPairFeaturePvalue extends AbstractFeature<Double> implements IClusterPairFeaturePvalue {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5371829071554514025L;

	protected IFeatureValueProvider provider;

	/**
	 * 
	 */
	public ClusterPairFeaturePvalue() {
		super(ObjectType.CLUSTER_PAIR, Double.class);
	}

	@Override
	public ClusterPairFeaturePvalue copy() {
		final ClusterPairFeaturePvalue copy = new ClusterPairFeaturePvalue();
		copy.provider = this.provider;
		return copy;
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures clusterPair)
			throws IncorrectlyInitializedException, FeatureCalculationException, InterruptedException {
		this.validateInitialized();
		try {
			return this.provider.getFeatureValue(this, clusterPair);
		} catch (final UnknownObjectFeatureValueProviderException | IncompatibleFeatureValueProviderException
				| IncompatibleFeatureAndObjectException e) {
			throw new FeatureCalculationException(e);
		}
	}

	@Override
	public String getName() {
		return "P-value";
	}

	@Override
	public IFeatureValueProvider getFeatureValueProvider() {
		return this.provider;
	}

	@Override
	public void setFeatureValueProvider(final IFeatureValueProvider provider) {
		this.provider = provider;
	}
}
