/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.validity.IClusterValidityIndex;
import dk.sdu.imada.ticone.clustering.validity.ValidityCalculationException;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 5, 2019
 *
 */
public class ClusteringFeatureValidity extends AbstractFeature<Double>
		implements IClusteringFeature<Double>, INumericFeature<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2040261863028056180L;

	private IClusterValidityIndex validityIndex;
	private ISimilarityFunction similarityFunction;

	/**
	 * 
	 */
	public ClusteringFeatureValidity() {
		super(ObjectType.CLUSTERING, Double.class);
	}

	/**
	 * 
	 */
	public ClusteringFeatureValidity(final IClusterValidityIndex validityIndex,
			final ISimilarityFunction similarityFunction) {
		this();
		this.validityIndex = validityIndex;
		this.similarityFunction = similarityFunction;
	}

	@Override
	public ClusteringFeatureValidity copy() {
		return new ClusteringFeatureValidity(this.validityIndex, this.similarityFunction);
	}

	@Override
	public String getName() {
		if (this.validityIndex != null)
			return String.format("Cluster Validity (%s)", this.validityIndex.toString());
		return "Cluster Validity";
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ClusteringFeatureValidity))
			return false;

		final ClusteringFeatureValidity other = (ClusteringFeatureValidity) obj;
		return this.validityIndex.getClass().equals(other.validityIndex.getClass());
	}

	@Override
	public int hashCode() {
		if (this.validityIndex != null)
			return super.hashCode() + this.validityIndex.getClass().hashCode();
		return super.hashCode();
	}

	@Override
	public IFeatureValue<Double> doCalculate(final IObjectWithFeatures clustering)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
		try {
			return this.value(clustering,
					this.validityIndex.getValidity((IClusterObjectMapping) clustering, similarityFunction));
		} catch (ValidityCalculationException e) {
			throw new FeatureCalculationException(e);
		}
	}

}
