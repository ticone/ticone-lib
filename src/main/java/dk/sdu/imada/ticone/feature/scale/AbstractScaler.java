/**
 * 
 */
package dk.sdu.imada.ticone.feature.scale;

import dk.sdu.imada.ticone.util.IConvertibleToNumber;
import dk.sdu.imada.ticone.util.ScalingException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

public abstract class AbstractScaler implements IScaler {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8439875204807222452L;

	/**
	 * 
	 */
	AbstractScaler() {
		super();
	}

	AbstractScaler(final AbstractScaler scaler) {
		this();
	}

	@Override
	public double scale(IConvertibleToNumber<? extends Number> value) throws ScalingException {
		try {
			return scale(new Double(value.toNumber().doubleValue() / value.cardinality())) * value.cardinality();
		} catch (ToNumberConversionException e) {
			throw new ScalingException(e);
		}
	}

	@Override
	public double[] scale(IConvertibleToNumber<? extends Number>... values) throws ScalingException {
		try {
			final Number[] numbers = new Number[values.length];
			int i = 0;
			for (IConvertibleToNumber<? extends Number> v : values)
				numbers[i++] = new Double(v.toNumber().doubleValue() / v.cardinality());
			double[] scaled = scale(numbers);
			for (i = 0; i < scaled.length; i++)
				scaled[i] *= values[i].cardinality();
			return scaled;
		} catch (ToNumberConversionException e) {
			throw new ScalingException(e);
		}
	}

	@Override
	public double[] scale(Number... values) throws ScalingException {
		final double[] result = new double[values.length];
		for (int i = 0; i < result.length; i++)
			result[i] = this.scale(values[i]);
		return result;
	}
}