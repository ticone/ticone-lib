/**
 * 
 */
package dk.sdu.imada.ticone.feature.scale;

import java.util.List;
import java.util.Objects;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.preprocessing.INormalizer;
import dk.sdu.imada.ticone.statistics.FeatureValueSampleException;
import dk.sdu.imada.ticone.statistics.IFeatureValueSample;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.IConvertibleToNumber;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ScalerInitializationException;
import dk.sdu.imada.ticone.util.ScalerNotInitializedException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
public class TanhNormalizerBuilder<V extends Comparable<V>, O extends IObjectWithFeatures, F extends IArithmeticFeature<V>>
		extends AbstractScalerBuilder<V, O, F> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6576240915535052428L;
	protected transient Double mean, var;

	/**
	 * 
	 */
	public TanhNormalizerBuilder() {
	}

	public TanhNormalizerBuilder(final Double mean, final Double var) {
		super();
		this.mean = mean;
		this.var = var;
	}

	public TanhNormalizerBuilder(final IConvertibleToNumber<Double> mean, final IConvertibleToNumber<Double> var)
			throws ToNumberConversionException {
		this(mean.toNumber(), var.toNumber());
	}

	/**
	 * @param featureToScale
	 * @param featureValueSample
	 */
	public TanhNormalizerBuilder(final F featureToScale, final IFeatureValueSample<V> featureValueSample) {
		super(featureToScale, featureValueSample);
	}

	public TanhNormalizerBuilder(final F featureToScale, final IObjectProvider provider, final int sampleSize,
			final long seed) {
		super(featureToScale, provider, sampleSize, seed);
	}

	public TanhNormalizerBuilder(final TanhNormalizerBuilder<V, O, F> scaler) {
		super(scaler);
		this.mean = scaler.mean;
		this.var = scaler.var;
	}

	/**
	 * @param mean the mean to set
	 */
	public TanhNormalizerBuilder<V, O, F> setMean(final Double mean) {
		this.mean = mean;
		return this;
	}

	public TanhNormalizerBuilder<V, O, F> setMean(final IConvertibleToNumber<Double> mean)
			throws ToNumberConversionException {
		return this.setMean(mean.toNumber());
	}

	/**
	 * @param var the var to set
	 */
	public TanhNormalizerBuilder<V, O, F> setVar(final double var) {
		this.var = var;
		return this;
	}

	public TanhNormalizerBuilder<V, O, F> setVar(final IConvertibleToNumber<Double> var)
			throws ToNumberConversionException {
		return this.setVar(var.toNumber());
	}

	@Override
	public TanhNormalizerBuilder<V, O, F> copy() {
		return new TanhNormalizerBuilder<>(this);
	}

	@Override
	protected void ensureFieldsForInitialization() throws FeatureValueSampleException, ScalerNotInitializedException,
			ScalerInitializationException, InterruptedException {
		if (this.mean != null && this.var != null)
			return;
		super.ensureFieldsForInitialization();
	}

	@Override
	protected void ensureValidFieldsForCreateInstance() throws IncorrectlyInitializedException {
		if (this.var.isInfinite() || this.var.isNaN())
			throw new IncorrectlyInitializedException("TanhNormalizer variance needs to be a positive finite number.");
		if (this.var.doubleValue() == 0.0)
			throw new IncorrectlyInitializedException("TanhNormalizer variance cannot be 0.");
	}

	@Override
	public TanhNormalizer doCreateInstance() throws CreateInstanceFactoryException {
		return new TanhNormalizer<>(this.mean, this.var);
	}

	@Override
	protected void reset() {
		super.reset();
		this.mean = null;
		this.var = null;
	}

	@Override
	public TanhNormalizerBuilder<V, O, F> setObjectProvider(final IObjectProvider forObject) {
		super.setObjectProvider(forObject);
		return this;
	}

	@Override
	protected void initialize() throws ScalerInitializationException {
		try {
			final List<? extends IFeatureValue<V>> values = this.excludeInvalidSampleValuesForInitialization();
			if (values.size() < 2)
				throw new ScalerInitializationException(
						"TanhNormalizer requires at least two numerical (non-NaN) values for initialization");
			this.mean = this.mean(values);
			this.var = this.sd(this.mean, values);
		} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException e) {
			throw new ScalerInitializationException(e);
		}
	}

	@Override
	protected boolean isInitialized() {
		return this.mean != null && this.var != null;
	}

	private double mean(final List<? extends IFeatureValue<V>> values)
			throws NotAnArithmeticFeatureValueException, ToNumberConversionException {
		double result = 0.0;

		for (final IFeatureValue<V> v : values)
			result += v.toNumber().doubleValue();

		return result / values.size();
	}

	private double sd(final double mean, final List<? extends IFeatureValue<V>> values)
			throws NotAnArithmeticFeatureValueException, ToNumberConversionException {
		double result = 0.0;

		for (final IFeatureValue<V> v : values)
			result += Math.pow(v.toNumber().doubleValue() - mean, 2.0);

		return Math.sqrt(result / values.size());
	}
}

class TanhNormalizer<V extends Number> extends AbstractScaler implements INormalizer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5655798437871355293L;
	protected Double mean, var;

	TanhNormalizer(final Double mean, final Double var) {
		super();
		this.mean = mean;
		this.var = var;
	}

	TanhNormalizer(final TanhNormalizer scaler) {
		super(scaler);
		this.mean = scaler.mean;
		this.var = scaler.var;
	}

	@Override
	public boolean equals(final Object obj) {
		return super.equals(obj) && (obj instanceof TanhNormalizer) && this.mean == ((TanhNormalizer) obj).mean
				&& this.var == ((TanhNormalizer) obj).var;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), this.mean, this.var);
	}

	@Override
	public TanhNormalizer copy() {
		return new TanhNormalizer<>(this);
	}

	@Override
	public double normalize(final double value) {
		if (this.var == 0.0) {
			if (value == this.mean)
				return 0.5;
			else if (value > this.mean)
				return 1.0;
			return -1.0;
		}
		return 0.5 * (Math.tanh(0.01 * ((value - this.mean) / this.var)) + 1);
	}

	@Override
	public double scale(final Number value) {
		if (Double.isNaN(value.doubleValue()))
			return Double.NaN;
		return this.normalize(value.doubleValue());
	}

	@Override
	public String toString() {
		return String.format("%s: %.3f %.3f", this.getClass().getSimpleName(), this.mean, this.var);
	}
}
