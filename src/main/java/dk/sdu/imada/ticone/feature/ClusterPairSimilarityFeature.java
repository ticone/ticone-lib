/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.pair.IClusterPairFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

public class ClusterPairSimilarityFeature extends SimilarityFeature implements IClusterPairFeature<ISimilarityValue> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2568750804994435420L;

	/**
	 * @param simFunc
	 * @throws IncompatibleSimilarityFunctionException
	 */
	public ClusterPairSimilarityFeature(final ISimilarityFunction simFunc)
			throws IncompatibleSimilarityFunctionException {
		super(ObjectType.CLUSTER_PAIR, simFunc);
	}

	protected ClusterPairSimilarityFeature(ClusterPairSimilarityFeature other) {
		super(other);
	}

	@Override
	public ClusterPairSimilarityFeature copy() {
		return new ClusterPairSimilarityFeature(this);
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction;
	}
}