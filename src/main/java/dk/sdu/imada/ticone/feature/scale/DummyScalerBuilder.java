/**
 * 
 */
package dk.sdu.imada.ticone.feature.scale;

import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.statistics.FeatureValueSampleException;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.ScalerInitializationException;
import dk.sdu.imada.ticone.util.ScalerNotInitializedException;
import dk.sdu.imada.ticone.util.ScalingException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 10, 2019
 *
 * @param <V>
 * @param <O>
 * @param <F>
 */
public class DummyScalerBuilder<V extends Comparable<V>, O extends IObjectWithFeatures, F extends IArithmeticFeature<V>>
		extends AbstractScalerBuilder<V, O, F> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3412432838958725174L;

	public DummyScalerBuilder() {
	}

	public DummyScalerBuilder(final AbstractScalerBuilder<V, O, F> scaler) {
		super(scaler);
	}

	@Override
	public DummyScalerBuilder<V, O, F> copy() {
		return new DummyScalerBuilder<>(this);
	}

	@Override
	public DummyScalerBuilder<V, O, F> setObjectProvider(final IObjectProvider forObject) {
		super.setObjectProvider(forObject);
		return this;
	}

	@Override
	public DummyScaler doBuild() throws CreateInstanceFactoryException, InterruptedException {
		return (DummyScaler) super.doBuild();
	}

	@Override
	public DummyScaler doCreateInstance() throws CreateInstanceFactoryException {
		return new DummyScaler();
	}

	@Override
	protected void initialize() throws SimilarityCalculationException, FeatureCalculationException,
			IncorrectlyInitializedException, IncompatibleObjectProviderException, ScalerInitializationException {
	}

	@Override
	public String toString() {
		return "DummyScaler";
	}

	@Override
	protected void ensureFieldsForInitialization() throws FeatureValueSampleException, ScalerNotInitializedException,
			InterruptedException, ScalerInitializationException {
	}

	@Override
	protected void ensureValidFieldsForCreateInstance() throws IncorrectlyInitializedException {
	}

	@Override
	protected boolean isInitialized() {
		return false;
	}
}

class DummyScaler extends AbstractScaler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 123777443506622489L;

	DummyScaler() {
		super();
	}

	DummyScaler(final DummyScaler scaler) {
		super(scaler);
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof DummyScaler;
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public DummyScaler copy() {
		return new DummyScaler(this);
	}

	@Override
	public double scale(final Number value) throws ScalingException {
		final double v = value.doubleValue();
		if (Double.isNaN(v))
			return 0.0;
		return 1.0;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

}
