/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.HashMap;
import java.util.Map;

import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessValue;
import dk.sdu.imada.ticone.statistics.IPvalue;

public class FeaturePvalueValue extends FeatureValue<IPvalue> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 944866835591349473L;

	protected long totalObservations, betterObservations;

	protected Map<IFitnessScore, IFitnessValue> fitnessValues;

	FeaturePvalueValue(IPvalue value) {
		super(FeaturePvalue.class, IPvalue.class, value);
		this.fitnessValues = new HashMap<>();
	}

	/**
	 * @param fitnessValue the fitnessValue to set
	 */
	public void setFitnessValue(IFitnessScore fitnessScore, IFitnessValue fitnessValue) {
		this.fitnessValues.put(fitnessScore, fitnessValue);
	}

	/**
	 * @return the fitnessValues
	 */
	public Map<IFitnessScore, IFitnessValue> getFitnessValues() {
		return this.fitnessValues;
	}

	/**
	 * @param totalObservations the totalObservations to set
	 */
	public void setTotalObservations(long totalObservations) {
		this.totalObservations = totalObservations;
	}

	/**
	 * @return the totalObservations
	 */
	public long getTotalObservations() {
		return this.totalObservations;
	}

	/**
	 * @param betterObservations the betterObservations to set
	 */
	public void setBetterObservations(long betterObservations) {
		this.betterObservations = betterObservations;
	}

	/**
	 * @return the betterObservations
	 */
	public long getBetterObservations() {
		return this.betterObservations;
	}
}