/**
 * 
 */
package dk.sdu.imada.ticone.feature.scale;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.statistics.FeatureValueSampleException;
import dk.sdu.imada.ticone.statistics.IFeatureValueSample;
import dk.sdu.imada.ticone.statistics.Quantiles;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ScalerInitializationException;
import dk.sdu.imada.ticone.util.ScalerNotInitializedException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
public class QuantileScalerBuilder<V extends Comparable<V>, O extends IObjectWithFeatures, F extends IArithmeticFeature<V>>
		extends AbstractScalerBuilder<V, O, F> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1588680079739012211L;
	protected transient double[] icdf;
	protected transient double[] quantiles;

	/**
	 * 
	 */
	public QuantileScalerBuilder() {
	}

	/**
	 * 
	 */
	public QuantileScalerBuilder(final double[] quantiles, final double[] icdf) {
		super();
		this.icdf = icdf;
		this.quantiles = quantiles;
	}

	public QuantileScalerBuilder(final F featureToScale, final IFeatureValueSample<V> featureValueSample) {
		super(featureToScale, featureValueSample);
	}

	public QuantileScalerBuilder(final F featureToScale, final IObjectProvider provider, final int sampleSize,
			final long seed) {
		super(featureToScale, provider, sampleSize, seed);
	}

	public QuantileScalerBuilder(final QuantileScalerBuilder<V, O, F> scaler) {
		super(scaler);
		this.quantiles = Arrays.copyOf(scaler.quantiles, scaler.quantiles.length);
	}

	@Override
	public QuantileScalerBuilder<V, O, F> copy() {
		return new QuantileScalerBuilder<>(this);
	}

	/**
	 * @param quantiles the quantiles to set
	 */
	public QuantileScalerBuilder<V, O, F> setQuantiles(final double[] quantiles) {
		this.quantiles = quantiles;
		return this;
	}

	/**
	 * @return the quantiles
	 */
	public double[] getQuantiles() {
		return this.quantiles;
	}

	@Override
	protected void ensureFieldsForInitialization() throws FeatureValueSampleException, ScalerNotInitializedException,
			ScalerInitializationException, InterruptedException {
		if (this.icdf != null)
			return;
		if (this.quantiles == null)
			throw new ScalerNotInitializedException("quantiles");
		super.ensureFieldsForInitialization();
	}

	@Override
	protected void ensureValidFieldsForCreateInstance() throws IncorrectlyInitializedException {
	}

	@Override
	public QuantileScaler doBuild() throws CreateInstanceFactoryException, InterruptedException {
		return (QuantileScaler) super.doBuild();
	}

	@Override
	protected void reset() {
		super.reset();
//		this.icdf = null;
//		this.quantiles = null;
	}

	@Override
	public QuantileScaler doCreateInstance() throws CreateInstanceFactoryException {
		return new QuantileScaler(this.quantiles, this.icdf);
	}

	@Override
	protected void initialize() throws ScalerInitializationException {
		try {
			final List<? extends IFeatureValue<V>> values = this.excludeInvalidSampleValuesForInitialization();
			this.icdf = Quantiles.calculate(values, this.quantiles);
		} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException e) {
			throw new ScalerInitializationException(e);
		}
	}

	@Override
	protected boolean isInitialized() {
		return this.icdf != null;
	}
}

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
class QuantileScaler extends AbstractScaler {

	/**
	 * 
	 */
	private static final long serialVersionUID = -570680829971540044L;

	protected double[] quantiles;

	/**
	 * Needs to be double because we may take the mean of multiple N values
	 */
	protected double[] icdf;

	/**
	 * 
	 */
	QuantileScaler(final double[] quantiles, final double[] icdf) {
		super();
		this.quantiles = quantiles;
		this.icdf = icdf;
	}

	QuantileScaler(final QuantileScaler scaler) {
		super(scaler);
		this.quantiles = Arrays.copyOf(scaler.quantiles, scaler.quantiles.length);
		this.icdf = Arrays.copyOf(scaler.icdf, scaler.icdf.length);
	}

	@Override
	public boolean equals(final Object obj) {
		return super.equals(obj) && obj instanceof QuantileScaler
				&& Arrays.equals(this.icdf, ((QuantileScaler) obj).icdf);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), this.getClass(), this.icdf);
	}

	@Override
	public double scale(final Number value) {
		final Double doubleValue = value.doubleValue();
		if (doubleValue.isNaN())
			return Double.NaN;
		int i = 0, j = this.icdf.length - 1;
		while (j > 0 && i < this.icdf.length - 1 && Double.compare(this.icdf[i], this.icdf[j]) != 0) {
			final int c = (int) Math.floor((i + j) / 2.0);
			if (doubleValue.doubleValue() < this.icdf[c]) {
				j = c - 1;
			} else if (doubleValue.doubleValue() >= this.icdf[c + 1])
				i = c + 1;
			else {
				i = c;
				j = c;
			}
		}
		return (double) i / (this.icdf.length - 1);
	}

	@Override
	public QuantileScaler copy() {
		return new QuantileScaler(this);
	}

	@Override
	public String toString() {
		return "Quantile Scaler";
	}

}
