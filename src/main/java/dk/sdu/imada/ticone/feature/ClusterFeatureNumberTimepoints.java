/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public class ClusterFeatureNumberTimepoints extends AbstractFeature<Integer> implements IClusterFeature<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8266354675638262958L;

	public ClusterFeatureNumberTimepoints() {
		super(ObjectType.CLUSTER, Integer.class);
	}

	@Override
	public ClusterFeatureNumberTimepoints copy() {
		return new ClusterFeatureNumberTimepoints();
	}

	@Override
	public String getName() {
		return "Number of Objects";
	}

	@Override
	public IFeatureValue<Integer> doCalculate(final IObjectWithFeatures cluster)
			throws IncorrectlyInitializedException, FeatureCalculationException {
		try {
			return value(cluster,
					PrototypeComponentType.TIME_SERIES
							.getComponent(ObjectType.CLUSTER.getBaseType().cast(cluster).getPrototype()).getTimeSeries()
							.asArray().length);
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			throw new FeatureCalculationException(e);
		}
	}
}
