/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.statistics.FeatureValueSamples;
import dk.sdu.imada.ticone.statistics.IFeatureValueSamples;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public class FeatureValueSampleManager implements IFeatureValueSampleManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7189828482250801703L;

	protected IObjectProvider provider;

	protected FeatureValueSamples fvSamples;

	/**
	 * 
	 */
	public FeatureValueSampleManager(final IObjectProvider provider) {
		super();
		this.provider = provider;
		this.fvSamples = new FeatureValueSamples(provider);
	}

	@Override
	public IFeatureValueSamples getFeatureValueSamples() {
		return this.fvSamples;
	}

	@Override
	public IObjectProvider getObjectProvider() {
		return this.provider;
	}
}
