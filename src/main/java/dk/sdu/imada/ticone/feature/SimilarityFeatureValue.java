/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.similarity.ISimilarityValue;

public class SimilarityFeatureValue extends FeatureValue<ISimilarityValue> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4021049253851181411L;

	SimilarityFeatureValue(final Class<? extends ISimilarityFeature> featureType, final ISimilarityValue simValue) {
		super(featureType, ISimilarityValue.class, simValue);
	}

	@Override
	public String toString() {
		if (!this.value.isCalculated())
			return "???";
		return this.value.toString();
	}
}