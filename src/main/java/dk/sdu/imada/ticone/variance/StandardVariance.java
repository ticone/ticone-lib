package dk.sdu.imada.ticone.variance;

import java.io.Serializable;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;

public class StandardVariance implements IObjectSetVariance, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5012276275537300007L;

	@Override
	public double calculateObjectSetVariance(final ITimeSeriesObject timeSeriesData) {
		double result = 0;

		for (final ITimeSeries signal : timeSeriesData.getOriginalTimeSeriesList()) {
			result += this.calculateVariance(signal.asArray());
		}
		result /= timeSeriesData.getOriginalTimeSeriesList().length;

		return result;
	}

	@Override
	public double calculateVariance(final double[] signal) {
		if (signal == null)
			return Double.NaN;
		double mean = 0;
		for (final double d : signal)
			mean += d;
		mean /= signal.length;

		double result = 0;
		for (final double d : signal)
			result += Math.pow(mean - d, 2);
		result /= signal.length;
		result = Math.sqrt(result);

		return result;
	}

}
