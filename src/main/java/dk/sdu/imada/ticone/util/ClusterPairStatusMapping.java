/**
 * 
 */
package dk.sdu.imada.ticone.util;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Dec 16, 2018
 *
 */
public class ClusterPairStatusMapping extends StatusMapping<IClusterPair> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5182779522245634444L;

	/**
	 * 
	 */
	public ClusterPairStatusMapping(final IFeatureStore featureStore) {
		super(featureStore);
	}

	@Override
	public IStatusComparator<IClusterPair> comparator() {
		return new StatusComparator();
	}

	class StatusComparator extends StatusMapping<IClusterPair>.StatusComparator {

	}
}
