/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Iterator;

import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;

public abstract class AbstractPairIterable<O1, O2, P extends IPair<O1, O2>> extends BoundIterable<P>
		implements IPairs<O1, O2, P>, IObjectProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1482701501218893044L;
	final protected O1[] firstObjects;
	final protected O2[] secondObjects;
	final protected PairIndexIterable pairIdxIt;
	final protected IPairBuilder<O1, O2, P> builder;
	private FeatureValueSampleManager featureValueSampleManager;
	private ObjectProviderManager objectProviderManager;

	/**
	 * 
	 */
	public AbstractPairIterable(final O1[] firstObjects, final O2[] secondObjects,
			final IPairBuilder<O1, O2, P> builder) {
		this(firstObjects, secondObjects, new PairIndexIterable(firstObjects.length, secondObjects.length), builder);
	}

	/**
	 * 
	 */
	public AbstractPairIterable(final O1[] firstObjects, final O2[] secondObjects, final PairIndexIterable pairIdxIt,
			final IPairBuilder<O1, O2, P> builder) {
		super(0, firstObjects.length * secondObjects.length);
		this.firstObjects = firstObjects;
		this.secondObjects = secondObjects;
		this.pairIdxIt = pairIdxIt;
		this.builder = builder;
		this.objectProviderManager = new ObjectProviderManager(this);
		this.featureValueSampleManager = new FeatureValueSampleManager(this);
	}

	@Override
	public IPairBuilder<O1, O2, P> getPairBuilder() {
		return this.builder;
	}

	@Override
	public PairIterator iterator() {
		return new PairIterator();
	}

	@Override
	public long longSize() {
		return this.pairIdxIt.longSize();
	}

	public P get(final long idx) {
		return this.get(IndexUtils.pairIndexToObjectListIndices(idx, this.secondObjects.length));
	}

	public P get(final int[] idx) {
		try {
			final O1 fst = getObject1(idx[0]);
			final O2 snd = getObject2(idx[1]);
			if (fst == null || snd == null)
				return null;
			return builder.setFirst(fst).setSecond(snd).build();
		} catch (FactoryException | CreateInstanceFactoryException | InterruptedException e) {
			// TODO
			return null;
		}
	}

	public O1 getObject1(final int x) {
		return this.firstObjects[x];
	}

	public O2 getObject2(final int y) {
		return this.secondObjects[y];
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}

	class PairIterator extends BoundIterator {
		Iterator<int[]> it = AbstractPairIterable.this.pairIdxIt.iterator();

		final IPairBuilder<O1, O2, P> builder = AbstractPairIterable.this.builder.copy();

		@Override
		public boolean hasNext() {
			return this.it.hasNext();
		}

		@Override
		public P next() {
			final int[] indices = this.it.next();
			try {
				final O1 fst = getObject1(indices[0]);
				final O2 snd = getObject2(indices[1]);
				if (fst == null || snd == null)
					return null;
				return builder.setFirst(fst).setSecond(snd).build();
			} catch (NullPointerException e) {
				e.printStackTrace();
				throw e;
			} catch (final InterruptedException | FactoryException | CreateInstanceFactoryException e) {
				// TODO
				return null;
			}
		}

	}

}