package dk.sdu.imada.ticone.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TiconeTask {

	protected transient Logger logger;
	protected List<TiconeTaskProgressListener> listener;

	protected boolean cancelled;

	public TiconeTask() {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.listener = new ArrayList<>();
	}

	public void addProgressListener(final TiconeTaskProgressListener l) {
		this.listener.add(l);
	}

	public void removeProgressListener(final TiconeTaskProgressListener listener) {
		this.listener.remove(listener);
	}

	protected void fireProgress(final String title, final String statusMessage, final Double percent) {
		final TiconeProgressEvent changeEvent = new TiconeProgressEvent(this, title, statusMessage, percent);
		for (final TiconeTaskProgressListener listener : this.listener)
			listener.progressUpdated(changeEvent);
	}

	public void clearProgressListener() {
		this.listener.clear();
	}

	public void cancel() {
		this.cancelled = true;
	}
}
