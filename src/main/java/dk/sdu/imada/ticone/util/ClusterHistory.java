package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * This class is used to save the state of the program. The
 * <tt>IClusterObjectMapping</tt> is kept for the given state, and a note
 * specifying what action was recently performed.
 *
 * @author Christian Wiwie
 * @author Christian Nørskov
 */
public class ClusterHistory implements Serializable, IClusterHistory<ClusterObjectMapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1135388420681759319L;
	private IClusterHistory<ClusterObjectMapping> parent;
	private final List<IClusterHistory<ClusterObjectMapping>> children;
	private final ClusterObjectMapping patternObjectMapping;
	private final ITimeSeriesObjects allObjects;
	private final int iterationNumber;
	private final String note;

	public ClusterHistory(final IClusterHistory<ClusterObjectMapping> parent, final ITimeSeriesObjects allObjects,
			final ClusterObjectMapping patternObjectMapping, final String note) {
		super();
		this.allObjects = Objects.requireNonNull(allObjects);
		this.patternObjectMapping = Objects.requireNonNull(patternObjectMapping);
		this.parent = parent;
		if (parent != null) {
			this.iterationNumber = parent.getIterationNumber() + 1;
			parent.addChild(this);
		} else {
			this.iterationNumber = 1;
		}
		this.children = new ArrayList<>();
		this.note = "#" + this.iterationNumber + " " + note;
	}

	/**
	 * @return the allObjects
	 */
	@Override
	public ITimeSeriesObjects getAllObjects() {
		return this.allObjects;
	}

	@Override
	public int getIterationNumber() {
		return this.iterationNumber;
	}

	@Override
	public ClusterObjectMapping getClusterObjectMapping() {
		return this.patternObjectMapping;
	}

	@Override
	public void addChild(final IClusterHistory<ClusterObjectMapping> patternHistory) {
		this.children.add(patternHistory);
	}

	@Override
	public IClusterHistory<ClusterObjectMapping> getParent() {
		return this.parent;
	}

	@Override
	public List<IClusterHistory<ClusterObjectMapping>> getChildren() {
		return this.children;
	}

	/**
	 * @param parent the parent to set
	 */
	@Override
	public void setParent(final IClusterHistory<ClusterObjectMapping> parent) {
		this.parent = parent;
	}

	@Override
	public String getNote() {
		return this.note;
	}

	@Override
	public String toString() {
		return this.getNote();
	}

}
