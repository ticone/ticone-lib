package dk.sdu.imada.ticone.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairsFactory;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValues;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesSomePairs;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueStorageException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.variance.IObjectSetVariance;

/**
 * Created by christian on 8/25/15.
 */
public class ExtractData {

	/**
	 * Finds the <tt>percent</tt> least fitting objects to all patterns (the
	 * globally least fitting).
	 * 
	 * @param patternObjectMapping
	 * @param percent
	 * @return
	 * @throws SimilarityCalculationException
	 * @throws IncomparableSimilarityValueException
	 * @throws InterruptedException
	 * @throws SimilarityValuesException
	 * @throws IncompatibleObjectTypeException
	 * @throws IncompatibleSimilarityValueException
	 * @throws IncompatibleSimilarityValueStorageException
	 */
	public static ITimeSeriesObjects getLeastFittingObjectsFromWholeDataset(
			final IClusterObjectMapping patternObjectMapping, final int percent,
			final ISimilarityFunction similarityFunction) throws SimilarityCalculationException, InterruptedException,
			SimilarityValuesException, IncompatibleObjectTypeException, IncompatibleSimilarityValueException,
			IncompatibleSimilarityValueStorageException {

		final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> similarities = similarityFunction
				.calculateSimilarities(patternObjectMapping.getObjectClusterPairs().asList(),
						ObjectType.OBJECT_CLUSTER_PAIR);
		final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sortedByValue = similarities
				.sortedByValue();

		long thresholdIndex = (int) ((sortedByValue.size() / (double) 100) * percent);

		thresholdIndex = Math.min(sortedByValue.size() - 1, thresholdIndex);
		final ISimilarityValue threshold = sortedByValue.get(new long[] { thresholdIndex })[0];

		final ITimeSeriesObjects leastFittingObjects = findLeastFittingObjectsWithThreshold(patternObjectMapping,
				threshold, similarityFunction);

		return leastFittingObjects;
	}

	public static ITimeSeriesObjects findLeastFittingObjectsWithThreshold(
			final IClusterObjectMapping patternObjectMapping, final ISimilarityValue threshold,
			final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		final ITimeSeriesObjects timeSeriesDatas = new TimeSeriesObjectList();
		for (final ICluster pattern : patternObjectMapping.getClusters()) {
			timeSeriesDatas.addAll(
					findObjectsFromPatternWithThreshold(pattern, patternObjectMapping, threshold, similarityFunction));
		}

		return timeSeriesDatas;
	}

	public static ITimeSeriesObjects findObjectsFromPatternWithThreshold(final ICluster pattern,
			final IClusterObjectMapping patternObjectMapping, final ISimilarityValue threshold,
			final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		final ITimeSeriesObjects timeSeriesDatas = new TimeSeriesObjectList();
		final ITimeSeriesObjects patternsData = pattern.getObjects();
		for (final ITimeSeriesObject timeSeriesData : patternsData) {
			final ISimilarityValue similarity = similarityFunction
					.calculateSimilarity(new ObjectClusterPair(timeSeriesData, pattern));
			if (similarity.lessThan(threshold)) {
				timeSeriesDatas.add(timeSeriesData);
			}
		}
		return timeSeriesDatas;
	}

	public static Pair<double[], int[]> getSimilarityHistogram(final IClusterObjectMapping patternObjectMapping,
			final ICluster pattern, final int numberBins, final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			NoComparableSimilarityValuesException, IncompatibleSimilarityValueException {
		ISimilarityValues<ITimeSeriesObject, ICluster, IObjectClusterPair> similarities;
		if (pattern == null) {
			similarities = similarityFunction.calculateSimilarities(
					patternObjectMapping.getObjectClusterPairs().asList(), ObjectType.OBJECT_CLUSTER_PAIR);
		} else {
			similarities = similarityFunction.calculateSimilarities(pattern.getObjects().toArray(),
					pattern.asSingletonList().toArray(), new ObjectClusterPairsFactory(),
					ObjectType.OBJECT_CLUSTER_PAIR);
		}

		// find min and max
		final double min = similarities.min().get();
		final double max = similarities.max().get();

		// determine bin width
		final double binWidth = (max - min) / numberBins;
		// ... and bins
		final double[] bins = new double[numberBins];
		for (int i = 0; i < numberBins; i++) {
			bins[i] = min + binWidth * i;
		}

		final int[] histogram = new int[numberBins];

		final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sortedByValue = similarities
				.sortedByValue();
		// similarities are sorted at this point
		int currentBin = 0;
		for (final ISimilarityValue p : sortedByValue) {
			// find bin
			while (currentBin < numberBins - 1 && p.get() >= bins[currentBin + 1]) {
				currentBin++;
			}
			histogram[currentBin]++;
		}

		return Pair.getPair(bins, histogram);
	}

	public static ITimeSeriesObjects findLeastVaryingObjects(final ITimeSeriesObjects objects, final int percent,
			final IObjectSetVariance varianceFunction) {

		final ITimeSeriesObjectList objectList = objects.asList();

		final List<Double> variances = getAllObjectsVariances(objectList, varianceFunction);

		final List<Pair<Double, ITimeSeriesObject>> sortedVariances = new ArrayList<>();
		for (int i = 0; i < objectList.size(); i++)
			sortedVariances.add(Pair.getPair(variances.get(i), objectList.get(i)));
		Collections.sort(sortedVariances, new Comparator<Pair<Double, ITimeSeriesObject>>() {
			@Override
			public int compare(final Pair<Double, ITimeSeriesObject> o1, final Pair<Double, ITimeSeriesObject> o2) {
				return o1.getFirst().compareTo(o2.getFirst());
			};
		});

		int thresholdIndex = (int) ((sortedVariances.size() / (double) 100) * percent);
		thresholdIndex = Math.max(Math.min(sortedVariances.size(), thresholdIndex), 0);

		final ITimeSeriesObjects leastFittingObjects = new TimeSeriesObjectList();
		for (int i = 0; i < thresholdIndex; i++) {
			leastFittingObjects.add(sortedVariances.get(i).getSecond());
		}

		return leastFittingObjects;
	}

	private static List<Double> getAllObjectsVariances(final ITimeSeriesObjectList objects,
			final IObjectSetVariance varianceFunction) {
		final List<Double> result = new ArrayList<>();
		for (final ITimeSeriesObject timeSeriesData : objects) {
			final double variance = varianceFunction.calculateObjectSetVariance(timeSeriesData);
			result.add(variance);
		}
		return result;
	}

	public static ITimeSeriesObjects findObjectsWithVariance(final ITimeSeriesObjects objectSets, final int percent,
			final IObjectSetVariance varianceFunction) {
		final List<Pair<ITimeSeriesObject, Double>> varianceList = new ArrayList<>();
		for (final ITimeSeriesObject timeSeriesData : objectSets) {
			final double variance = varianceFunction.calculateObjectSetVariance(timeSeriesData);
			varianceList.add(Pair.getPair(timeSeriesData, variance));
		}
		Collections.sort(varianceList, new Comparator<Pair<ITimeSeriesObject, Double>>() {
			@Override
			public int compare(final Pair<ITimeSeriesObject, Double> o1, final Pair<ITimeSeriesObject, Double> o2) {
				return o2.getSecond().compareTo(o1.getSecond());
			}
		});

		int thresholdIndex = (int) ((varianceList.size() / (double) 100) * percent);
		thresholdIndex = Math.max(Math.min(varianceList.size(), thresholdIndex), 0);

		final ITimeSeriesObjects timeSeriesDatas = new TimeSeriesObjectList();
		for (int i = 0; i < thresholdIndex; i++) {
			timeSeriesDatas.add(varianceList.get(i).getFirst());
		}
		return timeSeriesDatas;
	}

	public static ITimeSeriesObjects findObjectsWithVarianceThreshold(final ITimeSeriesObjects objectSets,
			final double threshold, final IObjectSetVariance varianceFunction) {
		final List<Pair<ITimeSeriesObject, Double>> varianceList = new ArrayList<>();
		for (final ITimeSeriesObject timeSeriesData : objectSets) {
			final double variance = varianceFunction.calculateObjectSetVariance(timeSeriesData);
			varianceList.add(Pair.getPair(timeSeriesData, variance));
		}
		Collections.sort(varianceList, new Comparator<Pair<ITimeSeriesObject, Double>>() {
			@Override
			public int compare(final Pair<ITimeSeriesObject, Double> o1, final Pair<ITimeSeriesObject, Double> o2) {
				return o2.getSecond().compareTo(o1.getSecond());
			}
		});

		final ITimeSeriesObjects timeSeriesDatas = new TimeSeriesObjectList();
		for (int i = 0; i < varianceList.size(); i++) {
			final double variance = varianceList.get(i).getSecond();
			if (variance < threshold) {
				timeSeriesDatas.add(varianceList.get(i).getFirst());
			}
		}
		/*
		 * for (TimeSeriesData timeSeriesData : objectSets) { double variance =
		 * varianceFunction.calculateObjectSetVariance(timeSeriesData); if (variance <
		 * threshold) { timeSeriesDatas.add(timeSeriesData); } }
		 */
		return timeSeriesDatas;
	}

	/**
	 * Find the <code>percent</code>% least fitting objects to specified pattern.
	 * 
	 * @param patternObjectMapping
	 * @param pattern
	 * @param percent
	 * @return
	 * @throws SimilarityCalculationException
	 * @throws IncomparableSimilarityValueException
	 * @throws InterruptedException
	 * @throws SimilarityValuesException
	 * @throws IncompatibleObjectTypeException
	 * @throws IncompatibleSimilarityValueException
	 * @throws IncompatibleSimilarityValueStorageException
	 */
	public static ITimeSeriesObjects getLeastFittingDataFromPattern(final IClusterObjectMapping patternObjectMapping,
			final ICluster pattern, final int percent, final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleObjectTypeException, IncompatibleSimilarityValueException,
			IncompatibleSimilarityValueStorageException {
		final ISimilarityValue similarityThreshold = findSimilarityThreshold(patternObjectMapping, pattern, percent,
				similarityFunction);
		if (similarityThreshold == null) {
			return null;
		}

		final ITimeSeriesObjects objectsToExtract = findObjectsFromPatternWithThreshold(pattern, patternObjectMapping,
				similarityThreshold, similarityFunction);

		return objectsToExtract;
	}

	/**
	 *
	 * @param patternObjectMapping
	 * @param pattern
	 * @param percent
	 * @return
	 * @throws SimilarityCalculationException
	 * @throws IncomparableSimilarityValueException
	 * @throws InterruptedException
	 * @throws SimilarityValuesException
	 * @throws IncompatibleSimilarityValueException
	 * @throws IncompatibleSimilarityValueStorageException
	 */
	private static ISimilarityValue findSimilarityThreshold(final IClusterObjectMapping patternObjectMapping,
			final ICluster pattern, final int percent, final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleSimilarityValueException, IncompatibleSimilarityValueStorageException {
		final ITimeSeriesObjects objects = pattern.getObjects();
		if (objects == null || objects.size() == 0) {
			return null;
		}

		final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> comparableSims = similarityFunction
				
				.calculateSimilarities(objects.toArray(), pattern.asSingletonList().toArray(),
						new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR)
				.sortedByValue();

		long numberOfObjectsToExtract = (int) ((objects.size() / 100.0) * percent);
		numberOfObjectsToExtract = Math.min(comparableSims.size() - 1, numberOfObjectsToExtract);
		if (numberOfObjectsToExtract < 1) {
			numberOfObjectsToExtract = 1;
		}
		// Rounding errors while using double, so to ensure to get all objects,
		// when 100 percent
		if (percent == 100) {
			numberOfObjectsToExtract = comparableSims.size();
		}
		return comparableSims.get(new long[] { (comparableSims.size() - numberOfObjectsToExtract) - 1 })[0];
	}

	// TODO: this one does only remove 10% of the objects, also in case of tie
	// similarities. problem?
	public static ITimeSeriesObjects findLeastConservedObjects(final ITimeSeriesObjects objectSets, final int percent,
			final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		if (objectSets.size() > 1 && objectSets.iterator().next().getOriginalTimeSeriesList().length == 1) {
			return new TimeSeriesObjectList();
		}

		final List<Pair<ITimeSeriesObject, ISimilarityValue>> similarities = getSortedObjectConformities(objectSets,
				similarityFunction);

		int thresholdIndex = (int) ((similarities.size() / (double) 100) * percent);
		thresholdIndex = Math.max(Math.min(similarities.size(), thresholdIndex), 0);

		final ITimeSeriesObjects leastFittingObjects = new TimeSeriesObjectSet();
		for (int i = 0; i < thresholdIndex; i++) {
			leastFittingObjects.add(similarities.get(i).getFirst());
		}

		return leastFittingObjects;
	}

	public static ITimeSeriesObjects findLeastConservedObjectsWithThreshold(final ITimeSeriesObjects objectSets,
			final ISimilarityValue threshold, final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		if (objectSets.size() > 1 && objectSets.iterator().next().getOriginalTimeSeriesList().length == 1) {
			return new TimeSeriesObjectSet();
		}
		final List<Pair<ITimeSeriesObject, ISimilarityValue>> similarities = getSortedObjectConformities(objectSets,
				similarityFunction);

		/**
		 * This will make them appear in order (good for showing "most" conserved of
		 * these.
		 */
		Collections.sort(similarities, new Comparator<Pair<ITimeSeriesObject, ISimilarityValue>>() {
			@Override
			public int compare(final Pair<ITimeSeriesObject, ISimilarityValue> o1,
					final Pair<ITimeSeriesObject, ISimilarityValue> o2) {
				return o2.getSecond().compareTo(o1.getSecond());
			}
		});

		final ITimeSeriesObjects leastFittingObjects = new TimeSeriesObjectSet();
		for (int i = 0; i < similarities.size(); i++) {
			if (similarities.get(i).getSecond().compareTo(threshold) < 0)
				leastFittingObjects.add(similarities.get(i).getFirst());
		}

		return leastFittingObjects;
	}

	private static List<Pair<ITimeSeriesObject, ISimilarityValue>> getSortedObjectConformities(
			final ITimeSeriesObjects objectSets, final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		final List<Pair<ITimeSeriesObject, ISimilarityValue>> similarities = new ArrayList<>();
		final Iterator<ITimeSeriesObject> objectSetIterator = objectSets.iterator();
		while (objectSetIterator.hasNext()) {
			final ITimeSeriesObject object = objectSetIterator.next();
			similarities.add(Pair.getPair(object, findObjectConformity(object, similarityFunction)));
		}
		Collections.sort(similarities, new Comparator<Pair<ITimeSeriesObject, ISimilarityValue>>() {
			@Override
			public int compare(final Pair<ITimeSeriesObject, ISimilarityValue> o1,
					final Pair<ITimeSeriesObject, ISimilarityValue> o2) {
				return o1.getSecond().compareTo(o2.getSecond());
			}
		});
		return similarities;
	}

	// take the average similarity of all pairs of time-series
	// TODO: could be a performance problem with many samples
	public static ISimilarityValue findObjectConformity(final ITimeSeriesObject object,
			final ISimilarityFunction similarityFunction)
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		return similarityFunction.calculateSimilarity(new ObjectPair(object, object));
	}
}
