package dk.sdu.imada.ticone.util;

import java.io.Serializable;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitClusterContainer;
import dk.sdu.imada.ticone.clustering.suggestclusters.IClusterSuggestion;
import dk.sdu.imada.ticone.clustering.suggestclusters.ISuggestNewCluster;

/**
 * Created by christian on 13-10-15.
 */
public class ActionContainer implements Serializable, IActionContainer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4495883334570911518L;

	private final ACTION_TYPE actionType;

	private ISplitCluster splitPattern = null;
	private ISplitClusterContainer splitPatternContainer = null;

	private final ISuggestNewCluster suggestNewPattern = null;
	private final IClusterSuggestion clusteringTransition = null;

	private ICluster patternToDelete = null;
	private IClusterObjectMapping.DELETE_METHOD deleteMethod;

	public ActionContainer(final ICluster patternToDelete, final IClusterObjectMapping.DELETE_METHOD deleteMethod) {
		this.actionType = ACTION_TYPE.DELETE_PATTERN;
		this.patternToDelete = patternToDelete;
		this.deleteMethod = deleteMethod;
	}

	public ActionContainer(final ISplitCluster splitPattern, final ISplitClusterContainer splitPatternContainer) {
		this.actionType = ACTION_TYPE.SPLIT_PATTERN;
		this.splitPattern = splitPattern;
		this.splitPatternContainer = splitPatternContainer;
	}

	@Override
	public ACTION_TYPE getActionType() {
		return this.actionType;
	}

	@Override
	public ISplitCluster getSplitPattern() {
		if (this.actionType != ACTION_TYPE.SPLIT_PATTERN) {
			return null;
		}
		return this.splitPattern;
	}

	@Override
	public ISplitClusterContainer getSplitPatternContainer() {
		if (this.actionType != ACTION_TYPE.SPLIT_PATTERN) {
			return null;
		}
		return this.splitPatternContainer;
	}

	@Override
	public ICluster getPatternToDelete() {
		if (this.actionType != ACTION_TYPE.DELETE_PATTERN) {
			return null;
		}
		return this.patternToDelete;
	}

	@Override
	public IClusterObjectMapping.DELETE_METHOD getDeleteType() {
		if (this.actionType != ACTION_TYPE.DELETE_PATTERN) {
			return null;
		}
		return this.deleteMethod;
	}

	@Override
	public IClusterSuggestion getClusteringTransition() {
		if (this.actionType != ACTION_TYPE.SUGGEST_PATTERN) {
			return null;
		}
		return this.clusteringTransition;
	}

	@Override
	public ISuggestNewCluster getSuggestNewPattern() {
		if (this.actionType != ACTION_TYPE.SUGGEST_PATTERN) {
			return null;
		}
		return this.suggestNewPattern;
	}
}
