/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public class ObjectProviderManager implements IObjectProviderManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3445327643740810693L;
	protected IObjectProvider provider;
	/**
	 * 
	 */
	protected Set<IObjectProviderListener> oProviderListener;

	/**
	 * 
	 */
	public ObjectProviderManager(final IObjectProvider provider) {
		super();
		this.provider = provider;
		this.oProviderListener = new HashSet<>();
	}

	/**
	 * @return the provider
	 */
	public IObjectProvider getProvider() {
		return this.provider;
	}

	@Override
	public void addObjectProviderListener(final IObjectProviderListener listener) {
		this.oProviderListener.add(listener);

	}

	@Override
	public void fireObjectProviderChanged() {
		final ObjectProviderChangeEvent e = new ObjectProviderChangeEvent(this.provider);
		for (final IObjectProviderListener l : this.oProviderListener)
			l.objectProviderChanged(e);
	}

	@Override
	public void removeObjectProviderListener(final IObjectProviderListener listener) {
		this.oProviderListener.remove(listener);
	}

}
