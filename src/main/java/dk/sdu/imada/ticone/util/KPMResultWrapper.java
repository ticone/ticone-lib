package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.network.kpm.Result;

public class KPMResultWrapper implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5475939312817668334L;
	protected String networkName;
	protected List<Result> results;
	protected Collection<IClusters> clusters;
	protected String kpmModel;

	public KPMResultWrapper(final String networkName, final List<Result> results, final Collection<IClusters> clusters,
			final String kpmModel) {
		super();
		this.networkName = networkName;
		this.results = results;
		this.clusters = clusters;
		this.kpmModel = kpmModel;
	}

	public Collection<IClusters> getClusters() {
		return this.clusters;
	}

	public String getNetworkName() {
		return this.networkName;
	}

	public List<Result> getResults() {
		return this.results;
	}

	public String getKpmModel() {
		return this.kpmModel;
	}
}
