/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterSet;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 15, 2018
 *
 */
public class ClusterStatusMapping extends StatusMapping<ICluster> implements IClusterStatusMapping {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4492134085141415831L;
	protected final Set<ICluster> splitClusters;
	protected final Map<Integer, IClusters> children;
	protected final IClusterSet clustersToShow;

	/**
	 * 
	 */
	public ClusterStatusMapping(final IFeatureStore featureStore) {
		super(featureStore);
		this.clustersToShow = new ClusterSet();
		this.splitClusters = new HashSet<>();
		this.children = new HashMap<>();
	}

	@Override
	public void addCluster(final ICluster pattern, final ITimeSeriesObjects patternsData, final boolean newCluster,
			final boolean splitCluster, final boolean filteredCluster) {
		this.add(pattern, newCluster, false, filteredCluster);
		this.clustersToShow.add(pattern);
		if (splitCluster)
			this.splitClusters.add(pattern);

		if (newCluster && pattern.getParent() != null) {
			final ICluster parent = pattern.getParent();
			if (!this.children.containsKey(parent.getClusterNumber())) {
				this.children.put(parent.getClusterNumber(), new ClusterList());
			}
			this.children.get(parent.getClusterNumber()).add(pattern);
		}

	}

	@Override
	public IClusters getChildren(final ICluster pattern) {
		if (this.children.containsKey(pattern.getClusterNumber())) {
			return this.children.get(pattern.getClusterNumber());
		}
		return null;
	}

	@Override
	public IClusters getClustersToShow() {
		return this.clustersToShow;
	}

	@Override
	public Collection<ICluster> getSplitClusters() {
		return Collections.unmodifiableSet(this.splitClusters);
	}

	@Override
	public IStatusComparator<ICluster> comparator() {
		return new StatusComparator();
	}

	class StatusComparator extends StatusMapping<ICluster>.StatusComparator {

		@Override
		protected int getPriority(final ICluster object) {
			if (splitClusters != null && splitClusters.contains(object))
				return 2;
			return super.getPriority(object);
		}

	}
}
