/**
 * 
 */
package dk.sdu.imada.ticone.util;

public class PairIndexIterable extends BoundIterable<int[]> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2141627880224532370L;
	final protected int firstObjects, secondObjects;

	/**
	 * @param similarityValuesAllPairs TODO
	 * 
	 */
	public PairIndexIterable(final int firstObjects, final int secondObjects) {
		this(firstObjects, secondObjects, 0, firstObjects * secondObjects);
	}

	/**
	 * @param start
	 * @param end
	 * @param similarityValuesAllPairs TODO
	 */
	public PairIndexIterable(final int firstObjects, final int secondObjects, final long start, final long end) {
		super(start, end);
		this.firstObjects = firstObjects;
		this.secondObjects = secondObjects;

	}

	@Override
	public BoundIterator iterator() {
		return new IndexIterator();
	}

	@Override
	public PairIndexIterable range(final long start, final long end) {
		return new PairIndexIterable(firstObjects, secondObjects, start, end);
	}

	class IndexIterator extends BoundIterator {

		// indices of the next element to be returned
		int nextX, nextY;

		// indices of the first element not to be returned
		int endX, endY;

		/**
		 * 
		 */
		public IndexIterator() {
			if (secondObjects < 1)
				return;
			final int[] startIdx = IndexUtils.pairIndexToObjectListIndices(start, secondObjects);
			if (startIdx == null)
				return;

			this.nextX = startIdx[0];
			this.nextY = startIdx[1];

			final int[] endIdx = IndexUtils.pairIndexToObjectListIndices(end, secondObjects);
			if (endIdx == null)
				return;

			this.endX = endIdx[0];
			this.endY = endIdx[1];
		}

		@Override
		public boolean hasNext() {
			return this.nextX < this.endX || this.nextY < this.endY;
		}

		@Override
		public int[] next() {
			final int[] result = new int[] { this.nextX, this.nextY };

			this.nextY++;
			if (this.nextY >= secondObjects) {
				this.nextX++;
				this.nextY = 0;
			}
			return result;
		}

	}
}