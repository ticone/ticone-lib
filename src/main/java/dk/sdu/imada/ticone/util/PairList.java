/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.ArrayList;
import java.util.Collection;

import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 23, 2019
 *
 * @param <O1>
 * @param <O2>
 * @param <P>
 */
public abstract class PairList<O1, O2, P extends IPair<O1, O2>> extends ArrayList<P>
		implements IObjectProvider, IPairCollection<O1, O2, P> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8719487926941704677L;
	private FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);
	private ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);
	final protected IPairBuilder<O1, O2, P> builder;

	protected PairList(final IPairBuilder<O1, O2, P> builder) {
		super();
		this.builder = builder;
		this.objectProviderManager = new ObjectProviderManager(this);
		this.featureValueSampleManager = new FeatureValueSampleManager(this);
	}

	protected PairList(final Collection<? extends P> c, final IPairBuilder<O1, O2, P> builder) {
		this(builder);
		this.addAll(c);
	}

	protected PairList(final PairList<O1, O2, P> other) {
		this(other, other.builder);
	}

	@Override
	public IPairBuilder<O1, O2, P> getPairBuilder() {
		return this.builder;
	}

	@Override
	public P[] toArray() {
		return (P[]) super.toArray();
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}
}
