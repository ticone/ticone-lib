package dk.sdu.imada.ticone.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import dk.sdu.imada.ticone.clustering.IStatusMappingListener;
import dk.sdu.imada.ticone.clustering.StatusMappingEvent;
import dk.sdu.imada.ticone.clustering.filter.FilterEvent;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.clustering.filter.IFilterListener;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Dec 15, 2018
 *
 * @param <T>
 */
public abstract class StatusMapping<T extends IObjectWithFeatures> implements IStatusMapping<T>, IFilterListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6452348645146020557L;
	protected final Set<T> allKnownObjects;
	protected final Set<T> newObjects;
	protected final Set<T> deletedObjects;
	protected IFilter<T> filter;
	protected final Set<T> filteredObjects;
	protected final IFeatureStore featureStore;

	private transient Set<IStatusMappingListener> listener;

	public StatusMapping(final IFeatureStore featureStore) {
		super();
		this.featureStore = featureStore;
		this.allKnownObjects = new HashSet<>();
		this.newObjects = new HashSet<>();
		this.deletedObjects = new HashSet<>();
		this.filteredObjects = new HashSet<>();
		this.listener = new HashSet<>();
	}

	public IStatusMapping<T> addToKnownObjects(final Collection<T> objects) {
		allKnownObjects.addAll(objects);
		return this;
	}

	@Override
	public void add(final T object, final boolean isNew, final boolean isDeleted, final boolean isFiltered) {
		this.allKnownObjects.add(object);
		if (isNew)
			this.newObjects.add(object);
		if (isDeleted)
			this.deletedObjects.add(object);
		if (isFiltered)
			this.filteredObjects.add(object);
	}

	/**
	 * @return the featureStore
	 */
	@Override
	public IFeatureStore getFeatureStore() {
		return this.featureStore;
	}

	/**
	 * @return the listener
	 */
	public Set<IStatusMappingListener> getListener() {
		if (this.listener == null)
			this.listener = new HashSet<>();
		return this.listener;
	}

	@Override
	public Collection<T> getNew() {
		return Collections.unmodifiableSet(newObjects);
	}

	@Override
	public Collection<T> getDeleted() {
		return Collections.unmodifiableSet(deletedObjects);
	}

	@Override
	public void setDeleted(final T object) {
		allKnownObjects.add(object);
		deletedObjects.add(object);
	}

	@Override
	public void setNew(final T object) {
		allKnownObjects.add(object);
		newObjects.add(object);
	}

	@Override
	public Collection<T> getFiltered() {
		return Collections.unmodifiableSet(this.filteredObjects);
	}

	@Override
	public boolean isFilterActive() {
		return this.filter != null && this.filter.isActive();
	}

	@Override
	public void filterChanged(final FilterEvent e) {
		this.filter = (IFilter<T>) e.getFilter();
		this.filteredObjects.clear();
		for (final T c : this.allKnownObjects) {
			if (!this.filter.check(this.featureStore, c))
				this.filteredObjects.add(c);
		}
		this.fireStatusMappingChanged(new StatusMappingEvent(this));
	}

	@Override
	public void addStatusMappingListener(final IStatusMappingListener l) {
		this.getListener().add(l);
	}

	@Override
	public void removeStatusMappingListener(final IStatusMappingListener l) {
		this.getListener().remove(l);
	}

	@Override
	public void fireStatusMappingChanged(final StatusMappingEvent e) {
		for (final IStatusMappingListener l : new ArrayList<>(this.listener))
			l.clusterStatusMappingChanged(e);
	}

	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();

		this.listener = new HashSet<>();
	}

	abstract class StatusComparator implements IStatusComparator<T> {

		protected boolean decreasing;
		protected boolean reverseComparator;

		@Override
		public int compare(T o1, T o2) {
			final int prio1 = this.getPriority(o1);
			final int prio2 = this.getPriority(o2);

			if (prio1 != prio2)
				return Integer.signum(this.decreasing && this.reverseComparator ? prio1 - prio2 : prio2 - prio1);
			return 0;
		}

		protected int getPriority(final T object) {
			if (newObjects != null && newObjects.contains(object))
				return 4;
			else if (deletedObjects != null && deletedObjects.contains(object))
				return 3;
			else if (filteredObjects != null && filteredObjects.contains(object))
				return -1;
			else
				return 0;
		}

		/**
		 * @return the decreasing
		 */
		@Override
		public boolean isDecreasing() {
			return this.decreasing;
		}

		/**
		 * @param decreasing the decreasing to set
		 */
		@Override
		public StatusComparator setDecreasing(final boolean decreasing) {
			this.decreasing = decreasing;
			return this;
		}

		/**
		 * @param reverseComparator the reverseComparator to set
		 * @return
		 */
		@Override
		public StatusComparator setReverse(final boolean reverseComparator) {
			this.reverseComparator = reverseComparator;
			return this;
		}

	}
}
