
/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Collection;
import java.util.HashSet;

import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public abstract class PairSet<O1, O2, P extends IPair<O1, O2>> extends HashSet<P>
		implements IObjectProvider, IPairCollection<O1, O2, P> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2337313230471977215L;

	private ObjectProviderManager objectProviderManager;
	private FeatureValueSampleManager featureValueSampleManager;

	final protected IPairBuilder<O1, O2, P> builder;

	protected PairSet(final IPairBuilder<O1, O2, P> builder) {
		super();
		this.builder = builder;
		this.objectProviderManager = new ObjectProviderManager(this);
		this.featureValueSampleManager = new FeatureValueSampleManager(this);
	}

	protected PairSet(final IPairCollection<O1, O2, P> other) {
		this(other, other.getPairBuilder());
	}

	protected PairSet(final Collection<? extends P> pairs, final IPairBuilder<O1, O2, P> builder) {
		this(builder);
		this.addAll(pairs);
	}

	protected PairSet(final IPairBuilder<O1, O2, P> builder, final P... pairs) {
		this(builder);
		for (final P o : pairs)
			this.add(o);
	}

	@Override
	public IPairBuilder<O1, O2, P> getPairBuilder() {
		return this.builder;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public P[] toArray() {
		return (P[]) super.toArray();
	}
}
