package dk.sdu.imada.ticone.util;

import java.util.EventObject;

public class TiconeProgressEvent extends EventObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5670028342131030211L;
	protected String title;
	protected String statusMessage;
	protected Double percent;

	/**
	 * Null values mean no change;
	 * 
	 * @param source
	 * @param title
	 * @param statusMessage
	 * @param percent
	 */
	public TiconeProgressEvent(final Object source, final String title, final String statusMessage,
			final Double percent) {
		super(source);
		this.title = title;
		this.statusMessage = statusMessage;
		this.percent = percent;
	}

	public String getTitle() {
		return this.title;
	}

	public Double getPercent() {
		return this.percent;
	}

	public String getStatusMessage() {
		return this.statusMessage;
	}
}
