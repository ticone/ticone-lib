/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 20, 2017
 *
 */
public abstract class AbstractTiconeResult implements ITiconeResult, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2253060966537406108L;

	protected transient Logger logger;
	protected Date creationDate;
	private String ticoneVersion;
	protected transient Set<ITiconeResultDestroyedListener> removeListener;
	protected transient Set<ITiconeResultChangeListener> changeListener;

	/**
	 * 
	 */
	public AbstractTiconeResult() {
		super();
		this.removeListener = new HashSet<>();
		this.changeListener = new HashSet<>();
		this.ticoneVersion = "ver: " + this.getClass().getPackage().getImplementationVersion();
		this.logger = LoggerFactory.getLogger(this.getClass());
	}

	public AbstractTiconeResult(final AbstractTiconeResult result) {
		super();
		this.removeListener = result.removeListener;
		this.changeListener = result.changeListener;
		this.ticoneVersion = result.ticoneVersion;
		this.logger = result.logger;
	}

	public void setDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public Date getDate() {
		return this.creationDate;
	}

	public void setTiconeVersion(final String ticoneVersion) {
		this.ticoneVersion = ticoneVersion;
	}

	@Override
	public String getTiconeVersion() {
		return this.ticoneVersion;
	}

	@Override
	public void addChangeListener(final ITiconeResultChangeListener listener) {
		this.changeListener.add(listener);
	}

	@Override
	public void removeChangeListener(final ITiconeResultChangeListener listener) {
		this.changeListener.remove(listener);
	}

	@Override
	public void addOnDestroyListener(ITiconeResultDestroyedListener listener) {
		this.removeListener.add(listener);
	}

	@Override
	public void removeOnDestroyListener(ITiconeResultDestroyedListener listener) {
		this.removeListener.remove(listener);
	}

	protected void fireStateChanged() {
		final TiconeResultChangeEvent changeEvent = new TiconeResultChangeEvent(this);
		for (final ITiconeResultChangeListener listener : new HashSet<>(this.changeListener))
			listener.resultChanged(changeEvent);
	}

	protected void fireDestroyed() {
		final TiconeResultDestroyedEvent event = new TiconeResultDestroyedEvent(this);
		for (final ITiconeResultDestroyedListener listener : new HashSet<>(this.removeListener))
			listener.ticoneResultDestroyed(event);
	}

	/**
	 * @return the changeListener
	 */
	public Set<ITiconeResultChangeListener> getChangeListener() {
		return this.changeListener;
	}

	public void clearListener() {
		if (this.changeListener == null)
			this.changeListener = new HashSet<>();
		else
			this.changeListener.clear();

		if (this.removeListener == null)
			this.removeListener = new HashSet<>();
		else
			this.removeListener.clear();
	}

	@Override
	public void destroy() {
		this.fireDestroyed();
		this.clearListener();
	}

	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.changeListener = new HashSet<>();
		this.removeListener = new HashSet<>();
	}
}
