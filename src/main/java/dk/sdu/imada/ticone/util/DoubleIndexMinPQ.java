/******************************************************************************
 *  Compilation:  javac IndexMinPQ.java
 *  Execution:    java IndexMinPQ
 *  Dependencies: StdOut.java
 *
 *  Minimum-oriented indexed PQ implementation using a binary heap.
 *
 ******************************************************************************/

package dk.sdu.imada.ticone.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.IndexMinPQ;

/**
 * Modified version of {@link IndexMinPQ} with primitive double type.
 * 
 * @see IndexMinPQ
 */
public class DoubleIndexMinPQ implements Iterable<Integer> {
	private final int maxN; // maximum number of elements on PQ
	private int n; // number of elements on PQ
	private final int[] pq; // binary heap using 1-based indexing
	private final int[] qp; // inverse of pq - qp[pq[i]] = pq[qp[i]] = i
	private final double[] keys; // keys[i] = priority of i

	/**
	 * Initializes an empty indexed priority queue with indices between {@code 0}
	 * and {@code maxN - 1}.
	 * 
	 * @param maxN the keys on this priority queue are index from {@code 0}
	 *             {@code maxN - 1}
	 * @throws IllegalArgumentException if {@code maxN < 0}
	 */
	public DoubleIndexMinPQ(final int maxN) {
		if (maxN < 0)
			throw new IllegalArgumentException();
		this.maxN = maxN;
		this.n = 0;
		this.keys = new double[maxN + 1]; // make this of length maxN??
		this.pq = new int[maxN + 1];
		this.qp = new int[maxN + 1]; // make this of length maxN??
		for (int i = 0; i <= maxN; i++)
			this.qp[i] = -1;
	}

	/**
	 * Returns true if this priority queue is empty.
	 *
	 * @return {@code true} if this priority queue is empty; {@code false} otherwise
	 */
	public boolean isEmpty() {
		return this.n == 0;
	}

	/**
	 * Is {@code i} an index on this priority queue?
	 *
	 * @param i an index
	 * @return {@code true} if {@code i} is an index on this priority queue;
	 *         {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
	 */
	public boolean contains(final int i) {
		if (i < 0 || i >= this.maxN)
			throw new IllegalArgumentException();
		return this.qp[i] != -1;
	}

	/**
	 * Returns the number of keys on this priority queue.
	 *
	 * @return the number of keys on this priority queue
	 */
	public int size() {
		return this.n;
	}

	/**
	 * Associates key with index {@code i}.
	 *
	 * @param i   an index
	 * @param key the key to associate with index {@code i}
	 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
	 * @throws IllegalArgumentException if there already is an item associated with
	 *                                  index {@code i}
	 */
	public void insert(final int i, final double key) {
		if (i < 0 || i >= this.maxN)
			throw new IllegalArgumentException();
		if (this.contains(i))
			throw new IllegalArgumentException("index is already in the priority queue");
		this.n++;
		this.qp[i] = this.n;
		this.pq[this.n] = i;
		this.keys[i] = key;
		this.swim(this.n);
	}

	/**
	 * Returns an index associated with a minimum key.
	 *
	 * @return an index associated with a minimum key
	 * @throws NoSuchElementException if this priority queue is empty
	 */
	public int minIndex() {
		if (this.n == 0)
			throw new NoSuchElementException("Priority queue underflow");
		return this.pq[1];
	}

	/**
	 * Returns a minimum key.
	 *
	 * @return a minimum key
	 * @throws NoSuchElementException if this priority queue is empty
	 */
	public double minKey() {
		if (this.n == 0)
			throw new NoSuchElementException("Priority queue underflow");
		return this.keys[this.pq[1]];
	}

	/**
	 * Removes a minimum key and returns its associated index.
	 * 
	 * @return an index associated with a minimum key
	 * @throws NoSuchElementException if this priority queue is empty
	 */
	public int delMin() {
		if (this.n == 0)
			throw new NoSuchElementException("Priority queue underflow");
		final int min = this.pq[1];
		this.exch(1, this.n--);
		this.sink(1);
		assert min == this.pq[this.n + 1];
		this.qp[min] = -1; // delete
		this.keys[min] = Double.POSITIVE_INFINITY;
		this.pq[this.n + 1] = -1; // not needed
		return min;
	}

	/**
	 * Returns the key associated with index {@code i}.
	 *
	 * @param i the index of the key to return
	 * @return the key associated with index {@code i}
	 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
	 * @throws NoSuchElementException   no key is associated with index {@code i}
	 */
	public double keyOf(final int i) {
		if (i < 0 || i >= this.maxN)
			throw new IllegalArgumentException();
		if (!this.contains(i))
			throw new NoSuchElementException("index is not in the priority queue");
		else
			return this.keys[i];
	}

	/**
	 * Change the key associated with index {@code i} to the specified value.
	 *
	 * @param i   the index of the key to change
	 * @param key change the key associated with index {@code i} to this key
	 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
	 * @throws NoSuchElementException   no key is associated with index {@code i}
	 */
	public void changeKey(final int i, final double key) {
		if (i < 0 || i >= this.maxN)
			throw new IllegalArgumentException();
		if (!this.contains(i))
			throw new NoSuchElementException("index is not in the priority queue");
		this.keys[i] = key;
		this.swim(this.qp[i]);
		this.sink(this.qp[i]);
	}

	/**
	 * Change the key associated with index {@code i} to the specified value.
	 *
	 * @param i   the index of the key to change
	 * @param key change the key associated with index {@code i} to this key
	 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
	 * @deprecated Replaced by {@code changeKey(int, Key)}.
	 */
	@Deprecated
	public void change(final int i, final double key) {
		this.changeKey(i, key);
	}

	/**
	 * Decrease the key associated with index {@code i} to the specified value.
	 *
	 * @param i   the index of the key to decrease
	 * @param key decrease the key associated with index {@code i} to this key
	 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
	 * @throws IllegalArgumentException if {@code key >= keyOf(i)}
	 * @throws NoSuchElementException   no key is associated with index {@code i}
	 */
	public void decreaseKey(final int i, final double key) {
		if (i < 0 || i >= this.maxN)
			throw new IllegalArgumentException();
		if (!this.contains(i))
			throw new NoSuchElementException("index is not in the priority queue");
		if (Double.compare(this.keys[i], key) <= 0)
			throw new IllegalArgumentException(
					"Calling decreaseKey() with given argument would not strictly decrease the key");
		this.keys[i] = key;
		this.swim(this.qp[i]);
	}

	/**
	 * Increase the key associated with index {@code i} to the specified value.
	 *
	 * @param i   the index of the key to increase
	 * @param key increase the key associated with index {@code i} to this key
	 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
	 * @throws IllegalArgumentException if {@code key <= keyOf(i)}
	 * @throws NoSuchElementException   no key is associated with index {@code i}
	 */
	public void increaseKey(final int i, final double key) {
		if (i < 0 || i >= this.maxN)
			throw new IllegalArgumentException();
		if (!this.contains(i))
			throw new NoSuchElementException("index is not in the priority queue");
		if (Double.compare(this.keys[i], key) >= 0)
			throw new IllegalArgumentException(
					"Calling increaseKey() with given argument would not strictly increase the key");
		this.keys[i] = key;
		this.sink(this.qp[i]);
	}

	/**
	 * Remove the key associated with index {@code i}.
	 *
	 * @param i the index of the key to remove
	 * @throws IllegalArgumentException unless {@code 0 <= i < maxN}
	 * @throws NoSuchElementException   no key is associated with index {@code i}
	 */
	public void delete(final int i) {
		if (i < 0 || i >= this.maxN)
			throw new IllegalArgumentException();
		if (!this.contains(i))
			throw new NoSuchElementException("index is not in the priority queue");
		final int index = this.qp[i];
		this.exch(index, this.n--);
		this.swim(index);
		this.sink(index);
		this.keys[i] = Double.POSITIVE_INFINITY;
		this.qp[i] = -1;
	}

	/***************************************************************************
	 * General helper functions.
	 ***************************************************************************/
	private boolean greater(final int i, final int j) {
		return Double.compare(this.keys[this.pq[i]], this.keys[this.pq[j]]) > 0;
	}

	private void exch(final int i, final int j) {
		final int swap = this.pq[i];
		this.pq[i] = this.pq[j];
		this.pq[j] = swap;
		this.qp[this.pq[i]] = i;
		this.qp[this.pq[j]] = j;
	}

	/***************************************************************************
	 * Heap helper functions.
	 ***************************************************************************/
	private void swim(int k) {
		while (k > 1 && this.greater(k / 2, k)) {
			this.exch(k, k / 2);
			k = k / 2;
		}
	}

	private void sink(int k) {
		while (2 * k <= this.n) {
			int j = 2 * k;
			if (j < this.n && this.greater(j, j + 1))
				j++;
			if (!this.greater(k, j))
				break;
			this.exch(k, j);
			k = j;
		}
	}

	/***************************************************************************
	 * Iterators.
	 ***************************************************************************/

	/**
	 * Returns an iterator that iterates over the keys on the priority queue in
	 * ascending order. The iterator doesn't implement {@code remove()} since it's
	 * optional.
	 *
	 * @return an iterator that iterates over the keys in ascending order
	 */
	@Override
	public Iterator<Integer> iterator() {
		return new HeapIterator();
	}

	private class HeapIterator implements Iterator<Integer> {
		// create a new pq
		private final DoubleIndexMinPQ copy;

		// add all elements to copy of heap
		// takes linear time since already in heap order so no keys move
		public HeapIterator() {
			this.copy = new DoubleIndexMinPQ(DoubleIndexMinPQ.this.pq.length - 1);
			for (int i = 1; i <= DoubleIndexMinPQ.this.n; i++)
				this.copy.insert(DoubleIndexMinPQ.this.pq[i], DoubleIndexMinPQ.this.keys[DoubleIndexMinPQ.this.pq[i]]);
		}

		@Override
		public boolean hasNext() {
			return !this.copy.isEmpty();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public Integer next() {
			if (!this.hasNext())
				throw new NoSuchElementException();
			return this.copy.delMin();
		}
	}
}

/******************************************************************************
 * Copyright 2002-2016, Robert Sedgewick and Kevin Wayne.
 *
 * This file is part of algs4.jar, which accompanies the textbook
 *
 * Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne, Addison-Wesley
 * Professional, 2011, ISBN 0-321-57351-X. http://algs4.cs.princeton.edu
 *
 *
 * algs4.jar is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * algs4.jar is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * algs4.jar. If not, see http://www.gnu.org/licenses.
 ******************************************************************************/
