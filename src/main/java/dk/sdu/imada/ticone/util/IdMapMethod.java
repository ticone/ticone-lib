package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.Objects;

public abstract class IdMapMethod implements IIdMapMethod, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8599517406972162938L;

	public static final IdMapMethod NONE = new IdMapMethod() {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4126752739818119674L;

		@Override
		public IIdMapMethod copy() {
			return this;
		}

		@Override
		public boolean isActive() {
			return false;
		}

		@Override
		public void setActive(boolean isActive) {
		}

		@Override
		public String getAlternativeId(String objectId) {
			return null;
		}
	};

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IdMapMethod)
			return isActive == ((IdMapMethod) obj).isActive;
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(isActive);
	}

	protected boolean isActive;

	@Override
	public abstract String getAlternativeId(final String objectId);

	@Override
	public void setActive(final boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public boolean isActive() {
		return this.isActive;
	}
}
