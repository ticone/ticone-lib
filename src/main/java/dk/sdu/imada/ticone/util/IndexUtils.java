/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 26, 2019
 *
 */
public final class IndexUtils {
	/**
	 * 
	 * @param idx
	 * @param secondListLength
	 * @throws IllegalArgumentException If {@code secondListLength} < 1.
	 * @return
	 */
	public static final int[] pairIndexToObjectListIndices(final long idx, final int secondListLength) {
		if (secondListLength < 1)
			throw new IllegalArgumentException();
		final int x = (int) (idx / secondListLength);
		final int y = (int) (idx % secondListLength);
		return new int[] { x, y };
	}

}
