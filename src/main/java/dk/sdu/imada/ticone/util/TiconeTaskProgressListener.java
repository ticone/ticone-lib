package dk.sdu.imada.ticone.util;

import java.util.EventListener;

public interface TiconeTaskProgressListener extends EventListener {
	void progressUpdated(TiconeProgressEvent event);
}
