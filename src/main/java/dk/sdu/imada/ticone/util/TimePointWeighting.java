package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.Arrays;

public class TimePointWeighting implements Serializable, ITimePointWeighting {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1240552141839372620L;

	public static final TimePointWeighting NONE = new TimePointWeighting(0);

	protected double[] timePointWeights;

	public TimePointWeighting(final int columns) {
		this.setTimePointColumns(columns);
	}

	public TimePointWeighting(final ITimePointWeighting weighting) {
		this(weighting.getTimePointColumns());
		for (int c = 0; c < weighting.getTimePointColumns(); c++)
			this.setWeightForTimePointColumn(c, weighting.getWeightForTimePointColumn(c));
	}

	@Override
	public TimePointWeighting copy() {
		return new TimePointWeighting(this);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof TimePointWeighting))
			return false;
		final TimePointWeighting other = (TimePointWeighting) obj;
		return Arrays.equals(this.timePointWeights, other.timePointWeights);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(this.timePointWeights);
	}

	@Override
	public int getTimePointColumns() {
		return this.timePointWeights.length;
	}

	@Override
	public void setTimePointColumns(final int columns) {
		this.timePointWeights = new double[columns];
		for (int c = 0; c < columns; c++)
			// default weight
			this.timePointWeights[c] = 100;
	}

	/**
	 * 
	 * @param tp     0-based index for the time-point
	 * @param weight
	 */
	@Override
	public void setWeightForTimePointColumn(final int tp, final double weight) {
		this.timePointWeights[tp] = weight;
	}

	@Override
	public double getWeightForTimePointColumn(final int idx) {
		return this.timePointWeights[idx];
	}
}