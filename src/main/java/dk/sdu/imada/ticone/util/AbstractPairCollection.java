/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;

import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 23, 2019
 *
 * @param <P>
 */
public abstract class AbstractPairCollection<O1, O2, P extends IPair<O1, O2>> extends AbstractCollection<P>
		implements IPairCollection<O1, O2, P>, IObjectProvider, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4039143839732917126L;
	protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);
	protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

	protected AbstractPairCollection() {
		super();
	}

	protected AbstractPairCollection(final Collection<? extends P> c) {
		super();
		this.addAll(c);
	}

	@Override
	public P[] toArray() {
		return (P[]) super.toArray();
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}

	@Override
	public String toString() {
		return String.format("%s (%d)", "ObjectPairList", this.size());
	}
}
