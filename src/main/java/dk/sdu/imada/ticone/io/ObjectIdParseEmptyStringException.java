package dk.sdu.imada.ticone.io;

public class ObjectIdParseEmptyStringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2821321619260928687L;

	/**
	 * 
	 */

	public ObjectIdParseEmptyStringException(final int lineIndex, final int columnIndex) {
		super(String.format("Line %d, column %d should contain an object id, but was an empty string.", lineIndex + 1,
				columnIndex + 1));
	}

}
