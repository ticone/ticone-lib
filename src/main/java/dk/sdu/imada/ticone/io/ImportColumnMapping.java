package dk.sdu.imada.ticone.io;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ImportColumnMapping implements Serializable, IImportColumnMapping {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4540553573769664764L;

	public static final ImportColumnMapping DEFAULT_FILE_COLUMNS_WO_REPLICATES = new ImportColumnMapping(0, "Object ID",
			-1, null, null, null);

	public static final ImportColumnMapping DEFAULT_FILE_COLUMNS_W_REPLICATES = new ImportColumnMapping(0, "Object ID",
			1, "Sample", null, null);

	protected int objectIdColumnIndex, replicateColumnIndex;
	protected String objectIdColumnName, replicateColumnName;
	protected int[] timePointsColumnIndices;
	protected String[] timePointsColumnNames;

	protected transient Set<IMappingChangeListener> mappingChangeListener;

	public ImportColumnMapping() {
		super();
		this.objectIdColumnIndex = -1;
		this.replicateColumnIndex = -1;
		this.mappingChangeListener = new HashSet<>();
	}

	public ImportColumnMapping(ImportColumnMapping other) {
		this(other.objectIdColumnIndex, other.objectIdColumnName, other.replicateColumnIndex, other.replicateColumnName,
				other.timePointsColumnIndices != null
						? Arrays.copyOf(other.timePointsColumnIndices, other.timePointsColumnIndices.length)
						: null,
				other.timePointsColumnNames != null
						? Arrays.copyOf(other.timePointsColumnNames, other.timePointsColumnNames.length)
						: null);
	}

	@Override
	public ImportColumnMapping copy() {
		return new ImportColumnMapping(this);
	}

	public ImportColumnMapping(final int objectIdColumnIndex, final String objectIdColumnName,
			final int replicateColumnIndex, final String replicateColumnName, final int[] timePointsColumnIndices,
			final String[] timePointsColumnNames) {
		super();
		this.objectIdColumnIndex = objectIdColumnIndex;
		this.objectIdColumnName = objectIdColumnName;
		this.replicateColumnIndex = replicateColumnIndex;
		this.replicateColumnName = replicateColumnName;
		this.timePointsColumnIndices = timePointsColumnIndices;
		this.timePointsColumnNames = timePointsColumnNames;
		this.mappingChangeListener = new HashSet<>();
	}

	@Override
	public boolean addMappingChangeListener(IMappingChangeListener listener) {
		return this.mappingChangeListener.add(listener);
	}

	@Override
	public boolean removeMappingChangeListener(IMappingChangeListener listener) {
		return this.mappingChangeListener.remove(listener);
	}

	protected void fireMappingChanged(final MappingChangedEvent event) {
		for (IMappingChangeListener l : this.mappingChangeListener)
			l.mappingChanged(event);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ImportColumnMapping) {
			ImportColumnMapping other = (ImportColumnMapping) obj;
			return objectIdColumnIndex == other.objectIdColumnIndex
					&& replicateColumnIndex == other.replicateColumnIndex
					&& Objects.equals(objectIdColumnName, other.objectIdColumnName)
					&& Objects.equals(replicateColumnName, other.replicateColumnName)
					&& Arrays.equals(timePointsColumnIndices, other.timePointsColumnIndices)
					&& Arrays.equals(timePointsColumnNames, other.timePointsColumnNames);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(objectIdColumnIndex, replicateColumnIndex, objectIdColumnName, replicateColumnName,
				timePointsColumnIndices, timePointsColumnNames);
	}

	private void readObject(final ObjectInputStream is) throws ClassNotFoundException, IOException {
		is.defaultReadObject();
		this.mappingChangeListener = new HashSet<>();
	}

	@Override
	public void setObjectIdColumn(final int objectIdColumnIndex, final String objectIdColumnName) {
		final boolean changed = this.objectIdColumnIndex != objectIdColumnIndex;
		this.objectIdColumnIndex = objectIdColumnIndex;
		this.objectIdColumnName = objectIdColumnName;
		if (changed)
			fireMappingChanged(new MappingChangedEvent());
	}

	@Override
	public void setReplicateColumn(final int replicateColumnIndex, final String replicateColumnName) {
		final boolean changed = this.replicateColumnIndex != replicateColumnIndex;
		this.replicateColumnIndex = replicateColumnIndex;
		this.replicateColumnName = replicateColumnName;
		if (changed)
			fireMappingChanged(new MappingChangedEvent());
	}

	@Override
	public void setTimePointsColumns(final int[] timePointsColumnIndices, final String[] timePointsColumnNames) {
		final boolean changed = !Arrays.equals(this.timePointsColumnIndices, timePointsColumnIndices);
		this.timePointsColumnIndices = timePointsColumnIndices;
		this.timePointsColumnNames = timePointsColumnNames;
		if (changed)
			fireMappingChanged(new MappingChangedEvent());
	}

	@Override
	public int getObjectIdColumnIndex() {
		return this.objectIdColumnIndex;
	}

	@Override
	public String getObjectIdColumnName() {
		return this.objectIdColumnName;
	}

	@Override
	public int getReplicateColumnIndex() {
		return this.replicateColumnIndex;
	}

	@Override
	public String getReplicateColumnName() {
		return this.replicateColumnName;
	}

	@Override
	public int[] getTimePointsColumnIndices() {
		return this.timePointsColumnIndices;
	}

	@Override
	public String[] getTimePointsColumnNames() {
		return this.timePointsColumnNames;
	}
}
