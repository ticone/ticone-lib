/**
 * 
 */
package dk.sdu.imada.ticone.io;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 1, 2019
 *
 */
public class LoadDataFromCollection extends LoadDataMethod {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8932479390251878692L;

	private final ITimeSeriesObjectList objects;

	public LoadDataFromCollection(ITimeSeriesObjectList objects) {
		super();
		this.objects = objects;
	}

	@Override
	public ILoadDataMethod copy() {
		return new LoadDataFromCollection(objects);
	}

	@Override
	protected void loadDataInternal(final ITimeSeriesPreprocessor preprocessor) throws LoadDataException {
		preprocessor.initializeObjects(objects);
	}

	@Override
	public String sourceAsString() {
		return String.format("Fixed list of %d objects", objects.size());
	}

}
