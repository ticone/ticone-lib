package dk.sdu.imada.ticone.io;

public class SampleNameParseEmptyStringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2442714460567337247L;

	/**
	 * 
	 */

	public SampleNameParseEmptyStringException(final int lineIndex, final int columnIndex) {
		super(String.format("Line %d, column %d should contain a sample name, but was an empty string.", lineIndex + 1,
				columnIndex + 1));
	}

}
