package dk.sdu.imada.ticone.io;

public class TimePointSignalNotANumberException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8631267812464107002L;

	public TimePointSignalNotANumberException(final int lineIndex, final int columnIndex, final String cellString) {
		super(String.format(
				"Line %d, column %d should contain a numerical time point signal, but it was non-numerical: '%s'",
				lineIndex + 1, columnIndex + 1, cellString));
	}

}
