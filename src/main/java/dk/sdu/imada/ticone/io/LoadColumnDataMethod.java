/**
 * 
 */
package dk.sdu.imada.ticone.io;

import com.google.common.base.Objects;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 1, 2019
 *
 */
public abstract class LoadColumnDataMethod extends LoadDataMethod
		implements ILoadColumnDataMethod, IMappingChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2613186391288901486L;

	protected ImportColumnMapping columnMapping;

	public LoadColumnDataMethod() {
		super();
		this.columnMapping = new ImportColumnMapping();
		this.columnMapping.addMappingChangeListener(this);
	}

	public LoadColumnDataMethod(LoadColumnDataMethod other) {
		super(other);
		this.columnMapping = other.columnMapping.copy();
		this.columnMapping.addMappingChangeListener(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LoadColumnDataMethod)
			return Objects.equal(columnMapping, ((LoadColumnDataMethod) obj).columnMapping);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(columnMapping);
	}

	@Override
	public ImportColumnMapping getColumnMapping() {
		return this.columnMapping;
	}

	protected abstract void setDefaultColumnMapping() throws LoadDataException;

	public void setColumnMapping(ImportColumnMapping columnMapping) {
		if (columnMapping != null)
			this.columnMapping.removeMappingChangeListener(this);
		this.columnMapping = columnMapping;
		this.columnMapping.addMappingChangeListener(this);
	}

	@Override
	public void mappingChanged(MappingChangedEvent event) {
		this.isDataLoaded = false;
	}
}
