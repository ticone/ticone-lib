package dk.sdu.imada.ticone.io;

import java.io.Serializable;

import com.google.common.base.Objects;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;

public abstract class LoadDataMethod implements Serializable, ILoadDataMethod {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2764328795266029999L;
	protected boolean isDataLoaded;

	public LoadDataMethod() {
		super();
	}

	public LoadDataMethod(LoadDataMethod other) {
		super();
		this.isDataLoaded = other.isDataLoaded;
	}

	@Override
	public boolean equals(Object obj) {
		return Objects.equal(this.getClass(), obj.getClass());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.getClass());
	}

	@Override
	public final void loadData(final ITimeSeriesPreprocessor preprocessor) throws LoadDataException {
		this.loadDataInternal(preprocessor);
	}

	/**
	 * Parses or retrieves a {@link ITimeSeriesObjectList} and eventually passes it
	 * to {@link ITimeSeriesPreprocessor#initializeObjects(ITimeSeriesObjectList)}.
	 * 
	 * @param processBuilder
	 * @throws LoadDataException
	 */
	protected abstract void loadDataInternal(final ITimeSeriesPreprocessor preprocessor) throws LoadDataException;

	@Override
	public boolean isDataLoaded() {
		return this.isDataLoaded;
	}

	@Override
	public abstract String sourceAsString();
}
