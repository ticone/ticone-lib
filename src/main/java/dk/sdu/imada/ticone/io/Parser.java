
package dk.sdu.imada.ticone.io;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import dk.sdu.imada.ticone.clustering.ClusterFactoryException;
import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder.ITimeSeriesPrototypeComponent;
import dk.sdu.imada.ticone.data.MultipleTimeSeriesSignalsForSameSampleParseException;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

/**
 * This class is used to parse Time Series Data and predefined patterns.
 *
 * @author Christian Nørskov
 */
public class Parser {

	/**
	 * Parses the input file with the patterns, and saves them to a Patter array
	 *
	 * @param scan the scanner for the file
	 * @return returns null if the file is not found, otherwise returns an array of
	 *         type Pattern
	 * @throws PrototypeComponentFactoryException
	 * @throws PrototypeFactoryException
	 * @throws IncompatiblePrototypeComponentException
	 * @throws MissingPrototypeFactoryException
	 * @throws InterruptedException
	 * @throws ClusterFactoryException
	 * @throws CreateInstanceFactoryException
	 * @throws IncompatiblePrototypeException
	 */
	public static ICluster[] parsePatterns(final Scanner scan, final IPrototypeBuilder prototypeFactory)
			throws FileNotFoundException, PrototypeComponentFactoryException, PrototypeFactoryException,
			IncompatiblePrototypeComponentException, MissingPrototypeFactoryException, InterruptedException,
			ClusterFactoryException, CreateInstanceFactoryException, IncompatiblePrototypeException {
		final IClusterList patterns = new ClusterList();
		final ClusterObjectMapping clustering = new ClusterObjectMapping(new TimeSeriesObjectSet(), prototypeFactory);
		String line;
		String[] parts;
		double[] pattern;

		while (scan.hasNextLine()) {
			line = scan.nextLine();
			parts = line.split("\t");
			pattern = new double[parts.length];
			for (int i = 0; i < pattern.length; i++) {
				pattern[i] = Double.parseDouble(parts[i]);
			}
			final ITimeSeriesPrototypeComponent tsComponent = PrototypeComponentType.TIME_SERIES
					.getComponentFactory(prototypeFactory).setTimeSeries(new TimeSeries(pattern)).build();
			final IPrototype prototype = prototypeFactory.setComponents(tsComponent).build();
			patterns.add(clustering.addCluster(prototype));
		}

		final ICluster[] p = new ICluster[patterns.size()];
		for (int i = 0; i < p.length; i++) {
			p[i] = patterns.get(i);
		}
		return p;
	}

	public static ITimeSeriesObjectList parseObjects(final Scanner scan)
			throws FileNotFoundException, TimePointSignalParseEmptyStringException, ObjectIdParseEmptyStringException,
			SampleNameParseEmptyStringException, MultipleTimeSeriesSignalsForSameSampleParseException,
			TimePointSignalNotANumberException {
		return parseObjects(scan, null);
	}

	/**
	 *
	 * @param scan
	 * @return
	 * @throws FileNotFoundException
	 * @throws TimePointSignalParseEmptyStringException
	 * @throws SampleNameParseEmptyStringException
	 * @throws ObjectIdParseEmptyStringException
	 * @throws MultipleTimeSeriesSignalsForSameSampleParseException
	 * @throws TimePointSignalNotANumberException
	 */
	public static ITimeSeriesObjectList parseObjects(final Scanner scan, final ImportColumnMapping columnMapping)
			throws FileNotFoundException, TimePointSignalParseEmptyStringException, ObjectIdParseEmptyStringException,
			SampleNameParseEmptyStringException, MultipleTimeSeriesSignalsForSameSampleParseException,
			TimePointSignalNotANumberException {
		return parseObjects(scan, columnMapping, false);
	}

	/**
	 * This method considers strings as candidates for object ids and sample ids,
	 * that represent either not a number at all, an integer number.
	 * 
	 * @param lineParts
	 * @return
	 */
	private static ImportColumnMapping guessFromLineParts(String[] lineParts) {
		int oIdIdx = -1, sIdx = -1;
		for (int i = 0; i < lineParts.length; i++) {
			boolean interpretAsString = false;
			try {
				Double.parseDouble(lineParts[i]);

			} catch (NumberFormatException e) {
				// this is not a number
				interpretAsString = true;
			}
			if (interpretAsString) {
				if (oIdIdx == -1)
					oIdIdx = i;
				else if (sIdx == -1) {
					sIdx = i;
					break;
				}
			}
		}
		return new ImportColumnMapping(oIdIdx, "Column 1", sIdx, null, null, null);
	}

	/**
	 *
	 * @param scan
	 * @return
	 * @throws FileNotFoundException
	 * @throws TimePointSignalParseEmptyStringException
	 * @throws ObjectIdParseEmptyStringException
	 * @throws SampleNameParseEmptyStringException
	 * @throws MultipleTimeSeriesSignalsForSameSampleParseException
	 * @throws TimePointSignalNotANumberException
	 */
	public static ITimeSeriesObjectList parseObjects(final Scanner scan, ImportColumnMapping columnMapping,
			final boolean hasHeaderLine) throws FileNotFoundException, TimePointSignalParseEmptyStringException,
			ObjectIdParseEmptyStringException, SampleNameParseEmptyStringException,
			MultipleTimeSeriesSignalsForSameSampleParseException, TimePointSignalNotANumberException {
		final ITimeSeriesObjectList objects = new TimeSeriesObjectList();
		final Map<String, Map<String, double[]>> timeSeriesDataMap = new LinkedHashMap<>();
		String line;
		String[] parts;
		double[] signal;

		int lineIndex = 0;

		// burn header line
		if (hasHeaderLine && scan.hasNextLine()) {
			scan.nextLine();
			lineIndex++;
		}

		boolean hasReplicateColumn = false;
		if (columnMapping != null)
			hasReplicateColumn = columnMapping.replicateColumnIndex > -1;

		final Object2IntMap<String> sampleNamesToNumbers = new Object2IntOpenHashMap<>();

		while (scan.hasNextLine()) {
			line = scan.nextLine();
			parts = line.split("\t");

			if (columnMapping == null) {
				columnMapping = guessFromLineParts(parts);
				hasReplicateColumn = columnMapping.replicateColumnIndex > -1;
			}

			final String objectName = parts[columnMapping.getObjectIdColumnIndex()];
			if (objectName.isEmpty())
				throw new ObjectIdParseEmptyStringException(lineIndex, columnMapping.getObjectIdColumnIndex());
			final String sampleName;
			if (hasReplicateColumn) {
				sampleName = parts[columnMapping.getReplicateColumnIndex()];
				if (sampleName.isEmpty())
					throw new SampleNameParseEmptyStringException(lineIndex, columnMapping.getObjectIdColumnIndex());
			} else
				sampleName = "sample1";

			int[] timePointColumnIndices;
			if (columnMapping.getTimePointsColumnIndices() == null) {
				timePointColumnIndices = new int[parts.length - (hasReplicateColumn ? 2 : 1)];
				for (int i = 0; i < timePointColumnIndices.length; i++)
					timePointColumnIndices[i] = i + (hasReplicateColumn ? 2 : 1);
			} else
				timePointColumnIndices = columnMapping.getTimePointsColumnIndices();

			signal = new double[timePointColumnIndices.length];
			for (int i = 0; i < signal.length; i++) {
				final String s = parts[timePointColumnIndices[i]];
				if (s.isEmpty()) {
					throw new TimePointSignalParseEmptyStringException(lineIndex, timePointColumnIndices[i]);
				}
				try {
					signal[i] = Double.parseDouble(s);
				} catch (final NumberFormatException e) {
					throw new TimePointSignalNotANumberException(lineIndex, timePointColumnIndices[i], s);
				}
			}

			sampleNamesToNumbers.putIfAbsent(sampleName, sampleNamesToNumbers.size());
			timeSeriesDataMap.putIfAbsent(objectName, new HashMap<>());
			if (timeSeriesDataMap.get(objectName).containsKey(sampleName))
				throw new MultipleTimeSeriesSignalsForSameSampleParseException(objectName, sampleName);
			timeSeriesDataMap.get(objectName).put(sampleName, signal);

			lineIndex++;
		}

		for (String objectName : timeSeriesDataMap.keySet()) {
			final Map<String, double[]> objectSampleMap = timeSeriesDataMap.get(objectName);
			final ITimeSeriesObject object = new TimeSeriesObject(objectName, objectSampleMap.size());

			for (String sampleName : objectSampleMap.keySet()) {
				object.addOriginalTimeSeriesToList(sampleNamesToNumbers.getInt(sampleName),
						new TimeSeries(objectSampleMap.get(sampleName)), sampleName);

			}
			objects.add(object);
		}

		return objects;
	}

	public static void writeObjectSetsToOutputStream(final ITimeSeriesObjects tsData, final Writer w)
			throws IOException {
		for (final ITimeSeriesObject ts : tsData) {
			for (int i = 0; i < ts.getSampleNameList().size(); i++) {
				final ITimeSeries signal = ts.getOriginalTimeSeriesList()[i];
				w.write(ts.getName());
				w.write("\t");
				if (ts.getSampleNameList().size() > 1) {
					final String sampleName = ts.getSampleNameList().get(i);
					w.write(sampleName);
					w.write("\t");
				}
				for (int t = 0; t < signal.getNumberTimePoints(); t++) {
					w.write(signal.get(t) + "");
					if (t < signal.getNumberTimePoints() - 1)
						w.write("\t");
				}
				w.write("\n");
			}
		}
	}

}
