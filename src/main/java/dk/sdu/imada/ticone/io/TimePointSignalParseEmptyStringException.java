package dk.sdu.imada.ticone.io;

public class TimePointSignalParseEmptyStringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2241865539181800566L;

	public TimePointSignalParseEmptyStringException(final int lineIndex, final int columnIndex) {
		super(String.format(
				"Line %d, column %d should contain a numerical time point signal, but was an empty string (missing value). This is not supported at this time.",
				lineIndex + 1, columnIndex + 1));
	}

}
