package dk.sdu.imada.ticone.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.MultipleTimeSeriesSignalsForSameSampleParseException;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.util.ObjectUtils;

public class LoadDataFromFile extends LoadColumnDataMethod {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3980126188359669686L;
	protected File importFile;
	protected boolean firstRowHeaderFile;

	public LoadDataFromFile() {
	}

	public LoadDataFromFile(LoadDataFromFile other) {
		super(other);
		this.importFile = other.importFile;
		this.firstRowHeaderFile = other.firstRowHeaderFile;
	}

	@Override
	public LoadDataFromFile copy() {
		return new LoadDataFromFile(this);
	}

	public void setImportFile(final File importFile) {
		if (ObjectUtils.bothNullOrNone(this.importFile, importFile)
				|| (this.importFile != null && !this.importFile.equals(importFile)))
			this.isDataLoaded = false;
		this.importFile = importFile;
	}

	public File getImportFile() {
		return this.importFile;
	}

	public void setFirstRowHeaderFile(final boolean firstRowHeaderFile) {
		if (this.firstRowHeaderFile != firstRowHeaderFile)
			this.isDataLoaded = false;
		this.firstRowHeaderFile = firstRowHeaderFile;
	}

	public boolean isFirstRowHeaderFile() {
		return this.firstRowHeaderFile;
	}

	@Override
	protected void loadDataInternal(final ITimeSeriesPreprocessor preprocessor) throws LoadDataException {
		this.isDataLoaded = false;
		try {
			final Scanner scan = new Scanner(this.importFile);
			final ITimeSeriesObjectList timeSeriesDatas = Parser.parseObjects(scan, this.columnMapping,
					this.firstRowHeaderFile);
			scan.close();

			preprocessor.initializeObjects(timeSeriesDatas);
			this.isDataLoaded = true;
		} catch (FileNotFoundException | TimePointSignalParseEmptyStringException | ObjectIdParseEmptyStringException
				| SampleNameParseEmptyStringException | MultipleTimeSeriesSignalsForSameSampleParseException
				| TimePointSignalNotANumberException fnfe) {
			throw new LoadDataException(fnfe);
		}
	}

	@Override
	public String sourceAsString() {
		return this.importFile.getName();
	}

	@Override
	protected void setDefaultColumnMapping() {
		this.isDataLoaded = false;
		// use all remaining columns as time points
		this.columnMapping.setTimePointsColumns(null, null);
		this.columnMapping.setObjectIdColumn(1, "Column 2");
		this.columnMapping.setReplicateColumn(0, "Column 1");
	}
}
