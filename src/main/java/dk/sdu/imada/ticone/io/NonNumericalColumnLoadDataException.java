package dk.sdu.imada.ticone.io;

public class NonNumericalColumnLoadDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3989015452494216405L;

	public NonNumericalColumnLoadDataException(final Throwable cause) {
		super(cause);
	}

	public NonNumericalColumnLoadDataException(final String text) {
		super(text);
	}
}
