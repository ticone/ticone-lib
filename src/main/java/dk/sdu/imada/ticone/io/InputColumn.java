package dk.sdu.imada.ticone.io;

import java.util.Objects;

public class InputColumn {

	public static final InputColumn NO_COLUMN = new InputColumn("--", -1);
	protected String name;
	protected int index;

	public InputColumn(final String name, final int index) {
		super();
		this.name = name;
		this.index = index;
	}

	public int getIndex() {
		return this.index;
	}

	public String getName() {
		return this.name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof InputColumn))
			return false;
		final InputColumn other = (InputColumn) obj;
		return Objects.equals(this.name, other.name) && Objects.equals(this.index, other.index);
	}

	@Override
	public int hashCode() {
		return (this.name + "_" + this.index).hashCode();
	}

	@Override
	public String toString() {
		return this.name;
	}
}
