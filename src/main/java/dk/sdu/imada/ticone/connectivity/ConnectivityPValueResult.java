package dk.sdu.imada.ticone.connectivity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Map;

import dk.sdu.imada.ticone.clustering.AbstractNamedTiconeResult;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.statistics.IPvalue;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.Object2IntMap;

public class ConnectivityPValueResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8055192411130362776L;
	protected AbstractNamedTiconeResult util;
	protected Object2IntMap<IClusterPair> directedEdgeCount, undirectedEdgeCount;
	// these are always the combined p-values, i.e. in case of two-sided test
	// two-sided p-values, otherwise one-sided p-values for larger log-odds;
	protected Map<IClusterPair, IPvalue> pvaluesDirected;
	protected Map<IClusterPair, IPvalue> pvaluesUndirected;
	protected Map<IClusterPair, IPvalue> pvaluesLogOddsSmallerDirected;
	protected Map<IClusterPair, IPvalue> pvaluesLogOddsSmallerUndirected;
	protected Map<IClusterPair, IPvalue> pvaluesLogOddsLargerDirected;
	protected Map<IClusterPair, IPvalue> pvaluesLogOddsLargerUndirected;
	protected Map<ICluster, Map<ICluster, IntList>> allRandomInterClusterEdgeCountDirected;
	protected ITiconeNetwork network;
	protected String networkName;
	protected boolean twoSidedPermutationTest;
	protected int numberPermutations;
	protected IShuffle permutationFunction;
	protected Map<IClusterPair, IntList> permutationEdgeCountsDirected, permutationEdgeCountsUndirected;

	public ConnectivityPValueResult(final AbstractNamedTiconeResult util, final ITiconeNetwork network,
			final Object2IntMap<IClusterPair> directedEdgeCount, final Object2IntMap<IClusterPair> undirectedEdgeCount,
			final Map<IClusterPair, IPvalue> pvaluesDirected, final Map<IClusterPair, IPvalue> pvaluesUndirected,
			final Map<IClusterPair, IPvalue> pvaluesLogOddsSmallerDirected,
			final Map<IClusterPair, IPvalue> pvaluesLogOddsSmallerUndirected,
			final Map<IClusterPair, IPvalue> pvaluesLogOddsLargerDirected,
			final Map<IClusterPair, IPvalue> pvaluesLogOddsLargerUndirected,
			final Map<ICluster, Map<ICluster, IntList>> allRandomInterClusterEdgeCountDirected,
			final boolean twoSidedPermutationTest, final int numberPermutations, final IShuffle permutationFunction,
			final Map<IClusterPair, IntList> permutationEdgeCountsDirected,
			final Map<IClusterPair, IntList> permutationEdgeCountsUndirected) {
		super();
		this.util = util;
		this.networkName = network.getName();
		this.directedEdgeCount = directedEdgeCount;
		this.undirectedEdgeCount = undirectedEdgeCount;
		this.pvaluesDirected = pvaluesDirected;
		this.pvaluesUndirected = pvaluesUndirected;
		this.pvaluesLogOddsSmallerDirected = pvaluesLogOddsSmallerDirected;
		this.pvaluesLogOddsSmallerUndirected = pvaluesLogOddsSmallerUndirected;
		this.pvaluesLogOddsLargerDirected = pvaluesLogOddsLargerDirected;
		this.pvaluesLogOddsLargerUndirected = pvaluesLogOddsLargerUndirected;
		this.allRandomInterClusterEdgeCountDirected = allRandomInterClusterEdgeCountDirected;
		this.twoSidedPermutationTest = twoSidedPermutationTest;
		this.numberPermutations = numberPermutations;
		this.permutationFunction = permutationFunction;
		this.permutationEdgeCountsDirected = permutationEdgeCountsDirected;
		this.permutationEdgeCountsUndirected = permutationEdgeCountsUndirected;
	}

	public AbstractNamedTiconeResult getUtil() {
		return this.util;
	}

	/**
	 * @return the permutationFunction
	 */
	public IShuffle getPermutationFunction() {
		return this.permutationFunction;
	}

	public int getNumberPermutations() {
		if (this.numberPermutations == 0) {
			this.numberPermutations = 1000;
		}
		return this.numberPermutations;
	}

	public Map<IClusterPair, Integer> getDirectedEdgeCount() {
		return this.directedEdgeCount;
	}

	public Map<IClusterPair, Integer> getUndirectedEdgeCount() {
		return this.undirectedEdgeCount;
	}

	public Map<IClusterPair, IPvalue> getDirectedEdgeCountPvalues() {
		return this.pvaluesDirected;
	}

	public Map<IClusterPair, IPvalue> getDirectedEdgeCountLogOddsSmallerPvalues() {
		return this.pvaluesLogOddsSmallerDirected;
	}

	public Map<IClusterPair, IPvalue> getDirectedEdgeCountLogOddsLargerPvalues() {
		return this.pvaluesLogOddsLargerDirected;
	}

	public Map<IClusterPair, IPvalue> getUndirectedEdgeCountLogOddsSmallerPvalues() {
		return this.pvaluesLogOddsSmallerUndirected;
	}

	public Map<IClusterPair, IPvalue> getUndirectedEdgeCountLogOddsLargerPvalues() {
		return this.pvaluesLogOddsLargerUndirected;
	}

	public Map<IClusterPair, IPvalue> getUndirectedEdgeCountPvalues() {
		return this.pvaluesUndirected;
	}

	public Map<ICluster, Map<ICluster, IntList>> getAllRandomInterClusterEdgeCountDirected() {
		return this.allRandomInterClusterEdgeCountDirected;
	}

	public String getNetworkName() {
		return this.networkName;
	}

	public boolean isTwoSidedPermutationTest() {
		return this.twoSidedPermutationTest;
	}

	private void readObject(final ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		// deserialize of older version: use string instead of network;
		if (this.networkName == null && this.network != null) {
			this.networkName = this.network.getName();
			this.network = null;
		}
	}

	public Map<IClusterPair, IntList> getPermutationEdgeCountsDirected() {
		return this.permutationEdgeCountsDirected;
	}

	public Map<IClusterPair, IntList> getPermutationEdgeCountsUndirected() {
		return this.permutationEdgeCountsUndirected;
	}

}
