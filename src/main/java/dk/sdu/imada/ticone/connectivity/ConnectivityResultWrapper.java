package dk.sdu.imada.ticone.connectivity;

import java.util.Date;

import dk.sdu.imada.ticone.clustering.AbstractNamedTiconeResult;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.util.ITiconeResultDestroyedListener;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;

public class ConnectivityResultWrapper extends AbstractNamedTiconeResult implements ITiconeResultDestroyedListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6828765301899029237L;
	protected int connectivityNumber;
	protected TiconeClusteringResult util;
	protected ConnectivityResult directedConnectivityResult;
	protected ConnectivityResult undirectedConnectivityResult;
	protected ConnectivityPValueResult edgeCrossoverConnectivityResult;
	protected int clusteringIteration;

	protected static int nextConnectivityNumber = 1;

	public ConnectivityResultWrapper(final TiconeClusteringResult util,
			final ConnectivityResult directedConnectivityResult, final ConnectivityResult undirectedConnectivityResult,
			final ConnectivityPValueResult edgeCrossoverConnectivityResult, final int clusteringIteration) {
		super();
		this.util = util;
		this.directedConnectivityResult = directedConnectivityResult;
		this.undirectedConnectivityResult = undirectedConnectivityResult;
		this.edgeCrossoverConnectivityResult = edgeCrossoverConnectivityResult;
		this.connectivityNumber = nextConnectivityNumber++;
		this.clusteringIteration = clusteringIteration;
		this.setDate(new Date());

		this.util.addOnDestroyListener(this);
	}

	public TiconeClusteringResult getClusteringResult() {
		return this.util;
	}

	public int getClusteringIteration() {
		return this.clusteringIteration;
	}

	public ConnectivityResult getDirectedConnectivityResult() {
		return this.directedConnectivityResult;
	}

	public ConnectivityPValueResult getConnectivityPValueResult() {
		return this.edgeCrossoverConnectivityResult;
	}

	public ConnectivityResult getUndirectedConnectivityResult() {
		return this.undirectedConnectivityResult;
	}

	@Override
	protected void initName() {
		this.name = String.format("Connectivity %d", this.connectivityNumber);
	}

	public static int getNextConnectivityNumber() {
		return nextConnectivityNumber;
	}

	public static void setNextConnectivityNumber(final int nextConnectivityNumber) {
		ConnectivityResultWrapper.nextConnectivityNumber = nextConnectivityNumber;
	}

	public int getConnectivityNumber() {
		return this.connectivityNumber;
	}

	@Override
	public void destroy() {
		super.destroy();
		// TODO: clear networks
	}

	@Override
	public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
		if (event.getResult().equals(util))
			this.destroy();
	}
}
