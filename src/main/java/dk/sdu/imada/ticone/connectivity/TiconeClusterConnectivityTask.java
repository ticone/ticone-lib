package dk.sdu.imada.ticone.connectivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cern.colt.bitvector.BitMatrix;
import cern.colt.function.IntIntProcedure;
import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping.ClusterObjectMappingCopy;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.ClusterObjectMappingPair;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IShuffleClusteringPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberDirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberUndirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IClusterPairFeatureNumberDirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.IClusterPairFeatureNumberUndirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.network.IShuffleNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.network.TiconeNetwork.TiconeNetworkEdge;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.network.kdtree.DistanceFunction;
import dk.sdu.imada.ticone.network.kdtree.SquareEuclideanDistanceFunction;
import dk.sdu.imada.ticone.statistics.BasicCalculatePValues;
import dk.sdu.imada.ticone.statistics.IPvalue;
import dk.sdu.imada.ticone.statistics.MultiplyPValues;
import dk.sdu.imada.ticone.statistics.PValueCalculationException;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult;
import dk.sdu.imada.ticone.statistics.PermutationTestChangeEvent;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.TiconeTask;
import dk.sdu.imada.ticone.util.ToNumberConversionException;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

/**
 * 
 * @author Christian Wiwie
 *
 */
public class TiconeClusterConnectivityTask extends TiconeTask {

	protected TiconeClusteringResult clusteringResult;
	protected IClusters selectedClusters;
	protected ClusterObjectMappingCopy copiedPom;
	protected int numberPermutations;
	protected boolean twoSidedPermutationTest;
	protected IShuffleNetwork networkShuffle;
	protected IShuffleClusteringPair clusteringPairShuffle;
	protected BasicCalculatePValues calculatePvalues;
	protected IFeatureStore clusterPairFeatureStore;

	protected Map<String, ICluster> entrezToClusters;

	protected DistanceFunction kdTreeDistanceFunction = new SquareEuclideanDistanceFunction();

	public static enum ConnectivityType {
		PERMUTATION_TEST, DIRECTED, UNDIRECTED
	}

	public TiconeClusterConnectivityTask(
			final ITiconeNetwork<? extends ITiconeNetworkNode, ? extends TiconeNetworkEdge> network,
			final TiconeClusteringResult clusteringResult, final ClusterObjectMapping originalClustering,
			final IClusters selectedClusters, final int numberPermutations, final boolean twoSidedPermutationTest,
			final IShuffleNetwork networkShuffle) throws InterruptedException {
		super();
		this.clusteringResult = clusteringResult;
		this.copiedPom = originalClustering.getCopy();
		this.selectedClusters = new ClusterSet();

		for (final ICluster c : new HashSet<>(originalClustering.getClusters())) {
			ICluster copiedCluster = copiedPom.getCopy(c);
			if (selectedClusters.contains(c))
				this.selectedClusters.add(copiedCluster);
			else
				copiedPom.getCopy().removeData(copiedCluster.getClusterNumber(),
						ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);
		}

		this.numberPermutations = numberPermutations;
		this.twoSidedPermutationTest = twoSidedPermutationTest;
		this.networkShuffle = networkShuffle;
	}

	public TiconeClusterConnectivityTask(
			final ITiconeNetwork<? extends ITiconeNetworkNode, ? extends TiconeNetworkEdge> network,
			final TiconeClusteringResult clusteringResult, final ClusterObjectMapping originalClustering,
			final IClusters selectedClusters, final int numberPermutations, final boolean twoSidedPermutationTest,
			final IShuffleClusteringPair clusteringPairShuffle) throws InterruptedException {
		super();
		this.clusteringResult = clusteringResult;
		this.copiedPom = originalClustering.getCopy();
		this.selectedClusters = new ClusterSet();

		for (final ICluster c : new HashSet<>(originalClustering.getClusters())) {
			ICluster copiedCluster = copiedPom.getCopy(c);
			if (selectedClusters.contains(c))
				this.selectedClusters.add(copiedCluster);
			else
				copiedPom.getCopy().removeData(copiedCluster.getClusterNumber(),
						ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);
		}

		this.numberPermutations = numberPermutations;
		this.twoSidedPermutationTest = twoSidedPermutationTest;
		this.clusteringPairShuffle = clusteringPairShuffle;
	}

	public Map<String, ICluster> initEntrezToNetworks() {
		if (this.entrezToClusters == null) {
			this.entrezToClusters = new HashMap<>();

			for (final ICluster cluster : this.selectedClusters) {
				final ITimeSeriesObjects objects = cluster.getObjects();
				for (final ITimeSeriesObject o : objects) {
					final String entrezId = o.getName();
					this.entrezToClusters.put(entrezId, cluster);
				}
			}
		}
		return this.entrezToClusters;
	}

	private Map<ICluster, Map<ICluster, Integer>> getDirectedEdgesBetweenClusters(
			final Map<String, ICluster> entrezToNetworks, final Collection<ICluster> selectedClustersCopied,
			final TiconeNetworkImpl network) {
		final BitMatrix connectedNodes = network.getConnectedNodesArray(false);

		final Map<ICluster, Map<ICluster, Integer>> counts = new HashMap<>();

		// make sure we have all cluster pairs in the map
		for (final ICluster c1 : selectedClustersCopied) {
			final Map<ICluster, Integer> tmp = new HashMap<>();

			for (final ICluster c2 : selectedClustersCopied) {
				tmp.put(c2, 0);

			}
			counts.put(c1, tmp);
		}

		// for (Pair<String, String> p : connectedNodes) {
		for (int n1 = 0; n1 < connectedNodes.rows(); n1++) {
			final TiconeNetworkNodeImpl sourceNode = network.getNetworkWideNodeIdToNode().get(n1);
			if (sourceNode == null)
				continue;
			final String sourceEntrezId = sourceNode.getName();
			final ICluster sourceNet = entrezToNetworks.get(sourceEntrezId);
			for (int n2 = 0; n2 < connectedNodes.columns(); n2++) {
				final TiconeNetworkNodeImpl targetNode = network.getNetworkWideNodeIdToNode().get(n2);
				if (targetNode == null)
					continue;

				if (!connectedNodes.get(n1, n2))
					continue;
				final String targetEntrezId = targetNode.getName();
				final ICluster targetNet = entrezToNetworks.get(targetEntrezId);
				if (sourceNet == null || targetNet == null)
					continue;
				if (!(selectedClustersCopied.contains(sourceNet)))
					continue;
				if (!(selectedClustersCopied.contains(targetNet)))
					continue;
				if (!counts.containsKey(sourceNet)) {
					counts.put(sourceNet, new HashMap<ICluster, Integer>());
				}
				if (!counts.get(sourceNet).containsKey(targetNet)) {
					counts.get(sourceNet).put(targetNet, 0);
				}
				if (!counts.containsKey(targetNet)) {
					counts.put(targetNet, new HashMap<ICluster, Integer>());
				}
				if (!counts.get(targetNet).containsKey(sourceNet)) {
					counts.get(targetNet).put(sourceNet, 0);
				}
				counts.get(sourceNet).put(targetNet, counts.get(sourceNet).get(targetNet) + 1);
			}
		}
		return counts;
	}

	protected ConnectivityResult getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(final TiconeNetworkImpl network) {
		if (network.getDirectedEnrichment() == null) {
			logger.info("Calculating directed connectivity enrichment ...");
			final Map<ICluster, Map<ICluster, Integer>> counts = this
					.getDirectedEdgesBetweenClusters(this.entrezToClusters, this.selectedClusters, network);
			final Object2IntMap<IClusterPair> edgeCounts = new Object2IntOpenHashMap<>();
			final Object2DoubleMap<IClusterPair> exp = new Object2DoubleOpenHashMap<>();
			final Object2DoubleMap<IClusterPair> enrichments = new Object2DoubleOpenHashMap<>();

			for (final ICluster cl1 : counts.keySet()) {
				for (final ICluster cl2 : counts.get(cl1).keySet()) {
					final IClusterPair pair = new ClusterPair(cl1, cl2);
					final int observed = counts.get(cl1).get(cl2);
					edgeCounts.put(pair, observed);

					final double expected = this.getExpectedDirectedEdgesBetweenClustersNodeDegrees(cl1, cl2,
							this.copiedPom.getCopy(), network);
					final double enrichment = Math.log10(observed / expected) / Math.log10(2);
					exp.put(pair, expected);
					enrichments.put(pair, enrichment);
				}
			}
			final ConnectivityResult result = new ConnectivityResult(this.clusteringResult, network,
					mapToOriginalClusters(edgeCounts), mapToOriginalClusters(exp), mapToOriginalClusters(enrichments));
			network.setDirectedEnrichment(result);
			logger.info(" done");
		}
		return network.getDirectedEnrichment();
	}

	private Object2IntMap<IClusterPair> mapToOriginalClusters(final Object2IntMap<IClusterPair> input) {
		final Object2IntMap<IClusterPair> result = new Object2IntOpenHashMap<>();
		for (IClusterPair p : input.keySet())
			result.put(new ClusterPair(this.copiedPom.getOriginal(p.getFirst()),
					this.copiedPom.getOriginal(p.getSecond())), input.getInt(p));

		return result;
	}

	private Object2DoubleMap<IClusterPair> mapToOriginalClusters(final Object2DoubleMap<IClusterPair> input) {
		final Object2DoubleMap<IClusterPair> result = new Object2DoubleOpenHashMap<>();
		for (IClusterPair p : input.keySet())
			result.put(new ClusterPair(this.copiedPom.getOriginal(p.getFirst()),
					this.copiedPom.getOriginal(p.getSecond())), input.getDouble(p));

		return result;
	}

	private <T> Map<IClusterPair, T> mapToOriginalClusters(final Map<IClusterPair, T> input) {
		final Map<IClusterPair, T> result = new HashMap<>();
		for (IClusterPair p : input.keySet())
			result.put(new ClusterPair(this.copiedPom.getOriginal(p.getFirst()),
					this.copiedPom.getOriginal(p.getSecond())), input.get(p));

		return result;
	}

	protected ConnectivityResult getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(
			final TiconeNetworkImpl network) {
		if (network.getUndirectedEnrichment() == null) {
			logger.info("Calculating undirected connectivity enrichment ...");
			final Object2IntMap<IClusterPair> counts = this.getUndirectedEdgesBetweenClusters(this.entrezToClusters,
					this.selectedClusters, network);
			final Object2DoubleMap<IClusterPair> exp = new Object2DoubleOpenHashMap<>();
			final Object2DoubleMap<IClusterPair> enrichments = new Object2DoubleOpenHashMap<>();

			for (final IClusterPair clusterPair : counts.keySet()) {
				final ICluster cl1 = clusterPair.getFirst();
				final ICluster cl2 = clusterPair.getSecond();
				final double expected = this.getExpectedUndirectedEdgesBetweenClustersNodeDegrees(cl1, cl2,
						this.copiedPom.getCopy(), network);
				double enrichment;
				final int observed = counts.get(clusterPair);
				// log2
				enrichment = Math.log10(observed / expected) / Math.log10(2);
				// enrichment = observed / expected - 1;
				exp.put(clusterPair, expected);
				enrichments.put(clusterPair, enrichment);
			}

			final ConnectivityResult result = new ConnectivityResult(this.clusteringResult, network,
					mapToOriginalClusters(counts), mapToOriginalClusters(exp), mapToOriginalClusters(enrichments));
			network.setUndirectedEnrichment(result);
			logger.info("done");
		}

		return network.getUndirectedEnrichment();
	}

	private Object2IntMap<IClusterPair> getUndirectedEdgesBetweenClusters(final Map<String, ICluster> entrezToClusters,
			final Collection<ICluster> selectedClustersCopied, final TiconeNetworkImpl network) {
		final BitMatrix connectedNodes = network.getConnectedNodesArray(true);

		final Object2IntMap<IClusterPair> counts = new Object2IntOpenHashMap<>();

		// make sure we have all cluster pairs in the map
		for (final ICluster c1 : selectedClustersCopied) {
			for (final ICluster c2 : selectedClustersCopied) {
				counts.put(new ClusterPair(c1, c2), 0);
			}
		}

		connectedNodes.forEachCoordinateInState(true, new IntIntProcedure() {

			int lastSourceId = -1, lastTargetId = -1;
			TiconeNetworkNodeImpl sourceNode, targetNode;
			ICluster sourceNet, targetNet;

			@Override
			public boolean apply(final int n1, final int n2) {
				if (n1 != this.lastSourceId) {
					this.lastSourceId = n1;
					this.sourceNode = network.getNetworkWideNodeIdToNode().get(n1);
				}
				if (this.sourceNode == null)
					return true;
				else {
					final String sourceEntrezId = this.sourceNode.getName();
					this.sourceNet = entrezToClusters.get(sourceEntrezId);
				}

				if (n2 != this.lastTargetId) {
					this.lastTargetId = n2;
					this.targetNode = network.getNetworkWideNodeIdToNode().get(n2);
				}
				if (this.targetNode == null)
					return true;
				else {
					final String targetEntrezId = this.targetNode.getName();
					this.targetNet = entrezToClusters.get(targetEntrezId);
				}

				if (!selectedClustersCopied.contains(this.sourceNet)
						|| !selectedClustersCopied.contains(this.targetNet))
					return true;

				final IClusterPair p1 = new ClusterPair(this.sourceNet, this.targetNet);
				final IClusterPair p2 = new ClusterPair(this.targetNet, this.sourceNet);
				if (!counts.containsKey(p1)) {
					counts.put(p1, 0);
				}
				counts.put(p1, counts.getInt(p1) + 1);

				if (!(this.sourceNet.equals(this.targetNet))) {
					if (!counts.containsKey(p2)) {
						counts.put(p2, 0);
					}
					counts.put(p2, counts.getInt(p2) + 1);
				}

				return true;
			}
		});
		return counts;
	}

	protected ITimeSeriesObjects removeObjectsNotInNetwork(final TiconeNetworkImpl network)
			throws ClusterConnectivityException {
		// remove objects from clustering, which are not in network
		final ITimeSeriesObjects objectsToRemove = new TimeSeriesObjectList();
		for (final ITimeSeriesObject tsd : this.copiedPom.getCopy().getAllObjects())
			if (!network.containsNode(tsd.getName())) {
				objectsToRemove.add(tsd);
			}
		this.copiedPom.getCopy().removeObjects(objectsToRemove);

		if (this.copiedPom.getCopy().getAllObjects().isEmpty()) {
			throw new ClusterConnectivityException("There are 0 matching IDs in the data set and network. "
					+ "Connectivity can only be analyzed if there is a reasonable overlap.");
		}
		logger.info("Removed " + objectsToRemove.size() + " objects from data set not contained in network.");
		return objectsToRemove;
	}

	protected Set<TiconeNetworkNodeImpl> removeNodesNotInDataset(final TiconeNetworkImpl network)
			throws ClusterConnectivityException {
		final Set<TiconeNetworkNodeImpl> removedNodes = new HashSet<>();
		// remove nodes from network, which are not in the dataset
		for (final TiconeNetworkNodeImpl node : new HashSet<>(network.getNodeSet())) {
			if (this.copiedPom.getCopy().getTimeSeriesData(node.getName()) == null) {
				network.removeNode(node);
				removedNodes.add(node);
			}
		}
		if (network.getNodeSet().isEmpty()) {
			throw new ClusterConnectivityException("There are 0 matching IDs in the data set and network. "
					+ "Connectivity can only be analyzed if there is a reasonable overlap.");
		}
		logger.info("Removed " + removedNodes.size() + " nodes from network not contained in dataset.");
		return removedNodes;
	}

	private double getExpectedDirectedEdgesBetweenClustersNodeDegrees(final ICluster copiedCluster1,
			final ICluster copiedCluster2, final IClusterObjectMapping pom, final TiconeNetworkImpl network) {
		final double[][] probs = network.getTotalDirectedEdgeProbabilityNodeDegrees();
		final List<int[]> degs = new ArrayList<>();
		for (int i = 0; i < probs.length; i++) {
			for (int j = 0; j < probs[i].length; j++) {
				if (probs[i][j] > 0)
					degs.add(new int[] { i, j });
			}
		}

		double prob = 0.0;
		for (final ITimeSeriesObject tsData1 : copiedCluster1.getObjects()) {
			final TiconeNetworkNodeImpl node1 = network.getNode(tsData1.getName());
			final int degree1 = network.getUndirectedNodeDegree(node1);
			for (final ITimeSeriesObject tsData2 : copiedCluster2.getObjects()) {
				final TiconeNetworkNodeImpl node2 = network.getNode(tsData2.getName());
				final int degree2 = network.getUndirectedNodeDegree(node2);
				prob += probs[degree1][degree2];
			}
		}
		return prob;
	}

	private double getExpectedUndirectedEdgesBetweenClustersNodeDegrees(final ICluster copiedCluster1,
			final ICluster copiedCluster2, final IClusterObjectMapping pom, final TiconeNetworkImpl network) {
		final double[][] probs = network.getTotalUndirectedEdgeProbabilityNodeDegrees();
		final List<int[]> degs = new ArrayList<>();
		for (int i = 0; i < probs.length; i++) {
			for (int j = 0; j < probs[i].length; j++) {
				if (probs[i][j] > 0)
					degs.add(new int[] { i, j });
			}
		}
		if (copiedCluster1.equals(copiedCluster2)) {
			double prob = 0.0;
			final ITimeSeriesObjectList nodes = copiedCluster1.getObjects().asList();
			for (int i = 0; i < nodes.size(); i++) {
				final ITimeSeriesObject tsData1 = nodes.get(i);
				final TiconeNetworkNodeImpl node1 = network.getNode(tsData1.getName());
				final int degree1 = network.getUndirectedNodeDegree(node1);
				for (int j = 0; j <= i; j++) {
					final ITimeSeriesObject tsData2 = nodes.get(j);
					final TiconeNetworkNodeImpl node2 = network.getNode(tsData2.getName());
					final int degree2 = network.getUndirectedNodeDegree(node2);

					final int minDegree = Math.min(degree1, degree2);
					final int maxDegree = Math.max(degree1, degree2);

					prob += probs[minDegree][maxDegree];
				}
			}
			return prob;
		} else {
			double prob = 0.0;
			for (final ITimeSeriesObject tsData1 : copiedCluster1.getObjects()) {
				final TiconeNetworkNodeImpl node1 = network.getNode(tsData1.getName());
				final int degree1 = network.getUndirectedNodeDegree(node1);
				for (final ITimeSeriesObject tsData2 : copiedCluster2.getObjects()) {
					final TiconeNetworkNodeImpl node2 = network.getNode(tsData2.getName());
					final int degree2 = network.getUndirectedNodeDegree(node2);

					final int minDegree = Math.min(degree1, degree2);
					final int maxDegree = Math.max(degree1, degree2);

					prob += probs[minDegree][maxDegree];
				}
			}
			return prob;
		}
	}

	protected ConnectivityPValueResult calculatePValues(final TiconeNetworkImpl network)
			throws ClusterConnectivityException, InterruptedException, NotAnArithmeticFeatureValueException,
			ToNumberConversionException {
		return this.calculatePValues(network, false);
	}

	protected ConnectivityPValueResult calculatePValues(final TiconeNetworkImpl network,
			final boolean storePermutationEdgeCounts) throws ClusterConnectivityException, InterruptedException {
		try {
			this.clusterPairFeatureStore = new BasicFeatureStore();
			final List<IFitnessScore> fitnessScores = new ArrayList<>();
			final List<Boolean> fitnessScoreTwoSided = new ArrayList<>();
			final IClusterPairFeatureNumberUndirectedConnectingEdges cfUEdges = new ClusterPairFeatureNumberUndirectedConnectingEdges();
			cfUEdges.setFeatureValueProvider(network);
			// cfUEdges.setScaleByClusterSizes(true);
			final IClusterPairFeatureNumberDirectedConnectingEdges cfDEdges = new ClusterPairFeatureNumberDirectedConnectingEdges();
			cfDEdges.setFeatureValueProvider(network);
			// cfDEdges.setScaleByClusterSizes(true);

			final ClusterObjectMappingPair clusteringPair = new ClusterObjectMappingPair(this.copiedPom.getCopy(),
					this.copiedPom.getCopy());

			logger.info("Calculating original feature values ...");
			for (final IArithmeticFeature<? extends Comparable<?>> f : new IArithmeticFeature[] { cfUEdges,
					cfDEdges }) {
				// fill the cluster feature store
				for (final IClusterPair c : clusteringPair.getClusterPairs()) {
					try {
						this.clusterPairFeatureStore.setFeatureValue(c, (IFeature) f, f.calculate(c));
					} catch (FeatureCalculationException | IncorrectlyInitializedException
							| IncompatibleFeatureAndObjectException e1) {
						throw new ClusterConnectivityException(e1);
					}
				}

				final IArithmeticFeature<? extends Comparable<?>>[] asList = new IArithmeticFeature[] { f };

				final IFitnessScore fitnessScore = new BasicFitnessScore(ObjectType.CLUSTER_PAIR, asList, null,
						new double[] { 1.0 });
				fitnessScores.add(fitnessScore);
				fitnessScoreTwoSided.add(this.twoSidedPermutationTest);
			}
			logger.info(" done");

			this.calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER_PAIR),
					this.clusteringPairShuffle != null ? this.clusteringPairShuffle : this.networkShuffle,
					fitnessScores, new List[] { new ArrayList<>() }, fitnessScoreTwoSided, new MultiplyPValues(),
					this.numberPermutations);
			this.calculatePvalues.setFeatureStore(this.clusterPairFeatureStore);
			this.calculatePvalues.setStorePermutedFeatureValues(true);

			this.calculatePvalues.addChangeListener(new ChangeListener() {

				@Override
				public void stateChanged(final ChangeEvent e) {
					if (e instanceof PermutationTestChangeEvent) {
						final PermutationTestChangeEvent ev = (PermutationTestChangeEvent) e;
						try {
							TiconeClusterConnectivityTask.this.fireProgress("", "Calculating p-values: Permutation "
									+ ev.getFinishedPermutations() + " of " + ev.getTotalPermutations(),
									ev.getPercentage());
						} finally {
							logger.info("Permutation done");
						}
					}
				}
			});

			final long seed = this.clusteringResult.getSeed();
			final PValueCalculationResult calculatePValues2 = this.calculatePvalues.calculatePValues(clusteringPair,
					seed);

			final Map<IClusterPair, IPvalue> twosidedPvaluesDirected = new HashMap<>();
			final Map<IClusterPair, IPvalue> twosidedPvaluesUndirected = new HashMap<>();

			for (final IClusterPair k : calculatePValues2.getObjects(ObjectType.CLUSTER_PAIR)) {
				for (final IFitnessScore fs : calculatePValues2.getFitnessScores(ObjectType.CLUSTER_PAIR)) {
					final IPvalue pval = calculatePValues2.getPValue(fs, k);
					if (fs.getFeatures()[0] instanceof ClusterPairFeatureNumberDirectedConnectingEdges)
						twosidedPvaluesDirected.put(k, pval);
					if (fs.getFeatures()[0] instanceof ClusterPairFeatureNumberUndirectedConnectingEdges)
						twosidedPvaluesUndirected.put(k, pval);
				}
			}

			final IClusterPairFeatureNumberDirectedConnectingEdges directedEdgesFeature = new ClusterPairFeatureNumberDirectedConnectingEdges();
			final IClusterPairFeatureNumberUndirectedConnectingEdges undirectedEdgesFeature = new ClusterPairFeatureNumberUndirectedConnectingEdges();

			final IFeatureStore featureStore = calculatePValues2.getFeatureStore();

			// calculate expected values as mean of observed values across all
			// permutations and then store enrichments in maps
			final Object2IntMap<IClusterPair> directedEdgeCounts = new Object2IntOpenHashMap<>(),
					undirectedEdgeCounts = new Object2IntOpenHashMap<>();

			// TODO: condition this again on the joined node degrees?
			double expectedDirectedEdgeCount = Double.NEGATIVE_INFINITY,
					expectedUndirectedEdgeCount = Double.NEGATIVE_INFINITY;

			final Object2DoubleMap<IClusterPair> directedEdgeCountExpected = new Object2DoubleOpenHashMap<>();
			final Object2DoubleMap<IClusterPair> undirectedEdgeCountExpected = new Object2DoubleOpenHashMap<>();

			final Object2DoubleMap<IClusterPair> directedEdgeCountEnrichment = new Object2DoubleOpenHashMap<>();
			final Object2DoubleMap<IClusterPair> undirectedEdgeCountEnrichment = new Object2DoubleOpenHashMap<>();

			final boolean objectSpecific = calculatePValues2
					.hasObjectSpecificPermutedFeatureValues(ObjectType.CLUSTER_PAIR);

			final boolean calculateDirected = this.calculatePvalues.getFeatures().contains(directedEdgesFeature);
			final boolean calculatedUndirected = this.calculatePvalues.getFeatures().contains(undirectedEdgesFeature);

			Map<IClusterPair, IntList> permutationEdgeCountsDirected = null, permutationEdgeCountsUndirected = null;

			if (objectSpecific) {
				permutationEdgeCountsDirected = new HashMap<>();
				permutationEdgeCountsUndirected = new HashMap<>();
				for (final IClusterPair k : calculatePValues2.getObjects(ObjectType.CLUSTER_PAIR)) {
					IntList edgeCounts = new IntArrayList();
					try {
						for (final IFeatureValue<? extends Comparable<?>> d : calculatePValues2
								.getPermutedFeatureValuesObjectSpecific(k, directedEdgesFeature))
							edgeCounts.add(d.toNumber().intValue());
					} catch (NullPointerException e) {
						e.printStackTrace();
					}
					permutationEdgeCountsDirected.put(k, edgeCounts);

					edgeCounts = new IntArrayList();
					for (final IFeatureValue<? extends Comparable<?>> d : calculatePValues2
							.getPermutedFeatureValuesObjectSpecific(k, undirectedEdgesFeature))
						edgeCounts.add(d.toNumber().intValue());
					permutationEdgeCountsUndirected.put(k, edgeCounts);
				}
				permutationEdgeCountsDirected = mapToOriginalClusters(permutationEdgeCountsDirected);
				permutationEdgeCountsUndirected = mapToOriginalClusters(permutationEdgeCountsUndirected);
			}

			if (!objectSpecific) {
				if (calculateDirected) {
					List<IFeatureValue<Double>> list = calculatePValues2
							.getPermutedFeatureValues(ObjectType.CLUSTER_PAIR, directedEdgesFeature);
					double[] doubles = new double[list.size()];
					for (int i = 0; i < list.size(); i++)
						doubles[i] = list.get(i).toNumber().doubleValue();
					expectedDirectedEdgeCount = ArraysExt.mean(doubles);
				}
				if (calculatedUndirected) {
					List<IFeatureValue<Double>> list = calculatePValues2
							.getPermutedFeatureValues(ObjectType.CLUSTER_PAIR, undirectedEdgesFeature);
					double[] doubles = new double[list.size()];
					for (int i = 0; i < list.size(); i++)
						doubles[i] = list.get(i).toNumber().doubleValue();
					expectedUndirectedEdgeCount = ArraysExt.mean(doubles);
				}
			}

			for (final IClusterPair k : featureStore.keySet(ObjectType.CLUSTER_PAIR)) {
				int dirScaleFactor;
				if (!directedEdgesFeature.isScaleByClusterSizes())
					dirScaleFactor = 1;
				else if (k.getFirst().equals(k.getSecond()))
					dirScaleFactor = k.getFirst().getObjects().size() * k.getFirst().getObjects().size();
				else
					dirScaleFactor = k.getFirst().getObjects().size() * k.getSecond().getObjects().size() * 2;
				int undirScaleFactor;
				if (!undirectedEdgesFeature.isScaleByClusterSizes())
					undirScaleFactor = 1;
				else if (k.getFirst().equals(k.getSecond()))
					undirScaleFactor = k.getFirst().getObjects().size() * (k.getFirst().getObjects().size() - 1) / 2
							// self edges
							+ k.getFirst().getObjects().size();
				else
					undirScaleFactor = k.getFirst().getObjects().size() * k.getSecond().getObjects().size();

				final int dirEdgeCount = (int) (featureStore.getFeatureValueMap(directedEdgesFeature).get(k).getValue()
						* dirScaleFactor);
				directedEdgeCounts.put(k, dirEdgeCount);

				final int undirEdgeCount = (int) (featureStore.getFeatureValueMap(undirectedEdgesFeature).get(k)
						.getValue() * undirScaleFactor);
				undirectedEdgeCounts.put(k, undirEdgeCount);

				double expDirEdgeCount = Double.NaN;
				double expUndirEdgeCount = Double.NaN;

				if (objectSpecific) {
					if (calculateDirected) {
						final List<IFeatureValue<Double>> permutedDirectedEdges = calculatePValues2
								.getPermutedFeatureValuesObjectSpecific(k, directedEdgesFeature);
						expDirEdgeCount = permutedDirectedEdges.stream().mapToDouble(v -> v.getValue().doubleValue())
								.average().getAsDouble() * dirScaleFactor;
					}

					if (calculatedUndirected) {
						final List<IFeatureValue<Double>> permutedUndirectedEdges = calculatePValues2
								.getPermutedFeatureValuesObjectSpecific(k, undirectedEdgesFeature);
						expUndirEdgeCount = permutedUndirectedEdges.stream()
								.mapToDouble(v -> v.getValue().doubleValue()).average().getAsDouble()
								* undirScaleFactor;
					}
				} else {
					if (calculateDirected) {
						expDirEdgeCount = expectedDirectedEdgeCount * dirScaleFactor;
					}

					if (calculatedUndirected) {
						expUndirEdgeCount = expectedUndirectedEdgeCount * undirScaleFactor;
					}
				}

				if (calculateDirected) {
					directedEdgeCountExpected.put(k, expDirEdgeCount);
					directedEdgeCountEnrichment.put(k, Math.log10(dirEdgeCount / expDirEdgeCount) / Math.log10(2));
				}

				if (calculatedUndirected) {
					undirectedEdgeCountExpected.put(k, expUndirEdgeCount);
					undirectedEdgeCountEnrichment.put(k, undirEdgeCount / expUndirEdgeCount);
					undirectedEdgeCountEnrichment.put(k,
							Math.log10(undirEdgeCount / expUndirEdgeCount) / Math.log10(2));
				}
			}

			if (calculateDirected) {
				final ConnectivityResult result = new ConnectivityResult(this.clusteringResult, network,
						mapToOriginalClusters(directedEdgeCounts), mapToOriginalClusters(directedEdgeCountExpected),
						mapToOriginalClusters(directedEdgeCountEnrichment));
				network.setDirectedEnrichment(result);
			}

			if (calculatedUndirected) {
				final ConnectivityResult result = new ConnectivityResult(this.clusteringResult, network,
						mapToOriginalClusters(undirectedEdgeCounts), mapToOriginalClusters(undirectedEdgeCountExpected),
						mapToOriginalClusters(undirectedEdgeCountEnrichment));
				network.setUndirectedEnrichment(result);
			}

			final ConnectivityPValueResult edgeCrossoverConnectivityResult = new ConnectivityPValueResult(
					this.clusteringResult, network, mapToOriginalClusters(directedEdgeCounts),
					mapToOriginalClusters(undirectedEdgeCounts), mapToOriginalClusters(twosidedPvaluesDirected),
					mapToOriginalClusters(twosidedPvaluesUndirected), mapToOriginalClusters(twosidedPvaluesDirected),
					mapToOriginalClusters(twosidedPvaluesUndirected), mapToOriginalClusters(twosidedPvaluesDirected),
					mapToOriginalClusters(twosidedPvaluesUndirected), null, this.twoSidedPermutationTest,
					this.numberPermutations,
					this.networkShuffle != null ? this.networkShuffle : this.clusteringPairShuffle,
					permutationEdgeCountsDirected, permutationEdgeCountsUndirected);

			return edgeCrossoverConnectivityResult;
		} catch (final PValueCalculationException | IncompatibleFeatureValueProviderException
				| NotAnArithmeticFeatureValueException | ToNumberConversionException e1) {
			throw new ClusterConnectivityException(e1);
		}
	}

	@Override
	public void cancel() {
		super.cancel();
		this.calculatePvalues.cancel();
	}
}