package dk.sdu.imada.ticone.connectivity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import dk.sdu.imada.ticone.clustering.AbstractNamedTiconeResult;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;

public class ConnectivityResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3577382555771153981L;
	protected AbstractNamedTiconeResult util;
	protected ITiconeNetwork network;
	protected String networkName;
	protected Object2IntMap<IClusterPair> edgeCount;
	protected Object2DoubleMap<IClusterPair> expectedEdgeCount;
	protected Object2DoubleMap<IClusterPair> edgeCountEnrichment;

	public ConnectivityResult(final AbstractNamedTiconeResult util, final ITiconeNetwork network,
			final Object2IntMap<IClusterPair> edgeCount, final Object2DoubleMap<IClusterPair> expectedEdgeCount,
			final Object2DoubleMap<IClusterPair> edgeCountEnrichment) {
		super();
		this.util = util;
		this.networkName = network.getName();
		this.edgeCount = edgeCount;
		this.expectedEdgeCount = expectedEdgeCount;
		this.edgeCountEnrichment = edgeCountEnrichment;
	}

	public AbstractNamedTiconeResult getUtil() {
		return this.util;
	}

	public String getNetworkName() {
		return this.networkName;
	}

	public Object2IntMap<IClusterPair> getEdgeCount() {
		return this.edgeCount;
	}

	public Object2DoubleMap<IClusterPair> getEdgeCountEnrichment() {
		return this.edgeCountEnrichment;
	}

	public Object2DoubleMap<IClusterPair> getExpectedEdgeCount() {
		return this.expectedEdgeCount;
	}

	private void readObject(final ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		// deserialize of older version: use string instead of network;
		if (this.networkName == null && this.network != null) {
			this.networkName = this.network.getName();
			this.network = null;
		}
	}
}
