/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPairList;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairsFactory;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureMedianSimilarity;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePrototypeSimilarity;
import dk.sdu.imada.ticone.feature.ClusterPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.FeatureValueProviderInitializationException;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.ISimilarityFeature;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.ObjectClusterPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.ObjectPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.similarity.AbstractWeightedCompositeSimilarityFunction.CompositeSimilarityValue;
import dk.sdu.imada.ticone.similarity.AbstractWeightedCompositeSimilarityFunction.SpecialCompositeSimilarityValue;
import dk.sdu.imada.ticone.util.BoundIterable;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IIterableWithSize;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.IPairBuilder;
import dk.sdu.imada.ticone.util.IPairsFactory;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IndexUtils;
import dk.sdu.imada.ticone.util.Iterables;
import dk.sdu.imada.ticone.util.MyParallel;
import dk.sdu.imada.ticone.util.MyParallel.BatchCount;
import dk.sdu.imada.ticone.util.MyParallel.Operation;
import dk.sdu.imada.ticone.util.MyScheduledThreadPoolExecutor;
import dk.sdu.imada.ticone.util.Pair;
import dk.sdu.imada.ticone.util.PairIndexIterable;
import dk.sdu.imada.ticone.util.TriFunction;
import dk.sdu.imada.ticone.util.TriPredicate;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;
import dk.sdu.imada.ticone.util.Utility;
import it.unimi.dsi.fastutil.BigArrays;
import it.unimi.dsi.fastutil.BigList;
import it.unimi.dsi.fastutil.BigSwapper;
import it.unimi.dsi.fastutil.Size64;
import it.unimi.dsi.fastutil.Swapper;
import it.unimi.dsi.fastutil.booleans.BooleanBigArrayBigList;
import it.unimi.dsi.fastutil.booleans.BooleanBigList;
import it.unimi.dsi.fastutil.doubles.DoubleBigArrayBigList;
import it.unimi.dsi.fastutil.doubles.DoubleBigList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntComparator;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongBigArrayBigList;
import it.unimi.dsi.fastutil.longs.LongBigList;
import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongComparator;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongList;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import it.unimi.dsi.fastutil.objects.ObjectBigList;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectList;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 26, 2018
 *
 */
public abstract class AbstractSimilarityFunction implements ISimilarityFunction {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9032303891007572829L;

	protected transient Logger logger;

	protected final ReadWriteLock rwLock = new ReentrantReadWriteLock();

	private final boolean isCopy;
	protected final boolean useCache;
	protected final IFeatureStore cache;
	// to look up calculated value in feature store
	protected final Map<ObjectType<?>, ISimilarityFeature> similarityFeatures;
	private transient ScheduledThreadPoolExecutor privateSimilarityFunctionThreadPool;

	private HashSet<ObjectType<?>> supportedObjectTypes;

	private final Map<ObjectType<?>, Collection<? extends IFeatureWithValueProvider>> providedFeaturesByObjectType;

	public static final int MIN_BATCH_SIZE = 50000;

	/**
	 * 
	 */
	public AbstractSimilarityFunction(boolean useCache) {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.isCopy = false;

		IFeatureStore cache = null;
		Map<ObjectType<?>, ISimilarityFeature> similarityFeatures = null;
		useCache = false;
		if (useCache) {
			try {
				cache = new BasicFeatureStore(1, 100000);
				similarityFeatures = new HashMap<>();
				similarityFeatures.put(ObjectType.OBJECT_PAIR, new ObjectPairSimilarityFeature(this));
				similarityFeatures.put(ObjectType.CLUSTER_PAIR, new ClusterPairSimilarityFeature(this));
				similarityFeatures.put(ObjectType.OBJECT_CLUSTER_PAIR, new ObjectClusterPairSimilarityFeature(this));
			} catch (IncompatibleSimilarityFunctionException e) {
				cache = null;
				similarityFeatures = null;
				useCache = false;
			}
		}
		this.useCache = useCache;
		this.similarityFeatures = similarityFeatures;
		this.cache = cache;

		this.supportedObjectTypes = new HashSet<>(
				Arrays.asList(ObjectType.CLUSTER_PAIR, ObjectType.OBJECT_CLUSTER_PAIR, ObjectType.OBJECT_PAIR));

		this.providedFeaturesByObjectType = new HashMap<>();
		this.initProvidedFeaturesByObjectType();

		initThreadPool();
	}

	private void initProvidedFeaturesByObjectType() {
		try {
			this.providedFeaturesByObjectType.clear();
			this.providedFeaturesByObjectType.put(ObjectType.CLUSTER,
					Arrays.asList(new ClusterFeatureAverageSimilarity(this), new ClusterFeatureMedianSimilarity(this)));
			this.providedFeaturesByObjectType.put(ObjectType.CLUSTER_PAIR, Arrays
					.asList(new ClusterPairFeaturePrototypeSimilarity(this), new ClusterPairSimilarityFeature(this)));
			this.providedFeaturesByObjectType.put(ObjectType.OBJECT_PAIR,
					Arrays.asList(new ObjectPairSimilarityFeature(this)));
			this.providedFeaturesByObjectType.put(ObjectType.OBJECT_CLUSTER_PAIR,
					Arrays.asList(new ObjectClusterPairSimilarityFeature(this)));
			this.providedFeaturesByObjectType.put(ObjectType.CLUSTERING,
					Arrays.asList(new ClusteringFeatureTotalObjectClusterSimilarity(this)));
		} catch (IncompatibleSimilarityFunctionException e) {
			throw new FeatureValueProviderInitializationException(this, e);
		}
	}

	public AbstractSimilarityFunction(final AbstractSimilarityFunction other) {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.isCopy = true;
		this.useCache = other.useCache;
		this.cache = other.cache;
		this.similarityFeatures = other.similarityFeatures;

		this.supportedObjectTypes = other.supportedObjectTypes;

		this.providedFeaturesByObjectType = new HashMap<>();
		this.initProvidedFeaturesByObjectType();

		this.privateSimilarityFunctionThreadPool = other.privateSimilarityFunctionThreadPool;
	}

	@Override
	public boolean equals(final Object obj) {
		return Objects.equals(this.getClass(), obj.getClass());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getClass());
	}

	private void initThreadPool() {
		this.privateSimilarityFunctionThreadPool = new MyScheduledThreadPoolExecutor(MyParallel.DEFAULT_THREADS,
				new MyThreadFactory(this.getClass()));
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		s.defaultReadObject();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.initThreadPool();
	}

	/**
	 * @return the privateSimilarityFunctionThreadPool
	 */
	protected ScheduledThreadPoolExecutor getPrivateSimilarityFunctionThreadPool() {
		return this.privateSimilarityFunctionThreadPool;
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		// only the instance that originally instantiated the executor should shut it
		// down
		this.privateSimilarityFunctionThreadPool = null;
//		if (!this.isCopy)
//			this.privateSimilarityFunctionThreadPool.shutdownNow();
	}

	@Override
	public abstract AbstractSimilarityFunction copy();

	@Override
	public int compare(final ISimilarityValue s1, final ISimilarityValue s2) {
		return s1.compareTo(s2);
	}

	@Override
	public <O1, O2, P extends IObjectWithFeatures & IPair<O1, O2>> ISimilarityValuesSomePairs<O1, O2, P> emptySimilarities(
			final Collection<P> objectPairs, final ObjectType<P> ofType) {
		return new SimilarityValuesSomePairs<>(objectPairs, ofType);
	}

	@Override
	public <O1, O2, P extends IObjectWithFeatures & IPair<O1, O2>> ISimilarityValuesAllPairs<O1, O2, P> emptySimilarities(
			final O1[] objects1, final O2[] objects2, final IPairsFactory<O1, O2, P> factory,
			final ObjectType<P> ofType) {
		return new SimilarityValuesAllPairs<>(objects1, objects2, factory, ofType);
	}

	@Override
	public <O extends IObjectWithFeatures & IPair<?, ?>> ISimilarityValue calculateSimilarity(O object)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		if (!providesValuesForObjectTypes().contains(object.getObjectType()))
			throw new IncompatibleObjectTypeException();
		ISimilarityFeature f = null;
		if (this.useCache) {
			f = this.similarityFeatures.get(object.getObjectType());
			if (this.cache.hasFeatureFor(f, object))
				return this.cache.getFeatureValue(object, f).getValue();

		}
		final ISimilarityValue result = doCalculateSimilarity(object);
		if (this.useCache) {
			this.cache.setFeatureValue(object, f, f.value(result));
		}
		return result;
	}

	/**
	 * 
	 * @param object
	 * @return A similarity value between 0.0 and 1.0.
	 * @throws IncompatibleObjectTypeException
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 */
	protected abstract <O extends IObjectWithFeatures & IPair<?, ?>> ISimilarityValue doCalculateSimilarity(O object)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException;

	@Override
	public Collection<ObjectType<?>> providesValuesForObjectTypes() {
		return supportedObjectTypes;
	}

	@Override
	public Collection<? extends IFeatureWithValueProvider> featuresProvidedValuesFor(ObjectType<?> objectType)
			throws IncompatibleFeatureValueProviderException {
		if (this.providedFeaturesByObjectType.containsKey(objectType))
			return this.providedFeaturesByObjectType.get(objectType);
		return ISimilarityFunction.super.featuresProvidedValuesFor(objectType);
	}

	@Override
	public <V> IFeatureValue<V> getFeatureValue(IFeature<V> feature, IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException, UnknownObjectFeatureValueProviderException,
			FeatureCalculationException, InterruptedException, IncompatibleFeatureAndObjectException {
		try {
			if (feature instanceof ClusterFeatureAverageSimilarity) {
				return (IFeatureValue<V>) getFeatureValue((ClusterFeatureAverageSimilarity) feature, object);
			} else if (feature instanceof ClusterFeatureMedianSimilarity) {
				return (IFeatureValue<V>) getFeatureValue((ClusterFeatureMedianSimilarity) feature, object);
			} else if (feature instanceof ClusteringFeatureTotalObjectClusterSimilarity) {
				return (IFeatureValue<V>) getFeatureValue((ClusteringFeatureTotalObjectClusterSimilarity) feature,
						object);
			} else if (feature instanceof ClusterPairFeaturePrototypeSimilarity) {
				return (IFeatureValue<V>) getFeatureValue((ClusterPairFeaturePrototypeSimilarity) feature, object);
			} else if (feature instanceof ClusterPairSimilarityFeature) {
				return (IFeatureValue<V>) getFeatureValue((ClusterPairSimilarityFeature) feature, object);
			} else if (feature instanceof ObjectClusterPairSimilarityFeature) {
				return (IFeatureValue<V>) getFeatureValue((ObjectClusterPairSimilarityFeature) feature, object);
			} else if (feature instanceof ObjectPairSimilarityFeature) {
				return (IFeatureValue<V>) getFeatureValue((ObjectPairSimilarityFeature) feature, object);
			}
		} catch (IncompatibleObjectTypeException e) {
			throw new IncompatibleFeatureAndObjectException();
		}
		throw new IncompatibleFeatureValueProviderException(this, feature);
	}

	/**
	 * @param feature
	 * @param object
	 * @return
	 * @throws InterruptedException
	 * @throws IncompatibleObjectTypeException
	 * @throws SimilarityCalculationException
	 */
	private IFeatureValue<ISimilarityValue> getFeatureValue(ObjectPairSimilarityFeature feature,
			IObjectWithFeatures object)
			throws FeatureCalculationException, IncompatibleObjectTypeException, InterruptedException {
		try {
			return feature.value(this.calculateSimilarity((IObjectPair) object));
		} catch (SimilarityCalculationException e) {
			throw new FeatureCalculationException(e);
		}
	}

	/**
	 * @param feature
	 * @param object
	 * @return
	 */
	private IFeatureValue<ISimilarityValue> getFeatureValue(ObjectClusterPairSimilarityFeature feature,
			IObjectWithFeatures object)
			throws FeatureCalculationException, IncompatibleObjectTypeException, InterruptedException {
		try {
			return feature.value(object, this.calculateSimilarity((IObjectClusterPair) object));
		} catch (final SimilarityCalculationException | IncompatibleObjectTypeException e) {
			throw new FeatureCalculationException(e);
		}
	}

	/**
	 * @param feature
	 * @param object
	 * @return
	 */
	private IFeatureValue<ISimilarityValue> getFeatureValue(ClusterPairSimilarityFeature feature,
			IObjectWithFeatures object)
			throws FeatureCalculationException, IncompatibleObjectTypeException, InterruptedException {
		try {
			return feature.value(object, this.calculateSimilarity((IClusterPair) object));
		} catch (final SimilarityCalculationException | IncompatibleObjectTypeException e) {
			throw new FeatureCalculationException(e);
		}
	}

	/**
	 * @param feature
	 * @param object
	 * @return
	 */
	private IFeatureValue<ISimilarityValue> getFeatureValue(ClusterPairFeaturePrototypeSimilarity feature,
			IObjectWithFeatures object)
			throws FeatureCalculationException, IncompatibleObjectTypeException, InterruptedException {
		try {
			return feature.value(this.calculateSimilarity((IClusterPair) object));
		} catch (final SimilarityCalculationException | IncompatibleObjectTypeException e) {
			throw new FeatureCalculationException(e);
		}
	}

	/**
	 * @param feature
	 * @param object
	 * @return
	 */
	private IFeatureValue<ISimilarityValue> getFeatureValue(ClusteringFeatureTotalObjectClusterSimilarity feature,
			IObjectWithFeatures object)
			throws FeatureCalculationException, IncompatibleObjectTypeException, InterruptedException {
		final IClusterObjectMapping clustering = (IClusterObjectMapping) object;
		try {
			final IObjectClusterPairList pairs = clustering.getObjectClusterPairs().asList();
			final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = this
					.calculateSimilarities(pairs, ObjectType.OBJECT_CLUSTER_PAIR);
			final Map<ICluster, ? extends ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair>> simsPerCluster = sims
					.partitionBySecondObject();

			ISimilarityValue totalAverageClusterSim = this.missingValuePlaceholder();
			List<ICluster> clusters = new ArrayList<>(clustering.getClusters());
			Collections.sort(clusters, new Comparator<ICluster>() {
				@Override
				public int compare(ICluster o1, ICluster o2) {
					return o1.getClusterNumber() - o2.getClusterNumber();
				}
			});
			for (final ICluster cluster : clusters) {
				ISimilarityValue sumSim;
				if (simsPerCluster.containsKey(cluster)) {
					final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> clusterSims = simsPerCluster
							.get(cluster);
					sumSim = clusterSims.sum();
				} else {
					sumSim = this.missingValuePlaceholder();
				}
				totalAverageClusterSim = totalAverageClusterSim.plus(sumSim);
			}
			return feature.value(object, totalAverageClusterSim);
		} catch (SimilarityCalculationException | SimilarityValuesException | IncompatibleSimilarityValueException e) {
			throw new FeatureCalculationException(e);
		}
	}

	/**
	 * @param feature
	 * @param object
	 * @return
	 */
	private IFeatureValue<ISimilarityValue> getFeatureValue(ClusterFeatureMedianSimilarity feature,
			IObjectWithFeatures object)
			throws IncompatibleObjectTypeException, InterruptedException, FeatureCalculationException {
		final ICluster cluster = ObjectType.CLUSTER.getBaseType().cast(object);
		IFeatureValue<ISimilarityValue> avgSim;
		try {
			final ITimeSeriesObject[] objects = cluster.getObjects().toArray();
			avgSim = feature.value(cluster, this.calculateSimilarities(objects, cluster.asSingletonList().toArray(),
					new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR).median());
		} catch (SimilarityCalculationException | SimilarityValuesException | IncompatibleSimilarityValueException e) {
			throw new FeatureCalculationException(e);
		}
		return avgSim;
	}

	/**
	 * @param feature
	 * @param object
	 * @return
	 * @throws FeatureCalculationException
	 */
	protected IFeatureValue<ISimilarityValue> getFeatureValue(ClusterFeatureAverageSimilarity feature,
			IObjectWithFeatures object)
			throws IncompatibleObjectTypeException, InterruptedException, FeatureCalculationException {
		final ICluster cluster = ObjectType.CLUSTER.getBaseType().cast(object);
		IFeatureValue<ISimilarityValue> avgSim;
		try {
			final ITimeSeriesObject[] objects = cluster.getObjects().toArray();
			avgSim = feature.value(cluster, this.calculateSimilarities(objects, cluster.asSingletonList().toArray(),
					new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR).mean());
		} catch (SimilarityValuesException | IncompatibleSimilarityValueException | SimilarityCalculationException e) {
			throw new FeatureCalculationException(e);
		}
		return avgSim;
	}

	/**
	 * Collection of similarity values.
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Oct 11, 2018
	 *
	 * @param <O1>
	 */
	public abstract class SimilarityValues<O1, O2, P extends IPair<O1, O2>> implements ISimilarityValues<O1, O2, P> {

		/**
		* 
		*/
		private static final long serialVersionUID = 580926748633424243L;

		protected final boolean hasLargeStorage;
		protected final ISmallSimilarityValueStorage smallStorage;
		protected final ILargeSimilarityValueStorage largeStorage;

		protected ISimilarityValue sumCache, meanCache, medianCache, minCache, maxCache;

		protected long whichMinCache, whichMaxCache;

		/**
		 * A constructor that constructs a {@link ISimilarityValueStorage} matching the
		 * requested number of pairs.
		 * 
		 * @param numberPairs
		 * @param ofType
		 */
		public SimilarityValues(final long numberPairs, final ObjectType<?> ofType) {
			super();

			if (requiresLargeStorage(numberPairs)) {
				this.hasLargeStorage = true;
				this.largeStorage = (ILargeSimilarityValueStorage) newSimilarityStorage(numberPairs, ofType);
				this.smallStorage = null;

			} else {
				this.hasLargeStorage = false;
				this.smallStorage = (ISmallSimilarityValueStorage) newSimilarityStorage(numberPairs, ofType);
				this.largeStorage = null;
			}
		}

		/**
		 * @throws IncompatibleSimilarityValueStorageException
		 * 
		 */
		public SimilarityValues(final long numberPairs, final ISimilarityValueStorage storage)
				throws IncompatibleSimilarityValueStorageException {
			super();

			if (Objects.requireNonNull(storage) instanceof ILargeSimilarityValueStorage) {
				this.hasLargeStorage = true;
				this.largeStorage = (ILargeSimilarityValueStorage) storage;
				this.smallStorage = null;
				if (numberPairs > largeStorage.size())
					throw new IncompatibleSimilarityValueStorageException("Storage too small");
				if (numberPairs != largeStorage.size())
					throw new IllegalArgumentException(
							"The size of the similarity value storage must be equal to the number of pairs");

			} else {
				this.hasLargeStorage = false;
				this.smallStorage = (ISmallSimilarityValueStorage) storage;
				this.largeStorage = null;
				if (numberPairs > Integer.MAX_VALUE)
					throw new IncompatibleSimilarityValueStorageException("Storage too small");
				if (numberPairs != smallStorage.size())
					throw new IllegalArgumentException(
							"The size of the similarity value storage must be equal to the number of pairs");
			}

		}

		/**
		 * Copy constructor
		 */
		public SimilarityValues(final SimilarityValues<O1, O2, P> values) {
			super();
			this.hasLargeStorage = values.hasLargeStorage;
			if (this.hasLargeStorage) {
				this.smallStorage = null;
				this.largeStorage = (ILargeSimilarityValueStorage) values.largeStorage.copy();

			} else {
				this.smallStorage = (ISmallSimilarityValueStorage) values.smallStorage.copy();
				this.largeStorage = null;

			}
			this.sumCache = values.sumCache;
			this.meanCache = values.meanCache;
			this.minCache = values.minCache;
			this.maxCache = values.maxCache;
		}

		@Override
		public AbstractSimilarityFunction getSimilarityFunction() {
			return AbstractSimilarityFunction.this;
		}

		@Override
		public String toString() {
			return this.getStorage().toString();
		}

		@Override
		public Iterator<ISimilarityValue> iterator() {
			if (hasLargeStorage)
				return this.largeStorage.iterator();
			return this.smallStorage.iterator();
		}

		@Override
		public boolean[] isMissing(long[] is) throws IncompatibleSimilarityValueStorageException {
			if (hasLargeStorage)
				return this.largeStorage.isMissing(is);
			throw new IncompatibleSimilarityValueStorageException();
		}

		@Override
		public boolean[] isMissing(int[] is) {
			if (hasLargeStorage)
				return this.largeStorage.isMissing(is);
			return this.smallStorage.isMissing(is);
		}

		protected void clearCaches() {
			final Lock writeLock = rwLock.writeLock();
			writeLock.lock();
			try {
				this.maxCache = null;
				this.minCache = null;
				this.sumCache = null;
				this.meanCache = null;
				this.medianCache = null;
			} finally {
				writeLock.unlock();
			}
		}

		/**
		 * @return the storage
		 */
		@Override
		public ISimilarityValueStorage getStorage() {
			if (hasLargeStorage)
				return this.largeStorage;
			return this.smallStorage;
		}

		protected boolean hasLargeStorage() {
			return this.hasLargeStorage;
		}

		@Override
		public long size() {
			if (hasLargeStorage)
				return largeStorage.size();
			return smallStorage.size();
		}

		@Override
		public boolean isEmpty() {
			if (hasLargeStorage)
				return this.largeStorage.size() == 0;
			return this.smallStorage.size() == 0;
		}

		@Override
		public ISimilarityValue mean()
				throws SimilarityCalculationException, InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				if (this.meanCache == null) {
					readLock.unlock();
					final Lock writeLock = rwLock.writeLock();
					writeLock.lock();
					try {
						if (this.isEmpty())
							this.meanCache = AbstractSimilarityFunction.this.missingValuePlaceholder();
						else
							this.meanCache = this.getStorage().mean();
					} finally {
						writeLock.unlock();
						readLock.lock();
					}
				}
				return this.meanCache;
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue sum()
				throws SimilarityCalculationException, InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				if (this.sumCache == null) {
					readLock.unlock();
					final Lock writeLock = rwLock.writeLock();
					writeLock.lock();
					try {
						if (this.isEmpty())
							this.sumCache = AbstractSimilarityFunction.this.missingValuePlaceholder();
						else
							this.sumCache = this.getStorage().sum();
					} finally {
						writeLock.unlock();
						readLock.lock();
					}
				}
				return this.sumCache;
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue median()
				throws SimilarityCalculationException, InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				if (this.medianCache == null) {
					readLock.unlock();
					final Lock writeLock = rwLock.writeLock();
					writeLock.lock();
					try {
						if (this.isEmpty())
							this.medianCache = AbstractSimilarityFunction.this.missingValuePlaceholder();
						else
							this.medianCache = this.getStorage().median();
					} finally {
						writeLock.unlock();
						readLock.lock();
					}
				}
				return this.medianCache;
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue max() throws SimilarityValuesException, InterruptedException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				if (maxCache == null) {
					readLock.unlock();
					final Lock writeLock = rwLock.writeLock();
					writeLock.lock();
					try {
						if (this.isEmpty())
							maxCache = AbstractSimilarityFunction.this.missingValuePlaceholder();
						else
							this.updateMaxCache();
					} finally {
						writeLock.unlock();
						readLock.lock();
					}
				}
				return this.maxCache;
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue min() throws SimilarityValuesException, InterruptedException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				if (minCache == null) {
					readLock.unlock();
					final Lock writeLock = rwLock.writeLock();
					writeLock.lock();
					try {
						if (this.isEmpty())
							this.minCache = AbstractSimilarityFunction.this.missingValuePlaceholder();
						else
							this.updateMinCache();
					} finally {
						writeLock.unlock();
						readLock.lock();
					}
				}
				return this.minCache;
			} finally {
				readLock.unlock();
			}
		}

		protected abstract void updateMinCache() throws SimilarityValuesException, InterruptedException;

		protected abstract void updateMaxCache() throws SimilarityValuesException, InterruptedException;

		protected abstract void updateFirstObjectMinCache() throws SimilarityValuesException, InterruptedException;

		protected abstract void updateFirstObjectMaxCache() throws SimilarityValuesException, InterruptedException;

		protected abstract void updateSecondObjectMinCache() throws SimilarityValuesException, InterruptedException;

		protected abstract void updateSecondObjectMaxCache() throws SimilarityValuesException, InterruptedException;

		class TupleValueComparator implements Comparator<Pair<?, ? extends ISimilarityValue>> {

			@Override
			public int compare(final Pair<?, ? extends ISimilarityValue> o1,
					final Pair<?, ? extends ISimilarityValue> o2) {
				final ISimilarityValue s1 = o1.getSecond();
				final ISimilarityValue s2 = o2.getSecond();
				final boolean c1 = s1 instanceof ISimilarityValue, c2 = s2 instanceof ISimilarityValue;
				if (c1 && c2)
					return s1.compareTo(s2);
				else if (!c1 && !c2)
					return 0;
				else if (!c2)
					return 1;
				return -1;

			}
		}

		@Override
		public boolean[] isSet(long[] is) throws IncompatibleSimilarityValueStorageException {
			if (hasLargeStorage)
				return this.largeStorage.isSet(is);
			return this.smallStorage.isSet(requireIntForStorage(is));
		}

		@Override
		public boolean[] isSet(int[] is) {
			if (hasLargeStorage)
				return this.largeStorage.isSet(is);
			return this.smallStorage.isSet(is);
		}

		@Override
		public ObjectType<?> getObjectType() {
			if (this.hasLargeStorage)
				return this.largeStorage.getObjectType();
			return this.smallStorage.getObjectType();
		}

		@Override
		public ISimilarityValue[] get(long[] is) throws IncompatibleSimilarityValueStorageException {
			if (hasLargeStorage)
				return this.largeStorage.get(is);
			return this.smallStorage.get(requireIntForStorage(is));
		}

		@Override
		public ISimilarityValue[] get(int[] is) throws SimilarityValuesException {
			if (hasLargeStorage)
				return this.largeStorage.get(is);
			return this.smallStorage.get(is);
		}

		@Override
		public ISimilarityValue[] set(long[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			try {
				if (hasLargeStorage) {
					return this.largeStorage.set(is, values);
				} else {
					return this.smallStorage.set(is, values);
				}
			} finally {
				this.clearCaches();
			}
		}

		@Override
		public ISimilarityValue[] set(int[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			try {
				if (hasLargeStorage) {
					return this.largeStorage.set(is, values);
				} else {
					return this.smallStorage.set(is, values);
				}
			} finally {
				this.clearCaches();
			}
		}

		@Override
		public long whichMax()
				throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				if (this.maxCache == null) {
					readLock.unlock();
					final Lock writeLock = rwLock.writeLock();
					writeLock.lock();
					try {
						this.updateMaxCache();
					} finally {
						writeLock.unlock();
						readLock.lock();
					}
				}
				return this.whichMaxCache;
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public long whichMin()
				throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				if (minCache == null) {
					readLock.unlock();
					final Lock writeLock = rwLock.writeLock();
					writeLock.lock();
					try {
						this.updateMinCache();
					} finally {
						writeLock.unlock();
						readLock.lock();
					}
				}
				return whichMinCache;
			} finally {
				readLock.unlock();
			}
		}
	}

	/**
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Nov 14, 2018
	 *
	 * @param <O1>
	 * @param <O2>
	 */
	public class SimilarityValuesAllPairs<O1, O2, P extends IPair<O1, O2>> extends SimilarityValues<O1, O2, P>
			implements ISimilarityValuesAllPairs<O1, O2, P> {

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class UpdateSecondObjectMinCacheOp implements Operation<int[], Void> {
			@Override
			public Void perform(final int[] secondObjects) throws SimilarityValuesException {
				try {
					int[] minIndices = findMinFirstObjectIndicesForSecondObjects(firstObjectIndices(), secondObjects);
					long[] minSimIndices = new long[minIndices.length];
					for (int p = 0; p < minIndices.length; p++)
						minSimIndices[p] = getSimilarityIdx(minIndices[p], secondObjects[p]);
					ISimilarityValue[] minSims = get(minSimIndices);
					for (int i = 0; i < secondObjects.length; i++) {
						final int idx2 = secondObjects[i];
						final int minIdx1 = minIndices[i];
						secondObjectWhichMinCache[idx2] = minIdx1;
						secondObjectMinCache[idx2] = minSims[i];
					}
					return null;
				} catch (IncompatibleSimilarityValueStorageException e) {
					throw new SimilarityValuesException(e);
				}
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class UpdateMinCacheOp implements Operation<Iterable<int[]>, int[]> {
			@Override
			public int[] perform(final Iterable<int[]> batch) throws Exception {
				ISimilarityValue batchMinSim = null;
				int[] batchMinIdx = null;

				for (final int[] idx : batch) {
					final ISimilarityValue sim = SimilarityValuesAllPairs.this.get(idx[0], idx[1]);
					if (batchMinSim == null || batchMinSim.greaterThan(sim)) {
						batchMinSim = sim;
						batchMinIdx = idx;
					}
				}

				return batchMinIdx;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class UpdateMaxCacheOp implements Operation<Iterable<int[]>, int[]> {
			@Override
			public int[] perform(final Iterable<int[]> batch) throws Exception {
				ISimilarityValue batchMaxSim = null;
				int[] batchMaxIdx = null;

				for (final int[] idx : batch) {
					final ISimilarityValue sim = SimilarityValuesAllPairs.this.get(idx[0], idx[1]);
					if (batchMaxSim == null || batchMaxSim.lessThan(sim)) {
						batchMaxSim = sim;
						batchMaxIdx = idx;
					}
				}

				return batchMaxIdx;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class UpdateFirstObjectMinCacheOp implements Operation<int[], Void> {
			@Override
			public Void perform(final int[] firstObjects) throws SimilarityValuesException {
				try {
					int[] minIndices = findMinSecondObjectIndicesForFirstObjects(firstObjects, secondObjectIndices());
					long[] minSimIndices = new long[minIndices.length];
					for (int p = 0; p < minIndices.length; p++)
						minSimIndices[p] = getSimilarityIdx(firstObjects[p], minIndices[p]);
					ISimilarityValue[] minSims = get(minSimIndices);
					for (int i = 0; i < firstObjects.length; i++) {
						final int idx1 = firstObjects[i];
						final int minIdx2 = minIndices[i];
						firstObjectWhichMinCache[idx1] = minIdx2;
						firstObjectMinCache[idx1] = minSims[i];
					}
					return null;
				} catch (IncompatibleSimilarityValueStorageException e) {
					throw new SimilarityValuesException(e);
				}
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class UpdateFirstObjectMaxCacheOp implements Operation<int[], Void> {
			@Override
			public Void perform(final int[] firstObjects) throws SimilarityValuesException {
				try {
					int[] maxIndices = findMaxSecondObjectIndicesForFirstObjects(firstObjects, secondObjectIndices());
					long[] maxSimIndices = new long[maxIndices.length];
					for (int p = 0; p < maxIndices.length; p++)
						maxSimIndices[p] = getSimilarityIdx(firstObjects[p], maxIndices[p]);
					ISimilarityValue[] maxSims = get(maxSimIndices);
					for (int i = 0; i < firstObjects.length; i++) {
						final int idx1 = firstObjects[i];
						final int maxIdx2 = maxIndices[i];
						firstObjectWhichMaxCache[idx1] = maxIdx2;
						firstObjectMaxCache[idx1] = maxSims[i];
					}
					return null;
				} catch (IncompatibleSimilarityValueStorageException e) {
					throw new SimilarityValuesException(e);
				}
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class UpdateSecondObjectMaxCacheOp implements Operation<int[], Void> {
			@Override
			public Void perform(final int[] secondObjects) throws SimilarityValuesException {
				try {
					int[] maxIndices = findMaxFirstObjectIndicesForSecondObjects(firstObjectIndices(), secondObjects);
					long[] maxSimIndices = new long[maxIndices.length];
					for (int p = 0; p < maxIndices.length; p++)
						maxSimIndices[p] = getSimilarityIdx(maxIndices[p], secondObjects[p]);
					ISimilarityValue[] maxSims = get(maxSimIndices);
					for (int i = 0; i < secondObjects.length; i++) {
						final int idx2 = secondObjects[i];
						final int maxIdx1 = maxIndices[i];
						secondObjectWhichMaxCache[idx2] = maxIdx1;
						secondObjectMaxCache[idx2] = maxSims[i];
					}
					return null;
				} catch (IncompatibleSimilarityValueStorageException e) {
					throw new SimilarityValuesException(e);
				}
			}
		}

		private int[] findMaxFirstObjectIndicesForSecondObjects(final int[] firstObjects, final int[] secondObjects)
				throws SimilarityValuesException {
			final ISimilarityValue[] sims = SimilarityValuesAllPairs.this.getForAllPairs(firstObjects, secondObjects);

			int s = 0;
			int[] maxIndices = new int[secondObjects.length];
			for (int i = 0; i < secondObjects.length; i++) {
				final int idx2 = secondObjects[i];
				ISimilarityValue objMaxSim = minValue();
				int maxIdx1 = -1;

				for (final int idx1 : firstObjects) {
					final ISimilarityValue sim = sims[s++];
					if (objMaxSim.lessThan(sim)) {
						objMaxSim = sim;
						maxIdx1 = idx1;
					}
				}
				maxIndices[i] = maxIdx1;
			}
			return maxIndices;
		}

		private int[] findMaxSecondObjectIndicesForFirstObjects(final int[] firstObjects, final int[] secondObjects)
				throws SimilarityValuesException {
			final ISimilarityValue[] sims = SimilarityValuesAllPairs.this.getForAllPairs(firstObjects, secondObjects);

//			System.out.println(Arrays.toString(Arrays.stream(sims).mapToDouble(v -> {
//				try {
//					return v.get();
//				} catch (SimilarityCalculationException e) {
//					return Double.NaN;
//				}
//			}).toArray()));

			int s = 0;
			int[] maxIndices = new int[firstObjects.length];
			for (int i = 0; i < firstObjects.length; i++) {
				final int idx1 = firstObjects[i];
				ISimilarityValue objMaxSim = minValue();
				int maxIdx2 = -1;

				for (final int idx2 : secondObjects) {
					final ISimilarityValue sim = sims[s++];
					if (objMaxSim.lessThan(sim)) {
						objMaxSim = sim;
						maxIdx2 = idx2;
					}
				}
				maxIndices[i] = maxIdx2;
			}
			return maxIndices;
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 884215944233833955L;
		protected final Map<O1, Integer> firstObjectToIndex;
		protected final Map<O2, Integer> secondObjectToIndex;
		protected final O1[] firstObjects;
		protected final int[] firstObjectIndices, secondObjectIndices;
		protected final O2[] secondObjects;
		protected final IPairBuilder<O1, O2, P> builder;
		protected final IPairsFactory<O1, O2, P> pairsFactory;

		protected ISimilarityValue[] firstObjectMinCache, firstObjectMaxCache, secondObjectMinCache,
				secondObjectMaxCache;

		protected int[] firstObjectWhichMinCache, firstObjectWhichMaxCache, secondObjectWhichMinCache,
				secondObjectWhichMaxCache;

		/**
		 * A constructor that creates a {@link ISimilarityValueStorage} matching the
		 * requested number of pairs.
		 * 
		 * @param objects1
		 * @param objects2
		 * @param pairsFactory
		 * @param ofType
		 */
		public SimilarityValuesAllPairs(final O1[] objects1, final O2[] objects2,
				final IPairsFactory<O1, O2, P> pairsFactory, final ObjectType<?> ofType) {
			super(objects1.length * (long) objects2.length, ofType);

			this.firstObjects = objects1;
			this.secondObjects = objects2;
			this.firstObjectIndices = ArraysExt.range(0, this.firstObjects.length);
			this.secondObjectIndices = ArraysExt.range(0, this.secondObjects.length);
			this.pairsFactory = pairsFactory;
			this.builder = pairsFactory.getPairBuilder();

			// initialize reverse lookup maps
			this.firstObjectToIndex = new HashMap<>(this.firstObjects.length, 1.0f);
			for (int idx : this.firstObjectIndices())
				this.firstObjectToIndex.put(this.getObject1(idx), idx);
			this.secondObjectToIndex = new HashMap<>(this.secondObjects.length, 1.0f);
			for (int idx : this.secondObjectIndices())
				this.secondObjectToIndex.put(this.getObject2(idx), idx);

			// initialize caches
			this.clearCaches();
		}

		/**
		 * @throws IncompatibleSimilarityValueStorageException
		 * 
		 */
		public SimilarityValuesAllPairs(final O1[] objects1, final O2[] objects2, final ISimilarityValueStorage storage,
				final IPairsFactory<O1, O2, P> pairsFactory) throws IncompatibleSimilarityValueStorageException {
			super(objects1.length * (long) objects2.length, storage);
			this.firstObjects = objects1;
			this.secondObjects = objects2;
			this.firstObjectIndices = ArraysExt.range(0, this.firstObjects.length);
			this.secondObjectIndices = ArraysExt.range(0, this.secondObjects.length);
			this.pairsFactory = pairsFactory;
			this.builder = pairsFactory.getPairBuilder();

			// initialize reverse lookup maps
			this.firstObjectToIndex = new HashMap<>(this.firstObjects.length, 1.0f);
			for (int idx : this.firstObjectIndices())
				this.firstObjectToIndex.put(this.getObject1(idx), idx);
			this.secondObjectToIndex = new HashMap<>(this.secondObjects.length, 1.0f);
			for (int idx : this.secondObjectIndices())
				this.secondObjectToIndex.put(this.getObject2(idx), idx);

			// initialize caches
			this.clearCaches();
		}

		/**
		 * Copy constructor
		 */
		public SimilarityValuesAllPairs(final SimilarityValuesAllPairs<O1, O2, P> values) {
			super(values);
			this.firstObjects = values.firstObjects;
			this.secondObjects = values.secondObjects;
			this.firstObjectIndices = values.firstObjectIndices;
			this.secondObjectIndices = values.secondObjectIndices;
			this.pairsFactory = values.pairsFactory;
			this.builder = values.builder;
			this.firstObjectToIndex = values.firstObjectToIndex;
			this.secondObjectToIndex = values.secondObjectToIndex;

			// copy caches
			this.firstObjectMinCache = Arrays.copyOf(values.firstObjectMinCache, values.firstObjectMinCache.length);
			this.secondObjectMinCache = Arrays.copyOf(values.secondObjectMinCache, values.secondObjectMinCache.length);
			this.firstObjectMaxCache = Arrays.copyOf(values.firstObjectMaxCache, values.firstObjectMaxCache.length);
			this.secondObjectMaxCache = Arrays.copyOf(values.secondObjectMaxCache, values.secondObjectMaxCache.length);
			this.firstObjectWhichMinCache = Arrays.copyOf(values.firstObjectWhichMinCache,
					values.firstObjectWhichMinCache.length);
			this.secondObjectWhichMinCache = Arrays.copyOf(values.secondObjectWhichMinCache,
					values.secondObjectWhichMinCache.length);
			this.firstObjectWhichMaxCache = Arrays.copyOf(values.firstObjectWhichMaxCache,
					values.firstObjectWhichMaxCache.length);
			this.secondObjectWhichMaxCache = Arrays.copyOf(values.secondObjectWhichMaxCache,
					values.secondObjectWhichMaxCache.length);
		}

		@Override
		public O1[] getObjects1() {
			return this.firstObjects;
		}

		@Override
		public O2[] getObjects2() {
			return this.secondObjects;
		}

		@Override
		public long getNumberOfPairs() {
			return ((long) this.firstObjects.length) * this.secondObjects.length;
		}

		@Override
		public Iterable<P> getPairs() {
			return pairsFactory.createIterable(this.firstObjects, this.secondObjects);
		}

		/**
		 * @return the pairsFactory
		 */
		@Override
		public IPairsFactory<O1, O2, P> getPairsFactory() {
			return this.pairsFactory;
		}

		@Override
		public Iterable<int[]> pairIndices() {
			return new IIterableWithSize<int[]>() {

				@Override
				public long longSize() {
					return firstObjects.length * (long) secondObjects.length;
				}

				@Override
				public Iterator<int[]> iterator() {
					return new Iterator<int[]>() {
						int i = 0, j = -1;

						@Override
						public boolean hasNext() {
							// we overstepped the first array
							if (i >= firstObjects.length)
								return false;
							// we finished the second array, and there are no more elements in the first
							// array
							if (i >= firstObjects.length - 1 && j + 1 >= secondObjects.length)
								return false;
							return firstObjects.length > 0 && secondObjects.length > 0;
						}

						@Override
						public int[] next() {
							if (!hasNext())
								throw new NoSuchElementException();
							if (j + 1 >= secondObjects.length) {
								i++;
								j = -1;
							}
							return new int[] { i, ++j };
						};
					};
				}
			};
		}

		@Override
		public ISimilarityValuesSomePairs<O1, O2, P> sortedByValue()
				throws SimilarityValuesException, InterruptedException {
			try {
				if (hasLargeStorage) {
					final ObjectBigList<int[]> sortedIndices = (ObjectBigList<int[]>) this.decreasingByValue();
					final ObjectBigList<P> sortedPairs = new ObjectBigArrayBigList<>();
					for (final int[] i : sortedIndices)
						sortedPairs.add(this.getPair(i));

					final LongBigList sortedSimIndices = new LongBigArrayBigList(sortedIndices.size64());
					for (long v = 0; v < sortedIndices.size64(); v++)
						sortedSimIndices.set(v,
								this.getSimilarityIdx(sortedIndices.get(v)[0], sortedIndices.get(v)[1]));

					final ISimilarityValuesSomePairs<O1, O2, P> sorted = new SimilarityValuesSomePairs<>(sortedPairs,
							this.largeStorage.subset(sortedSimIndices));
					return sorted;
				} else {
					final ObjectCollection<int[]> sortedIndices = this.decreasingByValue();

					final List<P> sortedPairs = new ArrayList<>();
					for (final int[] i : sortedIndices)
						sortedPairs.add(this.getPair(i));

					final int[] sortedSimIndices = new int[sortedPairs.size()];
					int v = 0;
					for (int[] idx : sortedIndices)
						sortedSimIndices[v++] = requireIntForStorage(this.getSimilarityIdx(idx[0], idx[1]));

					final ISimilarityValuesSomePairs<O1, O2, P> sorted = new SimilarityValuesSomePairs<>(sortedPairs,
							this.smallStorage.subset(sortedSimIndices));
					return sorted;

				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public <R> Map<R, ? extends SimilarityValuesSomePairs<O1, O2, P>> groupBy(
				final TriFunction<O1, O2, ISimilarityValue, R> f)
				throws SimilarityValuesException, InterruptedException {
			try {
				if (hasLargeStorage) {
					final Map<R, BigList<P>> groupedPairs = new LinkedHashMap<>();
					final Map<R, LongBigList> groupedSimilarityValues = new LinkedHashMap<>();

					final IPairBuilder<O1, O2, P> builder = this.builder.copy();

					for (int x = 0; x < this.firstObjects.length; x++) {
						final O1 o1 = this.firstObjects[x];
						for (int y = 0; y < this.secondObjects.length; y++) {
							final O2 o2 = this.secondObjects[y];
							final long idx = this.getSimilarityIdx(x, y);
							final ISimilarityValue s = this.largeStorage.get(idx);
							final R v = f.apply(o1, o2, s);
							groupedPairs.putIfAbsent(v, new ObjectBigArrayBigList<>());
							groupedPairs.get(v).add(builder.setFirst(o1).setSecond(o2).build());
							groupedSimilarityValues.putIfAbsent(v, new LongBigArrayBigList());
							groupedSimilarityValues.get(v).add(idx);
						}
					}

					final Map<R, SimilarityValuesSomePairs<O1, O2, P>> result = new LinkedHashMap<>();
					for (final R r : groupedPairs.keySet()) {
						final SimilarityValuesSomePairs<O1, O2, P> vals = new SimilarityValuesSomePairs<>(
								groupedPairs.remove(r), this.largeStorage.subset(groupedSimilarityValues.get(r)));
						result.put(r, vals);
					}
					return result;
				} else {
					final Map<R, List<P>> groupedPairs = new LinkedHashMap<>();
					final Map<R, IntArrayList> groupedSimilarityValues = new LinkedHashMap<>();

					final IPairBuilder<O1, O2, P> builder = this.builder.copy();

					if ((long) this.firstObjects.length * this.secondObjects.length > Integer.MAX_VALUE)
						throw new IncompatibleSimilarityValueStorageException();
					for (int x = 0; x < this.firstObjects.length; x++) {
						final O1 o1 = this.firstObjects[x];
						for (int y = 0; y < this.secondObjects.length; y++) {
							final O2 o2 = this.secondObjects[y];
							final int idx = this.getSimilarityIdxSmallUnchecked(x, y);
							final ISimilarityValue s = this.smallStorage.get(idx);
							final R v = f.apply(o1, o2, s);
							groupedPairs.putIfAbsent(v, new ArrayList<>());
							groupedPairs.get(v).add(builder.setFirst(o1).setSecond(o2).build());
							groupedSimilarityValues.putIfAbsent(v, new IntArrayList());
							groupedSimilarityValues.get(v).add(idx);
						}
					}

					final Map<R, SimilarityValuesSomePairs<O1, O2, P>> result = new LinkedHashMap<>();
					for (final R r : groupedPairs.keySet()) {
						final SimilarityValuesSomePairs<O1, O2, P> vals = new SimilarityValuesSomePairs<>(
								groupedPairs.remove(r),
								this.smallStorage.subset(groupedSimilarityValues.get(r).toIntArray()));
						result.put(r, vals);
					}
					return result;

				}
			} catch (final FactoryException | CreateInstanceFactoryException
					| IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public Int2ObjectMap<ISimilarityValuesAllPairs<O1, O2, P>> partitionByFirstObject()
				throws SimilarityValuesException, InterruptedException {
			try {
				if (hasLargeStorage) {
					final Map<Integer, ObjectBigList<P>> pairs = new LinkedHashMap<>();
					final Map<Integer, LongBigList> indices = new LinkedHashMap<>();

					for (final int i1 : this.firstObjectIndices()) {
						pairs.putIfAbsent(i1, new ObjectBigArrayBigList<>(this.firstObjects.length));
						indices.putIfAbsent(i1, new LongBigArrayBigList(this.firstObjects.length));

						for (final int i2 : this.secondObjectIndices()) {
							final int[] idx = new int[] { i1, i2 };
							pairs.get(i1).add(this.getPair(idx));
							indices.get(i1).add(this.getSimilarityIdx(i1, i2));
						}
					}

					final Int2ObjectMap<ISimilarityValuesAllPairs<O1, O2, P>> result = new Int2ObjectOpenHashMap<>();
					for (final int i1 : this.firstObjectIndices()) {
						final SimilarityValuesAllPairs<O1, O2, P> sims = new SimilarityValuesAllPairs<>(
								(O1[]) new Object[] { this.getObject1(i1) }, this.secondObjects,
								this.largeStorage.subset(indices.get(i1)), this.pairsFactory);
						result.put(i1, sims);
					}
					return result;
				} else {
					final Int2ObjectMap<ISimilarityValuesAllPairs<O1, O2, P>> result = new Int2ObjectOpenHashMap<>();
					for (final int i1 : this.firstObjectIndices()) {
						result.put(i1, this.subset(new int[] { i1 }, this.secondObjectIndices()));
					}
					return result;

				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public Int2ObjectMap<ISimilarityValuesAllPairs<O1, O2, P>> partitionBySecondObject()
				throws SimilarityValuesException, InterruptedException {
			try {
				if (hasLargeStorage) {
					final Map<Integer, ObjectBigList<P>> pairs = new LinkedHashMap<>();
					final Map<Integer, LongBigList> indices = new LinkedHashMap<>();

					for (final int i2 : this.secondObjectIndices()) {
						pairs.putIfAbsent(i2, new ObjectBigArrayBigList<>(this.secondObjects.length));
						indices.putIfAbsent(i2, new LongBigArrayBigList(this.secondObjects.length));

						long j = 0;
						for (final int i1 : this.firstObjectIndices()) {
							final int[] idx = new int[] { i1, i2 };
							pairs.get(i2).set(j, this.getPair(idx));
							indices.get(i2).set(j, this.getSimilarityIdx(i1, i2));
							j++;
						}
					}

					final Int2ObjectMap<ISimilarityValuesAllPairs<O1, O2, P>> result = new Int2ObjectOpenHashMap<>();
					for (final int i2 : this.secondObjectIndices()) {
						final SimilarityValuesAllPairs<O1, O2, P> sims = new SimilarityValuesAllPairs<>(
								this.firstObjects, (O2[]) new Object[] { this.getObject2(i2) },
								this.largeStorage.subset(indices.get(i2)), this.pairsFactory);
						result.put(i2, sims);
					}
					return result;
				} else {
					final Map<Integer, ObjectList<P>> pairs = new LinkedHashMap<>();
					final Map<Integer, IntArrayList> indices = new LinkedHashMap<>();

					for (final int i2 : this.secondObjectIndices()) {
						pairs.putIfAbsent(i2, new ObjectArrayList<>(this.secondObjects.length));
						indices.putIfAbsent(i2, new IntArrayList(this.secondObjects.length));

						int j = 0;
						for (final int i1 : this.firstObjectIndices()) {
							final int[] idx = new int[] { i1, i2 };
							pairs.get(i2).add(this.getPair(idx));
							indices.get(i2).add(requireIntForStorage(this.getSimilarityIdx(i1, i2)));
							j++;
						}
					}

					final Int2ObjectMap<ISimilarityValuesAllPairs<O1, O2, P>> result = new Int2ObjectOpenHashMap<>();
					for (final int i2 : this.secondObjectIndices()) {
						final SimilarityValuesAllPairs<O1, O2, P> sims = new SimilarityValuesAllPairs<>(
								this.firstObjects, (O2[]) new Object[] { this.getObject2(i2) },
								this.smallStorage.subset(indices.get(i2).toIntArray()), this.pairsFactory);
						result.put(i2, sims);
					}
					return result;
				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public SimilarityValuesSomePairs<O1, O2, P> filter(final TriPredicate<O1, O2, ISimilarityValue> p)
				throws SimilarityValuesException, InterruptedException {
			try {
				if (hasLargeStorage) {
					final ObjectBigList<P> objectPairs = new ObjectBigArrayBigList<>();
					final LongBigArrayBigList indices = new LongBigArrayBigList();

					final IPairBuilder<O1, O2, P> builder = this.builder.copy();

					for (final int[] idx : this.pairIndices()) {
						final int x = idx[0];
						final O1 o1 = this.getObject1(x);
						final int y = idx[1];
						final O2 o2 = this.getObject2(y);
						final ISimilarityValue s = this.largeStorage.get(this.getSimilarityIdx(idx[0], idx[1]));
						if (p.test(o1, o2, s)) {
							objectPairs.add(builder.setFirst(o1).setSecond(o2).build());
							indices.add(this.getSimilarityIdx(idx[0], idx[1]));
						}
					}
					final SimilarityValuesSomePairs<O1, O2, P> result = new SimilarityValuesSomePairs<>(objectPairs,
							this.largeStorage.subset(indices));
					return result;
				} else {
					final List<P> objectPairs = new ArrayList<>();
					final IntList indices = new IntArrayList();

					final IPairBuilder<O1, O2, P> builder = this.builder.copy();

					if ((long) this.firstObjects.length * this.secondObjects.length > Integer.MAX_VALUE)
						throw new IncompatibleSimilarityValueStorageException();
					final int[] simIdxs = this.getSimilarityIdxSmallUnchecked(this.firstObjectIndices,
							this.secondObjectIndices, true);
					final ISimilarityValue[] sims = this.smallStorage.get(simIdxs);
					int c = 0;
					for (int x : this.firstObjectIndices) {
						for (int y : this.secondObjectIndices) {
							final O1 o1 = this.getObject1(x);
							final O2 o2 = this.getObject2(y);
							final ISimilarityValue s = sims[c];
							if (p.test(o1, o2, s)) {
								objectPairs.add(builder.setFirst(o1).setSecond(o2).build());
								indices.add(simIdxs[c]);
							}
							c++;
						}
					}
					final SimilarityValuesSomePairs<O1, O2, P> result = new SimilarityValuesSomePairs<>(objectPairs,
							this.smallStorage.subset(indices.toIntArray()));
					return result;
				}
			} catch (final FactoryException | CreateInstanceFactoryException
					| IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public ISimilarityValue sum(int[] firstObjects, int[] secondObjects)
				throws SimilarityValuesException, InterruptedException {
			if (hasLargeStorage) {
				LongBigList indices = new LongBigArrayBigList(
						LongStream.range(0, firstObjects.length * (long) secondObjects.length).iterator());
				return this.largeStorage.sum(indices);
			}
			int[] indices = new int[firstObjects.length * secondObjects.length];
			return this.smallStorage.sum(indices);
		}

		@Override
		public ISimilarityValuesAllPairs<O1, O2, P> subset(int[] firstObjectsIdx, int[] secondObjectsIdx)
				throws SimilarityValuesException, InterruptedException {
			try {
				final O1[] firstObjects = (O1[]) new Object[firstObjectsIdx.length];
				for (int i = 0; i < firstObjectsIdx.length; i++)
					firstObjects[i] = this.firstObjects[firstObjectsIdx[i]];
				final O2[] secondObjects = (O2[]) new Object[secondObjectsIdx.length];
				for (int i = 0; i < secondObjectsIdx.length; i++)
					secondObjects[i] = this.secondObjects[secondObjectsIdx[i]];

				final ISimilarityValueStorage subsetStorage;

				if (hasLargeStorage) {
					final LongBigList indices = new LongBigArrayBigList(
							firstObjectsIdx.length * (long) secondObjectsIdx.length);
					for (int i = 0; i < firstObjectsIdx.length; i++)
						for (int j = 0; j < secondObjectsIdx.length; j++)
							indices.add(getSimilarityIdx(firstObjectsIdx[i], secondObjectsIdx[j]));
					subsetStorage = this.largeStorage.subset(indices);
				} else {
					if ((long) firstObjectsIdx.length * secondObjectsIdx.length > Integer.MAX_VALUE)
						throw new IncompatibleSimilarityValueStorageException();
					final int[] indices = new int[firstObjectsIdx.length * secondObjectsIdx.length];
					for (int i = 0; i < firstObjectsIdx.length; i++)
						for (int j = 0; j < secondObjectsIdx.length; j++)
							indices[secondObjectsIdx.length * i + j] = getSimilarityIdxSmallUnchecked(
									firstObjectsIdx[i], secondObjectsIdx[j]);
					subsetStorage = this.smallStorage.subset(indices);
				}
				final SimilarityValuesAllPairs<O1, O2, P> result = new SimilarityValuesAllPairs<>(firstObjects,
						secondObjects, subsetStorage, this.pairsFactory);

//				// copy caches as much as possible
//				final IntOpenHashSet firstObjectsIdxSet = new IntOpenHashSet(firstObjectsIdx),
//						secondObjectsIdxSet = new IntOpenHashSet(secondObjectsIdx);
//
//				final ISimilarityValue[] firstObjectMaxCacheSubset = new ISimilarityValue[firstObjectsIdx.length],
//						firstObjectMinCacheSubset = new ISimilarityValue[firstObjectsIdx.length],
//						secondObjectMaxCacheSubset = new ISimilarityValue[secondObjectsIdx.length],
//						secondObjectMinCacheSubset = new ISimilarityValue[secondObjectsIdx.length];
//				final int[] firstObjectWhichMaxCacheSubset = new int[firstObjectsIdx.length],
//						firstObjectWhichMinCacheSubset = new int[firstObjectsIdx.length],
//						secondObjectWhichMaxCacheSubset = new int[secondObjectsIdx.length],
//						secondObjectWhichMinCacheSubset = new int[secondObjectsIdx.length];
//
//				for (int i = 0; i < firstObjectsIdx.length; i++) {
//					final int o1Idx = firstObjectsIdx[i];
//					final int previousMaxIdx = this.firstObjectWhichMaxCache[o1Idx];
//
//					if (previousMaxIdx == -1 || !secondObjectsIdxSet.contains(previousMaxIdx)) {
//						firstObjectWhichMaxCacheSubset[i] = -1;
//						firstObjectMaxCacheSubset[i] = null;
//					} else {
//						firstObjectWhichMaxCacheSubset[i] = previousMaxIdx;
//						firstObjectMaxCacheSubset[i] = this.firstObjectMaxCache[o1Idx];
//					}
//
//					final int previousMinIdx = this.firstObjectWhichMinCache[o1Idx];
//
//					if (previousMinIdx == -1 || !secondObjectsIdxSet.contains(previousMinIdx)) {
//						firstObjectWhichMinCacheSubset[i] = -1;
//						firstObjectMinCacheSubset[i] = null;
//					} else {
//						firstObjectWhichMinCacheSubset[i] = previousMinIdx;
//						firstObjectMinCacheSubset[i] = this.firstObjectMinCache[o1Idx];
//					}
//				}
//
//				for (int i = 0; i < secondObjectsIdx.length; i++) {
//					final int o2Idx = secondObjectsIdx[i];
//					final int previousMaxIdx = this.secondObjectWhichMaxCache[o2Idx];
//
//					if (previousMaxIdx == -1 || !firstObjectsIdxSet.contains(previousMaxIdx)) {
//						secondObjectWhichMaxCacheSubset[i] = -1;
//						secondObjectMaxCacheSubset[i] = null;
//					} else {
//						secondObjectWhichMaxCacheSubset[i] = previousMaxIdx;
//						secondObjectMaxCacheSubset[i] = this.secondObjectMaxCache[o2Idx];
//					}
//
//					final int previousMinIdx = this.secondObjectWhichMinCache[o2Idx];
//
//					if (previousMinIdx == -1 || !firstObjectsIdxSet.contains(previousMinIdx)) {
//						secondObjectWhichMinCacheSubset[i] = -1;
//						secondObjectMinCacheSubset[i] = null;
//					} else {
//						secondObjectWhichMinCacheSubset[i] = previousMinIdx;
//						secondObjectMinCacheSubset[i] = this.secondObjectMinCache[o2Idx];
//					}
//				}
//
//				result.firstObjectMaxCache = firstObjectMaxCacheSubset;
//				result.firstObjectMinCache = firstObjectMinCacheSubset;
//				result.secondObjectMaxCache = secondObjectMaxCacheSubset;
//				result.secondObjectMinCache = secondObjectMinCacheSubset;
//
//				result.firstObjectWhichMaxCache = firstObjectWhichMaxCacheSubset;
//				result.firstObjectWhichMinCache = firstObjectWhichMinCacheSubset;
//				result.secondObjectWhichMaxCache = secondObjectWhichMaxCacheSubset;
//				result.secondObjectWhichMinCache = secondObjectWhichMinCacheSubset;

				return result;
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public O1 getObject1(final int x) {
			return this.firstObjects[x];
		}

		@Override
		public O2 getObject2(final int y) {
			return this.secondObjects[y];
		}

		@Override
		public int getObject1Index(O1 object1) {
			return this.firstObjectToIndex.get(object1);
		}

		@Override
		public int getObject2Index(O2 object2) {
			return this.secondObjectToIndex.get(object2);
		}

		@Override
		public int[] firstObjectIndices() {
			return this.firstObjectIndices;
		}

		@Override
		public int[] secondObjectIndices() {
			return this.secondObjectIndices;
		}

		@Override
		public P getPair(final int[] xy) throws SimilarityValuesException, InterruptedException {
			try {
				return this.builder.build(this.getObject1(xy[0]), this.getObject2(xy[1]));
			} catch (final FactoryException | CreateInstanceFactoryException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public ISimilarityValue set(final int x, final int y, final ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			try {
				final ISimilarityValue old;
				long similarityIdx = this.getSimilarityIdx(x, y);
				if (hasLargeStorage) {
					final long idx = similarityIdx;
					old = this.largeStorage.set(idx, value);
				} else {
					final int idx = requireIntForStorage(similarityIdx);
					old = this.smallStorage.set(idx, value);
				}
				if (old == null || value.greaterThan(old)) {
					this.clearCaches(similarityIdx);
				}
				return old;
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public ISimilarityValue get(final int x, final int y) throws SimilarityValuesException {
			try {
				if (hasLargeStorage)
					return this.largeStorage.get(this.getSimilarityIdx(x, y));
				return this.smallStorage.get(requireIntForStorage(this.getSimilarityIdx(x, y)));
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public ISimilarityValue[] get(final int[] xs, final int[] ys) throws SimilarityValuesException {
			try {
				if (xs.length != ys.length)
					throw new SimilarityValuesException(
							"The array lengths of first and second object indices need to be the same.");
				if (hasLargeStorage)
					return this.largeStorage.get(this.getSimilarityIdx(xs, ys, false));
				return this.smallStorage.get(requireIntForStorage(this.getSimilarityIdx(xs, ys, false)));
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public ISimilarityValue[] getForAllPairs(int[] xs, int[] ys) throws SimilarityValuesException {
			try {
				if (hasLargeStorage)
					return this.largeStorage.get(this.getSimilarityIdx(xs, ys, true));
				return this.smallStorage.get(requireIntForStorage(this.getSimilarityIdx(xs, ys, true)));
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		private long getSimilarityIdx(final int x, final int y) {
			return (long) x * this.secondObjects.length + y;
		}

		private long[] getSimilarityIdx(final int[] xs, final int[] ys, final boolean allPairs) {
			if (allPairs) {
				final long[] res = new long[xs.length * ys.length];
				int i = 0;
				for (int p = 0; p < xs.length; p++)
					for (int q = 0; q < ys.length; q++)
						res[i++] = getSimilarityIdx(xs[p], ys[q]);
				return res;
			} else {
				final long[] res = new long[xs.length];
				for (int p = 0; p < xs.length; p++)
					res[p] = getSimilarityIdx(xs[p], ys[p]);
				return res;
			}
		}

		private int getSimilarityIdxSmallUnchecked(final int x, final int y) {
			return x * this.secondObjects.length + y;
		}

		private int[] getSimilarityIdxSmallUnchecked(final int[] xs, final int[] ys, final boolean allPairs) {
			if (allPairs) {
				final int[] res = new int[xs.length * ys.length];
				int i = 0;
				for (int p = 0; p < xs.length; p++)
					for (int q = 0; q < ys.length; q++)
						res[i++] = xs[p] * this.secondObjects.length + ys[q];
				return res;
			} else {
				final int[] r = new int[xs.length];
				for (int p = 0; p < r.length; p++)
					r[p] = xs[p] * this.secondObjects.length + ys[p];
				return r;
			}
		}

		@Override
		protected void updateMinCache() throws SimilarityValuesException, InterruptedException {
			final Iterable<Iterable<int[]>> batches = Iterables.batchify(this.pairIndices(),
					new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

			final List<Future<int[]>> futures = new MyParallel<int[]>(privateSimilarityFunctionThreadPool).For(batches,
					new UpdateMinCacheOp());

			ISimilarityValue minSim = maxValue();
			int[] minIdx = null;

			for (final Future<int[]> f : futures) {
				try {
					final int[] batchMinIdx = f.get();

					int x = batchMinIdx[0];
					int y = batchMinIdx[1];
					if (minSim == null || minSim.compareTo(this.get(x, y)) > 0) {
						minSim = this.get(x, y);
						minIdx = batchMinIdx;
					}

				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				}
			}

			// no comparables
			if (minIdx == null)
				return;
			this.minCache = minSim;
			this.whichMinCache = getSimilarityIdx(minIdx[0], minIdx[1]);
		}

		@Override
		protected void updateMaxCache() throws SimilarityValuesException, InterruptedException {
			final Iterable<Iterable<int[]>> batches = Iterables.batchify(this.pairIndices(),
					new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

			final List<Future<int[]>> futures = new MyParallel<int[]>(privateSimilarityFunctionThreadPool).For(batches,
					new UpdateMaxCacheOp());

			ISimilarityValue maxSim = AbstractSimilarityValue.MIN;
			int[] maxIdx = null;

			for (final Future<int[]> f : futures) {
				try {
					final int[] batchMaxIdx = f.get();

					int x = batchMaxIdx[0];
					int y = batchMaxIdx[1];
					if (maxSim == null || maxSim.compareTo(this.get(x, y)) < 0) {
						maxSim = this.get(x, y);
						maxIdx = batchMaxIdx;
					}

				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				}
			}

			// no comparables
			if (maxIdx == null)
				return;
			this.maxCache = maxSim;
			this.whichMaxCache = getSimilarityIdx(maxIdx[0], maxIdx[1]);
		}

		@Override
		protected void updateFirstObjectMaxCache() throws SimilarityValuesException, InterruptedException {
			final int MIN_BATCH_SIZE = 50000;

			final int[] indices = Arrays.stream(this.firstObjectIndices())
					.filter(p -> this.firstObjectWhichMaxCache[p] == -1).toArray();

			if (indices.length < MIN_BATCH_SIZE) {
				new UpdateFirstObjectMaxCacheOp().perform(indices);
			} else {
				final Iterable<int[]> batches = Iterables.batchify(indices,
						new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<Void>> futures = new MyParallel<Void>(privateSimilarityFunctionThreadPool)
						.For(batches, new UpdateFirstObjectMaxCacheOp());

				for (Future<Void> f : futures) {
					try {
						f.get();
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
				}
			}
		}

		@Override
		protected void updateSecondObjectMaxCache() throws SimilarityValuesException, InterruptedException {
			final int MIN_BATCH_SIZE = 50000;

			final int[] indices = Arrays.stream(this.firstObjectIndices())
					.filter(p -> this.secondObjectWhichMaxCache[p] == -1).toArray();

			if (indices.length < MIN_BATCH_SIZE) {
				new UpdateSecondObjectMaxCacheOp().perform(indices);
			} else {
				final Iterable<int[]> batches = Iterables.batchify(indices,
						new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<Void>> futures = new MyParallel<Void>(privateSimilarityFunctionThreadPool)
						.For(batches, new UpdateSecondObjectMaxCacheOp());

				for (final Future<Void> f : futures) {
					try {
						f.get();
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() instanceof NoComparableSimilarityValuesException)
							continue;
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
				}
			}
		}

		@Override
		protected void updateFirstObjectMinCache() throws SimilarityValuesException, InterruptedException {
			final int MIN_BATCH_SIZE = 50000;

			final int[] indices = Arrays.stream(this.firstObjectIndices())
					.filter(p -> this.firstObjectWhichMinCache[p] == -1).toArray();

			if (indices.length < MIN_BATCH_SIZE) {
				new UpdateFirstObjectMinCacheOp().perform(indices);
			} else {
				final Iterable<int[]> batches = Iterables.batchify(indices,
						new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<Void>> futures = new MyParallel<Void>(privateSimilarityFunctionThreadPool)
						.For(batches, new UpdateFirstObjectMinCacheOp());

				for (final Future<Void> f : futures) {
					try {
						f.get();
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() instanceof NoComparableSimilarityValuesException)
							continue;
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
				}
			}
		}

		@Override
		protected void updateSecondObjectMinCache() throws SimilarityValuesException, InterruptedException {
			final int MIN_BATCH_SIZE = 50000;

			final int[] indices = Arrays.stream(this.firstObjectIndices())
					.filter(p -> this.secondObjectWhichMinCache[p] == -1).toArray();

			if (indices.length < MIN_BATCH_SIZE) {
				new UpdateSecondObjectMinCacheOp().perform(indices);
			} else {
				final Iterable<int[]> batches = Iterables.batchify(indices,
						new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

				final List<Future<Void>> futures = new MyParallel<Void>(privateSimilarityFunctionThreadPool)
						.For(batches, new UpdateSecondObjectMinCacheOp());

				for (final Future<Void> f : futures) {
					try {
						f.get();
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
				}
			}
		}

		@Override
		public P getPair(final long i) throws SimilarityValuesException, InterruptedException {
			return this.getPair(IndexUtils.pairIndexToObjectListIndices(i, this.secondObjects.length));
		}

		@Override
		public ObjectBigList<int[]> increasingByValue() throws SimilarityValuesException {
			if (hasLargeStorage) {
				final LongBigList sortedByValue = this.largeStorage.increasingByValue();

				final ObjectBigList<int[]> result = new ObjectBigArrayBigList<>(sortedByValue.size64());
				sortedByValue.forEach(
						(long i) -> result.add(IndexUtils.pairIndexToObjectListIndices(i, this.secondObjects.length)));

				return result;
			} else {
				final int[] sortedByValue = this.smallStorage.increasingByValue();
				return new ObjectBigArrayBigList<>(Arrays.stream(sortedByValue)
						.mapToObj(i -> IndexUtils.pairIndexToObjectListIndices(i, this.secondObjects.length))
						.iterator());
			}
		}

		@Override
		public ObjectCollection<int[]> decreasingByValue() throws SimilarityValuesException {
			if (hasLargeStorage) {
				final LongBigList sortedByValue = this.largeStorage.decreasingByValue();

				final ObjectBigList<int[]> result = new ObjectBigArrayBigList<>(sortedByValue.size64());
				sortedByValue.forEach(
						(long i) -> result.add(IndexUtils.pairIndexToObjectListIndices(i, this.secondObjects.length)));

				return result;
			} else {
				final int[] sortedByValue = this.smallStorage.decreasingByValue();
				return new ObjectArrayList<>(Arrays.stream(sortedByValue)
						.mapToObj(i -> IndexUtils.pairIndexToObjectListIndices(i, this.secondObjects.length))
						.iterator());
			}
		}

		@Override
		public boolean isMissing(final int x, final int y) throws SimilarityValuesException {
			try {
				if (hasLargeStorage)
					return this.largeStorage.isMissing(getSimilarityIdx(x, y));
				return this.smallStorage.isMissing(requireIntForStorage(getSimilarityIdx(x, y)));
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public boolean isSet(final int x, final int y) throws SimilarityValuesException {
			try {
				if (hasLargeStorage)
					return this.largeStorage.isSet(getSimilarityIdx(x, y));
				return this.smallStorage.isSet(requireIntForStorage(getSimilarityIdx(x, y)));
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public ISimilarityValue maxForFirstObject(int objectId, int... secondObjectIds)
				throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException {
			// we don't use/updated the cache if a subset of the second objects is specified
			if (secondObjectIds.length > 0)
				return get(objectId,
						findMaxSecondObjectIndicesForFirstObjects(new int[] { objectId }, secondObjectIds)[0]);

			if (this.firstObjectMaxCache[objectId] == null)
				this.updateFirstObjectMaxCache();
			return this.firstObjectMaxCache[objectId];
		}

		@Override
		public int[] whichMaxForFirstObject(int[] firstObjectIds, int... secondObjectIds)
				throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException {
			// we don't use/updated the cache if a subset of the second objects is specified
			if (secondObjectIds.length > 0)
				return findMaxSecondObjectIndicesForFirstObjects(firstObjectIds, secondObjectIds);

			final int[] r = new int[firstObjectIds.length];
			for (int p = 0; p < firstObjectIds.length; p++) {
				if (this.firstObjectWhichMaxCache[firstObjectIds[p]] == -1)
					this.updateFirstObjectMaxCache();
				r[p] = this.firstObjectWhichMaxCache[firstObjectIds[p]];
			}
			return r;
		}

		@Override
		public ISimilarityValue maxForSecondObject(int secondObjectId, int... firstObjectIds)
				throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException {
			// we don't use/updated the cache if a subset of the second objects is specified
			if (firstObjectIds.length > 0)
				return get(findMaxFirstObjectIndicesForSecondObjects(firstObjectIds, new int[] { secondObjectId })[0],
						secondObjectId);

			if (this.secondObjectMaxCache[secondObjectId] == null)
				this.updateSecondObjectMaxCache();
			return this.secondObjectMaxCache[secondObjectId];
		}

		@Override
		public int whichMaxForSecondObject(int secondObjectId, int... firstObjectIds)
				throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException {
			// we don't use/updated the cache if a subset of the second objects is specified
			if (firstObjectIds.length > 0)
				return findMaxFirstObjectIndicesForSecondObjects(firstObjectIds, new int[] { secondObjectId })[0];

			if (this.secondObjectWhichMaxCache[secondObjectId] == -1)
				this.updateSecondObjectMaxCache();
			return this.secondObjectWhichMaxCache[secondObjectId];
		}

		@Override
		protected void clearCaches() {
			super.clearCaches();
			this.firstObjectMaxCache = new ISimilarityValue[this.firstObjects.length];
			this.secondObjectMaxCache = new ISimilarityValue[this.secondObjects.length];
			this.firstObjectMinCache = new ISimilarityValue[this.firstObjects.length];
			this.secondObjectMinCache = new ISimilarityValue[this.secondObjects.length];

			this.firstObjectWhichMaxCache = new int[this.firstObjects.length];
			this.firstObjectWhichMinCache = new int[this.firstObjects.length];
			for (int i = 0; i < this.firstObjects.length; i++) {
				this.firstObjectWhichMinCache[i] = -1;
				this.firstObjectWhichMaxCache[i] = -1;
			}
			this.secondObjectWhichMinCache = new int[this.secondObjects.length];
			this.secondObjectWhichMaxCache = new int[this.secondObjects.length];
			for (int i = 0; i < this.secondObjects.length; i++) {
				this.secondObjectWhichMinCache[i] = -1;
				this.secondObjectWhichMaxCache[i] = -1;
			}
			this.whichMaxCache = -1;
			this.whichMinCache = -1;
		}

		protected void clearCaches(final long pairIdx) {
			final int[] idx = IndexUtils.pairIndexToObjectListIndices(pairIdx, this.secondObjects.length);
			final int o1idx = idx[0];
			final int o2idx = idx[1];

			this.firstObjectMaxCache[o1idx] = null;
			this.firstObjectMinCache[o1idx] = null;

			this.firstObjectWhichMaxCache[o1idx] = -1;
			this.firstObjectWhichMinCache[o1idx] = -1;

			this.secondObjectMaxCache[o2idx] = null;
			this.secondObjectMinCache[o2idx] = null;
			this.secondObjectWhichMinCache[o2idx] = -1;
			this.secondObjectWhichMaxCache[o2idx] = -1;

			for (int i = 0; i < this.firstObjects.length; i++) {
				if (this.firstObjectWhichMinCache[i] == o2idx) {
					this.firstObjectMinCache[i] = null;
					this.firstObjectWhichMinCache[i] = -1;
				}

				if (this.firstObjectWhichMaxCache[i] == o2idx) {
					this.firstObjectMaxCache[i] = null;
					this.firstObjectWhichMaxCache[i] = -1;
				}
			}

			for (int i = 0; i < this.secondObjects.length; i++) {
				if (this.secondObjectWhichMinCache[i] == o2idx) {
					this.secondObjectMinCache[i] = null;
					this.secondObjectWhichMinCache[i] = -1;
				}

				if (this.secondObjectWhichMaxCache[i] == o2idx) {
					this.secondObjectMaxCache[i] = null;
					this.secondObjectWhichMaxCache[i] = -1;
				}
			}

			if (this.whichMaxCache == pairIdx) {
				this.whichMaxCache = -1;
				this.maxCache = null;
			}
			if (this.whichMinCache == pairIdx) {
				this.whichMinCache = -1;
				this.minCache = null;
			}
		}

		private int[] findMinSecondObjectIndicesForFirstObjects(final int[] firstObjects, final int[] secondObjects)
				throws SimilarityValuesException {
			final ISimilarityValue[] sims = SimilarityValuesAllPairs.this.getForAllPairs(firstObjects, secondObjects);

			int s = 0;
			int[] minIndices = new int[firstObjects.length];
			for (int i = 0; i < firstObjects.length; i++) {
				final int idx1 = firstObjects[i];
				ISimilarityValue objMinSim = maxValue();
				int minIdx2 = -1;

				for (final int idx2 : secondObjects) {
					final ISimilarityValue sim = sims[s++];
					if (objMinSim.greaterThan(sim)) {
						objMinSim = sim;
						minIdx2 = idx2;
					}
				}
				minIndices[i] = minIdx2;
			}
			return minIndices;
		}

		private int[] findMinFirstObjectIndicesForSecondObjects(final int[] firstObjects, final int[] secondObjects)
				throws SimilarityValuesException {
			final ISimilarityValue[] sims = SimilarityValuesAllPairs.this.getForAllPairs(firstObjects, secondObjects);

			int s = 0;
			int[] minIndices = new int[secondObjects.length];
			for (int i = 0; i < secondObjects.length; i++) {
				final int idx2 = secondObjects[i];
				ISimilarityValue objMinSim = maxValue();
				int minIdx1 = -1;

				for (final int idx1 : firstObjects) {
					final ISimilarityValue sim = sims[s++];
					if (objMinSim.greaterThan(sim)) {
						objMinSim = sim;
						minIdx1 = idx1;
					}
				}
				minIndices[i] = minIdx1;
			}
			return minIndices;
		}

	}

	/**
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Nov 14, 2018
	 *
	 * @param <O1>
	 * @param <O2>
	 */
	public class SimilarityValuesSomePairs<O1, O2, P extends IPair<O1, O2>> extends SimilarityValues<O1, O2, P>
			implements ISimilarityValuesSomePairs<O1, O2, P> {

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 29, 2019
		 *
		 */
		private final class UpdateSecondObjectMinCacheOp implements Operation<long[], Map<O2, Long>> {
			@Override
			public Map<O2, Long> perform(final long[] batch) throws Exception {
				final Map<O2, Long> batchWhichMinima = new HashMap<>();
				final Map<O2, ISimilarityValue> batchMinima = new HashMap<>();
				ISimilarityValue[] sv = get(batch);
				int p = 0;
				for (final long pairIdx : batch) {
					final P pair = SimilarityValuesSomePairs.this.getPair(pairIdx);
					final O2 second = pair.getSecond();

					if (!batchWhichMinima.containsKey(second)) {
						batchWhichMinima.put(second, pairIdx);
						batchMinima.put(second, sv[p]);
					} else {
						final ISimilarityValue minSim = batchMinima.get(second);
						final ISimilarityValue newSim = sv[p];
						if (minSim.greaterThan(newSim))
							batchWhichMinima.put(second, pairIdx);
					}
					p++;
				}
				return batchWhichMinima;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 29, 2019
		 *
		 */
		private final class UpdateSecondObjectMaxCacheOp implements Operation<long[], Map<O2, Long>> {
			@Override
			public Map<O2, Long> perform(final long[] batch) throws Exception {
				final Map<O2, Long> batchWhichMaxima = new HashMap<>();
				final Map<O2, ISimilarityValue> batchMaxima = new HashMap<>();
				ISimilarityValue[] sv = get(batch);
				int p = 0;
				for (final long pairIdx : batch) {
					final P pair = SimilarityValuesSomePairs.this.getPair(pairIdx);
					final O2 second = pair.getSecond();

					if (!batchWhichMaxima.containsKey(second)) {
						batchWhichMaxima.put(second, pairIdx);
						batchMaxima.put(second, sv[p]);
					} else {
						final ISimilarityValue maxSim = batchMaxima.get(second);
						final ISimilarityValue newSim = sv[p];
						if (maxSim.lessThan(newSim))
							batchWhichMaxima.put(second, pairIdx);
					}
					p++;
				}
				return batchWhichMaxima;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 29, 2019
		 *
		 */
		private final class UpdateMinCacheOp implements Operation<long[], Long> {
			@Override
			public Long perform(final long[] batch) throws Exception {
				ISimilarityValue batchMinSim = null;
				Long batchMinIdx = null;

				ISimilarityValue[] sims = get(batch);

				for (int p = 0; p < batch.length; p++) {
					ISimilarityValue s = sims[p];
					if (batchMinSim == null || batchMinSim.compareTo(s) > 0) {
						batchMinSim = s;
						batchMinIdx = batch[p];
					}
				}

				return batchMinIdx;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 29, 2019
		 *
		 */
		private final class UpdateMaxCacheOp implements Operation<long[], Long> {
			@Override
			public Long perform(final long[] batch) throws Exception {
				ISimilarityValue batchMaxSim = null;
				Long batchMaxIdx = null;

				ISimilarityValue[] sims = get(batch);

				int p = 0;
				for (final long idx : batch) {
					if (batchMaxSim == null || batchMaxSim.compareTo(sims[p]) < 0) {
						batchMaxSim = sims[p];
						batchMaxIdx = idx;
					}
					p++;
				}

				return batchMaxIdx;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 29, 2019
		 *
		 */
		private final class UpdateFirstObjectMaxCacheOp implements Operation<long[], Map<O1, Long>> {
			@Override
			public Map<O1, Long> perform(final long[] batch) throws Exception {
				final Map<O1, Long> batchWhichMaxima = new HashMap<>();
				final Map<O1, ISimilarityValue> batchMaxima = new HashMap<>();
				ISimilarityValue[] sv = get(batch);
				int p = 0;
				for (final long pairIdx : batch) {
					final P pair = SimilarityValuesSomePairs.this.getPair(pairIdx);
					final O1 first = pair.getFirst();

					if (!batchWhichMaxima.containsKey(first)) {
						batchWhichMaxima.put(first, pairIdx);
						batchMaxima.put(first, sv[p]);
					} else {
						final ISimilarityValue maxSim = batchMaxima.get(first);
						final ISimilarityValue newSim = sv[p];
						if (maxSim.lessThan(newSim))
							batchWhichMaxima.put(first, pairIdx);
					}
					p++;
				}
				return batchWhichMaxima;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 29, 2019
		 *
		 */
		private final class UpdateFirstObjectMinCacheOp implements Operation<long[], Map<O1, Long>> {
			@Override
			public Map<O1, Long> perform(final long[] batch) throws Exception {
				final Map<O1, Long> batchWhichMinima = new HashMap<>();
				final Map<O1, ISimilarityValue> batchMinima = new HashMap<>();
				ISimilarityValue[] sv = get(batch);
				int p = 0;
				for (final long pairIdx : batch) {
					final P pair = SimilarityValuesSomePairs.this.getPair(pairIdx);
					final O1 first = pair.getFirst();

					if (!batchWhichMinima.containsKey(first)) {
						batchWhichMinima.put(first, pairIdx);
						batchMinima.put(first, sv[p]);
					} else {
						final ISimilarityValue minSim = batchMinima.get(first);
						final ISimilarityValue newSim = sv[p];
						if (minSim.greaterThan(newSim))
							batchWhichMinima.put(first, pairIdx);
					}
					p++;
				}
				return batchWhichMinima;
			}
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -9150225132453825695L;

		protected final ObjectBigList<P> indexToObjectPair;

		protected Map<O1, ISimilarityValue> firstObjectMinCache, firstObjectMaxCache;
		protected Map<O2, ISimilarityValue> secondObjectMinCache, secondObjectMaxCache;

		protected Map<O1, P> firstObjectWhichMinCache, firstObjectWhichMaxCache;
		protected Map<O2, P> secondObjectWhichMinCache, secondObjectWhichMaxCache;

		/**
		 * A constructor creating a {@link ISimilarityValueStorage} matching the
		 * requested number of pairs.
		 * 
		 * @param objectPairs
		 * @param ofType
		 */
		public SimilarityValuesSomePairs(final Collection<P> objectPairs, final ObjectType<?> ofType) {
			super(objectPairs instanceof Size64 ? ((Size64) objectPairs).size64() : objectPairs.size(), ofType);
			this.indexToObjectPair = new ObjectBigArrayBigList<>(objectPairs.iterator());
		}

		/**
		 * @throws IncompatibleSimilarityValueStorageException
		 * 
		 */
		public SimilarityValuesSomePairs(final Collection<P> objectPairs, final ISimilarityValueStorage storage)
				throws IncompatibleSimilarityValueStorageException {
			super(objectPairs instanceof Size64 ? ((Size64) objectPairs).size64() : objectPairs.size(), storage);
			this.indexToObjectPair = new ObjectBigArrayBigList<>(objectPairs.iterator());
		}

		@Override
		public Iterable<P> getPairs() {
			return this.indexToObjectPair;
		}

		@Override
		public long getNumberOfPairs() {
			return this.indexToObjectPair.size64();
		}

		@Override
		public P getPair(final long i) {
			return this.indexToObjectPair.get(i);
		}

		@Override
		public <R> Map<R, ? extends SimilarityValuesSomePairs<O1, O2, P>> groupBy(
				final TriFunction<O1, O2, ISimilarityValue, R> f) throws SimilarityValuesException {
			try {
				if (hasLargeStorage) {
					final Map<R, ObjectBigList<P>> groupedPairs = new LinkedHashMap<>();
					final Map<R, LongBigList> groupedIndices = new LinkedHashMap<>();

					long idx = 0;
					for (final P p : this.indexToObjectPair) {
						final ISimilarityValue s = this.largeStorage.get(idx);
						final R v = f.apply(p.getFirst(), p.getSecond(), s);
						groupedPairs.putIfAbsent(v, new ObjectBigArrayBigList<>());
						groupedPairs.get(v).add(p);
						groupedIndices.putIfAbsent(v, new LongBigArrayBigList());
						groupedIndices.get(v).add(idx);
						idx++;
					}

					final Map<R, SimilarityValuesSomePairs<O1, O2, P>> result = new LinkedHashMap<>();
					for (final R r : groupedPairs.keySet()) {
						final SimilarityValuesSomePairs<O1, O2, P> vals = new SimilarityValuesSomePairs<>(
								groupedPairs.remove(r), this.largeStorage.subset(groupedIndices.get(r)));
						result.put(r, vals);
					}
					return result;
				} else {
					final Map<R, ObjectArrayList<P>> groupedPairs = new LinkedHashMap<>();
					final Map<R, IntList> groupedIndices = new LinkedHashMap<>();

					int idx = 0;
					for (final P p : this.indexToObjectPair) {
						final ISimilarityValue s = this.smallStorage.get(idx);
						final R v = f.apply(p.getFirst(), p.getSecond(), s);
						groupedPairs.putIfAbsent(v, new ObjectArrayList<>());
						groupedPairs.get(v).add(p);
						groupedIndices.putIfAbsent(v, new IntArrayList());
						groupedIndices.get(v).add(idx);
						idx++;
					}

					final Map<R, SimilarityValuesSomePairs<O1, O2, P>> result = new LinkedHashMap<>();
					for (final R r : groupedPairs.keySet()) {
						final SimilarityValuesSomePairs<O1, O2, P> vals = new SimilarityValuesSomePairs<>(
								groupedPairs.remove(r), this.smallStorage.subset(groupedIndices.get(r).toIntArray()));
						result.put(r, vals);
					}
					return result;
				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public SimilarityValuesSomePairs<O1, O2, P> filter(final TriPredicate<O1, O2, ISimilarityValue> p)
				throws SimilarityValuesException {
			try {
				if (hasLargeStorage) {
					final ObjectBigList<P> objectPairs = new ObjectBigArrayBigList<>();
					final LongBigList indices = new LongBigArrayBigList();

					long idx = 0;
					for (final P pair : this.indexToObjectPair) {
						final ISimilarityValue s = this.largeStorage.get(idx);
						if (p.test(pair.getFirst(), pair.getSecond(), s)) {
							objectPairs.add(pair);
							indices.add(idx);
						}
						idx++;
					}

					final SimilarityValuesSomePairs<O1, O2, P> result = new SimilarityValuesSomePairs<>(objectPairs,
							this.largeStorage.subset(indices));
					return result;
				} else {
					final List<P> objectPairs = new ArrayList<>();
					final IntList indices = new IntArrayList();

					int idx = 0;
					for (final P pair : this.indexToObjectPair) {
						final ISimilarityValue s = this.smallStorage.get(idx);
						if (p.test(pair.getFirst(), pair.getSecond(), s)) {
							objectPairs.add(pair);
							indices.add(idx);
						}
						idx++;
					}

					final SimilarityValuesSomePairs<O1, O2, P> result = new SimilarityValuesSomePairs<>(objectPairs,
							this.smallStorage.subset(indices.toIntArray()));
					return result;
				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public Collection<? extends Number> decreasingByValue() throws SimilarityValuesException {
			if (hasLargeStorage)
				return this.largeStorage.decreasingByValue();
			return IntArrayList.wrap(this.smallStorage.decreasingByValue());
		}

		@Override
		public ISimilarityValuesSomePairs<O1, O2, P> sortedByValue()
				throws SimilarityValuesException, InterruptedException {
			try {
				if (hasLargeStorage) {
					final LongBigList sortedIndices = this.largeStorage.decreasingByValue();
					final ObjectBigList<P> sortedPairs = new ObjectBigArrayBigList<>(sortedIndices.size64());
					for (long i = 0; i < sortedIndices.size64(); i++)
						sortedPairs.add(this.getPair(sortedIndices.getLong(i)));
					final ISimilarityValuesSomePairs<O1, O2, P> sorted = new SimilarityValuesSomePairs<>(sortedPairs,
							this.largeStorage.subset(sortedIndices));
					return sorted;
				} else {
					final int[] sortedIndices = this.smallStorage.decreasingByValue();
					final ObjectBigList<P> sortedPairs = new ObjectBigArrayBigList<>(sortedIndices.length);
					for (int i = 0; i < sortedIndices.length; i++)
						sortedPairs.add(this.getPair(sortedIndices[i]));
					final ISimilarityValuesSomePairs<O1, O2, P> sorted = new SimilarityValuesSomePairs<>(sortedPairs,
							this.smallStorage.subset(sortedIndices));
					return sorted;

				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		protected void updateMinCache() throws SimilarityValuesException, InterruptedException {
			ISimilarityValue minSim = null;
			Long minIdx = null;

			final Iterable<long[]> batches = Iterables.batchifyLongs(this.pairIndices(),
					new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

			final List<Future<Long>> futures = new MyParallel<Long>(privateSimilarityFunctionThreadPool).For(batches,
					new UpdateMinCacheOp());

			final long[] batchMins = new long[futures.size()];
			int p = 0;
			for (final Future<Long> f : futures) {
				try {
					batchMins[p++] = f.get();

				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				}
			}

			try {
				ISimilarityValue[] batchMinSims = get(batchMins);
				for (p = 0; p < batchMinSims.length; p++) {
					ISimilarityValue v = batchMinSims[p];
					if (minSim == null || minSim.compareTo(v) > 0) {
						minSim = v;
						minIdx = batchMins[p];
					}
				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}

			this.minCache = minSim;
			this.whichMinCache = minIdx;
		}

		@Override
		protected void updateMaxCache() throws SimilarityValuesException, InterruptedException {
			if (this.isEmpty())
				return;

			ISimilarityValue maxSim = null;
			Long maxIdx = null;

			final Iterable<long[]> batches = Iterables.batchifyLongs(this.pairIndices(),
					new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

			final List<Future<Long>> futures = new MyParallel<Long>(privateSimilarityFunctionThreadPool).For(batches,
					new UpdateMaxCacheOp());

			final long[] batchMaxs = new long[futures.size()];
			int p = 0;
			for (final Future<Long> f : futures) {
				try {
					batchMaxs[p++] = f.get();

				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				}
			}

			try {
				ISimilarityValue[] batchMaxSims = get(batchMaxs);
				for (p = 0; p < batchMaxSims.length; p++) {
					ISimilarityValue v = batchMaxSims[p];
					if (maxSim == null || maxSim.compareTo(v) > 0) {
						maxSim = v;
						maxIdx = batchMaxs[p];
					}
				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}

			this.maxCache = maxSim;
			this.whichMaxCache = maxIdx;
		}

		@Override
		public Map<O1, ? extends ISimilarityValuesSomePairs<O1, O2, P>> partitionByFirstObject()
				throws SimilarityValuesException, InterruptedException {
			try {
				final Map<O1, List<P>> tmp = new LinkedHashMap<>();

				if (hasLargeStorage) {
					final Map<O1, LongBigList> indices = new LinkedHashMap<>();

					for (long i = 0; i < this.largeStorage.size(); i++) {
						final P p = this.getPair(i);
						final O1 o1 = p.getFirst();
						tmp.putIfAbsent(o1, new ArrayList<>());
						tmp.get(o1).add(p);
						indices.putIfAbsent(o1, new LongBigArrayBigList());
						indices.get(o1).add(i);
					}
					final Map<O1, SimilarityValuesSomePairs<O1, O2, P>> result = new LinkedHashMap<>();
					for (final O1 o1 : tmp.keySet())
						result.put(o1, new SimilarityValuesSomePairs<>(tmp.get(o1),
								this.largeStorage.subset(indices.get(o1))));
					return result;
				} else {
					final Map<O1, IntArrayList> indices = new LinkedHashMap<>();

					for (int i = 0; i < this.smallStorage.size(); i++) {
						final P p = this.getPair(i);
						final O1 o1 = p.getFirst();
						tmp.putIfAbsent(o1, new ArrayList<>());
						tmp.get(o1).add(p);
						indices.putIfAbsent(o1, new IntArrayList());
						indices.get(o1).add(i);
					}
					final Map<O1, SimilarityValuesSomePairs<O1, O2, P>> result = new LinkedHashMap<>();
					for (final O1 o1 : tmp.keySet())
						result.put(o1, new SimilarityValuesSomePairs<>(tmp.get(o1),
								this.smallStorage.subset(indices.get(o1).toIntArray())));
					return result;

				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public Map<O2, ? extends ISimilarityValuesSomePairs<O1, O2, P>> partitionBySecondObject()
				throws SimilarityValuesException, InterruptedException {
			try {
				final Map<O2, List<P>> tmp = new LinkedHashMap<>();
				if (hasLargeStorage) {
					final Map<O2, LongBigList> indices = new LinkedHashMap<>();

					for (long i = 0; i < this.largeStorage.size(); i++) {
						final P p = this.getPair(i);
						final O2 o2 = p.getSecond();
						tmp.putIfAbsent(o2, new ArrayList<>());
						tmp.get(o2).add(p);
						indices.putIfAbsent(o2, new LongBigArrayBigList());
						indices.get(o2).add(i);
					}
					final Map<O2, SimilarityValuesSomePairs<O1, O2, P>> result = new LinkedHashMap<>();
					for (final O2 o2 : tmp.keySet())
						result.put(o2, new SimilarityValuesSomePairs<>(tmp.get(o2),
								this.largeStorage.subset(indices.get(o2))));
					return result;
				} else {
					final Map<O2, IntArrayList> indices = new LinkedHashMap<>();

					for (int i = 0; i < this.smallStorage.size(); i++) {
						final P p = this.getPair(i);
						final O2 o2 = p.getSecond();
						tmp.putIfAbsent(o2, new ArrayList<>());
						tmp.get(o2).add(p);
						indices.putIfAbsent(o2, new IntArrayList());
						indices.get(o2).add(i);
					}
					final Map<O2, SimilarityValuesSomePairs<O1, O2, P>> result = new LinkedHashMap<>();
					for (final O2 o2 : tmp.keySet())
						result.put(o2, new SimilarityValuesSomePairs<>(tmp.get(o2),
								this.smallStorage.subset(indices.get(o2).toIntArray())));
					return result;

				}
			} catch (IncompatibleSimilarityValueStorageException e) {
				throw new SimilarityValuesException(e);
			}
		}

		@Override
		public Collection<? extends Number> increasingByValue() throws SimilarityValuesException {
			if (hasLargeStorage)
				return this.largeStorage.increasingByValue();
			return IntArrayList.wrap(this.smallStorage.increasingByValue());
		}

		@Override
		public LongCollection pairIndices() {
			if (hasLargeStorage)
				return indicesLargeStorage();
			else
				return LongArrayList.wrap(Arrays.stream(indicesSmallStorage()).mapToLong(i -> i).toArray());
		}

		protected LongBigArrayBigList indicesLargeStorage() {
			return new LongBigArrayBigList(new LongIterator() {
				long c = 0;

				@Override
				public boolean hasNext() {
					return c < indexToObjectPair.size64();
				}

				@Override
				public long nextLong() {
					return c++;
				}
			});
		}

		protected int[] indicesSmallStorage() {
			return IntStream.range(0, indexToObjectPair.size()).toArray();
		}

		@Override
		protected void updateFirstObjectMaxCache() throws SimilarityValuesException, InterruptedException {
			final Iterable<long[]> batches = Iterables.batchifyLongs(this.pairIndices(),
					new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

			final List<Future<Map<O1, Long>>> futures = new MyParallel<Map<O1, Long>>(
					privateSimilarityFunctionThreadPool).For(batches, new UpdateFirstObjectMaxCacheOp());

			final Map<O1, ISimilarityValue> all = new Object2ObjectOpenHashMap<>();
			final Map<O1, P> allWhich = new Object2ObjectOpenHashMap<>();

			for (final Future<Map<O1, Long>> f : futures) {
				try {
					final Map<O1, Long> map = f.get();
					final O1[] objects = (O1[]) new Object[map.size()];
					final long[] idxs = new long[map.size()];
					int p = 0;
					for (Map.Entry<O1, Long> e : map.entrySet()) {
						objects[p] = e.getKey();
						idxs[p] = e.getValue();
						p++;
					}
					final ISimilarityValue[] vs = get(idxs);

					for (p = 0; p < objects.length; p++) {
						final O1 o = objects[p];
						final ISimilarityValue newSim = vs[p];
						boolean insert = !all.containsKey(o) || newSim.greaterThan(all.get(o));
						if (insert) {
							all.put(o, newSim);
							allWhich.put(o, getPair(idxs[p]));
						}
					}
				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				} catch (IncompatibleSimilarityValueStorageException e) {
					throw new SimilarityValuesException(e);
				}
			}
			this.firstObjectMaxCache = all;
			this.firstObjectWhichMaxCache = allWhich;
		}

		@Override
		protected void updateFirstObjectMinCache() throws SimilarityValuesException, InterruptedException {
			final Iterable<long[]> batches = Iterables.batchifyLongs(this.pairIndices(),
					new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

			final List<Future<Map<O1, Long>>> futures = new MyParallel<Map<O1, Long>>(
					privateSimilarityFunctionThreadPool).For(batches, new UpdateFirstObjectMinCacheOp());

			final Map<O1, ISimilarityValue> all = new Object2ObjectOpenHashMap<>();
			final Map<O1, P> allWhich = new Object2ObjectOpenHashMap<>();

			for (final Future<Map<O1, Long>> f : futures) {
				try {
					final Map<O1, Long> map = f.get();
					final O1[] objects = (O1[]) new Object[map.size()];
					final long[] idxs = new long[map.size()];
					int p = 0;
					for (Map.Entry<O1, Long> e : map.entrySet()) {
						objects[p] = e.getKey();
						idxs[p] = e.getValue();
						p++;
					}
					final ISimilarityValue[] vs = get(idxs);

					for (p = 0; p < objects.length; p++) {
						final O1 o = objects[p];
						final ISimilarityValue newSim = vs[p];
						boolean insert = !all.containsKey(o) || newSim.lessThan(all.get(o));
						if (insert) {
							all.put(o, newSim);
							allWhich.put(o, getPair(idxs[p]));
						}
					}
				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				} catch (IncompatibleSimilarityValueStorageException e) {
					throw new SimilarityValuesException(e);
				}
			}
			this.firstObjectMinCache = all;
		}

		@Override
		protected void updateSecondObjectMaxCache() throws SimilarityValuesException, InterruptedException {
			final Iterable<long[]> batches = Iterables.batchifyLongs(this.pairIndices(),
					new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

			final List<Future<Map<O2, Long>>> futures = new MyParallel<Map<O2, Long>>(
					privateSimilarityFunctionThreadPool).For(batches, new UpdateSecondObjectMaxCacheOp());

			final Map<O2, ISimilarityValue> all = new Object2ObjectOpenHashMap<>();
			final Map<O2, P> allWhich = new Object2ObjectOpenHashMap<>();

			for (final Future<Map<O2, Long>> f : futures) {
				try {
					final Map<O2, Long> map = f.get();
					final O2[] objects = (O2[]) new Object[map.size()];
					final long[] idxs = new long[map.size()];
					int p = 0;
					for (Entry<O2, Long> e : map.entrySet()) {
						objects[p] = e.getKey();
						idxs[p] = e.getValue();
						p++;
					}
					final ISimilarityValue[] vs = get(idxs);

					for (p = 0; p < objects.length; p++) {
						final O2 o = objects[p];
						final ISimilarityValue newSim = vs[p];
						boolean insert = !all.containsKey(o) || newSim.greaterThan(all.get(o));
						if (insert) {
							all.put(o, newSim);
							allWhich.put(o, getPair(idxs[p]));
						}
					}
				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				} catch (IncompatibleSimilarityValueStorageException e) {
					throw new SimilarityValuesException(e);
				}
			}
			this.secondObjectMaxCache = all;
			this.secondObjectWhichMaxCache = allWhich;
		}

		@Override
		protected void updateSecondObjectMinCache() throws SimilarityValuesException, InterruptedException {
			final Iterable<long[]> batches = Iterables.batchifyLongs(this.pairIndices(),
					new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), 500000));

			final List<Future<Map<O2, Long>>> futures = new MyParallel<Map<O2, Long>>(
					privateSimilarityFunctionThreadPool).For(batches, new UpdateSecondObjectMinCacheOp());

			final Map<O2, ISimilarityValue> all = new Object2ObjectOpenHashMap<>();
			final Map<O2, P> allWhich = new Object2ObjectOpenHashMap<>();

			for (final Future<Map<O2, Long>> f : futures) {
				try {
					final Map<O2, Long> map = f.get();
					final O2[] objects = (O2[]) new Object[map.size()];
					final long[] idxs = new long[map.size()];
					int p = 0;
					for (Entry<O2, Long> e : map.entrySet()) {
						objects[p] = e.getKey();
						idxs[p] = e.getValue();
						p++;
					}
					final ISimilarityValue[] vs = get(idxs);

					for (p = 0; p < objects.length; p++) {
						final O2 o = objects[p];
						final ISimilarityValue newSim = vs[p];
						boolean insert = !all.containsKey(o) || newSim.lessThan(all.get(o));
						if (insert) {
							all.put(o, newSim);
							allWhich.put(o, getPair(idxs[p]));
						}
					}
				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				} catch (IncompatibleSimilarityValueStorageException e) {
					throw new SimilarityValuesException(e);
				}
			}
			this.secondObjectMinCache = all;
			this.secondObjectWhichMinCache = allWhich;
		}

		@Override
		public Map<O1, ISimilarityValue> maxByFirstObject()
				throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException {
			if (this.firstObjectWhichMaxCache == null)
				updateFirstObjectMaxCache();
			return this.firstObjectMaxCache;
		}

		@Override
		public Map<O1, P> whichMaxByFirstObject()
				throws SimilarityValuesException, InterruptedException, NoComparableSimilarityValuesException {
			if (this.firstObjectWhichMaxCache == null)
				updateFirstObjectMaxCache();
			return this.firstObjectWhichMaxCache;
		}

		@Override
		public Map<O2, ISimilarityValue> maxBySecondObject()
				throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException {
			if (this.secondObjectWhichMaxCache == null)
				updateSecondObjectMaxCache();
			return this.secondObjectMaxCache;
		}

		@Override
		public Map<O2, P> whichMaxBySecondObject()
				throws SimilarityValuesException, InterruptedException, NoComparableSimilarityValuesException {
			if (this.secondObjectWhichMaxCache == null)
				updateSecondObjectMaxCache();
			return this.secondObjectWhichMaxCache;
		}

		@Override
		protected void clearCaches() {
			super.clearCaches();
			this.firstObjectMaxCache = null;
			this.secondObjectMaxCache = null;
			this.firstObjectMinCache = null;
			this.secondObjectMinCache = null;
		}

	}

	class SmallSubsetOfLargeStorage implements ISmallSimilarityValueStorage {

		ILargeSimilarityValueStorage storage;
		long[] indices;

		public SmallSubsetOfLargeStorage(ILargeSimilarityValueStorage storage, long[] indices) {
			this.storage = storage;
			this.indices = indices;
		}

		@Override
		public ObjectType<?> getObjectType() {
			return this.storage.getObjectType();
		}

		@Override
		public ISimilarityValueStorage copy() {
			return new SmallSubsetOfLargeStorage(storage, indices);
		}

		protected long translateIndex(int i) {
			return this.indices[i];
		}

		protected long[] translateIndices(int[] indices) {
			final long[] result = new long[indices.length];
			for (int i = 0; i < indices.length; i++)
				result[i] = translateIndex(i);
			return result;
		}

		@Override
		public int[] decreasingByValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int[] increasingByValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ISimilarityValue get(long i) {
			return get(Math.toIntExact(i));
		}

		@Override
		public ISimilarityValue[] get(long[] is) {
			return Arrays.stream(is).mapToObj(i -> get(i)).toArray(ISimilarityValue[]::new);
		}

		@Override
		public ISimilarityValue get(int i) {
			return this.storage.get(translateIndex(i));
		}

		@Override
		public ISimilarityValue[] get(int[] is) {
			return Arrays.stream(is).mapToObj(i -> get(i)).toArray(ISimilarityValue[]::new);
		}

		@Override
		public boolean isMissing(long i) {
			return isMissing(Math.toIntExact(i));
		}

		@Override
		public boolean[] isMissing(long[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isMissing(is[p]);
			return r;
		}

		@Override
		public boolean isMissing(int i) {
			return this.storage.isMissing(translateIndex(i));
		}

		@Override
		public boolean[] isMissing(int[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isMissing(is[p]);
			return r;
		}

		@Override
		public boolean isSet(long i) {
			return isSet(Math.toIntExact(i));
		}

		@Override
		public boolean[] isSet(long[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isSet(is[p]);
			return r;
		}

		@Override
		public boolean isSet(int i) {
			return this.storage.isSet(translateIndex(i));
		}

		@Override
		public boolean[] isSet(int[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isSet(is[p]);
			return r;
		}

		@Override
		public ISimilarityValue mean() throws InterruptedException, SimilarityValuesException {
			return this.storage.mean(indices);
		}

		@Override
		public ISimilarityValue mean(int[] indices) throws InterruptedException, SimilarityValuesException {
			return this.storage.mean(translateIndices(indices));
		}

		@Override
		public ISimilarityValue median() throws InterruptedException, SimilarityValuesException {
			return this.storage.median(this.indices);
		}

		@Override
		public ISimilarityValue median(int[] indices) throws InterruptedException, SimilarityValuesException {
			return this.storage.median(translateIndices(indices));
		}

		@Override
		public ISimilarityValue set(long i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.storage.set(i, value);
		}

		@Override
		public ISimilarityValue[] set(long[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.storage.set(is, values);
		}

		@Override
		public ISimilarityValue set(int i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.storage.set(i, value);
		}

		@Override
		public ISimilarityValue[] set(int[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.storage.set(is, values);
		}

		@Override
		public long size() {
			return this.indices.length;
		}

		@Override
		public ISimilarityValue sum() throws InterruptedException, SimilarityValuesException {
			return this.storage.sum(this.indices);
		}

		@Override
		public ISimilarityValue sum(int[] indices) throws InterruptedException, SimilarityValuesException {
			return this.storage.sum(translateIndices(indices));
		}

		@Override
		public ISmallSimilarityValueStorage subset(int[] indices) {
			throw new UnsupportedOperationException();
		}

		@Override
		public ISimilarityValue[] values() {
			final ISimilarityValue[] values = new ISimilarityValue[indices.length];
			for (int c = 0; c < indices.length; c++)
				values[c] = this.get(c);
			return values;
		}

		@Override
		public Iterator<ISimilarityValue> iterator() {
			throw new UnsupportedOperationException();
		}
	}

	class SmallSubsetOfSmallStorage implements ISmallSimilarityValueStorage {

		ISmallSimilarityValueStorage storage;
		int[] indices;

		public SmallSubsetOfSmallStorage(ISmallSimilarityValueStorage storage, int[] indices) {
			this.storage = storage;
			this.indices = indices;
		}

		@Override
		public ObjectType<?> getObjectType() {
			return this.storage.getObjectType();
		}

		@Override
		public ISimilarityValueStorage copy() {
			return new SmallSubsetOfSmallStorage(storage, indices);
		}

		@Override
		public ISmallSimilarityValueStorage subset(int[] indices) {
			return this.storage.subset(translateIndices(indices));
		}

		@Override
		public int[] increasingByValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int[] decreasingByValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ISimilarityValue median() throws InterruptedException, SimilarityValuesException {
			return this.storage.median(indices);
		}

		@Override
		public ISimilarityValue median(int[] indices) throws InterruptedException, SimilarityValuesException {
			return this.storage.median(indices);
		}

		@Override
		public ISimilarityValue sum() throws InterruptedException, SimilarityValuesException {
			return this.storage.sum(indices);
		}

		@Override
		public ISimilarityValue sum(int[] indices) throws InterruptedException, SimilarityValuesException {
			return this.storage.sum(translateIndices(indices));
		}

		@Override
		public ISimilarityValue mean() throws InterruptedException, SimilarityValuesException {
			return this.storage.mean(indices);
		}

		@Override
		public ISimilarityValue mean(int[] indices) throws InterruptedException, SimilarityValuesException {
			return this.storage.mean(translateIndices(indices));
		}

		@Override
		public ISimilarityValue get(long i) throws IncompatibleSimilarityValueStorageException {
			return get(translateIndex(requireIntForStorage(i)));
		}

		@Override
		public ISimilarityValue[] get(long[] is) throws IncompatibleSimilarityValueStorageException {
			return storage.get(translateIndices(requireIntForStorage(is)));
		}

		@Override
		public ISimilarityValue get(int i) {
			return this.storage.get(translateIndex(i));
		}

		@Override
		public boolean isMissing(long i) {
			return isMissing(Math.toIntExact(i));
		}

		@Override
		public boolean isMissing(int i) {
			return this.storage.isMissing(translateIndex(i));
		}

		@Override
		public boolean isSet(long i) {
			return isSet(Math.toIntExact(i));
		}

		@Override
		public boolean isSet(int i) {
			return this.storage.isSet(translateIndex(i));
		}

		@Override
		public ISimilarityValue set(long i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.set(Math.toIntExact(i), value);
		}

		@Override
		public ISimilarityValue set(int i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.storage.set(translateIndex(i), value);
		}

		@Override
		public long size() {
			return this.indices.length;
		}

		@Override
		public ISimilarityValue[] values() {
			return this.storage.get(this.indices);
		}

		protected int translateIndex(final int subsetIdx) {
			return this.indices[subsetIdx];
		}

		protected int[] translateIndices(final int[] subsetIdx) {
			final int[] result = new int[subsetIdx.length];
			for (int i = 0; i < subsetIdx.length; i++)
				result[i] = translateIndex(subsetIdx[i]);
			return result;
		}

		@Override
		public ISimilarityValue[] get(int[] is) {
			return storage.get(translateIndices(is));
		}

		@Override
		public boolean[] isMissing(int[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isMissing(is[p]);
			return r;
		}

		@Override
		public boolean[] isMissing(long[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isMissing(is[p]);
			return r;
		}

		@Override
		public boolean[] isSet(int[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isSet(is[p]);
			return r;
		}

		@Override
		public boolean[] isSet(long[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isSet(is[p]);
			return r;
		}

		@Override
		public ISimilarityValue[] set(int[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.storage.set(is, values);
		}

		@Override
		public ISimilarityValue[] set(long[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.storage.set(is, values);
		}

		@Override
		public Iterator<ISimilarityValue> iterator() {
			return new Iterator<ISimilarityValue>() {

				int pos = 0;

				@Override
				public ISimilarityValue next() {
					return get(pos++);
				}

				@Override
				public boolean hasNext() {
					return pos < indices.length;
				}
			};
		}
	}

	class LargeSimilarityValueSubset implements ILargeSimilarityValueStorage {

		ILargeSimilarityValueStorage largeStorage;
		LongBigList indices;

		LargeSimilarityValueSubset(ILargeSimilarityValueStorage largeStorage, LongBigList indices) {
			super();
			this.indices = indices;
		}

		@Override
		public ObjectType<?> getObjectType() {
			return this.largeStorage.getObjectType();
		}

		@Override
		public LargeSimilarityValueSubset copy() {
			return new LargeSimilarityValueSubset(this, indices);
		}

		@Override
		public ILargeSimilarityValueStorage subset(LongBigList indices) {
			throw new UnsupportedOperationException();
		}

		@Override
		public SmallSubsetOfLargeStorage subset(long[] indices) {
			throw new UnsupportedOperationException();
		}

		@Override
		public LongBigList increasingByValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public LongBigList decreasingByValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ISimilarityValue median() throws InterruptedException, SimilarityValuesException {
			throw new UnsupportedOperationException();
		}

		@Override
		public ISimilarityValue median(long[] indices) throws InterruptedException, SimilarityValuesException {
			throw new UnsupportedOperationException();
		}

		@Override
		public ISimilarityValue median(LongBigList indices) throws InterruptedException, SimilarityValuesException {
			throw new UnsupportedOperationException();
		}

		protected long translateIndex(final long subsetIdx) {
			return this.indices.getLong(subsetIdx);
		}

		@Override
		public ISimilarityValue get(long i) {
			return this.largeStorage.get(translateIndex(i));
		}

		@Override
		public ISimilarityValue get(int i) {
			return this.get((long) i);
		}

		@Override
		public ISimilarityValue set(long i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.largeStorage.set(translateIndex(i), value);
		}

		@Override
		public ISimilarityValue[] set(long[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.largeStorage.set(translateIndices(is), values);
		}

		@Override
		public ISimilarityValue set(int i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.set((long) i, value);
		}

		@Override
		public ISimilarityValue[] set(int[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.set(is, values);
		}

		@Override
		public long size() {
			return this.indices.size64();
		}

		@Override
		public ObjectBigList<ISimilarityValue> values() {
			final ObjectBigList<ISimilarityValue> values = new ObjectBigArrayBigList<>(this.size());
			for (long idx = 0; idx < this.size(); idx++)
				values.add(this.get(idx));
			return values;
		}

		@Override
		public boolean isMissing(long i) {
			return this.largeStorage.isMissing(translateIndex(i));
		}

		@Override
		public boolean isMissing(int i) {
			return this.isMissing((long) i);
		}

		@Override
		public boolean isSet(long i) {
			return this.largeStorage.isSet(translateIndex(i));
		}

		@Override
		public boolean isSet(int i) {
			return this.isSet((long) i);
		}

		@Override
		public ISimilarityValue sum() throws InterruptedException, SimilarityValuesException {
			return this.largeStorage.sum(indices);
		}

		@Override
		public ISimilarityValue mean() throws InterruptedException, SimilarityValuesException {
			return this.largeStorage.mean(indices);
		}

		protected LongBigList translateIndices(LongBigList indices) {
			return indices.stream().map(l -> this.indices.getLong(l))
					.collect(Collectors.toCollection(LongBigArrayBigList::new));
		}

		protected long[] translateIndices(long[] indices) {
			final long[] result = new long[indices.length];
			for (int i = 0; i < indices.length; i++)
				result[i] = this.indices.getLong(indices[i]);
			return result;
		}

		@Override
		public ISimilarityValue mean(LongBigList indices) throws InterruptedException, SimilarityValuesException {
			return this.largeStorage.mean(translateIndices(indices));
		}

		@Override
		public ISimilarityValue mean(long[] indices) throws InterruptedException, SimilarityValuesException {
			return this.largeStorage.mean(translateIndices(indices));
		}

		@Override
		public ISimilarityValue sum(LongBigList indices) throws InterruptedException, SimilarityValuesException {
			return this.largeStorage.sum(translateIndices(indices));
		}

		@Override
		public ISimilarityValue sum(long[] indices) throws InterruptedException, SimilarityValuesException {
			return this.largeStorage.sum(translateIndices(indices));
		}

		@Override
		public ISimilarityValue[] get(int[] is) {
			return Arrays.stream(is).mapToObj(i -> get(i)).toArray(ISimilarityValue[]::new);
		}

		@Override
		public ISimilarityValue[] get(long[] is) {
			return Arrays.stream(is).mapToObj(i -> get(i)).toArray(ISimilarityValue[]::new);
		}

		@Override
		public boolean[] isMissing(int[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isMissing(is[p]);
			return r;
		}

		@Override
		public boolean[] isMissing(long[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isMissing(is[p]);
			return r;
		}

		@Override
		public boolean[] isSet(int[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isSet(is[p]);
			return r;
		}

		@Override
		public boolean[] isSet(long[] is) {
			boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isSet(is[p]);
			return r;
		}

		@Override
		public Iterator<ISimilarityValue> iterator() {
			throw new UnsupportedOperationException();
		}
	}

	public abstract class AbstractSimilarityValueStorage implements ISimilarityValueStorage, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1861763685630338809L;

		protected final ReadWriteLock rwLock = new ReentrantReadWriteLock();

		protected final ObjectType<?> ofType;
		protected final boolean isComposite;

		// only used if this storage is composite
		protected final ISimpleSimilarityFunction[] childFunctions;

		AbstractSimilarityValueStorage(final ObjectType<?> ofType) {
			this(ofType, new ISimpleSimilarityFunction[0]);
		}

		AbstractSimilarityValueStorage(final ObjectType<?> ofType, final ISimpleSimilarityFunction[] childFunctions) {
			super();
			this.ofType = ofType;
			this.isComposite = childFunctions != null && childFunctions.length > 0;
			this.childFunctions = childFunctions;
		}

		/**
		 * @return the ofType
		 */
		@Override
		public ObjectType<?> getObjectType() {
			return this.ofType;
		}

		/**
		 * @return the isComposite
		 */
		public boolean isComposite() {
			return this.isComposite;
		}

		protected abstract boolean isMissingChildValue(long i, int sf);

		protected abstract boolean isChildValueSet(long i, int sf);

		protected abstract double getRawValue(long i);

		protected abstract double[] getRawChildValues(long i);
	}

	public class LargeSimilarityValueStorage extends AbstractSimilarityValueStorage
			implements ILargeSimilarityValueStorage {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1630732952509947668L;

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class MeanCompositeOp implements Operation<long[], Pair<double[], int[]>> {
			@Override
			public Pair<double[], int[]> perform(final long[] it) {
				final int[] counts = new int[childFunctions.length];
				final double[] sum = new double[childFunctions.length];

				for (final long child : it) {
					final double[] vals = childValues.get(child);
					for (int cSF = 0; cSF < vals.length; cSF++)
						if (!isMissingChildValue.get(child)[cSF] && isChildValueSet.get(child)[cSF]
								&& Double.isFinite(vals[cSF])) {
							sum[cSF] += vals[cSF];
							counts[cSF]++;
						}
				}

				return Pair.of(sum, counts);
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class MeanNoncompositeOp implements Operation<long[], Pair<Double, Integer>> {
			@Override
			public Pair<Double, Integer> perform(final long[] it) {
				int batchCount = 0;
				double batchSum = 0.0;

				for (final long i : it) {
					final double val = values.getDouble(i);
					if (!isMissingValue.getBoolean(i) && isValueSet.getBoolean(i) && Double.isFinite(val)) {
						batchSum += val;
						batchCount++;
					}
				}

				return Pair.of(batchSum, batchCount);
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class MedianCompositeOp implements Operation<Iterable<Integer>, List<Double>> {
			@Override
			public List<Double> perform(final Iterable<Integer> childFuncIdxs) {
				final List<Double> result = new ArrayList<>();
				for (Integer childFuncIdx : childFuncIdxs) {
					final double[] sortedValues = LongStream.range(0, childValues.size64()).filter(idx -> {
						return !isMissingChildValue.get(idx)[childFuncIdx] && isChildValueSet.get(idx)[childFuncIdx];
					}).mapToDouble(idx -> childValues.get(idx)[childFuncIdx]).sorted().toArray();

					final double median = sortedValues.length % 2 == 1 ? sortedValues[sortedValues.length / 2]
							: (sortedValues[sortedValues.length / 2 - 1] + sortedValues[sortedValues.length / 2]) / 2.0;

					result.add(median);
				}

				return result;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class SumNoncompositeOp implements Operation<long[], Double> {
			@Override
			public Double perform(final long[] it) {
				double batchSum = 0.0;

				for (final long i : it) {
					final double val = values.getDouble(i);
					if (!isMissingValue.getBoolean(i) && isValueSet.getBoolean(i) && Double.isFinite(val)) {
						batchSum += val;
					}
				}

				return batchSum;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class SumCompositeOp implements Operation<long[], double[]> {

			@Override
			public double[] perform(final long[] it) {
				final double[] result = new double[childFunctions.length];

				for (final long child : it) {
					final double[] vals = childValues.get(child);
					for (int cSF = 0; cSF < vals.length; cSF++)
						if (!isMissingChildValue.get(child)[cSF] && isChildValueSet.get(child)[cSF]
								&& Double.isFinite(vals[cSF]))
							result[cSF] += vals[cSF];
				}

				return result;
			}
		}

		protected final BooleanBigList isValueSet, isMissingValue, isMaxValue, isMinValue;
		protected final DoubleBigList values;

		// only used if this storage is composite
		protected final ObjectBigList<boolean[]> isChildValueSet, isMissingChildValue, isMaxChildValue, isMinChildValue;
		protected final ObjectBigList<double[]> childValues;

		/**
		 * Constructs a non-composite storage.
		 * 
		 * @param size
		 * @param ofType
		 */
		LargeSimilarityValueStorage(final long size, final ObjectType<?> ofType) {
			super(ofType);
			this.isValueSet = new BooleanBigArrayBigList(size);
			this.isValueSet.size(size);
			this.isMissingValue = new BooleanBigArrayBigList(size);
			this.isMissingValue.size(size);
			this.isMaxValue = new BooleanBigArrayBigList(size);
			this.isMaxValue.size(size);
			this.isMinValue = new BooleanBigArrayBigList(size);
			this.isMinValue.size(size);
			this.values = new DoubleBigArrayBigList(size);
			this.values.size(size);
			for (long i = 0; i < this.values.size64(); i++)
				this.values.set(i, Double.NaN);

			this.isChildValueSet = null;
			this.isMissingChildValue = null;
			this.isMaxChildValue = null;
			this.isMinChildValue = null;
			this.childValues = null;
		}

		/**
		 * A copy constructor for a non-composite storage.
		 * 
		 * @param isValueSet
		 * @param indicesValueSet
		 * @param isMissingValue
		 * @param values
		 * @param ofType
		 */
		LargeSimilarityValueStorage(final BooleanBigList isValueSet, final BooleanBigList isMissingValue,
				final BooleanBigList isMaxValue, final BooleanBigList isMinValue, final DoubleBigList values,
				final ObjectType<?> ofType) {
			super(ofType);
			this.isValueSet = new BooleanBigArrayBigList(isValueSet);
			this.isMissingValue = new BooleanBigArrayBigList(isMissingValue);
			this.isMaxValue = new BooleanBigArrayBigList(isMaxValue);
			this.isMinValue = new BooleanBigArrayBigList(isMinValue);
			this.values = new DoubleBigArrayBigList(values);

			this.isChildValueSet = null;
			this.isMissingChildValue = null;
			this.isMaxChildValue = null;
			this.isMinChildValue = null;
			this.childValues = null;
		}

		/**
		 * Constructs a composite storage.
		 * 
		 * @param size
		 * @param ofType
		 * @param childFunctions
		 */
		LargeSimilarityValueStorage(final long size, final ObjectType<?> ofType,
				final ISimpleSimilarityFunction[] childFunctions) {
			super(ofType, childFunctions);
			this.isValueSet = new BooleanBigArrayBigList(size);
			this.isValueSet.size(size);
			this.isMissingValue = new BooleanBigArrayBigList(size);
			this.isMissingValue.size(size);
			this.isMaxValue = new BooleanBigArrayBigList(size);
			this.isMaxValue.size(size);
			this.isMinValue = new BooleanBigArrayBigList(size);
			this.isMinValue.size(size);
			this.values = new DoubleBigArrayBigList(size);
			this.values.size(size);
			for (long i = 0; i < this.values.size64(); i++)
				this.values.set(i, Double.NaN);

			this.isChildValueSet = new ObjectBigArrayBigList<>(size);
			this.isMissingChildValue = new ObjectBigArrayBigList<>(size);
			this.isMaxChildValue = new ObjectBigArrayBigList<>(size);
			this.isMinChildValue = new ObjectBigArrayBigList<>(size);
			this.childValues = new ObjectBigArrayBigList<>(size);
			for (long i = 0; i < size; i++) {
				this.isChildValueSet.set(i, new boolean[childFunctions.length]);
				this.isMissingChildValue.set(i, new boolean[childFunctions.length]);
				this.isMaxChildValue.set(i, new boolean[childFunctions.length]);
				this.isMinChildValue.set(i, new boolean[childFunctions.length]);
				final double[] cVs = new double[childFunctions.length];
				for (int j = 0; j < cVs.length; j++)
					cVs[j] = Double.NaN;
				this.childValues.set(i, cVs);
			}
		}

		/**
		 * A copy constructor for a composite storage.
		 * 
		 * @param isValueSet
		 * @param indicesValueSet
		 * @param isMissingValue
		 * @param values
		 * @param isChildValueSet
		 * @param childIndicesValueSet
		 * @param isMissingChildValue
		 * @param childValues
		 * @param ofType
		 */
		LargeSimilarityValueStorage(final ISimpleSimilarityFunction[] childFunctions, final BooleanBigList isValueSet,
				final BooleanBigList isMissingValue, final BooleanBigList isMaxValue, final BooleanBigList isMinValue,
				final DoubleBigList values, final ObjectBigList<boolean[]> isChildValueSet,
				final ObjectBigList<boolean[]> isMissingChildValue, final ObjectBigList<boolean[]> isMaxChildValue,
				final ObjectBigList<boolean[]> isMinChildValue, final ObjectBigList<double[]> childValues,
				final ObjectType<?> ofType) {
			super(ofType, childFunctions);
			this.isValueSet = new BooleanBigArrayBigList(isValueSet);
			this.isMissingValue = new BooleanBigArrayBigList(isMissingValue);
			this.isMaxValue = new BooleanBigArrayBigList(isMaxValue);
			this.isMinValue = new BooleanBigArrayBigList(isMinValue);
			this.values = new DoubleBigArrayBigList(values);

			this.isChildValueSet = new ObjectBigArrayBigList<>(isChildValueSet);
			this.isMissingChildValue = new ObjectBigArrayBigList<>(isMissingChildValue);
			this.isMaxChildValue = new ObjectBigArrayBigList<>(isMaxChildValue);
			this.isMinChildValue = new ObjectBigArrayBigList<>(isMinChildValue);
			this.childValues = new ObjectBigArrayBigList<>(childValues);
		}

		@Override
		public LargeSimilarityValueStorage copy() {
			if (isComposite)
				return new LargeSimilarityValueStorage(this.childFunctions, this.isValueSet, this.isMissingValue,
						this.isMaxValue, this.isMinValue, this.values, this.isChildValueSet, this.isMissingChildValue,
						this.isMaxChildValue, this.isMinChildValue, this.childValues, this.ofType);
			else
				return new LargeSimilarityValueStorage(this.isValueSet, this.isMissingValue, this.isMaxValue,
						this.isMinValue, this.values, this.ofType);
		}

		@Override
		public ObjectBigList<ISimilarityValue> values() {
			return values(new LongBigArrayBigList(Iterables.range(this.size()).iterator()));
		}

		protected ObjectBigList<ISimilarityValue> values(final LongBigList indices) {
			final ObjectBigList<ISimilarityValue> values = new ObjectBigArrayBigList<>(indices.size64());
			for (long idx = 0; idx < indices.size64(); idx++)
				values.add(this.get(indices.getLong(idx)));
			return values;
		}

		@Override
		public boolean isMissing(final long i) {
			return this.isMissingValue.getBoolean(i);
		}

		@Override
		public boolean[] isMissing(long[] is) {
			final boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isMissing(is[p]);
			return r;
		}

		@Override
		public boolean isMissing(int i) {
			return this.isMissing((long) i);
		}

		@Override
		public boolean[] isMissing(int[] is) {
			return this.isMissing(Arrays.stream(is).mapToLong(i -> i).toArray());
		}

		public void setMissing(final long i, final boolean isMissing) {
			this.isMissingValue.set(i, isMissing);
		}

		@Override
		public LongBigList increasingByValue() {
			final LongBigList indices = new LongBigArrayBigList();
			for (long i = 0; i < this.size(); i++)
				indices.add(i);
			final ObjectBigList<ISimilarityValue> values = this.values();

			final LongComparator cmp = (o1, o2) -> {
				final ISimilarityValue s1 = values.get(o1);
				final ISimilarityValue s2 = values.get(o2);
				return s1.compareTo(s2);
			};
			final BigSwapper swapper = (i, j) -> {
				long tmp = indices.getLong(i);
				indices.set(i, indices.getLong(j));
				indices.set(j, tmp);

				final ISimilarityValue val = values.get(i);
				values.set(i, values.get(j));
				values.set(j, val);
			};
			BigArrays.mergeSort(0, indices.size64(), cmp, swapper);
			return indices;
		}

		@Override
		public LongBigList decreasingByValue() {
			final LongBigList indices = new LongBigArrayBigList();
			for (long i = 0; i < this.size(); i++)
				indices.add(i);
			final ObjectBigList<ISimilarityValue> values = this.values();

			final LongComparator cmp = (o1, o2) -> {
				final ISimilarityValue s1 = values.get(o1);
				final ISimilarityValue s2 = values.get(o2);
				return s2.compareTo(s1);
			};
			final BigSwapper swapper = (i, j) -> {
				long tmp = indices.getLong(i);
				indices.set(i, indices.getLong(j));
				indices.set(j, tmp);

				final ISimilarityValue val = values.get(i);
				values.set(i, values.get(j));
				values.set(j, val);
			};
			BigArrays.mergeSort(0, indices.size64(), cmp, swapper);
			return indices;
		}

		@Override
		public boolean isSet(final long i) {
			return this.isValueSet.getBoolean(i);
		}

		@Override
		public boolean[] isSet(long[] is) {
			final boolean[] r = new boolean[is.length];
			for (int p = 0; p < is.length; p++)
				r[p] = isSet(is[p]);
			return r;
		}

		@Override
		public boolean isSet(int i) {
			return this.isSet((long) i);
		}

		@Override
		public boolean[] isSet(int[] is) {
			return this.isSet(Arrays.stream(is).mapToLong(i -> i).toArray());
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder("[");
			for (long i = 0; i < Math.min(this.values.size64(), 100); i++) {
				if (!this.isValueSet.getBoolean(i))
					sb.append("null");
				else if (this.isMissingValue.getBoolean(i))
					sb.append("NA");
				else
					sb.append(Double.toString(this.values.getDouble(i)));

				if (i < this.values.size64() - 1)
					if (i == 99)
						sb.append(", ...");
					else
						sb.append(", ");
			}

			sb.append("]");
			return sb.toString();
		}

		@Override
		public long size() {
			return this.values.size64();
		}

		@Override
		public ISimilarityValue get(final long i) {
			if (isComposite) {
				if (i == -1 || this.isMissingValue.getBoolean(i))
					return missingValuePlaceholder();
				else if (this.isMaxValue.getBoolean(i))
					return maxValue();
				else if (this.isMinValue.getBoolean(i))
					return minValue();
				else if (!this.isValueSet.getBoolean(i))
					return null;

				final ISimpleSimilarityValue[] values = new ISimpleSimilarityValue[childFunctions.length];
				for (int f = 0; f < this.childFunctions.length; f++) {
					values[f] = getChildValue(i, f);
				}

				return ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this).value(values, ofType);
			} else {
				if (i == -1 || this.isMissingValue.getBoolean(i))
					return missingValuePlaceholder();
				else if (this.isMaxValue.getBoolean(i))
					return maxValue();
				else if (this.isMinValue.getBoolean(i))
					return minValue();
				else if (!this.isValueSet.getBoolean(i))
					return null;
				return ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(this.values.getDouble(i),
						ofType);
			}
		}

		private ISimpleSimilarityValue getChildValue(final long i, final int f) {
			if (this.isMissingChildValue.get(i)[f])
				return this.childFunctions[f].missingValuePlaceholder();
			if (this.isMaxChildValue.get(i)[f])
				return this.childFunctions[f].maxValue();
			if (this.isMinChildValue.get(i)[f])
				return this.childFunctions[f].minValue();
			else if (!this.isValueSet.getBoolean(i))
				return null;
			return this.childFunctions[f].value(this.childValues.get(i)[f], ofType);
		}

		@Override
		public ISimilarityValue get(int i) {
			return this.get((long) i);
		}

		@Override
		public ISimilarityValue[] get(int[] is) {
			return this.get(Arrays.stream(is).mapToLong(i -> (long) i).toArray());
		}

		@Override
		public ISimilarityValue mean(long[] indices) throws InterruptedException, SimilarityValuesException {
			return mean(LongArrayList.wrap(indices));
		}

		@Override
		public ISimilarityValue mean(LongBigList indices) throws InterruptedException, SimilarityValuesException {
			return mean((Iterable) indices);
		}

		protected ISimilarityValue mean(final Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			if (isComposite)
				return this.meanComposite(indices);
			else
				return this.meanNoncomposite(indices);
		}

		private ISimpleSimilarityValue meanNoncomposite(final Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			final int MIN_BATCH_SIZE = 50000;

			double sum = 0.0;
			int count = 0;

			if (!Iterables.isAtLeastOfSize(indices, MIN_BATCH_SIZE)) {
				final Pair<Double, Integer> result = new MeanNoncompositeOp()
						.perform(new LongArrayList(indices.iterator()).toLongArray());
				sum = result.getFirst();
				count = result.getSecond();
			} else {
				final Iterable<long[]> batches = Iterables.batchifyLongs(indices,
						new BatchCount(getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<Pair<Double, Integer>>> futures = new MyParallel<Pair<Double, Integer>>(
						getPrivateSimilarityFunctionThreadPool()).For(batches, new MeanNoncompositeOp());

				for (final Future<Pair<Double, Integer>> f : futures)
					try {
						final Pair<Double, Integer> ds = f.get();
						sum += ds.getFirst();
						count += ds.getSecond();
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
			}

			return ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(sum /= count, ofType);

		}

		private ICompositeSimilarityValue meanComposite(final Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			final int MIN_BATCH_SIZE = 50000;

			final double[] sum;
			final int[] counts;

			if (!Iterables.isAtLeastOfSize(indices, MIN_BATCH_SIZE)) {
				final Pair<double[], int[]> result = new MeanCompositeOp()
						.perform(new LongArrayList(indices.iterator()).toLongArray());
				sum = result.getFirst();
				counts = result.getSecond();
			} else {
				sum = new double[childFunctions.length];
				counts = new int[childFunctions.length];
				final Iterable<long[]> batches = Iterables.batchifyLongs(indices,
						new BatchCount(getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<Pair<double[], int[]>>> futures = new MyParallel<Pair<double[], int[]>>(
						getPrivateSimilarityFunctionThreadPool()).For(batches, new MeanCompositeOp());

				for (final Future<Pair<double[], int[]>> f : futures)
					try {
						final Pair<double[], int[]> ds = f.get();
						final double[] batchSum = ds.getFirst();
						final int[] batchCount = ds.getSecond();
						for (int i = 0; i < batchSum.length; i++) {
							sum[i] += batchSum[i];
							counts[i] += batchCount[i];
						}
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
			}

			for (int i = 0; i < sum.length; i++)
				sum[i] /= counts[i];

			final ISimpleSimilarityValue[] childVals = new ISimpleSimilarityValue[sum.length];
			for (int i = 0; i < sum.length; i++)
				childVals[i] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
						.getSimilarityFunctions()[i].value(sum[i], ofType);
			return ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this).value(childVals, ofType);
		}

		@Override
		public ISimilarityValue median() throws InterruptedException, SimilarityValuesException {
			return this.median(Iterables.range(this.size()));
		}

		@Override
		public ISimilarityValue median(long[] indices) throws InterruptedException, SimilarityValuesException {
			return median(LongArrayList.wrap(indices));
		}

		@Override
		public ISimilarityValue median(LongBigList indices) throws InterruptedException, SimilarityValuesException {
			return median((Iterable) indices);
		}

		protected ISimilarityValue median(Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			if (isComposite)
				return medianComposite(indices);
			else
				return medianNoncomposite(indices);
		}

		private ISimpleSimilarityValue medianNoncomposite(Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			final double[] sortedValues = LongStream.range(0, childValues.size64()).filter(idx -> {
				return !isMissingValue.getBoolean(idx) && isValueSet.getBoolean(idx);
			}).mapToDouble(idx -> values.getDouble(idx)).sorted().toArray();

			final double median = sortedValues.length % 2 == 1 ? sortedValues[sortedValues.length / 2]
					: (sortedValues[sortedValues.length / 2 - 1] + sortedValues[sortedValues.length / 2]) / 2.0;

			return ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(median, ofType);
		}

		private ICompositeSimilarityValue medianComposite(Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			final Iterable<Iterable<Integer>> batches = Iterables.batchify(Iterables.range(childFunctions.length),
					new BatchCount(getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize()));

			final List<Future<List<Double>>> futures = new MyParallel<List<Double>>(
					getPrivateSimilarityFunctionThreadPool()).For(batches, new MedianCompositeOp());
			final double[] median = new double[childFunctions.length];
			int idx = 0;
			for (final Future<List<Double>> f : futures)
				try {
					final List<Double> medians = f.get();
					for (Double d : medians)
						median[idx++] = d;
				} catch (final ExecutionException e) {
					if (e.getCause() instanceof InterruptedException)
						throw (InterruptedException) e.getCause();
					if (e.getCause() != null)
						throw new SimilarityValuesException(e.getCause());
					throw new SimilarityValuesException(e);
				}
			final ISimpleSimilarityValue[] childVals = new ISimpleSimilarityValue[median.length];
			for (int i = 0; i < median.length; i++)
				childVals[i] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
						.getSimilarityFunctions()[i].value(median[i], ofType);
			return ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this).value(childVals, ofType);
		}

		@Override
		public ISimilarityValue set(long i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.set(new long[] { i }, new ISimilarityValue[] { value })[0];
		}

		@Override
		public ISimilarityValue[] set(long[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			if (isComposite)
				return this.setComposite(is, values);
			else
				return this.setNoncomposite(is, values);
		}

		@Override
		public ISimilarityValue set(int i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.set((long) i, value);
		}

		@Override
		public ISimilarityValue[] set(int[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			if (isComposite)
				return this.setComposite(Arrays.stream(is).mapToLong(i -> i).toArray(), values);
			else
				return this.setNoncomposite(Arrays.stream(is).mapToLong(i -> i).toArray(), values);
		}

		protected final ISimilarityValue[] set(final long[] is, double[] values, boolean[] isMissing, boolean[] isMax,
				boolean[] isMin) {
			final ISimilarityValue[] old = new ISimilarityValue[is.length];
			for (int p = 0; p < is.length; p++) {
				final long i = is[p];
				final boolean wasSet = this.isValueSet.getBoolean(i);
				if (wasSet)
					old[p] = this.get(i);
				this.values.set(i, values[p]);
				this.isValueSet.set(i, true);
				this.isMissingValue.set(i, isMissing[p]);
				this.isMaxValue.set(i, isMax[p]);
				this.isMinValue.set(i, isMin[p]);
			}
			return old;
		}

		private ISimilarityValue[] setNoncomposite(final long[] is, final ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			try {
				final boolean[] isMissing = new boolean[is.length], isMax = new boolean[is.length],
						isMin = new boolean[is.length];
				final double[] vs = new double[is.length];
				for (int p = 0; p < is.length; p++) {
					isMissing[p] = values[p] instanceof IMissingSimilarityValue;
					isMax[p] = values[p] instanceof IMaximalSimilarityValue;
					isMin[p] = values[p] instanceof IMinimalSimilarityValue;
					vs[p] = values[p].get();
				}
				return this.set(is, vs, isMissing, isMax, isMin);
			} catch (SimilarityCalculationException e) {
				throw new SimilarityValuesException(e);
			}
		}

		private ISimilarityValue[] setComposite(final long[] is, final ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			final ISimilarityValue[] old = this.setNoncomposite(is, values);
			// set child values
			for (int p = 0; p < is.length; p++) {
				ICompositeSimilarityValue v = (ICompositeSimilarityValue) values[p];
				final ISimpleSimilarityFunction[] childFunctions = v.getChildFunctions();
				for (int fIdx = 0; fIdx < childFunctions.length; fIdx++) {
					this.setChildValue(is[p], fIdx, v.getChildValues()[fIdx]);
				}
			}
			return old;
		}

		private ISimpleSimilarityValue setChildValue(final long i, final int fIdx,
				final ISimpleSimilarityValue childValue) throws SimilarityValuesException {
			try {
				ISimpleSimilarityValue old = getChildValue(i, fIdx);

				this.isChildValueSet.get(i)[fIdx] = true;
				this.isMissingChildValue.get(i)[fIdx] = childValue instanceof IMissingSimilarityValue;
				this.isMaxChildValue.get(i)[fIdx] = childValue instanceof IMaximalSimilarityValue;
				this.isMinChildValue.get(i)[fIdx] = childValue instanceof IMinimalSimilarityValue;
				this.childValues.get(i)[fIdx] = childValue.get();

				return old;
			} catch (final SimilarityCalculationException e) {
				throw new SimilarityValuesException(e);
			}
		}

		protected ISimilarityValue sum(final Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			if (isComposite)
				return sumComposite(indices);
			else
				return sumNoncomposite(indices);
		}

		private ICompositeSimilarityValue sumComposite(final Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			final int MIN_BATCH_SIZE = 50000;

			final double[] sum;

			if (!Iterables.isAtLeastOfSize(indices, MIN_BATCH_SIZE)) {
				sum = new SumCompositeOp().perform(new LongArrayList(indices.iterator()).toLongArray());
			} else {
				sum = new double[childFunctions.length];

				final Iterable<long[]> batches = Iterables.batchifyLongs(indices,
						new BatchCount(getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<double[]>> futures = new MyParallel<double[]>(
						getPrivateSimilarityFunctionThreadPool()).For(batches, new SumCompositeOp());

				for (final Future<double[]> f : futures)
					try {
						final double[] ds = f.get();
						for (int i = 0; i < ds.length; i++)
							sum[i] += ds[i];
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
			}

			final ISimpleSimilarityValue[] childVals = new ISimpleSimilarityValue[sum.length];
			for (int i = 0; i < sum.length; i++)
				childVals[i] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
						.getSimilarityFunctions()[i].value(sum[i], ofType);
			return ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this).value(childVals, this.ofType);
		}

		private ISimpleSimilarityValue sumNoncomposite(final Iterable<Long> indices)
				throws InterruptedException, SimilarityValuesException {
			final int MIN_BATCH_SIZE = 50000;

			double sum = 0.0;

			if (!Iterables.isAtLeastOfSize(indices, MIN_BATCH_SIZE)) {
				sum = new SumNoncompositeOp().perform(new LongArrayList(indices.iterator()).toLongArray());
			} else {
				final Iterable<long[]> batches = Iterables.batchifyLongs(indices,
						new BatchCount(getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<Double>> futures = new MyParallel<Double>(getPrivateSimilarityFunctionThreadPool())
						.For(batches, new SumNoncompositeOp());

				for (final Future<Double> f : futures)
					try {
						sum += f.get();
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
			}

			return ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(sum, ofType);
		}

		@Override
		public ISmallSimilarityValueStorage subset(long[] indices) {
			return new SmallSubsetOfLargeStorage(this, indices);
		}

		@Override
		public ILargeSimilarityValueStorage subset(final LongBigList indices) {
			return new LargeSimilarityValueSubset(this, indices);
		}

		@Override
		public ISimilarityValue mean() throws InterruptedException, SimilarityValuesException {
			return this.mean(Iterables.range(this.size()));
		}

		@Override
		public ISimilarityValue sum() throws InterruptedException, SimilarityValuesException {
			return this.sum(Iterables.range(this.size()));
		}

		@Override
		public ISimilarityValue sum(long[] indices) throws InterruptedException, SimilarityValuesException {
			return sum(LongArrayList.wrap(indices));
		}

		@Override
		public ISimilarityValue sum(LongBigList indices) throws InterruptedException, SimilarityValuesException {
			return sum((Iterable) indices);
		}

		@Override
		protected double[] getRawChildValues(long i) {
			return this.childValues.get(i);
		}

		@Override
		protected double getRawValue(long i) {
			return this.values.getDouble(i);
		}

		@Override
		protected boolean isChildValueSet(long i, int sf) {
			return this.isChildValueSet.get(i)[sf];
		}

		@Override
		protected boolean isMissingChildValue(long i, int sf) {
			return this.isMissingChildValue.get(i)[sf];
		}

		@Override
		public ISimilarityValue[] get(long[] is) {
			final ISimilarityValue[] r = new ISimilarityValue[is.length];
			if (isComposite) {
				for (int p = 0; p < is.length; p++) {
					final long i = is[p];
					if (this.isMissingValue.getBoolean(i))
						r[p] = missingValuePlaceholder();
					else if (!this.isValueSet.getBoolean(i))
						r[p] = null;

					final ISimpleSimilarityValue[] values = new ISimpleSimilarityValue[childFunctions.length];
					int f = 0;
					for (final ISimpleSimilarityFunction simFunc : ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
							.getSimilarityFunctions()) {
						final ISimpleSimilarityValue v = simFunc.value(this.childValues.get(i)[f], ofType);
						values[f] = v;
						f++;
					}

					r[p] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this).value(values, ofType);
				}
			} else {
				for (int p = 0; p < is.length; p++) {
					final long i = is[p];
					if (this.isMissingValue.getBoolean(i))
						r[p] = missingValuePlaceholder();
					r[p] = ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(this.values.getDouble(i),
							ofType);
				}
			}

			return r;
		}

		@Override
		public Iterator<ISimilarityValue> iterator() {
			return new Iterator<ISimilarityValue>() {
				private long curr = 0;

				@Override
				public boolean hasNext() {
					return this.curr < LargeSimilarityValueStorage.this.size();
				}

				@Override
				public ISimilarityValue next() {
					return LargeSimilarityValueStorage.this.get(this.curr++);
				}
			};
		}
	}

	public class SmallSimilarityValueStorage extends AbstractSimilarityValueStorage
			implements ISmallSimilarityValueStorage {

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class MedianCompositeOp implements Operation<Iterable<Integer>, List<Double>> {
			@Override
			public List<Double> perform(final Iterable<Integer> childFuncIdxs) {
				final List<Double> result = new ArrayList<>();
				for (Integer childFuncIdx : childFuncIdxs) {
					final double[] sortedValues = IntStream.range(0, childValues.length).filter(idx -> {
						return !isMissingChildValue[idx][childFuncIdx] && isChildValueSet[idx][childFuncIdx];
					}).mapToDouble(idx -> childValues[idx][childFuncIdx]).sorted().toArray();

					final double median = sortedValues.length % 2 == 1 ? sortedValues[sortedValues.length / 2]
							: (sortedValues[sortedValues.length / 2 - 1] + sortedValues[sortedValues.length / 2]) / 2.0;

					result.add(median);
				}

				return result;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class MeanNoncompositeOp implements Operation<int[], Pair<Double, Integer>> {
			@Override
			public Pair<Double, Integer> perform(final int[] iterable) {
				int batchCount = 0;
				double batchSum = 0.0;

				for (int i : iterable) {
					final double val = getRawValue(i);
					if (!isMissing(i) && isSet(i) && Double.isFinite(val)) {
						batchSum += val;
						batchCount++;
					}
				}

				return Pair.of(batchSum, batchCount);
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class MeanCompositeOp implements Operation<int[], Pair<double[], int[]>> {
			@Override
			public Pair<double[], int[]> perform(final int[] iterable) {
				final int[] counts = new int[childFunctions.length];
				final double[] sum = new double[childFunctions.length];

				for (int child : iterable) {
					final double[] vals = getRawChildValues(child);
					for (int cSF = 0; cSF < vals.length; cSF++)
						if (!isMissingChildValue(child, cSF) && isChildValueSet(child, cSF)
								&& Double.isFinite(vals[cSF])) {
							sum[cSF] += vals[cSF];
							counts[cSF]++;
						}
				}

				return Pair.of(sum, counts);
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class SumNoncompositeOp implements Operation<int[], Double> {
			@Override
			public Double perform(final int[] iterable) {
				double batchSum = 0.0;

				for (int i : iterable) {
					final double val = getRawValue(i);
					if (!isMissing(i) && isSet(i) && Double.isFinite(val))
						batchSum += val;
				}

				return batchSum;
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Mar 15, 2019
		 *
		 */
		private final class SumCompositeOp implements Operation<int[], Pair<double[], int[]>> {

			@Override
			public Pair<double[], int[]> perform(final int[] iterable) {
				final double[] result = new double[childFunctions.length];
				final int[] counts = new int[childFunctions.length];

				for (int child : iterable) {
					final double[] vals = getRawChildValues(child);
					for (int cSF = 0; cSF < vals.length; cSF++)
						if (!isMissingChildValue(child, cSF) && isChildValueSet(child, cSF)
								&& Double.isFinite(vals[cSF])) {
							result[cSF] += vals[cSF];
							counts[cSF]++;
						}
				}

				return Pair.getPair(result, counts);
			}
		}

		protected final boolean[] isValueSet, isMissingValue, isMaxValue, isMinValue;
		protected final double[] values;
		protected final int[] cardinalities;

		// only used if this storage is composite
		protected final boolean[][] isChildValueSet, isMissingChildValue, isMaxChildValue, isMinChildValue;
		protected final double[][] childValues;

		/**
		 * Constructs a non-composite storage.
		 * 
		 * @param size
		 * @param ofType
		 */
		SmallSimilarityValueStorage(final int size, final ObjectType<?> ofType) {
			super(ofType);
			this.isValueSet = new boolean[size];
			this.isMissingValue = new boolean[size];
			this.isMaxValue = new boolean[size];
			this.isMinValue = new boolean[size];
			this.cardinalities = new int[size];
			this.values = new double[size];
			for (int i = 0; i < this.values.length; i++)
				this.values[i] = Double.NaN;

			this.isChildValueSet = null;
			this.isMissingChildValue = null;
			this.childValues = null;
			this.isMaxChildValue = null;
			this.isMinChildValue = null;
		}

		/**
		 * A copy constructor for a non-composite storage.
		 * 
		 * @param isValueSet
		 * @param indicesValueSet
		 * @param isMissingValue
		 * @param values
		 * @param ofType
		 */
		SmallSimilarityValueStorage(final SmallSimilarityValueStorage other) {
			super(other.ofType, other.childFunctions);
			this.isValueSet = Arrays.copyOf(other.isValueSet, other.isValueSet.length);
			this.isMissingValue = Arrays.copyOf(other.isMissingValue, other.isMissingValue.length);
			this.isMaxValue = Arrays.copyOf(other.isMaxValue, other.isMaxValue.length);
			this.isMinValue = Arrays.copyOf(other.isMinValue, other.isMinValue.length);
			this.cardinalities = Arrays.copyOf(other.cardinalities, other.cardinalities.length);
			this.values = Arrays.copyOf(other.values, other.values.length);

			if (other.isComposite) {
				this.isChildValueSet = new boolean[other.isChildValueSet.length][];
				this.isMissingChildValue = new boolean[other.isMissingChildValue.length][];
				this.isMaxChildValue = new boolean[other.isMaxChildValue.length][];
				this.isMinChildValue = new boolean[other.isMinChildValue.length][];
				this.childValues = new double[other.childValues.length][];

				for (int c = 0; c < isChildValueSet.length; c++) {
					this.isChildValueSet[c] = Arrays.copyOf(isChildValueSet[c], isChildValueSet[c].length);
					this.isMissingChildValue[c] = Arrays.copyOf(isMissingChildValue[c], isMissingChildValue[c].length);
					this.isMaxChildValue[c] = Arrays.copyOf(isMaxChildValue[c], isMaxChildValue[c].length);
					this.isMinChildValue[c] = Arrays.copyOf(isMinChildValue[c], isMinChildValue[c].length);
					this.childValues[c] = Arrays.copyOf(childValues[c], childValues[c].length);
				}
			} else {
				this.isChildValueSet = null;
				this.isMissingChildValue = null;
				this.childValues = null;
				this.isMaxChildValue = null;
				this.isMinChildValue = null;
			}
		}

		/**
		 * Constructs a composite storage.
		 * 
		 * @param size
		 * @param ofType
		 * @param childFunctions
		 */
		SmallSimilarityValueStorage(final int size, final ObjectType<?> ofType,
				final ISimpleSimilarityFunction[] childFunctions) {
			super(ofType, childFunctions);
			this.isValueSet = new boolean[size];
			this.isMissingValue = new boolean[size];
			this.isMaxValue = new boolean[size];
			this.isMinValue = new boolean[size];
			this.cardinalities = new int[size];
			this.values = new double[size];
			for (int i = 0; i < this.values.length; i++)
				this.values[i] = Double.NaN;

			this.isChildValueSet = new boolean[size][childFunctions.length];
			this.isMissingChildValue = new boolean[size][childFunctions.length];
			this.isMaxChildValue = new boolean[size][childFunctions.length];
			this.isMinChildValue = new boolean[size][childFunctions.length];
			this.childValues = new double[size][childFunctions.length];
			for (int i = 0; i < this.childValues.length; i++)
				for (int j = 0; j < this.childValues[i].length; j++)
					this.childValues[i][j] = Double.NaN;
		}

		@Override
		public SmallSimilarityValueStorage copy() {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return new SmallSimilarityValueStorage(this);
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue[] values() {
			return values(IntStream.range(0, (int) this.size()).toArray());
		}

		protected ISimilarityValue[] values(final int[] indices) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				final ISimilarityValue[] values = new ISimilarityValue[indices.length];
				for (int c = 0; c < indices.length; c++)
					values[c] = this.get(indices[c]);
				return values;
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public boolean isMissing(final long i) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.isMissingValue[Math.toIntExact(i)];
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public boolean[] isMissing(long[] is) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				boolean[] r = new boolean[is.length];
				for (int p = 0; p < is.length; p++)
					r[p] = this.isMissingValue[Math.toIntExact(is[p])];
				return r;
			} finally {
				readLock.unlock();
			}
		}

		public boolean isMissing(final int i) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.isMissingValue[i];
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public boolean[] isMissing(int[] is) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				boolean[] r = new boolean[is.length];
				for (int p = 0; p < is.length; p++)
					r[p] = this.isMissingValue[is[p]];
				return r;
			} finally {
				readLock.unlock();
			}
		}

		protected final ISimilarityValue[] set(final int[] is, double[] values, boolean[] isMissing, boolean[] isMax,
				boolean[] isMin, final int[] cardinalities) {
			final Lock writeLock = rwLock.writeLock();
			writeLock.lock();
			try {
				final ISimilarityValue[] old = new ISimilarityValue[is.length];
				for (int p = 0; p < is.length; p++) {
					final int i = is[p];
					final boolean wasSet = this.isValueSet[i];
					if (wasSet)
						old[p] = this.get(i);
					this.values[i] = values[p];
					this.isValueSet[i] = true;
					this.isMissingValue[i] = isMissing[p];
					this.isMaxValue[i] = isMax[p];
					this.isMinValue[i] = isMin[p];
					this.cardinalities[i] = cardinalities[p];
				}
				return old;
			} finally {
				writeLock.unlock();
			}
		}

		@Override
		public int[] increasingByValue() {
			final int[] indices = new int[(int) this.size()];
			for (int i = 0; i < indices.length; i++)
				indices[i] = i;
			final ISimilarityValue[] values = this.values();
			final IntComparator cmp = (i, j) -> {
				final ISimilarityValue s1 = get(i);
				final ISimilarityValue s2 = get(j);
				return s1.compareTo(s2);
			};
			final Swapper swapper = (i, j) -> {
				int tmp = indices[i];
				indices[i] = indices[j];
				indices[j] = tmp;

				final ISimilarityValue val = values[i];
				values[i] = values[j];
				values[j] = val;
			};
			it.unimi.dsi.fastutil.Arrays.mergeSort(0, indices.length, cmp, swapper);
			return indices;
		}

		@Override
		public int[] decreasingByValue() {
			final int[] indices = new int[(int) this.size()];
			for (int i = 0; i < indices.length; i++)
				indices[i] = i;
			final ISimilarityValue[] values = this.values();

			final IntComparator cmp = (i, j) -> {
				final ISimilarityValue s1 = values[i];
				final ISimilarityValue s2 = values[j];
				return s2.compareTo(s1);
			};
			final Swapper swapper = (i, j) -> {
				int tmp = indices[i];
				indices[i] = indices[j];
				indices[j] = tmp;

				final ISimilarityValue val = values[i];
				values[i] = values[j];
				values[j] = val;
			};
			it.unimi.dsi.fastutil.Arrays.mergeSort(0, indices.length, cmp, swapper);
			return indices;
		}

		@Override
		public boolean isSet(final long i) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.isValueSet[Math.toIntExact(i)];
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public boolean[] isSet(long[] is) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				boolean[] r = new boolean[is.length];
				for (int p = 0; p < is.length; p++)
					r[p] = this.isValueSet[Math.toIntExact(is[p])];
				return r;
			} finally {
				readLock.unlock();
			}
		}

		public boolean isSet(final int i) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.isValueSet[i];
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public boolean[] isSet(int[] is) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				boolean[] r = new boolean[is.length];
				for (int p = 0; p < is.length; p++)
					r[p] = this.isValueSet[is[p]];
				return r;
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue mean() throws InterruptedException, SimilarityValuesException {
			return this.mean(IntStream.range(0, (int) this.size()).toArray());
		}

		@Override
		public ISimilarityValue sum() throws InterruptedException, SimilarityValuesException {
			return this.sum(IntStream.range(0, (int) this.size()).toArray());
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder("[");
			for (int i = 0; i < Math.min(this.size(), 100); i++) {
				if (!this.isValueSet[i])
					sb.append("NOT_SET");
				else if (this.isMissingValue[i])
					sb.append("MISSING");
				else
					sb.append(Double.toString(this.values[i]));

				if (i < this.size() - 1) {
					if (i == 99)
						sb.append(", ...");
					else
						sb.append(", ");
				}
			}

			sb.append("]");
			return sb.toString();
		}

		@Override
		public long size() {
			return this.values.length;
		}

		@Override
		public ISimilarityValue get(final long idx) {
			return get(Math.toIntExact(idx));
		}

		@Override
		public ISimilarityValue[] get(long[] is) {
			return get(Arrays.stream(is).mapToInt(i -> Math.toIntExact(i)).toArray());
		}

		public ISimilarityValue get(final int i) {
			return this.get(new int[] { i })[0];
		}

		@Override
		public ISimilarityValue[] get(int[] is) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				final ISimilarityValue[] r = new ISimilarityValue[is.length];
				if (isComposite) {
					for (int p = 0; p < is.length; p++) {
						final int i = is[p];
						if (i == -1 || this.isMissingValue[i] || !this.isValueSet[i])
							r[p] = missingValuePlaceholder();
						else if (this.isMaxValue[i])
							r[p] = maxValue();
						else if (this.isMinValue[i])
							r[p] = minValue();
						else {
							final ISimpleSimilarityValue[] values = new ISimpleSimilarityValue[childFunctions.length];
							int f = 0;
							for (final ISimpleSimilarityFunction simFunc : this.childFunctions) {
								if (i == -1 || this.isMissingChildValue[i][f])
									values[f] = simFunc.missingValuePlaceholder();
								else if (this.isMaxChildValue[i][f])
									values[f] = simFunc.maxValue();
								else if (this.isMinChildValue[i][f])
									values[f] = simFunc.minValue();
								else if (!this.isChildValueSet[i][f])
									values[f] = null;
								else
									values[f] = simFunc.value(this.childValues[i][f], ofType);
								f++;
							}

							r[p] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this).value(values,
									ofType, this.cardinalities[i]);
						}
					}
				} else {
					for (int p = 0; p < is.length; p++) {
						final int i = is[p];
						if (i == -1 || this.isMissingValue[i] || !this.isValueSet[i])
							r[p] = missingValuePlaceholder();
						else if (this.isMaxValue[i])
							r[p] = maxValue();
						else if (this.isMinValue[i])
							r[p] = minValue();
						else
							r[p] = ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(this.values[i],
									ofType);
					}
				}
				return r;
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue median() throws InterruptedException, SimilarityValuesException {
			return this.median(IntStream.range(0, (int) this.size()).toArray());
		}

		@Override
		public ISimilarityValue median(int[] indices) throws InterruptedException, SimilarityValuesException {
			throw new UnsupportedOperationException();
		}

		protected ISimilarityValue median(Iterable<Integer> indices)
				throws InterruptedException, SimilarityValuesException {
			if (isComposite)
				return medianComposite(indices);
			else
				return medianNoncomposite(indices);
		}

		private ISimpleSimilarityValue medianNoncomposite(Iterable<Integer> indices)
				throws InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				final double[] sortedValues = IntStream.range(0, values.length).filter(idx -> {
					return !isMissingValue[idx] && isValueSet[idx];
				}).mapToDouble(idx -> values[idx]).sorted().toArray();

				final double median = sortedValues.length % 2 == 1 ? sortedValues[sortedValues.length / 2]
						: (sortedValues[sortedValues.length / 2 - 1] + sortedValues[sortedValues.length / 2]) / 2.0;

				return ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(median, ofType);
			} finally {
				readLock.unlock();
			}
		}

		private ICompositeSimilarityValue medianComposite(Iterable<Integer> indices)
				throws InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				final Iterable<Iterable<Integer>> batches = Iterables.batchify(Iterables.range(childFunctions.length),
						new BatchCount(getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize()));

				final List<Future<List<Double>>> futures = new MyParallel<List<Double>>(
						getPrivateSimilarityFunctionThreadPool()).For(batches, new MedianCompositeOp());
				final double[] median = new double[childFunctions.length];
				int idx = 0;
				for (final Future<List<Double>> f : futures)
					try {
						final List<Double> medians = f.get();
						for (Double d : medians)
							median[idx++] = d;
					} catch (final ExecutionException e) {
						if (e.getCause() instanceof InterruptedException)
							throw (InterruptedException) e.getCause();
						if (e.getCause() != null)
							throw new SimilarityValuesException(e.getCause());
						throw new SimilarityValuesException(e);
					}
				final ISimpleSimilarityValue[] childVals = new ISimpleSimilarityValue[median.length];
				for (int i = 0; i < median.length; i++)
					childVals[i] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
							.getSimilarityFunctions()[i].value(median[i], ofType);
				return ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this).value(childVals, ofType);
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue set(final long i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.set(Math.toIntExact(i), value);
		}

		@Override
		public ISimilarityValue[] set(long[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.set(Arrays.stream(is).mapToInt(i -> Math.toIntExact(i)).toArray(), values);
		}

		@Override
		public ISimilarityValue set(int i, ISimilarityValue value)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			return this.set(new int[] { i }, new ISimilarityValue[] { value })[0];
		}

		@Override
		public ISimilarityValue[] set(int[] is, ISimilarityValue[] values)
				throws SimilarityValuesException, IncompatibleSimilarityValueException {
			if (isComposite)
				return this.setComposite(is, values);
			else
				return this.setNoncomposite(is, values);
		}

		private ISimilarityValue[] setNoncomposite(final int[] is, final ISimilarityValue[] values)
				throws SimilarityValuesException {
			try {
				final boolean[] isMissing = new boolean[is.length], isMax = new boolean[is.length],
						isMin = new boolean[is.length];
				final int[] cardinalities = new int[is.length];
				final double[] vs = new double[is.length];
				for (int p = 0; p < is.length; p++) {
					isMissing[p] = values[p] instanceof IMissingSimilarityValue;
					isMax[p] = values[p] instanceof IMaximalSimilarityValue;
					isMin[p] = values[p] instanceof IMinimalSimilarityValue;
					vs[p] = values[p].get();
					cardinalities[p] = 1;
				}
				return this.set(is, vs, isMissing, isMax, isMin, cardinalities);
			} catch (final SimilarityCalculationException e) {
				throw new SimilarityValuesException(e);
			}
		}

		private ISimilarityValue[] setComposite(final int[] is, final ISimilarityValue[] values)
				throws SimilarityValuesException {
			final ISimilarityValue[] old = this.setNoncomposite(is, values);
			final ISimpleSimilarityValue[][] childValues = new ISimpleSimilarityValue[values.length][];
			for (int p = 0; p < is.length; p++) {
				final int i = is[p];
				ICompositeSimilarityValue value = (ICompositeSimilarityValue) values[p];
				if (value instanceof SpecialCompositeSimilarityValue)
					childValues[p] = null;
				else
					childValues[p] = value.getChildValues();
				this.cardinalities[p] = value.cardinality();
			}
			// set child values
			this.setChildValue(is, childValues);
			return old;
		}

		private ISimpleSimilarityValue[][] setChildValue(final int[] is, final ISimpleSimilarityValue[][] childValue)
				throws SimilarityValuesException {
			final Lock writeLock = rwLock.writeLock();
			writeLock.lock();
			try {
				ISimpleSimilarityValue[][] old = new ISimpleSimilarityValue[is.length][];
				for (int p = 0; p < is.length; p++) {
					if (childValue[p] == null)
						continue;
					old[p] = new ISimpleSimilarityValue[childValue[p].length];
					for (int fIdx = 0; fIdx < childValue[p].length; fIdx++) {
						final int i = is[p];
						if (this.isChildValueSet[i][fIdx])
							old[p][fIdx] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
									.getSimilarityFunctions()[fIdx].value(this.childValues[i][fIdx], ofType);
						else
							old[p][fIdx] = null;

						this.isChildValueSet[i][fIdx] = true;
						this.isMissingChildValue[i][fIdx] = childValue[p][fIdx] instanceof IMissingSimilarityValue;
						this.isMaxChildValue[i][fIdx] = childValue[p][fIdx] instanceof IMaximalSimilarityValue;
						this.isMinChildValue[i][fIdx] = childValue[p][fIdx] instanceof IMinimalSimilarityValue;
						this.childValues[i][fIdx] = childValue[p][fIdx].get();
					}
				}
				return old;
			} catch (final SimilarityCalculationException e) {
				throw new SimilarityValuesException(e);
			} finally {
				writeLock.unlock();
			}
		}

		@Override
		public ISmallSimilarityValueStorage subset(final int[] indices) {
			return new SmallSubsetOfSmallStorage(this, indices);
		}

		@Override
		protected double[] getRawChildValues(long i) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.childValues[Math.toIntExact(i)];
			} finally {
				readLock.unlock();
			}
		}

		protected double[] getRawChildValues(final int i) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.childValues[i];
			} finally {
				readLock.unlock();
			}
		}

		@Override
		protected double getRawValue(long i) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.values[Math.toIntExact(i)];
			} finally {
				readLock.unlock();
			}
		}

		protected double getRawValue(final int i) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.values[i];
			} finally {
				readLock.unlock();
			}
		}

		@Override
		protected boolean isChildValueSet(long i, int sf) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.isChildValueSet[Math.toIntExact(i)][sf];
			} finally {
				readLock.unlock();
			}
		}

		protected boolean isChildValueSet(final int i, final int sf) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.isChildValueSet[i][sf];
			} finally {
				readLock.unlock();
			}
		}

		@Override
		protected boolean isMissingChildValue(long i, int sf) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.isMissingChildValue[Math.toIntExact(i)][sf];
			} finally {
				readLock.unlock();
			}
		}

		protected boolean isMissingChildValue(int i, int sf) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.isMissingChildValue[i][sf];
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public ISimilarityValue mean(final int[] indices) throws InterruptedException, SimilarityValuesException {
			if (isComposite)
				return this.meanComposite(indices);
			else
				return this.meanNoncomposite(indices);
		}

		ICompositeSimilarityValue meanComposite(final int[] indices)
				throws InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				final int MIN_BATCH_SIZE = 50000;
				final double[] sum;
				final int[] counts;
				if (indices.length < MIN_BATCH_SIZE) {
					final Pair<double[], int[]> result = new MeanCompositeOp().perform(indices);
					sum = result.getFirst();
					counts = result.getSecond();
				} else {
					sum = new double[childFunctions.length];
					counts = new int[childFunctions.length];

					final Iterable<int[]> batches = Iterables.batchify(indices, new BatchCount(
							getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize(), MIN_BATCH_SIZE));

					final List<Future<Pair<double[], int[]>>> futures = new MyParallel<Pair<double[], int[]>>(
							getPrivateSimilarityFunctionThreadPool()).For(batches, new MeanCompositeOp());

					for (final Future<Pair<double[], int[]>> f : futures)
						try {
							final Pair<double[], int[]> ds = f.get();
							final double[] batchSum = ds.getFirst();
							final int[] batchCount = ds.getSecond();
							for (int i = 0; i < batchSum.length; i++) {
								sum[i] += batchSum[i];
								counts[i] += batchCount[i];
							}
						} catch (final ExecutionException e) {
							if (e.getCause() instanceof InterruptedException)
								throw (InterruptedException) e.getCause();
							if (e.getCause() != null)
								throw new SimilarityValuesException(e.getCause());
							throw new SimilarityValuesException(e);
						}

				}
				for (int i = 0; i < sum.length; i++)
					sum[i] /= counts[i];

				final ISimpleSimilarityValue[] childVals = new ISimpleSimilarityValue[sum.length];
				for (int i = 0; i < sum.length; i++)
					childVals[i] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
							.getSimilarityFunctions()[i].value(sum[i], ofType);
				return ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this).value(childVals, ofType);
			} finally {
				readLock.unlock();
			}
		}

		ISimpleSimilarityValue meanNoncomposite(final int[] indices)
				throws InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				double sum = 0.0;
				int count = 0;

				final int MIN_BATCH_SIZE = 50000;

				if (indices.length < MIN_BATCH_SIZE) {
					final Pair<Double, Integer> result = new MeanNoncompositeOp().perform(indices);
					sum = result.getFirst();
					count = result.getSecond();
				} else {
					final Iterable<int[]> batches = Iterables.batchify(indices,
							new BatchCount(getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize(), 500000));

					final List<Future<Pair<Double, Integer>>> futures = new MyParallel<Pair<Double, Integer>>(
							getPrivateSimilarityFunctionThreadPool()).For(batches, new MeanNoncompositeOp());

					for (final Future<Pair<Double, Integer>> f : futures)
						try {
							final Pair<Double, Integer> ds = f.get();
							sum += ds.getFirst();
							count += ds.getSecond();
						} catch (final ExecutionException e) {
							if (e.getCause() instanceof InterruptedException)
								throw (InterruptedException) e.getCause();
							if (e.getCause() != null)
								throw new SimilarityValuesException(e.getCause());
							throw new SimilarityValuesException(e);
						}
				}

				return ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(sum /= count, ofType);
			} finally {
				readLock.unlock();
			}
		}

		public ISimilarityValue sum(final int[] indices) throws InterruptedException, SimilarityValuesException {
			if (isComposite)
				return sumComposite(indices);
			else
				return sumNoncomposite(indices);
		}

		ICompositeSimilarityValue sumComposite(final int[] indices)
				throws InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				final int MIN_BATCH_SIZE = 50000;

				final double[] sum;
				final int[] count;

				if (indices.length < MIN_BATCH_SIZE) {
					Pair<double[], int[]> r = new SumCompositeOp().perform(indices);
					sum = r.getFirst();
					count = r.getSecond();
				} else {
					final Iterable<int[]> batches = Iterables.batchify(indices, new BatchCount(
							getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize(), MIN_BATCH_SIZE));

					final List<Future<Pair<double[], int[]>>> futures = new MyParallel<Pair<double[], int[]>>(
							getPrivateSimilarityFunctionThreadPool()).For(batches, new SumCompositeOp());

					sum = new double[childFunctions.length];
					count = new int[childFunctions.length];

					for (final Future<Pair<double[], int[]>> f : futures)
						try {
							final Pair<double[], int[]> ds = f.get();
							for (int i = 0; i < ds.getFirst().length; i++) {
								sum[i] += ds.getFirst()[i];
								count[i] += ds.getSecond()[i];
							}
						} catch (final ExecutionException e) {
							if (e.getCause() instanceof InterruptedException)
								throw (InterruptedException) e.getCause();
							if (e.getCause() != null)
								throw new SimilarityValuesException(e.getCause());
							throw new SimilarityValuesException(e);
						}

				}
				final ISimpleSimilarityValue[] childVals = new ISimpleSimilarityValue[sum.length];
				for (int i = 0; i < sum.length; i++)
					childVals[i] = ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
							.getSimilarityFunctions()[i].value(sum[i], ofType, count[i]);

				final CompositeSimilarityValue value = (CompositeSimilarityValue) ((ICompositeSimilarityFunction) AbstractSimilarityFunction.this)
						.value(childVals, this.ofType, indices.length);
				return value;
			} finally {
				readLock.unlock();
			}
		}

		ISimpleSimilarityValue sumNoncomposite(final int[] indices)
				throws InterruptedException, SimilarityValuesException {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				final int MIN_BATCH_SIZE = 50000;
				double sum = 0.0;

				if (indices.length < MIN_BATCH_SIZE) {
					sum = new SumNoncompositeOp().perform(indices);
				} else {
					final Iterable<int[]> batches = Iterables.batchify(indices, new BatchCount(
							getPrivateSimilarityFunctionThreadPool().getMaximumPoolSize(), MIN_BATCH_SIZE));

					final List<Future<Double>> futures = new MyParallel<Double>(
							getPrivateSimilarityFunctionThreadPool()).For(batches, new SumNoncompositeOp());

					for (final Future<Double> f : futures)
						try {
							sum += f.get();
						} catch (final ExecutionException e) {
							if (e.getCause() instanceof InterruptedException)
								throw (InterruptedException) e.getCause();
							if (e.getCause() != null)
								throw new SimilarityValuesException(e.getCause());
							throw new SimilarityValuesException(e);
						}
				}

				return ((ISimpleSimilarityFunction) AbstractSimilarityFunction.this).value(sum, ofType);
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public Iterator<ISimilarityValue> iterator() {
			return new Iterator<ISimilarityValue>() {
				private long curr = 0;

				@Override
				public boolean hasNext() {
					return this.curr < SmallSimilarityValueStorage.this.size();
				}

				@Override
				public ISimilarityValue next() {
					return SmallSimilarityValueStorage.this.get(this.curr++);
				}
			};
		}

	}

	protected boolean requiresLargeStorage(final long storageSize) {
		return storageSize > Integer.MAX_VALUE;
	}

	@Override
	public ISimilarityValueStorage newSimilarityStorage(final long size, ObjectType<?> ofType) {
		if (requiresLargeStorage(size)) {
			if (this instanceof ICompositeSimilarityFunction)
				return new LargeSimilarityValueStorage(size, ofType,
						((ICompositeSimilarityFunction) this).getSimilarityFunctions());
			return new LargeSimilarityValueStorage(size, ofType);
		} else if (this instanceof ICompositeSimilarityFunction)
			return new SmallSimilarityValueStorage((int) size, ofType,
					((ICompositeSimilarityFunction) this).getSimilarityFunctions());
		return new SmallSimilarityValueStorage((int) size, ofType);
	}

	@Override
	public <O1 extends IObjectWithFeatures, O2 extends IObjectWithFeatures, P extends IObjectWithFeatures & IPair<O1, O2>> ISimilarityValuesSomePairs<O1, O2, P> calculateSimilarities(
			Collection<P> pairs, ObjectType<P> type)
			throws SimilarityCalculationException, InterruptedException, IncompatibleSimilarityValueException {
		try {
			final ISimilarityValuesSomePairs<O1, O2, P> result = this.emptySimilarities(pairs, type);

			BoundIterable<P> boundIterable = Iterables.toBoundIterable(pairs);

			if (boundIterable.longSize() == 0)
				return result;
			else if (boundIterable.longSize() < MIN_BATCH_SIZE) {
				LongList is = new LongArrayList();
				ObjectList<ISimilarityValue> sv = new ObjectArrayList<>();
				long i = 0;
				for (final P p : boundIterable) {
					if (!Utility.getProgress().getStatus())
						throw new InterruptedException();
					is.add(i++);
					sv.add(this.calculateSimilarity(p));
				}
				result.set(is.toLongArray(), sv.toArray(new ISimilarityValue[0]));
			} else {
				final Iterable<Iterable<P>> batches = Iterables.batchify(boundIterable,
						new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<Iterable<ISimilarityValue>>> queue = new MyParallel<Iterable<ISimilarityValue>>(
						privateSimilarityFunctionThreadPool) {
					@Override
					protected boolean isOnExceptionShutdown(final Exception e) {
						return e instanceof InterruptedException;
					};
				}.For(batches, new Operation<Iterable<P>, Iterable<ISimilarityValue>>() {
					@Override
					public Iterable<ISimilarityValue> perform(final Iterable<P> pairs) throws Exception {
						final List<ISimilarityValue> result = new LinkedList<>();
						for (final P p : pairs) {
							if (!Utility.getProgress().getStatus())
								throw new InterruptedException();
							result.add(AbstractSimilarityFunction.this.calculateSimilarity(p));
						}
						return result;
					}
				});

				LongList is = new LongArrayList();
				ObjectList<ISimilarityValue> sv = new ObjectArrayList<>();
				long i = 0;
				for (Future<Iterable<ISimilarityValue>> f : queue)
					for (final ISimilarityValue sim : f.get()) {
						is.add(i++);
						sv.add(sim);
					}
				result.set(is.toLongArray(), sv.toArray(new ISimilarityValue[0]));
				logger.debug(result.getClass() + " #sims: " + result.size());
			}
			return result;
		} catch (final ExecutionException e) {
			if (e.getCause() instanceof InterruptedException)
				throw (InterruptedException) e.getCause();
			else if (e.getCause() instanceof SimilarityCalculationException)
				throw (SimilarityCalculationException) e.getCause();
			else if (e.getCause() != null)
				throw new SimilarityCalculationException(e.getCause());
			throw new SimilarityCalculationException(e);
		} catch (final SimilarityValuesException e) {
			throw new SimilarityCalculationException(e);
		} catch (IncompatibleObjectTypeException e) {
			throw new SimilarityCalculationException(e);
		}
	}

	@Override
	public <O1 extends IObjectWithFeatures, O2 extends IObjectWithFeatures, P extends IObjectWithFeatures & IPair<O1, O2>> void calculateSimilarities(
			final int[] objects1, final int[] objects2, ISimilarityValuesAllPairs<O1, O2, P> similarities)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleSimilarityValueException {
		long n = (long) objects1.length * objects2.length;
		if (n == 0)
			return;
		else if (n <= Integer.MAX_VALUE) {
			if (similarities.getStorage() instanceof ISmallSimilarityValueStorage) {
				final int[] pairIndices = new int[objects1.length * objects2.length];
				for (int x = 0; x < objects1.length; x++)
					for (int y = 0; y < objects2.length; y++)
						pairIndices[x * objects2.length + y] = objects1[x] * similarities.secondObjectIndices().length
								+ objects2[y];
				calculateSimilarities(pairIndices, similarities);
			} else {
				final long[] pairIndices = new long[objects1.length * objects2.length];
				for (int x = 0; x < objects1.length; x++)
					for (int y = 0; y < objects2.length; y++)
						pairIndices[x * objects2.length + y] = (long) objects1[x]
								* similarities.secondObjectIndices().length + objects2[y];
				calculateSimilarities(pairIndices, similarities);
			}
			return;
		}

		try {
			long startTime = System.currentTimeMillis();
			BoundIterable<int[]> iterable = new PairIndexIterable(objects1.length, objects2.length);
			long startTime1 = System.currentTimeMillis();
			iterable = Iterables.toBoundIterable(Iterables.filter(iterable, t -> {
				try {
					return !similarities.isSet(objects1[t[0]], objects2[t[1]]);
				} catch (Exception e) {
					return false;
				}
			}));
			if (iterable.longSize() == 0)
				return;
			// if we have too few similarities, we avoid the overhead of parallelism
			if (iterable.longSize() < MIN_BATCH_SIZE) {
				for (int[] xy : iterable) {
					similarities.set(xy[0], xy[1], this.calculateSimilarity(similarities.getPair(xy)));
				}
				// System.out.println(String.format("calculateSimilarities (%d) %d %d",
				// iterable.longSize(),
				// startTime1 - startTime, System.currentTimeMillis() - startTime1));
			} else {
				long startTime2 = System.currentTimeMillis();
				final Iterable<Iterable<int[]>> batches = Iterables.batchify(iterable,
						new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), MIN_BATCH_SIZE));
				long startTime3 = System.currentTimeMillis();

				final List<Future<Iterable<ISimilarityValue>>> queue = new MyParallel<Iterable<ISimilarityValue>>(
						privateSimilarityFunctionThreadPool) {
					@Override
					protected boolean isOnExceptionShutdown(final Exception e) {
						return e instanceof InterruptedException;
					};
				}.For(batches, new Operation<Iterable<int[]>, Iterable<ISimilarityValue>>() {
					@Override
					public Iterable<ISimilarityValue> perform(final Iterable<int[]> pairs) throws Exception {
						final List<ISimilarityValue> result = new LinkedList<>();
						for (final int[] idx : pairs) {
							if (!Utility.getProgress().getStatus())
								throw new InterruptedException();
							result.add(AbstractSimilarityFunction.this.calculateSimilarity(similarities.getPair(idx)));
						}
						return result;
					}
				});
				long startTime4 = System.currentTimeMillis();

				final Iterator<int[]> iterator = iterable.iterator();
				for (Future<Iterable<ISimilarityValue>> f : queue)
					for (final ISimilarityValue sim : f.get()) {
						int[] xy = iterator.next();
						similarities.set(xy[0], xy[1], sim);
					}
				// System.out.println(String.format("calculateSimilarities %d %d %d %d %d",
				// startTime1 - startTime,
				// startTime2 - startTime1, startTime3 - startTime2, startTime4 - startTime3,
				// System.currentTimeMillis() - startTime4));
			}
		} catch (final ExecutionException e) {
			if (e.getCause() instanceof InterruptedException)
				throw (InterruptedException) e.getCause();
			else if (e.getCause() instanceof SimilarityCalculationException)
				throw (SimilarityCalculationException) e.getCause();
			else if (e.getCause() != null)
				throw new SimilarityCalculationException(e.getCause());
			throw new SimilarityCalculationException(e);
		} catch (IncompatibleObjectTypeException e) {
			throw new SimilarityCalculationException(e);
		}

	}

	public <O1 extends IObjectWithFeatures, O2 extends IObjectWithFeatures, P extends IObjectWithFeatures & IPair<O1, O2>> void calculateSimilarities(
			final int[] similarityIndices, ISimilarityValuesAllPairs<O1, O2, P> similarities)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleSimilarityValueException {
		try {
			final boolean[] isSet = similarities.isSet(similarityIndices);
			final int[] unsetSimilarityIndices = IntStream.range(0, similarityIndices.length).filter(i -> !isSet[i])
					.map(i -> similarityIndices[i]).toArray();

			if (unsetSimilarityIndices.length == 0)
				return;
			// if we have too few similarities, we avoid the overhead of parallelism
			if (unsetSimilarityIndices.length < MIN_BATCH_SIZE) {
				final ISimilarityValue[] vs = new ISimilarityValue[unsetSimilarityIndices.length];
				int p = 0;
				for (int xy : unsetSimilarityIndices) {
					vs[p++] = this.calculateSimilarity(similarities.getPair(xy));
				}
				similarities.set(unsetSimilarityIndices, vs);
			} else {
				final Iterable<int[]> batches = Iterables.batchify(unsetSimilarityIndices,
						new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<ISimilarityValue[]>> queue = new MyParallel<ISimilarityValue[]>(
						privateSimilarityFunctionThreadPool) {
					@Override
					protected boolean isOnExceptionShutdown(final Exception e) {
						return e instanceof InterruptedException;
					};
				}.For(batches, new Operation<int[], ISimilarityValue[]>() {
					@Override
					public ISimilarityValue[] perform(final int[] pairs) throws Exception {
						final ISimilarityValue[] result = new ISimilarityValue[pairs.length];
						int p = 0;
						for (final int idx : pairs) {
							if (!Utility.getProgress().getStatus())
								throw new InterruptedException();
							result[p++] = AbstractSimilarityFunction.this
									.calculateSimilarity(similarities.getPair(idx));
						}
						return result;
					}
				});

				Iterator<int[]> it1 = batches.iterator();
				Iterator<Future<ISimilarityValue[]>> it2 = queue.iterator();
				while (it1.hasNext() && it2.hasNext()) {
					similarities.set(it1.next(), it2.next().get());
				}
			}
		} catch (final ExecutionException e) {
			if (e.getCause() instanceof InterruptedException)
				throw (InterruptedException) e.getCause();
			else if (e.getCause() instanceof SimilarityCalculationException)
				throw (SimilarityCalculationException) e.getCause();
			else if (e.getCause() != null)
				throw new SimilarityCalculationException(e.getCause());
			throw new SimilarityCalculationException(e);
		} catch (IncompatibleObjectTypeException e) {
			throw new SimilarityCalculationException(e);
		}

	}

	public <O1 extends IObjectWithFeatures, O2 extends IObjectWithFeatures, P extends IObjectWithFeatures & IPair<O1, O2>> void calculateSimilarities(
			long[] similarityIndices, ISimilarityValuesAllPairs<O1, O2, P> similarities)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleSimilarityValueException {
		try {
			final boolean[] isSet = similarities.isSet(similarityIndices);
			final long[] unsetSimilarityIndices = IntStream.range(0, similarityIndices.length).filter(i -> !isSet[i])
					.mapToLong(i -> similarityIndices[i]).toArray();

			if (unsetSimilarityIndices.length == 0)
				return;
			// if we have too few similarities, we avoid the overhead of parallelism
			if (unsetSimilarityIndices.length < MIN_BATCH_SIZE) {
				final ISimilarityValue[] vs = new ISimilarityValue[unsetSimilarityIndices.length];
				int p = 0;
				for (long xy : unsetSimilarityIndices) {
					vs[p++] = this.calculateSimilarity(similarities.getPair(xy));
				}
				similarities.set(unsetSimilarityIndices, vs);
			} else {
				final Iterable<long[]> batches = Iterables.batchify(unsetSimilarityIndices,
						new BatchCount(privateSimilarityFunctionThreadPool.getMaximumPoolSize(), MIN_BATCH_SIZE));

				final List<Future<Iterable<ISimilarityValue>>> queue = new MyParallel<Iterable<ISimilarityValue>>(
						privateSimilarityFunctionThreadPool) {
					@Override
					protected boolean isOnExceptionShutdown(final Exception e) {
						return e instanceof InterruptedException;
					};
				}.For(batches, new Operation<long[], Iterable<ISimilarityValue>>() {
					@Override
					public Iterable<ISimilarityValue> perform(final long[] pairs) throws Exception {
						final List<ISimilarityValue> result = new LinkedList<>();
						for (final long idx : pairs) {
							if (!Utility.getProgress().getStatus())
								throw new InterruptedException();
							result.add(AbstractSimilarityFunction.this.calculateSimilarity(similarities.getPair(idx)));
						}
						return result;
					}
				});

				LongList is = new LongArrayList();
				ObjectList<ISimilarityValue> sv = new ObjectArrayList<>();

				long i = 0;
				for (Future<Iterable<ISimilarityValue>> f : queue)
					for (final ISimilarityValue sim : f.get()) {
						is.add(i++);
						sv.add(sim);
					}
				similarities.set(is.toLongArray(), sv.toArray(new ISimilarityValue[0]));
			}
		} catch (final ExecutionException e) {
			if (e.getCause() instanceof InterruptedException)
				throw (InterruptedException) e.getCause();
			else if (e.getCause() instanceof SimilarityCalculationException)
				throw (SimilarityCalculationException) e.getCause();
			else if (e.getCause() != null)
				throw new SimilarityCalculationException(e.getCause());
			throw new SimilarityCalculationException(e);
		} catch (IncompatibleObjectTypeException | IncompatibleSimilarityValueStorageException e) {
			throw new SimilarityCalculationException(e);
		}

	}

	@Override
	public <O1 extends IObjectWithFeatures, O2 extends IObjectWithFeatures, P extends IObjectWithFeatures & IPair<O1, O2>> ISimilarityValuesAllPairs<O1, O2, P> calculateSimilarities(
			O1[] objects1, O2[] objects2, final IPairsFactory<O1, O2, P> factory, ObjectType<P> type)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleSimilarityValueException {
		long startTime = System.currentTimeMillis();
		final ISimilarityValuesAllPairs<O1, O2, P> result = this.emptySimilarities(objects1, objects2, factory, type);
		long startTime1 = System.currentTimeMillis();
		calculateSimilarities(result.firstObjectIndices(), result.secondObjectIndices(), result);
		// System.out.println(String.format("calculateSimilarities %d %d", startTime1 -
		// startTime,
		// System.currentTimeMillis() - startTime1));
		return result;
	}

	protected static final int requireIntForStorage(final long idx) throws IncompatibleSimilarityValueStorageException {
		try {
			return Math.toIntExact(idx);
		} catch (ArithmeticException e) {
			throw new IncompatibleSimilarityValueStorageException(e);
		}
	}

	protected static final int[] requireIntForStorage(final long[] idx)
			throws IncompatibleSimilarityValueStorageException {
		try {
			final int[] result = new int[idx.length];
			for (int i = 0; i < result.length; i++)
				result[i] = Math.toIntExact(idx[i]);
			return result;
		} catch (ArithmeticException e) {
			throw new IncompatibleSimilarityValueStorageException(e);
		}
	}

}

class MyThreadFactory implements ThreadFactory {
	ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();

	Class<? extends AbstractSimilarityFunction> c;

	public MyThreadFactory(Class<? extends AbstractSimilarityFunction> c) {
		super();
		this.c = c;
	}

	@Override
	public Thread newThread(Runnable r) {
		Thread t = defaultThreadFactory.newThread(r);

		t.setName(t.getName().replace("pool-", String.format("%s-pool-", c.getSimpleName())));

		return t;
	}
}
