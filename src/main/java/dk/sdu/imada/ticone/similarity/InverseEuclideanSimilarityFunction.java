
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.ITimePointWeighting;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.TimePointWeighting;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * This class calculates the negative euclidean similarity of two objects.
 * 
 * @author Christian Wiwie
 * @author Christian Nørskov
 */
public class InverseEuclideanSimilarityFunction extends AbstractTimeSeriesSimilarityFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -550154574464578774L;

	private double maxDistance = 0.0;

	public InverseEuclideanSimilarityFunction() {
		super(true);
	}

	public InverseEuclideanSimilarityFunction(final boolean useCache) {
		super(useCache);
	}

	public InverseEuclideanSimilarityFunction(final ITimePointWeighting tpWeights) {
		this(true, tpWeights);
	}

	public InverseEuclideanSimilarityFunction(final boolean useCache, final ITimePointWeighting tpWeights) {
		super(useCache, tpWeights);
	}

	public InverseEuclideanSimilarityFunction(final InverseEuclideanSimilarityFunction other) {
		super(other);
		if (other.tpWeights != null)
			this.tpWeights = new TimePointWeighting(other.tpWeights);
		this.normalizeByNumberTimepoints = other.normalizeByNumberTimepoints;
		this.maxDistance = other.maxDistance;
	}

	@Override
	public InverseEuclideanSimilarityFunction copy() {
		return new InverseEuclideanSimilarityFunction(this);
	}

	@Override
	protected <O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue doCalculateSimilarity(O object,
			ITimeSeries[] timeSeries, ITimeSeries[] otherTimeSeries)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		double simSum = 0.0;
		for (ITimeSeries ts1 : timeSeries)
			for (ITimeSeries ts2 : otherTimeSeries)
				simSum += this.calculateTimeSeriesSimilarity(ts1.asArray(), ts2.asArray(), object.getObjectType())
						.calculate().get();
		return this.value(simSum / timeSeries.length / otherTimeSeries.length, object.getObjectType());
	}

	/**
	 * Calculates a similarity between two double arrays between 0.0 and 1.0.
	 * 
	 * @param data1
	 * @param data2
	 * @param ofType
	 * @return A similarity between 0.0 and 1.0.
	 * @throws TimeSeriesNotCompatibleException
	 */
	private ISimpleSimilarityValue calculateTimeSeriesSimilarity(final double[] data1, final double[] data2,
			final ObjectType<?> ofType) throws TimeSeriesNotCompatibleException {
		double distance = 0.0;
		if (data1.length != data2.length) {
			throw new TimeSeriesNotCompatibleException();
		}

		double weightSum = 0.0;
		for (int i = 0; i < data1.length; i++) {
			final double diff = data1[i] - data2[i];
			if (this.tpWeights != null) {
				double timePointWeight = this.getTimePointWeight(i);
				distance += timePointWeight * diff * diff;
				weightSum += timePointWeight;
			} else {
				distance += diff * diff;
				weightSum += 1.0;
			}
		}
		distance = Math.sqrt(distance);

		if (isNormalizeByNumberTimepoints())
			distance /= weightSum;

		final double similarity;
		if (maxDistance == 0.0)
			similarity = -distance;
		else
			similarity = Math.min(Math.max(0.0, (this.maxDistance - distance) / maxDistance), 1.0);

		return new SimpleSimilarityValue(similarity, ofType);

	}

	@Override
	public String toString() {
		return "Inverse Euclidian Distance";
	}

	private boolean normalizeByNumberTimepoints = true;

	/**
	 * @return the normalizeByNumberTimepoints
	 */
	@Override
	public boolean isNormalizeByNumberTimepoints() {
		return this.normalizeByNumberTimepoints;
	}

	/**
	 * @param normalizeByNumberTimepoints the normalizeByNumberTimepoints to set
	 */
	@Override
	public void setNormalizeByNumberTimepoints(boolean normalizeByNumberTimepoints) {
		this.normalizeByNumberTimepoints = normalizeByNumberTimepoints;
	}

	@Override
	public void ensureObjectIsKnown(IObjectWithFeatures object)
			throws UnknownObjectFeatureValueProviderException, IncompatibleFeatureValueProviderException {
	}

	@Override
	public boolean initializeForFeature(IFeature<?> feature) {
		return true;
	}

	@Override
	public void setTimeSeriesPreprocessor(ITimeSeriesPreprocessor preprocessor) {
		super.setTimeSeriesPreprocessor(preprocessor);
		double constant = 0.0;
		final int numberTimePoints = preprocessor.getObjects().get(0).getPreprocessedTimeSeriesList()[0]
				.getNumberTimePoints();
		double weightSum = 0.0;
		for (int tp = 0; tp < numberTimePoints; tp++) {
			double timePointWeight = getTimePointWeight(tp);
			weightSum += timePointWeight;
			constant += timePointWeight * Math.pow(preprocessor.getMaxValue() - preprocessor.getMinValue(), 2.0);
		}
		constant = Math.sqrt(constant);

		if (isNormalizeByNumberTimepoints())
			constant /= weightSum;

		this.maxDistance = constant;
	}
	
	/**
	 * @return the maxValue
	 */
	public double getMaxDistance() {
		return this.maxDistance;
	}
}
