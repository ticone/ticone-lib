/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
public abstract class AbstractSimilarityValue implements ISimilarityValue {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6591937591995383181L;

	public static final ISimilarityValue MAX = new MaximalSimilarityValue();

	public static final ISimilarityValue MIN = new MinimalSimilarityValue();

	protected final ObjectType<?> ofType;

	protected double value = Double.NaN;

	protected boolean calculated;

	protected final int cardinality;

	public AbstractSimilarityValue(final ObjectType<?> ofType) {
		this(ofType, 1);
	}

	public AbstractSimilarityValue(final ObjectType<?> ofType, final int cardinality) {
		super();
		this.ofType = ofType;
		this.cardinality = cardinality;
	}

	@Override
	public String toString() {
		if (!this.calculated)
			return "???";
		return Double.toString(this.value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof ISimilarityValue)
			try {
				return Double.compare(this.get(), ((ISimilarityValue) obj).get()) == 0;
			} catch (final SimilarityCalculationException e) {
			}
		return false;
	}

	@Override
	public int hashCode() {
		try {
			return Double.hashCode(this.get());
		} catch (final SimilarityCalculationException e) {
			return 0;
		}
	}

	@Override
	public boolean isCalculated() {
		return this.calculated;
	}

	protected double doGet() {
		return this.value;
	}

	@Override
	public double get() throws SimilarityCalculationException {
		if (!isCalculated())
			doCalculate();
		return doGet();
	}

	@Override
	public ISimilarityValue calculate() throws SimilarityCalculationException {
		if (!isCalculated())
			doCalculate();
		return this;
	}

	protected abstract ISimilarityValue doCalculate() throws SimilarityCalculationException;

	@Override
	public int cardinality() {
		return this.cardinality;
	}

}

/**
 * @author Christian Wiwie
 * 
 * @since Oct 1, 2018
 *
 */
class MaximalSimilarityValue extends AbstractSimilarityValue implements IMaximalSimilarityValue {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3286679372037926359L;

	/**
	 * 
	 */
	public MaximalSimilarityValue() {
		super(null);
		this.value = Double.POSITIVE_INFINITY;
		this.calculated = true;
	}

	@Override
	protected IMaximalSimilarityValue doCalculate() {
		return this;
	}
}

/**
 * @author Christian Wiwie
 * 
 * @since Oct 1, 2018
 *
 */
class MinimalSimilarityValue extends AbstractSimilarityValue implements IMinimalSimilarityValue {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6733255954578257091L;

	/**
	 * 
	 */
	public MinimalSimilarityValue() {
		super(null);
		this.value = Double.NEGATIVE_INFINITY;
		this.calculated = true;
	}

	@Override
	protected IMinimalSimilarityValue doCalculate() {
		return this;
	}
}
