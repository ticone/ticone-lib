/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.util.ITimePointWeighting;
import dk.sdu.imada.ticone.util.TimePointWeighting;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 16, 2019
 *
 */
public abstract class AbstractTimeSeriesSimilarityFunction extends AbstractSimpleSimilarityFunction<ITimeSeries>
		implements IWeightedTimeSeriesSimilarityFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5890865767447158525L;
	protected ITimePointWeighting tpWeights;
	protected ITimeSeriesPreprocessor preprocessor;

	/**
	 * @param useCache
	 */
	public AbstractTimeSeriesSimilarityFunction(final boolean useCache) {
		super(useCache);
	}

	public AbstractTimeSeriesSimilarityFunction(final boolean useCache, final ITimePointWeighting tpWeights) {
		super(useCache);
		this.tpWeights = tpWeights;
	}

	/**
	 * @param other
	 */
	public AbstractTimeSeriesSimilarityFunction(AbstractTimeSeriesSimilarityFunction other) {
		super(other);
		if (other.tpWeights != null)
			this.tpWeights = new TimePointWeighting(other.tpWeights);
		if (other.preprocessor != null)
			this.preprocessor = other.preprocessor;
	}

	@Override
	public ITimePointWeighting getTimePointWeights() {
		return this.tpWeights;
	}

	@Override
	public void setTimePointWeights(ITimePointWeighting tpWeights) {
		this.tpWeights = tpWeights;
	}

	@Override
	public abstract AbstractTimeSeriesSimilarityFunction copy();

	@Override
	public void setTimeSeriesPreprocessor(ITimeSeriesPreprocessor preprocessor) {
		this.preprocessor = preprocessor;
	}

	@Override
	public ITimeSeriesPreprocessor getTimeSeriesPreprocessor() {
		return this.preprocessor;
	}
}