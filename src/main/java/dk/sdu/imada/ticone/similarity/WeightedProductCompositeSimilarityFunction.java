/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import java.text.DecimalFormat;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.ScalingException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 28, 2018
 *
 */
public class WeightedProductCompositeSimilarityFunction extends AbstractWeightedCompositeSimilarityFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7752863218540998993L;

	public WeightedProductCompositeSimilarityFunction(final ISimpleSimilarityFunction... similarityFunctions) {
		this(true, similarityFunctions);
	}

	public WeightedProductCompositeSimilarityFunction(final boolean useCache,
			final ISimpleSimilarityFunction... similarityFunctions) {
		super(useCache, similarityFunctions);
	}

	public WeightedProductCompositeSimilarityFunction(AbstractWeightedCompositeSimilarityFunction other) {
		super(other);
	}

	@Override
	public WeightedProductCompositeSimilarityFunction copy() {
		return new WeightedProductCompositeSimilarityFunction(this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.similarityFunctions.length; i++) {
			if (this.functionWeights[i] != null)
				if (this.functionWeights[i].doubleValue() == 0.0) {
					continue;
				} else
					sb.append(new DecimalFormat("#.###").format(this.functionWeights[i].doubleValue()));
			sb.append(this.similarityFunctions[i]);
			if (i < this.similarityFunctions.length - 1)
				sb.append(" * ");
		}
		return sb.toString();
	}

	@Override
	protected double calculateFromScaledChildValues(final ISimpleSimilarityValue[] scaledChildSimilarities)
			throws SimilarityCalculationException {
		double sim = 1.0;
		int usedChilds = 0;
		double weightSum = 0.0;
		for (int i = 0; i < scaledChildSimilarities.length; i++) {
			final ISimilarityValue childSim = scaledChildSimilarities[i];

			double childValue;

			if (childSim instanceof IMissingSimilarityValue) {
				if (this.ignoreChildMissing)
					continue;
				else
					return Double.NaN;
			} else
				childValue = childSim.get();

			if (Double.isInfinite(childValue))
				if (this.ignoreChildInfinity)
					continue;
				else
					return childValue;

			if (Double.isNaN(childValue))
				if (this.missingChildHandling == MISSING_CHILD_HANDLING.IGNORE_CHILD)
					continue;
				else if (this.missingChildHandling == MISSING_CHILD_HANDLING.RETURN_MISSING)
					return Double.NaN;
				else if (this.missingChildHandling == MISSING_CHILD_HANDLING.TREAT_AS_ZERO)
					childValue = 0.0;

			final double weight = this.functionWeights[i] != null ? this.functionWeights[i] : 1.0;
			sim *= childValue * weight + (1 - weight);
			weightSum += weight;
			usedChilds++;
		}
		if (usedChilds == 0)
			throw new SimilarityCalculationException(
					"Either no child similarities were present or all of them were NaN or infinity and thus ignored.");
		if (weightSum == 0.0)
			throw new SimilarityCalculationException("At least one non-zero function weight has to be specified");
		return sim;
	}

	@Override
	protected String toHtmlString(final CompositeSimilarityValue value) {
		final ISimpleSimilarityFunction[] childFunctions = this.similarityFunctions;
		final ISimpleSimilarityValue[] childValues = value.getChildValues();
		final StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		sb.append("<b>");
		sb.append(this.toString());
		sb.append("</b><br/>");
		sb.append("<br/>");
		sb.append("S(x,y) = ");

		double weightSum = 0.0;
		for (int i = 0; i < this.functionWeights.length; i++)
			if (this.functionWeights[i] != null)
				weightSum += this.functionWeights[i];

		for (int i = 0; i < childFunctions.length; i++) {
			if (childFunctions.length > 1)
				sb.append(String.format("1/%d * ", childFunctions.length));
			if (this.functionWeights[i] != null) {
				final double w = this.functionWeights[i];
				sb.append(String.format("( %.3f * ", w, w));
			}
			if (this.functionScalersObjectClusterPair[i] != null)
				sb.append(String.format("t%d(S%d(x,y))", i + 1, i + 1));
			else
				sb.append(String.format("S%d(x,y)", i + 1));

			if (this.functionWeights[i] != null) {
				sb.append(String.format(" + (%.3f - %.3f))", weightSum, this.functionWeights[i]));
			}

			if (i + 1 < childFunctions.length)
				sb.append(" * ");
			else
				sb.append("<br/>");
		}
		sb.append("<br/>");
		sb.append("<i>Child Similarity Functions:</i><br/>");
		for (int i = 0; i < childFunctions.length; i++) {
			sb.append(String.format("S%d: ", i + 1));
			sb.append(childFunctions[i]);
			sb.append("<br/>");
		}
		sb.append("<br/>");
		for (int i = 0; i < childFunctions.length; i++) {
			try {
				sb.append(String.format("S%d(x,y) = %.5f", i + 1, childValues[i].get()));
			} catch (SimilarityCalculationException e) {
				sb.append("S%d(x,y) = ???");
			}
			sb.append("<br/>");
		}
		boolean anyScaler = false;
		for (int i = 0; i < this.functionScalersObjectClusterPair.length; i++)
			if (this.functionScalersObjectClusterPair[i] != null) {
				anyScaler = true;
				break;
			}
		if (anyScaler) {
			sb.append("<br/>");
			sb.append("<i>Scalers:</i><br/>");
			for (int i = 0; i < this.functionScalersObjectClusterPair.length; i++) {
				if (this.functionScalersObjectClusterPair[i] != null) {
					sb.append(String.format("t%d: ", i + 1));
					sb.append(this.functionScalersObjectClusterPair[i]);
					sb.append("<br/>");
				}
			}
			sb.append("<br/>");
			for (int i = 0; i < this.functionScalersObjectClusterPair.length; i++) {
				if (this.functionScalersObjectClusterPair[i] != null) {
					double scale;
					try {
						scale = this.functionScalersObjectClusterPair[i].scale(childValues[i]);
					} catch (ScalingException e) {
						scale = Double.NaN;
					}
					sb.append(String.format("t%d(S%d(x,y)) = %.5f", i + 1, i + 1, scale));
					sb.append("<br/>");
				}
			}
		}
		sb.append("</html>");
		return sb.toString();
	}

	@Override
	public void ensureObjectIsKnown(IObjectWithFeatures object)
			throws UnknownObjectFeatureValueProviderException, IncompatibleFeatureValueProviderException {
	}

	@Override
	public boolean initializeForFeature(IFeature<?> feature) {
		return true;
	}
}
