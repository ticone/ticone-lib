/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import java.util.Arrays;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.ScalingException;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 28, 2018
 *
 */
public abstract class AbstractWeightedCompositeSimilarityFunction extends AbstractSimilarityFunction
		implements ICompositeSimilarityFunction {

	public class CompositeSimilarityValue extends AbstractSimilarityValue implements ICompositeSimilarityValue {
		/**
		 * 
		 */
		private static final long serialVersionUID = 273916574019222449L;
		final protected ISimpleSimilarityValue[] values;
		protected ISimpleSimilarityValue[] scaledValues;

		public CompositeSimilarityValue(final ISimpleSimilarityValue[] values, final ObjectType<?> ofType) {
			this(values, ofType, 1);
		}

		public CompositeSimilarityValue(final ISimpleSimilarityValue[] values, final ObjectType<?> ofType,
				final int cardinality) {
			super(ofType, cardinality);
			this.values = values;
		}

		/**
		 * @return the type of the instance for which this similarity value has been
		 *         calculated.
		 */
		public ObjectType<?> ofType() {
			return this.ofType;
		}

		/**
		 * @return the functionWeights
		 */
		@Override
		public Double[] getFunctionWeights() {
			return AbstractWeightedCompositeSimilarityFunction.this.functionWeights;
		}

		@Override
		public ISimpleSimilarityValue getChildSimilarityValue(final int i) {
			return this.values[i];
		}

		@Override
		public ISimpleSimilarityValue getScaledChildSimilarityValue(int i) {
			if (this.scaledValues == null)
				return null;
			return this.scaledValues[i];
		}

		@Override
		public ISimpleSimilarityFunction[] getChildFunctions() {
			return AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions;
		}

		@Override
		public ISimpleSimilarityValue[] getChildValues() {
			return this.values;
		}

		@Override
		public ISimpleSimilarityValue[] getScaledChildValues() {
			return this.scaledValues;
		}

		@Override
		public ICompositeSimilarityValue times(final double value) throws SimilarityCalculationException {
			final ISimpleSimilarityValue[] newValues = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this
					.getSimilarityFunctions().length];
			for (int i = 0; i < newValues.length; i++)
				newValues[i] = this.values[i].times(value);
			return new CompositeSimilarityValue(newValues, ofType);
		}

		@Override
		public ICompositeSimilarityValue divideBy(final double value) throws SimilarityCalculationException {
			final ISimpleSimilarityValue[] newValues = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this
					.getSimilarityFunctions().length];
			for (int i = 0; i < newValues.length; i++)
				newValues[i] = this.values[i].divideBy(value);
			return new CompositeSimilarityValue(newValues, ofType);
		}

		@Override
		public ICompositeSimilarityValue divideBy(final ICompositeSimilarityValue value)
				throws SimilarityCalculationException {
			if (!(Arrays.equals(this.getChildFunctions(), value.getChildFunctions())))
				throw new SimilarityCalculationException(
						"The passed composite similarity value has different similarity functions than this one");
			final ISimpleSimilarityValue[] newValues = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length];
			for (int i = 0; i < AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length; i++)
				newValues[i] = this.values[i].divideBy(value.getChildSimilarityValue(i));
			return new CompositeSimilarityValue(newValues, ofType);
		}

		@Override
		public ICompositeSimilarityValue minus(final ICompositeSimilarityValue value)
				throws SimilarityCalculationException {
			if (!(Arrays.equals(this.getChildFunctions(), value.getChildFunctions())))
				throw new SimilarityCalculationException(
						"The passed composite similarity value has different similarity functions than this one");
			final ISimpleSimilarityValue[] newValues = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length];
			for (int i = 0; i < AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length; i++)
				newValues[i] = this.values[i].minus(value.getChildSimilarityValue(i));
			return new CompositeSimilarityValue(newValues, ofType, this.cardinality - value.cardinality());
		}

		@Override
		public ICompositeSimilarityValue plus(final ICompositeSimilarityValue value)
				throws SimilarityCalculationException {
			if (!(Arrays.equals(this.getChildFunctions(), value.getChildFunctions())))
				throw new SimilarityCalculationException(
						"The passed composite similarity value has different similarity functions than this one");
			final ISimpleSimilarityValue[] newValues = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length];
			for (int i = 0; i < AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length; i++)
				newValues[i] = this.values[i].plus(value.getChildSimilarityValue(i));
			return new CompositeSimilarityValue(newValues, ofType, this.cardinality + value.cardinality());
		}

		@Override
		public ICompositeSimilarityValue times(final ICompositeSimilarityValue value)
				throws SimilarityCalculationException {
			if (!(Arrays.equals(this.getChildFunctions(), value.getChildFunctions())))
				throw new SimilarityCalculationException(
						"The passed composite similarity value has different similarity functions than this one");
			final ISimpleSimilarityValue[] newValues = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length];
			for (int i = 0; i < AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length; i++)
				newValues[i] = this.values[i].times(value.getChildSimilarityValue(i));
			return new CompositeSimilarityValue(newValues, ofType);
		}

		@Override
		public ICompositeSimilarityValue doCalculate() throws SimilarityCalculationException {
			try {
				this.scaledValues = this.scaleChildSimilarityValues(this.values, this.ofType);
				this.value = AbstractWeightedCompositeSimilarityFunction.this
						.calculateFromScaledChildValues(this.scaledValues);
				this.calculated = true;
				return this;
			} catch (ScalingException e) {
				throw new SimilarityCalculationException(e);
			}
		}

		@Override
		public IScaler[] getFunctionScalers() {
			return AbstractWeightedCompositeSimilarityFunction.this.functionScalersObjectClusterPair;
		}

		@Override
		public ICompositeSimilarityValue minus(final double value) throws SimilarityCalculationException {
			if (Double.isNaN(value))
				return this;
			final ISimpleSimilarityValue[] newValues = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this
					.getSimilarityFunctions().length];
			for (int i = 0; i < newValues.length; i++)
				newValues[i] = this.values[i].minus(value);
			return new CompositeSimilarityValue(newValues, ofType, this.cardinality - 1);

		}

		@Override
		public ICompositeSimilarityValue plus(final double value, final int cardinality)
				throws SimilarityCalculationException {
			if (Double.isNaN(value))
				return this;
			final ISimpleSimilarityValue[] newValues = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this
					.getSimilarityFunctions().length];
			for (int i = 0; i < newValues.length; i++)
				newValues[i] = this.values[i].plus(value);
			return new CompositeSimilarityValue(newValues, ofType, this.cardinality + cardinality);

		}

		@Override
		public String toHtmlString() {
			return AbstractWeightedCompositeSimilarityFunction.this.toHtmlString(this);
		}

		protected ISimpleSimilarityValue[] scaleChildSimilarityValues(final ISimpleSimilarityValue[] values,
				final ObjectType<?> ofType) throws SimilarityCalculationException, ScalingException {
			final ISimpleSimilarityValue[] scaledChildValues = new ISimpleSimilarityValue[values.length];
			for (int i = 0; i < getSimilarityFunctions().length; i++) {
				final ISimpleSimilarityFunction f = getSimilarityFunctions()[i];
				ISimpleSimilarityValue childValue = values[i];
				if (ofType == ObjectType.OBJECT_CLUSTER_PAIR && functionScalersObjectClusterPair[i] != null) {
					final ISimilarityValue childSim = childValue.calculate();
					childValue = f.value(functionScalersObjectClusterPair[i].scale(childSim), ofType);
				} else if (ofType == ObjectType.OBJECT_PAIR && functionScalersObjectPair[i] != null) {
					final ISimilarityValue childSim = childValue.calculate();
					childValue = f.value(functionScalersObjectPair[i].scale(childSim), ofType);
				} else if (ofType == ObjectType.CLUSTER_PAIR && functionScalersClusterPair[i] != null) {
					final ISimilarityValue childSim = childValue.calculate();
					childValue = f.value(functionScalersClusterPair[i].scale(childSim), ofType);
				}
				scaledChildValues[i] = childValue;
			}
			return scaledChildValues;
		}
	}

	abstract class SpecialCompositeSimilarityValue extends CompositeSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5087033032672976559L;

		public SpecialCompositeSimilarityValue() {
			super(null, null);
			this.value = Double.NaN;
			this.calculated = true;
		}

		@Override
		public ISimpleSimilarityValue[] getChildValues() {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public ISimpleSimilarityValue getChildSimilarityValue(final int i) {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public IScaler[] getFunctionScalers() {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public Double[] getFunctionWeights() {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public ICompositeSimilarityValue plus(final ICompositeSimilarityValue value)
				throws SimilarityCalculationException {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public ICompositeSimilarityValue minus(final ICompositeSimilarityValue value)
				throws SimilarityCalculationException {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public ICompositeSimilarityValue times(final ICompositeSimilarityValue value)
				throws SimilarityCalculationException {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public ICompositeSimilarityValue divideBy(final ICompositeSimilarityValue value)
				throws SimilarityCalculationException {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public ICompositeSimilarityValue times(final double value) throws SimilarityCalculationException {
			return this;
		}

		@Override
		public ICompositeSimilarityValue times(final ISimilarityValue value) throws SimilarityCalculationException {
			return this;
		}

		@Override
		public ICompositeSimilarityValue divideBy(final double value) throws SimilarityCalculationException {
			return this;
		}

		@Override
		public ICompositeSimilarityValue divideBy(final ISimilarityValue value) throws SimilarityCalculationException {
			return this;
		}

		/**
		 * Calculate as if this missing value was 0.
		 */
		@Override
		public ICompositeSimilarityValue plus(final ISimilarityValue value) throws SimilarityCalculationException {
			if (value instanceof IMissingSimilarityValue)
				return this;
			if (value instanceof ICompositeSimilarityValue)
				return (ICompositeSimilarityValue) value;
			return this.plus(value.get());
		}

		/**
		 * Calculate as if this missing value was 0.
		 */
		@Override
		public ICompositeSimilarityValue plus(final double value) throws SimilarityCalculationException {
			if (Double.isNaN(value))
				return this;

			final ISimpleSimilarityValue[] vals = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length];
			for (int i = 0; i < vals.length; i++)
				vals[i] = AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions[i].value(value, ofType);

			return AbstractWeightedCompositeSimilarityFunction.this.value(vals, this.ofType);
		}

		/**
		 * Calculate as if this missing value was 0.
		 */
		@Override
		public ICompositeSimilarityValue minus(final ISimilarityValue value) throws SimilarityCalculationException {
			if (value instanceof IMissingSimilarityValue)
				return this;
			return this.minus(value.get());
		}

		/**
		 * Calculate as if this missing value was 0.
		 */
		@Override
		public ICompositeSimilarityValue minus(final double value) throws SimilarityCalculationException {
			if (Double.isNaN(value))
				return this;

			final ISimpleSimilarityValue[] vals = new ISimpleSimilarityValue[AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions.length];
			for (int i = 0; i < vals.length; i++)
				vals[i] = AbstractWeightedCompositeSimilarityFunction.this.similarityFunctions[i].value(-value, ofType);

			return AbstractWeightedCompositeSimilarityFunction.this.value(vals, this.ofType);
		}

	}

	public class MissingCompositeSimilarityValue extends SpecialCompositeSimilarityValue
			implements IMissingSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5586363522368457410L;

		@Override
		public ICompositeSimilarityValue doCalculate() throws SimilarityCalculationException {
			return this;
		}

		@Override
		public String toString() {
			return "Missing Similarity";
		}

	}

	public class MaximalCompositeSimilarityValue extends SpecialCompositeSimilarityValue
			implements IMaximalSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1442353224551904601L;

		@Override
		public ICompositeSimilarityValue doCalculate() throws SimilarityCalculationException {
			return this;
		}

		@Override
		public String toString() {
			return "Maximal Similarity";
		}
	}

	public class MinimalCompositeSimilarityValue extends SpecialCompositeSimilarityValue
			implements IMinimalSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7793843719216912849L;

		@Override
		public ICompositeSimilarityValue doCalculate() throws SimilarityCalculationException {
			return this;
		}

		@Override
		public String toString() {
			return "Minimal Similarity";
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5317416308618671951L;

	public final MaximalCompositeSimilarityValue MAX = new MaximalCompositeSimilarityValue();
	public final MinimalCompositeSimilarityValue MIN = new MinimalCompositeSimilarityValue();

	final protected IScaler[] functionScalersObjectClusterPair, functionScalersObjectPair, functionScalersClusterPair;
	final protected Double[] functionWeights;
	protected boolean ignoreChildInfinity, ignoreChildMissing;
	protected MISSING_CHILD_HANDLING missingChildHandling;
	final protected ISimpleSimilarityFunction[] similarityFunctions;

	/**
	 * 
	 */
	public AbstractWeightedCompositeSimilarityFunction(final boolean useCache,
			final ISimpleSimilarityFunction... similarityFunctions) {
		super(useCache);

		this.similarityFunctions = new ISimpleSimilarityFunction[similarityFunctions.length];
		for (int i = 0; i < similarityFunctions.length; i++)
			if (similarityFunctions[i] != null)
				this.similarityFunctions[i] = (ISimpleSimilarityFunction) similarityFunctions[i].copy();
		this.functionScalersObjectClusterPair = new IScaler[similarityFunctions.length];
		this.functionScalersObjectPair = new IScaler[similarityFunctions.length];
		this.functionScalersClusterPair = new IScaler[similarityFunctions.length];
		this.functionWeights = new Double[similarityFunctions.length];
		this.ignoreChildInfinity = true;
		this.missingChildHandling = MISSING_CHILD_HANDLING.RETURN_MISSING;
		this.ignoreChildMissing = false;
	}

	public AbstractWeightedCompositeSimilarityFunction(final AbstractWeightedCompositeSimilarityFunction other) {
		super(other);

		this.similarityFunctions = new ISimpleSimilarityFunction[other.similarityFunctions.length];
		for (int i = 0; i < other.similarityFunctions.length; i++)
			if (other.similarityFunctions[i] != null)
				this.similarityFunctions[i] = (ISimpleSimilarityFunction) other.similarityFunctions[i].copy();
		this.functionScalersObjectClusterPair = new IScaler[similarityFunctions.length];
		this.functionScalersObjectPair = new IScaler[similarityFunctions.length];
		this.functionScalersClusterPair = new IScaler[similarityFunctions.length];
		this.functionWeights = new Double[similarityFunctions.length];

		for (int i = 0; i < other.similarityFunctions.length; i++)
			if (other.functionScalersObjectClusterPair[i] != null)
				this.functionScalersObjectClusterPair[i] = other.functionScalersObjectClusterPair[i].copy();
		for (int i = 0; i < other.similarityFunctions.length; i++)
			if (other.functionScalersObjectPair[i] != null)
				this.functionScalersObjectPair[i] = other.functionScalersObjectPair[i].copy();
		for (int i = 0; i < other.similarityFunctions.length; i++)
			if (other.functionScalersClusterPair[i] != null)
				this.functionScalersClusterPair[i] = other.functionScalersClusterPair[i].copy();
		for (int i = 0; i < other.similarityFunctions.length; i++)
			this.functionWeights[i] = other.functionWeights[i];
		this.ignoreChildInfinity = other.ignoreChildInfinity;
		this.ignoreChildMissing = other.ignoreChildMissing;
		this.missingChildHandling = other.missingChildHandling;
	}

	@Override
	public abstract AbstractWeightedCompositeSimilarityFunction copy();

	@Override
	public IScaler getScalerForObjectClusterPair(final int i) {
		return this.functionScalersObjectClusterPair[i];
	}

	@Override
	public IScaler getScalerForObjectPair(final int i) {
		return this.functionScalersObjectPair[i];
	}

	@Override
	public IScaler getScalerForClusterPair(final int i) {
		return this.functionScalersClusterPair[i];
	}

	@Override
	public ISimpleSimilarityFunction[] getSimilarityFunctions() {
		return this.similarityFunctions;
	}

	public double getWeight(final int i) {
		return this.functionWeights[i];
	}

	/**
	 * @return the ignoreChildInfinity
	 */
	public boolean isIgnoreChildInfinity() {
		return this.ignoreChildInfinity;
	}

	@Override
	public final boolean initialize() throws InterruptedException {
		if (!super.initialize())
			return false;
		return initializeChildren();
	}

	protected boolean initializeChildren() throws InterruptedException {
		boolean result = true;
		for (ISimpleSimilarityFunction sf : this.similarityFunctions)
			if (!result)
				break;
			else
				result &= sf.initialize();
		return result;
	}

	@Override
	public ICompositeSimilarityValue missingValuePlaceholder() {
		return new MissingCompositeSimilarityValue();
	}

	@Override
	public MaximalCompositeSimilarityValue maxValue() {
		return MAX;
	}

	@Override
	public MinimalCompositeSimilarityValue minValue() {
		return MIN;
	}

	/**
	 * @param ignoreChildInfinity the ignoreChildInfinity to set
	 */
	public void setIgnoreChildInfinity(final boolean ignoreChildInfinity) {
		this.ignoreChildInfinity = ignoreChildInfinity;
	}

	/**
	 * @param ignoreChildNaN the ignoreChildNaN to set
	 */
	@Override
	public void setMissingChildHandling(final MISSING_CHILD_HANDLING nanChildHandling) {
		this.missingChildHandling = nanChildHandling;
	}

	/**
	 * @param ignoreChildMissing the ignoreChildMissing to set
	 */
	public void setIgnoreChildMissing(boolean ignoreChildMissing) {
		this.ignoreChildMissing = ignoreChildMissing;
	}

	@Override
	public void setScalerForObjectClusterPair(final int i, final IScaler scaler) {
		this.functionScalersObjectClusterPair[i] = scaler;
	}

	@Override
	public void setScalerForObjectPair(final int i, final IScaler scaler) {
		this.functionScalersObjectPair[i] = scaler;
	}

	@Override
	public void setScalerForClusterPair(final int i, final IScaler scaler) {
		this.functionScalersClusterPair[i] = scaler;
	}

	@Override
	public void setScalerForAnyPair(int i, IScaler scaler) {
		this.setScalerForClusterPair(i, scaler);
		this.setScalerForObjectClusterPair(i, scaler);
		this.setScalerForObjectPair(i, scaler);
	}

	public void setWeight(final int i, final double weight) {
		if (Double.compare(weight, 0.0) < 0 || Double.compare(weight, 1.0) > 0)
			throw new IllegalArgumentException("Function weights need to be within [0,1]");
		this.functionWeights[i] = weight;
	}

	@Override
	public ICompositeSimilarityValue value(final ISimpleSimilarityValue[] values, final ObjectType<?> ofType,
			final int cardinality) {
		if (values != null && Arrays.stream(values).anyMatch(v -> v instanceof IMissingSimilarityValue)
				&& this.missingChildHandling == MISSING_CHILD_HANDLING.RETURN_MISSING)
			return missingValuePlaceholder();
		return new CompositeSimilarityValue(values, ofType, cardinality);
	}

	protected abstract double calculateFromScaledChildValues(ISimpleSimilarityValue[] scaledChildSimilarities)
			throws SimilarityCalculationException;

	protected abstract String toHtmlString(CompositeSimilarityValue value);

	@Override
	public final <O extends IObjectWithFeatures & IPair<?, ?>> ICompositeSimilarityValue calculateSimilarity(O object)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		return (ICompositeSimilarityValue) super.calculateSimilarity(object);
	}

	@Override
	protected final <O extends IObjectWithFeatures & IPair<?, ?>> ICompositeSimilarityValue doCalculateSimilarity(
			O object) throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		if (object == null)
			return missingValuePlaceholder();
		final ISimpleSimilarityValue[] values = new ISimpleSimilarityValue[getSimilarityFunctions().length];
		int count = 0;
		for (int i = 0; i < getSimilarityFunctions().length; i++) {
			final ISimpleSimilarityFunction<?> simFunc = getSimilarityFunctions()[i];
			if (simFunc.providesValuesForObjectTypes().contains(object.getObjectType())) {
				final ISimpleSimilarityValue childSim = ((ISimpleSimilarityFunction<?>) simFunc)
						.calculateSimilarity(object);
				values[i] = childSim;
				count++;
			}
		}
		if (getSimilarityFunctions().length > 0 && count == 0)
			throw new SimilarityCalculationException(
					"None of the child similarity function values could be calculated.");
		ICompositeSimilarityValue value = value(values, object.getObjectType());
//		System.out.println(object + "\t" + value.get() + "\t" + Arrays.toString(values));
		return value;
	}

}
