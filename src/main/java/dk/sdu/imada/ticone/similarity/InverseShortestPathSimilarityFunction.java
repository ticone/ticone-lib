
package dk.sdu.imada.ticone.similarity;

import java.util.Arrays;
import java.util.Collection;

import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.ClusterPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.ObjectClusterFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.ObjectClusterPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.ObjectPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.ObjectPairSimilarityFeature;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 16, 2018
 *
 */
public class InverseShortestPathSimilarityFunction
		extends AbstractSimpleSimilarityFunction<Collection<? extends INetworkMappedTimeSeriesObject>>
		implements INetworkBasedSimilarityFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4922121547293832675L;

	protected TiconeNetworkImpl network;
	private boolean isDirected, useFeatureCaches, ensureKnownObjects;

	// we store these features as fields, such that their cache is used for multiple
	// similarity calculations
	private ObjectPairFeatureShortestDistance objectPairFeatureShortestDistance;
	private ClusterPairFeatureShortestDistance clusterPairFeatureShortestPath;
	private ObjectClusterFeatureShortestDistance objectClusterFeatureShortestPath;

	public InverseShortestPathSimilarityFunction() {
		this(true, (TiconeNetworkImpl) null);
	}

	public InverseShortestPathSimilarityFunction(final boolean useCache) {
		this(useCache, (TiconeNetworkImpl) null);
	}

	public InverseShortestPathSimilarityFunction(final TiconeNetworkImpl network) {
		super(true);
		this.network = network;

		this.updateFeatures();
	}

	public InverseShortestPathSimilarityFunction(final boolean useCache, final TiconeNetworkImpl network) {
		super(useCache);
		this.network = network;

		this.updateFeatures();
	}

	public InverseShortestPathSimilarityFunction(final InverseShortestPathSimilarityFunction other) {
		super(other);
		this.network = other.network;
		this.ensureKnownObjects = other.ensureKnownObjects;
		this.isDirected = other.isDirected;
		this.clusterPairFeatureShortestPath = other.clusterPairFeatureShortestPath.copy();
		this.objectClusterFeatureShortestPath = other.objectClusterFeatureShortestPath.copy();
		this.objectPairFeatureShortestDistance = other.objectPairFeatureShortestDistance.copy();
		this.useFeatureCaches = other.useFeatureCaches;
	}

	@Override
	public boolean initialize() throws InterruptedException {
		boolean result = super.initialize();
		result &= this.network.initializeForFeature(objectPairFeatureShortestDistance);
		result &= this.network.initializeForFeature(clusterPairFeatureShortestPath);
		result &= this.network.initializeForFeature(objectClusterFeatureShortestPath);
		return result;
	}

	@Override
	public void ensureObjectIsKnown(IObjectWithFeatures object)
			throws UnknownObjectFeatureValueProviderException, IncompatibleFeatureValueProviderException {
		this.network.ensureObjectIsKnown(object);
	}

	@Override
	public boolean initializeForFeature(IFeature<?> feature) {
		if (feature instanceof ObjectPairSimilarityFeature)
			return this.network.initializeForFeature(objectPairFeatureShortestDistance);
		else if (feature instanceof ClusterPairSimilarityFeature)
			return this.network.initializeForFeature(clusterPairFeatureShortestPath);
		else if (feature instanceof ObjectClusterPairSimilarityFeature)
			return this.network.initializeForFeature(objectClusterFeatureShortestPath);
		return true;
	}

	@Override
	public InverseShortestPathSimilarityFunction copy() {
		return new InverseShortestPathSimilarityFunction(this);
	}

	protected void updateFeatures() {
		this.objectPairFeatureShortestDistance = new ObjectPairFeatureShortestDistance(this.isDirected);
		this.objectPairFeatureShortestDistance.setUseCache(this.useFeatureCaches);
		this.objectPairFeatureShortestDistance.setEnsureKnownObjects(this.ensureKnownObjects);
		this.objectClusterFeatureShortestPath = new ObjectClusterFeatureShortestDistance(this.isDirected);
		this.objectClusterFeatureShortestPath.setUseCache(this.useFeatureCaches);
		this.objectClusterFeatureShortestPath.setEnsureKnownObjects(this.ensureKnownObjects);
		this.clusterPairFeatureShortestPath = new ClusterPairFeatureShortestDistance(this.isDirected);
		this.clusterPairFeatureShortestPath.setUseCache(this.useFeatureCaches);
		this.clusterPairFeatureShortestPath.setEnsureKnownObjects(this.ensureKnownObjects);
	}

	/**
	 * @param useFeatureCaches the useFeatureCaches to set
	 */
	public void setUseFeatureCaches(final boolean useFeatureCaches) {
		this.useFeatureCaches = useFeatureCaches;
		this.updateFeatures();
	}

	/**
	 * @param ensureKnownObjects the ensureKnownObjects to set
	 */
	public void setEnsureKnownObjects(final boolean ensureKnownObjects) {
		this.ensureKnownObjects = ensureKnownObjects;
		this.updateFeatures();
	}

	/**
	 * @param isDirected the isDirected to set
	 */
	public void setDirected(final boolean isDirected) {
		this.isDirected = isDirected;
		this.updateFeatures();
	}

	/**
	 * @return the isDirected
	 */
	public boolean isDirected() {
		return this.isDirected;
	}

	@Override
	public TiconeNetworkImpl getNetwork() {
		return this.network;
	}

	/**
	 * @param network the network to set
	 */
	@Override
	public void setNetwork(ITiconeNetwork<?, ?> network) {
		this.network = (TiconeNetworkImpl) network;
	}

	@Override
	public String toString() {
		return "Inverse Shortest Network Distance";
	}

	@Override
	protected <O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue doCalculateSimilarity(O object,
			Collection<? extends INetworkMappedTimeSeriesObject>[] firstAspects,
			Collection<? extends INetworkMappedTimeSeriesObject>[] secondAspects)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		try {
			final int maxDistance = network.getNodeCount() - 1;
			if (object.getObjectType() == ObjectType.OBJECT_CLUSTER_PAIR) {
				final double value = this.network.getFeatureValue(this.objectClusterFeatureShortestPath, object)
						.getValue().doubleValue();
				if (Double.isNaN(value))
					return this.missingValuePlaceholder();
				else if (value == Double.NEGATIVE_INFINITY)
					return MAX;
				return new SimpleSimilarityValue((maxDistance - value) / maxDistance, object.getObjectType());
			} else if (object.getObjectType() == ObjectType.OBJECT_PAIR) {
				final double value = this.network.getFeatureValue(this.objectPairFeatureShortestDistance, object)
						.getValue().doubleValue();
				if (Double.isNaN(value))
					return this.missingValuePlaceholder();
				else if (value == Double.NEGATIVE_INFINITY)
					return MAX;
				return new SimpleSimilarityValue((maxDistance - value) / maxDistance, object.getObjectType());
			} else if (object.getObjectType() == ObjectType.CLUSTER_PAIR) {
				final double value = this.network.getFeatureValue(this.clusterPairFeatureShortestPath, object)
						.getValue().doubleValue();
				if (Double.isNaN(value))
					return this.missingValuePlaceholder();
				else if (value == Double.NEGATIVE_INFINITY)
					return MAX;
				return new SimpleSimilarityValue((maxDistance - value) / maxDistance, object.getObjectType());
			}
		} catch (UnknownObjectFeatureValueProviderException | FeatureCalculationException
				| IncompatibleFeatureValueProviderException | IncompatibleFeatureAndObjectException e) {
			throw new SimilarityCalculationException(e);
		}
		throw new IncompatibleObjectTypeException();
	}

	@Override
	public SimpleSimilarityValue value(final double value, final ObjectType<?> ofType, final int cardinality) {
		if (Double.isNaN(value))
			return this.missingValuePlaceholder();
		return super.value(value, ofType, cardinality);
	}

	@Override
	public Collection<? extends IFeatureValueProvider> dependsOn() {
		return Arrays.asList(network);
	}

	@Override
	public boolean updateDependency(IFeatureValueProvider previous, IFeatureValueProvider updated) {
		if (previous == this.network && updated instanceof TiconeNetworkImpl) {
			setNetwork((TiconeNetworkImpl) updated);
			return true;
		}
		return super.updateDependency(previous, updated);
	}
}
