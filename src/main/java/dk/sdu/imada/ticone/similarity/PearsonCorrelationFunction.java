
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.CenteredTimeSeriesFeature;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder.ITimeSeriesPrototypeComponent;
import dk.sdu.imada.ticone.data.MeanTimeSeriesFeature;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.VarianceTimeSeriesFeature;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.ITimePointWeighting;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.TimePointWeighting;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * This class calculates the Pearson correlation of two time series and converts
 * it to a similarity value.
 * 
 * @author Christian Wiwie
 * @author Christian Nørskov
 */
public class PearsonCorrelationFunction extends AbstractTimeSeriesSimilarityFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4508655300701488615L;

	protected boolean scale;

	private final IFeature<Double> meanF = new MeanTimeSeriesFeature(), varF = new VarianceTimeSeriesFeature();
	private final IFeature<ITimeSeries> centeredF = new CenteredTimeSeriesFeature();

	public PearsonCorrelationFunction() {
		this(true);
	}

	public PearsonCorrelationFunction(final boolean useCache) {
		this(useCache, (TimePointWeighting) null);
	}

	public PearsonCorrelationFunction(final ITimePointWeighting tpWeights) {
		this(true, tpWeights);
	}

	public PearsonCorrelationFunction(final boolean useCache, final ITimePointWeighting tpWeights) {
		super(useCache, tpWeights);
		this.meanF.setUseCache(false);
		this.varF.setUseCache(false);
		this.centeredF.setUseCache(false);
		this.normalizeByNumberTimepoints = false;
		this.scale = true;
	}

	public PearsonCorrelationFunction(final PearsonCorrelationFunction other) {
		super(other);
		this.meanF.setUseCache(false);
		this.varF.setUseCache(false);
		this.centeredF.setUseCache(false);
		this.normalizeByNumberTimepoints = other.normalizeByNumberTimepoints;
		this.scale = other.scale;
	}

	@Override
	public PearsonCorrelationFunction copy() {
		return new PearsonCorrelationFunction(this);
	}

	/**
	 * @return the scale
	 */
	public boolean isScale() {
		return this.scale;
	}

	/**
	 * @param scale the scale to set
	 */
	public void setScale(final boolean scale) {
		this.scale = scale;
	}

	@Override
	public boolean isDefinedFor(IObjectWithFeatures object) throws InterruptedException {
		if (object.getObjectType() == ObjectType.OBJECT) {
			final ITimeSeriesObject o = ObjectType.OBJECT.getBaseType().cast(object);
			for (ITimeSeries ts : o.getPreprocessedTimeSeriesList()) {
				try {
					if (Double.compare(varF.calculate(ts).getValue().doubleValue(), 0.0) != 0)
						return true;
				} catch (IncompatibleFeatureAndObjectException | FeatureCalculationException
						| IncorrectlyInitializedException e) {
				}
			}
		} else if (object.getObjectType() == ObjectType.CLUSTER) {
			final ICluster o = ObjectType.CLUSTER.getBaseType().cast(object);
			if (!(o.getPrototype().getPrototypeComponentTypes().contains(PrototypeComponentType.TIME_SERIES)))
				return false;
			try {
				ITimeSeriesPrototypeComponent component = PrototypeComponentType.TIME_SERIES
						.getComponent(o.getPrototype());
				return Double.compare(varF.calculate(component.getTimeSeries()).getValue().doubleValue(), 0.0) != 0;
			} catch (IncompatibleFeatureAndObjectException | FeatureCalculationException
					| IncorrectlyInitializedException | IncompatiblePrototypeComponentException
					| MissingPrototypeException e) {
			}
		}
		return false;
	}

	@Override
	protected <O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue doCalculateSimilarity(O object,
			ITimeSeries[] timeSeries, ITimeSeries[] otherTimeSeries)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		try {
			if (this.tpWeights != null)
				return calculateWeightedSimilarity(object, timeSeries, otherTimeSeries);
			else
				return calculateUnweightedSimilarity(object, timeSeries, otherTimeSeries);
		} catch (FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleFeatureAndObjectException e) {
			throw new SimilarityCalculationException(e);
		}
	}

	/**
	 * 
	 * @param object
	 * @param timeSeries
	 * @param otherTimeSeries
	 * @return A similarity value between 0.0 and 1.0.
	 * @throws IncompatibleObjectTypeException
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 */
	protected <O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue calculateWeightedSimilarity(O object,
			ITimeSeries[] timeSeries, ITimeSeries[] otherTimeSeries)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		try {
			double simSum = 0.0;
			for (ITimeSeries ts1 : timeSeries) {
				final double mean1 = calculateWeightedMean(ts1.asArray());
				for (ITimeSeries ts2 : otherTimeSeries) {
					final double mean2 = calculateWeightedMean(ts2.asArray());
					simSum += this.calculateWeightedTimeSeriesSimilarity(ts1, mean1, ts2, mean2, object.getObjectType())
							.calculate().get();
				}
			}
			return this.value(simSum / timeSeries.length / otherTimeSeries.length, object.getObjectType());
		} catch (FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleFeatureAndObjectException e) {
			throw new SimilarityCalculationException(e);
		}
	}

	/**
	 * 
	 * @param object
	 * @param timeSeries
	 * @param otherTimeSeries
	 * @return A similarity value between 0.0 and 1.0.
	 * @throws IncompatibleObjectTypeException
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 * @throws FeatureCalculationException
	 * @throws IncorrectlyInitializedException
	 * @throws IncompatibleFeatureAndObjectException
	 */
	protected <O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue calculateUnweightedSimilarity(
			O object, ITimeSeries[] timeSeries, ITimeSeries[] otherTimeSeries)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException,
			FeatureCalculationException, IncorrectlyInitializedException, IncompatibleFeatureAndObjectException {
		double simSum = 0.0;
		for (ITimeSeries ts1 : timeSeries) {
			final double mean1 = meanF.calculate(ts1).getValue().doubleValue();
			for (ITimeSeries ts2 : otherTimeSeries) {
				final double mean2 = meanF.calculate(ts2).getValue().doubleValue();
				simSum += this.calculateUnweightedTimeSeriesSimilarity(ts1, mean1, ts2, mean2, object.getObjectType())
						.calculate().get();
			}
		}
		return this.value(simSum / timeSeries.length / otherTimeSeries.length, object.getObjectType());
	}

	SimpleSimilarityValue calculateWeightedTimeSeriesSimilarity(final ITimeSeries data1, final ITimeSeries data2,
			final ObjectType<?> ofType) throws TimeSeriesNotCompatibleException, FeatureCalculationException,
			IncorrectlyInitializedException, InterruptedException, IncompatibleFeatureAndObjectException {
		try {
			return new SimpleSimilarityValue(this.calculateWeightedPearson(data1.asArray(),
					calculateWeightedMean(data1.asArray()), data2.asArray(), calculateWeightedMean(data2.asArray())),
					ofType);
		} catch (SimilarityFunctionNotDefinedForInputException e) {
			return missingValuePlaceholder();
		}
	}

	public SimpleSimilarityValue calculateWeightedTimeSeriesSimilarity(final ITimeSeries data1, final double mean1,
			final ITimeSeries data2, final double mean2, final ObjectType<?> ofType)
			throws TimeSeriesNotCompatibleException, FeatureCalculationException, IncorrectlyInitializedException,
			InterruptedException, IncompatibleFeatureAndObjectException {
		try {
			return new SimpleSimilarityValue(
					this.calculateWeightedPearson(data1.asArray(), mean1, data2.asArray(), mean2), ofType);
		} catch (SimilarityFunctionNotDefinedForInputException e) {
			return missingValuePlaceholder();
		}
	}

	SimpleSimilarityValue calculateUnweightedTimeSeriesSimilarity(final ITimeSeries data1, final ITimeSeries data2,
			final ObjectType<?> ofType) throws TimeSeriesNotCompatibleException, FeatureCalculationException,
			IncorrectlyInitializedException, InterruptedException, IncompatibleFeatureAndObjectException {
		try {
			return new SimpleSimilarityValue(
					this.calculateUnweightedPearson(data1, meanF.calculate(data1).getValue().doubleValue(), data2,
							meanF.calculate(data2).getValue().doubleValue()),
					ofType);
		} catch (SimilarityFunctionNotDefinedForInputException e) {
			return missingValuePlaceholder();
		}
	}

	public SimpleSimilarityValue calculateUnweightedTimeSeriesSimilarity(final ITimeSeries data1, final double mean1,
			final ITimeSeries data2, final double mean2, final ObjectType<?> ofType)
			throws TimeSeriesNotCompatibleException, FeatureCalculationException, IncorrectlyInitializedException,
			InterruptedException, IncompatibleFeatureAndObjectException {
		try {
			return new SimpleSimilarityValue(this.calculateUnweightedPearson(data1, mean1, data2, mean2), ofType);
		} catch (SimilarityFunctionNotDefinedForInputException e) {
			return missingValuePlaceholder();
		}
	}

	private double calculateWeightedPearson(final double[] data1, final double mean1, final double[] data2,
			final double mean2) throws TimeSeriesNotCompatibleException, SimilarityFunctionNotDefinedForInputException {
		double pearson = 0;

		final double varianceProduct = this.calculateWeightedVariance(data1, mean1)
				* this.calculateWeightedVariance(data2, mean2);
		if (varianceProduct != 0)
			pearson = this.calculateWeightedCovariance(data1, data2, mean1, mean2) / (Math.sqrt(varianceProduct));
		else
			throw new SimilarityFunctionNotDefinedForInputException("The variances of the inputs need to be non-zero.");

		if (this.scale)
			// For scaling to the range [0,1]
			return (pearson + 1) / 2;
		return pearson;
	}

	private double calculateUnweightedPearson(final ITimeSeries data1, final double mean1, final ITimeSeries data2,
			final double mean2)
			throws TimeSeriesNotCompatibleException, FeatureCalculationException, IncorrectlyInitializedException,
			InterruptedException, IncompatibleFeatureAndObjectException, SimilarityFunctionNotDefinedForInputException {
		double pearson = 0;

		final double varianceProduct = varF.calculate(data1).getValue().doubleValue()
				* varF.calculate(data2).getValue().doubleValue();
		if (varianceProduct != 0)
			pearson = this.calculateUnweightedCovariance(data1, mean1, data2, mean2) / (Math.sqrt(varianceProduct));
		else
			throw new SimilarityFunctionNotDefinedForInputException("The variances of the inputs need to be non-zero.");

		if (this.scale)
			// For scaling to the range [0,1]
			return (pearson + 1) / 2;
		return pearson;
	}

	/**
	 * Calculates the mean of a double array.
	 * 
	 * @param x an array.
	 * @return the mean of the array.
	 */
	private double calculateWeightedMean(final double[] x) {
		final int n = x.length;
		double mean = 0;
		double weightSum = 0;
		for (int i = 0; i < n; i++) {
			final double weight = this.getTimePointWeight(i);
			mean += weight * x[i];
			weightSum += weight;
		}
		return mean / weightSum;
	}

	private double calculateWeightedVariance(final double[] x, final double mean)
			throws TimeSeriesNotCompatibleException {
		return this.calculateWeightedCovariance(x, x, mean, mean);
	}

	/**
	 * Calculates the standard deviation of an array.
	 * 
	 * @param x    an array.
	 * @param mean the mean of the array x.
	 * @return the standard deviation of x.
	 */
	protected double calculateWeightedCovariance(final double[] x, final double[] y, final double mean1,
			final double mean2) throws TimeSeriesNotCompatibleException {
		if (x.length != y.length)
			throw new TimeSeriesNotCompatibleException();
		final int n = x.length;
		double s = 0, weightSum = 0;
		for (int i = 0; i < n; i++) {
			final double weight = this.getTimePointWeight(i);
			weightSum += weight;
			s += (x[i] - mean1) * (y[i] - mean2) * weight;
		}
		return s / weightSum;
	}

	protected double calculateUnweightedCovariance(final ITimeSeries x, final double mean1, final ITimeSeries y,
			final double mean2) throws TimeSeriesNotCompatibleException, FeatureCalculationException,
			IncorrectlyInitializedException, InterruptedException, IncompatibleFeatureAndObjectException {
		if (x.getNumberTimePoints() != y.getNumberTimePoints())
			throw new TimeSeriesNotCompatibleException();

		final double[] xArr = centeredF.calculate(x).getValue().asArray();
		final double[] yArr = centeredF.calculate(y).getValue().asArray();

		final int n = xArr.length;
		double s = 0;
		for (int i = 0; i < n; i++) {
			s += xArr[i] * yArr[i];
		}
		return s / n;
	}

	@Override
	public String toString() {
		return "Pearson Correlation";
	}

	private boolean normalizeByNumberTimepoints = true;

	/**
	 * @return the normalizeByNumberTimepoints
	 */
	@Override
	public boolean isNormalizeByNumberTimepoints() {
		return this.normalizeByNumberTimepoints;
	}

	/**
	 * @param normalizeByNumberTimepoints the normalizeByNumberTimepoints to set
	 */
	@Override
	public void setNormalizeByNumberTimepoints(boolean normalizeByNumberTimepoints) {
		this.normalizeByNumberTimepoints = normalizeByNumberTimepoints;
	}

	@Override
	public void ensureObjectIsKnown(IObjectWithFeatures object)
			throws UnknownObjectFeatureValueProviderException, IncompatibleFeatureValueProviderException {
	}

	@Override
	public boolean initializeForFeature(IFeature<?> feature) {
		return true;
	}
}
