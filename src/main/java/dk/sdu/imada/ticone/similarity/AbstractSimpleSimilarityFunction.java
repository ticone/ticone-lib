/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.ScalingException;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 15, 2018
 *
 */
public abstract class AbstractSimpleSimilarityFunction<A> extends AbstractSimilarityFunction
		implements ISimpleSimilarityFunction<A> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8393160845494559017L;
	protected IScaler functionScalerClusterPair;
	protected IScaler functionScalerObjectClusterPair;
	protected IScaler functionScalerObjectPair;

	protected final MaximalSimpleSimilarityValue MAX = new MaximalSimpleSimilarityValue();
	protected final MinimalSimpleSimilarityValue MIN = new MinimalSimpleSimilarityValue();

	public AbstractSimpleSimilarityFunction(final boolean useCache) {
		super(useCache);
	}

	public AbstractSimpleSimilarityFunction(final AbstractSimpleSimilarityFunction<A> other) {
		super(other);
	}

	@Override
	public abstract AbstractSimpleSimilarityFunction<A> copy();

	@Override
	public MissingSimpleSimilarityValue missingValuePlaceholder() {
		return new MissingSimpleSimilarityValue();
	}

	@Override
	public MaximalSimpleSimilarityValue maxValue() {
		return MAX;
	}

	@Override
	public MinimalSimpleSimilarityValue minValue() {
		return MIN;
	}

	@Override
	public SimpleSimilarityValue value(final double value, final ObjectType<?> ofType, final int cardinality) {
		if (Double.isNaN(value))
			return missingValuePlaceholder();
		return new SimpleSimilarityValue(value, ofType, cardinality);
	}

	/**
	 * A non-composite similarity value.
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Nov 15, 2018
	 *
	 */
	public class SimpleSimilarityValue extends AbstractSimilarityValue implements ISimpleSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4671393344548319987L;

		protected Double scaledValue;

		SimpleSimilarityValue(final double value, final ObjectType<?> ofType) {
			this(value, ofType, 1);
		}

		SimpleSimilarityValue(final double value, final ObjectType<?> ofType, final int cardinality) {
			super(ofType, cardinality);
			this.value = value;
		}

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof ISimilarityValue)
				return this.compareTo((ISimilarityValue) obj) == 0;
			return false;
		}

		@Override
		public int hashCode() {
			return Double.hashCode(this.value);
		}

		@Override
		protected SimpleSimilarityValue doCalculate() throws SimilarityCalculationException {
			try {
				this.scaledValue = scale();
				this.calculated = true;
				return this;
			} catch (ScalingException e) {
				throw new SimilarityCalculationException(e);
			}
		}

		@Override
		protected double doGet() {
			if (this.scaledValue != null)
				return this.scaledValue.doubleValue();
			return super.doGet();
		}

		@Override
		public ISimpleSimilarityValue plus(final double value, final int cardinality)
				throws SimilarityCalculationException {
			if (Double.isNaN(value))
				return this;
			if (Double.isNaN(this.value))
				return new SimpleSimilarityValue(value, ofType, cardinality);
			return new SimpleSimilarityValue(this.get() + value, ofType, this.cardinality + cardinality);
		}

		@Override
		public ISimpleSimilarityValue minus(final double value) throws SimilarityCalculationException {
			if (Double.isNaN(value))
				return this;
			if (Double.isNaN(this.value))
				return new SimpleSimilarityValue(-value, ofType);
			return new SimpleSimilarityValue(this.get() - value, ofType, this.cardinality - 1);
		}

		@Override
		public ISimpleSimilarityValue times(final double value) throws SimilarityCalculationException {
			return new SimpleSimilarityValue(this.get() * value, ofType);
		}

		@Override
		public ISimpleSimilarityValue divideBy(final double value) throws SimilarityCalculationException {
			return new SimpleSimilarityValue(this.get() / value, ofType);
		}

		@Override
		public ISimpleSimilarityValue divideBy(final ISimilarityValue value) throws SimilarityCalculationException {
			return (ISimpleSimilarityValue) super.divideBy(value);
		}

		@Override
		public ISimpleSimilarityValue minus(final ISimilarityValue value) throws SimilarityCalculationException {
			return (ISimpleSimilarityValue) super.minus(value);
		}

		@Override
		public ISimpleSimilarityValue plus(final ISimilarityValue value) throws SimilarityCalculationException {
			return (ISimpleSimilarityValue) super.plus(value);
		}

		@Override
		public ISimpleSimilarityValue times(final ISimilarityValue value) throws SimilarityCalculationException {
			return (ISimpleSimilarityValue) super.times(value);
		}

		protected double scale() throws SimilarityCalculationException, ScalingException {
			if (ofType == ObjectType.OBJECT_CLUSTER_PAIR && functionScalerObjectClusterPair != null) {
				return functionScalerObjectClusterPair.scale(value);
			} else if (ofType == ObjectType.OBJECT_PAIR && functionScalerObjectPair != null) {
				return functionScalerObjectPair.scale(value);
			} else if (ofType == ObjectType.CLUSTER_PAIR && functionScalerClusterPair != null) {
				return functionScalerClusterPair.scale(value);
			}
			return value;
		}
	}

	abstract class SpecialSimpleSimilarityValue extends SimpleSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = -2522833307430135571L;

		/**
		 * @param abstractSimpleSimilarityFunction
		 * @param value
		 * @param ofType
		 */
		SpecialSimpleSimilarityValue(double value, ObjectType<?> ofType) {
			super(value, ofType);
		}

		/**
		 * Calculate as if this missing value was 0.
		 */
		@Override
		public ISimpleSimilarityValue plus(final ISimilarityValue value) throws SimilarityCalculationException {
			if (value instanceof IMissingSimilarityValue || value instanceof IMaximalSimilarityValue
					|| value instanceof IMinimalSimilarityValue)
				return this;
			return this.plus(value.get());
		}

		/**
		 * Calculate as if this missing value was 0.
		 */
		@Override
		public ISimpleSimilarityValue plus(final double value) throws SimilarityCalculationException {
			if (Double.isNaN(value))
				return this;
			return AbstractSimpleSimilarityFunction.this.value(value, ofType);
		}

		/**
		 * Calculate as if this missing value was 0.
		 */
		@Override
		public ISimpleSimilarityValue minus(final ISimilarityValue value) throws SimilarityCalculationException {
			if (value instanceof IMissingSimilarityValue)
				return this;
			return this.minus(value.get());
		}

		/**
		 * Calculate as if this missing value was 0.
		 */
		@Override
		public ISimpleSimilarityValue minus(final double value) throws SimilarityCalculationException {
			if (Double.isNaN(value))
				return this;
			return AbstractSimpleSimilarityFunction.this.value(-value, ofType);
		}

		@Override
		public ISimpleSimilarityValue times(final double value) throws SimilarityCalculationException {
			return this;
		}

		@Override
		public ISimpleSimilarityValue times(final ISimilarityValue value) throws SimilarityCalculationException {
			return this;
		}

		@Override
		public ISimpleSimilarityValue divideBy(final double value) throws SimilarityCalculationException {
			return this;
		}

		@Override
		public ISimpleSimilarityValue divideBy(final ISimilarityValue value) throws SimilarityCalculationException {
			return this;
		}

	}

	public class MissingSimpleSimilarityValue extends SpecialSimpleSimilarityValue implements IMissingSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3022592069298847795L;

		/**
		 * 
		 */
		MissingSimpleSimilarityValue() {
			super(Double.NaN, null);
		}

		@Override
		public MissingSimpleSimilarityValue doCalculate() {
			return this;
		}

		@Override
		public String toString() {
			return "Missing Similarity Value";
		}

	}

	class MaximalSimpleSimilarityValue extends SpecialSimpleSimilarityValue implements IMaximalSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = 743799887738720169L;

		/**
		 * 
		 */
		private MaximalSimpleSimilarityValue() {
			super(Double.POSITIVE_INFINITY, null);
			this.calculated = true;
		}

		@Override
		protected MaximalSimpleSimilarityValue doCalculate() {
			return this;
		}

		@Override
		public String toString() {
			return "Maximal Similarity Value";
		}
	}

	public class MinimalSimpleSimilarityValue extends SpecialSimpleSimilarityValue implements IMinimalSimilarityValue {

		/**
		 * 
		 */
		private static final long serialVersionUID = -29929143546620054L;

		/**
		 * 
		 */
		public MinimalSimpleSimilarityValue() {
			super(Double.NEGATIVE_INFINITY, null);
			this.calculated = true;
		}

		@Override
		protected MinimalSimpleSimilarityValue doCalculate() {
			return this;
		}

		@Override
		public String toString() {
			return "Minimal Similarity Value";
		}
	}

	@Override
	public <O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue calculateSimilarity(O object)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		return (ISimpleSimilarityValue) super.calculateSimilarity(object);
	}

	@Override
	protected final <O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue doCalculateSimilarity(O object)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException {
		try {
			final IPair<A[], A[]> aspectPair = getAspectExtractor(object.getObjectType()).extract(object);
			return doCalculateSimilarity(object, aspectPair.getFirst(), aspectPair.getSecond());
		} catch (SimilarityAspectExtractException e) {
			throw new SimilarityCalculationException(e);
		}
	}

	/**
	 * 
	 * @param object
	 * @param firstAspects
	 * @param secondAspects
	 * @return A similarity value between 0.0 and 1.0.
	 * @throws IncompatibleObjectTypeException
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 */
	protected abstract <O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue doCalculateSimilarity(
			O object, A[] firstAspects, A[] secondAspects)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException;

	@Override
	public IScaler getScalerForClusterPair() {
		return this.functionScalerClusterPair;
	}

	@Override
	public IScaler getScalerForObjectClusterPair() {
		return this.functionScalerObjectClusterPair;
	}

	@Override
	public IScaler getScalerForObjectPair() {
		return this.functionScalerObjectPair;
	}

	@Override
	public void setScalerForAnyPair(IScaler scaler) {
		this.setScalerForClusterPair(scaler);
		this.setScalerForObjectClusterPair(scaler);
		this.setScalerForObjectPair(scaler);
	}

	@Override
	public void setScalerForClusterPair(final IScaler scaler) {
		this.functionScalerClusterPair = scaler;
	}

	@Override
	public void setScalerForObjectClusterPair(final IScaler scaler) {
		this.functionScalerObjectClusterPair = scaler;
	}

	@Override
	public void setScalerForObjectPair(final IScaler scaler) {
		this.functionScalerObjectPair = scaler;
	}

}
