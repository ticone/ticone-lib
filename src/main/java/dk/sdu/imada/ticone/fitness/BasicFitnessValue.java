/**
 * 
 */
package dk.sdu.imada.ticone.fitness;

import java.io.Serializable;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.scale.IScaler;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 11, 2019
 *
 */
public class BasicFitnessValue implements IFitnessValue, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4417148585855894432L;

	public static final BasicFitnessValue MINIMUM = new BasicFitnessValue(Double.NEGATIVE_INFINITY, null, null, null,
			null);

	protected final IArithmeticFeature<? extends Comparable<?>>[] features;
	protected final IFeatureValue<?>[] featureValues;
	protected final IScaler[] featureScaler;
	protected final double[] featureWeights;

	protected final double fitnessValue;

	public BasicFitnessValue(final double fitnessValue, final IArithmeticFeature<? extends Comparable<?>>[] features,
			final IFeatureValue<?>[] featureValues, final IScaler[] featureScaler, final double[] featureWeights) {
		super();
		this.fitnessValue = fitnessValue;
		this.features = features;
		this.featureValues = featureValues;
		this.featureScaler = featureScaler;
		this.featureWeights = featureWeights;
	}

	/**
	 * @return the features
	 */
	@Override
	public IArithmeticFeature<? extends Comparable<?>>[] getFeatures() {
		return this.features;
	}

	/**
	 * @return the featureValues
	 */
	@Override
	public IFeatureValue<?>[] getFeatureValues() {
		return this.featureValues;
	}

	/**
	 * @return the featureScaler
	 */
	@Override
	public IScaler[] getFeatureScalers() {
		return this.featureScaler;
	}

	/**
	 * @return the featureWeights
	 */
	@Override
	public double[] getFeatureWeights() {
		return this.featureWeights;
	}

	@Override
	public double getValue() {
		return this.fitnessValue;
	}

	@Override
	public String toString() {
		return Double.toString(fitnessValue);
	}
}
