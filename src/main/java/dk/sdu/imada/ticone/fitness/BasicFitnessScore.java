/**
 * 
 */
package dk.sdu.imada.ticone.fitness;

import java.util.Arrays;
import java.util.Objects;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.feature.scale.IScalerBuilder;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ScalerInitializationException;
import dk.sdu.imada.ticone.util.ScalingException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 29, 2017
 *
 */
public class BasicFitnessScore implements IFitnessScore {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5969615171225532683L;

	protected static <S extends IObjectWithFeatures> IArithmeticFeature<? extends Comparable<?>>[] copyFeatures(
			final IArithmeticFeature<? extends Comparable<?>>[] features) {
		final IArithmeticFeature<? extends Comparable<?>>[] result = new IArithmeticFeature[features.length];

		int fc = 0;
		for (final IArithmeticFeature<? extends Comparable<?>> f : features)
			result[fc++] = (IArithmeticFeature<? extends Comparable<?>>) f.copy();

		return result;
	}

	protected static <S extends IObjectWithFeatures> IScaler[] copyScalers(final IScaler[] featureScaler) {
		if (featureScaler == null)
			return null;

		return Arrays.stream(featureScaler).map(s -> s.copy()).toArray(IScaler[]::new);
	}

	protected static <S extends IObjectWithFeatures> IScalerBuilder[] copyScalerBuilders(
			final IScalerBuilder[] featureScalerBuilder) {
		if (featureScalerBuilder == null)
			return null;

		return Arrays.stream(featureScalerBuilder).map(s -> (IScalerBuilder) s.copy()).toArray(IScalerBuilder[]::new);
	}

	protected final ObjectType<?> objectType;
	protected IObjectProvider objectProvider;
	protected IArithmeticFeature<? extends Comparable<?>>[] features;
	protected IScalerBuilder[] featureScalerBuilder;
	protected IScaler[] featureScaler;
	protected double[] featureWeights;
	protected IFeatureStore featureStore;

	protected boolean smallerIsBetter;

	public BasicFitnessScore(final ObjectType<?> objectType,
			final IArithmeticFeature<? extends Comparable<?>>[] dataFeatures) {
		super();

		this.objectType = objectType;
		this.features = dataFeatures;
		this.featureScalerBuilder = null;
		this.featureWeights = null;
	}

	public BasicFitnessScore(final ObjectType<?> objectType,
			final IArithmeticFeature<? extends Comparable<?>>[] dataFeatures, final IScalerBuilder[] scalerBuilder,
			final double[] featureWeights) {
		this(objectType, dataFeatures);
		if (scalerBuilder != null
				&& (dataFeatures.length != scalerBuilder.length || scalerBuilder.length != featureWeights.length))
			throw new IllegalArgumentException("The passed arguments have incompatible lengths");

		this.featureScalerBuilder = scalerBuilder;
		this.featureWeights = featureWeights;
	}

	public BasicFitnessScore(final BasicFitnessScore other) {
		this(other.objectType, copyFeatures(other.features), copyScalerBuilders(other.featureScalerBuilder),
				other.featureWeights);
		this.featureScaler = copyScalers(other.featureScaler);
		this.objectProvider = other.objectProvider;
		this.featureStore = other.featureStore.copy();
		this.smallerIsBetter = other.smallerIsBetter;
	}

	@Override
	public BasicFitnessScore copy() {
		return new BasicFitnessScore(this);
	}

	/**
	 * @param smallerIsBetter the smallerIsBetter to set
	 */
	@Override
	public void setSmallerIsBetter(final boolean smallerIsBetter) {
		this.smallerIsBetter = smallerIsBetter;
	}

	@Override
	public int compare(final IFitnessValue o1, final IFitnessValue o2) {
		if (this.smallerIsBetter)
			return Double.compare(o2.getValue(), o1.getValue());
		else
			// larger is better
			return Double.compare(o1.getValue(), o2.getValue());
	}

	@Override
	public boolean equals(final Object obj) {
		if (!this.getClass().equals(obj.getClass()))
			return false;
		final BasicFitnessScore other = (BasicFitnessScore) obj;
		return this.features.equals(other.features);
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode() + this.features.hashCode();
	}

	@Override
	public BasicFitnessValue calculateFitness(final IObjectWithFeatures objects)
			throws FitnessScoreNotInitializedException, FitnessCalculationException {
		if (this.objectProvider == null || this.featureStore == null)
			throw new FitnessScoreNotInitializedException();
		try {
			double result = 0;
			double weightSum = 0;
			final IFeatureValue<? extends Comparable<?>>[] featureValues = new IFeatureValue[this.features.length];
			for (int f = 0; f < this.features.length; f++) {
				final IArithmeticFeature<? extends Comparable<?>> feature = this.features[f];
				final IFeatureValue<? extends Comparable<?>> v = this.featureStore.getFeatureValue(objects, feature);
				featureValues[f] = v;
				// ignore NaN values
				if (v.isNaN())
					continue;

				double doubleValue;
				if (this.featureScaler != null && this.features.length > 1)
					doubleValue = this.featureScaler[f].scale(v.toNumber());
				else
					doubleValue = v.toNumber().doubleValue();

				final double w;
				if (this.featureWeights != null)
					w = this.featureWeights[f];
				else
					w = 1.0;
				result += w * doubleValue;
				weightSum += w;
			}
			if (weightSum == 0.0)
				return BasicFitnessValue.MINIMUM;
			result /= weightSum;
			return new BasicFitnessValue(result, features, featureValues, featureScaler, featureWeights);
		} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException | ScalingException e) {
			throw new FitnessCalculationException(e);
		}
	}

	/**
	 * @param featureStore the clusterFeatureStore to set
	 * @throws FitnessScoreInitializationException
	 * @throws InterruptedException
	 * @throws NullPointerException                If either {@code objects} or
	 *                                             {@code featureStore} is null.
	 */
	@Override
	public void setObjectsAndFeatureStore(final IObjectProvider objects, final IFeatureStore featureStore)
			throws FitnessScoreInitializationException, InterruptedException {
		this.objectProvider = Objects.requireNonNull(objects);
		this.featureStore = Objects.requireNonNull(featureStore);

		try {
			this.initializeFeatureValuesAndScalers();
		} catch (ScalerInitializationException | FitnessScoreNotInitializedException | FactoryException
				| CreateInstanceFactoryException e) {
			throw new FitnessScoreInitializationException(e);
		}
	}

	/**
	 * @return the featureWeights
	 */
	@Override
	public double[] getFeatureWeights() {
		return this.featureWeights;
	}

	@Override
	public IArithmeticFeature<? extends Comparable<?>>[] getFeatures() {
		return this.features;
	}

	/**
	 * @return the featureScalerBuilder
	 */
	@Override
	public IScalerBuilder[] getFeatureScalerBuilder() {
		return this.featureScalerBuilder;
	}

	/**
	 * @return the featureValues
	 */
	@Override
	public IFeatureStore getFeatureValues() {
		return this.featureStore;
	}

	@Override
	public ObjectType<?> getObjectType() {
		return this.objectType;
	}

	protected void initializeFeatureValuesAndScalers() throws FitnessScoreNotInitializedException,
			ScalerInitializationException, FactoryException, CreateInstanceFactoryException, InterruptedException {
		if (this.objectProvider == null)
			throw new FitnessScoreNotInitializedException("objectProvider");
		if (this.featureStore == null)
			throw new FitnessScoreNotInitializedException("featureStore");
		// initialize feature scaler using the distribution of feature values
		// for the original clusters
//		final Map<IArithmeticFeature< ? extends Number>, IFeatureValueSample> featureVals = new HashMap<>();
//		for (final IArithmeticFeature< ? extends Number> f : this.getFeatures())
//			featureVals.put(f, new FeatureValueSample<>(f));
//
//		int p = 0;
//		for (final S c : this.getObjectsFromProvider()) {
//			for (final IArithmeticFeature< ? extends Number> f : this.getFeatures())
//				featureVals.get(f).setValue(c, this.featureStore.getFeatureValue(c, f));
//			p++;
//		}

		if (this.featureScalerBuilder != null && this.featureScalerBuilder.length > 1) {
			this.featureScaler = new IScaler[this.featureScalerBuilder.length];
			int s = 0;
			for (IScalerBuilder<?, ?, ?> scalerBuilder : featureScalerBuilder) {
				this.featureScaler[s++] = scalerBuilder.build();
			}
		}
	}

	@Override
	public String getName() {
		final StringBuilder sb = new StringBuilder();

		for (final IFeature<? extends Comparable<?>> f : this.features) {
			sb.append(f);
			sb.append(", ");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);

		return sb.toString();
	}

}
