package dk.sdu.imada.ticone.preprocessing;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;

public class PreprocessingSummary implements Serializable, IPreprocessingSummary<ClusterObjectMapping> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7626936205930935152L;

	protected boolean removeObjectsNotInNetwork, removeObjectsLeastConserved, removeObjectsLowVariance;
	protected boolean removeObjectsLeastConservedIsPercent, removeObjectsLowVarianceIsPercent;
	protected ISimilarityValue removeObjectsLeastConservedThreshold;
	protected double removeObjectsLowVarianceThreshold;

	protected ITimeSeriesObjects removedObjectsNotInNetwork;
	protected ITimeSeriesObjects removedObjectsLeastConserved;
	protected ITimeSeriesObjects removedObjectsLowVariance;

	protected ITiconeNetwork network;
	protected String networkName;

	private transient Set<IPreprocessingSummaryListener> listener;

	public PreprocessingSummary() {
		super();
		this.removedObjectsNotInNetwork = new TimeSeriesObjectSet();
		this.removedObjectsLeastConserved = new TimeSeriesObjectSet();
		this.removedObjectsLowVariance = new TimeSeriesObjectSet();
		this.listener = new HashSet<>();
	}

	public PreprocessingSummary(PreprocessingSummary other) {
		super();
		this.removeObjectsLeastConserved = other.removeObjectsLeastConserved;
		this.removeObjectsLeastConservedIsPercent = other.removeObjectsLeastConservedIsPercent;
		this.removeObjectsLeastConservedThreshold = other.removeObjectsLeastConservedThreshold;
		this.removeObjectsLowVariance = other.removeObjectsLowVariance;
		this.removeObjectsLowVarianceIsPercent = other.removeObjectsLowVarianceIsPercent;
		this.removeObjectsLowVarianceThreshold = other.removeObjectsLowVarianceThreshold;
		this.removeObjectsNotInNetwork = other.removeObjectsNotInNetwork;

		if (other.removedObjectsNotInNetwork != null)
			this.removedObjectsNotInNetwork = other.removedObjectsNotInNetwork.copy();
		if (other.removedObjectsLeastConserved != null)
			this.removedObjectsLeastConserved = other.removedObjectsLeastConserved.copy();
		if (other.removedObjectsLowVariance != null)
			this.removedObjectsLowVariance = other.removedObjectsLowVariance.copy();

		this.network = other.network;
		this.networkName = other.networkName;

		this.listener = new HashSet<>();
	}

	@Override
	public PreprocessingSummary copy() {
		return new PreprocessingSummary(this);
	}

	@Override
	public void setRemoveObjectsLeastConserved(final boolean removeObjectsLeastConserved) {
		this.removeObjectsLeastConserved = removeObjectsLeastConserved;
	}

	@Override
	public boolean isRemoveObjectsLeastConserved() {
		return this.removeObjectsLeastConserved;
	}

	@Override
	public void setRemoveObjectsLeastConservedThreshold(final ISimilarityValue removeObjectsLeastConservedThreshold,
			final boolean isPercent) {
		this.removeObjectsLeastConservedThreshold = removeObjectsLeastConservedThreshold;
		this.removeObjectsLeastConservedIsPercent = isPercent;
	}

	@Override
	public ISimilarityValue getRemoveObjectsLeastConservedThreshold() {
		return this.removeObjectsLeastConservedThreshold;
	}

	@Override
	public boolean isRemoveObjectsLeastConservedIsPercent() {
		return this.removeObjectsLeastConservedIsPercent;
	}

	@Override
	public void setRemoveObjectsLowVariance(final boolean removeObjectsLowVariance) {
		this.removeObjectsLowVariance = removeObjectsLowVariance;
	}

	@Override
	public boolean isRemoveObjectsLowVariance() {
		return this.removeObjectsLowVariance;
	}

	@Override
	public void setRemoveObjectsLowVarianceThreshold(final double removeObjectsLowVarianceThreshold,
			final boolean isPercent) {
		this.removeObjectsLowVarianceThreshold = removeObjectsLowVarianceThreshold;
		this.removeObjectsLowVarianceIsPercent = isPercent;
	}

	@Override
	public double getRemoveObjectsLowVarianceThreshold() {
		return this.removeObjectsLowVarianceThreshold;
	}

	@Override
	public boolean isRemoveObjectsLowVarianceIsPercent() {
		return this.removeObjectsLowVarianceIsPercent;
	}

	@Override
	public void setRemoveObjectsNotInNetwork(final boolean removeObjectsNotInNetwork) {
		this.removeObjectsNotInNetwork = removeObjectsNotInNetwork;
	}

	@Override
	public boolean isRemoveObjectsNotInNetwork() {
		return this.removeObjectsNotInNetwork;
	}

	@Override
	public void addRemovedObjectsNotInNetwork(final ITimeSeriesObjects removedObjects) {
		this.removedObjectsNotInNetwork.addAll(removedObjects);
	}

	/**
	 * @return the network
	 */
	@Override
	public ITiconeNetwork getNetwork() {
		return this.network;
	}

	@Override
	public void setNetwork(final ITiconeNetwork network) {
		this.network = network;
		this.networkName = network.getName();
	}

	@Override
	public String getNetworkName() {
		return this.networkName;
	}

	@Override
	public ITimeSeriesObjects getRemovedObjectsNotInNetwork() {
		return this.removedObjectsNotInNetwork;
	}

	@Override
	public void addRemovedObjectsLeastConserved(final ITimeSeriesObjects removedObjects) {
		this.removedObjectsLeastConserved.addAll(removedObjects);
	}

	@Override
	public ITimeSeriesObjects getRemovedObjectsLeastConserved() {
		return this.removedObjectsLeastConserved;
	}

	@Override
	public void addRemovedObjectsLowVariance(final ITimeSeriesObjects removedObjects) {
		this.removedObjectsLowVariance.addAll(removedObjects);
	}

	@Override
	public ITimeSeriesObjects getRemovedObjectsLowVariance() {
		return this.removedObjectsLowVariance;
	}

	private void readObject(final ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		// deserialize of older version: use string instead of network;
		if (this.networkName == null && this.network != null) {
			this.networkName = this.network.getName();
			this.network = null;
		}
		this.listener = new HashSet<>();
	}

	@Override
	public void addListener(final IPreprocessingSummaryListener l) {
		this.getListener().add(l);
	}

	/**
	 * @return the listener
	 */
	public Set<IPreprocessingSummaryListener> getListener() {
		if (this.listener == null)
			this.listener = new HashSet<>();
		return this.listener;
	}

	@Override
	public void removeListener(final IPreprocessingSummaryListener l) {
		this.listener.remove(l);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PreprocessingSummary) {
			PreprocessingSummary other = (PreprocessingSummary) obj;
			return Objects.equals(network, other.network) && Objects.equals(networkName, other.networkName)
//					&& Objects.equals(pvalueResult, other.pvalueResult)
					&& Objects.equals(removedObjectsLeastConserved, other.removedObjectsLeastConserved)
					&& Objects.equals(removedObjectsLowVariance, other.removedObjectsLowVariance)
					&& Objects.equals(removedObjectsNotInNetwork, other.removedObjectsNotInNetwork)
					&& Objects.equals(removeObjectsLeastConserved, other.removeObjectsLeastConserved)
					&& Objects.equals(removeObjectsLeastConservedIsPercent, other.removeObjectsLeastConservedIsPercent)
					&& Objects.equals(removeObjectsLeastConservedThreshold, other.removeObjectsLeastConservedThreshold)
					&& Objects.equals(removeObjectsLowVariance, other.removeObjectsLowVariance)
					&& Objects.equals(removeObjectsLowVarianceIsPercent, other.removeObjectsLowVarianceIsPercent)
					&& Objects.equals(removeObjectsLowVarianceThreshold, other.removeObjectsLowVarianceThreshold)
					&& Objects.equals(removeObjectsNotInNetwork, other.removeObjectsNotInNetwork);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(network, networkName, // pvalueResult,
				removedObjectsLeastConserved, removedObjectsLowVariance, removedObjectsNotInNetwork,
				removeObjectsLeastConserved, removeObjectsLeastConservedIsPercent, removeObjectsLeastConservedThreshold,
				removeObjectsLowVariance, removeObjectsLowVarianceIsPercent, removeObjectsLowVarianceThreshold,
				removeObjectsNotInNetwork);
	}

	/**
	 * 
	 */
	private void fireStateChanged() {
		final PreprocessingSummaryEvent e = new PreprocessingSummaryEvent(this);
		for (final IPreprocessingSummaryListener l : this.listener) {
			l.preprocessingSummaryChanged(e);
		}
	}
}
