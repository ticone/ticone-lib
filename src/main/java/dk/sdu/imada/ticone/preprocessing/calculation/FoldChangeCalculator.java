
package dk.sdu.imada.ticone.preprocessing.calculation;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;

/**
 * This class calculates fold change values for each time series object and
 * applies it as their exact patterns. It extends the abstract class
 * PatternCalculation, and therefore implements the function calculatePatterns.
 * It calculates the patterns on the given List of TimeSeriesData.
 *
 * @author Christian Nørskov
 * @see AbstractTimeSeriesPreprocessor
 * @see ITimeSeriesObject
 */
public class FoldChangeCalculator extends AbstractTimeSeriesPreprocessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6072704231769883292L;

	@Override
	public void process() {
		double[] fChange;
		// Find the smallest value
		double minVal = Double.MAX_VALUE;
		for (final ITimeSeriesObject tsd : super.getObjects()) {
			for (int i = 0; i < tsd.getOriginalTimeSeriesList().length; i++) {
				final ITimeSeries signal = tsd.getOriginalTimeSeriesList()[i];
				for (int j = 0; j < signal.getNumberTimePoints(); j++) {
					if (signal.get(j) > 0 && signal.get(j) < minVal) {
						minVal = signal.get(j);
					}
				}
			}
		}

		for (final ITimeSeriesObject tsd : super.getObjects()) {
			for (int j = 0; j < tsd.getOriginalTimeSeriesList().length; j++) {
				final ITimeSeries signal = tsd.getOriginalTimeSeriesList()[j];
				double[] asArray = signal.asArray();
				// Add pseudocounts to signal (to avoid division by 0)
				for (int i = 0; i < asArray.length; i++)
					asArray[i] += minVal;
				fChange = this.calculatePattern(asArray);
				tsd.addPreprocessedTimeSeries(j, new TimeSeries(fChange));
			}
		}
	}

	/**
	 * Fold Change Pattern Calculation
	 */
	private double[] calculatePattern(final double[] data) {
		final double[] fChange = new double[data.length];
		fChange[0] = 0;
		for (int i = 1; i < data.length; i++) {
			if (data[i - 1] == 0 || data[i] == 0) {
				fChange[i] = 0;
			} else {
				fChange[i] = (Math.log10(data[i] / data[i - 1]) / Math.log10(2));
			}
			if (fChange[i] < this.minValue) {
				this.minValue = fChange[i];
			}
			if (fChange[i] > this.maxValue) {
				this.maxValue = fChange[i];
			}
		}
		return fChange;
	}

}
