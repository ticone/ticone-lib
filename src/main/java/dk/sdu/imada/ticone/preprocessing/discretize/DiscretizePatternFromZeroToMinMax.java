package dk.sdu.imada.ticone.preprocessing.discretize;

import java.util.ArrayList;
import java.util.Arrays;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;

/**
 * Created by christian on 7/6/15.
 */
public class DiscretizePatternFromZeroToMinMax implements IDiscretizeTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7453771756966864303L;
	private double[] discretizationValues;
	private final double min;
	private final double max;
	private final int steps;

	public DiscretizePatternFromZeroToMinMax(final double min, final double max, final int steps) {
		this.min = min > 0.0 ? 0.0 : min;
		this.max = max < 0.0 ? 0.0 : max;
		this.steps = steps;
		this.calculateDiscretizationValues();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.discretizationValues);
		long temp;
		temp = Double.doubleToLongBits(this.max);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(this.min);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + this.steps;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiscretizePatternFromZeroToMinMax other = (DiscretizePatternFromZeroToMinMax) obj;
		if (!Arrays.equals(this.discretizationValues, other.discretizationValues))
			return false;
		if (Double.doubleToLongBits(this.max) != Double.doubleToLongBits(other.max))
			return false;
		if (Double.doubleToLongBits(this.min) != Double.doubleToLongBits(other.min))
			return false;
		if (this.steps != other.steps)
			return false;
		return true;
	}

	@Override
	public DiscretizePatternFromZeroToMinMax copy() {
		final DiscretizePatternFromZeroToMinMax d = new DiscretizePatternFromZeroToMinMax(this.min, this.max,
				this.steps);
		d.discretizationValues = this.discretizationValues;
		return d;
	}

	private void calculateDiscretizationValues() {
		ArrayList<Double> tempValues = new ArrayList<>();

		int stepsAboveZero = 0;
		int stepsBelowZero = 0;
		boolean hasZero = false;

		double stepsize = (this.max - this.min) / (this.steps);
		for (int i = 0; i < this.steps + 1; i++) {
			final double value = this.min + stepsize * i;
			tempValues.add(value);
			if (value < 0.0) {
				stepsBelowZero++;
			} else if (value > 0.0) {
				stepsAboveZero++;
			} else {
				hasZero = true;
			}
		}

		// If the none of the values are zero, compute new values from min->0
		// and 0->max.
		if (!hasZero) {
			tempValues = new ArrayList<>();
			if (stepsBelowZero > 0) {
				stepsize = (0 - this.min) / stepsBelowZero;
				for (int i = 0; i < stepsBelowZero; i++) {
					tempValues.add(this.min + stepsize * i);
				}
			}
			if (stepsAboveZero > 0) {
				stepsize = (this.max) / stepsAboveZero;
				for (int i = 0; i < stepsAboveZero + 1; i++) {
					tempValues.add(0 + stepsize * i);
				}
			}
		}

		this.discretizationValues = new double[tempValues.size()];
		for (int i = 0; i < tempValues.size(); i++) {
			this.discretizationValues[i] = tempValues.get(i);
		}

	}

	@Override
	public double[] getDiscretizationValues() {
		return this.discretizationValues;
	}

	@Override
	public TimeSeries discretizeObjectTimeSeries(final ITimeSeries component) {
		final double[] pattern = component.asArray();
		double minDistance, distance;
		double newValue = 0;
		for (int i = 0; i < pattern.length; i++) {
			minDistance = Integer.MAX_VALUE;
			for (int j = 0; j < this.discretizationValues.length; j++) {
				distance = Math.abs(pattern[i] - this.discretizationValues[j]);
				if (minDistance > distance) {
					minDistance = distance;
					newValue = this.discretizationValues[j];
				}
			}
			pattern[i] = newValue;
		}
		return new TimeSeries(pattern);
	}

}
