package dk.sdu.imada.ticone.preprocessing.filter;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.filter.IDataFilter;
import dk.sdu.imada.ticone.util.ExtractData;
import dk.sdu.imada.ticone.variance.IObjectSetVariance;

public class LeastVaryingObjectSetsThresholdFilter implements IDataFilter {

	protected double threshold;
	protected IObjectSetVariance varianceFunction;

	public LeastVaryingObjectSetsThresholdFilter(final double threshold, final IObjectSetVariance varianceFunction) {
		super();
		this.threshold = threshold;
		this.varianceFunction = varianceFunction;
	}

	@Override
	public ITimeSeriesObjects filterObjectSets(final ITimeSeriesObjects objectSets) {
		final ITimeSeriesObjects result = new TimeSeriesObjectList(objectSets);
		result.removeAll(
				ExtractData.findObjectsWithVarianceThreshold(objectSets, this.threshold, this.varianceFunction));
		return result;
	}
}
