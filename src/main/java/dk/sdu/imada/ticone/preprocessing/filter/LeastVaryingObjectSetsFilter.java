package dk.sdu.imada.ticone.preprocessing.filter;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.filter.IDataFilter;
import dk.sdu.imada.ticone.util.ExtractData;
import dk.sdu.imada.ticone.variance.IObjectSetVariance;

public class LeastVaryingObjectSetsFilter implements IDataFilter {

	protected int percent;
	protected IObjectSetVariance varianceFunction;

	public LeastVaryingObjectSetsFilter(final int percent, final IObjectSetVariance varianceFunction) {
		super();
		this.percent = percent;
		this.varianceFunction = varianceFunction;
	}

	@Override
	public ITimeSeriesObjects filterObjectSets(final ITimeSeriesObjects objectSets) {
		final ITimeSeriesObjects result = new TimeSeriesObjectList(objectSets);
		result.removeAll(ExtractData.findLeastVaryingObjects(objectSets.asList(), this.percent, this.varianceFunction));
		return result;
	}
}
