package dk.sdu.imada.ticone.preprocessing.discretize;

import java.util.ArrayList;
import java.util.Arrays;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;

/**
 * Created by christian on 7/6/15.
 */
public class DiscretizePatternMinToMaxWithZero implements IDiscretizeTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2152677010228590695L;
	private final double[] discretizationValues;

	DiscretizePatternMinToMaxWithZero(final double[] discretizationValues) {
		super();
		this.discretizationValues = discretizationValues;
	}

	public DiscretizePatternMinToMaxWithZero(double min, double max, final int steps) {
		final ArrayList<Double> tempValues = new ArrayList<>();

		min = min > 0.0 ? 0.0 : min;
		max = max < 0.0 ? 0.0 : max;

		final double stepsize = (max - min) / (steps);
		for (int i = 0; i < steps + 1; i++) {
			tempValues.add(min + stepsize * i);
		}
		for (int i = 1; i < tempValues.size(); i++) {
			if (tempValues.get(i - 1) < 0.0 && tempValues.get(i) > 0.0) {
				tempValues.add(i, 0.0);
			}
		}
		this.discretizationValues = new double[tempValues.size()];
		for (int i = 0; i < tempValues.size(); i++) {
			this.discretizationValues[i] = tempValues.get(i);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.discretizationValues);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiscretizePatternMinToMaxWithZero other = (DiscretizePatternMinToMaxWithZero) obj;
		if (!Arrays.equals(this.discretizationValues, other.discretizationValues))
			return false;
		return true;
	}

	@Override
	public DiscretizePatternMinToMaxWithZero copy() {
		return new DiscretizePatternMinToMaxWithZero(this.discretizationValues);
	}

	@Override
	public TimeSeries discretizeObjectTimeSeries(final ITimeSeries component) {
		final double[] pattern = component.asArray();
		double minDistance, distance;
		double newValue = 0;
		for (int i = 0; i < pattern.length; i++) {
			minDistance = Integer.MAX_VALUE;
			for (int j = 0; j < this.discretizationValues.length; j++) {
				distance = Math.abs(pattern[i] - this.discretizationValues[j]);
				if (minDistance > distance) {
					minDistance = distance;
					newValue = this.discretizationValues[j];
				}
			}
			pattern[i] = newValue;
		}
		return new TimeSeries(pattern);
	}

	@Override
	public double[] getDiscretizationValues() {
		return this.discretizationValues;
	}

}
