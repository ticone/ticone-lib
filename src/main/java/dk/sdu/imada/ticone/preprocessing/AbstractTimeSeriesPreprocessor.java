
package dk.sdu.imada.ticone.preprocessing;

import java.io.Serializable;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.MaxValueTimeSeriesFeature;
import dk.sdu.imada.ticone.data.MinValueTimeSeriesFeature;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IFeature;

public abstract class AbstractTimeSeriesPreprocessor implements Serializable, ITimeSeriesPreprocessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2509511842014413078L;
	public ITimeSeriesObjectList objects;
	public double minValue;
	public double maxValue;

	protected IFeature<Double> minF, maxF;

	public AbstractTimeSeriesPreprocessor() {
		super();
		this.minF = new MinValueTimeSeriesFeature();
		this.minF.setUseCache(false);
		this.maxF = new MaxValueTimeSeriesFeature();
		this.maxF.setUseCache(false);
	}

	@Override
	public void initializeObjects(final ITimeSeriesObjectList objects) {
		this.objects = objects;
		this.minValue = Double.POSITIVE_INFINITY;
		this.maxValue = Double.NEGATIVE_INFINITY;
	}

	@Override
	public void initializeObjects(ITimeSeriesObject... objects) {
		this.initializeObjects(new TimeSeriesObjectList(objects));
	}

	@Override
	public ITimeSeriesObjectList getObjects() {
		return this.objects;
	}

	@Override
	public double getMinValue() {
		return this.minValue;
	}

	@Override
	public double getMaxValue() {
		return this.maxValue;
	}

}
