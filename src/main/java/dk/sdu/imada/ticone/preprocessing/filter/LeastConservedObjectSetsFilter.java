package dk.sdu.imada.ticone.preprocessing.filter;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.filter.FilterException;
import dk.sdu.imada.ticone.data.filter.IDataFilter;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.util.ExtractData;

public class LeastConservedObjectSetsFilter implements IDataFilter {

	protected int percent;
	protected ISimilarityFunction similarityFunction;

	public LeastConservedObjectSetsFilter(final int percent, final ISimilarityFunction similarityFunction) {
		super();
		this.percent = percent;
		this.similarityFunction = similarityFunction;
	}

	@Override
	public ITimeSeriesObjects filterObjectSets(final ITimeSeriesObjects objectSets)
			throws FilterException, InterruptedException {
		final ITimeSeriesObjects result = new TimeSeriesObjectList(objectSets);
		try {
			result.removeAll(
					ExtractData.findLeastConservedObjects(objectSets.asList(), this.percent, this.similarityFunction));
		} catch (final SimilarityCalculationException | IncompatibleObjectTypeException e) {
			throw new FilterException(e);
		}
		return result;
	}
}
