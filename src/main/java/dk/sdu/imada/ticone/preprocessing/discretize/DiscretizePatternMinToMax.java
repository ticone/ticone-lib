package dk.sdu.imada.ticone.preprocessing.discretize;

import java.util.Arrays;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;

/**
 * Created by christian on 7/6/15.
 */
public class DiscretizePatternMinToMax implements IDiscretizeTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8201956397214615899L;
	double[] discretizationValues;

	DiscretizePatternMinToMax(final double[] discretizationValues) {
		super();
		this.discretizationValues = discretizationValues;
	}

	public DiscretizePatternMinToMax(final double min, final double max, final int steps) {
		this.discretizationValues = new double[steps + 1];
		final double stepsize = (max - min) / (steps);
		for (int i = 0; i < steps + 1; i++) {
			this.discretizationValues[i] = min + stepsize * i;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.discretizationValues);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiscretizePatternMinToMax other = (DiscretizePatternMinToMax) obj;
		if (!Arrays.equals(this.discretizationValues, other.discretizationValues))
			return false;
		return true;
	}

	@Override
	public DiscretizePatternMinToMax copy() {
		return new DiscretizePatternMinToMax(this.discretizationValues);
	}

	@Override
	public TimeSeries discretizeObjectTimeSeries(final ITimeSeries component) {
		final double[] pattern = component.asArray();
		double minDistance, distance;
		double newValue = 0;
		for (int i = 0; i < pattern.length; i++) {
			minDistance = Integer.MAX_VALUE;
			for (int j = 0; j < this.discretizationValues.length; j++) {
				distance = Math.abs(pattern[i] - this.discretizationValues[j]);
				if (minDistance > distance) {
					minDistance = distance;
					newValue = this.discretizationValues[j];
				}
			}
			pattern[i] = newValue;
		}
		return new TimeSeries(pattern);
	}

	@Override
	public double[] getDiscretizationValues() {
		return this.discretizationValues;
	}

}
