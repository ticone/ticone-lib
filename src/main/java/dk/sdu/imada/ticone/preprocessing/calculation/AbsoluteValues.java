package dk.sdu.imada.ticone.preprocessing.calculation;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * Created by christian on 14-12-15.
 */
public class AbsoluteValues extends AbstractTimeSeriesPreprocessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8644896695537895515L;

	@Override
	public void process() throws PreprocessingException, InterruptedException {
		try {
			this.minValue = Double.POSITIVE_INFINITY;
			this.maxValue = Double.NEGATIVE_INFINITY;
			for (final ITimeSeriesObject tsd : super.getObjects()) {
				final ITimeSeries[] signalList = tsd.getOriginalTimeSeriesList();
				for (int i = 0; i < signalList.length; i++) {
					final ITimeSeries signal = signalList[i];
					if (signal.getNumberTimePoints() == 0)
						return;

					final double min = minF.calculate(signal).getValue().doubleValue();
					final double max = maxF.calculate(signal).getValue().doubleValue();

					if (min < this.minValue)
						this.minValue = min;
					if (max > this.maxValue)
						this.maxValue = max;

					tsd.addPreprocessedTimeSeries(i, new TimeSeries(signal));
				}
			}
		} catch (IncompatibleFeatureAndObjectException | FeatureCalculationException
				| IncorrectlyInitializedException e) {
			throw new PreprocessingException(e);
		}
	}
}
