
package dk.sdu.imada.ticone.preprocessing.calculation;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * This class normalizes the values for each time series object and applies it
 * as their exact patterns. It extends the abstract class PatternCalculation,
 * and therefore implements the function calculatePatterns. It calculates the
 * patterns on the given List of TimeSeriesData.
 *
 * @author Christian Nørskov
 * @see AbstractTimeSeriesPreprocessor
 * @see ITimeSeriesObject
 */
public class TimeSeriesMinMaxNormalizer extends AbstractTimeSeriesPreprocessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5942497505391412961L;

	// TODO: Fix min and max value calculation
	@Override
	public void process() throws PreprocessingException {
		try {
			this.minValue = 0;
			this.maxValue = 1;
			double minVal = Double.POSITIVE_INFINITY;

			double maxValueInDataset = Double.NEGATIVE_INFINITY;
			double minValueInDataset = Double.POSITIVE_INFINITY;
			// Find minimum value of signals, for pseudo counts, and maximum value
			// of normalizing.
			for (final ITimeSeriesObject tsd : super.getObjects()) {
				for (final ITimeSeries ts : tsd.getOriginalTimeSeriesList()) {
					final double min = minF.calculate(ts).getValue().doubleValue();
					final double max = maxF.calculate(ts).getValue().doubleValue();
					if (min < minVal)
						minVal = min;
					if (max > maxValueInDataset)
						maxValueInDataset = max;
					if (min < minValueInDataset && min < 0)
						minValueInDataset = min;
				}
			}
			if (minValueInDataset > maxValueInDataset)
				minValueInDataset = 0;

			// Normalize all data
			for (int i = 0; i < super.getObjects().size(); i++) {
				super.getObjects().get(i).normalize(maxValueInDataset, minValueInDataset);
			}
		} catch (IncompatibleFeatureAndObjectException | FeatureCalculationException | IncorrectlyInitializedException
				| InterruptedException e) {
			throw new PreprocessingException(e);
		}
	}

}
