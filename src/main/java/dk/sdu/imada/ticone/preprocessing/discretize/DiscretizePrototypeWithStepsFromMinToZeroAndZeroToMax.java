package dk.sdu.imada.ticone.preprocessing.discretize;

import java.util.Arrays;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.TimeSeries;

/**
 * This class is used to discretize the values for each time points for
 * patterns.
 * <p>
 * It is given a minimum and maximum value, and how many steps there should be
 * below zero, and how many steps there should be above zero.
 * </p>
 *
 * <p>
 * It uses the min value and the negative steps to calculate each discretized
 * value below zero, and likewise with the values above zero, but here it uses
 * the max value and the positive steps.
 * </p>
 *
 * @author Christian Nørskov
 * @see IDiscretizeTimeSeries
 */
public class DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax implements IDiscretizeTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5829949700926392351L;
	private final double min;
	private final double max;
	private final int negativeSteps;
	private final int positiveSteps;
	private double[] discretizationValues;

	/**
	 * Initialize the object and calculate all discretized values.
	 * 
	 * @param min           the minimum value a pattern can have.
	 * @param max           the maxumum value a pattern can have.
	 * @param negativeSteps the number of steps below zero (going from min to 0)
	 * @param positiveSteps the number of steps above zero (going form 0 to max)
	 */
	public DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(final double min, final double max,
			final int negativeSteps, final int positiveSteps) {
		this.min = min;
		this.max = max;
		this.negativeSteps = negativeSteps;
		this.positiveSteps = positiveSteps;
		this.calculateDiscretizationSteps();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.discretizationValues);
		long temp;
		temp = Double.doubleToLongBits(this.max);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(this.min);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + this.negativeSteps;
		result = prime * result + this.positiveSteps;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax other = (DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax) obj;
		if (!Arrays.equals(this.discretizationValues, other.discretizationValues))
			return false;
		if (Double.doubleToLongBits(this.max) != Double.doubleToLongBits(other.max))
			return false;
		if (Double.doubleToLongBits(this.min) != Double.doubleToLongBits(other.min))
			return false;
		if (this.negativeSteps != other.negativeSteps)
			return false;
		if (this.positiveSteps != other.positiveSteps)
			return false;
		return true;
	}

	@Override
	public DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax copy() {
		final DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax d = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
				this.min, this.max, this.negativeSteps, this.positiveSteps);
		d.discretizationValues = this.discretizationValues.clone();
		return d;
	}

	private void calculateDiscretizationSteps() {
		int totalSteps = 1; // 0 should be counted as well
		totalSteps += this.min < 0 ? this.negativeSteps : 0;
		totalSteps += this.max > 0 ? this.positiveSteps : 0;
		this.discretizationValues = new double[totalSteps];
		int index = 0;
		if (this.min < 0) {
			for (int i = 0; i < this.negativeSteps; i++) {
				this.discretizationValues[index] = this.min + (Math.abs(this.min) / this.negativeSteps) * i;
				index++;
			}
		}
		this.discretizationValues[index] = 0;
		index++;
		if (this.max > 0) {
			for (int i = 0; i < this.positiveSteps; i++) {
				this.discretizationValues[index] = 0 + (this.max / this.positiveSteps) * (i + 1);
				index++;
			}
		}
	}

	/**
	 * returns the array of possible discretized values.
	 * 
	 * @return returns the double array of possible discretized values.
	 */
	@Override
	public double[] getDiscretizationValues() {
		return this.discretizationValues;
	}

	/**
	 * Discretizes the given double array, by assigning each value to the nearest
	 * discretized value.
	 * 
	 * @param object the double array that should be discretized
	 * @return the discretized double array
	 */
	@Override
	public TimeSeries discretizeObjectTimeSeries(final ITimeSeries component) {
		final double[] pattern = component.asArray();
		double minDistance, distance;
		double newValue = 0;
		for (int i = 0; i < pattern.length; i++) {
			minDistance = Double.MAX_VALUE;
			for (int j = 0; j < this.discretizationValues.length; j++) {
				distance = Math.abs(pattern[i] - this.discretizationValues[j]);
				if (minDistance > distance) {
					minDistance = distance;
					newValue = this.discretizationValues[j];
				}
			}
			pattern[i] = newValue;
		}
		return new TimeSeries(pattern);
	}

	public int getNegativeSteps() {
		return this.negativeSteps;
	}

	public int getPositiveSteps() {
		return this.positiveSteps;
	}
}
