package dk.sdu.imada.ticone.comparison;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping.ClusterObjectMappingCopy;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.ClusterObjectMappingPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureCommonObjectsOverPrototypeSimilarity;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePercentageCommonObjects;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePvalue;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.statistics.BasicCalculatePValues;
import dk.sdu.imada.ticone.statistics.MultiplyPValues;
import dk.sdu.imada.ticone.statistics.PValueCalculationException;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult;
import dk.sdu.imada.ticone.statistics.PermutationTestChangeEvent;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.TiconeTask;

/**
 * 
 * @author Christian Wiwie
 *
 */
public class TiconeComparisonTask extends TiconeTask {

	protected IShuffle shuffleClusteringPair;
	protected BasicCalculatePValues calculatePvalues;
	protected IFeatureStore clusterPairFeatureStore;
	protected ISimilarityFunction similarityFunction;
	protected int permutations;
	protected int clustering1Iteration, clustering2Iteration;
	protected TiconeClusteringResult clustering1, clustering2;
	protected ClusterObjectMappingCopy pom1, pom2;

	public TiconeComparisonTask(final TiconeClusteringResult clustering1, final TiconeClusteringResult clustering2,
			final IShuffle shuffleClusteringPair, final int clustering1Iteration, final int clustering2Iteration,
			final IClusters clustering1clusters, final IClusters clustering2clusters,
			final ISimilarityFunction similarity, final int permutations) throws InterruptedException {
		super();
		this.clustering1Iteration = clustering1Iteration;
		this.clustering2Iteration = clustering2Iteration;

		this.clustering1 = clustering1;
		this.clustering2 = clustering2;

		IClusterHistory<ClusterObjectMapping> tmp = this.clustering1.getClusterHistory();
		while (tmp.getIterationNumber() > this.clustering1Iteration) {
			tmp = tmp.getParent();
		}
		this.pom1 = tmp.getClusterObjectMapping().getCopy(true);

		for (final ICluster c : new HashSet<>(this.pom1.getCopy().getClusters()))
			if (!clustering1clusters.contains(this.pom1.getOriginal(c)))
				this.pom1.getCopy().removeData(c.getClusterNumber(),
						dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);

		tmp = this.clustering2.getClusterHistory();
		while (tmp.getIterationNumber() > this.clustering2Iteration) {
			tmp = tmp.getParent();
		}
		this.pom2 = tmp.getClusterObjectMapping().getCopy(true);

		for (final ICluster c : new HashSet<>(this.pom2.getCopy().getClusters()))
			if (!clustering2clusters.contains(this.pom2.getOriginal(c)))
				this.pom2.getCopy().removeData(c.getClusterNumber(),
						dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);

		this.similarityFunction = similarity;
		this.permutations = permutations;

		this.shuffleClusteringPair = shuffleClusteringPair;
	}

//	protected void calculatePairwiseSimilaritiesOfClusters(final IClusterObjectMapping pom1,
//			final IClusterObjectMapping pom2, final ISimilarity similarityFunction,
//			final Map<Pair<ICluster, ICluster>, Integer> commonObjects,
//			final Map<Pair<ICluster, ICluster>, Double> prototypeSimilarity) throws ClusteringComparisonException {
//		Map<ICluster, ITimeSeriesObjects> clusters1 = new HashMap<ICluster, ITimeSeriesObjects>();
//		Map<ICluster, ITimeSeriesObjects> clusters2 = new HashMap<ICluster, ITimeSeriesObjects>();
//
//		for (ICluster p : pom1.clusterSet()) {
//			clusters1.put(p, new HashSet<ITimeSeriesObject>());
//			for (ITimeSeriesObject data : pom1.getClusterObjects(p)) {
//				clusters1.get(p).add(data);
//			}
//		}
//		for (ICluster p : pom2.clusterSet()) {
//			clusters2.put(p, new HashSet<ITimeSeriesObject>());
//			for (ITimeSeriesObject data : pom2.getClusterObjects(p)) {
//				clusters2.get(p).add(data);
//			}
//		}
//
//		for (ICluster p1 : clusters1.keySet()) {
//			Set<String> common1 = new HashSet<String>();
//			for (ITimeSeriesObject tsd : clusters1.get(p1))
//				common1.add(tsd.getName());
//
//			for (ICluster p2 : clusters2.keySet()) {
//				ITimeSeriesObjects common = new HashSet<ITimeSeriesObject>();
//				for (ITimeSeriesObject tsd : clusters2.get(p2))
//					if (common1.contains(tsd.getName()))
//						common.add(tsd);
//
//				double protoSim;
//				try {
//					protoSim = similarityFunction.calculateDataSimilarity(p1.getPrototype(), p2.getPrototype());
//				} catch (TimeSeriesNotCompatibleException e) {
//					throw new ClusteringComparisonException(e);
//				}
//
//				commonObjects.put(Pair.of(p1, p2), common.size());
//				prototypeSimilarity.put(Pair.of(p1, p2), protoSim);
//			}
//		}
//
//	}

	protected ClusteringComparisonResult<TiconeClusteringResult> calculateClusterComparisonMatrix()
			throws InterruptedException, ClusteringComparisonException {

		try {
			// fireProgress("Calculating p-values", null, null);
			// calculate p-values

			this.fireProgress("Calculating properties of clusters", null, null);
			final ClusterObjectMappingPair clusteringPair = new ClusterObjectMappingPair(this.pom1.getCopy(),
					this.pom2.getCopy());

			this.clusterPairFeatureStore = new BasicFeatureStore();

			final List<IFitnessScore> fitnessScores = new ArrayList<>();
			final List<Boolean> fitnessScoreTwoSided = new ArrayList<>();

			final IArithmeticFeature<Double> f = new ClusterPairFeatureCommonObjectsOverPrototypeSimilarity(
					this.similarityFunction);

			logger.info("Calculating original fitness scores ...");
			final IArithmeticFeature<? extends Comparable<?>>[] asList = new IArithmeticFeature[] { f };

			// fill the cluster feature store
			for (final IClusterPair c : clusteringPair.getClusterPairs()) {
				this.clusterPairFeatureStore.setFeatureValue(c, f, f.calculate(c));
			}

			final IFitnessScore fitnessScore = new BasicFitnessScore(ObjectType.CLUSTER_PAIR, asList, null,
					new double[] { 1.0 });
			fitnessScores.add(fitnessScore);
			fitnessScoreTwoSided.add(false);
			logger.info(" done");

			this.calculatePvalues = new BasicCalculatePValues(Arrays.asList(ObjectType.CLUSTER_PAIR),
					this.shuffleClusteringPair, fitnessScores, new List[] { new ArrayList<>() }, fitnessScoreTwoSided,
					new MultiplyPValues(), this.permutations);
			this.calculatePvalues.setFeatureStore(this.clusterPairFeatureStore);

			this.calculatePvalues.addChangeListener(new ChangeListener() {

				@Override
				public void stateChanged(final ChangeEvent e) {
					if (e instanceof PermutationTestChangeEvent) {
						final PermutationTestChangeEvent ev = (PermutationTestChangeEvent) e;
						try {
							TiconeComparisonTask.this
									.fireProgress("",
											"Calculating p-values for clusters: Permutation "
													+ ev.getFinishedPermutations() + " of " + ev.getTotalPermutations(),
											ev.getPercentage());
						} finally {
							logger.info("{}", ev.getPercentage());
						}
					}
				}
			});

			final long seed = this.clustering1.getSeed();

			PValueCalculationResult pvalues;
			pvalues = this.calculatePvalues.calculatePValues(clusteringPair, seed);

			final ClusterPairFeaturePercentageCommonObjects cpfPco = new ClusterPairFeaturePercentageCommonObjects();
			final ClusterPairFeaturePvalue cfPvalue = new ClusterPairFeaturePvalue();
			cfPvalue.setFeatureValueProvider(pvalues);
			for (final IObjectWithFeatures p : pvalues.getObjects(ObjectType.CLUSTER_PAIR)) {
				clusterPairFeatureStore.setFeatureValue(p, cfPvalue, cfPvalue.calculate(p));
				clusterPairFeatureStore.setFeatureValue(p, cpfPco, cpfPco.calculate(p));
			}

			return new ClusteringComparisonResult<>(this.clustering1, this.clustering2, this.clustering1Iteration,
					this.clustering2Iteration, this.pom1.getCopy(), this.pom2.getCopy(), this.similarityFunction,
					this.permutations, pvalues);
		} catch (final PValueCalculationException | IncompatibleFeatureAndObjectException | FeatureCalculationException
				| IncorrectlyInitializedException | IncompatibleFeatureValueProviderException e1) {
			throw new ClusteringComparisonException(e1);
		}
	}

	@Override
	public void cancel() {
		super.cancel();
		this.calculatePvalues.cancel();
	}
};