package dk.sdu.imada.ticone.comparison;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import dk.sdu.imada.ticone.clustering.AbstractNamedTiconeResult;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.filter.BasicFilter;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.clustering.pair.ClusterObjectMappingPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterObjectMappingPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult;
import dk.sdu.imada.ticone.util.ClusterPairStatusMapping;

public class ClusteringComparisonResult<CLUSTERING_RESULT extends TiconeClusteringResult>
		extends AbstractNamedTiconeResult {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2921248750060426358L;
	/**
	 * 
	 */
	protected int comparisonNumber;
	protected CLUSTERING_RESULT clustering1, clustering2;
	protected int clustering1iteration, clustering2iteration;
	protected ClusterObjectMapping pom1, pom2;
	protected ISimilarityFunction similarityFunction;
	protected int permutations;
	protected PValueCalculationResult pvalues;
	protected IFilter<IClusterPair> filter;
	protected ClusterPairStatusMapping clusterPairStatusMapping;

	protected static int nextComparisonNumber = 1;

	public ClusteringComparisonResult(final CLUSTERING_RESULT clustering1, final CLUSTERING_RESULT clustering2,
			final int clustering1iteration, final int clustering2iteration, final ClusterObjectMapping pom1,
			final ClusterObjectMapping pom2, final ISimilarityFunction similarityFunction, final int permutations,
			final PValueCalculationResult pvalues) {
		super();
		this.clustering1 = clustering1;
		this.clustering2 = clustering2;
		this.clustering1iteration = clustering1iteration;
		this.clustering2iteration = clustering2iteration;
		this.pom1 = pom1;
		this.pom2 = pom2;
		this.similarityFunction = similarityFunction;
		this.permutations = permutations;
		this.pvalues = pvalues;
		this.comparisonNumber = nextComparisonNumber++;
		this.setDate(new Date());
		this.filter = new BasicFilter<>();
		this.setupClusterPairStatusMapping();
	}

	public IFeatureStore getFeatureStore() {
		return this.pvalues.getFeatureStore();
	}

	public Set<String> calculateCommonObjects(final IClusterPair clusterPair) {
		final ITimeSeriesObjects objects1 = clusterPair.getFirst().getObjects(),
				objects2 = clusterPair.getSecond().getObjects();
		final Set<String> objectIds1 = new HashSet<>(), objectIds2 = new HashSet<>();
		for (final ITimeSeriesObject obj : objects1) {
			objectIds1.add(obj.getName());
		}
		for (final ITimeSeriesObject obj : objects2) {
			objectIds2.add(obj.getName());
		}
		objectIds1.retainAll(objectIds2);

		return objectIds1;
	}

	public CLUSTERING_RESULT getClustering1() {
		return this.clustering1;
	}

	public CLUSTERING_RESULT getClustering2() {
		return this.clustering2;
	}

	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	public PValueCalculationResult getPvalues() {
		return this.pvalues;
	}

	public static void setNextComparisonNumber(final int nextComparisonNumber) {
		ClusteringComparisonResult.nextComparisonNumber = nextComparisonNumber;
	}

	public static int getNextComparisonNumber() {
		return nextComparisonNumber;
	}

	public int getComparisonNumber() {
		return this.comparisonNumber;
	}

	@Override
	protected void initName() {
		this.name = String.format("Comparison %d", this.comparisonNumber);
	}

	public int getClustering1iteration() {
		return this.clustering1iteration;
	}

	public int getClustering2iteration() {
		return this.clustering2iteration;
	}

	public IClusterObjectMapping getPom1() {
		return this.pom1;
	}

	public IClusterObjectMapping getPom2() {
		return this.pom2;
	}

	public int getPermutations() {
		return this.permutations == 0 ? 1000 : this.permutations;
	}

	@Override
	public void destroy() {
		super.destroy();
	}

	/**
	 * @return the filter
	 */
	public IFilter<IClusterPair> getFilter() {
		return this.filter;
	}

	/**
	 * @return the clusterPairStatusMapping
	 */
	public ClusterPairStatusMapping getClusterPairStatusMapping() {
		return this.clusterPairStatusMapping;
	}

	public void setupClusterPairStatusMapping() {
		final IClusterObjectMappingPair mapping = new ClusterObjectMappingPair(pom1, pom2);
		final ClusterPairStatusMapping statusMapping = new ClusterPairStatusMapping(this.getFeatureStore());
		for (final IClusterPair pair : mapping.getClusterPairs()) {
			statusMapping.add(pair, false, false, false);
		}
		this.setClusterPairStatusMapping(statusMapping);
	}

	private void setClusterPairStatusMapping(final ClusterPairStatusMapping statusMapping) {
		if (this.getFilter() != null)
			this.getFilter().removeFilterListener(this.clusterPairStatusMapping);
		this.clusterPairStatusMapping = statusMapping;
		this.getFilter().addFilterListener(this.clusterPairStatusMapping);
	}

	private void readObject(final ObjectInputStream s) throws ClassNotFoundException, IOException {
		s.defaultReadObject();
		this.getFilter().addFilterListener(this.clusterPairStatusMapping);
	}
}
