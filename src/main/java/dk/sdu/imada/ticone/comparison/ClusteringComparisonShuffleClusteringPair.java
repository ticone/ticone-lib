/**
 * 
 */
package dk.sdu.imada.ticone.comparison;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IShuffleClustering;
import dk.sdu.imada.ticone.clustering.pair.ClusterObjectMappingPair;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterObjectMappingPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IShuffleClusteringPair;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleResultWithMapping;
import dk.sdu.imada.ticone.util.Utility;

public class ClusteringComparisonShuffleClusteringPair extends AbstractShuffle implements IShuffleClusteringPair {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2532588531674002592L;

	final IShuffleClustering shuffleClustering1, shuffleClustering2;

	public ClusteringComparisonShuffleClusteringPair(final IShuffleClustering shuffleClustering1,
			final IShuffleClustering shuffleClustering2) {
		super();
		this.shuffleClustering1 = shuffleClustering1;
		this.shuffleClustering2 = shuffleClustering2;
	}

	@Override
	public ObjectType<IClusterObjectMappingPair> supportedObjectType() {
		return ObjectType.CLUSTERING_PAIR;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.shuffleClustering1 == null)
			throw new ShuffleNotInitializedException("shuffleClustering1");
		if (this.shuffleClustering2 == null)
			throw new ShuffleNotInitializedException("shuffleClustering2");
		return true;
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return type == ObjectType.CLUSTER_PAIR && this.shuffleClustering1 != null
				&& this.shuffleClustering1.producesShuffleMappingFor(ObjectType.CLUSTER);
	}

	@Override
	public IShuffleResultWithMapping doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final IClusterObjectMappingPair pair = supportedObjectType().getBaseType().cast(object);
		final IShuffleResult shuffleResult1 = this.shuffleClustering1.shuffle(pair.getClustering1(), seed);
		final IClusterObjectMapping random1 = this.shuffleClustering1.supportedObjectType().getBaseType()
				.cast(shuffleResult1.getShuffled());
		final IShuffleResult shuffleResult2 = this.shuffleClustering2.shuffle(pair.getClustering2(), seed);
		final IClusterObjectMapping random2 = this.shuffleClustering2.supportedObjectType().getBaseType()
				.cast(shuffleResult2.getShuffled());

		final IShuffleResultWithMapping result = new ShuffleResultWithMapping(pair,
				new ClusterObjectMappingPair(random1, random2));

		if (shuffleResult1 instanceof IShuffleResultWithMapping) {
			final IShuffleResultWithMapping castResult1 = (IShuffleResultWithMapping) shuffleResult1,
					castResult2 = (IShuffleResultWithMapping) shuffleResult2;
			final IShuffleMapping shuffleMapping = new BasicShuffleMapping();
			for (final ICluster c1 : castResult1.getShuffleMapping().keySet(ObjectType.CLUSTER)) {
				final ICluster shuffleC1 = castResult1.getShuffleMapping().get(c1);
				for (final ICluster c2 : castResult2.getShuffleMapping().keySet(ObjectType.CLUSTER)) {
					if (!Utility.getProgress().getStatus())
						throw new InterruptedException();
					final ICluster shuffleC2 = castResult2.getShuffleMapping().get(c2);

					final IClusterPair origPair = new ClusterPair(c1, c2);
					final IClusterPair shufflePair = new ClusterPair(shuffleC1, shuffleC2);
					shuffleMapping.put(origPair, shufflePair);
				}
			}
			result.setShuffleMapping(shuffleMapping);
		}
		return result;
	}

	@Override
	public String getName() {
		return "asd";
	}

	@Override
	public ClusteringComparisonShuffleClusteringPair copy() {
		return new ClusteringComparisonShuffleClusteringPair(this.shuffleClustering1.copy(),
				this.shuffleClustering2.copy());
	}

	@Override
	public ClusteringComparisonShuffleClusteringPair newInstance() {
		return new ClusteringComparisonShuffleClusteringPair(this.shuffleClustering1.newInstance(),
				this.shuffleClustering2.newInstance());
	}

	@Override
	public boolean validateParameters() {
		return true;
	}
}