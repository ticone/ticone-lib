package dk.sdu.imada.ticone.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import de.wiwie.wiutils.utils.Pair;

public class TiconeNetworkImplFactory extends TiconeNetworkFactory<TiconeNetworkImpl>
		implements ITiconeNetworkImplFactory {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7819707246807880270L;

	protected boolean isMultiGraph, cloneAttributes;

	protected boolean reuseInstanceIfExists;

	protected transient Map<NetworkAsIds, Map<List<Boolean>, WeakReference<TiconeNetworkImpl>>> createdInstances = new HashMap<>();

	public TiconeNetworkImplFactory() {
		this(false);
	}

	public TiconeNetworkImplFactory(final boolean isMultiGraph) {
		super();
		this.isMultiGraph = isMultiGraph;
	}

	private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		this.createdInstances = new HashMap<>();
	}

	protected static NetworkAsIds getNetworkAsIds(final ITiconeNetwork<?, ?> network) {
		final Set<String> nodeIds = network.getNodeIds();

		final Set<Pair<String, String>> edgesAsNodeIds = network.getEdgeSet().stream()
				.map(e -> Pair.getPair(e.getSource().getName(), e.getTarget().getName())).collect(Collectors.toSet());

		return new NetworkAsIds(nodeIds, edgesAsNodeIds);
	}

	protected boolean instanceHasBeenCreatedFor(final NetworkAsIds networkAsIds, final boolean isMultiGraph,
			final boolean hasCopyAttributes) {
		final List<Boolean> asList = Arrays.asList(isMultiGraph, hasCopyAttributes);
		return this.createdInstances.containsKey(networkAsIds)
				&& this.createdInstances.get(networkAsIds).containsKey(asList)
				&& !this.createdInstances.get(networkAsIds).get(asList).isEnqueued();
	}

	protected synchronized TiconeNetworkImpl getCreatedInstanceFor(final NetworkAsIds networkAsIds,
			final boolean isMultiGraph, final boolean hasCopyAttributes) {
		if (this.instanceHasBeenCreatedFor(networkAsIds, isMultiGraph, hasCopyAttributes)) {
			return this.createdInstances.get(networkAsIds).get(Arrays.asList(isMultiGraph, hasCopyAttributes)).get();
		}
		return null;
	}

	/**
	 * @param reuseInstanceIfExists the reuseInstanceIfExists to set
	 */
	@Override
	public void setReuseInstanceIfExists(final boolean reuseInstanceIfExists) {
		this.reuseInstanceIfExists = reuseInstanceIfExists;
	}

	/**
	 * @return the reuseInstanceIfExists
	 */
	@Override
	public boolean isReuseInstanceIfExists() {
		return this.reuseInstanceIfExists;
	}

	/**
	 * @return the isMultiGraph
	 */
	@Override
	public boolean isMultiGraph() {
		return this.isMultiGraph;
	}

	/**
	 * @param isMultiGraph the isMultiGraph to set
	 */
	public void setMultiGraph(final boolean isMultiGraph) {
		this.isMultiGraph = isMultiGraph;
	}

	/**
	 * @return the cloneAttributes
	 */
	@Override
	public boolean isCloneAttributes() {
		return this.cloneAttributes;
	}

	/**
	 * @param cloneAttributes the cloneAttributes to set
	 */
	@Override
	public void setCloneAttributes(final boolean cloneAttributes) {
		this.cloneAttributes = cloneAttributes;
	}

	@Override
	public TiconeNetworkImpl getInstance() {
		return this.getInstance(true);
	}

	@Override
	public TiconeNetworkImpl getInstance(final boolean registerNetwork) {
		final TiconeNetworkImpl net = new TiconeNetworkImpl("");
		net.setMultiGraph(this.isMultiGraph);
		return net;
	}

	@Override
	public TiconeNetworkImpl getInstance(
			final ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge<?>> network) {
		synchronized (this.createdInstances) {
			NetworkAsIds networkAsIds = null;
			if (this.reuseInstanceIfExists) {
				networkAsIds = getNetworkAsIds(network);
				if (this.instanceHasBeenCreatedFor(networkAsIds, this.isMultiGraph, this.cloneAttributes)) {
					final TiconeNetworkImpl oldInstance = this.getCreatedInstanceFor(networkAsIds, this.isMultiGraph,
							this.cloneAttributes);
					if (oldInstance != null)
						return oldInstance;
				}
			}
			final TiconeNetworkImpl net = TiconeNetworkImpl.getInstance(network, this.cloneAttributes);
			net.setMultiGraph(this.isMultiGraph);
			if (this.reuseInstanceIfExists) {
				this.createdInstances.putIfAbsent(networkAsIds, new HashMap<>());
				this.createdInstances.get(networkAsIds).put(Arrays.asList(this.isMultiGraph, this.cloneAttributes),
						new WeakReference<>(net));
			}
			return net;
		}
	}
}

class NetworkAsIds extends Pair<Set<String>, Set<Pair<String, String>>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1286062208776578733L;

	public NetworkAsIds(final Set<String> el1, final Set<Pair<String, String>> el2) {
		super(el1, el2);
	}

}