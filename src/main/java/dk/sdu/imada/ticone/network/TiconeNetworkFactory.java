package dk.sdu.imada.ticone.network;

import java.io.Serializable;

public abstract class TiconeNetworkFactory<TICONE_NETWORK extends ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge>>
		implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8103736005780821906L;

	public abstract TICONE_NETWORK getInstance();

	public abstract TICONE_NETWORK getInstance(boolean registerNetwork);
}
