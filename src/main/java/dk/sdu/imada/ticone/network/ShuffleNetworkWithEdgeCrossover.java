/**
 * 
 */
package dk.sdu.imada.ticone.network;

import java.awt.HeadlessException;

import javax.swing.JOptionPane;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleParameter;
import dk.sdu.imada.ticone.permute.ShuffleParameterException;

/**
 * @author Christian Wiwie
 * 
 * @since May 2, 2017
 *
 */
public class ShuffleNetworkWithEdgeCrossover extends AbstractShuffle implements IShuffleNetwork {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3090110097557431541L;

	@ShuffleParameter(uiName = "edge swap factor", uiDescription = "Factor S for number of edge swaps", uiDefaultValue = "16.12")
	protected double factorEdgeSwaps;

	protected final boolean isDirected;

	/**
	 * 
	 */
	public ShuffleNetworkWithEdgeCrossover(final boolean isDirected) {
		super();
		this.isDirected = isDirected;
		this.factorEdgeSwaps = 16.12;
	}

	@Override
	public ObjectType<ITiconeNetwork> supportedObjectType() {
		return ObjectType.NETWORK;
	}

	/**
	 * @return the isDirected
	 */
	public boolean isDirected() {
		return this.isDirected;
	}

	@Override
	public ShuffleNetworkWithEdgeCrossover copy() {
		final ShuffleNetworkWithEdgeCrossover s = new ShuffleNetworkWithEdgeCrossover(this.isDirected);
		s.factorEdgeSwaps = this.factorEdgeSwaps;
		return s;
	}

	@Override
	public boolean validateParameters() throws ShuffleParameterException {
		if (this.factorEdgeSwaps <= 0.0) {
			throw new ShuffleParameterException("The edge swap factor has to be larger than 0");
		} else if (this.factorEdgeSwaps <= 15.0) {
			try {
				final int answer = JOptionPane.showConfirmDialog(null,
						"You chose a small edge swap factor. This may result in permuted networks that are not sufficiently randomized and this may lead to inadequate p-values. Do you want to proceed?",
						"Small Edge Swap Factor", JOptionPane.YES_NO_OPTION);
				if (answer == JOptionPane.NO_OPTION)
					return false;
			} catch (HeadlessException e) {
			}
		}
		return true;
	}

	/**
	 * @param factorEdgeSwaps the factorEdgeSwaps to set
	 */
	public ShuffleNetworkWithEdgeCrossover setFactorEdgeSwaps(final double factorEdgeSwaps) {
		this.factorEdgeSwaps = factorEdgeSwaps;
		return this;
	}

	/**
	 * @return the factorEdgeSwaps
	 */
	public double getFactorEdgeSwaps() {
		return this.factorEdgeSwaps;
	}

	@Override
	public String getName() {
		if (this.isDirected)
			return "Shuffle Directed Network with Edge Crossovers";
		return "Shuffle Undirected Network with Edge Crossovers";
	}

	@Override
	public IShuffleNetwork newInstance() {
		return new ShuffleNetworkWithEdgeCrossover(this.isDirected);
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResult doShuffle(IObjectWithFeatures object, long seed) throws ShuffleException,
			ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException {
		final ITiconeNetwork network = supportedObjectType().getBaseType().cast(object);
		final ITiconeNetwork copy = network.copy();
		copy.performEdgeCrossovers(this.factorEdgeSwaps, this.isDirected, seed);
		return new BasicShuffleResult(network, copy);
	}
}
