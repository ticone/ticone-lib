/**
 * 
 */
package dk.sdu.imada.ticone.network;

import java.util.List;
import java.util.PrimitiveIterator.OfDouble;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleParameterException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 4, 2019
 *
 */
public class CreateRandomNetwork extends AbstractShuffle implements IShuffleNetwork {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5379271910947936081L;
	protected final boolean isDirected;

	/**
	 * 
	 */
	public CreateRandomNetwork(final boolean isDirected) {
		super();
		this.isDirected = isDirected;
	}

	@Override
	public ObjectType<ITiconeNetwork> supportedObjectType() {
		return ObjectType.NETWORK;
	}

	/**
	 * @return the isDirected
	 */
	public boolean isDirected() {
		return this.isDirected;
	}

	@Override
	public CreateRandomNetwork copy() {
		final CreateRandomNetwork s = new CreateRandomNetwork(this.isDirected);
		return s;
	}

	@Override
	public boolean validateParameters() throws ShuffleParameterException {
		return true;
	}

	@Override
	public String getName() {
		if (this.isDirected)
			return "Create Random Directed Network";
		return "Create Random Undirected Network";
	}

	@Override
	public IShuffleNetwork newInstance() {
		return new CreateRandomNetwork(this.isDirected);
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResult doShuffle(IObjectWithFeatures object, long seed) throws ShuffleException,
			ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException {
		final ITiconeNetwork network = supportedObjectType().getBaseType().cast(object);

		ScheduledThreadPoolExecutor privateThreadPool = null;
		if (network instanceof TiconeNetworkImpl)
			privateThreadPool = ((TiconeNetworkImpl) network).privateThreadPool;

		final TiconeNetworkImpl random = randomNetwork(new Random(seed), network.getNodeCount(),
				getEdgeDensity(network), privateThreadPool);
		return new BasicShuffleResult(network, random);
	}

	public double getEdgeDensity(ITiconeNetwork<?, ?> network) {
		if (this.isDirected)
			return network.getEdgeCount() / Math.pow(network.getNodeCount(), 2);
		return (double) network.getEdgeCount()
				/ (network.getNodeCount() * (network.getNodeCount() - 1) / 2 + network.getNodeCount());
	}

	public TiconeNetworkImpl randomNetwork(final Random r, final int numberNodes, final double edgeProbability,
			final ScheduledThreadPoolExecutor privateThreadPool) {
		final TiconeNetworkImpl network = new TiconeNetworkImpl(
				String.format("random-network-%d-%b-%3f", numberNodes, isDirected, edgeProbability), privateThreadPool);

		for (int o = 0; o < numberNodes; o++)
			network.addNode(String.format("Object %d", o + 1));

		if (isDirected) {
			final double[] doubles = r.doubles(numberNodes * numberNodes).toArray();
			int nIdx1 = 0;
			for (final TiconeNetworkNodeImpl n1 : network.getNodeList()) {
				int nIdx2 = 0;
				for (final TiconeNetworkNodeImpl n2 : network.getNodeList()) {
					if (doubles[nIdx1 * nIdx2 + nIdx2] < edgeProbability) {
						network.addEdge(n1, n2, isDirected);
					}
					nIdx2++;
				}
				nIdx1++;
			}
		} else {
			final OfDouble doubles = r.doubles(numberNodes * (numberNodes - 1) / 2 + numberNodes).iterator();

			final List<TiconeNetworkNodeImpl> nodeList = network.getNodeList();
			for (int nIdx1 = 0; nIdx1 < nodeList.size(); nIdx1++) {
				for (int nIdx2 = nIdx1; nIdx2 < nodeList.size(); nIdx2++) {
					if (doubles.next() < edgeProbability) {
						network.addEdge(nodeList.get(nIdx1), nodeList.get(nIdx2), isDirected);
					}
				}
			}
		}

		return network;
	}
}
