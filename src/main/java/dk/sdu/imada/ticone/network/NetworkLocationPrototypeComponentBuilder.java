/**
 * 
 */
package dk.sdu.imada.ticone.network;

import java.util.Collection;
import java.util.Objects;

import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.network.INetworkLocationPrototypeComponentBuilder.INetworkLocationPrototypeComponent;
import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 7, 2018
 *
 */
public class NetworkLocationPrototypeComponentBuilder extends
		AbstractBuilder<INetworkLocationPrototypeComponent, PrototypeComponentFactoryException, CreateInstanceFactoryException>
		implements INetworkLocationPrototypeComponentBuilder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8388011447922054261L;

	protected IAggregateCluster<Collection<? extends INetworkMappedTimeSeriesObject>> aggregationFunction;

	protected transient Collection<? extends INetworkMappedTimeSeriesObject> nodes;

	protected transient ITimeSeriesObjects objects;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.aggregationFunction == null) ? 0 : this.aggregationFunction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NetworkLocationPrototypeComponentBuilder other = (NetworkLocationPrototypeComponentBuilder) obj;
		if (this.aggregationFunction == null) {
			if (other.aggregationFunction != null)
				return false;
		} else if (!this.aggregationFunction.equals(other.aggregationFunction))
			return false;
		return true;
	}

	/**
	 * @param aggregationFunction the aggregationFunction to set
	 */
	@Override
	public NetworkLocationPrototypeComponentBuilder setAggregationFunction(
			final IAggregateCluster<Collection<? extends INetworkMappedTimeSeriesObject>> aggregationFunction) {
		this.aggregationFunction = aggregationFunction;
		return this;
	}

	@Override
	public IAggregateCluster<Collection<? extends INetworkMappedTimeSeriesObject>> getAggregationFunction() {
		return this.aggregationFunction;
	}

	@Override
	public PrototypeComponentType getType() {
		return PrototypeComponentType.NETWORK_LOCATION;
	}

	@Override
	public NetworkLocationPrototypeComponentBuilder copy() {
		final NetworkLocationPrototypeComponentBuilder f = new NetworkLocationPrototypeComponentBuilder();
		f.aggregationFunction = this.aggregationFunction.copy();
		f.nodes = this.nodes;
		f.objects = this.objects;
		return f;
	}

	@Override
	protected NetworkLocationPrototypeComponent doBuild()
			throws PrototypeComponentFactoryException, InterruptedException {
		if (this.nodes != null) {
			return new NetworkLocationPrototypeComponent(this.nodes);
		} else if (this.objects != null) {
			try {
				final Collection<? extends INetworkMappedTimeSeriesObject> aggregateCluster = this.aggregationFunction
						.aggregate(this.objects);
				return new NetworkLocationPrototypeComponent(aggregateCluster);
			} catch (final InterruptedException e) {
				throw e;
			} catch (final Exception e) {
				throw new PrototypeComponentFactoryException(e);
			}

		}
		throw new PrototypeComponentFactoryException(
				"Neither fixed network location nor objects for aggregation provided.");
	}

	@Override
	protected void reset() {
		nodes = null;
		objects = null;
	}

	@Override
	public ITimeSeriesObjects getObjects() {
		return this.objects;
	}

	@Override
	public NetworkLocationPrototypeComponentBuilder setNetworkLocation(
			final Collection<? extends INetworkMappedTimeSeriesObject> nodes) {
		this.nodes = nodes;
		return this;
	}

	@Override
	public final Collection<? extends INetworkMappedTimeSeriesObject> getNetworkLocation() {
		return this.nodes;
	}

	@Override
	public NetworkLocationPrototypeComponentBuilder setObjects(final ITimeSeriesObjects objects) {
		this.objects = objects;
		return this;
	}

}

class NetworkLocationPrototypeComponent implements INetworkLocationPrototypeComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4449961202904446521L;
	protected Collection<? extends INetworkMappedTimeSeriesObject> nodes;

	/**
	 * 
	 */
	NetworkLocationPrototypeComponent(final Collection<? extends INetworkMappedTimeSeriesObject> nodes) {
		super();
		this.nodes = nodes;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof NetworkLocationPrototypeComponent)
			return Objects.equals(this.nodes, ((NetworkLocationPrototypeComponent) obj).nodes);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.nodes);
	}

	@Override
	public String toString() {
		return this.nodes.toString();
	}

	@Override
	public Collection<? extends INetworkMappedTimeSeriesObject> getNetworkLocation() {
		return this.nodes;
	}

	@Override
	public NetworkLocationPrototypeComponent copy() {
		return new NetworkLocationPrototypeComponent(this.nodes);
	}

	@Override
	public PrototypeComponentType getType() {
		return PrototypeComponentType.NETWORK_LOCATION;
	}
}