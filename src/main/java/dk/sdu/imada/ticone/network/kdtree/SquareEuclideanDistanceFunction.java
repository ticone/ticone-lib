package dk.sdu.imada.ticone.network.kdtree;

/**
 *
 */
public class SquareEuclideanDistanceFunction implements DistanceFunction {
	@Override
	public double distance(final double[] p1, final double[] p2) {
		double d = 0;

		for (int i = 0; i < p1.length; i++) {
			final double diff = (p1[i] - p2[i]);
			d += diff * diff;
		}

		return d;
	}

	@Override
	public double distanceToRect(final double[] point, final double[] min, final double[] max) {
		double d = 0;

		for (int i = 0; i < point.length; i++) {
			double diff = 0;
			if (point[i] > max[i]) {
				diff = (point[i] - max[i]);
			} else if (point[i] < min[i]) {
				diff = (point[i] - min[i]);
			}
			d += diff * diff;
		}

		return d;
	}
}