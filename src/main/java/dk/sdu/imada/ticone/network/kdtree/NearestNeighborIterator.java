package dk.sdu.imada.ticone.network.kdtree;

import java.util.Arrays;
import java.util.Iterator;

/**
 *
 */
public class NearestNeighborIterator<T> implements Iterator<T>, Iterable<T> {
	private final DistanceFunction distanceFunction;
	private final double[] searchPoint;
	private final MinHeap<KdNode<T>> pendingPaths;
	private final IntervalHeap<T> evaluatedPoints;
	private int pointsRemaining;
	private double lastDistanceReturned;

	protected NearestNeighborIterator(final KdNode<T> treeRoot, final double[] searchPoint, final int maxPointsReturned,
			final DistanceFunction distanceFunction) {
		this.searchPoint = Arrays.copyOf(searchPoint, searchPoint.length);
		this.pointsRemaining = Math.min(maxPointsReturned, treeRoot.size());
		this.distanceFunction = distanceFunction;
		this.pendingPaths = new BinaryHeap.Min<>();
		this.pendingPaths.offer(0, treeRoot);
		this.evaluatedPoints = new IntervalHeap<>();
	}

	/* -------- INTERFACE IMPLEMENTATION -------- */

	@Override
	public boolean hasNext() {
		return this.pointsRemaining > 0;
	}

	@Override
	public T next() {
		if (!this.hasNext()) {
			throw new IllegalStateException("NearestNeighborIterator has reached end!");
		}

		while (this.pendingPaths.size() > 0 && (this.evaluatedPoints.size() == 0
				|| (this.pendingPaths.getMinKey() < this.evaluatedPoints.getMinKey()))) {
			KdTree.nearestNeighborSearchStep(this.pendingPaths, this.evaluatedPoints, this.pointsRemaining,
					this.distanceFunction, this.searchPoint);
		}

		// Return the smallest distance point
		this.pointsRemaining--;
		this.lastDistanceReturned = this.evaluatedPoints.getMinKey();
		final T value = this.evaluatedPoints.getMin();
		this.evaluatedPoints.removeMin();
		return value;
	}

	public double distance() {
		return this.lastDistanceReturned;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<T> iterator() {
		return this;
	}
}
