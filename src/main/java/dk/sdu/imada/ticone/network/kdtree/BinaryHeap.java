package dk.sdu.imada.ticone.network.kdtree;

import java.util.Arrays;

/**
 * An implementation of an implicit binary heap. Min-heap and max-heap both
 * supported
 */
public abstract class BinaryHeap<T> {
	protected static final int defaultCapacity = 64;
	private final int direction;
	private Object[] data;
	private double[] keys;
	private int capacity;
	private int size;

	protected BinaryHeap(final int capacity, final int direction) {
		this.direction = direction;
		this.data = new Object[capacity];
		this.keys = new double[capacity];
		this.capacity = capacity;
		this.size = 0;
	}

	public void offer(final double key, final T value) {
		// If move room is needed, double array size
		if (this.size >= this.capacity) {
			this.capacity *= 2;
			this.data = Arrays.copyOf(this.data, this.capacity);
			this.keys = Arrays.copyOf(this.keys, this.capacity);
		}

		// Insert new value at the end
		this.data[this.size] = value;
		this.keys[this.size] = key;
		this.siftUp(this.size);
		this.size++;
	}

	protected void removeTip() {
		if (this.size == 0) {
			throw new IllegalStateException();
		}

		this.size--;
		this.data[0] = this.data[this.size];
		this.keys[0] = this.keys[this.size];
		this.data[this.size] = null;
		this.siftDown(0);
	}

	protected void replaceTip(final double key, final T value) {
		if (this.size == 0) {
			throw new IllegalStateException();
		}

		this.data[0] = value;
		this.keys[0] = key;
		this.siftDown(0);
	}

	@SuppressWarnings("unchecked")
	protected T getTip() {
		if (this.size == 0) {
			throw new IllegalStateException();
		}

		return (T) this.data[0];
	}

	protected double getTipKey() {
		if (this.size == 0) {
			throw new IllegalStateException();
		}

		return this.keys[0];
	}

	private void siftUp(int c) {
		for (int p = (c - 1) / 2; c != 0
				&& this.direction * this.keys[c] > this.direction * this.keys[p]; c = p, p = (c - 1) / 2) {
			final Object pData = this.data[p];
			final double pDist = this.keys[p];
			this.data[p] = this.data[c];
			this.keys[p] = this.keys[c];
			this.data[c] = pData;
			this.keys[c] = pDist;
		}
	}

	private void siftDown(int p) {
		for (int c = p * 2 + 1; c < this.size; p = c, c = p * 2 + 1) {
			if (c + 1 < this.size && this.direction * this.keys[c] < this.direction * this.keys[c + 1]) {
				c++;
			}
			if (this.direction * this.keys[p] < this.direction * this.keys[c]) {
				// Swap the points
				final Object pData = this.data[p];
				final double pDist = this.keys[p];
				this.data[p] = this.data[c];
				this.keys[p] = this.keys[c];
				this.data[c] = pData;
				this.keys[c] = pDist;
			} else {
				break;
			}
		}
	}

	public int size() {
		return this.size;
	}

	public int capacity() {
		return this.capacity;
	}

	public static final class Max<T> extends BinaryHeap<T> implements MaxHeap<T> {
		public Max() {
			super(defaultCapacity, 1);
		}

		public Max(final int capacity) {
			super(capacity, 1);
		}

		@Override
		public void removeMax() {
			this.removeTip();
		}

		@Override
		public void replaceMax(final double key, final T value) {
			this.replaceTip(key, value);
		}

		@Override
		public T getMax() {
			return this.getTip();
		}

		@Override
		public double getMaxKey() {
			return this.getTipKey();
		}
	}

	public static final class Min<T> extends BinaryHeap<T> implements MinHeap<T> {
		public Min() {
			super(defaultCapacity, -1);
		}

		public Min(final int capacity) {
			super(capacity, -1);
		}

		@Override
		public void removeMin() {
			this.removeTip();
		}

		@Override
		public void replaceMin(final double key, final T value) {
			this.replaceTip(key, value);
		}

		@Override
		public T getMin() {
			return this.getTip();
		}

		@Override
		public double getMinKey() {
			return this.getTipKey();
		}
	}
}