package dk.sdu.imada.ticone.network.kdtree;

import java.util.Arrays;

/**
 *
 */
class KdNode<T> {
	// All types
	protected int dimensions;
	protected int bucketCapacity;
	protected int size;

	// Leaf only
	protected double[][] points;
	protected Object[] data;

	// Stem only
	protected KdNode<T> left, right;
	protected int splitDimension;
	protected double splitValue;

	// Bounds
	protected double[] minBound, maxBound;
	protected boolean singlePoint;

	protected KdNode(final int dimensions, final int bucketCapacity) {
		// Init base
		this.dimensions = dimensions;
		this.bucketCapacity = bucketCapacity;
		this.size = 0;
		this.singlePoint = true;

		// Init leaf elements
		this.points = new double[bucketCapacity + 1][];
		this.data = new Object[bucketCapacity + 1];
	}

	@Override
	public KdNode<T> clone() {
		final KdNode<T> copy = new KdNode<>(this.dimensions, this.bucketCapacity);
		copy.size = this.size;
		copy.points = new double[this.points.length][];
		for (int i = 0; i < copy.points.length; i++) {
			copy.points[i] = new double[this.points[i].length];
			for (int j = 0; j < copy.points[i].length; j++)
				copy.points[i][j] = this.points[i][j];
		}
		// for our case, this is enough
		copy.data = copy.points;
		copy.left = this.left.clone();
		copy.right = this.right.clone();
		copy.splitDimension = this.splitDimension;
		copy.splitValue = this.splitValue;
		copy.minBound = new double[this.minBound.length];
		for (int i = 0; i < copy.minBound.length; i++)
			copy.minBound[i] = this.minBound[i];
		copy.maxBound = new double[this.maxBound.length];
		for (int i = 0; i < copy.maxBound.length; i++)
			copy.maxBound[i] = this.maxBound[i];
		copy.singlePoint = this.singlePoint;
		return copy;
	}

	/* -------- SIMPLE GETTERS -------- */

	public int size() {
		return this.size;
	}

	public boolean isLeaf() {
		return this.points != null;
	}

	/* -------- OPERATIONS -------- */

	public void addPoint(final double[] point, final T value) {
		KdNode<T> cursor = this;
		while (!cursor.isLeaf()) {
			cursor.extendBounds(point);
			cursor.size++;
			if (point[cursor.splitDimension] > cursor.splitValue) {
				cursor = cursor.right;
			} else {
				cursor = cursor.left;
			}
		}
		cursor.addLeafPoint(point, value);
	}

	/* -------- INTERNAL OPERATIONS -------- */

	public void addLeafPoint(final double[] point, final T value) {
		// Add the data point
		this.points[this.size] = point;
		this.data[this.size] = value;
		this.extendBounds(point);
		this.size++;

		if (this.size == this.points.length - 1) {
			// If the node is getting too large
			if (this.calculateSplit()) {
				// If the node successfully had it's split value calculated,
				// split node
				this.splitLeafNode();
			} else {
				// If the node could not be split, enlarge node
				this.increaseLeafCapacity();
			}
		}
	}

	private boolean checkBounds(final double[] point) {
		for (int i = 0; i < this.dimensions; i++) {
			if (point[i] > this.maxBound[i])
				return false;
			if (point[i] < this.minBound[i])
				return false;
		}
		return true;
	}

	private void extendBounds(final double[] point) {
		if (this.minBound == null) {
			this.minBound = Arrays.copyOf(point, this.dimensions);
			this.maxBound = Arrays.copyOf(point, this.dimensions);
			return;
		}

		for (int i = 0; i < this.dimensions; i++) {
			if (Double.isNaN(point[i])) {
				if (!Double.isNaN(this.minBound[i]) || !Double.isNaN(this.maxBound[i])) {
					this.singlePoint = false;
				}
				this.minBound[i] = Double.NaN;
				this.maxBound[i] = Double.NaN;
			} else if (this.minBound[i] > point[i]) {
				this.minBound[i] = point[i];
				this.singlePoint = false;
			} else if (this.maxBound[i] < point[i]) {
				this.maxBound[i] = point[i];
				this.singlePoint = false;
			}
		}
	}

	private void increaseLeafCapacity() {
		this.points = Arrays.copyOf(this.points, this.points.length * 2);
		this.data = Arrays.copyOf(this.data, this.data.length * 2);
	}

	private boolean calculateSplit() {
		if (this.singlePoint)
			return false;

		double width = 0;
		for (int i = 0; i < this.dimensions; i++) {
			double dwidth = (this.maxBound[i] - this.minBound[i]);
			if (Double.isNaN(dwidth))
				dwidth = 0;
			if (dwidth > width) {
				this.splitDimension = i;
				width = dwidth;
			}
		}

		if (width == 0) {
			return false;
		}

		// Start the split in the middle of the variance
		this.splitValue = (this.minBound[this.splitDimension] + this.maxBound[this.splitDimension]) * 0.5;

		// Never split on infinity or NaN
		if (this.splitValue == Double.POSITIVE_INFINITY) {
			this.splitValue = Double.MAX_VALUE;
		} else if (this.splitValue == Double.NEGATIVE_INFINITY) {
			this.splitValue = -Double.MAX_VALUE;
		}

		// Don't let the split value be the same as the upper value as
		// can happen due to rounding errors!
		if (this.splitValue == this.maxBound[this.splitDimension]) {
			this.splitValue = this.minBound[this.splitDimension];
		}

		// Success
		return true;
	}

	private void splitLeafNode() {
		this.right = new KdNode<>(this.dimensions, this.bucketCapacity);
		this.left = new KdNode<>(this.dimensions, this.bucketCapacity);

		// Move locations into children
		for (int i = 0; i < this.size; i++) {
			final double[] oldLocation = this.points[i];
			final Object oldData = this.data[i];
			if (oldLocation[this.splitDimension] > this.splitValue) {
				this.right.addLeafPoint(oldLocation, (T) oldData);
			} else {
				this.left.addLeafPoint(oldLocation, (T) oldData);
			}
		}

		this.points = null;
		this.data = null;
	}
}
