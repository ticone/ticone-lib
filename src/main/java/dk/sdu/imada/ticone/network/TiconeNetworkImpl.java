package dk.sdu.imada.ticone.network;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import cern.colt.bitvector.BitMatrix;
import de.wiwie.wiutils.utils.Triple;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.connectivity.ConnectivityResult;
import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObject.NetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberDirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberUndirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IClusterPairFeatureNumberDirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.IClusterPairFeatureNumberUndirectedConnectingEdges;
import dk.sdu.imada.ticone.feature.IClusterPairFeatureShortestPath;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.feature.IObjectClusterFeatureShortestPath;
import dk.sdu.imada.ticone.feature.IObjectPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.ObjectClusterFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.ObjectPairFeatureShortestDistance;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkEdgeImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.network.kdtree.KdTree;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;
import dk.sdu.imada.ticone.util.Utility;
import it.unimi.dsi.fastutil.objects.ObjectList;

public class TiconeNetworkImpl extends TiconeNetwork<TiconeNetworkNodeImpl, TiconeNetworkEdgeImpl>
		implements IFeatureValueProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5620616430189386954L;

	public static TiconeNetworkImpl parseFromFile(final String name, final boolean isDirected, final File networkFile)
			throws IOException {
		final BufferedReader sc = new BufferedReader(new FileReader(networkFile));
		try {
			return parseFromReader(name, isDirected, sc);
		} finally {
			sc.close();
		}
	}

	public static TiconeNetworkImpl parseFromReader(final String name, final boolean isDirected,
			final BufferedReader sc) throws IOException {
		final TiconeNetworkImpl network = new TiconeNetworkImpl(name);

		while (sc.ready()) {
			final String line = sc.readLine();
			// TODO: assume tab-separation for now
			final String[] lineSplit = line.split("\t");
			if (!network.containsNode(lineSplit[0]))
				network.addNode(lineSplit[0]);
			if (!network.containsNode(lineSplit[1]))
				network.addNode(lineSplit[1]);
			network.addEdge(lineSplit[0], lineSplit[1], isDirected);
		}

		sc.close();

		return network;
	}

	public static <TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>> TiconeNetworkImpl getInstance(
			final ITiconeNetwork<TICONE_NODE, TICONE_EDGE> origNetwork, final boolean cloneAttributes) {
		// if (origNetwork instanceof TiCoNENetworkImpl)
		// return (TiCoNENetworkImpl) origNetwork;

		final TiconeNetworkImpl newNetwork = new TiconeNetworkImpl(origNetwork);

		if (origNetwork instanceof TiconeNetworkImpl) {
			final List<TiconeNetworkNodeImpl> oldNetworkWideIdArray = ((TiconeNetworkImpl) origNetwork)
					.getNetworkWideNodeIdToNode();
			if (oldNetworkWideIdArray != null) {
				newNetwork.networkWideNodeIdToNodeList = new ArrayList<>(oldNetworkWideIdArray.size());
			}
		}

		// copy nodes
		for (final TICONE_NODE origNode : origNetwork.getNodeSet()) {
			if (origNetwork instanceof TiconeNetworkImpl) {
				final int origNetworkWideId = ((TiconeNetworkNodeImpl) origNode).networkWideId;
				final TiconeNetworkNodeImpl newNode = newNetwork.addNode(origNode.getName(), origNetworkWideId);
				newNode.setUndirectedDegree(((TiconeNetworkNodeImpl) origNode).undirectedDegree);
			} else {
				TiconeNetworkNodeImpl newNode = newNetwork.addNode(origNode.getName());
				newNode.setAlternativeName(origNode.getAlternativeName());
			}
		}
		// copy edges
		final Map<TICONE_EDGE, TiconeNetworkEdgeImpl> edgeMap = new HashMap<>();
		for (final TICONE_EDGE origEdge : origNetwork.getEdgeSet()) {
			TiconeNetworkEdgeImpl addEdge;
			if (origNetwork instanceof TiconeNetworkImpl) {
				addEdge = newNetwork.addEdge(origEdge.getSource().getName(), origEdge.getTarget().getName(),
						origEdge.isDirected(), ((TiconeNetworkEdgeImpl) origEdge).networkWideId);
			} else {
				addEdge = newNetwork.addEdge(origEdge.getSource().getName(), origEdge.getTarget().getName(),
						origEdge.isDirected());
			}
			edgeMap.put(origEdge, addEdge);
		}

		if (cloneAttributes) {
			// copy node attributes
			for (final Triple<String, Class, Boolean> nodeAttr : origNetwork.getNodeAttributes()) {
				newNetwork.createNodeAttribute(nodeAttr.getFirst(), nodeAttr.getSecond(), nodeAttr.getThird());

				for (final TICONE_NODE origNode : origNetwork.getNodeList())
					newNetwork.setNodeAttribute(newNetwork.getNode(origNode.getName()), nodeAttr.getFirst(),
							origNetwork.getValue(origNode, nodeAttr.getFirst(), nodeAttr.getSecond()));
			}
			// copy edge attributes
			for (final Triple<String, Class, Boolean> edgeAttr : origNetwork.getEdgeAttributes()) {
				newNetwork.createEdgeAttribute(edgeAttr.getFirst(), edgeAttr.getSecond(), edgeAttr.getThird());

				for (final TICONE_EDGE origEdge : origNetwork.getEdgeList())
					newNetwork.setEdgeAttribute(edgeMap.get(origEdge), edgeAttr.getFirst(),
							origNetwork.getValue(origEdge, edgeAttr.getFirst(), edgeAttr.getSecond()));
			}
		}
		return newNetwork;
	}

	protected Map<String, Object> networkAttributes;
	protected Map<String, Map<TiconeNetworkNodeImpl, Object>> nodeAttributes;
	protected Map<String, Map<EdgeWrapper, Object>> edgeAttributes;

	// statistics
	protected transient BitMatrix undirectedConnectedNodes, directedConnectedNodes;
	protected transient int[] totalNodeDegreeCounts;
	protected transient int[][] totalDirectedEdgeCountNodeDegrees;
	protected transient double[][] totalDirectedEdgeProbabilityNodeDegrees;
	protected transient int[][] totalUndirectedEdgeCountNodeDegrees;
	protected transient double[][] totalUndirectedEdgeProbabilityNodeDegrees;
	protected transient KdTree<double[]> nearestNeighborsJoinedNodeDegreesDirected,
			nearestNeighborsJoinedNodeDegreesUndirected;

	protected transient boolean isNodeDegreesUndirectedInitialized;

	protected transient int maxUndirectedNodeDegree;

	protected transient boolean isEdgeCrossoverCancelled;

	protected ConnectivityResult directedEnrichment, undirectedEnrichment;

	protected transient int[][] theoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees,
			theoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees;

	protected transient ShortestDistanceCalculator shortestDistanceCalculatorDirectedIgnore,
			shortestDistanceCalculatorUndirectedIgnore;

	private final ClusterPairFeatureNumberDirectedConnectingEdgesCalculator clusterPairFeatureNumberDirectedConnectingEdgesCalculator = new ClusterPairFeatureNumberDirectedConnectingEdgesCalculator();

	private final ClusterPairFeatureNumberUndirectedConnectingEdgesCalculator clusterPairFeatureNumberUndirectedConnectingEdgesCalculator = new ClusterPairFeatureNumberUndirectedConnectingEdgesCalculator();

	public TiconeNetworkImpl(final String name) {
		this(name, null);
	}

	public TiconeNetworkImpl(final String name, final ScheduledThreadPoolExecutor privateThreadPool) {
		super(name, privateThreadPool);
		this.nodeAttributes = new HashMap<>();
		this.edgeAttributes = new HashMap<>();
	}

	public <TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>> TiconeNetworkImpl(
			ITiconeNetwork<TICONE_NODE, TICONE_EDGE> origNetwork) {
		super(origNetwork);
		this.nodeAttributes = new HashMap<>();
		this.edgeAttributes = new HashMap<>();
	}

	public boolean isMultiGraph() {
		return this.isMultiGraph;
	}

	public void setMultiGraph(final boolean isMultiGraph) {
		this.isMultiGraph = isMultiGraph;
	}

	public ConnectivityResult getDirectedEnrichment() {
		return this.directedEnrichment;
	}

	public void setDirectedEnrichment(final ConnectivityResult directedEnrichment) {
		this.directedEnrichment = directedEnrichment;
	}

	public ConnectivityResult getUndirectedEnrichment() {
		return this.undirectedEnrichment;
	}

	public void setUndirectedEnrichment(final ConnectivityResult undirectedEnrichment) {
		this.undirectedEnrichment = undirectedEnrichment;
	}

	@Override
	public String toString() {
		return String.format("Network %s (|V|=%d,|E|=%d)", this.name, this.nodes.size(), this.edges.size());
	}
//
//	@Override
//	public boolean equals(Object obj) {
//		// makes sure that we have identical classes
//		if (!(super.equals(obj)))
//			return false;
//
//		TiconeNetworkImpl other = (TiconeNetworkImpl) obj;
//		return Objects.equals(this.nodeAttributes, other.nodeAttributes)
//				&& Objects.equals(this.edgeAttributes, other.edgeAttributes);
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(super.hashCode(), this.nodeAttributes, this.edgeAttributes);
//	}

	@Override
	public TiconeNetworkNodeImpl addNode(final String name) {
		return this.addNodes(new String[] { name })[0];
	}

	@Override
	public synchronized TiconeNetworkNodeImpl[] addNodes(final String[] names) {
		final TiconeNetworkNodeImpl[] result = new TiconeNetworkNodeImpl[names.length];

		ensureSize(this.getNetworkWideNodeIdToNode(), this.nextNetworkWideNodeId.get() + names.length);

		int p = 0;
		for (final String name : names) {
			if (this.nodeIdToNode.containsKey(name))
				result[p] = this.nodeIdToNode.get(name);
			else {
				result[p] = new TiconeNetworkNodeImpl(name, this.nextNetworkWideNodeId.get());
				this.getNetworkWideNodeIdToNode().set(this.nextNetworkWideNodeId.get(), result[p]);
				this.insertNodeIntoNodeDatastructures(result[p]);
				this.insertNodeIntoEdgeDatastructures(result[p]);
			}
			p++;
		}

		return result;
	}

	public TiconeNetworkNodeImpl addNode(final String name, final int networkWideId) {
		return this.addNodes(new String[] { name }, new int[] { networkWideId })[0];
	}

	public synchronized TiconeNetworkNodeImpl[] addNodes(final String[] names, final int[] networkWideIds) {
		final TiconeNetworkNodeImpl[] result = new TiconeNetworkNodeImpl[names.length];

		final List<TiconeNetworkNodeImpl> newNodes = new ArrayList<>();
		final Map<String, TiconeNetworkNodeImpl> newNodeIdToNode = new HashMap<>();

		int networkWideIdsMax = Integer.MIN_VALUE;

		int p = 0;
		for (final String name : names) {
			if (this.nodeIdToNode.containsKey(name))
				result[p] = this.nodeIdToNode.get(name);
			else {
				result[p] = new TiconeNetworkNodeImpl(name, networkWideIds[p]);
				newNodes.add(result[p]);
				newNodeIdToNode.put(name, result[p]);

				if (networkWideIds[p] > networkWideIdsMax)
					networkWideIdsMax = networkWideIds[p];
				this.insertNodeIntoEdgeDatastructures(result[p]);
			}
			p++;
		}

		this.nodes.addAll(newNodes);
		this.nodeIdToNode.putAll(newNodeIdToNode);
		if (networkWideIdsMax >= this.nextNetworkWideNodeId.get())
			this.nextNetworkWideNodeId.set(networkWideIdsMax + 1);
		ensureSize(this.getNetworkWideNodeIdToNode(), networkWideIdsMax + 1);
		for (final TiconeNetworkNodeImpl node : newNodes)
			this.getNetworkWideNodeIdToNode().set(node.networkWideId, node);

		return result;
	}

	@Override
	public TiconeNetworkNodeImpl removeNode(final String name) {
		return this.removeNodesByIds(new String[] { name })[0];
	}

	@Override
	public TiconeNetworkNodeImpl[] removeNodesByIds(final String[] names) {
		final TiconeNetworkNodeImpl[] nodes = new TiconeNetworkNodeImpl[names.length];
		for (int p = 0; p < nodes.length; p++)
			nodes[p] = this.nodeIdToNode.get(names[p]);

		this.removeNodes(nodes);
		return nodes;
	}

	@Override
	public boolean removeNode(final TiconeNetworkNodeImpl node) {
		return this.removeNodes(new TiconeNetworkNodeImpl[] { node });
	}

	@Override
	public <N extends TiconeNetworkNodeImpl> boolean removeNodes(final TiconeNetworkNodeImpl[] nodes) {
		final Set<TiconeNetworkNodeImpl> nodeSet = new HashSet<>(Arrays.asList(nodes));
		final List<String> nodeIds = new ArrayList<>();
		for (int p = 0; p < nodes.length; p++) {
			final TiconeNetworkNodeImpl node = nodes[p];
			nodeIds.add(node.name);
			this.getNetworkWideNodeIdToNode().set(node.networkWideId, null);
		}

		this.nodeIdToNode.keySet().removeAll(nodeIds);
		this.nodes.removeAll(nodeSet);
		for (final String nodeAttr : this.nodeAttributes.keySet())
			this.nodeAttributes.get(nodeAttr).keySet().removeAll(nodeSet);

		final Set<TiconeNetworkEdgeImpl> edgesToBeRemoved = new HashSet<>();

		// it seems that keySet().removeAll() does not provide speed boni; i.e.
		// we just leave it as it was
		for (final TiconeNetworkNodeImpl node : nodes) {
			final Map<TiconeNetworkNodeImpl, Set<TiconeNetworkEdgeImpl>> incomingEdges = node.incomingEdges;
			if (incomingEdges != null) {
				for (final Set<TiconeNetworkEdgeImpl> edgeSet : incomingEdges.values())
					edgesToBeRemoved.addAll(edgeSet);
				for (final TiconeNetworkNodeImpl deletedEdgeSource : incomingEdges.keySet()) {
					deletedEdgeSource.outgoingEdges.remove(node);
				}
			}
			final Map<TiconeNetworkNodeImpl, Set<TiconeNetworkEdgeImpl>> outgoingEdges = node.outgoingEdges;
			if (outgoingEdges != null) {
				for (final Set<TiconeNetworkEdgeImpl> edgeSet : outgoingEdges.values())
					edgesToBeRemoved.addAll(edgeSet);
				for (final TiconeNetworkNodeImpl deletedEdgeTarget : outgoingEdges.keySet()) {
					deletedEdgeTarget.incomingEdges.remove(node);
				}
			}
			final Set<TiconeNetworkEdgeImpl> selfEdges = node.selfEdges;
			if (selfEdges != null)
				edgesToBeRemoved.addAll(selfEdges);
		}
		// remove these edges
		this.edges.removeAll(edgesToBeRemoved);
		return true;
	}

	@Override
	public TiconeNetworkEdgeImpl addEdge(final String sourceNodeId, final String targetNodeId,
			final boolean isDirected) {
		if (sourceNodeId == null || targetNodeId == null)
			return null;
		return this.addEdge(this.nodeIdToNode.get(sourceNodeId), this.nodeIdToNode.get(targetNodeId), isDirected);
	}

	@Override
	public synchronized TiconeNetworkEdgeImpl addEdge(final TiconeNetworkNodeImpl source,
			final TiconeNetworkNodeImpl target, final boolean isDirected) {
		if (source == null || target == null)
			return null;
		final TiconeNetworkEdgeImpl edge = new TiconeNetworkEdgeImpl(source, target, isDirected,
				this.nextNetworkWideEdgeId.get());
		return this.insertEdgeIntoEdgeDatastructures(edge);
	}

	private void removeEdgeFromEdgeDatastructures(final TiconeNetworkEdgeImpl edge) {
		final TiconeNetworkNodeImpl source = edge.getSource();
		final TiconeNetworkNodeImpl target = edge.getTarget();

		if (source.equals(target)) {
			source.selfEdges.remove(edge);
		} else {
			if (source.outgoingEdges.containsKey(target)) {
				final Set<TiconeNetworkEdgeImpl> set = source.outgoingEdges.get(target);
				set.remove(edge);
				if (set.isEmpty())
					source.outgoingEdges.remove(target);
			}
			if (target.incomingEdges.containsKey(source)) {
				final Set<TiconeNetworkEdgeImpl> set = target.incomingEdges.get(source);
				set.remove(edge);
				if (set.isEmpty())
					target.incomingEdges.remove(source);
			}
		}
	}

	public TiconeNetworkEdgeImpl addEdge(final String sourceNodeId, final String targetNodeId, final boolean isDirected,
			final long networkWideId) {
		if (sourceNodeId == null || targetNodeId == null)
			return null;
		return this.addEdge(this.nodeIdToNode.get(sourceNodeId), this.nodeIdToNode.get(targetNodeId), isDirected,
				networkWideId);
	}

	public synchronized TiconeNetworkEdgeImpl addEdge(final TiconeNetworkNodeImpl source,
			final TiconeNetworkNodeImpl target, final boolean isDirected, final long networkWideId) {
		if (source == null || target == null)
			return null;
		final TiconeNetworkEdgeImpl edge = new TiconeNetworkEdgeImpl(source, target, isDirected, networkWideId);
		return this.insertEdgeIntoEdgeDatastructures(edge);
	}

	public boolean removeEdge(final TiconeNetworkEdgeImpl edge) {
		final boolean result = this.edges.remove(edge);
		final EdgeWrapper ew = this.edgeToEdgeWrapper.remove(edge);
		this.edgeWrapperToEdge.remove(ew);
		this.removeEdgeFromEdgeDatastructures(edge);
		return result;
	}

	public void removeDuplicateEdges() {
		// If we are a multi graph, we don't do anything
		if (this.isMultiGraph)
			return;

		final List<TiconeNetworkEdgeImpl> edgeList = this.getEdgeList();
		this.edges.clear();
		this.edgeWrapperToEdge.clear();
		this.edgeToEdgeWrapper.clear();
		for (final TiconeNetworkNodeImpl n : this.getNodeSet()) {
			n.incomingEdges.clear();
			n.outgoingEdges.clear();
			n.selfEdges.clear();
		}

		for (final TiconeNetworkEdgeImpl e : edgeList) {
			this.addEdge(e.source, e.target, e.isDirected);
		}

		// force refreshing these probabilities from the above counts
		this.clearStatistics();
		this.initNodeDegreesUndirected(true);
		this.getTotalUndirectedEdgeProbabilityNodeDegrees();
		this.getTotalDirectedEdgeProbabilityNodeDegrees();
	}

	// @Override
	// public boolean containsEdge(TiCoNENetworkNodeImpl n1,
	// TiCoNENetworkNodeImpl n2) {
	// return n1.equals(n2) ? this.selfEdges.containsKey(n1)
	// : this.outgoingEdges.containsKey(n1) &&
	// this.outgoingEdges.get(n1).containsKey(n2)
	// && !this.outgoingEdges.get(n1).get(n2).isEmpty();
	// }

	@Override
	public <T> T getValue(final TiconeNetworkNodeImpl node, final String col, final Class<T> c) {
		return c.cast(this.nodeAttributes.get(col).get(node));
	}

	@Override
	public <T> T getValue(final TiconeNetworkEdgeImpl e, final String col, final Class<T> c) {
		return c.cast(this.edgeAttributes.get(col).get(this.edgeToEdgeWrapper.get(e)));
	}

	@Override
	public boolean hasNodeAttribute(final String attribute) {
		return this.nodeAttributes.containsKey(attribute);
	}

	@Override
	// TODO: implement immutability
	public void createNodeAttribute(final String attribute, final Class c, final boolean isImmutable) {
		if (!this.nodeAttributes.containsKey(attribute))
			this.nodeAttributes.put(attribute, new HashMap<TiconeNetworkNodeImpl, Object>());
	}

	@Override
	public void setNodeAttribute(final TiconeNetworkNodeImpl node, final String attribute, final Object value) {
		this.nodeAttributes.get(attribute).put(node, value);
	}

	@Override
	public void setEdgeAttribute(final TiconeNetworkEdgeImpl edge, final String attribute, final Object value) {
		this.edgeAttributes.get(attribute).put(this.edgeToEdgeWrapper.get(edge), value);
	}

	@Override
	public boolean hasEdgeAttribute(final String attribute) {
		return this.edgeAttributes.containsKey(attribute);
	}

	@Override
	public void createEdgeAttribute(final String attribute, final Class c, final boolean isImmutable) {
		if (!this.edgeAttributes.containsKey(attribute))
			this.edgeAttributes.put(attribute, new HashMap<EdgeWrapper, Object>());
	}

	@Override
	public boolean hasNetworkAttribute(final String attribute) {
		return this.networkAttributes.containsKey(attribute);
	}

	@Override
	public void createNetworkAttribute(final String attribute, final Class c, final boolean isImmutable) {
		if (!this.networkAttributes.containsKey(attribute))
			this.networkAttributes.put(attribute, null);
	}

	@Override
	public void setNetworkAttribute(final String attribute, final Object value) {
		this.networkAttributes.put(attribute, value);
	}

	@Override
	public <N extends TiconeNetworkNodeImpl> List<TiconeNetworkEdgeImpl> getAdjacentEdgeList(final N node,
			final TiconeEdgeType types) {
		final List<TiconeNetworkEdgeImpl> neighbors = new ArrayList<>();
		if (types.equals(TiconeEdgeType.INCOMING) || types.equals(TiconeEdgeType.ANY)) {
			for (final TiconeNetworkNodeImpl neighbor : node.incomingEdges.keySet()) {
				neighbors.addAll(node.incomingEdges.get(neighbor));
			}
		}
		if (types.equals(TiconeEdgeType.OUTGOING) || types.equals(TiconeEdgeType.ANY)) {
			for (final TiconeNetworkNodeImpl neighbor : node.outgoingEdges.keySet()) {
				neighbors.addAll(node.outgoingEdges.get(neighbor));
			}
		}
		if (types.equals(TiconeEdgeType.SELF) || types.equals(TiconeEdgeType.ANY)) {
			neighbors.addAll(node.selfEdges);
		}
		return neighbors;
	}

	public <N extends TiconeNetworkNodeImpl> Iterator<TiconeNetworkEdgeImpl> getAdjacentEdges(final N node,
			final TiconeEdgeType types) {
		return node.getAdjacentEdges(types);
	}

	@Override
	public int getAdjacentEdgeCount(final TiconeNetworkNodeImpl node, final TiconeEdgeType types) {
		int neighbors = 0;
		if (types.equals(TiconeEdgeType.INCOMING) || types.equals(TiconeEdgeType.ANY)) {
			for (final TiconeNetworkNodeImpl neighbor : node.incomingEdges.keySet()) {
				neighbors += node.incomingEdges.get(neighbor).size();
			}
		}
		if (types.equals(TiconeEdgeType.OUTGOING) || types.equals(TiconeEdgeType.ANY)) {
			for (final TiconeNetworkNodeImpl neighbor : node.outgoingEdges.keySet()) {
				neighbors += node.outgoingEdges.get(neighbor).size();
			}
		}
		if (types.equals(TiconeEdgeType.SELF) || types.equals(TiconeEdgeType.ANY)) {
			neighbors += node.selfEdges.size();
		}
		return neighbors;
	}

	public ObjectList<TiconeNetworkNodeImpl> getNeighbors(final TiconeNetworkNodeImpl node,
			final TiconeEdgeType types) {
		return node.getNeighbors(types);
	}

	@Override
	public <N extends TiconeNetworkNodeImpl> Collection<TiconeNetworkEdgeImpl> getEdges(final N n1, final N n2,
			final TiconeEdgeType types) {
		final Collection<TiconeNetworkEdgeImpl> result = new ArrayList<>();
		if (n1.equals(n2)) {
			if (types.equals(TiconeEdgeType.SELF) || types.equals(TiconeEdgeType.ANY)) {
				result.addAll(n1.selfEdges);
			}
		} else {
			if (types.equals(TiconeEdgeType.INCOMING) || types.equals(TiconeEdgeType.ANY)) {
				if (n1.incomingEdges.containsKey(n2))
					result.addAll(n1.incomingEdges.get(n2));
			}
			if (types.equals(TiconeEdgeType.OUTGOING) || types.equals(TiconeEdgeType.ANY)) {
				if (n1.outgoingEdges.containsKey(n2))
					result.addAll(n1.outgoingEdges.get(n2));
			}
		}
		return result;
	}

	@Override
	public <N extends TiconeNetworkNodeImpl> Set<TiconeNetworkEdgeImpl> getEdgesByNodeIds(final Set<String> nodeIds1,
			final Set<String> nodeIds2, final TiconeEdgeType type) {
		final Set<TiconeNetworkEdgeImpl> result = new HashSet<>();

		// we cache these values such that we do not have to check them over and
		// over again for each pair of nodes
		final Set<TiconeNetworkNodeImpl> hasSelfEdges = new HashSet<>(), hasOutgoingEdges = new HashSet<>(),
				hasIncomingEdges = new HashSet<>();
		for (final String n : nodeIds1) {
			final TiconeNetworkNodeImpl node = this.getNode(n);
			if (!node.selfEdges.isEmpty())
				hasSelfEdges.add(node);
			if (!node.incomingEdges.isEmpty())
				hasIncomingEdges.add(node);
			if (!node.outgoingEdges.isEmpty())
				hasOutgoingEdges.add(node);
		}

		// add self edges
		if (type.equals(TiconeEdgeType.SELF) || type.equals(TiconeEdgeType.ANY)) {
			for (final TiconeNetworkNodeImpl n1 : hasSelfEdges) {
				result.addAll(n1.selfEdges);
			}
		}

		// add outgoing edges
		if (type.equals(TiconeEdgeType.OUTGOING) || type.equals(TiconeEdgeType.ANY)) {
			for (final TiconeNetworkNodeImpl n1 : hasOutgoingEdges) {
				final Map<TiconeNetworkNodeImpl, Set<TiconeNetworkEdgeImpl>> edges = n1.outgoingEdges;
				for (final TiconeNetworkNodeImpl n2 : hasIncomingEdges) {
					if (edges.containsKey(n2))
						result.addAll(edges.get(n2));
				}
			}
		}
		// add incoming edges
		if (type.equals(TiconeEdgeType.INCOMING) || type.equals(TiconeEdgeType.ANY)) {
			for (final TiconeNetworkNodeImpl n1 : hasIncomingEdges) {
				final Map<TiconeNetworkNodeImpl, Set<TiconeNetworkEdgeImpl>> edges = n1.incomingEdges;
				for (final TiconeNetworkNodeImpl n2 : hasOutgoingEdges) {
					if (edges.containsKey(n2))
						result.addAll(edges.get(n2));
				}
			}
		}

		return result;

	}

	@Override
	public Set<Triple<String, Class, Boolean>> getNodeAttributes() {
		final Set<Triple<String, Class, Boolean>> result = new HashSet<>();
		for (final String attr : this.nodeAttributes.keySet())
			if (!this.nodeAttributes.get(attr).isEmpty())
				result.add(Triple.getTriple(attr,
						(Class) this.nodeAttributes.get(attr).values().iterator().next().getClass(), false));
			else
				result.add(Triple.getTriple(attr, (Class) Object.class, false));
		return result;
	}

	@Override
	public Set<Triple<String, Class, Boolean>> getEdgeAttributes() {
		final Set<Triple<String, Class, Boolean>> result = new HashSet<>();
		for (final String attr : this.edgeAttributes.keySet())
			if (!this.edgeAttributes.get(attr).isEmpty())
				result.add(Triple.getTriple(attr,
						(Class) this.edgeAttributes.get(attr).values().iterator().next().getClass(), false));
			else
				result.add(Triple.getTriple(attr, (Class) Object.class, false));
		return result;
	}

	@Override
	public Set<Triple<String, Class, Boolean>> getNetworkAttributes() {
		final Set<Triple<String, Class, Boolean>> result = new HashSet<>();
		for (final String attr : this.networkAttributes.keySet())
			if (this.networkAttributes.get(attr) != null)
				result.add(Triple.getTriple(attr, (Class) this.networkAttributes.get(attr).getClass(), false));
			else
				result.add(Triple.getTriple(attr, (Class) Object.class, false));
		return result;
	}

	@Override
	public TiconeNetworkImpl copy() {
		final TiconeNetworkImpl copy = TiconeNetworkImpl.getInstance(this, false);
		copy.isMultiGraph = this.isMultiGraph;
		// clone statistics
		// the ones which are not modified by edge crossovers we can just keep
		// by reference
		// copy.nodeDegreesUndirected = this.nodeDegreesUndirected;
		copy.maxUndirectedNodeDegree = this.maxUndirectedNodeDegree;
		copy.isNodeDegreesUndirectedInitialized = this.isNodeDegreesUndirectedInitialized;
		if (this.totalNodeDegreeCounts != null)
			copy.totalNodeDegreeCounts = Arrays.copyOf(this.totalNodeDegreeCounts, this.totalNodeDegreeCounts.length);

		if (this.totalDirectedEdgeCountNodeDegrees != null) {
			copy.totalDirectedEdgeCountNodeDegrees = new int[this.totalDirectedEdgeCountNodeDegrees.length][];
			for (int i = 0; i < this.totalDirectedEdgeCountNodeDegrees.length; i++)
				copy.totalDirectedEdgeCountNodeDegrees[i] = Arrays.copyOf(this.totalDirectedEdgeCountNodeDegrees[i],
						this.totalDirectedEdgeCountNodeDegrees[i].length);
		}
		if (this.totalUndirectedEdgeCountNodeDegrees != null) {
			copy.totalUndirectedEdgeCountNodeDegrees = new int[this.totalUndirectedEdgeCountNodeDegrees.length][];
			for (int i = 0; i < this.totalUndirectedEdgeCountNodeDegrees.length; i++)
				copy.totalUndirectedEdgeCountNodeDegrees[i] = Arrays.copyOf(this.totalUndirectedEdgeCountNodeDegrees[i],
						this.totalUndirectedEdgeCountNodeDegrees[i].length);
		}
		if (this.totalDirectedEdgeProbabilityNodeDegrees != null) {
			copy.totalDirectedEdgeProbabilityNodeDegrees = new double[this.totalDirectedEdgeProbabilityNodeDegrees.length][];
			for (int i = 0; i < this.totalDirectedEdgeProbabilityNodeDegrees.length; i++)
				copy.totalDirectedEdgeProbabilityNodeDegrees[i] = Arrays.copyOf(
						this.totalDirectedEdgeProbabilityNodeDegrees[i],
						this.totalDirectedEdgeProbabilityNodeDegrees[i].length);
		}
		if (this.totalUndirectedEdgeProbabilityNodeDegrees != null) {
			copy.totalUndirectedEdgeProbabilityNodeDegrees = new double[this.totalUndirectedEdgeProbabilityNodeDegrees.length][];
			for (int i = 0; i < this.totalUndirectedEdgeProbabilityNodeDegrees.length; i++)
				copy.totalUndirectedEdgeProbabilityNodeDegrees[i] = Arrays.copyOf(
						this.totalUndirectedEdgeProbabilityNodeDegrees[i],
						this.totalUndirectedEdgeProbabilityNodeDegrees[i].length);
		}
		return copy;
	}

	@Override
	public void performEdgeCrossovers(final double factorEdgeSwaps, final boolean isDirected, final long seed)
			throws InterruptedException {
		// avoid infinite loop for total graphs
		if (!this.isMultiGraph) {
			if (isDirected && this.edges.size() == this.nodes.size() * this.nodes.size())
				return;
			else if (!isDirected
					&& this.edges.size() == (this.nodes.size() * (this.nodes.size() - 1) / 2 + this.nodes.size())) {
				return;
			}
		}

		final Random random = new Random(seed);
		this.isEdgeCrossoverCancelled = false;

		final List<TiconeNetworkEdgeImpl> edgeList = this.getEdgeList();

		final BitMatrix connectedNodesArray = this.getConnectedNodesArray(!isDirected);

		// perform edge crossover
		for (int i = 0; i < Math.round((double) this.getEdgeCount() / 2 * factorEdgeSwaps); i++) {
			if (!Utility.getProgress().getStatus())
				throw new InterruptedException();
			if (this.isEdgeCrossoverCancelled)
				break;
			final int edgeInd1 = random.nextInt(edgeList.size());
			final int edgeInd2 = random.nextInt(edgeList.size());

			if (edgeInd1 == edgeInd2) {
				i--;
				continue;
			}

			final TiconeNetworkEdgeImpl edge1 = edgeList.get(edgeInd1);
			final TiconeNetworkEdgeImpl edge2 = edgeList.get(edgeInd2);

			TiconeNetworkNodeImpl s1 = edge1.getSource(), s2 = edge2.getSource(), t1 = edge1.getTarget(),
					t2 = edge2.getTarget();

			// cross-over of X->X and Y->Y changes NDD and inserts a duplicate
			// edge
			if (!isDirected && s1.equals(t1) && s2.equals(t2)) {
				i--;
				continue;
			}

			TiconeNetworkNodeImpl newS1, newS2, newT1, newT2;

			// interpret undirected edges as t1 -> s1
			if (!isDirected && random.nextDouble() < 0.5) {
				final TiconeNetworkNodeImpl tmp = s1;
				s1 = t1;
				t1 = tmp;
			}
			// interpret undirected edges as t2 -> s2
			if (!isDirected && random.nextDouble() < 0.5) {
				final TiconeNetworkNodeImpl tmp = s2;
				s2 = t2;
				t2 = tmp;
			}
			newS1 = s1;
			newS2 = s2;

			newT1 = t2;
			newT2 = t1;

			// do not insert duplicate edges
			// directed graph; edges already present?
			if (!this.isMultiGraph && isDirected && (connectedNodesArray.get(newS1.networkWideId, newT1.networkWideId)
					|| connectedNodesArray.get(newS2.networkWideId, newT2.networkWideId))) {
				i--;
				continue;
			}
			// undirected graph; edges already present?
			else if (!this.isMultiGraph
					&& (!isDirected && (connectedNodesArray.get(newS1.networkWideId, newT1.networkWideId)
							|| connectedNodesArray.get(newS2.networkWideId, newT2.networkWideId)
							|| connectedNodesArray.get(newT1.networkWideId, newS1.networkWideId)
							|| connectedNodesArray.get(newT2.networkWideId, newS2.networkWideId)))) {
				i--;
				continue;
			}
			// edges not present
			else {
				if (isDirected) {
					connectedNodesArray.put(edge1.getSource().networkWideId, edge1.getTarget().networkWideId, false);
					connectedNodesArray.put(edge2.getSource().networkWideId, edge2.getTarget().networkWideId, false);

					connectedNodesArray.put(newS1.networkWideId, newT1.networkWideId, true);
					connectedNodesArray.put(newS2.networkWideId, newT2.networkWideId, true);
				} else {
					connectedNodesArray.put(edge1.getSource().networkWideId, edge1.getTarget().networkWideId, false);
					connectedNodesArray.put(edge1.getTarget().networkWideId, edge1.getSource().networkWideId, false);

					connectedNodesArray.put(edge2.getSource().networkWideId, edge2.getTarget().networkWideId, false);
					connectedNodesArray.put(edge2.getTarget().networkWideId, edge2.getSource().networkWideId, false);

					if (newS1.networkWideId <= newT1.networkWideId)
						connectedNodesArray.put(newS1.networkWideId, newT1.networkWideId, true);
					else
						connectedNodesArray.put(newT1.networkWideId, newS1.networkWideId, true);
					if (newS2.networkWideId <= newT2.networkWideId)
						connectedNodesArray.put(newS2.networkWideId, newT2.networkWideId, true);
					else
						connectedNodesArray.put(newT2.networkWideId, newS2.networkWideId, true);
				}
			}

			edge1.setSource(newS1);
			edge2.setSource(newS2);
			edge1.setTarget(newT1);
			edge2.setTarget(newT2);
		}
		this.initializeEdgeDatastructures();
	}

	/**
	 * 
	 */
	@Override
	protected void initializeEdgeDatastructures() {
		this.edgeAttributes = new HashMap<>();
		super.initializeEdgeDatastructures();
	}

	public void clearStatistics() {
		this.totalNodeDegreeCounts = null;
		this.totalDirectedEdgeCountNodeDegrees = null;
		this.totalDirectedEdgeProbabilityNodeDegrees = null;
		this.totalUndirectedEdgeCountNodeDegrees = null;
		this.totalUndirectedEdgeProbabilityNodeDegrees = null;
		this.nearestNeighborsJoinedNodeDegreesDirected = null;
		this.nearestNeighborsJoinedNodeDegreesUndirected = null;
	}

	public KdTree<double[]> getNearestNeighborsJoinedNodeDegrees(final List<int[]> nodeDegreesEdgeCounts,
			final boolean isDirected) {
		if (isDirected) {
			if (this.nearestNeighborsJoinedNodeDegreesDirected == null) {
				this.nearestNeighborsJoinedNodeDegreesDirected = new KdTree<>(2);
				for (final int[] degs : nodeDegreesEdgeCounts) {
					final double[] p = new double[] { degs[0], degs[1] };
					this.nearestNeighborsJoinedNodeDegreesDirected.addPoint(p, p);
				}
			}
			return this.nearestNeighborsJoinedNodeDegreesDirected;
		} else {
			if (this.nearestNeighborsJoinedNodeDegreesUndirected == null) {
				this.nearestNeighborsJoinedNodeDegreesUndirected = new KdTree<>(2);
				for (final int[] degs : nodeDegreesEdgeCounts) {
					final double[] p = new double[] { degs[0], degs[1] };
					this.nearestNeighborsJoinedNodeDegreesUndirected.addPoint(p, p);
				}
			}
			return this.nearestNeighborsJoinedNodeDegreesUndirected;
		}
	}

	public int[] getNodeDegreesUndirected() {
		final int[] result = new int[this.nodes.size()];
		final Iterator<TiconeNetworkNodeImpl> it = this.nodes.iterator();
		for (int i = 0; i < this.nodes.size(); i++) {
			final TiconeNetworkNodeImpl n = it.next();
			result[i] = n.undirectedDegree;
		}
		return result;
	}

	public int[][] getTotalDirectedEdgeCountNodeDegrees() {
		if (this.totalDirectedEdgeCountNodeDegrees == null) {
			final int maxDeg = this.maxUndirectedNodeDegree;
			final int[][] counts = new int[maxDeg + 1][maxDeg + 1];

			for (final TiconeNetworkEdgeImpl e : this.getEdgeList()) {
				// TODO: why was this in here?
				// if (e.getSource().equals(e.getTarget()))
				// continue;
				final int degree1 = this.getUndirectedNodeDegree(e.getSource());
				final int degree2 = this.getUndirectedNodeDegree(e.getTarget());
				counts[degree1][degree2]++;
			}
			this.totalDirectedEdgeCountNodeDegrees = counts;
		}
		return this.totalDirectedEdgeCountNodeDegrees;
	}

	public double[][] getTotalDirectedEdgeProbabilityNodeDegrees() {
		if (this.totalDirectedEdgeProbabilityNodeDegrees == null) {
			final int[][] edgeCounts = this.getTotalDirectedEdgeCountNodeDegrees();

			final double[][] edgeProbs = new double[edgeCounts.length][];
			for (int degree1 = 0; degree1 < edgeCounts.length; degree1++) {
				edgeProbs[degree1] = new double[edgeCounts[degree1].length];
				for (int degree2 = 0; degree2 < edgeCounts[degree1].length; degree2++) {
					if (edgeCounts[degree1][degree2] == 0)
						continue;
					double prob;
					final int possible = this.getTheoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees(degree1,
							degree2);
					if (possible == 0) {
						prob = 0.0;
					}
					// } else if (degree1 != degree2)
					// prob = (edgeCounts[degree1][degree2] +
					// (edgeCounts[degree2][degree1])) / (double) possible;
					else
						prob = edgeCounts[degree1][degree2] / (double) possible;
					edgeProbs[degree1][degree2] = prob;
				}
			}
			this.totalDirectedEdgeProbabilityNodeDegrees = edgeProbs;
		}
		return this.totalDirectedEdgeProbabilityNodeDegrees;
	}

	public int[] getTotalNodeDegreeCounts() {
		if (this.totalNodeDegreeCounts == null) {
			final List<Integer> degreeList = new ArrayList<>();
			final List<TiconeNetworkNodeImpl> nodes = this.getNodeList();
			for (int i = 0; i < nodes.size(); i++) {
				final TiconeNetworkNodeImpl n1 = nodes.get(i);
				final int degree = this.getUndirectedNodeDegree(n1);
				degreeList.add(degree);
			}
			final int[] result = new int[Collections.max(degreeList) + 1];
			for (final int degree : degreeList)
				result[degree]++;

			this.totalNodeDegreeCounts = result;
		}
		return this.totalNodeDegreeCounts;
	}

	public synchronized BitMatrix getConnectedNodesArray(final boolean isUndirected) {
		if (isUndirected && this.undirectedConnectedNodes != null)
			return this.undirectedConnectedNodes;
		else if (!isUndirected && this.directedConnectedNodes != null)
			return this.directedConnectedNodes;

		BitMatrix connectedNodes;
		if (isUndirected) {
			connectedNodes = new BitMatrix(this.nextNetworkWideNodeId.get(), this.nextNetworkWideNodeId.get());
			for (final TiconeNetworkEdgeImpl e : this.getEdgeList()) {
				final TiconeNetworkNodeImpl source = e.getSource();
				final TiconeNetworkNodeImpl target = e.getTarget();

				if (source.networkWideId <= target.networkWideId)
					connectedNodes.put(source.networkWideId, target.networkWideId, true);
				else
					connectedNodes.put(target.networkWideId, source.networkWideId, true);
			}
			this.undirectedConnectedNodes = connectedNodes;
		} else {
			connectedNodes = new BitMatrix(this.nextNetworkWideNodeId.get(), this.nextNetworkWideNodeId.get());
			for (final TiconeNetworkEdgeImpl e : this.getEdgeList()) {
				final TiconeNetworkNodeImpl source = e.getSource();
				final TiconeNetworkNodeImpl target = e.getTarget();

				connectedNodes.put(source.networkWideId, target.networkWideId, true);
			}
			this.directedConnectedNodes = connectedNodes;
		}
		return connectedNodes;
	}

	/**
	 * The resulting map contains for each pair of possible joined node degrees the
	 * count of undirected edges between them. For each joined node degree this map
	 * only contains one direction, i.e. minDegree -> maxDegree -> edgeCount. Thus,
	 * the total number of edge counts in this map equals the total number of
	 * undirected edges in the network.
	 * 
	 * @param network
	 * @return
	 */
	public int[][] getTotalUndirectedEdgeCountNodeDegrees() {
		if (this.totalUndirectedEdgeCountNodeDegrees == null) {
			final int maxDeg = this.maxUndirectedNodeDegree;
			final int[][] result = new int[maxDeg + 1][maxDeg + 1];
			final BitMatrix connectedNodes = this.getConnectedNodesArray(true);

			for (int i = 0; i < connectedNodes.rows(); i++) {
				final TiconeNetworkNodeImpl node1 = this.getNetworkWideNodeIdToNode().get(i);
				if (node1 == null)
					continue;
				for (int j = i; j < connectedNodes.columns(); j++) {
					if (!connectedNodes.get(i, j))
						continue;
					final TiconeNetworkNodeImpl node2 = this.getNetworkWideNodeIdToNode().get(j);
					if (node2 == null)
						continue;
					final int degree1 = this.getUndirectedNodeDegree(node1);
					final int degree2 = this.getUndirectedNodeDegree(node2);

					// for each undirected edge we add one count for
					// minDegree->maxDegree
					final int minDegree = Math.min(degree1, degree2);
					final int maxDegree = Math.max(degree1, degree2);

					result[minDegree][maxDegree]++;
				}
			}

			// // we have counted self-edges twice
			// for (int i = 0; i < result.length; i++)
			// result[i][i] /= 2;

			this.totalUndirectedEdgeCountNodeDegrees = result;
		}
		return this.totalUndirectedEdgeCountNodeDegrees;
	}

	/**
	 * The resulting map contains for each pair of possible joined node degrees the
	 * probability of an undirected edge between them. For each joined node degree
	 * this map only contains one direction, i.e. minDegree -> maxDegree -> P(edge).
	 * 
	 * @param network
	 * @return
	 */
	public double[][] getTotalUndirectedEdgeProbabilityNodeDegrees() {
		if (this.totalUndirectedEdgeProbabilityNodeDegrees == null) {
			final int[][] edgeCounts = this.getTotalUndirectedEdgeCountNodeDegrees();

			final double[][] edgeProbs = new double[edgeCounts.length][];
			for (int degree1 = 0; degree1 < edgeCounts.length; degree1++) {
				edgeProbs[degree1] = new double[edgeCounts[degree1].length];
				for (int degree2 = degree1; degree2 < edgeCounts[degree1].length; degree2++) {
					if (edgeCounts[degree1][degree2] == 0)
						continue;
					final int theo = this.getTheoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees(degree1,
							degree2);
					final double prob = theo > 0 ? edgeCounts[degree1][degree2] / (double) theo : 0.0;
					edgeProbs[degree1][degree2] = prob;
				}
			}
			this.totalUndirectedEdgeProbabilityNodeDegrees = edgeProbs;
		}
		return this.totalUndirectedEdgeProbabilityNodeDegrees;

	}

	public int getUndirectedNodeDegree(final TiconeNetworkNodeImpl node) {
		return node.undirectedDegree;
	}

	public void initNodeDegreesUndirected() {
		this.initNodeDegreesUndirected(false);
	}

	public void initNodeDegreesUndirected(final boolean force) {
		if (force || !this.isNodeDegreesUndirectedInitialized) {
			int maxDeg = 0;
			for (final TiconeNetworkNodeImpl node : this.getNodeList()) {
				final int deg = this.getAdjacentEdgeCount(node, TiconeEdgeType.ANY);
				node.setUndirectedDegree(deg);
				if (deg > maxDeg)
					maxDeg = deg;
			}
			this.isNodeDegreesUndirectedInitialized = true;
			this.maxUndirectedNodeDegree = maxDeg;
		}
	}

	public int getTheoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees(final int degree1, final int degree2) {
		if (this.theoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees == null) {
			this.theoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees = new int[this.maxUndirectedNodeDegree
					+ 1][this.maxUndirectedNodeDegree + 1];
			for (int i = 0; i < this.theoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees.length; i++) {
				for (int j = 0; j < this.theoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees[i].length; j++) {
					this.theoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees[i][j] = -1;
				}
			}
		}
		final int tmp = this.theoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees[degree1][degree2];
		if (tmp > -1)
			return tmp;
		int result;
		if (degree1 != degree2)
			result = this.getTotalNodeDegreeCounts()[degree1] * this.totalNodeDegreeCounts[degree2];
		else
			result = this.getTotalNodeDegreeCounts()[degree1] * this.totalNodeDegreeCounts[degree1];
		this.theoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees[degree1][degree2] = result;
		return result;
	}

	public int getTheoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees(final int degree1, final int degree2) {
		if (this.theoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees == null) {
			this.theoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees = new int[this.maxUndirectedNodeDegree
					+ 1][this.maxUndirectedNodeDegree + 1];
			for (int i = 0; i < this.theoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees.length; i++) {
				for (int j = 0; j < this.theoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees[i].length; j++) {
					this.theoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees[i][j] = -1;
				}
			}
		}
		final int tmp = this.theoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees[degree1][degree2];
		if (tmp > -1)
			return tmp;
		int result;
		if (degree1 != degree2) {
			result = this.getTotalNodeDegreeCounts()[degree1] * this.totalNodeDegreeCounts[degree2];
		} else
			result = (int) (this.getTotalNodeDegreeCounts()[degree1] * (this.totalNodeDegreeCounts[degree1] - 1) / 2.0
					+ this.totalNodeDegreeCounts[degree1]);
		this.theoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees[degree1][degree2] = result;
		return result;
	}

	public void cancelEdgeCrossover() {
		this.isEdgeCrossoverCancelled = true;
	}

	public void removeNodesNotInDataset(final ITimeSeriesObjects dataSet) {
		final Set<String> dataSetIds = new HashSet<>();
		for (final ITimeSeriesObject o : dataSet)
			dataSetIds.add(o.getName());
		final Set<TiconeNetworkNodeImpl> removedNodes = new HashSet<>();
		// remove nodes from network, which are not in the dataset
		for (final TiconeNetworkNodeImpl node : new HashSet<>(this.getNodeSet())) {
			if (!dataSetIds.contains(node.getName())) {
				this.removeNode(node);
				removedNodes.add(node);
			}
		}
		logger.info("Removed " + removedNodes.size() + " nodes from network not contained in dataset.");
	}

	protected void ensureObjectIsKnown(final IObjectWithFeatures pair, final ITimeSeriesObject object)
			throws UnknownObjectFeatureValueProviderException {
		if (object instanceof NetworkMappedTimeSeriesObject) {
			if (!((NetworkMappedTimeSeriesObject) object).isMappedTo(this))
				throw new UnknownObjectFeatureValueProviderException(this, pair != null ? pair : object,
						"Object is linked to a different network");
		} else if (!this.containsNode(object.getName()))
			throw new UnknownObjectFeatureValueProviderException(this, pair != null ? pair : object,
					String.format("No node found with ID %s", object.getName()));
	}

	protected void ensureObjectIsKnown(final IObjectWithFeatures pair,
			final Collection<? extends INetworkMappedTimeSeriesObject> nodes)
			throws UnknownObjectFeatureValueProviderException {
		for (INetworkMappedTimeSeriesObject node : nodes) {
			if (!node.isMappedTo(this))
				throw new UnknownObjectFeatureValueProviderException(this, pair,
						"Object has not been mapped to this network");
			else if (!this.containsNode(node.getName()))
				throw new UnknownObjectFeatureValueProviderException(this, pair,
						String.format("No node found with ID %s", node.getName()));
		}
	}

	@Override
	public void ensureObjectIsKnown(IObjectWithFeatures object) throws UnknownObjectFeatureValueProviderException {
		if (object instanceof IObjectPair) {
			this.ensureObjectIsKnown(object, ((IObjectPair) object).getFirst());
			this.ensureObjectIsKnown(object, ((IObjectPair) object).getSecond());
		} else if (object instanceof IClusterPair) {
			final IClusterPair clusterPair = (IClusterPair) object;
			try {
				final Collection<? extends INetworkMappedTimeSeriesObject> n1 = PrototypeComponentType.NETWORK_LOCATION
						.getComponent(clusterPair.getFirst().getPrototype()).getNetworkLocation();
				this.ensureObjectIsKnown(clusterPair, n1);

				final Collection<? extends INetworkMappedTimeSeriesObject> n2 = PrototypeComponentType.NETWORK_LOCATION
						.getComponent(clusterPair.getSecond().getPrototype()).getNetworkLocation();
				this.ensureObjectIsKnown(clusterPair, n2);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				throw new UnknownObjectFeatureValueProviderException(this, clusterPair,
						"At least one of the specified clusters does not have a network location prototype component");
			}
		} else if (object instanceof IObjectClusterPair) {
			final IObjectClusterPair objectClusterPair = (IObjectClusterPair) object;
			try {
				final ITimeSeriesObject o = objectClusterPair.getObject();
				final Collection<? extends INetworkMappedTimeSeriesObject> n = PrototypeComponentType.NETWORK_LOCATION
						.getComponent(objectClusterPair.getCluster().getPrototype()).getNetworkLocation();

				this.ensureObjectIsKnown(objectClusterPair, o);
				this.ensureObjectIsKnown(objectClusterPair, n);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				throw new UnknownObjectFeatureValueProviderException(this, objectClusterPair,
						"The specified cluster does not have a network location prototype component");
			}

		} else if (object instanceof ITimeSeriesObject) {
			this.ensureObjectIsKnown(null, (ITimeSeriesObject) object);
		}
	}

	private ShortestDistanceCalculator getShortestDistanceCalculatorDirected() throws InterruptedException {
		return this.shortestDistanceCalculatorDirectedIgnore;
	}

	private ShortestDistanceCalculator getShortestDistanceCalculatorUndirected() throws InterruptedException {
		return this.shortestDistanceCalculatorUndirectedIgnore;
	}

	class ClusterPairFeatureNumberDirectedConnectingEdgesCalculator implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -454998503334480298L;

		IFeatureValue<Double> calculate(final IClusterPair pair,
				final IClusterPairFeatureNumberDirectedConnectingEdges feature)
				throws IncompatibleFeatureAndObjectException {

			final BitMatrix connectedNodes = TiconeNetworkImpl.this.getConnectedNodesArray(false);

			double edges = 0;

			final ITimeSeriesObjects objs1 = pair.getFirst().getObjects();
			final ITimeSeriesObjects objs2 = pair.getSecond().getObjects();

			for (final ITimeSeriesObject o1 : objs1) {
				final int id1 = this.getNode(o1).networkWideId;
				for (final ITimeSeriesObject o2 : objs2) {
					final int id2 = this.getNode(o2).networkWideId;
					if (connectedNodes.get(id1, id2))
						edges++;
				}
			}

			if (feature.isScaleByClusterSizes()) {
				if (pair.getFirst().equals(pair.getSecond()))
					edges /= objs1.size() * objs2.size();
				else
					edges /= (2 * objs1.size() * objs2.size());
			}

			return feature.value(pair, edges);
		}

		public TiconeNetworkNodeImpl getNode(final ITimeSeriesObject object) {
			TiconeNetworkNodeImpl result;
			if (object instanceof NetworkMappedTimeSeriesObject && ((NetworkMappedTimeSeriesObject) object)
					.getNode(TiconeNetworkImpl.this) instanceof TiconeNetworkNodeImpl)
				result = (TiconeNetworkNodeImpl) ((NetworkMappedTimeSeriesObject) object)
						.getNode(TiconeNetworkImpl.this);
			else
				result = TiconeNetworkImpl.this.getNode(object.getName());
			return result;
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}
	}

	class ClusterPairFeatureNumberUndirectedConnectingEdgesCalculator implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -890471625325404265L;

		IFeatureValue<Double> calculate(final IClusterPair pair,
				final IClusterPairFeatureNumberUndirectedConnectingEdges feature)
				throws ObjectNotInNetworkException, IncompatibleFeatureAndObjectException {

			final BitMatrix connectedNodes = TiconeNetworkImpl.this.getConnectedNodesArray(true);

			double edges = 0;

			final ICluster c1 = pair.getFirst();
			final ITimeSeriesObjects objs1 = c1.getObjects();
			final ICluster c2 = pair.getSecond();
			final ITimeSeriesObjects objs2 = c2.getObjects();

			for (final ITimeSeriesObject o1 : objs1) {
				final int id1 = this.getNode(o1).networkWideId;
				for (final ITimeSeriesObject o2 : objs2) {
					final int id2 = this.getNode(o2).networkWideId;
					if (id1 <= id2) {
						if (connectedNodes.get(id1, id2))
							edges++;
					} else if (!c1.equals(c2)) {
						if (connectedNodes.get(id2, id1))
							edges++;
					}
				}
			}

			if (feature.isScaleByClusterSizes()) {
				if (pair.getFirst().equals(pair.getSecond()))
					edges /= ((objs1.size() * (objs1.size() - 1)) / 2) + objs1.size();
				else
					edges /= (objs1.size() * objs2.size());
			}

			return feature.value(pair, edges);
		}

		public TiconeNetworkNodeImpl getNode(final ITimeSeriesObject object) {
			TiconeNetworkNodeImpl result;
			if (object instanceof NetworkMappedTimeSeriesObject && ((NetworkMappedTimeSeriesObject) object)
					.getNode(TiconeNetworkImpl.this) instanceof TiconeNetworkNodeImpl)
				result = (TiconeNetworkNodeImpl) ((NetworkMappedTimeSeriesObject) object)
						.getNode(TiconeNetworkImpl.this);
			else
				result = TiconeNetworkImpl.this.getNode(object.getName());
			return result;
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}
	}

	@Override
	public Collection<? extends IFeatureWithValueProvider> featuresProvidedValuesFor(ObjectType<?> objectType)
			throws IncompatibleFeatureValueProviderException {
		if (objectType.equals(ObjectType.CLUSTER_PAIR))
			return Arrays.asList(new ClusterPairFeatureNumberDirectedConnectingEdges(),
					new ClusterPairFeatureNumberUndirectedConnectingEdges(),
					new ClusterPairFeatureShortestDistance(true), new ClusterPairFeatureShortestDistance(false));
		else if (objectType.equals(ObjectType.OBJECT_PAIR))
			return Arrays.asList(new ObjectPairFeatureShortestDistance(true),
					new ObjectPairFeatureShortestDistance(false));
		else if (objectType.equals(ObjectType.OBJECT_CLUSTER_PAIR))
			return Arrays.asList(new ObjectClusterFeatureShortestDistance(true),
					new ObjectClusterFeatureShortestDistance(false));
		return IFeatureValueProvider.super.featuresProvidedValuesFor(objectType);
	}

	@Override
	public <V> IFeatureValue<V> getFeatureValue(IFeature<V> feature, IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException, UnknownObjectFeatureValueProviderException,
			FeatureCalculationException, InterruptedException, IncompatibleFeatureAndObjectException {
		if (feature instanceof IClusterPairFeatureNumberDirectedConnectingEdges) {
			IClusterPair p = (IClusterPair) object;
			for (ITimeSeriesObject o : p.getFirst().getObjects())
				this.ensureObjectIsKnown(o);
			for (ITimeSeriesObject o : p.getSecond().getObjects())
				this.ensureObjectIsKnown(o);
			return (IFeatureValue<V>) this.clusterPairFeatureNumberDirectedConnectingEdgesCalculator
					.calculate((IClusterPair) object, (IClusterPairFeatureNumberDirectedConnectingEdges) feature);
		} else if (feature instanceof IClusterPairFeatureNumberUndirectedConnectingEdges) {
			IClusterPair p = (IClusterPair) object;
			for (ITimeSeriesObject o : p.getFirst().getObjects())
				this.ensureObjectIsKnown(o);
			for (ITimeSeriesObject o : p.getSecond().getObjects())
				this.ensureObjectIsKnown(o);
			try {
				return (IFeatureValue<V>) this.clusterPairFeatureNumberUndirectedConnectingEdgesCalculator
						.calculate((IClusterPair) object, (IClusterPairFeatureNumberUndirectedConnectingEdges) feature);
			} catch (ObjectNotInNetworkException e) {
				throw new FeatureCalculationException(e);
			}
		} else if (feature instanceof IObjectPairFeatureShortestDistance) {
			if (((IObjectPairFeatureShortestDistance) feature).isEnsureKnownObjects())
				this.ensureObjectIsKnown(object);

			if (((IObjectPairFeatureShortestDistance) feature).isDirected())
				return (IFeatureValue<V>) this.getShortestDistanceCalculatorDirected().calculate((IObjectPair) object,
						(IObjectPairFeatureShortestDistance) feature);
			else {
				return (IFeatureValue<V>) this.getShortestDistanceCalculatorUndirected().calculate((IObjectPair) object,
						(IObjectPairFeatureShortestDistance) feature);
			}
		} else if (feature instanceof IClusterPairFeatureShortestPath) {
			if (((IClusterPairFeatureShortestPath) feature).isEnsureKnownObjects())
				this.ensureObjectIsKnown(object);

			if (((IClusterPairFeatureShortestPath) feature).isDirected())
				return (IFeatureValue<V>) this.getShortestDistanceCalculatorDirected().calculate((IClusterPair) object,
						(IClusterPairFeatureShortestPath) feature);
			else {
				return (IFeatureValue<V>) this.getShortestDistanceCalculatorUndirected()
						.calculate((IClusterPair) object, (IClusterPairFeatureShortestPath) feature);
			}
		} else if (feature instanceof IObjectClusterFeatureShortestPath) {
			if (((IObjectClusterFeatureShortestPath) feature).isEnsureKnownObjects())
				this.ensureObjectIsKnown(object);

			if (((IObjectClusterFeatureShortestPath) feature).isDirected())
				return (IFeatureValue<V>) this.getShortestDistanceCalculatorDirected()
						.calculate((IObjectClusterPair) object, (IObjectClusterFeatureShortestPath) feature);
			else {
				try {
					return (IFeatureValue<V>) this.getShortestDistanceCalculatorUndirected()
							.calculate((IObjectClusterPair) object, (IObjectClusterFeatureShortestPath) feature);
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
			}
		}
		throw new IncompatibleFeatureValueProviderException(this, feature);
	}

	@Override
	public boolean initializeForFeature(IFeature<?> feature) {
		if (feature instanceof IClusterPairFeatureShortestPath) {
			try {
				if (((IClusterPairFeatureShortestPath) feature).isDirected()) {
					if (this.shortestDistanceCalculatorDirectedIgnore == null)
						this.shortestDistanceCalculatorDirectedIgnore = new ShortestDistanceCalculator(true);
				} else {
					if (this.shortestDistanceCalculatorUndirectedIgnore == null)
						this.shortestDistanceCalculatorUndirectedIgnore = new ShortestDistanceCalculator(false);
				}
				return true;
			} catch (InterruptedException e) {
				return false;
			}
		} else if (feature instanceof IObjectClusterFeatureShortestPath) {
			try {
				if (((IObjectClusterFeatureShortestPath) feature).isDirected()) {
					if (this.shortestDistanceCalculatorDirectedIgnore == null)
						this.shortestDistanceCalculatorDirectedIgnore = new ShortestDistanceCalculator(true);
				} else {
					if (this.shortestDistanceCalculatorUndirectedIgnore == null)
						this.shortestDistanceCalculatorUndirectedIgnore = new ShortestDistanceCalculator(false);
				}
				return true;
			} catch (InterruptedException e) {
				return false;
			}
		} else if (feature instanceof IObjectPairFeatureShortestDistance) {
			try {
				if (((IObjectPairFeatureShortestDistance) feature).isDirected()) {
					if (this.shortestDistanceCalculatorDirectedIgnore == null)
						this.shortestDistanceCalculatorDirectedIgnore = new ShortestDistanceCalculator(true);
				} else {
					if (this.shortestDistanceCalculatorUndirectedIgnore == null)
						this.shortestDistanceCalculatorUndirectedIgnore = new ShortestDistanceCalculator(false);
				}
				return true;
			} catch (InterruptedException e) {
				return false;
			}
		}
		return false;
	}

	@Override
	protected TiconeNetworkNodeImpl getNode(final ITiconeNetworkNode node) {
		if (node instanceof TiconeNetworkNodeImpl)
			return (TiconeNetworkNodeImpl) node;

		return TiconeNetworkImpl.this.getNode(node.getName());
	}

	@Override
	protected TiconeNetworkNodeImpl getNode(final ITimeSeriesObject object) throws ObjectNotInNetworkException {
		TiconeNetworkNodeImpl result;
		if (object instanceof NetworkMappedTimeSeriesObject && ((NetworkMappedTimeSeriesObject) object)
				.getNode(TiconeNetworkImpl.this) instanceof TiconeNetworkNodeImpl)
			result = (TiconeNetworkNodeImpl) ((NetworkMappedTimeSeriesObject) object).getNode(TiconeNetworkImpl.this);
		else
			result = TiconeNetworkImpl.this.getNode(object.getName());
		if (result == null)
			throw new ObjectNotInNetworkException(object.getName(), this);
		return result;
	}

	public class TiconeNetworkNodeImpl
			extends TiconeNetwork<TiconeNetworkNodeImpl, TiconeNetworkEdgeImpl>.TiconeNetworkNode {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3015879072850675348L;
		protected int undirectedDegree;

		TiconeNetworkNodeImpl(final String name, final int networkWideId) {
			this(name, null, networkWideId);
		}

		TiconeNetworkNodeImpl(final String name, final String alternativeName, final int networkWideId) {
			super(name, alternativeName);
			this.networkWideId = networkWideId;
		}

		@Override
		public TiconeNetworkImpl getNetwork() {
			return TiconeNetworkImpl.this;
		}

		@Override
		public String toString() {
			return this.name;
		}

		@Override
		public String getName() {
			return this.name;
		}

		public void setUndirectedDegree(final int undirectedDegree) {
			this.undirectedDegree = undirectedDegree;
		}

		public int getUndirectedDegree() {
			return this.undirectedDegree;
		}

		/**
		 * @return the incomingEdges
		 */
		public Map<TiconeNetworkNodeImpl, Set<TiconeNetworkEdgeImpl>> getIncomingEdges() {
			return this.incomingEdges;
		}

		/**
		 * @return the outgoingEdges
		 */
		public Map<TiconeNetworkNodeImpl, Set<TiconeNetworkEdgeImpl>> getOutgoingEdges() {
			return this.outgoingEdges;
		}

		/**
		 * @return the selfEdges
		 */
		public Set<TiconeNetworkEdgeImpl> getSelfEdges() {
			return this.selfEdges;
		}

		public <N extends TiconeNetworkNodeImpl> Iterator<TiconeNetworkEdgeImpl> getAdjacentEdges(
				final TiconeEdgeType types) {
			final List<Iterator<TiconeNetworkEdgeImpl>> neighbors = new ArrayList<>();
			if (types.equals(TiconeEdgeType.INCOMING) || types.equals(TiconeEdgeType.ANY)) {
				for (final TiconeNetworkNodeImpl neighbor : this.incomingEdges.keySet()) {
					neighbors.add(this.incomingEdges.get(neighbor).iterator());
				}
			}
			if (types.equals(TiconeEdgeType.OUTGOING) || types.equals(TiconeEdgeType.ANY)) {
				for (final TiconeNetworkNodeImpl neighbor : this.outgoingEdges.keySet()) {
					neighbors.add(this.outgoingEdges.get(neighbor).iterator());
				}
			}
			if (types.equals(TiconeEdgeType.SELF) || types.equals(TiconeEdgeType.ANY)) {
				neighbors.add(this.selfEdges.iterator());
			}
			return new Iterator<TiconeNetworkEdgeImpl>() {

				protected Iterator<TiconeNetworkEdgeImpl> currentIterator;

				final protected Iterator<Iterator<TiconeNetworkEdgeImpl>> iteratorIterator = neighbors.iterator();

				@Override
				public boolean hasNext() {
					while (this.currentIterator == null || !this.currentIterator.hasNext()) {
						if (!this.iteratorIterator.hasNext())
							return false;
						this.currentIterator = this.iteratorIterator.next();
					}
					return true;
				}

				@Override
				public TiconeNetworkEdgeImpl next() {
					return this.currentIterator.next();
				}
			};
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}

	}

	public class TiconeNetworkEdgeImpl
			extends TiconeNetwork<TiconeNetworkNodeImpl, TiconeNetworkEdgeImpl>.TiconeNetworkEdge {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2815991357307288617L;

		TiconeNetworkEdgeImpl(final TiconeNetworkNodeImpl source, final TiconeNetworkNodeImpl target,
				final boolean isDirected, final long networkWideId) {
			super(source, target, isDirected, networkWideId);
		}

		public TiconeNetworkImpl getNetwork() {
			return TiconeNetworkImpl.this;
		}

		@Override
		public String toString() {
			if (this.isDirected)
				return this.source.name + " -> " + this.target.name;
			return this.source.name + " <-> " + this.target.name;
		}

		public void setSource(final TiconeNetworkNodeImpl source) {
			this.source = source;
		}

		public void setTarget(final TiconeNetworkNodeImpl target) {
			this.target = target;
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}
	}

	private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
	}
}
