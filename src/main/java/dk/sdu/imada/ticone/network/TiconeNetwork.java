package dk.sdu.imada.ticone.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IClusterPairFeatureShortestPath;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectClusterFeatureShortestPath;
import dk.sdu.imada.ticone.feature.IObjectPairFeatureShortestDistance;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.network.TiconeNetwork.ShortestDistanceCalculator;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.DoubleIndexMinPQ;
import dk.sdu.imada.ticone.util.MyParallel;
import dk.sdu.imada.ticone.util.MyParallel.Operation;
import dk.sdu.imada.ticone.util.MyScheduledThreadPoolExecutor;
import dk.sdu.imada.ticone.util.Progress;
import dk.sdu.imada.ticone.util.Utility;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;

public abstract class TiconeNetwork<TICONE_NODE extends TiconeNetwork<TICONE_NODE, TICONE_EDGE>.TiconeNetworkNode, TICONE_EDGE extends TiconeNetwork<TICONE_NODE, TICONE_EDGE>.TiconeNetworkEdge>
		implements ITiconeNetwork<TICONE_NODE, TICONE_EDGE> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4468637669314201260L;

	protected static AtomicLong nextNetworkUID = new AtomicLong();
	protected static AtomicLong nextConnectedComponentUID = new AtomicLong();
	protected static AtomicLong nextNodeUID = new AtomicLong();
	protected static AtomicLong nextEdgeUID = new AtomicLong();

	protected transient Logger logger;
	protected long uid;
	protected String name;

	protected int expectedNodeDegree = 100, expectedNumberNodes = 5000;

	protected TICONE_NODE[] networkWideNodeIdToNode;

	protected ArrayList<TICONE_NODE> networkWideNodeIdToNodeList;

	protected AtomicInteger nextNetworkWideNodeId = new AtomicInteger();
	protected AtomicLong nextNetworkWideEdgeId = new AtomicLong();
	protected AtomicInteger nextNetworkWideConnectedComponentId = new AtomicInteger();

	final protected Set<TICONE_NODE> nodes;
	protected Map<String, TICONE_NODE> nodeIdToNode;
	protected transient Set<ConnectedComponent> connectedComponents;

	final protected Set<TICONE_EDGE> edges;

	final protected Map<TICONE_EDGE, EdgeWrapper> edgeToEdgeWrapper;
	final protected Map<EdgeWrapper, TICONE_EDGE> edgeWrapperToEdge;

	private final boolean isCopy;
	transient ScheduledThreadPoolExecutor privateThreadPool;

	protected boolean isMultiGraph = false;

	public <TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>> TiconeNetwork(
			final ITiconeNetwork<TICONE_NODE, TICONE_EDGE> origNetwork) {
		this(origNetwork.getName(),
				origNetwork instanceof TiconeNetwork ? ((TiconeNetwork) origNetwork).privateThreadPool : null);
	}

	public TiconeNetwork(final String name) {
		this(name, null);
	}

	TiconeNetwork(final String name, final ScheduledThreadPoolExecutor privateThreadPool) {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.name = name;
		this.uid = nextNetworkUID.getAndIncrement();
		this.nodes = new HashSet<>();
		this.nodeIdToNode = new HashMap<>(10000);

		this.edges = new HashSet<>();
		this.edgeWrapperToEdge = new HashMap<>();
		this.edgeToEdgeWrapper = new HashMap<>();

		this.isCopy = privateThreadPool != null;
		if (privateThreadPool == null)
			initThreadPool();
		else
			this.privateThreadPool = privateThreadPool;
	}

	@Override
	public boolean equals(Object obj) {
		if (!Objects.equals(getClass(), obj.getClass()))
			return false;
		return this.uid == ((TiconeNetwork) obj).uid;
	}

	@Override
	public int hashCode() {
		return Long.hashCode(this.uid);
	}

	/**
	 * @return the uid
	 */
	@Override
	public long getUID() {
		return this.uid;
	}

	protected abstract TICONE_NODE getNode(final ITiconeNetworkNode node);

	protected abstract TICONE_NODE getNode(final ITimeSeriesObject object) throws ObjectNotInNetworkException;

	protected void insertNodeIntoNodeDatastructures(final TICONE_NODE node) {
		this.nodes.add(node);
		this.nodeIdToNode.put(node.name, node);
		if (node.networkWideId >= this.nextNetworkWideNodeId.get())
			this.nextNetworkWideNodeId.set(node.networkWideId + 1);
	}

	protected void insertNodeIntoEdgeDatastructures(final TICONE_NODE node) {
//		this.incomingEdges.put(node, node.incomingEdges);
//		this.outgoingEdges.put(node, node.outgoingEdges);
//		this.selfEdges.put(node, node.selfEdges);
	}

	@Override
	public TICONE_NODE getNode(final String name) {
		return this.nodeIdToNode.get(name);
	}

	@Override
	public Set<String> getNodeIds() {
		return new HashSet<>(this.nodeIdToNode.keySet());
	}

	@Override
	public boolean containsNode(final String nodeId) {
		return this.nodeIdToNode.containsKey(nodeId);
	}

	@Override
	public <N extends TICONE_NODE> Collection<TICONE_EDGE> getEdges(final Set<N> nodes1, final Set<N> nodes2,
			final TiconeEdgeType type) {
		final Set<TICONE_EDGE> result = new HashSet<>();
		for (final N n1 : nodes1)
			for (final N n2 : nodes2)
				result.addAll(this.getEdges(n1, n2, type));
		return result;
	}

	@Override
	public <N extends TICONE_NODE> Set<TICONE_EDGE> getEdgesByNodeIds(final Set<String> nodeIds1,
			final Set<String> nodeIds2, final TiconeEdgeType type) {
		final Set<TICONE_NODE> nodes1 = new HashSet<>(), nodes2 = new HashSet<>();
		for (final String id : nodeIds1)
			nodes1.add(this.getNode(id));
		for (final String id : nodeIds2)
			nodes2.add(this.getNode(id));

		final Set<TICONE_EDGE> result = new HashSet<>();

		for (final TICONE_NODE n1 : nodes1)
			for (final TICONE_NODE n2 : nodes2)
				result.addAll(this.getEdges(n1, n2, type));
		return result;
	}

	public synchronized Set<ConnectedComponent> calculateConnectedComponents() throws InterruptedException {
		return this.calculateConnectedComponents(null);
	}

	public synchronized Set<ConnectedComponent> calculateConnectedComponents(final Progress progress)
			throws InterruptedException {
		this.logger.debug("Calculating connected components ...");
		this.getNodeSet().forEach(n -> n.resetConnectedComponent());
		this.nextNetworkWideConnectedComponentId.set(0);
		final Set<ConnectedComponent> connectedComponents = new HashSet<>();
		final HashSet<TICONE_NODE> unprocessedNodes = new HashSet<>(this.getNodeSet());

		while (!unprocessedNodes.isEmpty()) {
			if (progress != null && !progress.getStatus())
				throw new InterruptedException();
			final TICONE_NODE n1 = unprocessedNodes.iterator().next();
			unprocessedNodes.remove(n1);
			if (n1.connectedComponent == null) {
				// this component is used for all neighbors
				final ConnectedComponent newComp = new ConnectedComponent(
						TiconeNetwork.nextConnectedComponentUID.getAndIncrement(),
						this.nextNetworkWideConnectedComponentId.getAndIncrement());
				connectedComponents.add(newComp);

				// spread over all neighbors and set the component
				final Set<TICONE_NODE> unprocessedNodesCurrentComponent = new HashSet<>();
				unprocessedNodesCurrentComponent.add(n1);
				while (!unprocessedNodesCurrentComponent.isEmpty()) {
					final TICONE_NODE n2 = unprocessedNodesCurrentComponent.iterator().next();
					unprocessedNodes.remove(n2);
					unprocessedNodesCurrentComponent.remove(n2);

					if (n2.connectedComponent == null) {
						n2.connectedComponent = newComp;
						n2.componentWideId = newComp.nodes.size();
						newComp.addNode(n2);

						// only visit neighbors that haven't been processed
						n2.getNeighbors(TiconeEdgeType.ANY).forEach(n -> {
							if (unprocessedNodes.contains(n))
								unprocessedNodesCurrentComponent.add(n);
						});
					} else {
						assert (n2.connectedComponent == newComp);
					}
				}
			}
		}
		this.connectedComponents = connectedComponents;
		return connectedComponents;
	}

	public synchronized ArrayList<TICONE_NODE> getNetworkWideNodeIdToNode() {
		if (this.networkWideNodeIdToNodeList == null) {
			// old version, deserialized
			if (this.networkWideNodeIdToNode != null) {
				this.networkWideNodeIdToNodeList = new ArrayList<>(this.networkWideNodeIdToNode.length);
				for (final TICONE_NODE node : this.networkWideNodeIdToNode)
					this.networkWideNodeIdToNodeList.add(node);
			} else {
				this.networkWideNodeIdToNodeList = new ArrayList<>(this.nextNetworkWideNodeId.get() + 1);
				ensureSize(this.networkWideNodeIdToNodeList, this.nextNetworkWideNodeId.get() + 1);
				for (final TICONE_NODE n : this.getNodeSet())
					this.networkWideNodeIdToNodeList.set(n.networkWideId, n);
			}
		}
		return this.networkWideNodeIdToNodeList;
	}

	@Override
	public int getNodeCount() {
		return this.nodes.size();
	}

	@Override
	public List<TICONE_NODE> getNodeList() {
		return new ArrayList<>(this.nodes);
	}

	@Override
	public Set<TICONE_NODE> getNodeSet() {
		return this.nodes;
	}

	public static void ensureSize(final ArrayList<?> list, final int size) {
		// Prevent excessive copying while we're adding
		list.ensureCapacity(size);
		while (list.size() < size) {
			list.add(null);
		}
	}

	public abstract class TiconeNetworkEdge implements Serializable, ITiconeNetworkEdge<TICONE_NODE> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4710508822157939452L;

		protected TICONE_NODE source, target;
		protected boolean isDirected;

		protected long networkWideId;

		protected long uid;

		public TiconeNetworkEdge(final TICONE_NODE source, final TICONE_NODE target, final boolean isDirected,
				final long networkWideId) {
			super();
			Objects.requireNonNull(source);
			Objects.requireNonNull(target);
			this.source = source;
			this.target = target;
			this.uid = nextEdgeUID.getAndIncrement();
			this.isDirected = isDirected;
			this.networkWideId = networkWideId;
		}

		@Override
		public int hashCode() {
			return Long.hashCode(this.networkWideId);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof TiconeNetwork.TiconeNetworkEdge)
				return this.networkWideId == ((TiconeNetwork.TiconeNetworkEdge) obj).networkWideId;
			return false;
		}

		@Override
		public TICONE_NODE getSource() {
			return this.source;
		}

		@Override
		public TICONE_NODE getTarget() {
			return this.target;
		}

		@Override
		public boolean isDirected() {
			return this.isDirected;
		}

		@Override
		public long getUID() {
			return this.uid;
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}
	}

	public abstract class TiconeNetworkNode implements Serializable, ITiconeNetworkNode {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8553099214913708118L;

		protected String name, alternativeName;

		protected transient ConnectedComponent connectedComponent;

		protected int componentWideId;

		protected final Object2ObjectMap<TICONE_NODE, Set<TICONE_EDGE>> incomingEdges;
		protected final Object2ObjectMap<TICONE_NODE, Set<TICONE_EDGE>> outgoingEdges;

		protected transient Set<TICONE_EDGE> selfEdges;

		protected int networkWideId;

		final protected long uid;

		/**
		 * A node without alternative name.
		 * 
		 * @param name
		 */
		public TiconeNetworkNode(final String name) {
			this(name, null);
		}

		public TiconeNetworkNode(final String name, final String alternativeName) {
			super();
			this.name = Objects.requireNonNull(name);
			this.uid = nextNodeUID.getAndIncrement();
			this.alternativeName = alternativeName;
			this.componentWideId = -1;
			this.incomingEdges = new Object2ObjectOpenHashMap<>();
			this.outgoingEdges = new Object2ObjectOpenHashMap<>();
			this.selfEdges = new HashSet<>();
		}

		@Override
		public String getName() {
			return this.name;
		}

		@Override
		public boolean hasAlternativeName() {
			return this.alternativeName != null;
		}

		@Override
		public String getAlternativeName() {
			return this.alternativeName;
		}

		/**
		 * @param alternativeName the alternativeName to set
		 */
		@Override
		public void setAlternativeName(String alternativeName) {
			this.alternativeName = alternativeName;
		}

		@Override
		public TiconeNetwork<TICONE_NODE, TICONE_EDGE> getNetwork() {
			return TiconeNetwork.this;
		}

		/**
		 * @return the connectedComponent
		 * @throws InterruptedException
		 */
		@Override
		public ConnectedComponent getConnectedComponent() throws InterruptedException {
			if (this.connectedComponent == null)
				TiconeNetwork.this.connectedComponents = TiconeNetwork.this.calculateConnectedComponents();
			return this.connectedComponent;
		}

		@Override
		public void setConnectedComponent(final IConnectedComponent<? extends ITiconeNetworkNode> component) {
			// TODO
			this.connectedComponent = (ConnectedComponent) component;
		}

		@Override
		public void resetConnectedComponent() {
			this.connectedComponent = null;
		}

		/**
		 * @return the componentWideId
		 */
		@Override
		public int getComponentWideId() {
			return this.componentWideId;
		}

		/**
		 * @param componentWideId the componentWideId to set
		 */
		@Override
		public void setComponentWideId(final int componentWideId) {
			this.componentWideId = componentWideId;
		}

		@Override
		public ObjectList<TICONE_NODE> getNeighbors(final TiconeEdgeType types) {
			final ObjectList<TICONE_NODE> neighbors = new ObjectArrayList<>();
			if (types.equals(TiconeEdgeType.INCOMING) || types.equals(TiconeEdgeType.ANY)) {
				neighbors.addAll(this.getIncomingNeighbors());
			}
			if (types.equals(TiconeEdgeType.OUTGOING) || types.equals(TiconeEdgeType.ANY)) {
				neighbors.addAll(this.getOutgoingNeighbors());
			}
			if (types.equals(TiconeEdgeType.SELF) || types.equals(TiconeEdgeType.ANY)) {
				if (!this.selfEdges.isEmpty()) {
					neighbors.add((TICONE_NODE) this);
				}
			}
			return neighbors;
		}

		public Set<TICONE_NODE> getIncomingNeighbors() {
			return this.incomingEdges.keySet();
		}

		public Set<TICONE_NODE> getOutgoingNeighbors() {
			return this.outgoingEdges.keySet();
		}

		/**
		 * @return the networkWideId
		 */
		public int getNetworkWideId() {
			return this.networkWideId;
		}

		@Override
		public long getUID() {
			return this.uid;
		}

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof TiconeNetwork.TiconeNetworkNode)
				return Objects.equals(this.name, ((TiconeNetwork.TiconeNetworkNode) obj).name);
			return false;
		}

		@Override
		public int hashCode() {
			return this.name.hashCode();
		}
	}

	public class ConnectedComponent implements IConnectedComponent<TICONE_NODE>, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4918751837287282218L;
		protected long uid;
		protected int networkWideId;
		protected List<TICONE_NODE> nodes;
		protected double[][] compDistsDirected, compDistsUndirected;

		/**
		 * 
		 */
		ConnectedComponent(final long uid, final int networkWideId) {
			super();
			this.uid = uid;
			this.networkWideId = networkWideId;
			this.nodes = new ArrayList<>();
		}

		/**
		 * @return the compDists
		 */
		public double[][] getCompDists(final boolean isDirected) {
			if (isDirected)
				return this.compDistsDirected;
			return this.compDistsUndirected;
		}

		/**
		 * @return the compDists
		 */
		public void setCompDists(final boolean isDirected, final double[][] dists) {
			if (isDirected)
				this.compDistsDirected = dists;
			else
				this.compDistsUndirected = dists;
		}

		@Override
		public void addNode(final TICONE_NODE node) {
			this.nodes.add(node);
		}

		/**
		 * @return the nodes
		 */
		@Override
		public List<TICONE_NODE> getNodes() {
			return this.nodes;
		}

		@Override
		public TiconeNetwork<TICONE_NODE, TICONE_EDGE> getNetwork() {
			return TiconeNetwork.this;
		}

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof TiconeNetwork.ConnectedComponent)
				return this.getNetwork().equals(((ConnectedComponent) obj).getNetwork())
						&& this.uid == ((ConnectedComponent) obj).uid;
			return false;
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.getNetwork(), this.uid);
		}

		@Override
		public String toString() {
			return String.format("Connected component %d (UID %d)", this.networkWideId, this.uid);
		}

		@Override
		public int getNetworkWideId() {
			return this.networkWideId;
		}

		/**
		 * @return the networkWideId
		 */
		@Override
		public long getUID() {
			return this.uid;
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}
	}

	public interface INetworkDistanceFunction {
		double getDistance(Collection<? extends INetworkMappedTimeSeriesObject> nodes1,
				Collection<? extends INetworkMappedTimeSeriesObject> nodes2, boolean isDirected)
				throws IncompatibleNetworkException;
	}

	public class SingleLinkageNetworkDistance implements INetworkDistanceFunction {

		@Override
		public double getDistance(Collection<? extends INetworkMappedTimeSeriesObject> nodes1,
				Collection<? extends INetworkMappedTimeSeriesObject> nodes2, boolean isDirected)
				throws IncompatibleNetworkException {
			double minDistance = Double.NaN;
			INetworkMappedTimeSeriesObject min1 = null;
			for (INetworkMappedTimeSeriesObject n1 : nodes1) {
				for (INetworkMappedTimeSeriesObject n2 : nodes2) {
					final double d = getDistance(n1, n2, isDirected);
					if (min1 == null || !Double.isNaN(d) && Double.compare(d, minDistance) < 0) {
						minDistance = d;
						min1 = n1;
					}
				}
			}

			return minDistance;
		}

		private double getDistance(INetworkMappedTimeSeriesObject o1, INetworkMappedTimeSeriesObject o2,
				boolean isDirected) throws IncompatibleNetworkException {
			if (!o1.isMappedTo(TiconeNetwork.this) || !o2.isMappedTo(TiconeNetwork.this))
				throw new IncompatibleNetworkException();
			TICONE_NODE n1 = (TICONE_NODE) o1.getNode(TiconeNetwork.this);
			TICONE_NODE n2 = (TICONE_NODE) o2.getNode(TiconeNetwork.this);
			if (n1 == n2)
				return Double.NEGATIVE_INFINITY;
			final ConnectedComponent comp1 = n1.connectedComponent;
			final ConnectedComponent comp2 = n2.connectedComponent;

			// non-connected pairs
			if (comp1 != comp2)
				return Double.NaN;

			final int id1 = n1.componentWideId;
			final int id2 = n2.componentWideId;

			return comp1.getCompDists(isDirected)[id1][id2];
		}
	}

	public class ShortestDistanceCalculator {

		private Logger logger;

		final private boolean isDirected;
		private INetworkDistanceFunction networkDistanceFunction;

		/**
		 * @throws InterruptedException
		 * 
		 */
		ShortestDistanceCalculator(final boolean isDirected) throws InterruptedException {
			super();
			this.logger = LoggerFactory.getLogger(this.getClass());
			this.isDirected = isDirected;
			this.networkDistanceFunction = new SingleLinkageNetworkDistance();

			int maxNetworkWideId = -1;
			for (final TICONE_NODE n : TiconeNetwork.this.getNodeSet()) {
				maxNetworkWideId = Math.max(maxNetworkWideId, n.networkWideId);
			}

			if (TiconeNetwork.this.connectedComponents == null)
				TiconeNetwork.this.calculateConnectedComponents();

			calculateShortestPaths(isDirected);
		}

		/**
		 * @return the isDirected
		 */
		public boolean isDirected() {
			return this.isDirected;
		}

		IFeatureValue<Double> calculate(final IObjectPair pair, final IObjectPairFeatureShortestDistance feature)
				throws InterruptedException, FeatureCalculationException, IncompatibleFeatureAndObjectException {
			try {
				final ITimeSeriesObject o1 = pair.getFirst(), o2 = pair.getSecond();
				final TICONE_NODE n1 = TiconeNetwork.this.getNode(o1), n2 = TiconeNetwork.this.getNode(o2);

				return feature.value(pair,
						networkDistanceFunction.getDistance(Arrays.asList(o1.mapToNetworkNode(TiconeNetwork.this, n1)),
								Arrays.asList(o2.mapToNetworkNode(TiconeNetwork.this, n2)), isDirected));
			} catch (ObjectNotInNetworkException | IncompatibleNetworkException e) {
				throw new FeatureCalculationException(e);
			}
		}

		IFeatureValue<Double> calculate(final IObjectClusterPair pair, final IObjectClusterFeatureShortestPath feature)
				throws FeatureCalculationException, InterruptedException, IncompatibleFeatureAndObjectException {
			try {
				final ITimeSeriesObject o = pair.getObject();
				final TICONE_NODE n1 = TiconeNetwork.this.getNode(o);

				final Collection<? extends INetworkMappedTimeSeriesObject> networkLocation = PrototypeComponentType.NETWORK_LOCATION
						.getComponent(pair.getCluster().getPrototype()).getNetworkLocation();

				return feature.value(pair, networkDistanceFunction.getDistance(
						Arrays.asList(o.mapToNetworkNode(TiconeNetwork.this, n1)), networkLocation, isDirected));
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException | ObjectNotInNetworkException
					| IncompatibleNetworkException e) {
				throw new FeatureCalculationException(e);
			}
		}

		IFeatureValue<Double> calculate(final IClusterPair pair, final IClusterPairFeatureShortestPath feature)
				throws FeatureCalculationException, InterruptedException {
			try {
				Collection<? extends INetworkMappedTimeSeriesObject> networkLocation1 = PrototypeComponentType.NETWORK_LOCATION
						.getComponent(pair.getFirst().getPrototype()).getNetworkLocation();
				Collection<? extends INetworkMappedTimeSeriesObject> networkLocation2 = PrototypeComponentType.NETWORK_LOCATION
						.getComponent(pair.getSecond().getPrototype()).getNetworkLocation();

				return feature.value(pair,
						networkDistanceFunction.getDistance(networkLocation1, networkLocation2, isDirected));
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException
					| IncompatibleFeatureAndObjectException | IncompatibleNetworkException e) {
				throw new FeatureCalculationException(e);
			}
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}
	}

	class EdgeWrapper implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2364346863000791640L;
		TICONE_EDGE edge;

		EdgeWrapper(final TICONE_EDGE edge) {
			super();
			this.edge = edge;
		}

		@Override
		public boolean equals(final Object obj) {
			if (!(obj instanceof TiconeNetwork.EdgeWrapper))
				return false;
			final EdgeWrapper other = (EdgeWrapper) obj;
			if (!(this.edge.isDirected == other.edge.isDirected))
				return false;

			if (TiconeNetwork.this.isMultiGraph) {
				return this.edge.networkWideId == other.edge.networkWideId;
			} else if (this.edge.isDirected) {
				return Objects.equals(this.edge.getSource(), other.edge.getSource())
						&& Objects.equals(this.edge.getTarget(), other.edge.getTarget());
			} else {
				return new HashSet<>(Arrays.asList(this.edge.getSource(), this.edge.getTarget()))
						.equals(new HashSet<>(Arrays.asList(other.edge.getSource(), other.edge.getTarget())));
			}
		}

		@Override
		public int hashCode() {
			if (TiconeNetwork.this.isMultiGraph) {
				return ((int) (this.edge.networkWideId % Integer.MAX_VALUE));
			} else if (this.edge.isDirected) {
				return (this.edge.getSource() + " " + this.edge.getTarget()).hashCode();
			} else {
				return this.edge.getSource().hashCode() * this.edge.getTarget().hashCode();
			}
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}

	}

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Mar 31, 2019
	 *
	 */
	private final class ComponentShortestDistanceCalculationOp
			implements MyParallel.Operation<ConnectedComponent, Void> {

		private final Progress progress;
		private final boolean isDirected;

		public ComponentShortestDistanceCalculationOp(final boolean isDirected, final Progress progress) {
			super();
			this.isDirected = isDirected;
			this.progress = progress;
		}

		@Override
		public Void perform(final ConnectedComponent component) throws InterruptedException {
			final double[][] compDists = new double[component.nodes.size()][component.nodes.size()];
			for (int i = 0; i < compDists.length; i++) {
				for (int j = 0; j < compDists[i].length; j++)
					compDists[i][j] = Double.POSITIVE_INFINITY;
				compDists[i][i] = 0;
			}

			final AtomicInteger finishedStartNodes = new AtomicInteger();
			final List<Future<Void>> for1 = new MyParallel<Void>(privateThreadPool).For(component.nodes,
					new Operation<TICONE_NODE, Void>() {
						@Override
						public Void perform(final TICONE_NODE start) throws Exception {
							final int startComponentWideId = start.componentWideId;
							// relax vertices in order of distance from s
							final DoubleIndexMinPQ pq = new DoubleIndexMinPQ(component.nodes.size());
							pq.insert(startComponentWideId, compDists[startComponentWideId][startComponentWideId]);
							while (!pq.isEmpty()) {
								if (!Utility.getProgress().getStatus()) {
									throw new InterruptedException();
								}
								final int sourceComponentWideId = pq.delMin();
								final TICONE_NODE v = component.nodes.get(sourceComponentWideId);
								Set<TICONE_NODE> neighbors = v.getOutgoingNeighbors();
								for (TICONE_NODE w : neighbors) {
									relax(compDists, pq, start, v, w);
								}
								// also look at incoming edges
								if (!isDirected) {
									neighbors = v.getIncomingNeighbors();
									for (TICONE_NODE w : neighbors) {
										relax(compDists, pq, start, v, w);
									}
								}
							}
							finishedStartNodes.incrementAndGet();
							if (progress != null) {
								synchronized (progress) {
									if (!progress.getStatus())
										throw new InterruptedException();
									else
										progress.updateProgress(
												finishedStartNodes.doubleValue() / component.nodes.size(), null);
								}
							}
							return null;
						}
					});
			for (Future<Void> f : for1)
				try {
					f.get();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
//			for (TICONE_NODE start : component.nodes) {
//				if (progress != null) {
//					if (!progress.getStatus())
//						throw new InterruptedException();
//					else
//						progress.updateProgress(finishedStartNodes / (double) component.nodes.size(), null);
//				}
//				final int startComponentWideId = start.componentWideId;
//				// relax vertices in order of distance from s
//				final DoubleIndexMinPQ pq = new DoubleIndexMinPQ(component.nodes.size());
//				pq.insert(startComponentWideId, compDists[startComponentWideId][startComponentWideId]);
//				while (!pq.isEmpty()) {
//					if (!Utility.getProgress().getStatus()) {
//						throw new InterruptedException();
//					}
//					final int sourceComponentWideId = pq.delMin();
//					final TICONE_NODE v = (TICONE_NODE) component.nodes.get(sourceComponentWideId);
//					Set<TICONE_NODE> neighbors = v.getOutgoingNeighbors();
//					for (TICONE_NODE w : neighbors) {
//						relax(compDists, pq, start, v, w);
//					}
//					// also look at incoming edges
//					if (!this.isDirected) {
//						neighbors = v.getIncomingNeighbors();
//						for (TICONE_NODE w : neighbors) {
//							relax(compDists, pq, start, v, w);
//						}
//					}
//				}
//				finishedStartNodes++;
//			}

			component.setCompDists(isDirected, compDists);
			return null;
		}

		// relax edge e and update pq if changed
		private void relax(final double[][] compDists, final DoubleIndexMinPQ pq, final TICONE_NODE start,
				final TICONE_NODE source, final TICONE_NODE target) {
			final double distStartTarget = compDists[start.componentWideId][target.componentWideId];
			final double distStartSource = compDists[start.componentWideId][source.componentWideId];
			if (distStartTarget > distStartSource + 1) {
				final double newDistStartTarget = distStartSource + 1;
				compDists[start.componentWideId][target.componentWideId] = newDistStartTarget;

				if (pq.contains(target.componentWideId))
					pq.decreaseKey(target.componentWideId, newDistStartTarget);
				else
					pq.insert(target.componentWideId, newDistStartTarget);
			}
		}
	}

	/**
	 * @return the connectedComponents
	 */
	public Set<ConnectedComponent> getConnectedComponents() {
		return this.connectedComponents;
	}

	public synchronized void reinitializeNetworkWideNodeIds() {
		logger.debug("Reinitialization of network wide node ids requested ...");
		if (this.nextNetworkWideNodeId.get() == this.nodes.size()) {
			logger.debug("nothing to do ... returning");
			return;
		}

		final int old = this.nextNetworkWideNodeId.get();

		logger.debug(" recalculating node ids ...");
		this.networkWideNodeIdToNodeList = null;
		this.networkWideNodeIdToNode = null;

		this.nextNetworkWideNodeId.set(0);
		for (final TICONE_NODE n : this.nodes)
			n.networkWideId = this.nextNetworkWideNodeId.getAndIncrement();

		logger.debug("Old node id range: [%d,%d], new range: [%d,%d]", 0, old - 1, 0,
				this.nextNetworkWideNodeId.get() - 1);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

	protected TICONE_EDGE insertEdgeIntoEdgeDatastructures(final TICONE_EDGE edge) {
		if (edge == null)
			return null;
		final EdgeWrapper ew = new EdgeWrapper(edge);
		// check if edge is already contained in network
		if (this.edgeWrapperToEdge.containsKey(ew))
			return null;
		this.edgeWrapperToEdge.put(ew, edge);
		this.edgeToEdgeWrapper.put(edge, ew);
		this.edges.add(edge);
		final TICONE_NODE source = edge.getSource();
		final TICONE_NODE target = edge.getTarget();
		if (source.equals(target)) {
			source.selfEdges.add(edge);
		} else {
			source.outgoingEdges.putIfAbsent(target, new HashSet<TICONE_EDGE>());
			source.outgoingEdges.get(target).add(edge);
			target.incomingEdges.putIfAbsent(source, new HashSet<TICONE_EDGE>());
			target.incomingEdges.get(source).add(edge);
		}
		synchronized (this.nextNetworkWideEdgeId) {
			if (edge.networkWideId >= this.nextNetworkWideEdgeId.get())
				this.nextNetworkWideEdgeId.set(edge.networkWideId + 1);
		}
		return edge;
	}

	/**
	 * 
	 */
	protected void initializeEdgeDatastructures() {
		for (final TICONE_NODE n : this.nodes) {
			n.selfEdges.clear();
			n.outgoingEdges.clear();
			n.incomingEdges.clear();
			n.outgoingEdges.clear();
		}
		for (final TICONE_EDGE edge : this.edges) {
			this.insertEdgeIntoEdgeDatastructures(edge);
		}
	}

	private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		this.logger = LoggerFactory.getLogger(this.getClass());

		// set connected components to nodes
		if (this.connectedComponents != null)
			this.connectedComponents.forEach(c -> c.nodes.forEach(n -> n.connectedComponent = c));
	}

	private void initThreadPool() {
		this.privateThreadPool = new MyScheduledThreadPoolExecutor(MyParallel.DEFAULT_THREADS, new MyThreadFactory());
	}

	public void clearThreadPool() {
		if (!this.isCopy && this.privateThreadPool != null)
			this.privateThreadPool.shutdown();
		this.privateThreadPool = null;
	}

	@Override
	public int getEdgeCount() {
		return this.edges.size();
	}

	@Override
	public List<TICONE_EDGE> getEdgeList() {
		return new ArrayList<>(this.edges);
	}

	@Override
	public Set<TICONE_EDGE> getEdgeSet() {
		return this.edges;
	}

	public void calculateShortestPaths(final boolean isDirected) throws InterruptedException {
		this.calculateShortestPaths(isDirected, null);
	}

	public void calculateShortestPaths(final boolean isDirected, final Progress progress) throws InterruptedException {
		logger.debug("Calculating shortest paths of connected components ...");
//		final List<Future<Void>> for1 = new MyParallel<Void>(privateThreadPool).For(
//				TiconeNetwork.this.connectedComponents.stream().filter(c -> c.getCompDists(isDirected) == null).collect(
//						Collectors.toList()),
//				new ComponentShortestDistanceCalculationOp(isDirected, progress));
//		for (final Future<Void> f : for1)
//			try {
//				f.get();
//			} catch (final ExecutionException e) {
//				if (e.getCause() instanceof InterruptedException)
//					throw (InterruptedException) e.getCause();
//				e.printStackTrace();
//			}
		final TiconeNetwork<TICONE_NODE, TICONE_EDGE>.ComponentShortestDistanceCalculationOp op = new ComponentShortestDistanceCalculationOp(
				isDirected, progress);
		for (ConnectedComponent c : TiconeNetwork.this.connectedComponents.stream()
				.filter(c -> c.getCompDists(isDirected) == null).collect(Collectors.toList()))
			op.perform(c);
		logger.debug("Calculating shortest paths of connected components ... done");
	}

}

/**
 * @author Christian Wiwie
 * 
 * @since Feb 1, 2019
 *
 */
class MyThreadFactory implements ThreadFactory {
	ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();

	@Override
	public Thread newThread(Runnable r) {
		Thread t = defaultThreadFactory.newThread(r);

		t.setName(t.getName().replace("pool-",
				String.format("%s-pool-", ShortestDistanceCalculator.class.getSimpleName())));

		return t;
	}
}
