package dk.sdu.imada.ticone.network;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import dk.sdu.imada.ticone.network.TiconeNetwork.TiconeNetworkEdge;
import dk.sdu.imada.ticone.network.TiconeNetwork.TiconeNetworkNode;
import dk.sdu.imada.ticone.util.IIdMapMethod;

/**
 * @author Christian Wiwie
 * @author Christian Norskov
 * @since 3/24/15
 */
public class NetworkUtil {

	public static final String NETWORK_ATTRIBUTE_OBJECT_SIMILARITY_IS_TOTAL_NETWORK = "is_total_network";

	public static final String NODE_ATTRIBUTE_IS_OBJECT = "is_object";
	public static final String NODE_ATTRIBUTE_OBJECT_NAME = "name";
	public static final String NODE_ATTRIBUTE_OBJECT_ALTERNATIVE_NAME = "alt_name";
	public static final String NODE_ATTRIBUTE_IS_CLUSTER = "is_cluster";
	public static final String NODE_ATTRIBUTE_CLUSTER_NUMBER = "cluster_number";

	public static <TICONE_NODE extends ITiconeNetworkNode> List<TICONE_NODE> getAllNodes(
			final ITiconeNetwork<TICONE_NODE, ? extends TiconeNetworkEdge> TiCoNENetwork) {
		final List<TICONE_NODE> nodes = new ArrayList<>();

		for (final TICONE_NODE node : TiCoNENetwork.getNodeList()) {
			nodes.add(node);
		}
		return nodes;
	}

	public static <TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>, TICONE_NETWORK extends ITiconeNetwork<TICONE_NODE, TICONE_EDGE>> TICONE_NETWORK createNewNetwork(
			final TiconeNetworkFactory<TICONE_NETWORK> networkFactory, final String networkName,
			final Map<String, Object> networkAttributes, final Iterable<String> nodeNames,
			final Map<String, ? extends Iterable<String>> edgeMapList, final boolean registerNetwork,
			final Map<String, Map<String, Object>> nodeAttributes) {
		return createNewNetwork(networkFactory, networkName, networkAttributes, nodeNames, edgeMapList, registerNetwork,
				nodeAttributes, null);
	}

	public static <TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>, TICONE_NETWORK extends ITiconeNetwork<TICONE_NODE, TICONE_EDGE>> TICONE_NETWORK createNewNetwork(
			final TiconeNetworkFactory<TICONE_NETWORK> networkFactory, final String networkName,
			final Map<String, Object> networkAttributes, final Iterable<String> nodeNames,
			final Map<String, ? extends Iterable<String>> edgeMapList, final boolean registerNetwork,
			final Map<String, Map<String, Object>> nodeAttributes,
			final Map<String, Map<String, Map<String, Object>>> edgeAttributes) {
		final TICONE_NETWORK network = networkFactory.getInstance(registerNetwork);
		// give the network a name
		if (networkName != null && !networkName.isEmpty())
			network.setName(networkName);

		if (networkAttributes != null) {
			for (final String attr : networkAttributes.keySet()) {
				if (networkAttributes.get(attr) == null)
					continue;
				network.createNetworkAttribute(attr, networkAttributes.get(attr).getClass(), true);
				network.setNetworkAttribute(attr, networkAttributes.get(attr));
			}
		}

		// we assume that all nodes have the same attributes given
		final Set<String> newNodeAttributes = new HashSet<>();
		if (nodeAttributes != null && nodeAttributes.size() > 0) {
			final Map<String, Object> attrs = nodeAttributes.values().iterator().next();
			for (final String attr : attrs.keySet()) {
				if (!network.hasNodeAttribute(attr)) {
					network.createNodeAttribute(attr, attrs.get(attr).getClass(), true);
					newNodeAttributes.add(attr);
				}
			}
		}

		// we assume that all edges have the same attributes given
		final Set<String> newEdgeAttributes = new HashSet<>();
		if (edgeAttributes != null && edgeAttributes.size() > 0) {
			final Map<String, Object> attrs = edgeAttributes.values().iterator().next().values().iterator().next();
			for (final String attr : attrs.keySet()) {
				if (!network.hasEdgeAttribute(attr) && attrs.get(attr) != null) {
					network.createEdgeAttribute(attr, attrs.get(attr).getClass(), true);
					newEdgeAttributes.add(attr);
				}
			}
		}

		final Map<String, TICONE_NODE> existingNodes = new HashMap<>();
		for (final String nodeName : nodeNames) {

			TICONE_NODE node;
			if (existingNodes.containsKey(nodeName)) {
				node = existingNodes.get(nodeName);
			} else {
				node = network.addNode(nodeName);

				if (nodeAttributes != null) {
					final String alternativeNodeName = nodeAttributes.containsKey(nodeName)
							? (String) nodeAttributes.get(nodeName).get(NODE_ATTRIBUTE_OBJECT_ALTERNATIVE_NAME)
							: null;
					node.setAlternativeName(alternativeNodeName);

					addNodeAttributes(network, node, nodeAttributes, newNodeAttributes);
				}

				existingNodes.put(nodeName, node);
			}

			final Iterable<String> edgeList = edgeMapList.get(nodeName);
			if (edgeList != null) {
				for (final String neighborName : edgeList) {
					TICONE_NODE node2;
					if (existingNodes.containsKey(neighborName)) {
						node2 = existingNodes.get(neighborName);
						// TODO: Find out if this code is actually necessary...
					} else {
						node2 = network.addNode(neighborName);

						if (nodeAttributes != null) {
							final String alternativeNeighborName = nodeAttributes.containsKey(neighborName)
									? (String) nodeAttributes.get(neighborName)
											.get(NODE_ATTRIBUTE_OBJECT_ALTERNATIVE_NAME)

									: null;
							node2.setAlternativeName(alternativeNeighborName);

							addNodeAttributes(network, node2, nodeAttributes, newNodeAttributes);
						}
						existingNodes.put(neighborName, node2);
					}
					final TICONE_EDGE edge = network.addEdge(node, node2, true);

					addEdgeAttributes(network, edge, edgeAttributes, newEdgeAttributes);
				}
			}
		}

		return network;
	}

	private static void addNodeAttributes(final ITiconeNetwork network, final ITiconeNetworkNode node,
			final Map<String, Map<String, Object>> nodeAttributes, final Set<String> newNodeAttributes) {
		final String nodeName = node.getName();
		// do we have attributes for this node?
		if (nodeAttributes != null && nodeAttributes.containsKey(nodeName)) {
			final Map<String, Object> attrs = nodeAttributes.get(nodeName);
			for (final String attr : attrs.keySet()) {
				if (newNodeAttributes.contains(attr))
					network.setNodeAttribute(node, attr, attrs.get(attr));
			}
		}
	}

	private static void addEdgeAttributes(final ITiconeNetwork network, final ITiconeNetworkEdge edge,
			final Map<String, Map<String, Map<String, Object>>> edgeAttributes, final Set<String> newEdgeAttributes) {
		if (edge == null)
			return;
		final String sourceName = edge.getSource().getName();
		final String targetName = edge.getTarget().getName();
		// do we have attributes for this edge?
		if (edgeAttributes != null && edgeAttributes.containsKey(sourceName)
				&& edgeAttributes.get(sourceName).containsKey(targetName)) {
			final Map<String, Object> attrs = edgeAttributes.get(sourceName).get(targetName);
			for (final String attr : attrs.keySet()) {
				// TODO: why did we have this in here?
				// if (newEdgeAttributes.contains(attr))
				if (network.hasEdgeAttribute(attr) && attrs.get(attr) != null)
					network.setEdgeAttribute(edge, attr, attrs.get(attr));
			}
		}
	}

	public static <TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>, TICONE_NETWORK extends ITiconeNetwork<TICONE_NODE, TICONE_EDGE>> Pair<TICONE_NETWORK, Map<String, TICONE_NODE>> cloneTiCoNENetwork(
			final TiconeNetworkFactory<TICONE_NETWORK> networkFactory, final TICONE_NETWORK network) {
		final TICONE_NETWORK newNetwork = networkFactory.getInstance();

		final Map<String, TICONE_NODE> idToNewNode = new HashMap<>();

		for (final TICONE_NODE node : network.getNodeList()) {
			final String id = node.getName();
			final TICONE_NODE newNode = newNetwork.addNode(id);
			newNode.setAlternativeName(node.getAlternativeName());

			idToNewNode.put(id, newNode);
		}

		for (final ITiconeNetworkEdge edge : network.getEdgeList()) {
			final String sourceId = edge.getSource().getName();
			final String targetId = edge.getTarget().getName();

			newNetwork.addEdge(idToNewNode.get(sourceId), idToNewNode.get(targetId), edge.isDirected());
		}

		return Pair.of(newNetwork, idToNewNode);
	}

	public static List<Pair<String, String>> getEdgeList(
			final ITiconeNetwork<? extends TiconeNetworkNode, ? extends TiconeNetworkEdge> network) {
		final List<Pair<String, String>> edgeList = new ArrayList<>();

		for (final ITiconeNetworkEdge edge : network.getEdgeList()) {
			final String sourceId = edge.getSource().getName();
			final String targetId = edge.getTarget().getName();
			edgeList.add(Pair.of(sourceId, targetId));
		}

		return edgeList;
	}

	public static <TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>, TICONE_NETWORK extends ITiconeNetwork<TICONE_NODE, TICONE_EDGE>> TICONE_NETWORK getNodeInducedNetwork(
			final TiconeNetworkFactory<TICONE_NETWORK> networkFactory, final TICONE_NETWORK network,
			final String networkName, final Map<String, Object> networkAttributes,
			final Map<String, Map<String, Object>> nodesWithAttributes, final boolean registerNetwork,
			final IIdMapMethod idMapMethod) {
		final Map<String, Map<String, Object>> nodeAttributes = new HashMap<>();
		if (idMapMethod.isActive()) {
			for (final String tsd : nodesWithAttributes.keySet()) {
				nodeAttributes.put(tsd, new HashMap<String, Object>());
				nodeAttributes.get(tsd).putAll(nodesWithAttributes.get(tsd));
				final String altId = idMapMethod.getAlternativeId(tsd);
				if (altId != null && !altId.isEmpty()) {
					nodeAttributes.get(tsd).put(NODE_ATTRIBUTE_OBJECT_ALTERNATIVE_NAME, altId);
				}
			}
		}

		for (final String tsd : nodesWithAttributes.keySet()) {
			if (!nodeAttributes.containsKey(tsd))
				nodeAttributes.put(tsd, new HashMap<String, Object>());
			nodeAttributes.get(tsd).put("inOriginalNetwork", network.containsNode(tsd));
			nodeAttributes.get(tsd).putAll(nodesWithAttributes.get(tsd));
		}

		final Map<String, List<String>> edgeMap = new HashMap<>();
		for (final ITiconeNetworkEdge e : network.getEdgeList()) {
			final String sourceName = e.getSource().getName();
			final String targetName = e.getTarget().getName();
			if (nodesWithAttributes.containsKey(sourceName) && nodesWithAttributes.containsKey(targetName)) {
				if (!edgeMap.containsKey(sourceName))
					edgeMap.put(sourceName, new ArrayList<String>());
				edgeMap.get(sourceName).add(targetName);
			}
		}

		final List<String> nodeNames = new ArrayList<>(nodesWithAttributes.keySet());

		return createNewNetwork(networkFactory, networkName, networkAttributes, nodeNames, edgeMap, registerNetwork,
				nodeAttributes);
	}

	public static <TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>, TICONE_NETWORK extends ITiconeNetwork<TICONE_NODE, TICONE_EDGE>> TICONE_NETWORK parseNetworkFromScanner(
			final TiconeNetworkFactory<TICONE_NETWORK> networkFactory, final Scanner scanner, final String networkName,
			final Map<String, Object> networkAttributes, final boolean registerNetwork) {
		final List<String> nodeList = new ArrayList<>();
		final Map<String, List<String>> edgeMap = new HashMap<>();
		while (scanner.hasNext()) {
			final String line = scanner.nextLine();
			final String[] split = line.split("\t");
			final String id1 = split[0];
			final String id2 = split[1];
			if (!nodeList.contains(id1)) {
				nodeList.add(id1);
			}
			if (!nodeList.contains(id2)) {
				nodeList.add(id2);
			}
			if (!edgeMap.containsKey(id1)) {
				edgeMap.put(id1, new ArrayList<String>());
			}
			edgeMap.get(id1).add(id2);
		}
		return createNewNetwork(networkFactory, networkName, networkAttributes, nodeList, edgeMap, registerNetwork,
				null);
	}
}
