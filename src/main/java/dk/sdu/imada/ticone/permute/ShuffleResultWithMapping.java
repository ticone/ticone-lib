/**
 * 
 */
package dk.sdu.imada.ticone.permute;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 10, 2018
 *
 */
public class ShuffleResultWithMapping extends BasicShuffleResult implements IShuffleResultWithMapping {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1991685561377889537L;

	protected IShuffleMapping shuffleMapping;

	/**
	 * @param original
	 * @param shuffled
	 */
	public ShuffleResultWithMapping(final IObjectWithFeatures original, final IObjectWithFeatures shuffled) {
		super(original, shuffled);
	}

	public ShuffleResultWithMapping(final IObjectWithFeatures original, final IObjectWithFeatures shuffled,
			IShuffleMapping shuffleMapping) {
		super(original, shuffled);
		this.shuffleMapping = shuffleMapping;
	}

	/**
	 * @param shuffleMapping the shuffleMapping to set
	 */
	@Override
	public void setShuffleMapping(final IShuffleMapping shuffleMapping) {
		this.shuffleMapping = shuffleMapping;
	}

	@Override
	public boolean hasShuffleMapping() {
		return this.shuffleMapping != null;
	}

	@Override
	public IShuffleMapping getShuffleMapping() {
		return this.shuffleMapping;
	}

}
