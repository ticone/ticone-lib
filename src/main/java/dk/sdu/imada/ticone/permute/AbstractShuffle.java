/**
 * 
 */
package dk.sdu.imada.ticone.permute;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 21, 2019
 *
 */
public abstract class AbstractShuffle implements IShuffle {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2526359267563617915L;

	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	public IShuffleResult shuffle(final IObjectWithFeatures object, final long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		if (!canShuffle(object))
			throw new IncompatibleShuffleException();
		this.validateInitialized();
		return doShuffle(object, seed);
	}

	protected abstract IShuffleResult doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException;

	protected boolean validateInitialized() throws ShuffleNotInitializedException {
		return true;
	}

}
