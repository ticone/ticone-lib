
package dk.sdu.imada.ticone.data;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

/**
 * Class to keep data of time series objects.
 *
 * @author Christian Wiwie
 * @author Christian Norskov
 */
public class TimeSeriesObject implements ITimeSeriesObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5655464102142247845L;
	private final String name;
	private String alternativeName;
	private final Set<String> sampleNames;
	private String[] sampleNameArray;
	private ITimeSeries[] originalTimeSeriesList;
	private Map<IPrototype, ISimilarityValue> patternSimilarities;
	private ISimilarityValue maxSimilarity;
	private ITimeSeries[] preprocessedTimeSeriesList;

	public TimeSeriesObject(final String name) {
		super();
		this.name = name;
		this.patternSimilarities = new HashMap<>();
		this.maxSimilarity = AbstractSimilarityValue.MIN;
		this.sampleNames = new HashSet<>();
	}

	public TimeSeriesObject(final String name, final int numberSamples) {
		this(name);
		this.preprocessedTimeSeriesList = new ITimeSeries[numberSamples];
		this.originalTimeSeriesList = new ITimeSeries[numberSamples];
		this.sampleNameArray = new String[numberSamples];
	}

	/**
	 * Creates an object, with a given name, and the expression data for some time
	 * points.
	 * 
	 * @param name   the name/identifier
	 * @param signal the measurements of each time point in order
	 */
	public TimeSeriesObject(final String name, final int numberSamples, final ITimeSeries signal) {
		this(name, numberSamples, "", signal);
	}

	/**
	 * Creates an object, with a given name, and one sample of expression data for
	 * some time points.
	 * 
	 * @param name       the name/identifier
	 * @param sampleName the name of the sample
	 * @param signal     the measurements of each time point in order
	 */
	public TimeSeriesObject(final String name, final String sampleName, final ITimeSeries signal) {
		this(name, 1);
		this.setOriginalTimeSeries(0, signal, sampleName);
	}

	/**
	 * Creates an object, with a given name, and the expression data for some time
	 * points.
	 * 
	 * @param name       the name/identifier
	 * @param sampleName the name of the sample
	 * @param signal     the measurements of each time point in order
	 */
	public TimeSeriesObject(final String name, final int numberSamples, final String sampleName,
			final ITimeSeries signal) {
		this(name, numberSamples);
		this.setOriginalTimeSeries(0, signal, sampleName);
	}

	/**
	 * Creates an object, with a given name, and the expression data for several
	 * samples.
	 * 
	 * @param name        the name/identifier
	 * @param sampleNames the name of the sample
	 * @param signals     the measurements of each time point in order
	 */
	public TimeSeriesObject(final String name, final String[] sampleNames, final ITimeSeries[] signals) {
		this(name, sampleNames.length);
		this.originalTimeSeriesList = signals;
		for (int s = 0; s < signals.length; s++)
			this.preprocessedTimeSeriesList[s] = null;
		int s = 0;
		for (final String n : sampleNames) {
			this.sampleNames.add(n);
			this.sampleNameArray[s] = n;
			s++;
		}
	}

	public TimeSeriesObject(final TimeSeriesObject other) {
		super();
		this.alternativeName = other.alternativeName;
		this.maxSimilarity = other.maxSimilarity;
		this.name = other.name;
		this.originalTimeSeriesList = new ITimeSeries[other.originalTimeSeriesList.length];
		for (int i = 0; i < this.originalTimeSeriesList.length; i++)
			this.originalTimeSeriesList[i] = other.originalTimeSeriesList[i].copy();
		this.patternSimilarities = new HashMap<>(other.patternSimilarities);
		this.preprocessedTimeSeriesList = new ITimeSeries[other.preprocessedTimeSeriesList.length];
		for (int i = 0; i < this.preprocessedTimeSeriesList.length; i++)
			this.preprocessedTimeSeriesList[i] = other.preprocessedTimeSeriesList[i].copy();
		this.sampleNames = new HashSet<>(other.sampleNames);
		this.sampleNameArray = other.sampleNameArray.clone();
	}

	@Override
	public TimeSeriesObject copy() {
		return new TimeSeriesObject(this);
	}

	@Override
	public void setAlternativeName(final String alternativeName) {
		this.alternativeName = alternativeName;
	}

	@Override
	public String getAlternativeName() {
		return this.alternativeName;
	}

	@Override
	public boolean hasAlternativeName() {
		return this.alternativeName != null;
	}

	@Override
	public List<String> getSampleNameList() {
		return ObjectArrayList.wrap(this.sampleNameArray);
	}

	@Override
	public void addOriginalTimeSeriesToList(final int sampleIdx, final ITimeSeries signal, final String sampleName)
			throws MultipleTimeSeriesSignalsForSameSampleParseException {
		if (this.sampleNameArray[sampleIdx] == null || this.sampleNameArray[sampleIdx].equals(sampleName))
			this.setOriginalTimeSeries(sampleIdx, signal, sampleName);
		else if (this.sampleNames.contains(sampleName)) {
			throw new MultipleTimeSeriesSignalsForSameSampleParseException(this.name, sampleName);
		}
	}

	@Override
	public void addOriginalTimeSeriesToList(final ITimeSeries signal, final String sampleName)
			throws MultipleTimeSeriesSignalsForSameSampleParseException {
		if (this.sampleNames.contains(sampleName))
			throw new MultipleTimeSeriesSignalsForSameSampleParseException(name, sampleName);
		int pos = 0;
		while (this.originalTimeSeriesList[pos] != null)
			pos++;
		this.addOriginalTimeSeriesToList(pos, signal, sampleName);
	}

	@Override
	public void setOriginalTimeSeries(int sampleIdx, ITimeSeries signal, final String sampleName) {
		if (this.sampleNameArray[sampleIdx] != null && !this.sampleNameArray[sampleIdx].equals(sampleName))
			throw new IllegalArgumentException();
		this.sampleNameArray[sampleIdx] = sampleName;
		this.sampleNames.add(sampleName);
		this.originalTimeSeriesList[sampleIdx] = signal;
	}

	@Override
	public ITimeSeries[] getOriginalTimeSeriesList() {
		return this.originalTimeSeriesList;
	}

	@Override
	public void updateNearestPattern(final ICluster pattern, final ISimilarityValue similarity) {
		this.maxSimilarity = similarity;
	}

	@Override
	public void addPreprocessedTimeSeries(final int sampleNumber, final ITimeSeries pattern) {
		this.preprocessedTimeSeriesList[sampleNumber] = pattern;
	}

	@Override
	public ITimeSeries[] getPreprocessedTimeSeriesList() {
		return this.preprocessedTimeSeriesList;
	}

	@Override
	public ISimilarityValue getMaxSimilarity() {
		return this.maxSimilarity;
	}

	@Override
	public Map<IPrototype, ISimilarityValue> getMappingToPatterns() {
		return this.patternSimilarities;
	}

	@Override
	@Deprecated
	public ITimeSeries getOriginalTimeSeries() {
		return this.originalTimeSeriesList[0];
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void addPatternSimilarity(final ICluster pattern, final ISimilarityValue distance) {
		if (this.patternSimilarities == null) {
			this.patternSimilarities = new HashMap<>();
		}
		this.patternSimilarities.put(pattern.getPrototype(), distance);
	}

	@Override
	public void normalize(final double maxValueInDataset, final double minValueInDataset) {
		for (int s = 0; s < this.originalTimeSeriesList.length; s++) {
			final double[] pattern = new double[this.originalTimeSeriesList[s].getNumberTimePoints()];
			for (int tp = 0; tp < pattern.length; tp++) {
				pattern[tp] = (this.originalTimeSeriesList[s].get(tp) - minValueInDataset)
						/ (maxValueInDataset - minValueInDataset);
			}
			this.addPreprocessedTimeSeries(s, new TimeSeries(pattern));
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ITimeSeriesObject))
			return false;
		final ITimeSeriesObject other = (ITimeSeriesObject) obj;
		return Objects.equals(this.name, other.getName());
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	}

	@Override
	public TimeSeriesObjectList asSingletonList() {
		final TimeSeriesObjectList list = new TimeSeriesObjectList();
		list.add(this);
		return list;
	}

	@Override
	public NetworkMappedTimeSeriesObject mapToNetworkNode(final ITiconeNetwork<?, ?> network,
			final ITiconeNetworkNode node) {
		return new NetworkMappedTimeSeriesObject(network, node);
	}

	@Override
	public ITimeSeriesObject removeMappingToNetwork(ITiconeNetwork<?, ?> network) {
		return this;
	}

	public class NetworkMappedTimeSeriesObject implements INetworkMappedTimeSeriesObject {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3303628142152501472L;

		protected final ReadWriteLock rwLock;

		protected Long2ObjectMap<ITiconeNetworkNode> mappedToNodes;

		NetworkMappedTimeSeriesObject(final ITiconeNetwork<?, ?> network, final ITiconeNetworkNode node) {
			super();
			this.rwLock = new ReentrantReadWriteLock();
			this.mappedToNodes = new Long2ObjectOpenHashMap<>();
			this.mappedToNodes.put(network.getUID(), node);
		}

		NetworkMappedTimeSeriesObject(final NetworkMappedTimeSeriesObject other) {
			super();
			this.rwLock = new ReentrantReadWriteLock();
			this.mappedToNodes = new Long2ObjectOpenHashMap<>(other.mappedToNodes);
		}

		@Override
		public ITimeSeriesObject copy() {
			return TimeSeriesObject.this.copy().new NetworkMappedTimeSeriesObject(this);
		}

		@Override
		public int hashCode() {
			return TimeSeriesObject.this.hashCode();
		}

		@Override
		public boolean equals(final Object obj) {
			return TimeSeriesObject.this.equals(obj);
		}

		@Override
		public String toString() {
			return TimeSeriesObject.this.toString();
		}

		@Override
		public ITiconeNetworkNode getNode(final ITiconeNetwork<?, ?> network) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.mappedToNodes.get(network.getUID());
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public boolean isMappedTo(final ITiconeNetwork<?, ?> network) {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.mappedToNodes.containsKey(network.getUID());
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public LongSet mappedNetworks() {
			final Lock readLock = rwLock.readLock();
			readLock.lock();
			try {
				return this.mappedToNodes.keySet();
			} finally {
				readLock.unlock();
			}
		}

		@Override
		public void setAlternativeName(final String alternativeName) {
			TimeSeriesObject.this.setAlternativeName(alternativeName);
		}

		@Override
		public String getAlternativeName() {
			return TimeSeriesObject.this.getAlternativeName();
		}

		@Override
		public boolean hasAlternativeName() {
			return TimeSeriesObject.this.hasAlternativeName();
		}

		@Override
		public List<String> getSampleNameList() {
			return TimeSeriesObject.this.getSampleNameList();
		}

		@Override
		public void addOriginalTimeSeriesToList(final int sampleNumber, final ITimeSeries signal,
				final String sampleName) throws MultipleTimeSeriesSignalsForSameSampleParseException {
			TimeSeriesObject.this.addOriginalTimeSeriesToList(sampleNumber, signal, sampleName);
		}

		@Override
		public void addOriginalTimeSeriesToList(final ITimeSeries signal, final String sampleName)
				throws MultipleTimeSeriesSignalsForSameSampleParseException {
			TimeSeriesObject.this.addOriginalTimeSeriesToList(signal, sampleName);
		}

		@Override
		public void setOriginalTimeSeries(int sampleIdx, ITimeSeries signal, final String sampleName) {
			TimeSeriesObject.this.setOriginalTimeSeries(sampleIdx, signal, sampleName);
		}

		@Override
		public ITimeSeries[] getOriginalTimeSeriesList() {
			return TimeSeriesObject.this.getOriginalTimeSeriesList();
		}

		@Override
		public void updateNearestPattern(final ICluster pattern, final ISimilarityValue sim) {
			TimeSeriesObject.this.updateNearestPattern(pattern, sim);
		}

		@Override
		public void addPreprocessedTimeSeries(final int sampleNumber, final ITimeSeries pattern) {
			TimeSeriesObject.this.addPreprocessedTimeSeries(sampleNumber, pattern);
		}

		@Override
		public ITimeSeries[] getPreprocessedTimeSeriesList() {
			return TimeSeriesObject.this.getPreprocessedTimeSeriesList();
		}

		@Override
		public ISimilarityValue getMaxSimilarity() {
			return TimeSeriesObject.this.getMaxSimilarity();
		}

		@Override
		public Map<IPrototype, ISimilarityValue> getMappingToPatterns() {
			return TimeSeriesObject.this.getMappingToPatterns();
		}

		@Override
		public ITimeSeries getOriginalTimeSeries() {
			return TimeSeriesObject.this.getOriginalTimeSeries();
		}

		@Override
		public String getName() {
			return TimeSeriesObject.this.getName();
		}

		@Override
		public void addPatternSimilarity(final ICluster pattern, final ISimilarityValue distance) {
			TimeSeriesObject.this.addPatternSimilarity(pattern, distance);
		}

		@Override
		public void normalize(final double maxValueInDataset, final double minValueInDataset) {
			TimeSeriesObject.this.normalize(maxValueInDataset, minValueInDataset);
		}

		@Override
		public ITimeSeriesObjects asSingletonList() {
			return TimeSeriesObject.this.asSingletonList();
		}

		@Override
		public NetworkMappedTimeSeriesObject mapToNetworkNode(final ITiconeNetwork<?, ?> network,
				final ITiconeNetworkNode node) {
			if (network == null || node == null)
				return this;

			final Lock writeLock = rwLock.writeLock();
			writeLock.lock();
			try {
				if (!this.isMappedTo(network))
					this.mappedToNodes.put(network.getUID(), node);
				return this;
			} finally {
				writeLock.unlock();
			}
		}

		@Override
		public ITimeSeriesObject removeMappingToNetwork(ITiconeNetwork<?, ?> network) {
			if (network == null)
				return this;
			final Lock writeLock = rwLock.writeLock();
			writeLock.lock();
			try {
				this.mappedToNodes.remove(network.getUID());
				return this;
			} finally {
				writeLock.unlock();
			}
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}

	}

	private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
	}
}
