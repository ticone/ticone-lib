package dk.sdu.imada.ticone.data;

import java.util.PrimitiveIterator.OfInt;
import java.util.Random;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.MultipleTimeSeriesSignalsForSameSampleParseException;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleResultWithMapping;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.util.Utility;

/**
 * This class is used to permute the whole dataset globally; meaning that values
 * are taking at random from the whole dataset with replacements. (Meaning that
 * on specific value can occur more than once).
 *
 * @author Christian Nørskov
 */
public class ShuffleDatasetGlobally extends AbstractShuffle implements IShuffleDataset {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1451209421600206326L;

	@Override
	public ObjectType<ITimeSeriesObjects> supportedObjectType() {
		return ObjectType.OBJECTS;
	}

	@Override
	public ShuffleDatasetGlobally copy() {
		return new ShuffleDatasetGlobally();
	}

	@Override
	public ShuffleDatasetGlobally newInstance() {
		return new ShuffleDatasetGlobally();
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return type == ObjectType.OBJECT;
	}

	/**
	 * Given all time series objects, it generates a new dataset, by filling each
	 * row with random values from the given time series objects.
	 * 
	 * @param timeSeriesDataList the list of timeseries objects
	 * @return
	 * @throws ShuffleNotInitializedException
	 * @throws ShuffleException
	 * @throws InterruptedException
	 * @throws IncompatibleMappingAndObjectTypeException
	 */
	@Override
	public IShuffleResultWithMapping doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final ITimeSeriesObjectList objects = ObjectType.OBJECTS.getBaseType().cast(object).asList();
		final int replicates = objects.get(0).getPreprocessedTimeSeriesList().length;
		final int timePoints = objects.get(0).getPreprocessedTimeSeriesList()[0].getNumberTimePoints();
		final ITimeSeriesObjectList permutedDataList = new TimeSeriesObjectList();
		final Random random = new Random(seed);
		final OfInt randomIndices = random.ints(objects.size() * timePoints, 0, objects.size() * timePoints).iterator();

		final IShuffleMapping mapping = new BasicShuffleMapping();

		for (int o = 0; o < objects.size(); o++) {
			final ITimeSeriesObject shuffledObject = objects.get(o).copy();
			final double[][] dataList = new double[replicates][timePoints];
			for (int tp = 0; tp < timePoints; tp++) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				int randomIdx = randomIndices.nextInt();
				int randomObjIndex = randomIdx / timePoints;
				int randomTpIndex = randomIdx % timePoints;
				final ITimeSeriesObject originalObject = objects.get(randomObjIndex);
				for (int r = 0; r < replicates; r++) {
					dataList[r][tp] = originalObject.getPreprocessedTimeSeriesList()[r].get(randomTpIndex);
				}
			}
			for (int r = 0; r < replicates; r++) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				shuffledObject.addPreprocessedTimeSeries(r, new TimeSeries(dataList[r]));
				try {
					shuffledObject.addOriginalTimeSeriesToList(r, null, "sample" + r);
				} catch (final MultipleTimeSeriesSignalsForSameSampleParseException e) {
					// Should not happen because the original data set worked
					e.printStackTrace();
				}
			}
			permutedDataList.add(shuffledObject);
			mapping.put(objects.get(o), shuffledObject);
		}

		final IShuffleResultWithMapping result = new ShuffleResultWithMapping(objects, permutedDataList);
		result.setShuffleMapping(mapping);
		return result;
	}

	@Override
	public String getName() {
		return "Shuffle object time series (Globally)";
	}
}
