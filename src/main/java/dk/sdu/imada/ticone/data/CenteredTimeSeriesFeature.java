/**
 * 
 */
package dk.sdu.imada.ticone.data;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.feature.AbstractFeature;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Mar 15, 2019
 *
 */
public class CenteredTimeSeriesFeature extends AbstractFeature<ITimeSeries> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7719767817735549949L;

	private final MeanTimeSeriesFeature meanF = new MeanTimeSeriesFeature();

	CenteredTimeSeriesFeature(CenteredTimeSeriesFeature other) {
		super(other);
	}

	public CenteredTimeSeriesFeature() {
		super(ObjectType.TIME_SERIES, ITimeSeries.class);
	}

	@Override
	public String getName() {
		return "Centered Time-series";
	}

	@Override
	public IFeature<ITimeSeries> copy() {
		return new CenteredTimeSeriesFeature(this);
	}

	@Override
	protected IFeatureValue<ITimeSeries> doCalculate(IObjectWithFeatures object)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
		try {
			final ITimeSeries timeSeries = ObjectType.TIME_SERIES.getBaseType().cast(object);
			final double mean = meanF.calculate(timeSeries).getValue().doubleValue();

			final TimeSeries centered = new TimeSeries(ArraysExt.subtract(timeSeries.asArray(), mean));
			final IFeatureValue<ITimeSeries> result = value(object, centered);

			centered.centered = result;
			centered.mean = meanF.value(0.0);

			return result;
		} catch (IncompatibleFeatureAndObjectException e) {
			throw new FeatureCalculationException(e);
		}
	}

}
