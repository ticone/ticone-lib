/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Arrays;

import dk.sdu.imada.ticone.feature.FeatureValueNotSetException;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.VarianceTimeSeriesFeature;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 31, 2018
 *
 */
public class TimeSeries implements ITimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7646282969840495433L;

	public static TimeSeries of(final double... values) {
		return new TimeSeries(values);
	}

	protected double[] timeSeries;

	protected IFeatureValue<Double> mean, var, min, max;

	protected IFeatureValue<ITimeSeries> centered;

	public TimeSeries(final double... timeSeries) {
		super();
		this.timeSeries = timeSeries;
	}

	public TimeSeries(final ITimeSeries other) {
		this(other.asArray().clone());
		if (other instanceof TimeSeries) {
			this.max = ((TimeSeries) other).max;
			this.min = ((TimeSeries) other).min;
			this.mean = ((TimeSeries) other).mean;
			this.centered = ((TimeSeries) other).centered;
			this.var = ((TimeSeries) other).var;
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof ITimeSeries) {
			return Arrays.equals(this.timeSeries, ((ITimeSeries) obj).asArray());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(this.timeSeries);
	}

	@Override
	public String toString() {
		return Arrays.toString(this.timeSeries);
	}

	/**
	 * @return the timeSeries
	 */
	@Override
	public double[] asArray() {
		return this.timeSeries;
	}

	@Override
	public TimeSeries copy() {
		return new TimeSeries(this);
	}

	@Override
	public int getNumberTimePoints() {
		return this.timeSeries.length;
	}

	@Override
	public boolean storesFeatureValues() {
		return true;
	}

	@Override
	public boolean hasFeatureValueSet(IFeature<?> feature) {
		if (feature instanceof MeanTimeSeriesFeature)
			return this.mean != null;
		else if (feature instanceof MinValueTimeSeriesFeature)
			return this.min != null;
		else if (feature instanceof MaxValueTimeSeriesFeature)
			return this.max != null;
		else if (feature instanceof VarianceTimeSeriesFeature)
			return this.var != null;
		else if (feature instanceof CenteredTimeSeriesFeature)
			return this.centered != null;
		return false;
	}

	@Override
	public <V> void setFeatureValue(IFeature<V> feature, IFeatureValue<V> value) {
		if (feature instanceof MeanTimeSeriesFeature)
			this.mean = (IFeatureValue<Double>) value;
		else if (feature instanceof MinValueTimeSeriesFeature)
			this.min = (IFeatureValue<Double>) value;
		else if (feature instanceof MaxValueTimeSeriesFeature)
			this.max = (IFeatureValue<Double>) value;
		else if (feature instanceof VarianceTimeSeriesFeature)
			this.var = (IFeatureValue<Double>) value;
		else if (feature instanceof CenteredTimeSeriesFeature)
			this.centered = (IFeatureValue<ITimeSeries>) value;
	}

	@Override
	public <V> IFeatureValue<V> getFeatureValue(IFeature<V> feature) throws FeatureValueNotSetException {
		if (feature instanceof MeanTimeSeriesFeature)
			return (IFeatureValue<V>) this.mean;
		else if (feature instanceof MinValueTimeSeriesFeature)
			return (IFeatureValue<V>) this.min;
		else if (feature instanceof MaxValueTimeSeriesFeature)
			return (IFeatureValue<V>) this.max;
		else if (feature instanceof VarianceTimeSeriesFeature)
			return (IFeatureValue<V>) this.var;
		else if (feature instanceof CenteredTimeSeriesFeature)
			return (IFeatureValue<V>) this.centered;
		throw new FeatureValueNotSetException(this, feature);
	}
}