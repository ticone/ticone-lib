/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.util.AbstractPairIterable;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IObjectProviderManager;
import dk.sdu.imada.ticone.util.IPairBuilder;
import dk.sdu.imada.ticone.util.IPairsFactory;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectProviderManager;
import dk.sdu.imada.ticone.util.ObjectSampleException;
import dk.sdu.imada.ticone.util.Pair;
import dk.sdu.imada.ticone.util.PairIndexIterable;
import dk.sdu.imada.ticone.util.PairList;
import dk.sdu.imada.ticone.util.PairSet;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 5, 2018
 *
 */
public class ObjectPair extends Pair<ITimeSeriesObject, ITimeSeriesObject> implements IObjectPair {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2078506021690299558L;

	/**
	 * @param el1
	 * @param el2
	 */
	public ObjectPair(final ITimeSeriesObject el1, final ITimeSeriesObject el2) {
		super(Objects.requireNonNull(el1), Objects.requireNonNull(el2));
	}

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Nov 15, 2018
	 *
	 */
	public static class ObjectPairBuilder
			extends AbstractBuilder<IObjectPair, FactoryException, CreateInstanceFactoryException>
			implements IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4696128236538783493L;
		protected transient ITimeSeriesObject first, second;

		@Override
		public ObjectPairBuilder copy() {
			return new ObjectPairBuilder().setFirst(this.first).setSecond(this.second);
		}

		@Override
		public IObjectPair build(ITimeSeriesObject first, ITimeSeriesObject second) {
			return new ObjectPair(first, second);
		}

		@Override
		public IObjectPair doBuild() {
			return new ObjectPair(this.first, this.second);
		}

		@Override
		protected void reset() {
			first = null;
			second = null;
		}

		@Override
		public ObjectPairBuilder setFirst(final ITimeSeriesObject first) {
			this.first = first;
			return this;
		}

		@Override
		public ObjectPairBuilder setSecond(final ITimeSeriesObject second) {
			this.second = second;
			return this;
		}
	}

	public static class ObjectPairsFactory
			implements IPairsFactory<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6567739286478164780L;
		private final ObjectPairBuilder objectPairBuilder = new ObjectPairBuilder();

		@Override
		public IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> getPairBuilder() {
			return objectPairBuilder;
		}

		@Override
		public IObjectPairList createEmptyList() {
			return new ObjectPairList(objectPairBuilder);
		}

		@Override
		public IObjectPairSet createEmptySet() {
			return new ObjectPairSet(objectPairBuilder);
		}

		@Override
		public IObjectPairs createIterable(final ITimeSeriesObject[] objects, final ITimeSeriesObject[] otherObjects) {
			return new ObjectPairIterable(objectPairBuilder, objects, otherObjects);
		}

		@Override
		public IObjectPairList createList(final Iterable<? extends ITimeSeriesObject> objects,
				final Iterable<? extends ITimeSeriesObject> otherObjects) {
			final ObjectPairList result = new ObjectPairList(objectPairBuilder);
			result.addPairsOf(objects, otherObjects);
			return result;
		}

		@Override
		public IObjectPairSet createSet(final Iterable<? extends ITimeSeriesObject> objects,
				final Iterable<? extends ITimeSeriesObject> otherObjects) {
			final ObjectPairSet result = new ObjectPairSet(objectPairBuilder);
			result.addPairsOf(objects, otherObjects);
			return result;
		}

	}

	static class ObjectPairIterable extends AbstractPairIterable<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>
			implements IObjectPairs {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4998993203973247780L;

		protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);
		protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

		/**
		 * 
		 */
		ObjectPairIterable(final IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> objectPairBuilder,
				final ITimeSeriesObject[] objects1, final ITimeSeriesObject[] objects2) {
			super(objects1, objects2, objectPairBuilder);
		}

		ObjectPairIterable(ITimeSeriesObject[] firstObjects, ITimeSeriesObject[] secondObjects,
				PairIndexIterable pairIdxIt,
				final IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> builder) {
			super(firstObjects, secondObjects, pairIdxIt, builder);
		}

		@Override
		public ObjectPairList asList() {
			final ObjectPairList result = new ObjectPairList(builder);
			this.forEach(p -> result.add(p));
			return result;
		}

		@Override
		public ObjectPairSet asSet() {
			final ObjectPairSet result = new ObjectPairSet(builder);
			this.forEach(p -> result.add(p));
			return result;
		}

		@Override
		public IObjectPairs copy() {
			return new ObjectPairIterable(builder, this.firstObjects, this.secondObjects);
		}

		@Override
		public IFeatureValueSampleManager getFeatureValueSampleManager() {
			return this.featureValueSampleManager;
		}

		@Override
		public IObjectProviderManager getObjectProviderManager() {
			return this.objectProviderManager;
		}

		@Override
		public ObjectPairIterable range(final long start, final long end) {
			return new ObjectPairIterable(firstObjects, secondObjects, this.pairIdxIt.range(start, end), builder);
		}

		@Override
		public <T extends IObjectWithFeatures> int getNumberObjectsOfType(ObjectType<T> type)
				throws IncompatibleObjectProviderException {
			if (type == ObjectType.OBJECT_PAIR)
				return this.firstObjects.length * this.firstObjects.length;
			return super.getNumberObjectsOfType(type);
		}

		@Override
		public <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
				throws IncompatibleObjectProviderException {
			if (type == ObjectType.OBJECT_PAIR)
				return (Collection<T>) asList();
			return IObjectPairs.super.getObjectsOfType(type);
		}

		@Override
		public <T extends IObjectWithFeatures> List<T> sampleObjectsOfType(ObjectType<T> type, int sampleSize,
				long seed) throws IncompatibleObjectProviderException, ObjectSampleException, InterruptedException {
			try {
				final Random r = new Random(seed);
				if (type == ObjectType.OBJECT_PAIR) {
					final int[] indices1 = r.ints(sampleSize, 0, this.firstObjects.length).toArray();
					final int[] indices2 = r.ints(sampleSize, 0, this.secondObjects.length).toArray();

					final List<IObjectPair> result = new ArrayList<>();
					for (int p = 0; p < sampleSize; p++)
						result.add(this.builder.build(this.firstObjects[indices1[p]], this.secondObjects[indices2[p]]));
				}
			} catch (FactoryException | CreateInstanceFactoryException e) {
				throw new ObjectSampleException(e);
			}
			return super.sampleObjectsOfType(type, sampleSize, seed);
		}

	}

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Oct 8, 2018
	 *
	 */
	static class ObjectPairList extends PairList<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>
			implements IObjectPairList {

		/**
		 * 
		 */
		private static final long serialVersionUID = 8719487926941704677L;

		protected ObjectPairList(
				final IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> objectPairBuilder) {
			super(objectPairBuilder);
		}

		protected ObjectPairList(

				final Collection<? extends IObjectPair> c,
				final IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> objectPairBuilder) {
			super(c, objectPairBuilder);
		}

		protected ObjectPairList(final ObjectPairList other) {
			this(other, other.builder);
		}

		@Override
		public IObjectPairList copy() {
			return new ObjectPairList(this);
		}

		@Override
		public boolean addPairOf(ITimeSeriesObject o, ITimeSeriesObject o2) {
			return this.add(objectPairBuilder.build(o, o2));
		}

		@Override
		public IObjectPair[] toArray() {
			return super.toArray(new IObjectPair[0]);
		}

		@Override
		public ObjectPairSet asSet() {
			return new ObjectPairSet(this, this.builder);
		}

		@Override
		public String toString() {
			return String.format("%s (%d)", "ObjectPairList", this.size());
		}

		private final ObjectPairBuilder objectPairBuilder = new ObjectPairBuilder();
	}

	/**
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Nov 12, 2018
	 *
	 */
	static class ObjectPairSet extends PairSet<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>
			implements IObjectPairSet {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2337313230471977215L;

		protected ObjectPairSet(final IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> b) {
			super(b);
		}

		protected ObjectPairSet(final IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> b,
				final IObjectPair... objectPairs) {
			this(b);
			for (final IObjectPair o : objectPairs)
				this.add(o);
		}

		protected ObjectPairSet(final ObjectPairSet other) {
			super(other);
		}

		protected ObjectPairSet(final Collection<? extends IObjectPair> objectPairs,
				final IPairBuilder<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> b) {
			super(objectPairs, b);
		}

		@Override
		public IObjectPair[] toArray() {
			return super.toArray(new IObjectPair[0]);
		}

		@Override
		public ObjectPairSet asSet() {
			return new ObjectPairSet(this, builder);
		}

		@Override
		public ObjectPairSet copy() {
			return new ObjectPairSet(this);
		}

		@Override
		public IObjectPairList asList() {
			return new ObjectPairList(this, this.builder);
		}

		@Override
		public boolean addPairOf(ITimeSeriesObject object, ITimeSeriesObject otherObject) {
			return this.add(new ObjectPair(object, otherObject));
		}

		@Override
		public String toString() {
			return String.format("%s (%d)", "ObjectPairSet", this.size());
		}

		private final ObjectPairBuilder objectPairBuilder = new ObjectPairBuilder();
	}

}
