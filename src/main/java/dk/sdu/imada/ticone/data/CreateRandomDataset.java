package dk.sdu.imada.ticone.data;

import java.util.Random;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.MultipleTimeSeriesSignalsForSameSampleParseException;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleResultWithMapping;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.util.Utility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Mar 12, 2019
 *
 */
public class CreateRandomDataset extends AbstractShuffle implements IShuffleDataset {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1226894384494143822L;

	private IDiscretizeTimeSeries discretizeTimeSeries;

	public CreateRandomDataset() {
	}

	public CreateRandomDataset(CreateRandomDataset other) {
		super();
		this.discretizeTimeSeries = other.discretizeTimeSeries;
	}

	@Override
	public ObjectType<ITimeSeriesObjects> supportedObjectType() {
		return ObjectType.OBJECTS;
	}

	@Override
	public CreateRandomDataset copy() {
		return new CreateRandomDataset(this);
	}

	@Override
	public CreateRandomDataset newInstance() {
		return new CreateRandomDataset();
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return type == ObjectType.OBJECT;
	}

	/**
	 * @param discretizeTimeSeries the discretizeTimeSeries to set
	 */
	public void setDiscretizeTimeSeries(IDiscretizeTimeSeries discretizeTimeSeries) {
		this.discretizeTimeSeries = discretizeTimeSeries;
	}

	/**
	 * @return the discretizeTimeSeries
	 */
	public IDiscretizeTimeSeries getDiscretizeTimeSeries() {
		return this.discretizeTimeSeries;
	}

	/**
	 * Given all time series objects, it generates a new dataset, by filling each
	 * row with random values from the given time series objects.
	 * 
	 * @param timeSeriesDataList the list of timeseries objects
	 * @return
	 * @throws ShuffleNotInitializedException
	 * @throws ShuffleException
	 * @throws InterruptedException
	 * @throws IncompatibleMappingAndObjectTypeException
	 */
	@Override
	public IShuffleResultWithMapping doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final ITimeSeriesObjectList objects = ObjectType.OBJECTS.getBaseType().cast(object).asList();
		double minValue = Double.POSITIVE_INFINITY, maxValue = Double.NEGATIVE_INFINITY;
		for (ITimeSeriesObject o : objects) {
			ITimeSeries[] preprocessedTimeSeriesList = o.getPreprocessedTimeSeriesList();
			for (int i = 0; i < preprocessedTimeSeriesList.length; i++)
				for (int j = 0; j < preprocessedTimeSeriesList[i].getNumberTimePoints(); j++) {
					double val = preprocessedTimeSeriesList[i].get(j);
					if (val < minValue)
						minValue = val;
					if (val > maxValue)
						maxValue = val;
				}
		}

		final ITimeSeriesObjectList permutedObjectList = new TimeSeriesObjectList();

		final IShuffleMapping mapping = new BasicShuffleMapping();

		final Random random = new Random(seed);

		for (int o = 0; o < objects.size(); o++) {
			final ITimeSeriesObject originalObject = objects.get(o);
			final int replicates = originalObject.getPreprocessedTimeSeriesList().length;
			final ITimeSeriesObject shuffledObject = new TimeSeriesObject(objects.get(o).getName(), replicates);
			for (int r = 0; r < replicates; r++) {
				final int timePoints = originalObject.getPreprocessedTimeSeriesList()[r].getNumberTimePoints();
				final double[] randomValues = random.doubles(timePoints, minValue, maxValue).toArray();
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				ITimeSeries timeSeries = new TimeSeries(randomValues);
				if (this.discretizeTimeSeries != null)
					timeSeries = this.discretizeTimeSeries.discretizeObjectTimeSeries(timeSeries);
				shuffledObject.addPreprocessedTimeSeries(r, timeSeries);
				try {
					shuffledObject.addOriginalTimeSeriesToList(r, timeSeries, "sample" + r);
				} catch (final MultipleTimeSeriesSignalsForSameSampleParseException e) {
					// Should not happen because the original data set worked
					e.printStackTrace();
				}
			}
			permutedObjectList.add(shuffledObject);
			mapping.put(originalObject, shuffledObject);
		}

		final IShuffleResultWithMapping result = new ShuffleResultWithMapping(objects, permutedObjectList);
		result.setShuffleMapping(mapping);
		return result;
	}

	@Override
	public String getName() {
		return "Create random data set";
	}
}
