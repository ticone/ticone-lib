/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Random;

import dk.sdu.imada.ticone.clustering.prototype.permute.IShuffleTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 5, 2017
 *
 */
public class ShuffleTimeSeries extends AbstractShuffle implements IShuffleTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1026849077823545619L;

	@Override
	public ObjectType<ITimeSeries> supportedObjectType() {
		return ObjectType.TIME_SERIES;
	}

	@Override
	public ShuffleTimeSeries copy() {
		return new ShuffleTimeSeries();
	}

	@Override
	public ShuffleTimeSeries newInstance() {
		return new ShuffleTimeSeries();
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResult doShuffle(IObjectWithFeatures object, long seed) throws ShuffleException,
			ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException {
		final ITimeSeries timeSeries = ObjectType.TIME_SERIES.getBaseType().cast(object);
		final double[] shuffled = timeSeries.asArray().clone();

		// we just shuffle the entries, i.e. we do draw the prototype values
		// without replacement
		this.shuffleArray(shuffled, seed);

		final TimeSeries result = new TimeSeries(shuffled);
		return new BasicShuffleResult(timeSeries, result);
	}

	private void shuffleArray(final double[] array, final long seed) {
		final Random random = new Random(seed);
		int index;
		double temp;
		for (int i = 0; i < array.length; i++) {
			index = random.nextInt(array.length);
			temp = array[index];
			array[index] = array[i];
			array[i] = temp;
		}
	}

	@Override
	public String getName() {
		return "Permute prototype time series";
	}
}
