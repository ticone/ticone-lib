/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairBuilder;
import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeriesObject.NetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.util.IObjectProviderManager;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectProviderManager;
import dk.sdu.imada.ticone.util.ObjectSampleException;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public class TimeSeriesObjectList extends ObjectArrayList<ITimeSeriesObject> implements ITimeSeriesObjectList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2564646131029426862L;
	protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);
	protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

	/**
	 * 
	 */
	public TimeSeriesObjectList() {
		super();
	}

	/**
	 * 
	 */
	public TimeSeriesObjectList(final int capacity) {
		super(capacity);
	}

	public TimeSeriesObjectList(final ITimeSeriesObject... objects) {
		super(objects);
	}

	public TimeSeriesObjectList(final Collection<ITimeSeriesObject> os) {
		super(os);
	}

	@Override
	public TimeSeriesObjectSet asSet() {
		return new TimeSeriesObjectSet(this);
	}

	@Override
	public ITimeSeriesObject[] toArray() {
		final ITimeSeriesObject[] res = new ITimeSeriesObject[this.size];
		System.arraycopy(this.a, 0, res, 0, this.size);
		return res;
	}

	@Override
	public String toString() {
		return String.format("TimeSeriesObjectList (%d)", this.size());
	}

	@Override
	public TimeSeriesObjectList copy() {
		return new TimeSeriesObjectList(this);
	}

	@Override
	public IObjectPairs getObjectPairs() {
		final ITimeSeriesObject[] array = this.toArray();
		return new ObjectPairsFactory().createIterable(array, array);
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}

	@Override
	public boolean contains(String objectName) {
		return this.stream().anyMatch(o -> o.getName().equals(objectName));
	}

	@Override
	public ITimeSeriesObject get(String objectName) {
		for (ITimeSeriesObject o : this)
			if (o.getName().equals(objectName))
				return o;
		return null;
	}

	@Override
	public TimeSeriesObjectList mapToNetwork(final ITiconeNetwork<?, ?> network) {
		if (network == null)
			return this;
		for (int i = 0; i < this.size(); i++) {
			final ITimeSeriesObject o = this.get(i);

			if (o instanceof NetworkMappedTimeSeriesObject) {
				if (!((NetworkMappedTimeSeriesObject) o).isMappedTo(network))
					o.mapToNetworkNode(network, network.getNode(o.getName()));
			} else {
				final ITiconeNetworkNode n = network.getNode(o.getName());
				if (n != null)
					this.set(i, o.mapToNetworkNode(network, n));
			}
		}
		return this;
	}

	@Override
	public ITimeSeriesObjectList removeMappingToNetwork(ITiconeNetwork<?, ?> network) {
		if (network == null)
			return this;
		for (int i = 0; i < this.size(); i++) {
			final ITimeSeriesObject o = this.get(i);
			o.removeMappingToNetwork(network);
		}
		return this;
	}

	@Override
	public <T extends IObjectWithFeatures> List<T> sampleObjectsOfType(ObjectType<T> type, int sampleSize, long seed)
			throws IncompatibleObjectProviderException, ObjectSampleException {
		final Random random = new Random(seed);
		if (type == ObjectType.OBJECT) {
			final int[] indices = random.ints(sampleSize, 0, this.size()).toArray();
			final List<ITimeSeriesObject> result = new ArrayList<>();
			for (int i : indices)
				result.add(this.get(i));
			return (List<T>) result;
		} else if (type == ObjectType.OBJECT_PAIR) {
			final int[] indices1 = random.ints(sampleSize, 0, this.size()).toArray();
			final int[] indices2 = random.ints(sampleSize, 0, this.size()).toArray();
			final List<IObjectPair> result = new ArrayList<>();
			final ObjectPairBuilder opb = new ObjectPairBuilder();
			for (int p = 0; p < sampleSize; p++)
				result.add(opb.build(this.get(indices1[p]), this.get(indices2[p])));
			return (List<T>) result;
		}
		throw new ObjectSampleException();
	}

}
