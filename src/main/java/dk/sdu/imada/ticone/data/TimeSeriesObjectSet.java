/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Collection;
import java.util.LinkedHashSet;

import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.util.IObjectProviderManager;
import dk.sdu.imada.ticone.util.ObjectProviderManager;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 12, 2018
 *
 */
public class TimeSeriesObjectSet extends LinkedHashSet<ITimeSeriesObject> implements ITimeSeriesObjectSet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2564646131029426862L;
	protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);
	protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

	/**
	 * 
	 */
	public TimeSeriesObjectSet() {
		super();
	}

	public TimeSeriesObjectSet(final ITimeSeriesObject... objects) {
		this();
		for (final ITimeSeriesObject o : objects)
			this.add(o);
	}

	/**
	 * 
	 */
	public TimeSeriesObjectSet(final Collection<ITimeSeriesObject> os) {
		this();
		this.addAll(os);
	}

	@Override
	public TimeSeriesObjectList asList() {
		return new TimeSeriesObjectList(this);
	}

	@Override
	public ITimeSeriesObject[] toArray() {
		return this.asList().toArray();
	}

	@Override
	public TimeSeriesObjectSet copy() {
		return new TimeSeriesObjectSet(this);
	}

	@Override
	public IObjectPairs getObjectPairs() {
		final ITimeSeriesObject[] array = this.toArray();
		return new ObjectPairsFactory().createIterable(array, array);
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}

	@Override
	public TimeSeriesObjectSet mapToNetwork(final ITiconeNetwork<?, ?> network) {
		if (network == null)
			return this;

		this.clear();
		final TimeSeriesObjectList asList = this.asList();
		asList.mapToNetwork(network);
		this.addAll(asList);
		return this;
	}

	@Override
	public ITimeSeriesObjectSet removeMappingToNetwork(ITiconeNetwork<?, ?> network) {
		if (network == null)
			return this;

		this.clear();
		final TimeSeriesObjectList asList = this.asList();
		asList.removeMappingToNetwork(network);
		this.addAll(asList);
		return this;
	}

	@Override
	public boolean contains(String objectName) {
		return this.stream().anyMatch(o -> o.getName().equals(objectName));
	}

	@Override
	public ITimeSeriesObject get(String objectName) {
		for (ITimeSeriesObject o : this)
			if (o.getName().equals(objectName))
				return o;
		return null;
	}

	@Override
	public String toString() {
		return String.format("TimeSeriesObjectSet (%d)", this.size());
	}
}
