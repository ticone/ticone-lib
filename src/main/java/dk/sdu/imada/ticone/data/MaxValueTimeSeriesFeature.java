/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Arrays;

import dk.sdu.imada.ticone.feature.AbstractFeature;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.INumericFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 14, 2019
 *
 */
public class MaxValueTimeSeriesFeature extends AbstractFeature<Double> implements INumericFeature<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8228168223825043065L;

	MaxValueTimeSeriesFeature(MaxValueTimeSeriesFeature other) {
		super(other);
	}

	public MaxValueTimeSeriesFeature() {
		super(ObjectType.TIME_SERIES, Double.class);
	}

	@Override
	public String getName() {
		return "Time-series Max";
	}

	@Override
	public IFeature<Double> copy() {
		return new MaxValueTimeSeriesFeature(this);
	}

	@Override
	protected IFeatureValue<Double> doCalculate(IObjectWithFeatures object)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
		final ITimeSeries timeSeries = ObjectType.TIME_SERIES.getBaseType().cast(object);
		return value(object, Arrays.stream(timeSeries.asArray()).max().getAsDouble());
	}

}
