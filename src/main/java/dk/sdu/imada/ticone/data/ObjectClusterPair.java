/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Collection;
import java.util.Objects;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.util.AbstractPairIterable;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IObjectProviderManager;
import dk.sdu.imada.ticone.util.IPairBuilder;
import dk.sdu.imada.ticone.util.IPairsFactory;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectProviderManager;
import dk.sdu.imada.ticone.util.Pair;
import dk.sdu.imada.ticone.util.PairIndexIterable;
import dk.sdu.imada.ticone.util.PairList;
import dk.sdu.imada.ticone.util.PairSet;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 5, 2018
 *
 */
public class ObjectClusterPair extends Pair<ITimeSeriesObject, ICluster> implements IObjectClusterPair {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3728734733104392678L;

	/**
	 * @param el1
	 * @param el2
	 */
	public ObjectClusterPair(final ITimeSeriesObject el1, final ICluster el2) {
		super(Objects.requireNonNull(el1), Objects.requireNonNull(el2));
	}

	@Override
	public ICluster getCluster() {
		return this.getSecond();
	}

	@Override
	public ITimeSeriesObject getObject() {
		return this.getFirst();
	}

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Nov 15, 2018
	 *
	 */
	public static class ObjectClusterPairBuilder
			extends AbstractBuilder<IObjectClusterPair, FactoryException, CreateInstanceFactoryException>
			implements IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6545675930580817179L;
		protected transient ITimeSeriesObject first;
		protected transient ICluster second;

		@Override
		public ObjectClusterPairBuilder copy() {
			return new ObjectClusterPairBuilder().setFirst(this.first).setSecond(this.second);
		}

		@Override
		public IObjectClusterPair build(ITimeSeriesObject first, ICluster second) {
			return new ObjectClusterPair(first, second);
		}

		@Override
		public IObjectClusterPair doBuild() {
			return new ObjectClusterPair(this.first, this.second);
		}

		@Override
		protected void reset() {
			first = null;
			second = null;
		}

		@Override
		public ObjectClusterPairBuilder setFirst(final ITimeSeriesObject first) {
			this.first = first;
			return this;
		}

		@Override
		public ObjectClusterPairBuilder setSecond(final ICluster second) {
			this.second = second;
			return this;
		}

	}

	public static class ObjectClusterPairsFactory
			implements IPairsFactory<ITimeSeriesObject, ICluster, IObjectClusterPair> {

		@Override
		public IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> getPairBuilder() {
			return new ObjectClusterPairBuilder();
		}

		@Override
		public IObjectClusterPairList createEmptyList() {
			return new ObjectClusterPairList(getPairBuilder());
		}

		@Override
		public IObjectClusterPairSet createEmptySet() {
			return new ObjectClusterPairSet(getPairBuilder());
		}

		@Override
		public IObjectClusterPairs createIterable(final ITimeSeriesObject[] objects, final ICluster[] otherObjects) {
			return new ObjectClusterPairIterable(objects, otherObjects);
		}

		@Override
		public IObjectClusterPairList createList(final Iterable<? extends ITimeSeriesObject> objects,
				final Iterable<? extends ICluster> otherObjects) {
			final ObjectClusterPairList result = new ObjectClusterPairList(getPairBuilder());
			result.addPairsOf(objects, otherObjects);
			return result;
		}

		@Override
		public IObjectClusterPairSet createSet(final Iterable<? extends ITimeSeriesObject> objects,
				final Iterable<? extends ICluster> otherObjects) {
			final ObjectClusterPairSet result = new ObjectClusterPairSet(getPairBuilder());
			result.addPairsOf(objects, otherObjects);
			return result;
		}

	}

	/**
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Jan 14, 2019
	 *
	 */
	static class ObjectClusterPairIterable extends AbstractPairIterable<ITimeSeriesObject, ICluster, IObjectClusterPair>
			implements IObjectClusterPairs {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7547648196138460567L;

		protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);
		protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

		/**
		 * 
		 */
		ObjectClusterPairIterable(final ITimeSeriesObject[] objects1, final ICluster[] objects2) {
			super(objects1, objects2, new ObjectClusterPairBuilder());
		}

		ObjectClusterPairIterable(ITimeSeriesObject[] firstObjects, ICluster[] secondObjects,
				PairIndexIterable pairIdxIt, IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> builder) {
			super(firstObjects, secondObjects, pairIdxIt, builder);
		}

		@Override
		public IObjectClusterPairs copy() {
			return new ObjectClusterPairIterable(this.firstObjects, this.secondObjects);
		}

		@Override
		public ObjectClusterPairList asList() {
			final ObjectClusterPairList result = new ObjectClusterPairList(this.builder);
			this.forEach(p -> result.add(p));
			return result;
		}

		@Override
		public ObjectClusterPairSet asSet() {
			final ObjectClusterPairSet result = new ObjectClusterPairSet(this.builder);
			this.forEach(p -> result.add(p));
			return result;
		}

		@Override
		public IFeatureValueSampleManager getFeatureValueSampleManager() {
			return this.featureValueSampleManager;
		}

		@Override
		public IObjectProviderManager getObjectProviderManager() {
			return this.objectProviderManager;
		}

		@Override
		public ObjectClusterPairIterable range(final long start, final long end) {
			return new ObjectClusterPairIterable(firstObjects, secondObjects, this.pairIdxIt.range(start, end),
					builder);
		}

		@Override
		public <T extends IObjectWithFeatures> int getNumberObjectsOfType(ObjectType<T> type)
				throws IncompatibleObjectProviderException {
			if (type == ObjectType.OBJECT_CLUSTER_PAIR)
				return firstObjects.length * secondObjects.length;
			return super.getNumberObjectsOfType(type);
		}

		@Override
		public <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
				throws IncompatibleObjectProviderException {
			if (type.equals(ObjectType.OBJECT_CLUSTER_PAIR))
				return (Collection<T>) asList();
			return IObjectClusterPairs.super.getObjectsOfType(type);
		}
	}

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Oct 8, 2018
	 *
	 */
	public static class ObjectClusterPairList extends PairList<ITimeSeriesObject, ICluster, IObjectClusterPair>
			implements IObjectClusterPairList {

		/**
		 * 
		 */
		private static final long serialVersionUID = 522576117352047179L;

		ObjectClusterPairList(final IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> builder) {
			super(builder);
		}

		ObjectClusterPairList(final Collection<? extends IObjectClusterPair> pairs,
				IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> builder) {
			super(pairs, builder);
		}

		ObjectClusterPairList(final Iterable<? extends ITimeSeriesObject> objects,
				final Iterable<? extends ICluster> otherObjects,
				IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> builder) {
			this(builder);
			this.addPairsOf(objects, otherObjects);
		}

		public ObjectClusterPairList addPairsOf(final ITimeSeriesObjects objects, final IClusters clusters) {
			for (final ITimeSeriesObject o : objects)
				this.addPairsOf(o, clusters.toArray(new ICluster[clusters.size()]));
			return this;
		}

		public ObjectClusterPairList addPairsOf(final ICluster cluster,
				final Collection<? extends ITimeSeriesObject> objects) {
			for (final ITimeSeriesObject object : objects)
				this.addPairOf(object, cluster);
			return this;
		}

		@Override
		public boolean addPairOf(ITimeSeriesObject object, ICluster otherObject) {
			return this.add(new ObjectClusterPair(object, otherObject));
		}

		@Override
		public String toString() {
			return String.format("%s (%d)", "ObjectClusterPairs", this.size());
		}

		@Override
		public IObjectClusterPair[] toArray() {
			return super.toArray(new IObjectClusterPair[0]);
		}

		@Override
		public IObjectClusterPairSet asSet() {
			return new ObjectClusterPairSet(this, this.builder);
		}

		@Override
		public ObjectClusterPairList copy() {
			return new ObjectClusterPairList(this, this.builder);
		}
	}

	/**
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Jan 14, 2019
	 *
	 */
	static class ObjectClusterPairSet extends PairSet<ITimeSeriesObject, ICluster, IObjectClusterPair>
			implements IObjectClusterPairSet {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5373418550070763896L;

		protected ObjectProviderManager objectProviderManager;

		protected FeatureValueSampleManager featureValueSampleManager;

		ObjectClusterPairSet(final IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> builder) {
			super(builder);
			this.objectProviderManager = new ObjectProviderManager(this);
		}

		ObjectClusterPairSet(final IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> builder,
				final IObjectClusterPair... pairs) {
			this(builder);
			for (final IObjectClusterPair o : pairs)
				this.add(o);
		}

		ObjectClusterPairSet(final Collection<? extends IObjectClusterPair> pairs,
				final IPairBuilder<ITimeSeriesObject, ICluster, IObjectClusterPair> builder) {
			super(pairs, builder);
		}

		@Override
		public IObjectProviderManager getObjectProviderManager() {
			return this.objectProviderManager;
		}

		@Override
		public IFeatureValueSampleManager getFeatureValueSampleManager() {
			return this.featureValueSampleManager;
		}

		@Override
		public IObjectClusterPair[] toArray() {
			return super.toArray(new IObjectClusterPair[0]);
		}

		@Override
		public ObjectClusterPairSet asSet() {
			return new ObjectClusterPairSet(this, this.builder);
		}

		@Override
		public ObjectClusterPairSet copy() {
			return new ObjectClusterPairSet(this, this.builder);
		}

		@Override
		public IObjectClusterPairList asList() {
			return new ObjectClusterPairList(this, this.builder);
		}

		@Override
		public boolean addPairOf(ITimeSeriesObject object, ICluster otherObject) {
			return this.add(new ObjectClusterPair(object, otherObject));
		}

		@Override
		public String toString() {
			return String.format("%s (%d)", "ObjectClusterPairSet", this.size());
		}
	}
}
