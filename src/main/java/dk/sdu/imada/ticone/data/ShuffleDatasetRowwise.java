package dk.sdu.imada.ticone.data;

import java.util.Random;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.MultipleTimeSeriesSignalsForSameSampleParseException;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleResultWithMapping;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.util.Utility;

/**
 * This class is used to permute the whole dataset rowwise, meaning that the
 * values for each object are shuffled around.
 *
 * @author Christian Nørskov
 */
public class ShuffleDatasetRowwise extends AbstractShuffle implements IShuffleDataset {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3179889512406569960L;

	@Override
	public ObjectType<ITimeSeriesObjects> supportedObjectType() {
		return ObjectType.OBJECTS;
	}

	@Override
	public ShuffleDatasetRowwise copy() {
		return new ShuffleDatasetRowwise();
	}

	@Override
	public ShuffleDatasetRowwise newInstance() {
		return new ShuffleDatasetRowwise();
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return type == ObjectType.OBJECT;
	}

	/**
	 * Given all time series objects, it permutes each row individually by shuffling
	 * the values.
	 * 
	 * @param timeSeriesDataList the list of timeseries objects
	 * @return returns the permuted dataset.
	 * @throws ShuffleNotInitializedException
	 * @throws ShuffleException
	 * @throws InterruptedException
	 * @throws IncompatibleMappingAndObjectTypeException
	 */
	@Override
	public IShuffleResultWithMapping doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final ITimeSeriesObjectList timeSeriesDataList = ObjectType.OBJECTS.getBaseType().cast(object).asList();
		final ITimeSeriesObjectList permutedTimeSeriesData = new TimeSeriesObjectList();

		final IShuffleMapping mapping = new BasicShuffleMapping();

		final Random random = new Random(seed);

		for (int i = 0; i < timeSeriesDataList.size(); i++) {
			if (!Utility.getProgress().getStatus())
				throw new InterruptedException();
			final ITimeSeriesObject permuteRow = this.permuteRow(timeSeriesDataList.get(i), random);
			permutedTimeSeriesData.add(permuteRow);
			mapping.put(timeSeriesDataList.get(i), permuteRow);
		}
		final IShuffleResultWithMapping result = new ShuffleResultWithMapping(timeSeriesDataList,
				permutedTimeSeriesData);
		result.setShuffleMapping(mapping);

		return result;
	}

	private ITimeSeriesObject permuteRow(final ITimeSeriesObject tsd, final Random random) {
		final int timePoints = tsd.getPreprocessedTimeSeriesList()[0].getNumberTimePoints();
		final int replicates = tsd.getPreprocessedTimeSeriesList().length;
		final double[][] patterns = new double[replicates][timePoints];

		final int[] randomIndices = random.ints(timePoints, 0, timePoints).toArray();

		for (int tp = 0; tp < timePoints; tp++) {
			final int valueIndex = randomIndices[tp];
			for (int r = 0; r < replicates; r++) {
				patterns[r][tp] = tsd.getPreprocessedTimeSeriesList()[r].get(valueIndex);
			}
		}
		final ITimeSeriesObject permTSD = tsd.copy();
		for (int i = 0; i < replicates; i++) {
			permTSD.addPreprocessedTimeSeries(i, new TimeSeries(patterns[i]));
			try {
				permTSD.addOriginalTimeSeriesToList(i, null, "s" + i);
			} catch (final MultipleTimeSeriesSignalsForSameSampleParseException e) {
				// Should not happen because the original data set worked
				e.printStackTrace();
			}
		}
		return permTSD;
	}

	@Override
	public String getName() {
		return "Shuffle object time series (Individually)";
	}
}
