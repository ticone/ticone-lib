/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Random;

import dk.sdu.imada.ticone.clustering.prototype.permute.IShuffleTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.util.Utility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 24, 2017
 *
 */
public class CreateRandomTimeSeries extends AbstractShuffle implements IShuffleTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6158949956059857462L;

	protected ITimeSeriesObjectList allObjects;

	protected boolean preserveTimepoints;

	public CreateRandomTimeSeries() {
		this(null);
	}

	/**
	 * 
	 */
	public CreateRandomTimeSeries(final ITimeSeriesObjectList allObjects) {
		super();
		this.allObjects = allObjects;
	}

	@Override
	public ObjectType<ITimeSeries> supportedObjectType() {
		return ObjectType.TIME_SERIES;
	}

	@Override
	public CreateRandomTimeSeries copy() {
		final CreateRandomTimeSeries createRandomTimeSeries = new CreateRandomTimeSeries(this.allObjects);
		createRandomTimeSeries.preserveTimepoints = this.preserveTimepoints;
		return createRandomTimeSeries;
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.allObjects == null)
			throw new ShuffleNotInitializedException("allObjects");
		return true;
	}

	@Override
	public CreateRandomTimeSeries newInstance() {
		final CreateRandomTimeSeries res = new CreateRandomTimeSeries();
		return res;
	}

	/**
	 * @param allObjects the allObjects to set
	 */
	public void setAllObjects(final ITimeSeriesObjectList allObjects) {
		this.allObjects = allObjects;
	}

	/**
	 * @param preserveTimepoints the preserveTimepoints to set
	 */
	public void setPreserveTimepoints(boolean preserveTimepoints) {
		this.preserveTimepoints = preserveTimepoints;
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResult doShuffle(IObjectWithFeatures object, long seed) throws ShuffleException,
			ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException {
		final ITimeSeries timeSeries = ObjectType.TIME_SERIES.getBaseType().cast(object);
		if (timeSeries.asArray().length == 0)
			throw new ShuffleException("Can only create random prototypes with at least 1 time point.");
		final Random random = new Random(seed);
		final double[] randomPrototype = new double[timeSeries.asArray().length];
		for (int tp = 0; tp < randomPrototype.length; tp++) {
			if (!Utility.getProgress().getStatus())
				throw new InterruptedException();
			final ITimeSeriesObject ro = this.allObjects.get(random.nextInt(this.allObjects.size()));
			final int rs = random.nextInt(ro.getOriginalTimeSeriesList().length);
			final int rtp = !this.preserveTimepoints
					? random.nextInt(ro.getOriginalTimeSeriesList()[rs].getNumberTimePoints())
					: tp;
			randomPrototype[tp] = ro.getOriginalTimeSeriesList()[rs].get(rtp);
		}
		final ITimeSeries res = new TimeSeries(randomPrototype);
		return new BasicShuffleResult(timeSeries, res);
	}

	@Override
	public String getName() {
		return "Create prototypes with random time series";
	}

	@Override
	public String toString() {
		return String.format("%s (preserveTimepoints=%b)", super.toString(), this.preserveTimepoints);
	}
}
