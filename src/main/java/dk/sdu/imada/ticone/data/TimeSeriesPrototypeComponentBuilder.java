/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Objects;

import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder.ITimeSeriesPrototypeComponent;
import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 1, 2018
 *
 */
public class TimeSeriesPrototypeComponentBuilder extends
		AbstractBuilder<ITimeSeriesPrototypeComponent, PrototypeComponentFactoryException, CreateInstanceFactoryException>
		implements ITimeSeriesPrototypeComponentBuilder {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5992433620798336278L;

	protected IAggregateCluster<ITimeSeries> aggregationFunction;

	protected IDiscretizeTimeSeries discretizeFunction;

	protected transient ITimeSeries timeSeries;

	protected transient ITimeSeriesObjects objects;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.aggregationFunction == null) ? 0 : this.aggregationFunction.hashCode());
		result = prime * result + ((this.discretizeFunction == null) ? 0 : this.discretizeFunction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeSeriesPrototypeComponentBuilder other = (TimeSeriesPrototypeComponentBuilder) obj;
		if (this.aggregationFunction == null) {
			if (other.aggregationFunction != null)
				return false;
		} else if (!this.aggregationFunction.equals(other.aggregationFunction))
			return false;
		if (this.discretizeFunction == null) {
			if (other.discretizeFunction != null)
				return false;
		} else if (!this.discretizeFunction.equals(other.discretizeFunction))
			return false;
		return true;
	}

	@Override
	public TimeSeriesPrototypeComponentBuilder copy() {
		final TimeSeriesPrototypeComponentBuilder f = new TimeSeriesPrototypeComponentBuilder();
		if (this.aggregationFunction != null)
			f.aggregationFunction = this.aggregationFunction.copy();
		if (this.discretizeFunction != null)
			f.discretizeFunction = this.discretizeFunction.copy();
		f.timeSeries = this.timeSeries;
		f.objects = this.objects;
		return f;
	}

	/**
	 * @param aggregationFunction the aggregationFunction to set
	 */
	@Override
	public TimeSeriesPrototypeComponentBuilder setAggregationFunction(
			final IAggregateCluster<ITimeSeries> aggregationFunction) {
		this.aggregationFunction = aggregationFunction;
		return this;
	}

	/**
	 * @param discretizeFunction the discretizeFunction to set
	 */
	public TimeSeriesPrototypeComponentBuilder setDiscretizeFunction(final IDiscretizeTimeSeries discretizeFunction) {
		this.discretizeFunction = discretizeFunction;
		return this;
	}

	@Override
	public IAggregateCluster<ITimeSeries> getAggregationFunction() {
		return this.aggregationFunction;
	}

	@Override
	public IDiscretizeTimeSeries getDiscretizeFunction() {
		return this.discretizeFunction;
	}

	@Override
	public PrototypeComponentType getType() {
		return PrototypeComponentType.TIME_SERIES;
	}

	@Override
	protected TimeSeriesPrototypeComponent doBuild() throws PrototypeComponentFactoryException {
		if (this.timeSeries != null) {
			if (this.discretizeFunction != null)
				this.timeSeries = this.discretizeFunction.discretizeObjectTimeSeries(this.timeSeries);
			return new TimeSeriesPrototypeComponent(this.timeSeries);
		} else if (this.objects != null) {
			if (this.aggregationFunction == null)
				throw new PrototypeComponentFactoryException("No cluster aggregation function has been set");
			try {
				ITimeSeries aggregateCluster = this.aggregationFunction.aggregate(this.objects);
				if (this.discretizeFunction != null)
					aggregateCluster = this.discretizeFunction.discretizeObjectTimeSeries(aggregateCluster);
				return new TimeSeriesPrototypeComponent(aggregateCluster);
			} catch (final Exception e) {
				throw new PrototypeComponentFactoryException(e);
			}
		}
		throw new PrototypeComponentFactoryException(
				"Either a time-series or objects and an aggregation function have to be specified.");
	}

	@Override
	protected void reset() {
		objects = null;
		timeSeries = null;
	}

	@Override
	public ITimeSeries getTimeSeries() {
		return this.timeSeries;
	}

	@Override
	public TimeSeriesPrototypeComponentBuilder setTimeSeries(final ITimeSeries timeSeries) {
		this.timeSeries = timeSeries;
		return this;
	}

	@Override
	public ITimeSeriesPrototypeComponentBuilder setTimeSeries(final double[] timeSeries) {
		this.timeSeries = new TimeSeries(timeSeries);
		return this;
	}

	@Override
	public ITimeSeriesObjects getObjects() {
		return this.objects;
	}

	@Override
	public TimeSeriesPrototypeComponentBuilder setObjects(final ITimeSeriesObjects objects) {
		this.objects = objects;
		return this;
	}
}

class TimeSeriesPrototypeComponent implements ITimeSeriesPrototypeComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8855787699158530006L;

	protected ITimeSeries timeSeries;

	/**
	 * 
	 */
	TimeSeriesPrototypeComponent(final ITimeSeries timeSeries) {
		super();
		this.timeSeries = timeSeries;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof TimeSeriesPrototypeComponent)
			return Objects.equals(this.timeSeries, ((TimeSeriesPrototypeComponent) obj).timeSeries);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.timeSeries);
	}

	@Override
	public String toString() {
		return this.timeSeries.toString();
	}

	@Override
	public ITimeSeries getTimeSeries() {
		return this.timeSeries;
	}

	@Override
	public TimeSeriesPrototypeComponent copy() {
		return new TimeSeriesPrototypeComponent(this.timeSeries.copy());
	}

	@Override
	public PrototypeComponentType getType() {
		return PrototypeComponentType.TIME_SERIES;
	}
}
