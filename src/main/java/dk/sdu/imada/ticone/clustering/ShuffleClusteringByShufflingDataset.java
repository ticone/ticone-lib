/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Random;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.permute.BasicShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public class ShuffleClusteringByShufflingDataset extends AbstractShuffleClusteringWithSimilarityReassignment {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1755871987744257117L;

	protected IShuffleDataset shuffleDataset;
	protected IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder;
	protected boolean randomNumberClusters;

	public ShuffleClusteringByShufflingDataset(final ShuffleClusteringByShufflingDataset s) {
		super(s);
		this.clusteringMethodBuilder = s.clusteringMethodBuilder;
		this.shuffleDataset = s.shuffleDataset.copy();
		this.randomNumberClusters = s.randomNumberClusters;
	}

	public ShuffleClusteringByShufflingDataset(
			final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder,
			final int numberOfClusters, final IShuffleDataset shuffleDataset)
			throws IncompatibleSimilarityFunctionException {
		super(clusteringMethodBuilder.getSimilarityFunction());
		this.clusteringMethodBuilder = clusteringMethodBuilder;
		this.shuffleDataset = shuffleDataset;
	}

	@Override
	public ShuffleClusteringByShufflingDataset copy() {
		return new ShuffleClusteringByShufflingDataset(this);
	}

	@Override
	public boolean validateParameters() {
		// TODO: number of clusters?
		return true;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.shuffleDataset == null)
			throw new ShuffleNotInitializedException("shuffleDataset");
		if (this.clusteringMethodBuilder == null)
			throw new ShuffleNotInitializedException("clusteringMethod");
		return super.validateInitialized();
	}

	@Override
	public ShuffleClusteringByShufflingDataset newInstance() {
		// TODO: do we need to deep clone some stuff?
		ShuffleClusteringByShufflingDataset shuffleClusteringByShufflingDataset = new ShuffleClusteringByShufflingDataset(
				this);
		shuffleClusteringByShufflingDataset.randomNumberClusters = this.randomNumberClusters;
		return shuffleClusteringByShufflingDataset;
	}

	/**
	 * @return the shuffleDataset
	 */
	public IShuffleDataset getShuffleDataset() {
		return this.shuffleDataset;
	}

	@Override
	public String getName() {
		return this.shuffleDataset.getName();
	}

	@Override
	public boolean producesShuffleMappingFor(final ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResult doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final IClusterObjectMapping clustering = supportedObjectType().getBaseType().cast(object);
		final IShuffleResult shuffleResult = this.shuffleDataset.shuffle(clustering.getAllObjects(), seed);
		final ITimeSeriesObjects shuffledObjects = (ITimeSeriesObjects) shuffleResult.getShuffled();
		shuffledObjects.removeIf(o -> {
			try {
				return !similarityFunction.isDefinedFor(o);
			} catch (InterruptedException e1) {
				return false;
			}
		});
		try {
			final Random random = new Random(seed);
			final IClusterList originalClusters = clustering.getClusters().copy().asList();
			int maxNumberClusters = shuffledObjects.size();
			int numberClusters;
			if (!randomNumberClusters)
				numberClusters = originalClusters.size();
			else {
				numberClusters = (int) Math
						.abs(Math.round(random.nextGaussian() * originalClusters.size() + originalClusters.size()));
				numberClusters = Math.max(Math.min(maxNumberClusters, numberClusters), 1);
			}

			final IClusteringMethod<ClusterObjectMapping> clusteringMethod = this.clusteringMethodBuilder.copy()
					.build();
			ClusterObjectMapping shuffledClustering = clusteringMethod.findClusters(shuffledObjects, numberClusters,
					seed);
			return new BasicShuffleResult(clustering, shuffledClustering);
		} catch (final ClusterOperationException | ClusteringMethodFactoryException
				| CreateClusteringMethodInstanceFactoryException e) {
			throw new ShuffleException(e);
		}
	}

	public IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> getClusteringMethodBuilder() {
		return this.clusteringMethodBuilder;
	}

	/**
	 * @param randomNumberClusters the randomNumberClusters to set
	 */
	public void setRandomNumberClusters(boolean randomNumberClusters) {
		this.randomNumberClusters = randomNumberClusters;
	}

	@Override
	public void setSimilarityFunction(ISimilarityFunction similarityFunction) {
		super.setSimilarityFunction(similarityFunction);
		if (clusteringMethodBuilder != null)
			clusteringMethodBuilder.setSimilarityFunction(similarityFunction);
	}
}
