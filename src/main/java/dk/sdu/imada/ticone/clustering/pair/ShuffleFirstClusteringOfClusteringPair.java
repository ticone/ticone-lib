/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IShuffleClustering;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleResultWithMapping;
import dk.sdu.imada.ticone.util.Utility;

public class ShuffleFirstClusteringOfClusteringPair extends AbstractShuffle implements IShuffleClusteringPair {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2532588531674002592L;

	final IShuffleClustering shuffleClustering1;

	public ShuffleFirstClusteringOfClusteringPair(final IShuffleClustering shuffleClustering1) {
		super();
		this.shuffleClustering1 = shuffleClustering1;
	}

	@Override
	public ObjectType<IClusterObjectMappingPair> supportedObjectType() {
		return ObjectType.CLUSTERING_PAIR;
	}

	@Override
	public ShuffleFirstClusteringOfClusteringPair copy() {
		return new ShuffleFirstClusteringOfClusteringPair(this.shuffleClustering1.copy());
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.shuffleClustering1 == null)
			throw new ShuffleNotInitializedException("shuffleClustering1");
		return true;
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return type == ObjectType.CLUSTER_PAIR && this.shuffleClustering1 != null
				&& this.shuffleClustering1.producesShuffleMappingFor(ObjectType.CLUSTER);
	}

	@Override
	public IShuffleResultWithMapping doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final IClusterObjectMappingPair pair = supportedObjectType().getBaseType().cast(object);
		final IShuffleResult shuffleResult1 = this.shuffleClustering1.shuffle(pair.getClustering1(), seed);
		final IClusterObjectMapping random1 = (IClusterObjectMapping) shuffleResult1.getShuffled();

		final IShuffleResultWithMapping result = new ShuffleResultWithMapping(pair,
				new ClusterObjectMappingPair(random1, pair.getClustering2()));
		if (shuffleResult1 instanceof IShuffleResultWithMapping) {
			final IShuffleResultWithMapping castResult = (IShuffleResultWithMapping) shuffleResult1;
			final IShuffleMapping shuffleMapping = castResult.getShuffleMapping();

			final IShuffleMapping pairShuffleMapping = new BasicShuffleMapping();
			for (final IClusterPair originalClusterPair : pair.getClusterPairs()) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				final IClusterPair shufflePair = new ClusterPair(shuffleMapping.get(originalClusterPair.getFirst()),
						originalClusterPair.getSecond());

				pairShuffleMapping.put(originalClusterPair, shufflePair);
			}
			result.setShuffleMapping(shuffleMapping);
		}
		return result;
	}

	@Override
	public String getName() {
		return "asd";
	}

	@Override
	public ShuffleFirstClusteringOfClusteringPair newInstance() {
		return new ShuffleFirstClusteringOfClusteringPair(this.shuffleClustering1.newInstance());
	}
}