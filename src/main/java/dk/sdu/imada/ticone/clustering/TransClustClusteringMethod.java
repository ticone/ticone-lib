package dk.sdu.imada.ticone.clustering;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.clustering.wraptransclust.ConsoleWrapper;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ITimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.similarity.TimeSeriesNotCompatibleException;
import dk.sdu.imada.ticone.util.AggregationException;
import dk.sdu.imada.ticone.util.IProgress;
import dk.sdu.imada.ticone.util.Utility;

/**
 * This class finds the initial clustering of the time series data using the
 * external library TransClust. It implements the interface
 * InitialClusteringInterface.
 *
 * @author Christian Nørskov
 * @see IClusteringMethod
 * @see ITimeSeriesObject
 */
public class TransClustClusteringMethod extends AbstractClusteringMethod<ClusterObjectMapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4003205135279839349L;

	private boolean initialized = false;

	private IProgress progress;
	private double pairwiseSimilarityThreshold;
	private String temporaryDirectoryPath = "data" + File.separator;
	private double minSimilarity = 1;
	private ITimeSeriesObjectList objects;

	/**
	 * The initialize function for this class, that should be called before using
	 * the interface.
	 *
	 * @param discretizePatternFunc
	 * @param ISimilarityFunc
	 * @param pairwiseSimilarityThreshold
	 * @throws IncompatibleSimilarityFunctionException
	 * @see IDiscretizeTimeSeries
	 * @see ITimeSeriesSimilarityFunction
	 * @see ITimeSeriesObject
	 * @see ClusterObjectMapping
	 */
	TransClustClusteringMethod(final IPrototypeBuilder prototypeFactory, final ISimilarityFunction ISimilarityFunc,
			final double pairwiseSimilarityThreshold) throws IncompatibleSimilarityFunctionException {
		super(ISimilarityFunc, prototypeFactory);
		if (pairwiseSimilarityThreshold > 1 || pairwiseSimilarityThreshold < 0) {
			logger.error("Incorrect input.");
			return;
		}
		this.pairwiseSimilarityThreshold = pairwiseSimilarityThreshold;
		this.initialized = true;
	}

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	TransClustClusteringMethod(TransClustClusteringMethod other) {
		super(other);
		this.pairwiseSimilarityThreshold = other.pairwiseSimilarityThreshold;
		this.initialized = other.initialized;
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction && similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	public IClusteringMethod<ClusterObjectMapping> copy() {
		return new TransClustClusteringMethod(this);
	}

	public double getPairwiseSimilarityThreshold() {
		return this.pairwiseSimilarityThreshold;
	}

	/**
	 * This function starts the clustering of the time series data with TransClust,
	 * and returns a mapping of the found clustering.
	 * 
	 * @param timeSeriesDatas  the list of all time series objects
	 * @param numberOfClusters the specified number of clusters wanted
	 * @return returns the Pattern Object Mapping of the clusters.
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	@Override
	protected ClusterObjectMapping doFindClusters(final ITimeSeriesObjects timeSeriesDatas, final int numberOfClusters,
			final long seed) throws ClusterOperationException, InterruptedException {
		if (!this.initialized || timeSeriesDatas == null || numberOfClusters < 1) {
			logger.error("Wrong input or not initialized.");
			return null;
		}
		this.objects = timeSeriesDatas.asList();
		try {
			final IPrototypeBuilder prototypeBuilder = this.prototypeBuilder.copy();
			if (numberOfClusters == 1) {
				return this.oneCluster(this.objects, prototypeBuilder);
			}
			this.progress = Utility.getProgress();

			this.createTemporaryDirectory();

			// String similarityFileName = temporaryDirectoryPath +
			// "temp_similarities.txt";
			// String clusterFileName = temporaryDirectoryPath +
			// "temp_clusters.txt";
			File clusterFile;
			File similarityFile;
			try {
				clusterFile = File.createTempFile("transclust", "clusterFile");
				similarityFile = File.createTempFile("transclust", "similarityFile");
			} catch (final IOException e) {
				e.printStackTrace();
				return null;
			}

			final HashMap<String, ITimeSeriesObject> timeSeriesDataHashMap = new HashMap<>();
			for (final ITimeSeriesObject timeSeriesData : timeSeriesDatas) {
				timeSeriesDataHashMap.put(timeSeriesData.getName(), timeSeriesData);
			}

			if (!this.progress.getStatus()) {
				return null;
			}
			this.calculatePairWiseSimilarities(this.objects, similarityFile.getAbsolutePath(),
					this.pairwiseSimilarityThreshold, this.progress);

			if (!this.progress.getStatus()) {
				throw new InterruptedException();
			}

			this.progress.updateProgress(this.progress.getProgress(), "Clustering objects.");
			String[] clustersLine;
			clustersLine = this.binarySearchForClusters(clusterFile.getAbsolutePath(), similarityFile.getAbsolutePath(),
					numberOfClusters);

			if (!this.progress.getStatus()) {
				throw new InterruptedException();
			}

			this.progress.updateProgress(this.progress.getProgress(), "Mapping object to clusters.");

			final ClusterObjectMapping patternObjectMapping = this.mapPatternsFromClusters(clustersLine[2].split(";"),
					timeSeriesDataHashMap, prototypeBuilder);

			return patternObjectMapping;
		} catch (SimilarityCalculationException | CreatePrototypeInstanceFactoryException | PrototypeFactoryException
				| ClusterFactoryException | CreateClusterInstanceFactoryException | IncompatibleObjectTypeException
				| SimilarityValuesException | IncompatibleSimilarityValueException | IncompatiblePrototypeException
				| DuplicateMappingForObjectException e) {
			throw new ClusterOperationException(e);
		}
	}

	/**
	 *
	 * @param timeSeriesDatas
	 * @return
	 * @throws SimilarityCalculationException
	 * @throws PrototypeFactoryException
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws IncompatibleObjectTypeException
	 * @throws IncompatibleSimilarityValueException
	 * @throws SimilarityValuesException
	 * @throws DuplicateMappingForObjectException
	 */
	private ClusterObjectMapping oneCluster(final ITimeSeriesObjectList timeSeriesDatas,
			final IPrototypeBuilder prototypeBuilder) throws SimilarityCalculationException,
			CreatePrototypeInstanceFactoryException, PrototypeFactoryException, InterruptedException,
			ClusterFactoryException, CreateClusterInstanceFactoryException, IncompatibleObjectTypeException,
			SimilarityValuesException, IncompatibleSimilarityValueException, DuplicateMappingForObjectException {
		final ClusterObjectMapping patternObjectMapping = new ClusterObjectMapping(timeSeriesDatas.asSet().copy(),
				prototypeBuilder);
		patternObjectMapping.addCluster(timeSeriesDatas, getSimilarityFunction());
		return patternObjectMapping;
	}

	public void createTemporaryDirectory() {
		final File temporaryDirectory = new File(this.temporaryDirectoryPath);
		temporaryDirectory.mkdirs();
	}

	public void updatePathToTemporaryDirectory(final String path) {
		this.temporaryDirectoryPath = path;
	}

	/**
	 * Makes a system call to TransClust.jar with the given parameters.
	 * 
	 * @param similarityFileName the name for the similarity file.
	 * @param clusterFileName    the name for the result file, where the clusters
	 *                           will be printed to.
	 * @param minThreshold       the minimum threshold.
	 * @param maxThreshold       the maximum threshold.
	 * @param tss                the threshold step size after each iteration.
	 */
	private void clusterObjects(final String similarityFileName, final String clusterFileName,
			final double minThreshold, final double maxThreshold, double tss) throws InterruptedException {
		if (tss == 0) {
			tss = 1;
		}
		final String options = "-i " + similarityFileName + " -sim " + similarityFileName + " -o " + clusterFileName
				+ " -minT " + minThreshold + " -maxT " + maxThreshold + " -tss " + tss + " -mode 2 -verbose";
		// TransClust.main(options.split(" "));
		try {
			new ConsoleWrapper(options.split(" "));
		} catch (final Exception e) {
			throw new InterruptedException("Stopped");
		}
	}

	/**
	 * Uses transclust in a binary search for finding the most suitable clustering,
	 * based on the number of patterns wanted.
	 * 
	 * @param clusterFileName
	 * @param similarityFileName
	 * @param numberOfPatternsWanted
	 * @return
	 */
	private String[] binarySearchForClusters(final String clusterFileName, final String similarityFileName,
			final int numberOfPatternsWanted) throws InterruptedException {
		String[] line = null;
		int currentNumberOfClusters = -1;
		double minThreshold = this.minSimilarity;
		BigDecimal minThresholdBD = new BigDecimal(minThreshold);
		minThresholdBD = minThresholdBD.setScale(2, BigDecimal.ROUND_DOWN);
		minThreshold = minThresholdBD.doubleValue();
		double maxThreshold = 1;
		// TODO: FIX Binary search for the better

		String[] lineWithMoreThanWanted = null;
		while (Math.abs(minThreshold - maxThreshold) > 0.0025 && numberOfPatternsWanted != currentNumberOfClusters) {
			if (!this.progress.getStatus()) {
				throw new InterruptedException("Stopped");
			}

			double currentThreshold = (minThreshold + maxThreshold) / 2;
			// Discretize search steps (to reduce runtime, and avoid infinite
			// searches)
			BigDecimal bd = new BigDecimal(currentThreshold);
			bd = bd.setScale(3, BigDecimal.ROUND_CEILING);
			currentThreshold = bd.doubleValue();
			try {
				this.clusterObjects(similarityFileName, clusterFileName, currentThreshold, currentThreshold, 1);

				final File clusterFile = new File(clusterFileName);
				final Scanner sc = new Scanner(clusterFile);
				long waitingTime = 0;
				while (!sc.hasNext() && waitingTime < 5000) {
					waitingTime += 50;
					try {
						Thread.sleep(50);
					} catch (final InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (sc.hasNext()) {
					line = sc.nextLine().split("\t");
					currentNumberOfClusters = line[2].split(";").length;
				} else {
					currentNumberOfClusters = 0;
				}

				if (lineWithMoreThanWanted == null) {
					lineWithMoreThanWanted = line;
				}

				if (currentNumberOfClusters > numberOfPatternsWanted
						&& currentNumberOfClusters < lineWithMoreThanWanted[2].split(";").length) {
					lineWithMoreThanWanted = line;
				}

				if (currentNumberOfClusters < numberOfPatternsWanted) {
					minThreshold = currentThreshold;
				} else if (currentNumberOfClusters > numberOfPatternsWanted) {
					maxThreshold = currentThreshold;
				}
			} catch (final IOException ioe) {
				return null;
			}
		}

		if (!Utility.getProgress().getStatus()) {
			throw new InterruptedException("Stopped");
		}
		if (line[2].split(";").length < numberOfPatternsWanted) {
			return lineWithMoreThanWanted;
		}
		return line;
	}

	/**
	 * Given a set of clusters, generate patterns from these clusters, and map the
	 * time series objects to the specified patterns
	 * 
	 * @param clusters the clusters from TransClust
	 * @return the pattern to object mappings.
	 * @throws AggregationException
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws IncompatibleObjectTypeException
	 * @throws IncompatiblePrototypeException
	 * @throws DuplicateMappingForObjectException
	 */
	private ClusterObjectMapping mapPatternsFromClusters(final String[] clusters,
			final Map<String, ITimeSeriesObject> timeSeriesDataHashMap, final IPrototypeBuilder prototypeBuilder)
			throws CreatePrototypeInstanceFactoryException, PrototypeFactoryException, SimilarityCalculationException,
			InterruptedException, ClusterFactoryException, CreateClusterInstanceFactoryException,
			IncompatibleObjectTypeException, IncompatiblePrototypeException, DuplicateMappingForObjectException {
		final ClusterObjectMapping pom = new ClusterObjectMapping(
				new TimeSeriesObjectSet(timeSeriesDataHashMap.values()), prototypeBuilder);
		for (final String cluster : clusters) {
			final String[] timeSeriesDataForCurrentPattern = cluster.split(",");

			final IPrototype pattern = this.calculatePattern(timeSeriesDataForCurrentPattern, timeSeriesDataHashMap,
					prototypeBuilder);

			final ICluster p = pom.addCluster(pattern);

			this.addObjectsToMapping(timeSeriesDataForCurrentPattern, timeSeriesDataHashMap, p, pom);
		}
		return pom;
	}

	private void addObjectsToMapping(final String[] timeSeriesDataForPattern,
			final Map<String, ITimeSeriesObject> timeSeriesDataHashMap, final ICluster pattern,
			final ClusterObjectMapping pom) throws SimilarityCalculationException, InterruptedException,
			IncompatibleObjectTypeException, DuplicateMappingForObjectException {
		for (final String object : timeSeriesDataForPattern) {
			final ITimeSeriesObject tsd = timeSeriesDataHashMap.get(object);
			final ISimilarityValue similarity = this.getSimilarityFunction()
					.calculateSimilarity(new ObjectClusterPair(tsd, pattern));
			pom.addMapping(tsd, pattern, similarity);
		}
	}

	private IPrototype calculatePattern(final String[] timeSeriesDataForPattern,
			final Map<String, ITimeSeriesObject> timeSeriesDataHashMap, final IPrototypeBuilder prototypeBuilder)
			throws TimeSeriesNotCompatibleException, CreatePrototypeInstanceFactoryException, PrototypeFactoryException,
			InterruptedException {
		IPrototype pattern;
		final ITimeSeriesObjects timeSeriesDatas = new TimeSeriesObjectList();
		for (int i = 0; i < timeSeriesDataForPattern.length; i++) {
			timeSeriesDatas.add(timeSeriesDataHashMap.get(timeSeriesDataForPattern[i]));
		}
		pattern = prototypeBuilder.setObjects(timeSeriesDatas).build();
		return pattern;
	}

	public void calculatePairWiseSimilarities(final ITimeSeriesObjectList timeSeriesDatas, final String output,
			final double threshold, final IProgress progress)
			throws InterruptedException, SimilarityCalculationException, IncompatibleObjectTypeException {
		try {
			final PrintWriter printWriter = new PrintWriter(output);
			Double similarity;
			for (int i = 0; i < timeSeriesDatas.size(); i++) {
				if (!progress.getStatus()) {
					printWriter.close();
					throw new InterruptedException();
				}
				if (i % (Math.ceil((double) timeSeriesDatas.size() / 100)) == 0) {
					final int percent = (int) (((double) i / timeSeriesDatas.size()) * 100);
					progress.updateProgress(progress.getProgress(),
							"Calculating pairwise similarities: " + percent + "%");
				}
				for (int j = 0; j < timeSeriesDatas.size(); j++) {
					similarity = this.getSimilarityFunction()
							.calculateSimilarity(new ObjectPair(timeSeriesDatas.get(i), timeSeriesDatas.get(j)))
							.calculate().get();
					if (similarity >= threshold) {
						printWriter.println(timeSeriesDatas.get(i).getName() + "\t" + timeSeriesDatas.get(j).getName()
								+ "\t" + similarity);
						if (similarity < this.minSimilarity) {
							this.minSimilarity = similarity;
						}
					}

				}
				printWriter.flush();
			}
			printWriter.close();
		} catch (final FileNotFoundException fnfe) {
			logger.error(fnfe.getMessage(), fnfe);
		}
	}

	@Override
	public String toString() {
		return "TransClust";
	}
}
