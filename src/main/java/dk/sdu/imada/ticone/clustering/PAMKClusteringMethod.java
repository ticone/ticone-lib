package dk.sdu.imada.ticone.clustering;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair;
import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototype;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesSomePairs;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.AggregationException;
import dk.sdu.imada.ticone.util.Utility;
import it.unimi.dsi.fastutil.ints.Int2ObjectLinkedOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * @author Christian Wiwie
 * @author Christian Norskov
 * @since 9/21/15
 */
public class PAMKClusteringMethod extends ParallelizedClusteringMethod<ClusterObjectMapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8663046512063356412L;

	private final int swapIterations, nstart;
	private final double swapProbability;

	PAMKClusteringMethod(final ISimilarityFunction similarityFunction, final int swapIterations, final int nstart,
			final double swapProbability, final IPrototypeBuilder prototypeFactory)
			throws IncompatibleSimilarityFunctionException {
		super(similarityFunction, prototypeFactory);
		this.swapIterations = swapIterations;
		this.nstart = nstart;
		this.swapProbability = swapProbability;
	}

	PAMKClusteringMethod(PAMKClusteringMethod other) {
		super(other);
		this.swapIterations = other.swapIterations;
		this.nstart = other.nstart;
		this.swapProbability = other.swapProbability;
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction && similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	public IClusteringMethod<ClusterObjectMapping> copy() {
		return new PAMKClusteringMethod(this);
	}

	/**
	 * @return the nstart
	 */
	public int getNstart() {
		return this.nstart;
	}

	/**
	 * @return the maxIterations
	 */
	public int getSwapIterations() {
		return this.swapIterations;
	}

	long seed;

	@Override
	protected ClusterObjectMapping doFindClusters(final ITimeSeriesObjects allObjects, final int numberOfClusters,
			final long seed) throws ClusterOperationException, InterruptedException {
		final long startTime = System.currentTimeMillis();
		IntList[] clusters = new IntList[numberOfClusters];
		int[] clustersMedoids = new int[numberOfClusters];

		try {
			double swapProbability = this.swapProbability;
			if (swapProbability < 0)
				swapProbability = recommendedSwapProbability(allObjects.size(), numberOfClusters);

			this.seed = seed;
			final Random random = new Random(seed);
			if (numberOfClusters == 1)
				return this.oneCluster(allObjects, prototypeBuilder);

			final int[] allObjectIdx;
			final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities;
			if (this.similarities == null) {
				similarities = similarityFunction.emptySimilarities(allObjects.toArray(), allObjects.toArray(),
						new ObjectPairsFactory(), ObjectType.OBJECT_PAIR);
				allObjectIdx = similarities.firstObjectIndices();
			} else {
				similarities = this.similarities;
				allObjectIdx = allObjects.stream().mapToInt(o -> similarities.getObject1Index(o)).toArray();
			}

			ISimilarityValue bestSimilarity = similarityFunction.missingValuePlaceholder();
			ClusterObjectMapping bestClustering = null;
			for (int n = 0; n < nstart; n++) {
				int[] nonMedoidIndicesArray = this.setStartMedoids(allObjectIdx, numberOfClusters, clustersMedoids,
						random);
				this.logger.trace("medoids:\t" + Arrays.toString(clustersMedoids));

				IntSet startAssignedObjects = new IntOpenHashSet();
				ISimilarityValue startMaxSimilarity = this.assignObjectsToNearestMedoid(nonMedoidIndicesArray, clusters,
						clustersMedoids, startAssignedObjects, similarities);
				this.logger.trace("clusters:\t" + Arrays.toString(clusters));
				this.logger.trace("similarity:\t" + startMaxSimilarity.get());

				if (startMaxSimilarity.greaterThan(bestSimilarity)) {
					bestClustering = this.createMapping(allObjects.asList(), clusters, clustersMedoids, similarities,
							prototypeBuilder);
					bestSimilarity = startMaxSimilarity;

//					System.out.println("PAMK: (start*=" + n + ")\t" + bestSimilarity);
				}

				int iteration = 0;

				ISimilarityValue tmpSimilarity = null;

				while (swapProbability > 0 && iteration++ < this.swapIterations) {
					final double[] swapsRandoms = random.doubles(clustersMedoids.length * nonMedoidIndicesArray.length)
							.toArray();

					final int[] newNonMedoidIndicesArray = nonMedoidIndicesArray.clone();
					final int[] newClustersMedoids = clustersMedoids.clone();
					final IntList[] newClusters = clusters.clone();
					final IntSet newAssignedObjects = new IntOpenHashSet();

					boolean didAnySwaps = false;

					for (int m = 0; m < clustersMedoids.length; m++) {
						for (int o = 0; o < nonMedoidIndicesArray.length; o++) {
							final int idx = m * nonMedoidIndicesArray.length + o;
							if (swapsRandoms[idx] < swapProbability) {
								int tmp = newClustersMedoids[m];
								newClustersMedoids[m] = newNonMedoidIndicesArray[o];
								newNonMedoidIndicesArray[o] = tmp;
								if (!Utility.getProgress().getStatus()) {
									throw new InterruptedException();
								}
								didAnySwaps = true;
							}
						}
					}

					if (!didAnySwaps)
						continue;

					tmpSimilarity = this.assignObjectsToNearestMedoid(newNonMedoidIndicesArray, newClusters,
							newClustersMedoids, newAssignedObjects, similarities);

					if (tmpSimilarity.greaterThan(startMaxSimilarity)) {
						startMaxSimilarity = tmpSimilarity;
						clusters = newClusters;
						clustersMedoids = newClustersMedoids;
						nonMedoidIndicesArray = newNonMedoidIndicesArray;
						startAssignedObjects = newAssignedObjects;
					}
				}
				;

				if (startMaxSimilarity.greaterThan(bestSimilarity)) {
					bestClustering = this.createMapping(allObjects.asList(), clusters, clustersMedoids, similarities,
							prototypeBuilder);
					bestSimilarity = startMaxSimilarity;
//					System.out.println("PAMK: (start=" + n + ")\t" + bestSimilarity);
				}

//				System.out.println(bestClustering.asObjectPartition());
//				System.out.println(bestClustering.getUnassignedObjects());
			}
			this.logger.debug(String.format("PAMK:\t%d\tN=%d\tk=%d\tnstart=%d\tswapProbability=%.3f",
					System.currentTimeMillis() - startTime, allObjects.size(), numberOfClusters, nstart,
					swapProbability));
			return bestClustering;
		} catch (SimilarityCalculationException | AggregationException | IncompatiblePrototypeException
				| CreatePrototypeInstanceFactoryException | PrototypeFactoryException | ClusterFactoryException
				| CreateClusterInstanceFactoryException | SimilarityValuesException | IncompatibleObjectTypeException
				| IncompatibleSimilarityValueException | DuplicateMappingForObjectException e) {
			throw new ClusterOperationException(e);
		}
	}

	/**
	 * @param numberObjects
	 * @param numberClusters
	 * @return
	 */
	public static double recommendedSwapProbability(final int numberObjects, final int numberClusters) {
		return 0.2;
	}

	private ClusterObjectMapping oneCluster(final ITimeSeriesObjects allObjects,
			final IPrototypeBuilder prototypeBuilder)
			throws AggregationException, SimilarityCalculationException, IncompatiblePrototypeException,
			CreatePrototypeInstanceFactoryException, PrototypeFactoryException, InterruptedException,
			ClusterFactoryException, CreateClusterInstanceFactoryException, IncompatibleObjectTypeException,
			SimilarityValuesException, IncompatibleSimilarityValueException, DuplicateMappingForObjectException {
		final ClusterObjectMapping clustering = new ClusterObjectMapping(allObjects.asSet().copy(), prototypeBuilder);
		clustering.addCluster(allObjects, similarityFunction);
		return clustering;
	}

	/**
	 * Creates the patternobjectmapping from the found clusters.
	 * 
	 * @return
	 * @throws AggregationException
	 * @throws SimilarityCalculationException
	 * @throws IncompatiblePrototypeException
	 * @throws PrototypeFactoryException
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws IncompatibleObjectTypeException
	 * @throws DuplicateMappingForObjectException
	 */
	private ClusterObjectMapping createMapping(final ITimeSeriesObjectList allObjects, final IntList[] clusters,
			final int[] clusterMedoids,
			final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> sims,
			final IPrototypeBuilder prototypeBuilder) throws AggregationException, SimilarityCalculationException,
			IncompatiblePrototypeException, CreatePrototypeInstanceFactoryException, PrototypeFactoryException,
			InterruptedException, ClusterFactoryException, CreateClusterInstanceFactoryException,
			IncompatibleObjectTypeException, DuplicateMappingForObjectException {
		final ClusterObjectMapping clustering = new ClusterObjectMapping(allObjects.asSet().copy(), prototypeBuilder);

		for (int i = 0; i < clusters.length; i++) {
			try {
				if (clusters[i].isEmpty())
					continue;
				final IPrototype prototype;
				final TimeSeriesObjectList objects = clusters[i].stream().map(idx -> sims.getObject1(idx))
						.collect(Collectors.toCollection(TimeSeriesObjectList::new));
				prototype = prototypeBuilder.setObjects(objects).build();
				if (prototype instanceof MissingPrototype)
					continue;
				final ICluster cluster = clustering.addCluster(prototype);
				if (!(similarityFunction.isDefinedFor(cluster)))
					clustering.removeCluster(cluster);
				else {
					final ISimilarityValuesSomePairs<ITimeSeriesObject, ICluster, IObjectClusterPair> clusterSims = similarityFunction
							.calculateSimilarities(new ObjectClusterPair.ObjectClusterPairsFactory().createList(objects,
									cluster.asSingletonList()), ObjectType.OBJECT_CLUSTER_PAIR);
					int o = 0;
					for (ISimilarityValue similarity : clusterSims) {
						final ITimeSeriesObject tsd = clusterSims.getPair(o).getFirst();
						clustering.addMapping(tsd, cluster, similarity);
						o++;
					}
				}
			} catch (SimilarityValuesException | IncompatibleSimilarityValueException e) {
				// TODO: skipping that cluster
			}
		}
		return clustering;
	}

	private ISimilarityValue assignObjectsToNearestMedoid(final int[] nonMedoidIndicesArray, final IntList[] clusters,
			final int[] clustersMedoids, final IntSet assignedObjects,
			ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> sims)
			throws InterruptedException, ClusterOperationException, IncompatibleSimilarityValueException {
		final Int2ObjectMap<IntList> medoidToCluster = new Int2ObjectLinkedOpenHashMap<>();

		ISimilarityValue similarity = similarityFunction.missingValuePlaceholder();

		try {
			final int[] medoidIndices = Arrays.stream(clustersMedoids).filter(m -> m > -1).toArray();
			getSimilarityFunction().calculateSimilarities(medoidIndices, medoidIndices, sims);
			for (int i = 0; i < clusters.length; i++) {
				medoidToCluster.put(clustersMedoids[i], IntArrayList.wrap(new int[] { clustersMedoids[i] }));
				assignedObjects.add(clustersMedoids[i]);
				similarity = similarity.plus(sims.get(clustersMedoids[i], clustersMedoids[i]));
			}

			getSimilarityFunction().calculateSimilarities(nonMedoidIndicesArray, medoidIndices, sims);

			final int[] maxIdxs = sims.whichMaxForFirstObject(nonMedoidIndicesArray, medoidIndices);
			final ISimilarityValue[] maxSims = sims.get(nonMedoidIndicesArray, maxIdxs);
			int p = 0;
			for (int objectIdx : nonMedoidIndicesArray) {
				int clusterIdx = maxIdxs[p];
				if (clusterIdx >= 0) {
					medoidToCluster.get(clusterIdx).add(objectIdx);
					similarity = similarity.plus(maxSims[p++]);
					assignedObjects.add(objectIdx);
				}
			}

			int i = 0;
			for (int medoidIdx : medoidToCluster.keySet()) {
				clusters[i++] = medoidToCluster.get(medoidIdx);
			}

			return similarity;
		} catch (SimilarityValuesException | NoComparableSimilarityValuesException | SimilarityCalculationException e) {
			throw new ClusterOperationException(e);
		}
	}

	/**
	 * Find the starting medoids, by selecting <tt>numberOfClusters</tt> random
	 * objects.
	 * 
	 * @param objects          the objects to choose medoids from.
	 * @param numberOfClusters the number of medoids wanted (number of clusters)
	 * @return
	 */
	private int[] setStartMedoids(final int[] objects, final int numberOfClusters, final int[] clustersMedoids,
			final Random random) {
		int chosenMedoids = 0;
		final IntArrayList choices = new IntArrayList(objects);

		while (chosenMedoids < numberOfClusters) {
			final int index = random.nextInt(choices.size());
			clustersMedoids[chosenMedoids] = choices.removeInt(index);
			chosenMedoids++;
		}
		return choices.toIntArray();
	}

	@Override
	public String toString() {
		return "PAMK";
	}
}
