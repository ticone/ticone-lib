/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Random;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototype;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.util.Utility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since May 5, 2017
 *
 */
public class ShuffleClusteringRandomlyWithSameNumberClusters extends AbstractShuffle implements IShuffleClustering {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5659698381144009011L;

	@Override
	public ObjectType<IClusterObjectMapping> supportedObjectType() {
		return ObjectType.CLUSTERING;
	}

	@Override
	public ShuffleClusteringRandomlyWithSameNumberClusters copy() {
		return new ShuffleClusteringRandomlyWithSameNumberClusters();
	}

	@Override
	public ShuffleClusteringRandomlyWithSameNumberClusters newInstance() {
		return new ShuffleClusteringRandomlyWithSameNumberClusters();
	}

	@Override
	public String getName() {
		return "Create Random Clustering";
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResult doShuffle(IObjectWithFeatures object, long seed) throws ShuffleException,
			ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException {
		final IClusterObjectMapping clustering = (IClusterObjectMapping) object;
		try {
			final Random random = new Random(seed);
			final int size = clustering.getClusters().size();
			final IClusterObjectMapping newClustering = clustering.newInstance(clustering.getAllObjects().asSet());

			final IClusterList shuffledClusters = new ClusterList();
			for (int i = 0; i < size; i++) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				final ICluster shuffledCluster = newClustering.addCluster(new MissingPrototype());
				// make sure that we keep all the clusters and return exactly
				// the same number of clusters as we got
				shuffledCluster.setKeep(true);
				shuffledClusters.add(shuffledCluster);
			}
			// randomly assign objects to clusters
			for (final ITimeSeriesObject o : clustering.getAllObjects()) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				final ICluster c = shuffledClusters.get(random.nextInt(shuffledClusters.size()));
				newClustering.addMapping(o, c, AbstractSimilarityValue.MAX);
			}
			return new BasicShuffleResult(clustering, newClustering);
		} catch (ClusterFactoryException | CreateClusterInstanceFactoryException | IncompatiblePrototypeException
				| DuplicateMappingForObjectException e) {
			throw new ShuffleException(e);
		}
	}

	@Override
	public boolean validateParameters() {
		return true;
	}
}
