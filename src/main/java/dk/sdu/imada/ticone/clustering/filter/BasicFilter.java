/**
 * 
 */
package dk.sdu.imada.ticone.clustering.filter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 26, 2017
 *
 */
public class BasicFilter<T extends IObjectWithFeatures> implements IFilter<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4398200143761654690L;

	private boolean isActive;
	private IArithmeticFeature<?> feature;
	private FILTER_OPERATOR filterOperator;
	private double filterInput;

	private transient Set<IFilterListener> listener = new HashSet<>();

	/**
	 * @param feature the feature to set
	 */
	@Override
	public void setFeature(final IArithmeticFeature<?> feature) {
		this.feature = feature;
	}

	/**
	 * @param filterOperator the filterOperator to set
	 */
	@Override
	public void setFilterOperator(final FILTER_OPERATOR filterOperator) {
		this.filterOperator = filterOperator;
	}

	/**
	 * @param filterInput the filterInput to set
	 */
	@Override
	public void setFilterInput(final double filterInput) {
		this.filterInput = filterInput;
	}

	@Override
	public boolean isActive() {
		return this.isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	@Override
	public void setActive(final boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the feature
	 */
	@Override
	public IArithmeticFeature<?> getFeature() {
		return this.feature;
	}

	/**
	 * @return the filterOperator
	 */
	@Override
	public FILTER_OPERATOR getFilterOperator() {
		return this.filterOperator;
	}

	/**
	 * @return the filterInput
	 */
	@Override
	public double getFilterInput() {
		return this.filterInput;
	}

	@Override
	public boolean check(final IFeatureStore featureStore, final T object) {
		try {
			if (!this.isActive())
				return true;

			final IFeatureValue<?> featureValue = featureStore.getFeatureValue(object, this.feature);
			int comp = Double.compare(featureValue.toNumber().doubleValue(), this.filterInput);
			switch (this.filterOperator) {
			case LESS:
				return comp < 0;
			case LESS_OR_EQUAL:
				return comp <= 0;
			case EQUAL:
				return comp == 0;
			case EQUAL_OR_GREATER:
				return comp >= 0;
			case GREATER:
				return comp > 0;
			case NOT_EQUAL:
				return comp != 0;
			}
			return false;
		} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException e) {
			return false;
		}
	}

	@Override
	public void addFilterListener(final IFilterListener l) {
		this.listener.add(l);
	}

	@Override
	public void removeFilterListener(final IFilterListener l) {
		this.listener.remove(l);
	}

	@Override
	public void fireFilterChanged() {
		final FilterEvent e = new FilterEvent(this);
		for (final IFilterListener l : this.listener)
			l.filterChanged(e);
	}

	private void readObject(ObjectInputStream s) throws ClassNotFoundException, IOException {
		s.defaultReadObject();
		this.listener = new HashSet<>();
	}
}
