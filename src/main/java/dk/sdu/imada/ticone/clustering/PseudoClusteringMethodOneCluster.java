package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ITimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;

/**
 * This class puts all elements into one cluster.
 *
 * @author Christian Wiwie
 * @see IClusteringMethod
 * @see ITimeSeriesObject
 */
public class PseudoClusteringMethodOneCluster extends AbstractClusteringMethod<ClusterObjectMapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6292310919048247959L;

	private boolean initialized = false;

	/**
	 * The initialize function for this class, that should be called before using
	 * the interface.
	 *
	 * @param discretizePatternFunc
	 * @param similarityFunction
	 * @param numberOfTimeSeriesDataPoints
	 * @param pairwiseSimilarityThreshold
	 * @throws IncompatibleSimilarityFunctionException
	 * @see IDiscretizeTimeSeries
	 * @see ITimeSeriesSimilarityFunction
	 * @see ITimeSeriesObject
	 * @see IClusterObjectMapping
	 */
	public PseudoClusteringMethodOneCluster(final IPrototypeBuilder prototypeFactory,
			final ISimilarityFunction similarityFunction, final int numberOfTimeSeriesDataPoints,
			final double pairwiseSimilarityThreshold) throws IncompatibleSimilarityFunctionException {
		super(similarityFunction, prototypeFactory);
		if (numberOfTimeSeriesDataPoints < 1 || pairwiseSimilarityThreshold > 1 || pairwiseSimilarityThreshold < 0) {
			logger.error("Incorrect input.");
			return;
		}
		this.initialized = true;
	}

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	PseudoClusteringMethodOneCluster(PseudoClusteringMethodOneCluster other) {
		super(other);
		this.initialized = other.initialized;
	}

	@Override
	public IClusteringMethod<ClusterObjectMapping> copy() {
		return new PseudoClusteringMethodOneCluster(this);
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return true;
	}

	/**
	 * This function starts the clustering of the time series data with TransClust,
	 * and returns a mapping of the found clustering.
	 * 
	 * @param timeSeriesDatas  the list of all time series objects
	 * @param numberOfClusters the specified number of clusters wanted
	 * @return returns the Pattern Object Mapping of the clusters.
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 */
	@Override
	protected ClusterObjectMapping doFindClusters(final ITimeSeriesObjects timeSeriesDatas, final int numberOfClusters,
			final long seed) throws ClusterOperationException, InterruptedException {
		if (!this.initialized || timeSeriesDatas == null || numberOfClusters < 1) {
			logger.error("Wrong input or not initialized.");
			return null;
		}
		try {
			return this.oneCluster(timeSeriesDatas.asList());
		} catch (SimilarityCalculationException | IncompatiblePrototypeComponentException
				| CreatePrototypeInstanceFactoryException | PrototypeFactoryException | MissingPrototypeException
				| ClusterFactoryException | CreateClusterInstanceFactoryException | SimilarityValuesException
				| IncompatibleSimilarityValueException | DuplicateMappingForObjectException e) {
			throw new ClusterOperationException(e);
		}
	}

	/**
	 *
	 * @param timeSeriesDatas
	 * @return
	 * @throws SimilarityCalculationException
	 * @throws IncompatiblePrototypeComponentException
	 * @throws PrototypeFactoryException
	 * @throws MissingPrototypeException
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws IncompatibleSimilarityValueException
	 * @throws SimilarityValuesException
	 * @throws DuplicateMappingForObjectException
	 */
	private ClusterObjectMapping oneCluster(final ITimeSeriesObjectList timeSeriesDatas)
			throws SimilarityCalculationException, IncompatiblePrototypeComponentException,
			CreatePrototypeInstanceFactoryException, PrototypeFactoryException, MissingPrototypeException,
			InterruptedException, ClusterFactoryException, CreateClusterInstanceFactoryException,
			SimilarityValuesException, IncompatibleSimilarityValueException, DuplicateMappingForObjectException {
		final ClusterObjectMapping patternObjectMapping = new ClusterObjectMapping(timeSeriesDatas.asSet().copy(),
				prototypeBuilder);
		final ICluster pattern = patternObjectMapping.addCluster(timeSeriesDatas, similarityFunction);
		for (int i = 0; i < timeSeriesDatas.size(); i++) {
			final ISimilarityValue similarity = this.getSimilarityFunction().missingValuePlaceholder();
			patternObjectMapping.addMapping(timeSeriesDatas.get(i), pattern, similarity);
		}
		return patternObjectMapping;
	}
}
