/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.wiwie.wiutils.utils.Pair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.network.TiconeEdgeType;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl.TiconeNetworkNodeImpl;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleResultWithMapping;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototype;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.util.Utility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since May 10, 2017
 *
 */
public class ShuffleClusteringByShufflingClustersDegreePreserving extends AbstractShuffle
		implements IShuffleClustering {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1630280216065166164L;

	protected ITiconeNetwork network;

	@Override
	public ObjectType<IClusterObjectMapping> supportedObjectType() {
		return ObjectType.CLUSTERING;
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.network == null)
			throw new ShuffleNotInitializedException("network");
		return true;
	}

	@Override
	public ShuffleClusteringByShufflingClustersDegreePreserving copy() {
		final ShuffleClusteringByShufflingClustersDegreePreserving s = new ShuffleClusteringByShufflingClustersDegreePreserving();
		s.network = this.network;
		return s;
	}

	@Override
	public ShuffleClusteringByShufflingClustersDegreePreserving newInstance() {
		return new ShuffleClusteringByShufflingClustersDegreePreserving();
	}

	/**
	 * @param network the network to set
	 */
	public void setNetwork(final ITiconeNetwork network) {
		this.network = network;
		if (this.network instanceof TiconeNetworkImpl) {
			((TiconeNetworkImpl) this.network).initNodeDegreesUndirected();
		}
	}

	@Override
	public String getName() {
		return "Shuffle Clusters (Cluster Degree Preserving)";
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return type == ObjectType.CLUSTER;
	}

	@Override
	public IShuffleResultWithMapping doShuffle(final IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final IClusterObjectMapping clustering = this.supportedObjectType().getBaseType().cast(object);
		try {
			final IClusterSet clusters = clustering.getClusters().copy();

			final List<Pair<ICluster, Integer>> clusterDegrees = new ArrayList<>();
			final List<Pair<ITimeSeriesObject, Integer>> nodeDegrees = new LinkedList<>();
			// final int[] clusterDegreeLimits = new int[clusterList.size()];
			// current total degrees per cluster
			for (final ICluster cluster : clusters) {
				int clusterDegree = 0;
				for (final ITimeSeriesObject o : cluster.getObjects()) {
					if (!Utility.getProgress().getStatus())
						throw new InterruptedException();
					int degree;
					final ITiconeNetworkNode node = this.network.getNode(o.getName());
					if (this.network instanceof TiconeNetworkImpl)
						degree = ((TiconeNetworkNodeImpl) node).getUndirectedDegree();
					else
						degree = this.network.getAdjacentEdgeCount(node, TiconeEdgeType.ANY);
					clusterDegree += degree;
					nodeDegrees.add(Pair.getPair(o, degree));
				}
				// clusterDegreeLimits[c] = clusterDegree;
				clusterDegrees.add(Pair.getPair(cluster, clusterDegree));
			}

			// sort objects by degree
			Collections.sort(nodeDegrees, new Comparator<Pair<ITimeSeriesObject, Integer>>() {

				@Override
				public int compare(final Pair<ITimeSeriesObject, Integer> o1,
						final Pair<ITimeSeriesObject, Integer> o2) {
					return o2.getSecond().compareTo(o1.getSecond());
				}
			});

			final Map<ICluster, ICluster> shuffledClusters = new HashMap<>();
			final IShuffleMapping shuffleMapping = new BasicShuffleMapping();

			final IClusterObjectMapping shuffledClustering = clustering.newInstance(clustering.getAllObjects().asSet());
			for (final ICluster cluster : clustering.getClusters()) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				final ICluster shuffledCluster = shuffledClustering.addCluster(new MissingPrototype());
				shuffledClusters.put(cluster, shuffledCluster);
				shuffleMapping.put(cluster, shuffledCluster);
			}

			// iterate over objects in order of decreasing node degree
			for (final Pair<ITimeSeriesObject, Integer> objectAndDegree : nodeDegrees) {
				// this is to ensure, that we do not get the same clustering every
				// time;
				Collections.shuffle(clusterDegrees);
				// assign objects to clusters while aiming the same total degrees of
				// the clusters
				boolean objectAssigned = false;
				int maxClusterDegree = Integer.MIN_VALUE;
				Pair<ICluster, Integer> maxCluster = null;
				for (final Pair<ICluster, Integer> clusterAndDegree : clusterDegrees) {
					if (!Utility.getProgress().getStatus())
						throw new InterruptedException();
					// this is the total degree of the original cluster;
					// we aim for the same in the shuffled cluster;
					if (clusterAndDegree.getSecond() < objectAndDegree.getSecond()) {
						// use this guy as backup in case we dont find any cluster
						// where the object fits in
						if (clusterAndDegree.getSecond() > maxClusterDegree) {
							maxClusterDegree = clusterAndDegree.getSecond();
							maxCluster = clusterAndDegree;
						}
						continue;
					}

					final ICluster cluster = clusterAndDegree.getFirst();
					final ICluster shuffledCluster = shuffledClusters.get(cluster);

					shuffledClustering.addMapping(objectAndDegree.getFirst(), shuffledCluster,
							AbstractSimilarityValue.MAX);
					clusterAndDegree.setSecond(clusterAndDegree.getSecond() - objectAndDegree.getSecond());
					// make sure that we keep all the clusters and return exactly
					// the same number of clusters as we got
					shuffledCluster.setKeep(true);
					objectAssigned = true;
					break;
				}
				if (!objectAssigned) {
					// if (maxCluster == null)
					// throw new ShuffleException(String.format("The object %s does
					// not fit into any cluster",
					// objectAndDegree.getFirst().getName()));
					// assign the object to the cluster with the so far biggest
					// remaining degree
					shuffledClustering.addMapping(objectAndDegree.getFirst(),
							shuffledClusters.get(maxCluster.getFirst()), AbstractSimilarityValue.MAX);
					maxCluster.setSecond(maxCluster.getSecond() - objectAndDegree.getSecond());
				}
			}

			shuffledClustering.updatePrototypes();
			final IShuffleResultWithMapping result = new ShuffleResultWithMapping(clustering, shuffledClustering);
			result.setShuffleMapping(shuffleMapping);

			return result;
		} catch (CreatePrototypeInstanceFactoryException | PrototypeFactoryException | ClusterFactoryException
				| CreateClusterInstanceFactoryException | IncompatiblePrototypeException
				| DuplicateMappingForObjectException e) {
			throw new ShuffleException(e);
		}
	}
}
