package dk.sdu.imada.ticone.clustering.aggregate;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObject.NetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.util.AggregationException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Dec 23, 2018
 *
 */
public class AggregateClusterAllNodes implements IAggregateClusterIntoNetworkLocation {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8499588593620542052L;

	/**
	 * 
	 */
	public AggregateClusterAllNodes() {
		super();
	}

	private ITiconeNetwork<? extends ITiconeNetworkNode, ?> network;

	public AggregateClusterAllNodes(final ITiconeNetwork<? extends ITiconeNetworkNode, ?> network) {
		super();
		this.network = network;
	}

	@Override
	public AggregateClusterAllNodes copy() {
		return new AggregateClusterAllNodes(this.network);
	}

	/**
	 * @param network the network to set
	 */
	@Override
	public void setNetwork(final ITiconeNetwork<? extends ITiconeNetworkNode, ?> network) {
		this.network = network;
	}

	@Override
	public Collection<? extends INetworkMappedTimeSeriesObject> doAggregate(final ITimeSeriesObjects objects)
			throws AggregationException, InterruptedException {
		final Collection<INetworkMappedTimeSeriesObject> result = new HashSet<>();
		for (final ITimeSeriesObject o : objects) {
			if (o instanceof NetworkMappedTimeSeriesObject)
				result.add(((NetworkMappedTimeSeriesObject) o));
			else if (this.network.containsNode(o.getName()))
				result.add(o.mapToNetworkNode(network, this.network.getNode(o.getName())));
			else
				continue;
		}
		return result;
	}

	@Override
	public String toString() {
		return "All Cluster Nodes";
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof AggregateClusterAllNodes;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getClass());
	}
}
