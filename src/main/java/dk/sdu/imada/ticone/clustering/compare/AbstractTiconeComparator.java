/**
 * 
 */
package dk.sdu.imada.ticone.clustering.compare;

import java.util.Comparator;
import java.util.Objects;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IStatusMapping;
import dk.sdu.imada.ticone.util.IStatusMapping.IStatusComparator;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 26, 2017
 *
 */
public abstract class AbstractTiconeComparator implements Comparator<IObjectWithFeatures> {

	protected boolean decreasing, reverseComparator;

	protected IStatusMapping<? extends IObjectWithFeatures> statusMapping;

	protected IStatusComparator<IObjectWithFeatures> statusComparator;

	public AbstractTiconeComparator() {
		this(false);
	}

	public AbstractTiconeComparator(final boolean decreasing) {
		super();
		this.decreasing = decreasing;
	}

	/**
	 * @param statusMapping the statusMapping to set
	 */
	public AbstractTiconeComparator setStatusMapping(IStatusMapping<? extends IObjectWithFeatures> statusMapping) {
		this.statusMapping = Objects.requireNonNull(statusMapping);
		this.statusComparator = (IStatusComparator) this.statusMapping.comparator();
		this.statusComparator.setReverse(reverseComparator);
		return this;
	}

	/**
	 * @return the statusMapping
	 */
	public IStatusMapping<? extends IObjectWithFeatures> getStatusMapping() {
		return this.statusMapping;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null)
			return false;
		return this.getClass().equals(obj.getClass());
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	/**
	 * @param reverseComparator the reverseComparator to set
	 */
	public void setReverseComparator(final boolean reverseComparator) {
		this.reverseComparator = reverseComparator;
		if (this.statusComparator != null)
			this.statusComparator.setReverse(reverseComparator);
	}

	/**
	 * @return the decreasing
	 */
	public boolean isDecreasing() {
		return this.decreasing;
	}

	/**
	 * @param decreasing the decreasing to set
	 */
	public Comparator<? extends IObjectWithFeatures> setDecreasing(final boolean decreasing) {
		this.decreasing = decreasing;
		this.statusComparator.setDecreasing(decreasing);
		return this;
	}

	protected int compareStatus(IObjectWithFeatures o1, IObjectWithFeatures o2) {
		if (this.decreasing) {
			final IObjectWithFeatures tmp = o1;
			o1 = o2;
			o2 = tmp;
		}

		return this.statusComparator.compare(o1, o2);
	}

}