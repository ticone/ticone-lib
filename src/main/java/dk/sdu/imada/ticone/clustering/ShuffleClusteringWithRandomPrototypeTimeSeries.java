/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Random;

import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.permute.BasicShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototype;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.Utility;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public class ShuffleClusteringWithRandomPrototypeTimeSeries
		extends AbstractShuffleClusteringWithSimilarityReassignment {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1755871987744257117L;

	protected IPrototypeBuilder prototypeFactory;

	protected CreateRandomTimeSeries randomTimeSeries;

	protected boolean randomNumberClusters;

	public ShuffleClusteringWithRandomPrototypeTimeSeries(final ShuffleClusteringWithRandomPrototypeTimeSeries s) {
		super(s);
		this.prototypeFactory = s.prototypeFactory.copy();
		this.randomTimeSeries = s.randomTimeSeries.copy();
		this.randomNumberClusters = s.randomNumberClusters;
	}

	public ShuffleClusteringWithRandomPrototypeTimeSeries(final ISimilarityFunction simFunc,
			final IPrototypeBuilder prototypeFactory, final CreateRandomTimeSeries randomPrototypes)
			throws IncompatibleSimilarityFunctionException {
		super(simFunc);
		this.prototypeFactory = prototypeFactory;
		this.randomTimeSeries = randomPrototypes;
	}

	@Override
	public ObjectType<IClusterObjectMapping> supportedObjectType() {
		return ObjectType.CLUSTERING;
	}

	@Override
	public ShuffleClusteringWithRandomPrototypeTimeSeries copy() {
		return new ShuffleClusteringWithRandomPrototypeTimeSeries(this);
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.randomTimeSeries == null)
			throw new ShuffleNotInitializedException("randomPrototypes");
		return super.validateInitialized();
	}

	@Override
	public ShuffleClusteringWithRandomPrototypeTimeSeries newInstance() {
		return new ShuffleClusteringWithRandomPrototypeTimeSeries(this);
	}

	@Override
	public String getName() {
		return this.randomTimeSeries.getName();
	}

	@Override
	public boolean producesShuffleMappingFor(final ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResult doShuffle(final IObjectWithFeatures object, final long seed)
			throws ShuffleException, ShuffleNotInitializedException, InterruptedException, IncompatibleShuffleException,
			IncompatibleMappingAndObjectTypeException {
		final IClusterObjectMapping clustering = supportedObjectType().getBaseType().cast(object);
		final IPrototypeBuilder prototypeFactory = this.prototypeFactory.copy();
		final IClusterObjectMapping shuffledClustering = clustering.newInstance(clustering.getAllObjects().asSet());

		if (clustering.getClusters().isEmpty())
			return new BasicShuffleResult(clustering, shuffledClustering);

		try {
			final Random random = new Random(seed);
			final IClusterList originalClusters = clustering.getClusters().copy().asList();
			int maxNumberClusters = clustering.getAllObjects().size();
			int numberClusters;
			if (!randomNumberClusters)
				numberClusters = originalClusters.size();
			else {
				numberClusters = (int) Math
						.abs(Math.round(random.nextGaussian() * originalClusters.size() + originalClusters.size()));
				numberClusters = Math.max(Math.min(maxNumberClusters, numberClusters), 1);
			}

			logger.debug("" + numberClusters);
			for (int c = 0; c < numberClusters; c++) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				IPrototype shuffledPrototype;
				try {
					final ITimeSeries timeSeries = PrototypeComponentType.TIME_SERIES
							.getComponent(originalClusters.get(c).getPrototype()).getTimeSeries();

					final IShuffleResult shuffleResult = this.randomTimeSeries.shuffle(timeSeries, random.nextLong());

					final ITimeSeries shuffledTS = timeSeries.getObjectType().getBaseType()
							.cast(shuffleResult.getShuffled());
					shuffledPrototype = prototypeFactory.copy(originalClusters.get(0).getPrototype(),
							PrototypeComponentType.TIME_SERIES.getComponentFactory(prototypeFactory)
									.setTimeSeries(shuffledTS).build());
				} catch (MissingPrototypeException | MissingPrototypeFactoryException e) {
					shuffledPrototype = new MissingPrototype();
				}
				final ICluster shuffledCluster = shuffledClustering.addCluster(shuffledPrototype);

				// make sure that we keep all the clusters and return exactly
				// the
				// same number of clusters as we got
				shuffledCluster.setKeep(true);
			}
			shuffledClustering.removeDuplicatePrototypes(true);
			// we do not modify the objects, just the clusters
			// recluster with new prototypes
			ITimeSeriesObjectList copy = clustering.getAllObjects().copy();
			shuffledClustering.addObjectsToMostSimilarCluster(copy, this.similarityFunction);
			shuffledClustering.removeEmptyClusters(true);
			for (ICluster c : shuffledClustering.getClusters()) {
				c.updatePrototype(prototypeFactory.setObjects(c.getObjects()).build());
			}
			return new BasicShuffleResult(clustering, shuffledClustering);
		} catch (SimilarityCalculationException | IncompatiblePrototypeComponentException | PrototypeFactoryException
				| PrototypeComponentFactoryException | SimilarityValuesException | NoComparableSimilarityValuesException
				| ClusterFactoryException | CreateInstanceFactoryException | IncompatibleSimilarityValueException
				| IncompatiblePrototypeException e) {
			throw new ShuffleException(e);
		}
	}

	/**
	 * @param randomNumberClusters the randomNumberClusters to set
	 */
	public void setRandomNumberClusters(boolean randomNumberClusters) {
		this.randomNumberClusters = randomNumberClusters;
	}

	public void setPreserveTimepoints(boolean preserveTimepoints) {
		this.randomTimeSeries.setPreserveTimepoints(preserveTimepoints);
	}

	/**
	 * @return the randomTimeSeries
	 */
	public CreateRandomTimeSeries getCreateRandomTimeSeries() {
		return this.randomTimeSeries;
	}

	@Override
	public String toString() {
		return String.format("%s (randomNumberClusters=%b)", super.toString(), this.randomNumberClusters);
	}
}
