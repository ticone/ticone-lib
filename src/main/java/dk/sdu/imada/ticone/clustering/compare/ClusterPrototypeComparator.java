package dk.sdu.imada.ticone.clustering.compare;

import java.util.HashMap;
import java.util.Map;

import dk.sdu.imada.ticone.clustering.ClusterUtility;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.feature.FeatureComparisonException;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;

public class ClusterPrototypeComparator extends AbstractTiconeComparator {

	protected Map<ICluster, Integer> patternPosition;

	public ClusterPrototypeComparator(final IClusterList patterns, final ISimilarityFunction similarity)
			throws ClusterCompareException, InterruptedException {
		super();

		IClusterList sortedClusters;
		try {
			sortedClusters = ClusterUtility.getPatternSingleLinkage(patterns, similarity);
		} catch (final SimilarityCalculationException | SimilarityValuesException
				| IncompatibleSimilarityValueException e) {
			throw new ClusterCompareException(e);
		} catch (final Exception e) {
			e.printStackTrace();
			throw e;
		}

		// store pattern -> position mapping
		this.patternPosition = new HashMap<>();
		for (int i = 0; i < sortedClusters.size(); i++) {
			this.patternPosition.put(sortedClusters.get(i), i);
		}
	}

	@Override
	public int compare(IObjectWithFeatures o1, IObjectWithFeatures o2) {
		if (!(o1 instanceof ICluster && o2 instanceof ICluster))
			throw new FeatureComparisonException("This comparator can only compare ICluster objects");
		final int statusComp = super.compareStatus(o1, o2);
		if (statusComp != 0)
			return statusComp;

		if (this.decreasing && this.reverseComparator) {
			final ICluster tmp = (ICluster) o1;
			o1 = o2;
			o2 = tmp;
		}

		final double val1 = this.patternPosition.get(o1);
		final double val2 = this.patternPosition.get(o2);
		return Double.compare(val1, val2);
	};

	@Override
	public String toString() {
		return "Prototype";
	}
}