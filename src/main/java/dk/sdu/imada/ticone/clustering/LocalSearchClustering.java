package dk.sdu.imada.ticone.clustering;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping.ObjectPartition;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.Utility;

/**
 * Created by christian on 21-10-15.
 */
public class LocalSearchClustering {

	public static final double DEFAULT_EPSILON = 0.000001;

	public static final int DEFAULT_MAX_ITERATIONS = 100;

	private Logger logger;
	private final double epsilon;
	private final int maxIterations;

	/**
	 * 
	 */
	public LocalSearchClustering() {
		this(DEFAULT_EPSILON, DEFAULT_MAX_ITERATIONS);
	}

	/**
	 * 
	 */
	public LocalSearchClustering(final int maxIterations) {
		this(DEFAULT_EPSILON, maxIterations);
	}

	/**
	 * 
	 */
	public LocalSearchClustering(final double epsilon, final int maxIterations) {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.epsilon = epsilon;
		this.maxIterations = maxIterations;
	}

	/**
	 * Keeps doing iterations of timeSeriesClusteringWithOverrepresentedPatterns
	 * until it is converged.
	 * 
	 * @param clusteringProcess
	 * @throws TooFewObjectsClusteringException
	 * @throws ClusterOperationException
	 * @throws SimilarityCalculationException
	 */
	public void computeIterationsUntilConvergence(final IClusteringProcess<ClusterObjectMapping, ?> clusteringProcess)
			throws InterruptedException, TooFewObjectsClusteringException, ClusterOperationException,
			SimilarityCalculationException {
		ISimilarityValue currentSimilarity = clusteringProcess.getCurrentIteration() > 0
				? clusteringProcess.getAverageObjectClusterSimilarity()
				: clusteringProcess.getSimilarityFunction().missingValuePlaceholder();
		int currentNumberAssignedObjects = 0;

		int iteration = 0;

		IClusterHistory<ClusterObjectMapping> history = clusteringProcess.getHistory();
		ObjectPartition newPartition;
		final Set<ObjectPartition> previousPartitions = new HashSet<>();
		while (history != null) {
			previousPartitions.add(history.getClusterObjectMapping().asObjectPartition());
			history = history.getParent();
		}

		boolean reachedMaxIterations = iteration >= maxIterations;
		boolean foundDuplicatePartition = false;
		double similarityDifference = epsilon + 1;
		ISimilarityValue oldSimilarity = AbstractSimilarityValue.MIN;
		while (!reachedMaxIterations && similarityDifference >= epsilon && !foundDuplicatePartition) {
			if (clusteringProcess.getHistory() != null)
				clusteringProcess.getProgress().updateProgress(Double.NaN,
						String.format("Iterations until convergence: Iteration %d",
								clusteringProcess.getHistory().getIterationNumber() + 1),
						null);
			if (!Utility.getProgress().getStatus()) {
				throw new InterruptedException();
			}
			clusteringProcess.doIteration();
			oldSimilarity = currentSimilarity;
			currentSimilarity = clusteringProcess.getAverageObjectClusterSimilarity();
			currentNumberAssignedObjects = clusteringProcess.getLatestClustering().getAssignedObjects().size();
			newPartition = clusteringProcess.getLatestClustering().asObjectPartition();

			foundDuplicatePartition = previousPartitions.contains(newPartition);
			previousPartitions.add(newPartition);
			reachedMaxIterations = ++iteration >= maxIterations;
			similarityDifference = currentSimilarity.get() - oldSimilarity.get();
			logger.info("Similarity Difference: " + similarityDifference);
		}

		if (foundDuplicatePartition) {
			clusteringProcess.resetToIteration(clusteringProcess.getCurrentIteration() - 1);
			logger.info("Cycle detected: Stopping");
		} else if (reachedMaxIterations)
			logger.info("Reached max iterations: Stopping");
		else if (!(similarityDifference >= epsilon)) {
			clusteringProcess.resetToIteration(clusteringProcess.getCurrentIteration() - 1);
			logger.info("Similarity difference (|{}-{}|={}) smaller than epsilon ({}): Stopping", oldSimilarity.get(),
					currentSimilarity.get(), similarityDifference, epsilon);
		}

	}
}
