package dk.sdu.imada.ticone.clustering.aggregate;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeries;

/**
 * This class is used to refine the patterns, by taking the mean of each time
 * point of all assigned time series data
 *
 * @author Christian Wiwie
 * @author Christian Nørskov
 */
public class AggregateClusterMeanTimeSeries implements IAggregateClusterIntoTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2910092336531658670L;

	@Override
	public AggregateClusterMeanTimeSeries copy() {
		return new AggregateClusterMeanTimeSeries();
	}

	/**
	 * Refines the given pattern using the given time series objects, by taking the
	 * mean for each time point.
	 * 
	 * @param timeSeriesData The time series objects used to refine the pattern
	 * @return returns the refined double array
	 */
	@Override
	public TimeSeries doAggregate(final ITimeSeriesObjects objects) {
		final int timePoints = objects.iterator().next().getPreprocessedTimeSeriesList()[0].getNumberTimePoints();

		int count = 0;

		final double[] refinedPattern = new double[timePoints];
		for (final ITimeSeriesObject tsd : objects) {
			for (int j = 0; j < tsd.getPreprocessedTimeSeriesList().length; j++) {
				final ITimeSeries tempPattern = tsd.getPreprocessedTimeSeriesList()[j];
				for (int i = 0; i < tempPattern.getNumberTimePoints(); i++) {
					refinedPattern[i] += tempPattern.get(i);
				}
				count++;
			}
		}
		for (int i = 0; i < refinedPattern.length; i++) {
			refinedPattern[i] /= (count);
		}
		return new TimeSeries(refinedPattern);
	}

	@Override
	public String toString() {
		return "Mean Time Series";
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof AggregateClusterMeanTimeSeries;
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}
}
