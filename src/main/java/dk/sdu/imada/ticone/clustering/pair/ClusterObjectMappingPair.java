/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair.ClusterPairsFactory;
import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IObjectProviderManager;
import dk.sdu.imada.ticone.util.ObjectProviderManager;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public class ClusterObjectMappingPair implements IClusterObjectMappingPair {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4527469248255530334L;

	public static IClusterPairSet getAllClusterPairs(final IClusterObjectMapping clustering1,
			final IClusterObjectMapping clustering2) {
		return new ClusterPairsFactory().createSet(clustering1.getClusters().asList(),
				clustering2.getClusters().asList());
	}

	protected IClusterObjectMapping clustering1, clustering2;

	protected IClusterPairSet clusterPairs;

	protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

	protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);

	/**
	 * 
	 */
	public ClusterObjectMappingPair(final IClusterObjectMapping clustering1, final IClusterObjectMapping clustering2) {
		super();
		this.clustering1 = clustering1;
		this.clustering2 = clustering2;
		this.clusterPairs = getAllClusterPairs(clustering1, clustering2);
	}

	@Override
	public IObjectProvider copy() throws InterruptedException {
		return new ClusterObjectMappingPair(clustering1.copy(), clustering2.copy());
	}

	@Override
	public String toString() {
		return this.clustering1.getClusters().toString() + ", " + this.clustering2.getClusters().toString();
	}

	@Override
	public IClusterPairs getClusterPairs() {
		return this.clusterPairs;
	}

	@Override
	public IClusterObjectMapping getClustering1() {
		return this.clustering1;
	}

	@Override
	public IClusterObjectMapping getClustering2() {
		return this.clustering2;
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}
}
