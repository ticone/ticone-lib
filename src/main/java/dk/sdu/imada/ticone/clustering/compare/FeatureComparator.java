package dk.sdu.imada.ticone.clustering.compare;

import java.util.Objects;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;

public class FeatureComparator<N extends Comparable<N>> extends AbstractTiconeComparator {

	protected IFeature<N> feature;
	protected IFeatureStore store;

	public FeatureComparator(final IFeature<N> feature, final IFeatureStore store) {
		super();
		this.feature = feature;
		this.store = store;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!super.equals(obj))
			return false;

		final FeatureComparator other = (FeatureComparator) obj;

		return Objects.equals(this.feature, other.feature);
	}

	@Override
	public int hashCode() {
		return super.hashCode() + (this.feature != null ? this.feature.hashCode() : 0);
	}

	/**
	 * @return the store
	 */
	public IFeatureStore getStore() {
		return this.store;
	}

	/**
	 * @return the feature
	 */
	public IFeature<N> getFeature() {
		return this.feature;
	}

	@Override
	public int compare(IObjectWithFeatures o1, IObjectWithFeatures o2) {
		final int statusComp = super.compareStatus(o1, o2);
		if (statusComp != 0)
			return statusComp;

		if (this.decreasing && this.reverseComparator) {
			final IObjectWithFeatures tmp = o1;
			o1 = o2;
			o2 = tmp;
		}

		boolean present1 = this.store.getFeaturesFor(o1).contains(this.feature);
		boolean present2 = this.store.getFeaturesFor(o2).contains(this.feature);
		if (!present1)
			if (!present2)
				return 0;
			else
				return -1;
		else if (!present2)
			return 1;

		N v1 = this.store.getFeatureValue(o1, this.feature).getValue();
		N v2 = this.store.getFeatureValue(o2, this.feature).getValue();

		if (v1 == null)
			if (v2 == null)
				return 0;
			else
				return -1;
		else if (v2 == null)
			return 1;
		return v1.compareTo(v2);
	}

	@Override
	public String toString() {
		return this.feature.getName();
	}
}