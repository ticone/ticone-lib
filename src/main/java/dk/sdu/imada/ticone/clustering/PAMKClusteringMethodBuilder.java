/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 28, 2019
 *
 */
public class PAMKClusteringMethodBuilder extends AbstractClusteringMethodBuilder<PAMKClusteringMethod> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3394321983356355187L;

	public static final int DEFAULT_SWAPPING_ITERATIONS = 10;

	public static final int DEFAULT_NSTART = 30;

	public static final double DEFAULT_SWAP_PROBABILITY = -1;

	private int swapIterations, nstart;
	private double swapProbability;

	public PAMKClusteringMethodBuilder() {
		super();
		this.swapIterations = DEFAULT_SWAPPING_ITERATIONS;
		this.nstart = DEFAULT_NSTART;
		this.swapProbability = DEFAULT_SWAP_PROBABILITY;
	}

	public PAMKClusteringMethodBuilder(PAMKClusteringMethodBuilder other) {
		super(other);
		this.swapIterations = other.swapIterations;
		this.nstart = other.nstart;
		this.swapProbability = other.swapProbability;
	}

	@Override
	public PAMKClusteringMethodBuilder copy() {
		return new PAMKClusteringMethodBuilder(this);
	}

	@Override
	public PAMKClusteringMethod copy(PAMKClusteringMethod clusteringMethod) {
		return new PAMKClusteringMethod(clusteringMethod);
	}

	@Override
	public PAMKClusteringMethod doBuild() throws ClusteringMethodFactoryException,
			CreateClusteringMethodInstanceFactoryException, InterruptedException {
		try {
			return new PAMKClusteringMethod(similarityFunction, swapIterations, nstart, swapProbability,
					prototypeBuilder);
		} catch (IncompatibleSimilarityFunctionException e) {
			throw new CreateClusteringMethodInstanceFactoryException(e);
		}
	}

	@Override
	protected void reset() {
	}

	/**
	 * @return the maxIterations
	 */
	public int getSwapIterations() {
		return this.swapIterations;
	}

	/**
	 * @param swapIterations the maxIterations to set
	 */
	public PAMKClusteringMethodBuilder setSwapIterations(int swapIterations) {
		this.swapIterations = swapIterations;
		return this;
	}

	/**
	 * @return the nstart
	 */
	public int getNstart() {
		return this.nstart;
	}

	/**
	 * @return the numberSwaps
	 */
	public double getSwapProbability() {
		return this.swapProbability;
	}

	/**
	 * @param nstart the nstart to set
	 */
	public PAMKClusteringMethodBuilder setNstart(int nstart) {
		this.nstart = nstart;
		return this;
	}

	/**
	 * @param swapProbability the swapProbability to set
	 */
	public PAMKClusteringMethodBuilder setSwapProbability(double swapProbability) {
		this.swapProbability = swapProbability;
		return this;
	}
}
