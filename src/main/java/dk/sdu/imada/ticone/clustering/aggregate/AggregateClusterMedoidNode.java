package dk.sdu.imada.ticone.clustering.aggregate;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeriesObject.NetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.network.IConnectedComponent;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.network.TiconeNetwork.TiconeNetworkNode;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.INetworkBasedSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValues;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.AggregationException;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 7, 2018
 *
 */
public class AggregateClusterMedoidNode implements IAggregateClusterIntoNetworkLocation {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8715230649796357711L;

	private ITiconeNetwork<? extends ITiconeNetworkNode, ?> network;

	private INetworkBasedSimilarityFunction similarityFunction;
	private ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities;
	private Int2ObjectMap<ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>> sims;

	/**
	 * 
	 */
	public AggregateClusterMedoidNode() {
		super();
	}

	public AggregateClusterMedoidNode(final ITiconeNetwork<? extends ITiconeNetworkNode, ?> network) {
		super();
		this.network = network;
	}

	public AggregateClusterMedoidNode(final ITiconeNetwork<? extends ITiconeNetworkNode, ?> network,
			INetworkBasedSimilarityFunction similarityFunction) throws IncompatibleSimilarityFunctionException {
		this(network);
		this.setSimilarityFunction(Objects.requireNonNull(similarityFunction));
	}

	AggregateClusterMedoidNode(final AggregateClusterMedoidNode other) {
		super();
		this.network = other.network;
		if (other.similarityFunction != null)
			this.similarityFunction = (INetworkBasedSimilarityFunction) other.similarityFunction.copy();
	}

	@Override
	public AggregateClusterMedoidNode copy() {
		return new AggregateClusterMedoidNode(this);
	}

	protected boolean isValidSimilarityFunction(final ISimilarityFunction similarityFunction) {
		return similarityFunction.providesValuesForObjectTypes().contains(ObjectType.OBJECT_PAIR)
				&& similarityFunction instanceof INetworkBasedSimilarityFunction;
	}

	protected void validateSimilarityFunction(final ISimilarityFunction similarityFunction)
			throws IncompatibleSimilarityFunctionException {
		if (!isValidSimilarityFunction(similarityFunction))
			throw new IncompatibleSimilarityFunctionException();
	}

	/**
	 * @param network the network to set
	 */
	@Override
	public void setNetwork(final ITiconeNetwork<? extends ITiconeNetworkNode, ?> network) {
		this.network = network;
	}

	public void setSimilarityFunction(final ISimilarityFunction similarityFunction)
			throws IncompatibleSimilarityFunctionException {
		this.validateSimilarityFunction(similarityFunction);
		this.similarityFunction = (INetworkBasedSimilarityFunction) similarityFunction;
	}

	/**
	 * @return the similarityFunction
	 */
	public INetworkBasedSimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	public ISimilarityValues<?, ?, ?> getSimilarityValues() {
		return this.similarities;
	}

	/**
	 * @param similarities the similarities to set
	 * @throws IncompatibleSimilarityFunctionException
	 */
	public void setSimilarities(
			ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities)
			throws IncompatibleSimilarityFunctionException {
		this.validateSimilarityFunction(similarities.getSimilarityFunction());
		this.similarityFunction = (INetworkBasedSimilarityFunction) similarities.getSimilarityFunction();
		this.similarities = similarities;
		try {
			this.sims = this.similarities.partitionByFirstObject();
		} catch (InterruptedException | SimilarityValuesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the similarities
	 */
	public ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> getSimilarities() {
		return this.similarities;
	}

	@Override
	public Collection<? extends INetworkMappedTimeSeriesObject> doAggregate(final ITimeSeriesObjects objects)
			throws AggregationException, InterruptedException {
		try {
			// consider only largest subset of objects in the same connected component
			Map<IConnectedComponent, ITimeSeriesObjects> componentToObjects = new HashMap<>();
			for (final ITimeSeriesObject o : objects) {
				IConnectedComponent comp;
				if (o instanceof NetworkMappedTimeSeriesObject
						&& ((INetworkMappedTimeSeriesObject) o).isMappedTo(this.network))
					comp = ((TiconeNetworkNode) ((NetworkMappedTimeSeriesObject) o).getNode(this.network))
							.getConnectedComponent();
				else if (this.network.containsNode(o.getName()))
					comp = this.network.getNode(o.getName()).getConnectedComponent();
				else
					continue;
				componentToObjects.putIfAbsent(comp, new TimeSeriesObjectList());
				componentToObjects.get(comp).add(o);
			}

			IConnectedComponent largestComponent = null;
			int largestCount = 0;
			for (final Entry<IConnectedComponent, ITimeSeriesObjects> e : componentToObjects.entrySet())
				if (largestComponent == null || e.getValue().size() > largestCount) {
					largestComponent = e.getKey();
					largestCount = e.getValue().size();
				}

			if (largestComponent == null)
				throw new AggregationException("No objects were found in network");

			final ITimeSeriesObjects consideredObjects = componentToObjects.get(largestComponent);
			componentToObjects = null;

			ITimeSeriesObject medoidObject = null;
			if (consideredObjects.size() == 1) {
				medoidObject = consideredObjects.iterator().next();
			} else {
				final Int2ObjectMap<ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>> sims;
				final int[] objectIndices;
				if (similarities != null) {
					if (this.sims == null)
						this.sims = this.similarities.partitionByFirstObject();

					objectIndices = objects.stream().mapToInt(o -> {
						try {
							int object1Index = this.similarities.getObject1Index(o);
							return object1Index;
						} catch (Exception e) {
							throw e;
						}
					}).toArray();

					this.getSimilarityFunction().calculateSimilarities(objectIndices, objectIndices, similarities);
					sims = this.sims;
				} else {
					final ITimeSeriesObject[] objectArray = consideredObjects.toArray();
					final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = this
							.getSimilarityFunction().calculateSimilarities(objectArray, objectArray,
									new ObjectPairsFactory(), ObjectType.OBJECT_PAIR);
					sims = similarities.partitionByFirstObject();
					objectIndices = similarities.firstObjectIndices();
				}

				ISimilarityValue maxSim = AbstractSimilarityValue.MIN;
				for (int oIdx : objectIndices) {
					try {
						final ISimilarityValue sum = sims.get(oIdx).filter((o1, o2, s) -> {
							try {
								return Double.isFinite(s.get());
							} catch (SimilarityCalculationException e1) {
								return false;
							}
						}).sum();
						if (maxSim.lessThan(sum)) {
							medoidObject = sims.get(oIdx).getObject1(0);
							maxSim = sum;
						}
					} catch (final SimilarityCalculationException e1) {
					}
				}
			}

			if (medoidObject instanceof NetworkMappedTimeSeriesObject)
				return Arrays.asList(((NetworkMappedTimeSeriesObject) medoidObject));
			return Arrays.asList(medoidObject.mapToNetworkNode(network, this.network.getNode(medoidObject.getName())));
		} catch (SimilarityCalculationException | SimilarityValuesException | IncompatibleSimilarityValueException e) {
			throw new AggregationException(e);
		}
	}

	@Override
	public String toString() {
		return "Medoid Cluster Node";
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof AggregateClusterMedoidNode
				&& Objects.equals(this.similarityFunction, ((AggregateClusterMedoidNode) obj).similarityFunction);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getClass(), this.similarityFunction);
	}
}
