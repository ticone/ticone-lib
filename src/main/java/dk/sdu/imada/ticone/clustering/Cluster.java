/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IObjectProviderManager;
import dk.sdu.imada.ticone.util.ObjectProviderManager;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 11, 2018
 *
 */
public class Cluster implements ICluster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9039534188500320231L;

	protected IClusterObjectMapping clustering;
	// unfortunately needed additionally to above clustering, because the clustering
	// cannot be deserialized in time when it is needed in hashCode() as it depends
	// on clusters being deserialized first, including this one.
	protected Long clusteringId;
	protected IPrototype prototype;
	protected boolean keep = false;
	// this number increases throughout all clusterings
	protected long internalClusterNumber = -1;
	protected int clusterNumber = -1;
	protected ICluster parent = null;

	protected Map<String, ITimeSeriesObject> idToObject;
	protected Map<ITimeSeriesObject, ISimilarityValue> objectSimilarities;

	// added 2016/12/08
	protected String name;

	protected FeatureValueSampleManager featureValueSampleManager;

	protected ObjectProviderManager objectProviderManager;

	/**
	 * 
	 */
	Cluster() {
		super();
		this.idToObject = new HashMap<>();
		this.objectSimilarities = new LinkedHashMap<>();
		this.featureValueSampleManager = new FeatureValueSampleManager(this);
		this.objectProviderManager = new ObjectProviderManager(this);
	}
//
//	Cluster(final Cluster other, final boolean clone) {
//		super();
//		if (clone)
//			this.internalClusterNumber = other.getInternalClusterId();
//		if (clone)
//			this.clusterNumber = other.getClusterNumber();
//		this.keep = other.isKeep();
//		this.objectSimilarities = new HashMap<>(other.getSimilarities());
//		this.prototype = other.getPrototype();
//		this.avgSimilarity = other.getAvgSimilarity();
//
//	}

	@Override
	public IObjectProvider copy() throws InterruptedException {
//		return new Cluster(this, false);
		// TODO
		return null;
	}

	void setClustering(IClusterObjectMapping clustering) {
		this.clustering = Objects.requireNonNull(clustering);
		this.clusteringId = clustering.getClusteringId();
	}

	@Override
	public IClusterObjectMapping getClustering() {
		return this.clustering;
	}

	/**
	 * @return the objectSimilarities
	 */
	@Override
	public ITimeSeriesObjectList getObjects() {
		return new TimeSeriesObjectList(this.objectSimilarities.keySet());
	}

	@Override
	public int size() {
		return this.objectSimilarities.size();
	}

	@Override
	public void addObject(final ITimeSeriesObject object, final ISimilarityValue similarity)
			throws DuplicateMappingForObjectException {
		this.objectSimilarities.put(object, similarity);
		this.idToObject.put(object.getName(), object);
	}

	@Override
	public void removeObjects(final ITimeSeriesObjects objects) {
		for (final ITimeSeriesObject object : objects) {
			this.objectSimilarities.remove(object);
			this.idToObject.remove(object.getName());
		}
	}

	@Override
	public boolean containsObject(final ITimeSeriesObject object) {
		return this.objectSimilarities.containsKey(object);
	}

	@Override
	public ISimilarityValue getSimilarity(final ITimeSeriesObject object) throws ClusterObjectMappingNotFoundException {
		if (!this.containsObject(object))
			throw new ClusterObjectMappingNotFoundException();
		return this.objectSimilarities.get(object);
	}

	@Override
	public Map<ITimeSeriesObject, ISimilarityValue> getSimilarities() {
		return Collections.unmodifiableMap(this.objectSimilarities);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Cluster))
			return false;

		final Cluster other = (Cluster) obj;

		if (!Objects.equals(this.clusteringId, other.clusteringId))
			return false;

		return this.clusterNumber == other.clusterNumber;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.clusteringId, this.clusterNumber);
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		if (this.name == null) {
			this.name = "Cluster " + this.clusterNumber;
		}
		return this.name;
	}

	@Override
	public void updatePrototype(final IPrototype prototype) {
		if (prototype == null)
			throw new NullPointerException("Prototype may not be null");
		this.prototype = prototype;
	}

	@Override
	public void setParent(final ICluster parent) {
		this.parent = parent;
	}

	@Override
	public ICluster getParent() {
		return this.parent;
	}

	@Override
	public IPrototype getPrototype() {
		return this.prototype;
	}

	@Override
	public void setKeep(final boolean keep) {
		this.keep = keep;
	}

	@Override
	public boolean isKeep() {
		return this.keep;
	}

	@Override
	public int getClusterNumber() {
		return this.clusterNumber;
	}

	@Override
	public long getInternalClusterId() {
		if (this.internalClusterNumber == 0) {
			// we have deserialized this cluster object from an older class
			// version without this counter;
			// we assign clusetrs basically random longs, not necessarily in the
			// original order of their creation
			this.internalClusterNumber = ++ClusterBuilder.globalNumberOfClusters;
		}
		return this.internalClusterNumber;
	}

	@Override
	public String toString() {
		// return Arrays.toString(this.pattern);
		// return String.format("Cluster %d %s", this.patternNumber,
		// Arrays.toString(this.prototype));
		// return String.format("Cluster %d", this.clusterNumber);

		final StringBuilder sb = new StringBuilder();
		sb.append(this.getName());
		sb.append(" (");
		sb.append(this.objectSimilarities.size());
		sb.append(")");
		// sb.append(": {");
		// for (ITimeSeriesObject obj : this.objectSimilarities.keySet()) {
		// sb.append(obj.getName());
		// sb.append(",");
		// }
		// sb.deleteCharAt(sb.length() - 1);
		// sb.append("}");
		return sb.toString();
	}

	private void readObject(final ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		// deserialize of older version: use string instead of network;
		if (this.name == null) {
			this.name = "Cluster " + this.clusterNumber;
		}
	}

	@Override
	public ClusterList asSingletonList() {
		final ClusterList list = new ClusterList();
		list.add(this);
		return list;
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}
}