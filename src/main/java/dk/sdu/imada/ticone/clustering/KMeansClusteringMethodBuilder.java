/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 28, 2019
 *
 */
public class KMeansClusteringMethodBuilder extends AbstractClusteringMethodBuilder<KMeansClusteringMethod> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3394321983356355187L;

	public static final int DEFAULT_MAX_ITERATIONS = 50, DEFAULT_NSTART = 5;

	private int maxIterations, nstart;

	public KMeansClusteringMethodBuilder() {
		super();
		this.maxIterations = DEFAULT_MAX_ITERATIONS;
		this.nstart = DEFAULT_NSTART;
	}

	public KMeansClusteringMethodBuilder(KMeansClusteringMethodBuilder other) {
		super(other);
		this.maxIterations = other.maxIterations;
		this.nstart = other.nstart;
	}

	@Override
	public KMeansClusteringMethodBuilder copy() {
		return new KMeansClusteringMethodBuilder(this);
	}

	@Override
	public KMeansClusteringMethod copy(KMeansClusteringMethod clusteringMethod) {
		return new KMeansClusteringMethod(clusteringMethod);
	}

	@Override
	public KMeansClusteringMethod doBuild() throws ClusteringMethodFactoryException,
			CreateClusteringMethodInstanceFactoryException, InterruptedException {
		try {
			return new KMeansClusteringMethod(similarityFunction, prototypeBuilder, maxIterations, nstart);
		} catch (IncompatibleSimilarityFunctionException e) {
			throw new CreateClusteringMethodInstanceFactoryException(e);
		}
	}

	@Override
	protected void reset() {
	}

	/**
	 * @param nstart the nstart to set
	 */
	public KMeansClusteringMethodBuilder setNstart(int nstart) {
		this.nstart = nstart;
		return this;
	}

	/**
	 * @param maxIterations the maxIterations to set
	 */
	public KMeansClusteringMethodBuilder setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
		return this;
	}
}
