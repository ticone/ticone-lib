package dk.sdu.imada.ticone.clustering.suggestclusters;

import java.util.HashMap;
import java.util.Map;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * This class is used as a container for old patterns, and newly suggested
 * patterns, before they are applied to the clustering.
 * 
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public class ClusterSuggestion implements IClusterSuggestion {
	private final IClusterObjectMapping oldClusterObjectMapping;
	private final IClusterObjectMapping newClusterObjectMapping;
	// private Map<Pattern, List<TimeSeriesData>> patternsDataToDelete;
	private final ITimeSeriesObjects objectsToDelete;
	private final Map<ICluster, Boolean> patternsToKeepMap;

	public ClusterSuggestion(final IClusterObjectMapping oldClusterObjectMapping,
			final IClusterObjectMapping newClusterObjectMapping, final ITimeSeriesObjects objectsToDelete) {
		this.oldClusterObjectMapping = oldClusterObjectMapping;
		// this.patternsDataToDelete = patternsDataToDelete;
		this.objectsToDelete = objectsToDelete;
		this.newClusterObjectMapping = newClusterObjectMapping;
		this.patternsToKeepMap = new HashMap<>();
		this.markPatternsToKeep(newClusterObjectMapping.getClusters());
	}

	private void markPatternsToKeep(final IClusters patterns) {
		for (final ICluster p : patterns) {
			this.patternsToKeepMap.put(p, true);
		}
	}

	@Override
	public void keepPattern(final ICluster pattern, final boolean keep) {
		this.patternsToKeepMap.put(pattern, keep);
	}

	@Override
	public boolean getKeepStatusForPattern(final ICluster pattern) {
		return this.patternsToKeepMap.get(pattern);
	}

	@Override
	public IClusterObjectMapping getNewClusterObjectMapping() {
		return this.newClusterObjectMapping;
	}

	@Override
	public IClusterObjectMapping getOldClusterObjectMapping() {
		return this.oldClusterObjectMapping;
	}

	/*
	 * public Map<Pattern, List<TimeSeriesData>> getPatternsDataToDelete() { return
	 * patternsDataToDelete; }
	 */

	@Override
	public ITimeSeriesObjects getObjectsToDelete() {
		return this.objectsToDelete;
	}
}
