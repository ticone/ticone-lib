package dk.sdu.imada.ticone.clustering;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair.ClusterPairsFactory;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPairList;
import dk.sdu.imada.ticone.clustering.pair.IClusterPairs;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPairList;
import dk.sdu.imada.ticone.data.IObjectPairs;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectSet;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairBuilder;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairsFactory;
import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.feature.ClusterFeaturePrior;
import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IClusterFeaturePrior;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototype;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValues;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.IObjectProviderManager;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectProviderManager;
import dk.sdu.imada.ticone.util.ObjectSampleException;
import dk.sdu.imada.ticone.util.Progress;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntSet;

public class ClusterObjectMapping implements IClusterObjectMapping {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4662805729969664693L;

	@Override
	public void addObjectsToMostSimilarCluster(final ITimeSeriesObjects objects, final ISimilarityFunction similarity)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			NoComparableSimilarityValuesException, IncompatibleSimilarityValueException {
		this.addObjectsToMostSimilarCluster(objects, similarity, null);
	}

	@Override
	public void addObjectsToMostSimilarCluster(final ITimeSeriesObjects objects, final ISimilarityFunction similarity,
			final Progress progress) throws SimilarityCalculationException, InterruptedException,
			SimilarityValuesException, NoComparableSimilarityValuesException, IncompatibleSimilarityValueException {
		if (objects.isEmpty())
			return;
		final ITimeSeriesObjects newObjects = filterObjects(similarity, objects.stream());
		final ClusterSet clusters = filterClusters(similarity, this.getClusters());

		if (progress != null)
			progress.updateProgress(null, "Calculating object-cluster similarities");

		final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = similarity
				.calculateSimilarities(newObjects.toArray(), clusters.toArray(), new ObjectClusterPairsFactory(),
						ObjectType.OBJECT_CLUSTER_PAIR);

		if (progress != null)
			progress.updateProgress(0.5, "Reassigning objects to most similar cluster");

		allObjects.addAll(objects);
		final int[] whichMaxForFirstObject = sims.whichMaxForFirstObject(sims.firstObjectIndices());
		int p = 0;
		for (final int objectIdx : sims.firstObjectIndices()) {
			int whichMax = whichMaxForFirstObject[p++];
			final ITimeSeriesObject object = sims.getObject1(objectIdx);
			final ISimilarityValue sim = sims.maxForFirstObject(objectIdx);
			final ICluster cluster = whichMax >= 0 ? sims.getObject2(whichMax) : null;

			final ICluster prevCluster = objectToCluster.get(object);
			ISimilarityValue prevSim = null;
			try {
				if (prevCluster != null) {
					prevSim = prevCluster.getSimilarity(object);
					if (prevSim.greaterThanOrEquals(sim))
						continue;
					else if (prevCluster != cluster)
						removeObjectsFromCluster(objectToCluster.get(object), object.asSingletonList());
				}
			} catch (ClusterObjectMappingNotFoundException e1) {
			}
			if (cluster == null)
				continue;
			object.updateNearestPattern(cluster, sim);
			try {
				addMapping(object, cluster, sim);
			} catch (DuplicateMappingForObjectException e) {
				e.printStackTrace();
			}
		}
	}

	private static ClusterSet filterClusters(final ISimilarityFunction similarity, IClusterSet clusters) {
		return clusters.stream().filter(c -> {
			try {
				return similarity.isDefinedFor(c);
			} catch (InterruptedException e1) {
				return true;
			}
		}).collect(Collectors.toCollection(ClusterSet::new));
	}

	private static ITimeSeriesObjects filterObjects(final ISimilarityFunction similarity,
			final Stream<ITimeSeriesObject> filteredObjects) {
		return filteredObjects.filter(o -> {
			try {
				return similarity.isDefinedFor(o);
			} catch (InterruptedException e1) {
				return true;
			}
		}).collect(Collectors.toCollection(TimeSeriesObjectList::new));
	}

	protected static int nextClusteringId;

	protected Logger logger;

	class ClusterMap implements IClusters, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6854232961650630379L;

		private Int2ObjectOpenHashMap<ICluster> map;

		ClusterMap() {
			map = new Int2ObjectOpenHashMap<>();
		}

		ClusterMap(ClusterMap other) {
			super();
			this.map = new Int2ObjectOpenHashMap<>(other.map);
		}

		@Override
		public String toString() {
			return map.values().toString();
		}

		@Override
		public Iterator<ICluster> iterator() {
			return map.values().iterator();
		}

		@Override
		public IObjectProviderManager getObjectProviderManager() {
			return null;
		}

		@Override
		public IFeatureValueSampleManager getFeatureValueSampleManager() {
			return null;
		}

		@Override
		public IClusterList asList() {
			return new ClusterList(map.values());
		}

		@Override
		public IClusterSet asSet() {
			return new ClusterSet(map.values());
		}

		@Override
		public IClusters copy() {
			return new ClusterMap(this);
		}

		@Override
		public boolean contains(Object o) {
			return map.containsValue(o);
		}

		@Override
		public boolean add(ICluster e) {
			return map.put(e.getClusterNumber(), e) != e;
		}

		@Override
		public boolean containsAll(Collection<?> c) {
			return map.values().containsAll(c);
		}

		@Override
		public boolean addAll(Collection<? extends ICluster> c) {
			boolean changed = false;
			for (ICluster cl : c)
				changed |= this.add(cl);
			return changed;
		}

		@Override
		public boolean removeAll(Collection<?> c) {
			boolean changed = false;
			for (Object o : c)
				if (o instanceof ICluster)
					changed |= remove(o);
			return changed;
		}

		@Override
		public boolean retainAll(Collection<?> c) {
			boolean changed = false;
			for (ICluster cl : this)
				if (!c.contains(cl))
					changed |= remove(cl);
			return changed;
		}

		@Override
		public <T> T[] toArray(T[] a) {
			return map.values().toArray(a);
		}

		@Override
		public ICluster[] toArray() {
			return toArray(new ICluster[0]);
		}

		@Override
		public int size() {
			return map.size();
		}

		@Override
		public boolean isEmpty() {
			return map.isEmpty();
		}

		@Override
		public boolean remove(Object o) {
			if (o instanceof ICluster)
				return map.remove(((ICluster) o).getClusterNumber(), o);
			return false;
		}

		@Override
		public void clear() {
			map.clear();
		}

		public boolean containsClusterWithNumber(int clusterNumber) {
			return map.containsKey(clusterNumber);
		}

		public ICluster get(int clusterNumber) {
			return map.get(clusterNumber);
		}

		public IntSet getClusterNumbers() {
			return map.keySet();
		}

		public ICluster remove(int clusterNumber) {
			return map.remove(clusterNumber);
		}

		@Override
		public IClusterPairs getClusterPairs() {
			final ICluster[] array = this.toArray();
			return new ClusterPair.ClusterPairsFactory().createIterable(array, array);
		}
	}

	protected long clusteringId;
	protected PrivateClusterBuilder clusterBuilder;
	protected ClusterMap clusters;
	protected final ITimeSeriesObjectSet allObjects;
	protected Map<String, ITimeSeriesObject> objectIdToObject;
	protected Map<ITimeSeriesObject, ICluster> objectToCluster;
	protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);

	protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

	public ClusterObjectMapping(final IPrototypeBuilder prototypeBuilder) {
		this(new TimeSeriesObjectSet(), prototypeBuilder);
	}

	public ClusterObjectMapping(final ITimeSeriesObjectSet allObjects, final IPrototypeBuilder prototypeBuilder) {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.clusteringId = ++nextClusteringId;
		this.allObjects = allObjects;
		this.clusters = new ClusterMap();
		this.objectIdToObject = new LinkedHashMap<>();
		this.objectToCluster = new LinkedHashMap<>();
		this.initClusterBuilder(Objects.requireNonNull(prototypeBuilder));
	}

	private void initClusterBuilder(final IPrototypeBuilder prototypeBuilder) {
		this.clusterBuilder = new PrivateClusterBuilder(prototypeBuilder);
		this.clusterBuilder.setClustering(this);
	}

	/**
	 * A type of {@link ClusterBuilder} that automatically adds built clusters to
	 * its {@link ClusterObjectMapping} instance.
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Dec 7, 2018
	 *
	 */
	class PrivateClusterBuilder extends ClusterBuilder {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1498195855051538496L;

		/**
		 * 
		 */
		PrivateClusterBuilder(final IPrototypeBuilder prototypeBuilder) {
			super();
			this.clustering = ClusterObjectMapping.this;
			this.prototypeBuilder = prototypeBuilder;
		}

		@Override
		protected Cluster doBuild()
				throws CreateClusterInstanceFactoryException, InterruptedException, ClusterFactoryException {
			return doBuild(true);
		}

		private Cluster doBuild(final boolean addToClustering)
				throws CreateClusterInstanceFactoryException, ClusterFactoryException, InterruptedException {
			final Cluster build = super.doBuild();
			if (addToClustering) {
				if (ClusterObjectMapping.this.clusters.containsClusterWithNumber(build.clusterNumber))
					throw new ClusterFactoryException(
							"The cluster could not be added to the clustering, because another cluster with the same number is already present.");
				ClusterObjectMapping.this.addCluster(build);
			}
			return build;
		}

		@Override
		public Cluster copy(ICluster cluster, boolean clone)
				throws ClusterFactoryException, CreateClusterInstanceFactoryException, InterruptedException {
			if (clone)
				internalClusterNumber = cluster.getInternalClusterId();
			if (clone)
				clusterNumber = cluster.getClusterNumber();
			isKeep = cluster.isKeep();
			objects = new LinkedHashMap<>(cluster.getSimilarities());
			if (cluster.getPrototype() != null) {
				if (prototypeBuilder != null
						&& !prototypeBuilder.producesComparablePrototypesTo(cluster.getPrototype()))
					throw new CreateClusterInstanceFactoryException(
							"Could not copy cluster with incompatible prototype components.");
				else
					prototype = cluster.getPrototype().copy();
			}
			Cluster build;
			if (clone && cluster.getClustering() == ClusterObjectMapping.this) {
				build = doBuild(false);
				reset();
			} else
				build = (Cluster) build();
			return build;
		}

		@Override
		protected Cluster createEmptyClusterInstance() {
			return new PrivateCluster();
		}

		class PrivateCluster extends Cluster {

			/**
			 * 
			 */
			private static final long serialVersionUID = -2656773301354762888L;

			/**
			 * 
			 */
			PrivateCluster() {
				super();
				this.setClustering(ClusterObjectMapping.this);
			}

			@Override
			public boolean equals(Object obj) {
				if (!(obj instanceof PrivateCluster))
					return false;

				final PrivateCluster other = (PrivateCluster) obj;

				if (this.clustering != other.clustering)
					return false;

				return this.clusterNumber == other.clusterNumber;
			}

			@Override
			public int hashCode() {
				return Objects.hash(this.clustering, this.clusterNumber);
			}

			@Override
			public void addObject(ITimeSeriesObject object, ISimilarityValue similarity)
					throws DuplicateMappingForObjectException {
				if (ClusterObjectMapping.this.objectToCluster.containsKey(object)
						&& ClusterObjectMapping.this.objectToCluster.get(object) != this)
					throw new DuplicateMappingForObjectException(
							"The specified object is already member of another cluster");
				super.addObject(object, similarity);

				objectToCluster.put(object, this);
				objectIdToObject.put(object.getName(), object);
				allObjects.add(object);
			}

			@Override
			public void removeObjects(ITimeSeriesObjects objects) {
				super.removeObjects(objects);

				for (ITimeSeriesObject o : objects) {
					objectToCluster.remove(o);
				}
			}
		}
	}

	@Override
	public boolean equalObjectPartition(final IClusterObjectMapping other) {
		return Objects.equals(this.clusters.stream().map(c -> c.getObjects()).collect(Collectors.toSet()),
				other.getClusters().stream().map(c -> c.getObjects()).collect(Collectors.toSet()));
	}

	@Override
	public void addMapping(final ITimeSeriesObject object, final ICluster cluster, final ISimilarityValue coefficient)
			throws DuplicateMappingForObjectException {
		if (object == null)
			return;
		verifyCluster(cluster);

		if (this.objectIdToObject.containsKey(object.getName())
				&& this.objectIdToObject.get(object.getName()) != object) {
			logger.warn(
					"Another object of the name '%s' is already contained in this ClusterObjectMapping -> Not adding",
					object.getName());
			return;
		}

		this.clusters.add(cluster);
		this.objectToCluster.put(object, cluster);
		this.objectIdToObject.putIfAbsent(object.getName(), object);

		cluster.addObject(object, coefficient);
	}

	@Override
	public void addMappings(final Map<ITimeSeriesObject, Map<ICluster, ISimilarityValue>> coefficients)
			throws DuplicateMappingForObjectException {
		for (final ITimeSeriesObject object : coefficients.keySet()) {
			for (final ICluster cluster : coefficients.get(object).keySet()) {
				this.addMapping(object, cluster, coefficients.get(object).get(cluster));
			}
		}
	}

	@Override
	public void mergeMappings(final IClusterObjectMapping patternObjectMapping)
			throws CreateClusterInstanceFactoryException, InterruptedException, ClusterFactoryException,
			DuplicateMappingForObjectException {
		// TODO: check that the two clusterings have the same number of
		// timepoints
		for (final ICluster p : patternObjectMapping.getClusters()) {
			final Cluster copiedCluster = clusterBuilder.copy(p);
			final ITimeSeriesObjects timeSeriesDataArrayList = copiedCluster.getObjects();
			for (final ITimeSeriesObject timeSeriesData : timeSeriesDataArrayList) {
				try {
					final ISimilarityValue coefficient = copiedCluster.getSimilarity(timeSeriesData);
					this.addMapping(timeSeriesData, copiedCluster, coefficient);
				} catch (final ClusterObjectMappingNotFoundException pomnfe) {

				}
			}
		}
	}

	@Override
	public Map<ICluster, ISimilarityValue> getSimilarities(final ITimeSeriesObject object) {
		final Map<ICluster, ISimilarityValue> result = new HashMap<>();
		for (final ICluster c : this.getClusters())
			if (c.containsObject(object))
				try {
					result.put(c, c.getSimilarity(object));
				} catch (final ClusterObjectMappingNotFoundException e) {
					e.printStackTrace();
				}
		return result;
	}

	@Override
	public ITimeSeriesObject getTimeSeriesData(final String objectName) {
		return this.objectIdToObject.get(objectName);
	}

	@Override
	public IObjectClusterPairList getObjectClusterPairs() {
		final IObjectClusterPairList pairs = new ObjectClusterPairsFactory().createEmptyList();
		for (final ITimeSeriesObject o : this.getAssignedObjects())
			pairs.add(new ObjectClusterPair(o, this.objectToCluster.get(o)));
		return pairs;
	}

	@Override
	public IClusterPairList getClusterPairs() {
		return new ClusterPairsFactory().createList(this.getClusters(), this.getClusters());
	}

	@Override
	public IPrototypeBuilder getPrototypeBuilder() {
		return this.clusterBuilder.prototypeBuilder;
	}

	/**
	 * Clusters created with this cluster builder are automatically added to this
	 * clustering.
	 * 
	 * @return
	 */
	public ClusterBuilder getClusterBuilder() {
		return this.clusterBuilder;
	}

	@Override
	public ITimeSeriesObjectList getAllObjects() {
		return this.allObjects.asList();
	}

	@Override
	public TimeSeriesObjectList getAssignedObjects() {
		return new TimeSeriesObjectList(this.objectToCluster.keySet());
	}

	@Override
	public ITimeSeriesObjectList getUnassignedObjects() {
		final ITimeSeriesObjectSet unassignedObjects = this.allObjects.copy();
		unassignedObjects.removeAll(objectToCluster.keySet());
		return unassignedObjects.asList();
	}

	@Override
	public IObjectPairs getObjectPairs() {
		return new ObjectPairsFactory().createList(this.getAssignedObjects(), this.getAssignedObjects());
	}

	@Override
	public IClusterSet getClusters() {
		return this.clusters.asSet();
	}

	@Override
	public ICluster getCluster(final int clusterNumber) {
		return this.clusters.get(clusterNumber);
	}

	@Override
	public IntSet getClusterNumbers() {
		return this.clusters.getClusterNumbers();
	}

	@Override
	public long getClusteringId() {
		return this.clusteringId;
	}

	@Override
	public void removeData(final int clusterNumber, final IClusterObjectMapping.DELETE_METHOD delete_method) {
		final ICluster cluster = requireClusterWithNumber(clusterNumber);
		switch (delete_method) {
		case ONLY_PROTOTYPE:
			this.removeCluster(cluster);
			break;
		case ONLY_OBJECTS:
			this.removeObjectsFromCluster(cluster);
			break;
		case BOTH_PROTOTYPE_AND_OBJECTS:
			this.removeObjectsFromCluster(cluster);
			this.removeCluster(cluster);
			break;
		}
	}

	/**
	 *
	 * @param cluster
	 */
	@Override
	public void removeCluster(final ICluster cluster) {
		verifyCluster(cluster);
		final ITimeSeriesObjects clusterObjects = cluster.getObjects();
		this.clusters.remove(cluster);

		for (final ITimeSeriesObject o : clusterObjects) {
			this.objectToCluster.remove(o);
		}
	}

	/**
	 * Removes all clusters from this clustering with a size of 0, i.e. that do not
	 * contain any objects.
	 * 
	 * Clusters marked as "to keep" will not be removed.
	 * 
	 * @return The number of removed empty clusters.
	 */
	@Override
	public int removeEmptyClusters() {
		return removeEmptyClusters(false);
	}

	/**
	 * @see #removeEmptyClusters()
	 * 
	 * @param ignoreKeep Whether to ignore the keep flag of clusters and force
	 *                   removal if empty.
	 * @return The number of removed empty clusters.
	 */
	@Override
	public int removeEmptyClusters(final boolean ignoreKeep) {
		final IClusterList emptyClusters = new ClusterList();
		for (ICluster c : this.clusters)
			if (c.size() == 0 && (ignoreKeep || !c.isKeep()))
				emptyClusters.add(c);
		for (ICluster c : emptyClusters)
			this.removeCluster(c);
		if (emptyClusters.size() > 0)
			logger.debug("Removed {} empty clusters", emptyClusters.size());
		return emptyClusters.size();
	}

	/**
	 * Removes duplicate prototypes from this clustering.
	 * 
	 * Prototypes marked as "to keep" will not be removed.
	 * 
	 * @return The number of removed empty clusters.
	 */
	@Override
	public int removeDuplicatePrototypes() {
		return removeDuplicatePrototypes(false);
	}

	/**
	 * @see #removeDuplicatePrototypes()
	 * 
	 * @param ignoreKeep Whether to ignore the keep flag and force removal if
	 *                   duplicate.
	 * @return The number of removed duplicate prototypes.
	 */
	@Override
	public int removeDuplicatePrototypes(final boolean ignoreKeep) {
		final IClusterList duplicateClusters = new ClusterList();
		final Map<IPrototype, ICluster> prototypeToCluster = new HashMap<>();
		for (ICluster c : this.clusters) {
			if (prototypeToCluster.containsKey(c.getPrototype()) && prototypeToCluster.get(c.getPrototype()) != c
					&& (ignoreKeep || !c.isKeep()))
				duplicateClusters.add(c);
			else
				prototypeToCluster.put(c.getPrototype(), c);
		}
		for (ICluster c : duplicateClusters)
			this.removeCluster(c);
		if (duplicateClusters.size() > 0)
			logger.debug("Removed {} duplicated prototypes", duplicateClusters.size());
		return duplicateClusters.size();

	}

	/**
	 *
	 * @param cluster
	 * @param patternsData
	 */
	private void removeObjectsFromCluster(final ICluster cluster) {
		this.removeObjectsFromCluster(cluster, cluster.getObjects());
	}

	public void removeObjectsFromAllClusters() {
		for (ICluster c : this.getClusters())
			this.removeObjectsFromCluster(c);
	}

	@Override
	public void removeObjectsFromCluster(final ICluster cluster, final ITimeSeriesObjects clusterObjects) {
		if (clusterObjects == null)
			return;
		verifyCluster(cluster);

		for (final ITimeSeriesObject o : clusterObjects) {
			if (!this.objectToCluster.containsKey(o))
				continue;
			this.objectToCluster.remove(o);
			this.objectIdToObject.remove(o.getName());
		}

		cluster.removeObjects(clusterObjects);
	}

	@Override
	public void removeObjects(ITimeSeriesObjects objects) {
		for (ICluster cluster : this.clusters) {
			this.removeObjectsFromCluster(cluster, objects);
		}
		this.allObjects.removeAll(objects);
	}

	private ICluster requireClusterWithNumber(final int clusterNumber) {
		if (!this.clusters.containsClusterWithNumber(clusterNumber))
			throw new IllegalArgumentException("This clustering does not contain a cluster with the specified number.");
		return this.clusters.get(clusterNumber);
	}

	private void verifyCluster(final ICluster cluster) {
		Objects.requireNonNull(cluster);
		if (cluster.getClustering() != this)
			throw new IllegalArgumentException("Cluster was not created for this clustering.");
	}

	@Override
	public ICluster addCluster(final IPrototype prototype) throws ClusterFactoryException,
			CreateClusterInstanceFactoryException, InterruptedException, IncompatiblePrototypeException {
		final Lock lock = this.clusterBuilder.getLock();
		lock.lock();
		try {
			if (!(prototype instanceof MissingPrototype) && !prototype.getPrototypeComponentTypes()
					.equals(this.clusterBuilder.prototypeBuilder.getPrototypeComponentFactories().keySet()))
				throw new IncompatiblePrototypeException();

			// check if another cluster has the same prototype
			final Optional<ICluster> filter = this.clusters.stream().filter(c -> c.getPrototype().equals(prototype))
					.findFirst();
			if (filter.isPresent()) {
				this.logger.debug("Another cluster with the same prototype is already present");
				return filter.get();
			}

			return this.clusterBuilder.setPrototype(prototype).build();
		} finally {
			lock.unlock();
		}
	}

	private boolean addCluster(final ICluster cluster) {
		if (this.clusters.contains(cluster))
			return false;
		for (final Entry<ITimeSeriesObject, ISimilarityValue> e : cluster.getSimilarities().entrySet()) {
			final ITimeSeriesObject o = e.getKey();
			this.objectToCluster.put(o, cluster);
			this.objectIdToObject.put(o.getName(), o);
		}
		this.allObjects.addAll(cluster.getObjects());
		this.clusters.add(cluster);
		return true;
	}

	@Override
	public ICluster addCluster(final ITimeSeriesObjects objects, final ISimilarityFunction simFunc)
			throws PrototypeFactoryException, CreatePrototypeInstanceFactoryException, InterruptedException,
			ClusterFactoryException, CreateClusterInstanceFactoryException, SimilarityCalculationException,
			SimilarityValuesException, IncompatibleSimilarityValueException, DuplicateMappingForObjectException {
		final Lock lock = this.clusterBuilder.getLock();
		lock.lock();
		try {
			final IPrototype prototype = this.clusterBuilder.prototypeBuilder.setObjects(objects).build();
			ICluster cluster = this.clusterBuilder.setPrototype(prototype).build();
			ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = simFunc
					.calculateSimilarities(objects.toArray(), cluster.asSingletonList().toArray(),
							new ObjectClusterPair.ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR);
			final ISimilarityValue[] allSims = sims.getForAllPairs(sims.firstObjectIndices(),
					sims.secondObjectIndices());
			int p = 0;
			for (int[] idx : sims.pairIndices())
				cluster.addObject(sims.getObject1(idx[0]), allSims[p++]);
			return cluster;
		} finally {
			lock.unlock();
		}
	}

	@Override
	public boolean addAll(final Collection<? extends ICluster> elems) {
		for (final ICluster c : elems)
			this.addCluster(c);
		return true;
	}

	public class ClusterObjectMappingCopy implements IClusterObjectMappingCopy {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5255377711222282350L;

		protected final boolean clone;

		protected final ClusterObjectMapping copy;

		protected final Map<ICluster, ICluster> originalToCopy, copyToOriginal;

		/**
		 * @throws InterruptedException
		 * @throws CreateClusterInstanceFactoryException
		 * @throws ClusterFactoryException
		 * 
		 */
		public ClusterObjectMappingCopy()
				throws ClusterFactoryException, CreateClusterInstanceFactoryException, InterruptedException {
			this(false);
		}

		ClusterObjectMappingCopy(final boolean clone)
				throws ClusterFactoryException, CreateClusterInstanceFactoryException, InterruptedException {
			super();
			this.clone = clone;
			final IPrototypeBuilder prototypeBuilder;
			if (ClusterObjectMapping.this.clusterBuilder.prototypeBuilder != null)
				prototypeBuilder = ClusterObjectMapping.this.clusterBuilder.prototypeBuilder.copy();
			else
				prototypeBuilder = null;
			this.copy = new ClusterObjectMapping(allObjects.copy(), prototypeBuilder);
			this.copy.clusterBuilder.nextClusterId = clusterBuilder.nextClusterId;
			if (clone)
				this.copy.clusteringId = clusteringId;
			this.originalToCopy = new HashMap<>();
			this.copyToOriginal = new HashMap<>();

			for (final ICluster p : ClusterObjectMapping.this.getClusters()) {
				final Cluster copiedCluster = copy.clusterBuilder.copy(p, clone);
				if (clone) {
					assert p.getClusterNumber() == copiedCluster.getClusterNumber();
				}
				assert p.getObjects().equals(copiedCluster.getObjects());
				final ITimeSeriesObjects timeSeriesDataArrayList = copiedCluster.getObjects();
				for (final ITimeSeriesObject timeSeriesData : timeSeriesDataArrayList) {
					try {
						final ISimilarityValue coefficient = copiedCluster.getSimilarity(timeSeriesData);
						copy.addMapping(timeSeriesData, copiedCluster, coefficient);
					} catch (final ClusterObjectMappingNotFoundException pomnfe) {

					} catch (DuplicateMappingForObjectException e) {
						e.printStackTrace();
					}
				}
				originalToCopy.put(p, copiedCluster);
				copyToOriginal.put(copiedCluster, p);
			}

			assert ClusterObjectMapping.this.getClusters().size() == copy.getClusters().size();
			assert ClusterObjectMapping.this.getAssignedObjects().size() == copy.getAssignedObjects().size();
		}

		/**
		 * @return the original
		 */
		@Override
		public ClusterObjectMapping getOriginal() {
			return ClusterObjectMapping.this;
		}

		@Override
		public ClusterObjectMapping getCopy() {
			return this.copy;
		}

		@Override
		public ICluster getOriginal(final ICluster copy) {
			return this.copyToOriginal.get(copy);
		}

		@Override
		public ICluster getCopy(final ICluster original) {
			return this.originalToCopy.get(original);
		}
	}

	@Override
	public ClusterObjectMappingCopy getCopy() throws InterruptedException {
		return getCopy(false);
	}

	/**
	 * 
	 * @param clone If true, equals and hashCode returns true for cloned and
	 *              original {@link ClusterObjectMapping}.
	 * @return
	 * @throws InterruptedException
	 */
	public ClusterObjectMappingCopy getCopy(final boolean clone) throws InterruptedException {
		try {
			return new ClusterObjectMappingCopy(clone);
		} catch (Exception e) {
			// shouldn't happen
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ClusterObjectMapping copy() throws InterruptedException {
		return this.getCopy().copy;
	}

	@Override
	public IClusterObjectMapping newInstance(ITimeSeriesObjectSet allObjects) throws InterruptedException {
		if (this.clusterBuilder.prototypeBuilder != null)
			return new ClusterObjectMapping(allObjects.copy(), this.clusterBuilder.prototypeBuilder.copy());
		return new ClusterObjectMapping(allObjects.copy(), null);
	}

	public <SF extends ISimilarityFunction> ISimilarityValue calculateAverageObjectClusterSimilarity(final SF simFunc)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleSimilarityValueException {
		final Map<ICluster, ? extends ISimilarityValues<ITimeSeriesObject, ICluster, IObjectClusterPair>> simsPerCluster = simFunc

				.calculateSimilarities(this.getObjectClusterPairs(), ObjectType.OBJECT_CLUSTER_PAIR)
				.partitionBySecondObject();

		ISimilarityValue totalAverageClusterSim = simFunc.missingValuePlaceholder();
		int c = 0;
		for (final ICluster cluster : this.getClusters()) {
			ISimilarityValue meanSim;
			if (simsPerCluster.containsKey(cluster)) {
				meanSim = simsPerCluster.get(cluster).mean();
				c++;
			} else
				meanSim = simFunc.missingValuePlaceholder();
			totalAverageClusterSim = totalAverageClusterSim.plus(meanSim);
		}
		return totalAverageClusterSim.divideBy(c);
	}

	@Override
	public String toString() {
		return String.format("Clustering %d", this.clusteringId);
	}

	@Override
	public void removeObjectsNotInNetwork(final ITiconeNetwork network) {
		// remove objects from clustering, which are not in network
		final ITimeSeriesObjects objectsToRemove = new TimeSeriesObjectList();
		for (final ITimeSeriesObject tsd : this.getAssignedObjects())
			if (!network.containsNode(tsd.getName())) {
				objectsToRemove.add(tsd);
			}
		for (final ICluster p : this.getClusters())
			this.removeObjectsFromCluster(p, objectsToRemove);
		logger.debug("Removed " + objectsToRemove.size() + " objects from data set not contained in network.");
	}

	@Override
	public <V> IFeatureValue<V> getFeatureValue(IFeature<V> feature, IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException, UnknownObjectFeatureValueProviderException,
			IncompatibleFeatureAndObjectException {
		if (feature instanceof IClusterFeaturePrior) {
			return (IFeatureValue<V>) getFeatureValue((ICluster) object, (IClusterFeaturePrior) feature);
		}
		throw new IncompatibleFeatureValueProviderException(this, feature);
	}

	private IFeatureValue<Double> getFeatureValue(final ICluster object, final IClusterFeaturePrior feature)
			throws UnknownObjectFeatureValueProviderException, IncompatibleFeatureAndObjectException {
		this.ensureObjectIsKnown(object);

		class Helper {
			protected double[] bins;
			protected double max = -Double.MAX_VALUE;
			protected double min = Double.MAX_VALUE;

			IFeatureValue<Double> calculate() throws IncompatibleFeatureAndObjectException {
				if (this.bins == null) {
					logger.debug("Recalculate density function");
					// new clustering
					// -> recalculate bins
					final ITimeSeriesObjects allObjects = ClusterObjectMapping.this.getAssignedObjects();

					for (final ITimeSeriesObject o : allObjects) {
						final ITimeSeries[] samples = o.getPreprocessedTimeSeriesList();
						for (int s = 0; s < samples.length; s++) {
							for (int tp = 0; tp < samples[s].getNumberTimePoints(); tp++) {
								this.min = Math.min(this.min, samples[s].get(tp));
								this.max = Math.max(this.max, samples[s].get(tp));
							}
						}
					}

					this.bins = new double[feature.getNumberBins()];
					long totalCount = 0l;

					// count bins
					for (final ITimeSeriesObject o : allObjects) {
						final ITimeSeries[] samples = o.getPreprocessedTimeSeriesList();
						for (int s = 0; s < samples.length; s++) {
							for (int tp = 0; tp < samples[s].getNumberTimePoints(); tp++) {
								final double v = samples[s].get(tp);
								final int bin = this.valueToBin(v);
								this.bins[bin]++;
								totalCount++;
							}
						}
					}

					for (int b = 0; b < this.bins.length; b++) {
						this.bins[b] /= totalCount;
						// avoid 0 probs
						this.bins[b] += Double.MIN_VALUE;
					}
					logger.debug(Arrays.toString(this.bins));
				}

				// calculate the probability to see each of the values
				final double[] probs = new double[object.getObjects().size()];
				final ITimeSeriesObjects clusterObjects = object.getObjects();
				int o = 0;
				for (final ITimeSeriesObject obj : clusterObjects) {
					double prob = 1.0;
					for (int s = 0; s < obj.getPreprocessedTimeSeriesList().length; s++)
						for (int tp = 0; tp < obj.getPreprocessedTimeSeriesList()[s].getNumberTimePoints(); tp++)
							prob *= this.bins[this.valueToBin(obj.getPreprocessedTimeSeriesList()[s].get(tp))];
					probs[o] = prob;
					o++;
				}

				return feature.value(object, ArraysExt.mean(probs));
			}

			private int valueToBin(final double value) {
				final int res = (int) ((value - this.min) / (this.max - this.min) * (feature.getNumberBins() - 1));
				return res;
			}

		}
		return new Helper().calculate();
	}

	@Override
	public void ensureObjectIsKnown(IObjectWithFeatures object) throws UnknownObjectFeatureValueProviderException {
		if (object instanceof ICluster)
			if (!this.getClusters().contains(object))
				throw new UnknownObjectFeatureValueProviderException(this, object);
		if (object instanceof IClusterPair) {
			final IClusterPair p = (IClusterPair) object;
			if (!this.getClusters().contains(p.getFirst()))
				throw new UnknownObjectFeatureValueProviderException(this, p.getFirst());
			if (!this.getClusters().contains(p.getSecond()))
				throw new UnknownObjectFeatureValueProviderException(this, p.getSecond());
		}
	}

	@Override
	public IFeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public IObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}

	@Override
	public ObjectPartition asObjectPartition() {
		return new ObjectPartition();
	}

	class ObjectPartition extends HashSet<HashSet<ITimeSeriesObject>> implements IObjectPartition {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7358236027421946242L;

		/**
		 * 
		 */
		ObjectPartition() {
			super();
			final List<Integer> clusterNumbers = new IntArrayList(ClusterObjectMapping.this.getClusterNumbers());
			Collections.sort(clusterNumbers);
			clusterNumbers.forEach(cN -> this.add(new HashSet<>(getCluster(cN).getObjects())));
		}

		private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
			stream.defaultReadObject();
		}
	}

	private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		this.logger = LoggerFactory.getLogger(this.getClass());
		if (this.objectToCluster != null)
			this.objectToCluster = new LinkedHashMap<>(this.objectToCluster);
	}

	@Override
	public Collection<ObjectType<?>> providedTypesOfObjects() {
		return Arrays.asList(ObjectType.OBJECT, ObjectType.OBJECT_PAIR, ObjectType.CLUSTER,
				ObjectType.OBJECT_CLUSTER_PAIR, ObjectType.CLUSTER_PAIR);
	}

	@Override
	public <T extends IObjectWithFeatures> int getNumberObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type.equals(ObjectType.CLUSTER))
			return this.clusters.size();
		else if (type.equals(ObjectType.CLUSTER_PAIR))
			return this.clusters.size() * this.clusters.size();
		else if (type.equals(ObjectType.OBJECT_PAIR))
			return this.allObjects.size() * this.allObjects.size();
		else if (type.equals(ObjectType.OBJECT))
			return this.allObjects.size();
		else if (type.equals(ObjectType.OBJECT_CLUSTER_PAIR)) {
			return this.objectToCluster.size();
		} else if (type.equals(ObjectType.CLUSTERING))
			return 1;
		return IClusterObjectMapping.super.getNumberObjectsOfType(type);
	}

	@Override
	public <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type.equals(ObjectType.CLUSTER))
			return (Collection<T>) this.getClusters();
		else if (type.equals(ObjectType.CLUSTER_PAIR))
			return (Collection<T>) this.getClusterPairs();
		else if (type.equals(ObjectType.OBJECT_PAIR))
			return (Collection<T>) this.getObjectPairs();
		else if (type.equals(ObjectType.OBJECT))
			return (Collection<T>) this.getAssignedObjects();
		else if (type.equals(ObjectType.OBJECT_CLUSTER_PAIR)) {
			return (Collection<T>) this.getObjectClusterPairs();
		} else if (type.equals(ObjectType.CLUSTERING))
			return (Collection<T>) Arrays.asList(this);
		return IClusterObjectMapping.super.getObjectsOfType(type);
	}

	@Override
	public <T extends IObjectWithFeatures> List<T> sampleObjectsOfType(ObjectType<T> type, int sampleSize, long seed)
			throws IncompatibleObjectProviderException, ObjectSampleException, InterruptedException {
		if (type == ObjectType.CLUSTER) {
			return this.clusters.asList().sampleObjectsOfType(type, sampleSize, seed);
		} else if (type == ObjectType.CLUSTER_PAIR) {
			return this.clusters.asList().sampleObjectsOfType(type, sampleSize, seed);
		} else if (type.equals(ObjectType.OBJECT_PAIR)) {
			return this.allObjects.asList().sampleObjectsOfType(type, sampleSize, seed);
		} else if (type.equals(ObjectType.OBJECT)) {
			return this.allObjects.asList().sampleObjectsOfType(type, sampleSize, seed);
		} else if (type.equals(ObjectType.OBJECT_CLUSTER_PAIR)) {
			final List<ITimeSeriesObject> objectSample = this.allObjects.asList().sampleObjectsOfType(ObjectType.OBJECT,
					sampleSize, seed);
			final ObjectClusterPairBuilder ocpb = new ObjectClusterPairBuilder();
			return (List<T>) objectSample.stream().map(o -> ocpb.build(o, this.objectToCluster.get(o)))
					.collect(Collectors.toList());
		}
		throw new ObjectSampleException();
	}

	@Override
	public Collection<? extends IFeatureWithValueProvider> featuresProvidedValuesFor(ObjectType<?> objectType)
			throws IncompatibleFeatureValueProviderException {
		if (objectType.equals(ObjectType.CLUSTER))
			return Arrays.asList(new ClusterFeaturePrior());
		return IClusterObjectMapping.super.featuresProvidedValuesFor(objectType);
	}

	@Override
	public void updatePrototypes()
			throws PrototypeFactoryException, CreatePrototypeInstanceFactoryException, InterruptedException {
		final Lock lock = this.clusterBuilder.getLock();
		lock.lock();
		try {
			for (ICluster c : this.clusters)
				if (!c.getObjects().isEmpty())
					c.updatePrototype(this.clusterBuilder.prototypeBuilder.setObjects(c.getObjects()).build());
		} finally {
			lock.unlock();
		}
	}
}
