/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 28, 2019
 *
 */
public class TransClustClusteringMethodBuilder extends AbstractClusteringMethodBuilder<TransClustClusteringMethod> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3394321983356355187L;

	private Double similarityThreshold;

	public TransClustClusteringMethodBuilder() {
		super();
	}

	public TransClustClusteringMethodBuilder(TransClustClusteringMethodBuilder other) {
		super(other);
		this.similarityThreshold = other.similarityThreshold;
	}

	@Override
	public TransClustClusteringMethodBuilder copy() {
		return new TransClustClusteringMethodBuilder(this);
	}

	@Override
	public TransClustClusteringMethod copy(TransClustClusteringMethod clusteringMethod) {
		return new TransClustClusteringMethod(clusteringMethod);
	}

	@Override
	public TransClustClusteringMethod doBuild() throws ClusteringMethodFactoryException,
			CreateClusteringMethodInstanceFactoryException, InterruptedException {
		try {
			return new TransClustClusteringMethod(prototypeBuilder, similarityFunction, similarityThreshold);
		} catch (IncompatibleSimilarityFunctionException e) {
			throw new CreateClusteringMethodInstanceFactoryException(e);
		}
	}

	/**
	 * @param similarityThreshold the similarityThreshold to set
	 */
	public TransClustClusteringMethodBuilder setSimilarityThreshold(Double similarityThreshold) {
		this.similarityThreshold = similarityThreshold;
		return this;
	}

	/**
	 * @return the similarityThreshold
	 */
	public Double getSimilarityThreshold() {
		return this.similarityThreshold;
	}

	@Override
	protected void reset() {
	}
}
