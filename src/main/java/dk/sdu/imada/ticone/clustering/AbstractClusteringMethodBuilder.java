/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 28, 2019
 *
 */
public abstract class AbstractClusteringMethodBuilder<CM extends AbstractClusteringMethod<? extends IClusterObjectMapping>>
		extends AbstractBuilder<CM, ClusteringMethodFactoryException, CreateClusteringMethodInstanceFactoryException>
		implements IClusteringMethodBuilder<CM> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4045934950278512904L;
	protected IPrototypeBuilder prototypeBuilder;
	protected ISimilarityFunction similarityFunction;

	public AbstractClusteringMethodBuilder() {
	}

	public AbstractClusteringMethodBuilder(AbstractClusteringMethodBuilder<CM> other) {
		super();
		this.prototypeBuilder = other.prototypeBuilder.copy();
		this.similarityFunction = other.similarityFunction;
	}

	/**
	 * @return the prototypeBuilder
	 */
	@Override
	public IPrototypeBuilder getPrototypeBuilder() {
		return this.prototypeBuilder;
	}

	/**
	 * @param prototypeBuilder the prototypeBuilder to set
	 * @return
	 */
	@Override
	public IClusteringMethodBuilder<CM> setPrototypeBuilder(IPrototypeBuilder prototypeBuilder) {
		this.prototypeBuilder = prototypeBuilder;
		return this;
	}

	/**
	 * @return the similarityFunction
	 */
	@Override
	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	/**
	 * @param similarityFunction the similarityFunction to set
	 * @return
	 */
	@Override
	public IClusteringMethodBuilder<CM> setSimilarityFunction(ISimilarityFunction similarityFunction) {
		this.similarityFunction = similarityFunction;
		return this;
	}
}
