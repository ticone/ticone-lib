/**
 * 
 */
package dk.sdu.imada.ticone.clustering.mergeclusters;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.DuplicateMappingForObjectException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair.ClusterPairsFactory;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;

/**
 * Merges clusters transitively together with a similarity larger than a
 * threshold.
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 13, 2019
 *
 */
public class MergeClustersWithSimilarityThreshold implements IMergeClusters {

	final protected ISimilarityValue threshold;

	final protected ISimilarityFunction clusterPairSimilarityFunc;

	final protected ISimilarityFunction objectClusterSimilarityFunc;

	final protected IPrototypeBuilder prototypeBuilder;

	public MergeClustersWithSimilarityThreshold(ISimilarityValue threshold,
			ISimilarityFunction clusterPairSimilarityFunc, ISimilarityFunction objectClusterSimilarityFunc,
			IPrototypeBuilder prototypeBuilder) {
		super();
		this.threshold = threshold;
		this.clusterPairSimilarityFunc = clusterPairSimilarityFunc;
		this.objectClusterSimilarityFunc = objectClusterSimilarityFunc;
		this.prototypeBuilder = prototypeBuilder;
	}

	@Override
	public <C extends IClusterObjectMapping> ClusterMergeResult<C> doMerge(C clustering)
			throws ClusterMergeException, InterruptedException {
		try {
			ICluster[] array = clustering.getClusters().toArray();
			final ISimilarityValuesAllPairs<ICluster, ICluster, IClusterPair> clusterSimilarities = this.clusterPairSimilarityFunc
					
					.calculateSimilarities(array, array, new ClusterPairsFactory(), ObjectType.CLUSTER_PAIR);

			final Map<ICluster, ClusterSet> clusterToMerged = new HashMap<>();
			for (ICluster c : clustering.getClusters()) {
				clusterToMerged.put(c, new ClusterSet(c));
			}

			for (int[] idx : clusterSimilarities.pairIndices()) {
				try {
					if (clusterSimilarities.get(idx[0], idx[1]).greaterThanOrEquals(threshold)) {
						final IClusterPair pair = clusterSimilarities.getPair(idx);
						final ClusterSet cs1 = clusterToMerged.get(pair.getFirst());
						cs1.addAll(clusterToMerged.get(pair.getSecond()));
						clusterToMerged.put(pair.getSecond(), cs1);
					}
				} catch (SimilarityValuesException e) {

				}
			}

			final Collection<IClusters> mergedClusterSets = new HashSet<>();

			for (ClusterSet clustersToMerge : clusterToMerged.values()) {
				if (clustersToMerge.size() < 2)
					continue;

				mergedClusterSets.add(clustersToMerge);

				final ITimeSeriesObjects newClusterObjects = new TimeSeriesObjectList();

				for (final ICluster cluster : clustersToMerge) {
					newClusterObjects.addAll(cluster.getObjects());
				}

				removeOldClusters(clustersToMerge, clustering);

				if (newClusterObjects.size() > 0) {
					ICluster newCluster;
					newCluster = clustering.addCluster(newClusterObjects, objectClusterSimilarityFunc);
					addMappings(newCluster, newClusterObjects, clustering);
				}
			}
			return new ClusterMergeResult<>(clustering, mergedClusterSets);
		} catch (final SimilarityCalculationException | CreateInstanceFactoryException | FactoryException
				| SimilarityValuesException | IncompatibleObjectTypeException | IncompatibleSimilarityValueException
				| DuplicateMappingForObjectException e) {
			throw new ClusterMergeException(e);
		}
	}

	private void addMappings(final ICluster cluster, final ITimeSeriesObjects objects,
			final IClusterObjectMapping clustering) throws SimilarityCalculationException, InterruptedException,
			IncompatibleObjectTypeException, DuplicateMappingForObjectException {
		for (final ITimeSeriesObject object : objects) {
			final ISimilarityValue similarity = objectClusterSimilarityFunc
					.calculateSimilarity(new ObjectClusterPair(object, cluster));
			clustering.addMapping(object, cluster, similarity);
		}
	}

	private void removeOldClusters(final IClusters clusters, final IClusterObjectMapping clustering) {
		for (final ICluster c : clusters) {
			clustering.removeData(c.getClusterNumber(),
					dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);
		}

	}

}
