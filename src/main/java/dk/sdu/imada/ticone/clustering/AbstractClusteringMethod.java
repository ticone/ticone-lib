/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 7, 2018
 *
 */
public abstract class AbstractClusteringMethod<C extends IClusterObjectMapping> implements IClusteringMethod<C> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1899183986378541980L;

	protected Logger logger;

	final IPrototypeBuilder prototypeBuilder;

	final ISimilarityFunction similarityFunction;

	ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities;

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	public AbstractClusteringMethod(final ISimilarityFunction similarityFunction,
			final IPrototypeBuilder prototypeBuilder) throws IncompatibleSimilarityFunctionException {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.similarityFunction = Objects.requireNonNull(similarityFunction);
		this.validateSimilarityFunction(this.similarityFunction);

		this.prototypeBuilder = prototypeBuilder;
	}

	AbstractClusteringMethod(final AbstractClusteringMethod<C> other) {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.similarityFunction = other.similarityFunction.copy();
		this.prototypeBuilder = other.prototypeBuilder.copy();
	}

	private final void validateSimilarityFunction(ISimilarityFunction similarityFunction)
			throws IncompatibleSimilarityFunctionException {
		if (!isValidSimilarityFunction(similarityFunction))
			throw new IncompatibleSimilarityFunctionException();
	}

	protected abstract boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction);

	/**
	 * @param similarities the similarities to set
	 */
	public void setSimilarities(
			ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities) {
		this.similarities = similarities;
	}

	/**
	 * @return the similarities
	 */
	public ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> getSimilarities() {
		return this.similarities;
	}

	@Override
	public final C findClusters(final ITimeSeriesObjects objects, final int numberOfClusters, final long seed)
			throws ClusterOperationException, InterruptedException {

		if (numberOfClusters < 1)
			throw new ClusterOperationException("The number of clusters need to be >= 1");

		if (objects.size() < numberOfClusters)
			throw new ClusterOperationException(new TooFewObjectsClusteringException(
					String.format("Cannot cluster %d objects into %d clusters.", objects.size(), numberOfClusters)));

		return doFindClusters(objects, numberOfClusters, seed);
	}

	/**
	 * When this method is called we can be sure that |clusters| >= 1 and |objects|
	 * < |clusters|.
	 * 
	 * @param objects
	 * @param numberOfClusters
	 * @param similarities
	 * @return
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 */
	protected abstract C doFindClusters(final ITimeSeriesObjects objects, final int numberOfClusters, final long seed)
			throws ClusterOperationException, InterruptedException;

	private void readObject(final ObjectInputStream stream) throws ClassNotFoundException, IOException {
		stream.defaultReadObject();
		this.logger = LoggerFactory.getLogger(this.getClass());
	}

	/**
	 * @return the similarityFunction
	 */
	@Override
	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	/**
	 * @return the prototypeBuilder
	 */
	@Override
	public IPrototypeBuilder getPrototypeBuilder() {
		return this.prototypeBuilder;
	}
}
