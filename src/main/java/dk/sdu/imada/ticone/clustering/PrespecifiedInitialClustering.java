/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 1, 2019
 *
 */
public class PrespecifiedInitialClustering extends AbstractInitialClusteringProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4582788559465066876L;

	private final ClusterObjectMapping initialClustering;

	public PrespecifiedInitialClustering(ClusterObjectMapping initialClustering) {
		super();
		this.initialClustering = initialClustering;
	}

	@Override
	public ClusterObjectMapping getInitialClustering(ITimeSeriesObjects timeSeriesDatas)
			throws ClusterOperationException, InterruptedException {
		return this.initialClustering;
	}

	@Override
	public int getInitialNumberOfClusters() {
		return initialClustering.getClusters().size();
	}
}
