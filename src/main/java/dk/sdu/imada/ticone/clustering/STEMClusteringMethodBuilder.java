/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 28, 2019
 *
 */
public class STEMClusteringMethodBuilder extends AbstractClusteringMethodBuilder<STEMClusteringMethod> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3394321983356355187L;

	public static final int DEFAULT_TIMEPOINTS = 10;

	private int timepoints;

	public STEMClusteringMethodBuilder() {
		super();
		this.timepoints = DEFAULT_TIMEPOINTS;
	}

	public STEMClusteringMethodBuilder(STEMClusteringMethodBuilder other) {
		super(other);
		this.timepoints = other.timepoints;
	}

	@Override
	public STEMClusteringMethodBuilder copy() {
		return new STEMClusteringMethodBuilder(this);
	}

	@Override
	public STEMClusteringMethod copy(STEMClusteringMethod clusteringMethod) {
		return new STEMClusteringMethod(clusteringMethod);
	}

	@Override
	public STEMClusteringMethod doBuild() throws ClusteringMethodFactoryException,
			CreateClusteringMethodInstanceFactoryException, InterruptedException {
		try {
			return new STEMClusteringMethod(similarityFunction, prototypeBuilder, timepoints);
		} catch (IncompatibleSimilarityFunctionException e) {
			throw new CreateClusteringMethodInstanceFactoryException(e);
		}
	}

	@Override
	protected void reset() {
	}

	/**
	 * @param timepoints the timepoints to set
	 */
	public STEMClusteringMethodBuilder setTimepoints(int timepoints) {
		this.timepoints = timepoints;
		return this;
	}
}
