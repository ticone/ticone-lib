/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 19, 2018
 *
 */
public class ClusterBuilder
		extends AbstractBuilder<ICluster, ClusterFactoryException, CreateClusterInstanceFactoryException>
		implements IClusterBuilder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6361747437184961233L;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.clustering == null) ? 0 : this.clustering.hashCode());
		result = prime * result + (this.counting ? 1231 : 1237);
		result = prime * result + this.nextClusterId;
		result = prime * result + ((this.prototypeBuilder == null) ? 0 : this.prototypeBuilder.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClusterBuilder other = (ClusterBuilder) obj;
		if (this.clustering == null) {
			if (other.clustering != null)
				return false;
		} else if (!this.clustering.equals(other.clustering))
			return false;
		if (this.counting != other.counting)
			return false;
		if (this.nextClusterId != other.nextClusterId)
			return false;
		if (this.prototypeBuilder == null) {
			if (other.prototypeBuilder != null)
				return false;
		} else if (!this.prototypeBuilder.equals(other.prototypeBuilder))
			return false;
		return true;
	}

	protected IClusterObjectMapping clustering;

	/**
	 * @param clustering the clustering to set
	 */
	@Override
	public ClusterBuilder setClustering(IClusterObjectMapping clustering) {
		this.clustering = clustering;
		return this;
	}

	/**
	 * @return the clustering
	 */
	public IClusterObjectMapping getClustering() {
		return this.clustering;
	}

	protected transient IPrototype prototype;

	protected IPrototypeBuilder prototypeBuilder;

	protected transient Map<ITimeSeriesObject, ISimilarityValue> objects;

	protected transient Boolean isKeep;

	protected transient Long internalClusterNumber;

	protected transient Integer clusterNumber;

	protected boolean counting = true;

	protected int nextClusterId = 1;

	protected static long globalNumberOfClusters = 0;

	@Override
	public IClusterBuilder copy() {
		final ClusterBuilder f = new ClusterBuilder();
		f.clustering = this.clustering;
		f.prototype = this.prototype;
		f.prototypeBuilder = this.prototypeBuilder.copy();
		f.objects = this.objects;
		f.isKeep = this.isKeep;
		f.clusterNumber = this.clusterNumber;
		f.nextClusterId = this.nextClusterId;
		return f;
	}

	public Cluster copy(ICluster cluster)
			throws ClusterFactoryException, CreateClusterInstanceFactoryException, InterruptedException {
		return copy(cluster, false);
	}

	/**
	 * @param cluster
	 * @param clone   If true, internal numbers are being copied as well.
	 * @return
	 * @throws ClusterFactoryException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws InterruptedException
	 */
	public Cluster copy(ICluster cluster, final boolean clone)
			throws ClusterFactoryException, CreateClusterInstanceFactoryException, InterruptedException {
		if (clone)
			internalClusterNumber = cluster.getInternalClusterId();
		if (clone)
			clusterNumber = cluster.getClusterNumber();
		isKeep = cluster.isKeep();
		objects = new HashMap<>(cluster.getSimilarities());
		if (cluster.getPrototype() != null) {
			if (prototypeBuilder != null && !prototypeBuilder.producesComparablePrototypesTo(cluster.getPrototype()))
				throw new CreateClusterInstanceFactoryException(
						"Could not copy cluster with incompatible prototype components.");
			else
				prototype = cluster.getPrototype();
		}
		Cluster build = (Cluster) build();
		return build;
	}

	@Override
	protected Cluster doBuild()
			throws CreateClusterInstanceFactoryException, ClusterFactoryException, InterruptedException {
		try {
			IPrototype p;
			if (this.prototype != null)
				p = this.prototype;
			else if (prototypeBuilder != null && this.objects != null) {
				this.prototypeBuilder.setObjects(new TimeSeriesObjectSet(this.objects.keySet()));
				p = this.prototypeBuilder.build();
			} else if (prototypeBuilder != null)
				p = this.prototypeBuilder.build();
			else
				throw new CreateClusterInstanceFactoryException(
						"Either prototype or prototype factory and objects have to be specified");

			final Cluster result = createEmptyClusterInstance();
			result.prototype = Objects.requireNonNull(p, "The prototype may not be null");
			if (this.isKeep != null)
				result.keep = isKeep;

			if (this.clusterNumber != null) {
				result.clusterNumber = clusterNumber;
			} else {
				result.clusterNumber = nextClusterId++;
			}

			if (this.internalClusterNumber != null) {
				result.internalClusterNumber = internalClusterNumber;
			} else {
				result.internalClusterNumber = ++ClusterBuilder.globalNumberOfClusters;
			}

			if (this.objects != null) {
				for (final ITimeSeriesObject o : this.objects.keySet())
					result.addObject(o, this.objects.get(o));
			}

			return result;
		} catch (FactoryException | CreateInstanceFactoryException | DuplicateMappingForObjectException e) {
			throw new CreateClusterInstanceFactoryException(e);
		}
	}

	protected Cluster createEmptyClusterInstance() {
		return new Cluster();
	}

	@Override
	protected void reset() {
		clusterNumber = null;
		internalClusterNumber = null;
		isKeep = null;
		objects = null;
		prototype = null;
	}

	@Override
	public ClusterBuilder setPrototype(final IPrototype prototype) {
		this.prototype = prototype;
		return this;
	}

	@Override
	public ClusterBuilder setPrototypeBuilder(final IPrototypeBuilder prototypeFactory) {
		this.prototypeBuilder = prototypeFactory;
		return this;
	}

	@Override
	public ClusterBuilder setIsKeep(final boolean isKeep) {
		this.isKeep = isKeep;
		return this;
	}

	@Override
	public ClusterBuilder setClusterNumber(final int clusterNumber) {
		this.clusterNumber = clusterNumber;
		return this;
	}

	/**
	 * @param internalClusterNumber the internalClusterNumber to set
	 */
	@Override
	public ClusterBuilder setInternalClusterNumber(Long internalClusterNumber) {
		this.internalClusterNumber = internalClusterNumber;
		return this;
	}

	/**
	 * @param objects the objects to set
	 * @throws ClusterFactoryException
	 */
	@Override
	public ClusterBuilder setObjects(final ITimeSeriesObjectList objects,
			final List<? extends ISimilarityValue> similarities) throws ClusterFactoryException {
		if (objects.size() != similarities.size())
			throw new ClusterFactoryException("Number of objects and similarities must be equal");
		this.objects = new HashMap<>();
		for (int i = 0; i < objects.size(); i++)
			this.objects.put(objects.get(i), similarities.get(i));
		return this;
	}

	public static long getGlobalNumberOfClusters() {
		return globalNumberOfClusters;
	}

	public static void setGlobalNumberOfClusters(final long globalNumberOfClusters) {
		ClusterBuilder.globalNumberOfClusters = globalNumberOfClusters;
	}

	public void resetClusterCount() {
		resetClusterCount(0);
	}

	public void resetClusterCount(final int clusterCount) {
		nextClusterId = clusterCount;
	}

	public int getClusterCount() {
		return nextClusterId;
	}

	public boolean stopCounting() {
		final boolean result = counting;
		counting = false;
		return result;
	}

	public void startCounting() {
		counting = true;
	}

	private transient Lock lock = new ReentrantLock();

	/**
	 * @return the lock
	 */
	public Lock getLock() {
		return lock;
	}

}