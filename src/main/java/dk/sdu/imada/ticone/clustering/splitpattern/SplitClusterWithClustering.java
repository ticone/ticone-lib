package dk.sdu.imada.ticone.clustering.splitpattern;

import dk.sdu.imada.ticone.clustering.ClusterFactoryException;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterOperationException;
import dk.sdu.imada.ticone.clustering.ClusteringMethodFactoryException;
import dk.sdu.imada.ticone.clustering.CreateClusterInstanceFactoryException;
import dk.sdu.imada.ticone.clustering.CreateClusteringMethodInstanceFactoryException;
import dk.sdu.imada.ticone.clustering.DuplicateMappingForObjectException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * This class is used to split a given pattern, using
 * <tt>InitialClusteringInterface</tt>. It takes the time series data assigned
 * to the given data, and reclusters them, using
 * <tt>InitialClusteringInterface</tt>.
 *
 * @author Christian Nørskov
 * @author Christian Wiwie
 * @see IClusteringMethod
 */
public class SplitClusterWithClustering extends AbstractSplitCluster {

	private final int numberOfClusters;
	private final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod;
	private long seed;

	/**
	 *
	 * @param numberOfClusters
	 * @param clusteringMethod
	 * @param seed                 TODO
	 * @param patternObjectMapping
	 */
	public SplitClusterWithClustering(final int numberOfClusters,
			final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod,
			final long seed) {
		super();
		this.clusteringMethod = clusteringMethod;
		this.numberOfClusters = numberOfClusters;
		this.seed = seed;
	}

	@Override
	protected SplitClusterContainer splitClusterInternal(final ICluster pattern)
			throws SplitClusterException, InterruptedException {

		final ITimeSeriesObjects timeSeriesDataArrayList = pattern.getObjects();

		// patternObjectMapping.deleteData(pattern,
		// IClusterObjectMapping.DELETE_BOTH_PATTERN_AND_OBJECTS);

		ClusterObjectMapping pom;
		try {
			pom = this.clusteringMethod.build().findClusters(timeSeriesDataArrayList, this.numberOfClusters, seed);
		} catch (final ClusterOperationException | ClusteringMethodFactoryException
				| CreateClusteringMethodInstanceFactoryException e) {
			throw new SplitClusterException(e);
		}

		// Here there are no candidates, so it is parsed as null
		final SplitClusterContainer splitPatternContainer = new SplitClusterContainer(pattern, pom, null);
		// patternObjectMapping.mergeMappings(pom);

		this.setPatternParent(splitPatternContainer);

		return splitPatternContainer;
	}

	private void setPatternParent(final ISplitClusterContainer splitPatternContainer) {
		final ICluster parent = splitPatternContainer.getOldPattern();

		int count = 1;
		for (final ICluster child : splitPatternContainer.getNewClusters().getClusters()) {
			child.setParent(parent);
			count++;
		}
	}

	@Override
	public IClusterObjectMapping applyNewClusters(final ISplitClusterContainer splitPatternContainer)
			throws SplitClusterException {
		try {
			final ICluster pattern = splitPatternContainer.getOldPattern();

			this.clustering.removeData(pattern.getClusterNumber(),
					dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);

			final IClusterObjectMapping newPatterns = splitPatternContainer.getNewClusters();

			this.clustering.mergeMappings(newPatterns);

			return this.clustering;
		} catch (CreateClusterInstanceFactoryException | InterruptedException | ClusterFactoryException
				| DuplicateMappingForObjectException e) {
			throw new SplitClusterException(e);
		}
	}

}
