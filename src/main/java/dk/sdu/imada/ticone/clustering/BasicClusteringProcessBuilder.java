/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wiwie.wiutils.utils.StringExt;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.mergeclusters.ClusterMergeException;
import dk.sdu.imada.ticone.clustering.mergeclusters.ClusterMergeResult;
import dk.sdu.imada.ticone.clustering.mergeclusters.MergeClustersWithSimilarityThreshold;
import dk.sdu.imada.ticone.clustering.mergeclusters.MergeSelectedClusters;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.clustering.splitpattern.AbstractSplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitClusterContainer;
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterException;
import dk.sdu.imada.ticone.clustering.suggestclusters.ClusterSuggestion;
import dk.sdu.imada.ticone.clustering.suggestclusters.IClusterSuggestion;
import dk.sdu.imada.ticone.clustering.suggestclusters.ISuggestNewCluster;
import dk.sdu.imada.ticone.clustering.suggestclusters.SuggestClusterException;
import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ShuffleTimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeaturePrototypeStandardVariance;
import dk.sdu.imada.ticone.feature.ClusteringFeatureTotalObjectClusterSimilarity;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.SimilarityFeature;
import dk.sdu.imada.ticone.feature.scale.IScalerBuilder;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.io.LoadDataException;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkEdge;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.preprocessing.AbstractTimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingException;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ITimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IWeightedTimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.similarity.TimeSeriesNotCompatibleException;
import dk.sdu.imada.ticone.statistics.ICalculatePValues;
import dk.sdu.imada.ticone.statistics.IPValueCalculationListener;
import dk.sdu.imada.ticone.statistics.IPValueCalculationResult;
import dk.sdu.imada.ticone.statistics.PValueCalculationEvent;
import dk.sdu.imada.ticone.statistics.PValueCalculationException;
import dk.sdu.imada.ticone.statistics.PermutationTestChangeEvent;
import dk.sdu.imada.ticone.util.ActionContainer;
import dk.sdu.imada.ticone.util.ClusterHistory;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IActionContainer;
import dk.sdu.imada.ticone.util.IBuilder;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.ITimePointWeighting;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.Iterables;
import dk.sdu.imada.ticone.util.MyParallel;
import dk.sdu.imada.ticone.util.MyParallel.BatchCount;
import dk.sdu.imada.ticone.util.MyParallel.Operation;
import dk.sdu.imada.ticone.util.MyScheduledThreadPoolExecutor;
import dk.sdu.imada.ticone.util.Progress;
import dk.sdu.imada.ticone.util.Utility;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public class BasicClusteringProcessBuilder<R extends ITiconeClusteringResult>
		implements IClusteringProcessBuilder<ClusterObjectMapping, R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6027212521697630968L;
	protected IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder;
	protected IIdMapMethod idMapMethod;
	protected IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider;
	protected ILoadDataMethod loadDataMethod;
	protected IPreprocessingSummary preprocessingSummary;
	protected IPrototypeBuilder prototypeBuilder;
	protected long seed;
	protected ISimilarityFunction similarityFunction;
	protected ITimePointWeighting timePointWeighting;
	private ITimeSeriesPreprocessor timeSeriesPreprocessor;
	private ITiconeClusteringResultFactory<ClusterObjectMapping, R> resultFactory;
	private Collection<IFeature<?>> requestedFeatures;

	/**
	 * @param requestedFeatures the requestedFeatures to set
	 */
	public void setRequestedFeatures(Collection<IFeature<?>> requestedFeatures) {
		this.requestedFeatures = requestedFeatures;
	}

	/**
	 * @return the requestedFeatures
	 */
	public Collection<IFeature<?>> getRequestedFeatures() {
		return this.requestedFeatures;
	}

	@Override
	public void setResultFactory(ITiconeClusteringResultFactory<ClusterObjectMapping, R> resultFactory) {
		this.resultFactory = resultFactory;
	}

	/**
	 * @return the resultFactory
	 */
	@Override
	public ITiconeClusteringResultFactory<ClusterObjectMapping, R> getResultFactory() {
		return this.resultFactory;
	}

	@Override
	public void setClusteringMethodBuilder(
			final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> initialClusteringInterface) {
		this.clusteringMethodBuilder = initialClusteringInterface;
	}

	@Override
	public void setIdMapMethod(final IIdMapMethod idMapMethod) {
		this.idMapMethod = idMapMethod;
	}

	/**
	 * @param initialClusteringProvider the initialClusteringProvider to set
	 */
	@Override
	public void setInitialClusteringProvider(
			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider) {
		this.initialClusteringProvider = initialClusteringProvider;
	}

	@Override
	public void setLoadDataMethod(final ILoadDataMethod loadDataMethod) {
		this.loadDataMethod = loadDataMethod;
	}

	@Override
	public void setPreprocessingSummary(final IPreprocessingSummary preprocessingSummary) {
		this.preprocessingSummary = preprocessingSummary;
	}

	/**
	 * @param prototypeFactory the prototypeFactory to set
	 */
	@Override
	public void setPrototypeBuilder(final IPrototypeBuilder prototypeFactory) {
		this.prototypeBuilder = prototypeFactory;
	}

	/**
	 * @param seed the random to set
	 */
	@Override
	public void setSeed(long seed) {
		this.seed = seed;
	}

	protected void validateSimilarityFunction(final ISimilarityFunction similarityFunction)
			throws IncompatibleSimilarityFunctionException {
		if (!isValidSimilarityFunction(similarityFunction))
			throw new IncompatibleSimilarityFunctionException();
	}

	protected boolean isValidSimilarityFunction(final ISimilarityFunction similarityFunction) {
		return similarityFunction.providesValuesForObjectTypes().containsAll(
				Arrays.asList(ObjectType.OBJECT_CLUSTER_PAIR, ObjectType.OBJECT_PAIR, ObjectType.CLUSTER_PAIR));
	}

	@Override
	public void setSimilarityFunction(final ISimilarityFunction similarityFunction)
			throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException,
			IncompatibleSimilarityFunctionException {
		this.validateSimilarityFunction(similarityFunction);
		this.similarityFunction = similarityFunction;

		final IAggregateCluster<ITimeSeries> aggregationFunction = PrototypeComponentType.TIME_SERIES
				.getComponentFactory(this.prototypeBuilder).getAggregationFunction();
		if (aggregationFunction != null && aggregationFunction instanceof AggregateClusterMedoidTimeSeries)
			((AggregateClusterMedoidTimeSeries) aggregationFunction)
					.setSimilarityFunction((ISimilarityFunction) similarityFunction);
		//
		// if (this.clusterPValueCalculationClusterFeatures != null) {
		// for (INumberFeature<ICluster> f :
		// this.clusterPValueCalculationClusterFeatures) {
		// if (f instanceof ClusterFeatureAverageSimilarity)
		// ((ClusterFeatureAverageSimilarity)
		// f).setSimilarityFunction(similarityFunction);
		// }
		// }

		if (this.similarityFunction instanceof IWeightedTimeSeriesSimilarityFunction)
			this.timePointWeighting = ((IWeightedTimeSeriesSimilarityFunction) this.similarityFunction)
					.getTimePointWeights();
		else if (this.similarityFunction instanceof ICompositeSimilarityFunction) {
			for (final ISimilarityFunction childSimFunc : ((ICompositeSimilarityFunction) this.similarityFunction)
					.getSimilarityFunctions()) {
				if (childSimFunc instanceof IWeightedTimeSeriesSimilarityFunction) {
					this.timePointWeighting = ((IWeightedTimeSeriesSimilarityFunction) childSimFunc)
							.getTimePointWeights();
					break;
				}
			}
		}
	}

	@Override
	public void setTimePointWeighting(final ITimePointWeighting timePointWeighting) {
		this.timePointWeighting = timePointWeighting;
	}

	@Override
	public void setTimeSeriesPreprocessor(final ITimeSeriesPreprocessor timeSeriesPreprocessor) {
		this.timeSeriesPreprocessor = timeSeriesPreprocessor;
	}

	@Override
	public IBuilder<IClusteringProcess<ClusterObjectMapping, R>, ClusterProcessFactoryException, CreateClusterProcessInstanceFactoryException> copy() {
		return new BasicClusteringProcessBuilder<>();
	}

	public IClusteringProcess<ClusterObjectMapping, R> build(
			final ITiconeClusteringResultFactory<ClusterObjectMapping, R> resultFactory, final IIdMapMethod idMapMethod,
			final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder,
			final IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider,
			final ILoadDataMethod loadDataMethod, final IPreprocessingSummary preprocessingSummary,
			final IPrototypeBuilder prototypeBuilder, final ISimilarityFunction similarityFunction,
			final ITimePointWeighting timePointWeighting, final ITimeSeriesPreprocessor timeSeriesPreprocessor,
			final long seed, final Collection<IFeature<?>> requestedFeatures)
			throws ClusterProcessFactoryException, CreateClusterProcessInstanceFactoryException, InterruptedException {
		BasicClusteringProcess basicClusteringProcess;
		try {
			if (!loadDataMethod.isDataLoaded()) {
				loadDataMethod.loadData(timeSeriesPreprocessor);
				timeSeriesPreprocessor.process();
			}
			basicClusteringProcess = new BasicClusteringProcess(resultFactory.copy(), idMapMethod.copy(),
					clusteringMethodBuilder, initialClusteringProvider, loadDataMethod.copy(),
					preprocessingSummary.copy(), prototypeBuilder.copy(),
					(ISimilarityFunction) similarityFunction.copy(), timePointWeighting.copy(), timeSeriesPreprocessor,
					seed);
		} catch (IncompatibleSimilarityFunctionException | LoadDataException | PreprocessingException e) {
			throw new CreateClusterProcessInstanceFactoryException(e);
		}
		requestedFeatures.forEach(f -> basicClusteringProcess.requestFeatureCalculation(f.copy()));
		return basicClusteringProcess;
	}

	@Override
	public IClusteringProcess<ClusterObjectMapping, R> build()
			throws ClusterProcessFactoryException, CreateClusterProcessInstanceFactoryException, InterruptedException {
		BasicClusteringProcess basicClusteringProcess;
		try {
			if (!loadDataMethod.isDataLoaded()) {
				loadDataMethod.loadData(timeSeriesPreprocessor);
				timeSeriesPreprocessor.process();
			}
			basicClusteringProcess = new BasicClusteringProcess(resultFactory.copy(), idMapMethod.copy(),
					clusteringMethodBuilder.copy(), initialClusteringProvider, loadDataMethod.copy(),
					preprocessingSummary.copy(), prototypeBuilder.copy(),
					(ISimilarityFunction) similarityFunction.copy(), timePointWeighting.copy(), timeSeriesPreprocessor,
					seed);
		} catch (IncompatibleSimilarityFunctionException | LoadDataException | PreprocessingException e) {
			throw new CreateClusterProcessInstanceFactoryException(e);
		}
		this.requestedFeatures.forEach(f -> basicClusteringProcess.requestFeatureCalculation(f.copy()));
		return basicClusteringProcess;
	}

	@Override
	public IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> getClusteringMethodBuilder() {
		return this.clusteringMethodBuilder;
	}

	@Override
	public IIdMapMethod getIdMapMethod() {
		return this.idMapMethod;
	}

	/**
	 * @return the initialClusteringProvider
	 */
	@Override
	public IInitialClusteringProvider<ClusterObjectMapping> getInitialClusteringProvider() {
		return this.initialClusteringProvider;
	}

	@Override
	public ILoadDataMethod getLoadDataMethod() {
		return this.loadDataMethod;
	}

	@Override
	public IPreprocessingSummary getPreprocessingSummary() {
		return this.preprocessingSummary;
	}

	/**
	 * @return the prototypeFactory
	 */
	@Override
	public IPrototypeBuilder getPrototypeBuilder() {
		return this.prototypeBuilder;
	}

	/**
	 * @return the random
	 */
	@Override
	public long getSeed() {
		return this.seed;
	}

	/**
	 * @return the simFunc
	 */
	@Override
	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	@Override
	public ITimePointWeighting getTimePointWeighting() {
		return this.timePointWeighting;
	}

	@Override
	public ITimeSeriesPreprocessor getTimeSeriesPreprocessor() {
		return this.timeSeriesPreprocessor;
	}

	/**
	 * Returns the number of objects in the current dataset. Returns -1 if it is not
	 * set.
	 * 
	 * @return Return the number of objects in the current dataset. Returns -1 if it
	 *         is not set.
	 */
	@Override
	public int getNumberOfObjectsInDataset() {
		if (this.timeSeriesPreprocessor != null) {
			return this.timeSeriesPreprocessor.getObjects().size();
		}
		return -1;
	}
}

/**
 * This class represents an iterative process of deriving a clustering. The
 * current state of the process is stored in a {@link TiconeClusteringResult}
 * instance.
 *
 *
 *
 * @see IClusteringProcess
 * @see ITimeSeriesObject
 * @see AbstractTimeSeriesPreprocessor
 * @see ITimeSeriesSimilarityFunction
 * @see IShuffleDataset
 * @see IDiscretizeTimeSeries
 * @see IClusteringMethod
 */
class BasicClusteringProcess<R extends TiconeClusteringResult> implements IClusteringProcess<ClusterObjectMapping, R> {

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Mar 16, 2019
	 *
	 */
	private final class RefinePrototypesOp implements Operation<Iterable<ICluster>, List<IPrototype>> {
		@Override
		public List<IPrototype> perform(final Iterable<ICluster> batch)
				throws SimilarityCalculationException, InterruptedException, ClusterOperationException {

			final IPrototypeBuilder prototypeFactory = BasicClusteringProcess.this.clusteringResult.prototypeBuilder
					.copy();

			final List<IPrototype> result = new ArrayList<>();
			for (final ICluster p : batch) {
				final IPrototype refinedPrototype;
				if (p.isKeep()) {
					refinedPrototype = p.getPrototype();
				} else {
					try {
						refinedPrototype = prototypeFactory.setObjects(p.getObjects()).build();
					} catch (CreatePrototypeInstanceFactoryException | PrototypeFactoryException e) {
						throw new ClusterOperationException(e);
					}
				}
				result.add(refinedPrototype);
			}

			return result;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3097256895592030112L;

	public static int getNextClusteringNumber() {
		return nextClusteringNumber;
	}

	public static void setNextClusteringNumber(final int nextClusteringNumber) {
		BasicClusteringProcess.nextClusteringNumber = nextClusteringNumber;
	}

	protected static int nextClusteringNumber = 1;

	private transient Logger logger = LoggerFactory.getLogger(this.getClass());

	private final ITiconeClusteringResultFactory<ClusterObjectMapping, R> resultFactory;
	private final IIdMapMethod idMapMethod;
	private final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder;
	private final IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider;
	private final ILoadDataMethod loadDataMethod;
	private final IPreprocessingSummary<ClusterObjectMapping> preprocessingSummary;
	private final IPrototypeBuilder prototypeBuilder;
	private final ISimilarityFunction similarityFunction;
	private final ITimePointWeighting timePointWeighting;
	private final ITimeSeriesPreprocessor timeSeriesPreprocessor;
	private final long seed;
	private transient List<IActionContainer> actionsToApplyBeforeNextIteration;
	private boolean initialized = false;
	private Progress progress;
	private transient ScheduledThreadPoolExecutor privateThreadPool;
	private final Map<ObjectType<?>, Set<IFeature<?>>> requestedFeatures;
	private IFeature<ISimilarityValue> averageSimilarityFeature;
	private R clusteringResult;
	private int numberOfTimePoints;
	private Set<String> unmappedNodes;

	private transient Set<IPValueCalculationListener> pvalueListener;

	/**
	 * This function initializes all the objects needed, before <tt>doIteration</tt>
	 * can be called.
	 * 
	 * @param timeSeriesData             The time series data, with calculated
	 *                                   patterns
	 * @param simFunc                    The similarity function which should be
	 *                                   used
	 * @param permutationFunction        The function to permutate the dataset with,
	 *                                   for p-value calculation.
	 * @param numberOfPermutations       The number of
	 *                                   clusteringPermutationIterations used for
	 *                                   calculating the p-values of the patterns
	 * @param numberOfInitialClusters    The number of initial patterns after first
	 *                                   iteration.
	 * @param discretizePatternFunction  The class used to discretize the patterns
	 * @param initialClusteringInterface The class used to cluster the time series
	 *                                   objects in the first iteration
	 * @throws TimeSeriesNotCompatibleException
	 * @throws InterruptedException
	 * @throws IncompatibleSimilarityFunctionException
	 * @see AbstractTimeSeriesPreprocessor
	 * @see ITimeSeriesSimilarityFunction
	 * @see IShuffleDataset
	 * @see IDiscretizeTimeSeries
	 * @see IClusteringMethod
	 */
	protected BasicClusteringProcess(final ITiconeClusteringResultFactory<ClusterObjectMapping, R> resultFactory,
			final IIdMapMethod idMapMethod,
			final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder,
			final IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider,
			final ILoadDataMethod loadDataMethod, final IPreprocessingSummary preprocessingSummary,
			final IPrototypeBuilder prototypeBuilder, final ISimilarityFunction similarityFunction,
			final ITimePointWeighting timePointWeighting, final ITimeSeriesPreprocessor timeSeriesPreprocessor,
			final long seed) throws InterruptedException, IncompatibleSimilarityFunctionException {
		super();

		this.resultFactory = resultFactory;
		this.idMapMethod = idMapMethod;
		this.clusteringMethodBuilder = clusteringMethodBuilder;
		this.initialClusteringProvider = initialClusteringProvider;
		this.loadDataMethod = loadDataMethod;
		this.preprocessingSummary = preprocessingSummary;
		this.prototypeBuilder = prototypeBuilder;
		this.similarityFunction = similarityFunction;
		this.timePointWeighting = timePointWeighting;
		this.timeSeriesPreprocessor = timeSeriesPreprocessor;
		this.seed = seed;

		this.pvalueListener = new HashSet<>();

		this.progress = Utility.getProgress();
		this.actionsToApplyBeforeNextIteration = new ArrayList<>();
		this.requestedFeatures = new HashMap<>();
		this.initRequestedFeatures();

	}

	@Override
	public boolean start() throws InterruptedException, ClusterOperationException {
		try {
			this.validateStart();
			this.averageSimilarityFeature = new ClusteringFeatureTotalObjectClusterSimilarity(getSimilarityFunction());
			this.initClusteringResult();
			clusteringResult.setClusteringProcess(this);

			this.initThreadPool();
			this.initialized = true;
			return this.initialized;
		} catch (NullPointerException | IllegalArgumentException | FactoryException | CreateInstanceFactoryException
				| IncompatibleSimilarityFunctionException e) {
			throw new ClusterOperationException(e);
		}
	}

	private void initClusteringResult() throws FactoryException, CreateInstanceFactoryException, InterruptedException {
		this.clusteringResult = this.resultFactory.createInstance(seed, loadDataMethod, initialClusteringProvider,
				numberOfTimePoints, timePointWeighting, idMapMethod, preprocessingSummary, similarityFunction,
				clusteringMethodBuilder, timeSeriesPreprocessor, prototypeBuilder);
	}

	@Override
	public boolean isStarted() {
		return this.clusteringResult != null;
	}

	protected void validateStart() throws IllegalArgumentException {
		Objects.requireNonNull(timeSeriesPreprocessor, "timeSeriesPreprocessor");
		Objects.requireNonNull(timeSeriesPreprocessor.getObjects(), "timeSeriesPreprocessor.getObjects()");
		Objects.requireNonNull(similarityFunction, "similarityFunction");
		Objects.requireNonNull(initialClusteringProvider, "initialClusteringProvider");
		Objects.requireNonNull(prototypeBuilder, "prototypeBuilder");
		if (initialClusteringProvider.getInitialNumberOfClusters() < 1)
			throw new IllegalArgumentException();
	}

	private void initRequestedFeatures() throws IncompatibleSimilarityFunctionException {
		final ClusteringFeatureTotalObjectClusterSimilarity f = new ClusteringFeatureTotalObjectClusterSimilarity(
				this.getSimilarityFunction());
		f.setUseCache(false);
		this.requestFeatureCalculation(f);

		final ClusterFeatureAverageSimilarity f2 = new ClusterFeatureAverageSimilarity(this.getSimilarityFunction());
		f2.setUseCache(false);
		this.requestFeatureCalculation(f);

		if (prototypeBuilder.getPrototypeComponentFactories().containsKey(PrototypeComponentType.TIME_SERIES)) {
			final ClusterFeaturePrototypeStandardVariance cfVar = new ClusterFeaturePrototypeStandardVariance();
			cfVar.setUseCache(false);
			this.requestFeatureCalculation(cfVar);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.clusteringResult == null) ? 0 : this.clusteringResult.hashCode());
		result = prime * result + (this.initialized ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicClusteringProcess other = (BasicClusteringProcess) obj;
		if (this.clusteringResult == null) {
			if (other.clusteringResult != null)
				return false;
		} else if (!this.clusteringResult.equals(other.clusteringResult))
			return false;
		if (this.initialized != other.initialized)
			return false;
		return true;
	}

	private void initThreadPool() {
		this.privateThreadPool = new MyScheduledThreadPoolExecutor(MyParallel.DEFAULT_THREADS,
				new PrivateThreadFactory());
	}

	/**
	 * @return the simFunc
	 */
	@Override
	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	/**
	 * This function returns the observable object, which will update the progress
	 * of the program.
	 * 
	 * @return returns the progress, which is an observable object.
	 */
	@Override
	public Progress getProgress() {
		return this.progress;
	}

	@Override
	public ICluster addCluster(final IPrototype prototype) throws ClusterOperationException, InterruptedException {
		try {
			if (!prototype.getPrototypeComponentTypes()
					.equals(this.prototypeBuilder.getPrototypeComponentFactories().keySet()))
				throw new ClusterOperationException(new IncompatiblePrototypeException());

			final ICluster cluster = this.clusteringResult.temporaryClustering.addCluster(prototype);
			cluster.setKeep(true);
			this.saveHistory("Added predefined cluster.");
			return cluster;
		} catch (FeatureCalculationException | IncorrectlyInitializedException | IncompatibleFeatureAndObjectException
				| ClusterFactoryException | CreateClusterInstanceFactoryException | IncompatiblePrototypeException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		}
	}

	/**
	 * This functions uses the given SplitPatternInterface object to split the given
	 * pattern, and returns a container for the old pattern and the newly found
	 * patterns.
	 * 
	 * @param splitCluster
	 * @param cluster      the pattern which should be split up.
	 * @return
	 * @throws SplitClusterException
	 * @throws TooFewObjectsClusteringException
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 * @see ISplitClusterContainer
	 * @see AbstractSplitCluster
	 * @see ICluster
	 *
	 */
	@Override
	public ISplitClusterContainer splitCluster(final ISplitCluster splitCluster, final int clusterNumber)
			throws SplitClusterException, TooFewObjectsClusteringException, ClusterOperationException,
			InterruptedException {
		splitCluster.setClustering(getTemporaryClustering());
		return splitCluster.splitCluster(getTemporaryClustering().clusters.get(clusterNumber));
	}

	/**
	 * This function applies the splitted patterns found with the
	 * SplitPatternInterface object.
	 * 
	 * @param splitCluster
	 * @param splitClusterContainer
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 * @see AbstractSplitCluster
	 * @see ISplitClusterContainer
	 */
	@Override
	public void applySplitClusters(final ISplitCluster splitCluster, final ISplitClusterContainer splitClusterContainer)
			throws ClusterOperationException, InterruptedException {
		try {
			splitCluster.applyNewClusters(splitClusterContainer);
			this.saveHistory("Splitting cluster.");
		} catch (final InterruptedException ie) {
			this.resetToLastSavedClustering();
			throw ie;
		} catch (FeatureCalculationException | IncorrectlyInitializedException | SplitClusterException
				| IncompatibleFeatureAndObjectException e) {
			new ClusterOperationException(e);
		}
	}

	private void resetToLastSavedClustering() {
		try {
			this.updateClusterObjectMapping(this.clusteringResult.getClusterHistory().getClusterObjectMapping());
		} catch (Exception e) {
		}
	}

	/**
	 * Returns the current state of the program.
	 * 
	 * @return returns the current state of the program in the form of
	 *         <tt>PatterHistory</tt>
	 * @see ClusterHistory
	 */
	@Override
	public IClusterHistory<ClusterObjectMapping> getHistory() {
		return this.clusteringResult.getClusterHistory();
	}

	@Override
	public void appendHistory(IClusterHistory<ClusterObjectMapping> history) {
		this.appendHistory(history);
	}

	@Override
	public void resetToIteration(final int iteration) throws InterruptedException {
		this.clusteringResult.resetToIteration(iteration);
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		this.privateThreadPool = null;
//		if (this.privateThreadPool != null)
//			this.privateThreadPool.shutdown();
	}

	/**
	 * This function uses SuggestNewPatternInterface to suggest new patterns based
	 * on the current clustering.
	 * 
	 * @param ISuggestNewPattern a class that implements the
	 *                           suggestNewPatternInterface, which suggests new
	 *                           patterns.
	 * @return
	 * @throws SuggestClusterException
	 * @throws TooFewObjectsClusteringException
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 * @see ISuggestNewCluster
	 * @see ClusterSuggestion
	 */
	@Override
	public IClusterSuggestion suggestNewClusters(final ISuggestNewCluster ISuggestNewPattern)
			throws SuggestClusterException, TooFewObjectsClusteringException, ClusterOperationException,
			InterruptedException {
		return ISuggestNewPattern.suggestNewClusters(this.clusteringResult.temporaryClustering);
	}

	/**
	 *
	 * @param suggestCluster
	 * @param clusterSuggestion
	 * @return
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 * @see ISuggestNewCluster
	 * @see ClusterSuggestion
	 * @see ClusterObjectMapping
	 */
	@Override
	public void applySuggestedClusters(final ISuggestNewCluster suggestCluster,
			final IClusterSuggestion clusterSuggestion) throws ClusterOperationException, InterruptedException {
		try {
			suggestCluster.applyNewClusters(clusterSuggestion);
			this.saveHistory("Added suggested clusters.");
		} catch (final InterruptedException ie) {
			this.resetToLastSavedClustering();
			throw ie;
		} catch (SuggestClusterException | IncorrectlyInitializedException | FeatureCalculationException
				| IncompatibleFeatureAndObjectException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		}
	}

	/**
	 * This function goes through an iteration of clustering; if it is the first
	 * iteration the initial clustering will happen, otherwise the
	 * expectation-maximization clustering will happen.
	 * 
	 * @return returns the pattern object mapping of the clustering.
	 * @throws TooFewObjectsClusteringException
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 * @see ClusterObjectMapping
	 * @see ICluster
	 */
	@Override
	public ClusterObjectMapping doIteration()
			throws TooFewObjectsClusteringException, ClusterOperationException, InterruptedException {
		if (!this.initialized) {
			throw new ClusterOperationException(
					"This clustering process hasn't been initialized. Did you forget to invoke start()?");
		}
		this.progress.start();
		final long time = System.currentTimeMillis();
		if (this.isFirstIteration()) {
			this.firstIteration();
		} else {
			this.followingIterations();
		}
		final long totalTime = (System.currentTimeMillis() - time) / 1000;
		logger.info(
				String.format("One complete iteration: %ds %dm %dh", totalTime, totalTime / 60, totalTime / 60 / 60));

		// Save the current state of the program
		this.progress.updateProgress(0.99, "Storing clustering");
		return this.getLatestClustering();
	}

	/**
	 * Function to delete all objects belonging to the specified pattern, or just
	 * the pattern itself, or both.
	 * 
	 * @param pattern      the pattern to either delete or delete objects from
	 * @param deleteMethod the given deleteMethod, using the enum DELETE_METHOD from
	 *                     <tt>ClusterObjectMapping</tt>, and can take the values:
	 *                     <tt>BOTH_PATTERN_AND_OBJECTS</tt>, <tt>ONLY_OBJECTS</tt>,
	 *                     or <tt>ONLY_PATTERN</tt>
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 * @see ClusterObjectMapping
	 * @see ICluster
	 */
	@Override
	public void deleteData(final int clusterNumber, final ClusterObjectMapping.DELETE_METHOD deleteMethod)
			throws ClusterOperationException, InterruptedException {
		try {
			String note = "";
			switch (deleteMethod) {
			case BOTH_PROTOTYPE_AND_OBJECTS:
				this.clusteringResult.temporaryObjectsToBeClustered
						.removeAll(this.clusteringResult.temporaryClustering.getCluster(clusterNumber).getObjects());
				this.clusteringResult.temporaryClustering.removeData(clusterNumber, deleteMethod);
				note = "Deleted prototype and objects.";
				break;
			case ONLY_OBJECTS:
				note = "Deleted objects";
				this.clusteringResult.temporaryObjectsToBeClustered
						.removeAll(this.clusteringResult.temporaryClustering.getCluster(clusterNumber).getObjects());
				this.clusteringResult.temporaryClustering.removeData(clusterNumber, deleteMethod);
				break;
			case ONLY_PROTOTYPE:
				note = "Deleted prototype";
				this.clusteringResult.temporaryClustering.removeData(clusterNumber, deleteMethod);
				break;
			}
			this.saveHistory(note);
		} catch (FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleFeatureAndObjectException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		} catch (final InterruptedException ie) {
			this.resetToLastSavedClustering();
			throw ie;
		}
	}

	@Override
	public void removeObjectsFromClusters(final ITimeSeriesObjects objects, final int... clusterNumberArray)
			throws ClusterOperationException, InterruptedException {
		try {
			final boolean isAllClusters;
			final IntSet clusterNumbers;
			if (clusterNumberArray.length > 0) {
				clusterNumbers = new IntOpenHashSet(clusterNumberArray);
				isAllClusters = false;
			} else {
				clusterNumbers = this.clusteringResult.temporaryClustering.clusters.getClusterNumbers();
				isAllClusters = true;
			}
			for (final int clusterNumber : clusterNumbers)
				this.clusteringResult.temporaryClustering.removeObjectsFromCluster(
						this.clusteringResult.temporaryClustering.clusters.get(clusterNumber), objects);
			if (isAllClusters)
				this.saveHistory("Deleted objects from all clusters");
			else if (clusterNumbers.size() == 1)
				this.saveHistory("Deleted objects from cluster #" + clusterNumbers.iterator().nextInt());
			else if (clusterNumbers.size() > 1)
				this.saveHistory("Deleted objects from clusters #" + StringExt.paste(", ", clusterNumbers));
		} catch (FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleFeatureAndObjectException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		}
	}

	/**
	 * Creates a new pattern with the timeseries objects assigned to the given
	 * patterns.
	 * 
	 * @param patternsToMerge the patterns that should be merged.
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 */
	@Override
	public void mergeClusters(final int... clusterNumbersToMerge)
			throws ClusterOperationException, InterruptedException {
		try {
			final IClusterSet patternsToMerge = Arrays.stream(clusterNumbersToMerge)
					.mapToObj(n -> this.clusteringResult.temporaryClustering.clusters.get(n))
					.collect(Collectors.toCollection(ClusterSet::new));
			new MergeSelectedClusters(patternsToMerge, this.getSimilarityFunction())
					.doMerge(this.clusteringResult.temporaryClustering);

			final StringBuilder sb = new StringBuilder("Merge clusters ");
			for (final ICluster c : patternsToMerge) {
				sb.append("'");
				sb.append(c.getName());
				sb.append("',");
			}
			sb.deleteCharAt(sb.length() - 1);

			this.saveHistory(sb.toString());
		} catch (final InterruptedException ie) {
			this.resetToLastSavedClustering();
			throw ie;
		} catch (ClusterMergeException | FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleFeatureAndObjectException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		}
	}

	@Override
	public void mergeClusters(ISimilarityValue threshold) throws ClusterOperationException, InterruptedException {
		try {
			final ClusterMergeResult<ClusterObjectMapping> mergeResult = new MergeClustersWithSimilarityThreshold(
					threshold, getSimilarityFunction(), getSimilarityFunction(), this.clusteringResult.prototypeBuilder)
							.doMerge(this.clusteringResult.temporaryClustering);

			final StringBuilder sb = new StringBuilder("Merge clusters ");
			for (final IClusters cs : mergeResult.getMergedClusterSets()) {
				for (final ICluster c : cs)
					sb.append(String.format("%d, ", c.getClusterNumber()));
				sb.deleteCharAt(sb.length() - 1);
				sb.append(" & ");
			}
			if (mergeResult.getMergedClusterSets().size() > 0) {
				sb.deleteCharAt(sb.length() - 1);
				sb.deleteCharAt(sb.length() - 1);
				sb.deleteCharAt(sb.length() - 1);
			}

			this.saveHistory(sb.toString());
		} catch (final InterruptedException ie) {
			this.resetToLastSavedClustering();
			throw ie;
		} catch (ClusterMergeException | FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleFeatureAndObjectException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		}
	}

	@Override
	public void createClusterWithObjects(final ITimeSeriesObjects objects)
			throws ClusterOperationException, InterruptedException {
		try {
			for (ICluster cluster : this.clusteringResult.temporaryClustering.clusters)
				this.clusteringResult.temporaryClustering.removeObjectsFromCluster(cluster, objects);

			this.clusteringResult.temporaryClustering.addCluster(objects, getSimilarityFunction());
			this.saveHistory("Created new cluster with objects");

		} catch (final InterruptedException ie) {
			this.resetToLastSavedClustering();
			throw ie;
		} catch (SimilarityValuesException | SimilarityCalculationException | FeatureCalculationException
				| IncorrectlyInitializedException | PrototypeFactoryException | CreatePrototypeInstanceFactoryException
				| ClusterFactoryException | CreateClusterInstanceFactoryException
				| IncompatibleFeatureAndObjectException | IncompatibleSimilarityValueException
				| DuplicateMappingForObjectException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		}
	}

	/**
	 * Save the current state of the program, with the given note.
	 * 
	 * @param note the note that should be saved with the history
	 * @throws InterruptedException
	 * @throws FeatureCalculationException
	 * @throws IncorrectlyInitializedException
	 * @throws IncompatibleFeatureAndObjectException
	 */
	private void saveHistory(final String note) throws InterruptedException, IncorrectlyInitializedException,
			FeatureCalculationException, IncompatibleFeatureAndObjectException {
		this.clusteringResult.saveCurrentStateToHistory(note);
		this.recalculateClusterObjectMappingFeatures();
	}

	/**
	 * The maximization-expectation clustering, followed by calculation of p-values.
	 * 
	 * @throws ClusterOperationException
	 */
	protected void followingIterations() throws InterruptedException, ClusterOperationException {
		try {
			this.progress.updateProgress(0.0, "Determining clusters to keep from last iteration");

			// Find clusters that have objects and/or are set as keep
			this.clusteringResult.temporaryClustering.removeEmptyClusters();
			if (!Utility.getProgress().getStatus()) {
				throw new InterruptedException("Stopped");
			}
			this.progress.updateProgress(0.25, "Aggregating clusters to calculate cluster prototypes");
			this.refineClusters(this.clusteringResult.temporaryClustering.getClusters().asList());

			this.clusteringResult.temporaryClustering.addObjectsToMostSimilarCluster(
					this.clusteringResult.temporaryObjectsToBeClustered.asSet(), this.getSimilarityFunction(),
					this.progress);

			this.clusteringResult.temporaryClustering.removeEmptyClusters();

			this.progress.updateProgress(null, "Aggregating clusters to calculate cluster prototypes");
			this.refineClusters(this.clusteringResult.temporaryClustering.getClusters().asList());

			this.progress.updateProgress(0.95, "Calculating cluster statistics");
			this.saveHistory("Optimization iteration");
		} catch (InterruptedException e) {
			this.resetToLastSavedClustering();
			throw e;
		} catch (FeatureCalculationException | IncorrectlyInitializedException | SimilarityCalculationException
				| SimilarityValuesException | NoComparableSimilarityValuesException
				| CreateClusterInstanceFactoryException | ClusterFactoryException
				| IncompatibleFeatureAndObjectException | IncompatibleSimilarityValueException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		}
	}

	@Override
	public int getCurrentIteration() {
		return this.clusteringResult.getClusterHistory() != null
				? this.clusteringResult.getClusterHistory().getIterationNumber()
				: 0;
	}

	private void updateClusterObjectMapping(final ClusterObjectMapping com)
			throws FeatureCalculationException, IncorrectlyInitializedException, InterruptedException {
		this.clusteringResult.setClusterObjectMapping(com);
	}

	private void recalculateClusterObjectMappingFeatures() throws IncorrectlyInitializedException, InterruptedException,
			FeatureCalculationException, IncompatibleFeatureAndObjectException {
		final int iterationNumber = this.clusteringResult.getClusterHistory().getIterationNumber();
		final ClusterObjectMapping clustering = this.clusteringResult.getClusterHistory().getClusterObjectMapping();
		// calculate features
		final IFeatureStore featureStore;
		if (this.clusteringResult.hasFeatureStore(iterationNumber))
			featureStore = this.clusteringResult.getFeatureStore(iterationNumber);
		else {
			featureStore = new BasicFeatureStore();
			this.clusteringResult.setFeatureStore(iterationNumber, featureStore);
		}

		// clustering features
		this.progress.updateProgress(null, "Calculating features for new clustering ...");
		for (IFeature<Object> f : this.getRequestedFeatureCalculations(ObjectType.CLUSTERING)) {
			try {
				this.progress.updateProgress(null, null, f.getName());
				if (!featureStore.getFeaturesFor(clustering).contains(f)) {
					featureStore.setFeatureValue(clustering, f, f.calculate(clustering));
				}
			} catch (final FeatureCalculationException e) {
				if (e.getCause() instanceof MissingPrototypeException)
					continue;
				throw e;
			}
		}

		// cluster features
		this.progress.updateProgress(null, "Calculating features for new clusters ...");
		for (IFeature<Object> f : this.getRequestedFeatureCalculations(ObjectType.CLUSTER))
			for (final ICluster c : clustering.getClusters()) {
				try {
					this.progress.updateProgress(null, null, f.getName());
					if (!featureStore.getFeaturesFor(c).contains(f)) {
						featureStore.setFeatureValue(c, f, f.calculate(c));
					}
				} catch (final FeatureCalculationException e) {
					if (e.getCause() instanceof MissingPrototypeException)
						continue;
					throw e;
				}
			}
	}

	/**
	 * Initial clustering of time series objects, using InitialClusteringInterface.
	 * 
	 * @throws TooFewObjectsClusteringException
	 * @throws ClusterOperationException
	 *
	 */
	private void firstIteration()
			throws InterruptedException, TooFewObjectsClusteringException, ClusterOperationException {
		try {
			if (!this.progress.getStatus())
				throw new InterruptedException("Stopped");

			this.resetStatus();

			this.progress.updateProgress(0.1, "Finding initial clustering.");
			this.updateClusterObjectMapping(
					this.clusteringResult.getInitialClusteringProvider().getInitialClustering(this.getObjects()));
			this.saveHistory("Initial clustering");
		} catch (FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleFeatureAndObjectException e) {
			this.resetToLastSavedClustering();
			throw new ClusterOperationException(e);
		}
	}

	protected void resetStatus() {
		this.progress.updateProgress(0.0, "", "");
	}

	/**
	 * Refines all the given patterns according to their assigned time series
	 * objects
	 * 
	 * @param clusters the patterns to refine
	 * @return the new refined pattern array.
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 * @throws ClusterFactoryException
	 * @throws CreateClusterInstanceFactoryException
	 */
	protected void refineClusters(final IClusterList clusters) throws ClusterOperationException, InterruptedException,
			CreateClusterInstanceFactoryException, ClusterFactoryException {
		final List<IPrototype> refinedPrototypes = this.getRefinedPrototypes(clusters);
		for (int i = 0; i < clusters.size(); i++) {
			clusters.get(i).updatePrototype(refinedPrototypes.get(i));
		}
	}

	protected List<IPrototype> getRefinedPrototypes(final IClusterList clusters)
			throws ClusterOperationException, InterruptedException {
		// 100 clusters at a time per thread
		final Iterable<Iterable<ICluster>> batches = Iterables.batchify(Iterables.toBoundIterable(clusters),
				new BatchCount(privateThreadPool.getMaximumPoolSize(), 100));

		final List<Future<List<IPrototype>>> futures = new MyParallel<List<IPrototype>>(privateThreadPool).For(batches,
				new RefinePrototypesOp());

		final List<IPrototype> refinedPrototypes = new ArrayList<>();

		for (Future<List<IPrototype>> f : futures) {
			try {
				for (final IPrototype p : f.get())
					refinedPrototypes.add(p);
			} catch (final ExecutionException e) {
				if (e.getCause() instanceof ClusterOperationException)
					throw (ClusterOperationException) e.getCause();
				throw new ClusterOperationException(e.getCause());
			}
		}
		return refinedPrototypes;
	}

	ClusterObjectMapping getTemporaryClustering() {
		return this.clusteringResult.temporaryClustering;
	}

	@Override
	public ClusterObjectMapping getLatestClustering() {
		return this.clusteringResult.getClusterHistory().getClusterObjectMapping();
	}

	/**
	 * @return the clusteringResult
	 */
	@Override
	public R getClusteringResult() {
		return this.clusteringResult;
	}

	@Override
	public void calculatePValues(final ICalculatePValues calculateClusterPValues, final long seed)
			throws InterruptedException, PValueCalculationException {
		try {
			this.firePValueCalculationStarted();
			this.resetStatus();
			final int permutations = calculateClusterPValues.getNumberPermutations();
			double actualProgess = 1 / (double) permutations;
			if (permutations == 0) {
				actualProgess = 1;
			}
			this.progress.updateProgress(actualProgess, "Calculating cluster p-values");

			if (!this.progress.getStatus()) {
				throw new InterruptedException("Stopped");
			}

			final double iterationProgressIncrease = (1 - this.progress.getProgress() - 0.05) / permutations;

			final IFeatureStore clusterFeatureStore = this.clusteringResult.getFeatureStore();
			calculateClusterPValues.setFeatureStore(clusterFeatureStore);
			for (IFitnessScore fitnessScore : calculateClusterPValues.getFitnessScores()) {
				final IScalerBuilder[] scaler = fitnessScore.getFeatureScalerBuilder();

				// initialize features and scalers; calculate features
				for (int fIdx = 0; fIdx < fitnessScore.getFeatures().length; fIdx++) {
					IFeature<? extends Comparable<?>> f = fitnessScore.getFeatures()[fIdx];
					// initialize feature
					if ((f instanceof SimilarityFeature)) {
						((SimilarityFeature) f).setSimilarityFunction(this.getSimilarityFunction());
					}

					// initialize scaler
					if (scaler != null) {
						// TODO: shouldn't we draw the random samples here from a randomly generated
						// clustering? or not relevant for scaling?
//			scaler.put(f, new QuantileScalerBuilder().setQuantiles(ArraysExt.range(0.0, 1.0, 1000, true))
//					.setFeatureToScale(f).setObjectProvider(originalPom).setRandom(this.random).setSampleSize(1000));
						IScalerBuilder scalerBuilder = scaler[fIdx];
						scalerBuilder.setObjectProvider(getLatestClustering()).setSeed(seed).setSampleSize(1000);
					}

					try {

						// fill the cluster feature store
						for (final IObjectWithFeatures c : this.getLatestClustering()
								.getObjectsOfType(f.supportedObjectType())) {
							final IFeatureValue<? extends Comparable<?>> val = f.calculate(c);
							clusterFeatureStore.setFeatureValue(c, (IFeature) f, val);
						}

					} catch (FeatureCalculationException | IncorrectlyInitializedException
							| IncompatibleObjectProviderException | IncompatibleFeatureAndObjectException e1) {
						throw new PValueCalculationException(e1);
					}
				}
			}

			final IShuffle shuffle = calculateClusterPValues.getShuffle();

			final IShuffle shuffleClustering;
			if (shuffle.supportedObjectType().equals(ObjectType.CLUSTERING))
				shuffleClustering = shuffle;
			else if (shuffle.supportedObjectType().equals(ObjectType.TIME_SERIES)) {
				if (shuffle instanceof CreateRandomTimeSeries) {
					((CreateRandomTimeSeries) shuffle).setAllObjects(this.getObjects().asList());

					final ShuffleClusteringWithRandomPrototypeTimeSeries shuffleClusteringWithRandomTimeSeries = new ShuffleClusteringWithRandomPrototypeTimeSeries(
							this.getSimilarityFunction(), this.clusteringResult.prototypeBuilder,
							(CreateRandomTimeSeries) shuffle);
					shuffleClusteringWithRandomTimeSeries.setRandomNumberClusters(true);

					shuffleClustering = shuffleClusteringWithRandomTimeSeries;
				} else if (shuffle instanceof ShuffleTimeSeries)
					shuffleClustering = new ShuffleClusteringByShufflingPrototypeTimeSeries(this.similarityFunction,
							this.clusteringResult.prototypeBuilder, new ShuffleTimeSeries());
				else
					shuffleClustering = null;
			} else if (shuffle.supportedObjectType().equals(ObjectType.OBJECTS)) {
				// TODO: for now, fall back to CLARA
				final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder;

				if (this.clusteringResult.initialClusteringProvider instanceof InitialClusteringMethod) {
					clusteringMethodBuilder = ((InitialClusteringMethod) this.clusteringResult.initialClusteringProvider).clusteringMethodBuilder
							.copy();
					if (clusteringMethodBuilder instanceof CLARAClusteringMethodBuilder)
						((CLARAClusteringMethodBuilder) clusteringMethodBuilder).setSampleSize(-1);
				} else
					clusteringMethodBuilder = new CLARAClusteringMethodBuilder().setSamples(10).setSampleSize(-1)
							.setPamkBuilder(new PAMKClusteringMethodBuilder().setSwapIterations(50).setNstart(5))
							.setSimilarityFunction(this.getSimilarityFunction())
							.setPrototypeBuilder(this.clusteringResult.prototypeBuilder);
				final ShuffleClusteringByShufflingDataset shuffleClusteringByShufflingDataset = new ShuffleClusteringByShufflingDataset(
						clusteringMethodBuilder,
						this.clusteringResult.initialClusteringProvider.getInitialNumberOfClusters(),
						(IShuffleDataset) shuffle);
				shuffleClusteringByShufflingDataset.setRandomNumberClusters(true);
				shuffleClustering = shuffleClusteringByShufflingDataset;
			} else
				shuffleClustering = null;

			calculateClusterPValues.setShuffle(shuffleClustering);

			calculateClusterPValues.addChangeListener(new ChangeListener() {

				@Override
				public void stateChanged(final ChangeEvent e) {
					if (e instanceof PermutationTestChangeEvent) {
						final PermutationTestChangeEvent ev = (PermutationTestChangeEvent) e;
						try {
							BasicClusteringProcess.this.progress.updateProgress(
									BasicClusteringProcess.this.progress.getProgress() + iterationProgressIncrease,
									"Calculating p-values for clusters: " + (ev.getFinishedPermutations()) + " of "
											+ ev.getTotalPermutations() + " permutations finished");
						} finally {
							logger.debug("Permutation done");
						}
					}
				}
			});

			final IPValueCalculationResult pvalsResult = calculateClusterPValues.calculatePValues(getLatestClustering(),
					seed);

			this.firePValueCalculationFinished();
			this.clusteringResult.setPvalueResult(this.getCurrentIteration(), pvalsResult);
		} catch (IncompatibleFeatureAndObjectException | IncompatibleSimilarityFunctionException e) {
			throw new PValueCalculationException(e);
		}
	}

	@Override
	public int getNumberOfInitialClusters() {
		return this.clusteringResult.getInitialClusteringProvider().getInitialNumberOfClusters();
	}

	@Override
	public ISimilarityValue getAverageObjectClusterSimilarity() {
		return this.clusteringResult.getFeatureStore()
				.getFeatureValue(this.clusteringResult.getClusterHistory().getClusterObjectMapping(),
						this.averageSimilarityFeature)
				.getValue();
	}

	/**
	 * @return the iterationToClusterPValues
	 */
	@Override
	public IPValueCalculationResult getPValues() {
		return this.getPValues(this.getCurrentIteration());
	}

	@Override
	public IPValueCalculationResult getPValues(final int iteration) {
		return this.clusteringResult.iterationToPValues.get(iteration);
	}

	@Override
	public void deleteOldIterations(final int firstIterationToKeep) throws CannotDeleteIterationException {
		if (this.clusteringResult.getClusterHistory().getIterationNumber() < firstIterationToKeep) {
			throw new CannotDeleteIterationException(this, firstIterationToKeep,
					"Cannot delete the current iteration.");
		}

		IClusterHistory<ClusterObjectMapping> history = this.getHistory();
		while (history != null && history.getIterationNumber() > firstIterationToKeep) {
			history = history.getParent();
		}

		final Set<Integer> deletedIterations = new HashSet<>();

		if (history != null && history.getIterationNumber() == firstIterationToKeep) {
			IClusterHistory<ClusterObjectMapping> oldParent = history.getParent();
			history.setParent(null);

			while (oldParent != null) {
				deletedIterations.add(oldParent.getIterationNumber());
				oldParent = oldParent.getParent();
			}
		}

		Iterator<Integer> it = this.clusteringResult.iterationToFeatureStore.keySet().iterator();
		while (it.hasNext()) {
			final int iteration = it.next();
			if (iteration < firstIterationToKeep) {
				it.remove();
				deletedIterations.add(iteration);
			}
		}
		it = this.clusteringResult.iterationToPValues.keySet().iterator();
		while (it.hasNext()) {
			final int iteration = it.next();
			if (iteration < firstIterationToKeep) {
				it.remove();
				deletedIterations.add(iteration);
			}
		}

		// is the order of event firing important?
		for (final int iteration : deletedIterations)
			this.clusteringResult.fireClusteringIterationDeleted(iteration);
	}

	private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();

		this.logger = LoggerFactory.getLogger(this.getClass());

		this.pvalueListener = new HashSet<>();

		this.actionsToApplyBeforeNextIteration = new ArrayList<>();
		this.initThreadPool();
	}

	@Override
	public boolean requestFeatureCalculation(IFeature<?> feature) {
		final ObjectType<? extends IObjectWithFeatures> type = feature.supportedObjectType();
		this.requestedFeatures.putIfAbsent(type, new HashSet<>());
		return this.requestedFeatures.get(type).add(feature);
	}

	@Override
	public Set<IFeature<Object>> getRequestedFeatureCalculations(ObjectType<?> type) {
		return (Set) this.requestedFeatures.getOrDefault(type, Collections.emptySet());
	}

	@Override
	public IIdMapMethod getIdMapMethod() {
		return this.idMapMethod;
	}

	/**
	 * @return the initialClusteringProvider
	 */
	@Override
	public IInitialClusteringProvider<ClusterObjectMapping> getInitialClusteringProvider() {
		return this.initialClusteringProvider;
	}

	@Override
	public ILoadDataMethod getLoadDataMethod() {
		return this.loadDataMethod;
	}

	@Override
	public ITimeSeriesObjects getObjects() {
		return this.clusteringResult.getObjectsOfIteration(getCurrentIteration());
	}

	@Override
	public IPreprocessingSummary getPreprocessingSummary() {
		return this.preprocessingSummary;
	}

	/**
	 * @return the prototypeFactory
	 */
	@Override
	public IPrototypeBuilder getPrototypeBuilder() {
		return this.prototypeBuilder;
	}

	@Override
	public ITimePointWeighting getTimePointWeighting() {
		return this.timePointWeighting;
	}

	@Override
	public ITimeSeriesPreprocessor getTimeSeriesPreprocessor() {
		return this.timeSeriesPreprocessor;
	}

	/**
	 * @return the random
	 */
	@Override
	public long getSeed() {
		return this.seed;
	}

	@Override
	public IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> getClusteringMethodBuilder() {
		return this.clusteringMethodBuilder;
	}

	@Override
	public boolean isFirstIteration() {
		return this.getHistory() == null || this.getHistory().getIterationNumber() < 1;
	}

	@Override
	public void applyActionsBeforeNextIteration() throws ClusterOperationException, InterruptedException {
		for (final IActionContainer actionContainer : this.actionsToApplyBeforeNextIteration) {
			final ActionContainer.ACTION_TYPE actionType = actionContainer.getActionType();
			switch (actionType) {
			case SPLIT_PATTERN:
				final ISplitCluster splitPattern = actionContainer.getSplitPattern();
				final ISplitClusterContainer splitPatternContainer = actionContainer.getSplitPatternContainer();
				this.applySplitClusters(splitPattern, splitPatternContainer);
				break;
			case SUGGEST_PATTERN:
				final ISuggestNewCluster suggestNewPattern = actionContainer.getSuggestNewPattern();
				final IClusterSuggestion clusteringTransition = actionContainer.getClusteringTransition();
				this.applySuggestedClusters(suggestNewPattern, clusteringTransition);
				break;
			case DELETE_PATTERN:
				final ICluster patternToDelete = actionContainer.getPatternToDelete();
				final IClusterObjectMapping.DELETE_METHOD deleteMethod = actionContainer.getDeleteType();
				this.deleteData(patternToDelete.getClusterNumber(), deleteMethod);
				break;
			}
		}
		this.resetActionsToApplyBeforeNextIteration();
	}

	@Override
	public void addNewActionsToApplyBeforeNextIteration(final IActionContainer actionContainer) {
		// GUIUtility.setBottomButtonCard(resultPanel, "Action");
		this.actionsToApplyBeforeNextIteration.add(actionContainer);
	}

	@Override
	public void resetActionsToApplyBeforeNextIteration() {
		this.actionsToApplyBeforeNextIteration = new ArrayList<>();
		// GUIUtility.setBottomButtonCard(resultPanel, "Iteration");
	}

	@Override
	public ITimeSeriesObjects removeDataNotInNetwork(
			final ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge<?>> network)
			throws PreprocessingException {
		final ITimeSeriesObjectList removedObjects = new TimeSeriesObjectList();
		if (network == null) {
			return removedObjects;
		}
		if (this.timeSeriesPreprocessor == null) {
			return removedObjects;
		}
		final Map<String, Integer> nodeHashMap = new HashMap<>();
		for (final ITiconeNetworkNode node : network.getNodeList()) {
			final String nodeName = node.getName();
			nodeHashMap.put(nodeName, 1);
		}

		final Iterator<ITimeSeriesObject> it = this.timeSeriesPreprocessor.getObjects().iterator();
		while (it.hasNext()) {
			final ITimeSeriesObject tsd = it.next();
			if (!nodeHashMap.containsKey(tsd.getName())) {
				it.remove();
				removedObjects.add(tsd);
			}
		}

		if (this.timeSeriesPreprocessor.getObjects().isEmpty()) {
			throw new PreprocessingException("There are 0 matching IDs in the data set and network.");
		}
		logger.info("Removed " + removedObjects.size() + " objects from data set not contained in network.");

		return removedObjects;
	}

	/**
	 * Returns -1 if there is no Network or NetworkView, -2 if PatternCalculation is
	 * not set.
	 * 
	 * @return
	 */
	@Override
	public int calculateNumberOfMappedObjects(
			final ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge> network) {
		// Calculate the number of object mapped to network, unmapped objects
		// and unmapped nodes.
		if (network == null) {
			return -1;
		}
		int numberOfMappedObjects = 0;
		final HashMap<String, Integer> timeSeriesHashMap = new HashMap<>();
		for (final ITimeSeriesObject tsd : this.timeSeriesPreprocessor.getObjects()) {
			timeSeriesHashMap.put(tsd.getName(), 1);
		}
		this.unmappedNodes = new HashSet<>();
		for (final ITiconeNetworkNode cyNode : network.getNodeList()) {
			final String nodeName = cyNode.getName();
			if (timeSeriesHashMap.containsKey(nodeName)) {
				numberOfMappedObjects++;
			} else {
				this.unmappedNodes.add(nodeName);
			}
		}
		return numberOfMappedObjects;
	}

	@Override
	public Set<String> getUnmappedNodes() {
		return this.unmappedNodes;
	}

	@Override
	public boolean addPValueCalculationListener(final IPValueCalculationListener listener) {
		return this.pvalueListener.add(listener);
	}

	protected void firePValueCalculationFinished() {
		final PValueCalculationEvent e = new PValueCalculationEvent(this);
		for (final IPValueCalculationListener listener : this.pvalueListener)
			listener.pvalueCalculationFinished(e);
	}

	protected void firePValueCalculationStarted() {
		final PValueCalculationEvent e = new PValueCalculationEvent(this);
		for (final IPValueCalculationListener listener : this.pvalueListener)
			listener.pvalueCalculationStarted(e);
	}

	@Override
	public boolean removePValueCalculationListener(final IPValueCalculationListener listener) {
		return this.pvalueListener.remove(listener);
	}
}

class PrivateThreadFactory implements ThreadFactory {

	ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();

	@Override
	public Thread newThread(Runnable r) {
		Thread t = defaultThreadFactory.newThread(r);

		t.setName(
				t.getName().replace("pool-", String.format("%s-pool-", BasicClusteringProcess.class.getSimpleName())));

		return t;
	}
}
