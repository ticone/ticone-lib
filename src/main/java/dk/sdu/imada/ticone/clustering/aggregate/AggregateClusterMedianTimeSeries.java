package dk.sdu.imada.ticone.clustering.aggregate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeries;

/**
 * This class is used to refine the patterns, by taking the median of each time
 * point of all assigned time series data
 *
 * @author Christian Wiwie
 * @author Christian Nørskov
 */
public class AggregateClusterMedianTimeSeries implements IAggregateClusterIntoTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8562170558772674163L;

	@Override
	public AggregateClusterMedianTimeSeries copy() {
		return new AggregateClusterMedianTimeSeries();
	}

	/**
	 * Refines the given pattern using the given time series objects, by taking the
	 * median of each time point.
	 * 
	 * @param timeSeriesData The time series objects used to refine the pattern
	 * @return returns the refined double array
	 */
	@Override
	public TimeSeries doAggregate(final ITimeSeriesObjects objects) {
		final int timePoints = objects.iterator().next().getPreprocessedTimeSeriesList()[0].getNumberTimePoints();
		final double[] refinedPattern = new double[timePoints];
		for (int i = 0; i < timePoints; i++) {
			final List<Double> points = new ArrayList<>();
			for (final ITimeSeriesObject o : objects.getObjects()) {
				for (final ITimeSeries ts : o.getPreprocessedTimeSeriesList())
					points.add(ts.get(i));
			}
			Collections.sort(points);
			refinedPattern[i] = points.get(points.size() / 2);
		}
		return new TimeSeries(refinedPattern);

	}

	@Override
	public String toString() {
		return "Median Time Series";
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof AggregateClusterMedianTimeSeries;
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}
}
