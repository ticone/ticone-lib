/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.AbstractPairIterable;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IPairBuilder;
import dk.sdu.imada.ticone.util.IPairsFactory;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectSampleException;
import dk.sdu.imada.ticone.util.Pair;
import dk.sdu.imada.ticone.util.PairIndexIterable;
import dk.sdu.imada.ticone.util.PairList;
import dk.sdu.imada.ticone.util.PairSet;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 28, 2017
 *
 */
public class ClusterPair extends Pair<ICluster, ICluster> implements IClusterPair {

	public static class ClusterPairsFactory implements IPairsFactory<ICluster, ICluster, IClusterPair> {

		@Override
		public IPairBuilder<ICluster, ICluster, IClusterPair> getPairBuilder() {
			return new ClusterPairBuilder();
		}

		@Override
		public IClusterPairList createEmptyList() {
			return new ClusterPairList(getPairBuilder());
		}

		@Override
		public IClusterPairSet createEmptySet() {
			return new ClusterPairSet(getPairBuilder());
		}

		@Override
		public IClusterPairs createIterable(final ICluster[] objects, final ICluster[] otherObjects) {
			return new ClusterPairIterable(objects, otherObjects);
		}

		@Override
		public IClusterPairList createList(final Iterable<? extends ICluster> objects,
				final Iterable<? extends ICluster> otherObjects) {
			final ClusterPairList result = new ClusterPairList(getPairBuilder());
			result.addPairsOf(objects, otherObjects);
			return result;
		}

		@Override
		public IClusterPairSet createSet(final Iterable<? extends ICluster> objects,
				final Iterable<? extends ICluster> otherObjects) {
			final ClusterPairSet result = new ClusterPairSet(getPairBuilder());
			result.addPairsOf(objects, otherObjects);
			return result;
		}

		class ClusterPairIterable extends AbstractPairIterable<ICluster, ICluster, IClusterPair>
				implements IClusterPairs {

			/**
			 * 
			 */
			private static final long serialVersionUID = 4998993203973247780L;

			ClusterPairIterable(final ICluster[] objects1, final ICluster[] objects2) {
				super(objects1, objects2, new ClusterPairBuilder());
			}

			ClusterPairIterable(final ICluster[] firstObjects, final ICluster[] secondObjects,
					final PairIndexIterable pairIdxIt, final IPairBuilder<ICluster, ICluster, IClusterPair> builder) {
				super(firstObjects, secondObjects, pairIdxIt, builder);
			}

			@Override
			public ClusterPairIterable copy() {
				return new ClusterPairIterable(this.firstObjects, this.secondObjects);
			}

			@Override
			public ClusterPairList asList() {
				final ClusterPairList result = new ClusterPairList(builder);
				this.forEach(p -> result.add(p));
				return result;
			}

			@Override
			public ClusterPairSet asSet() {
				final ClusterPairSet result = new ClusterPairSet(builder);
				this.forEach(p -> result.add(p));
				return result;
			}

			@Override
			public ClusterPairIterable range(final long start, final long end) {
				return new ClusterPairIterable(firstObjects, secondObjects, this.pairIdxIt.range(start, end), builder);
			}

			@Override
			public <T extends IObjectWithFeatures> int getNumberObjectsOfType(ObjectType<T> type)
					throws IncompatibleObjectProviderException {
				if (type == ObjectType.CLUSTER_PAIR)
					return this.firstObjects.length * this.firstObjects.length;
				return super.getNumberObjectsOfType(type);
			}

			@Override
			public <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
					throws IncompatibleObjectProviderException {
				if (type.equals(ObjectType.CLUSTER_PAIR))
					return (Collection<T>) asList();
				return super.getObjectsOfType(type);
			}

			@Override
			public <T extends IObjectWithFeatures> List<T> sampleObjectsOfType(ObjectType<T> type, int sampleSize,
					long seed) throws IncompatibleObjectProviderException, ObjectSampleException, InterruptedException {
				try {
					final Random r = new Random(seed);
					if (type == ObjectType.CLUSTER_PAIR) {
						final int[] indices1 = r.ints(sampleSize, 0, this.firstObjects.length).toArray();
						final int[] indices2 = r.ints(sampleSize, 0, this.secondObjects.length).toArray();

						final List<IClusterPair> result = new ArrayList<>();
						for (int p = 0; p < sampleSize; p++)
							result.add(this.builder.build(this.firstObjects[indices1[p]],
									this.secondObjects[indices2[p]]));
					}
				} catch (FactoryException | CreateInstanceFactoryException e) {
					throw new ObjectSampleException(e);
				}
				return super.sampleObjectsOfType(type, sampleSize, seed);
			}

		}

		/**
		 * 
		 * @author Christian Wiwie
		 * 
		 * @since Nov 12, 2018
		 *
		 */
		class ClusterPairSet extends PairSet<ICluster, ICluster, IClusterPair> implements IClusterPairSet {
			/**
			 * 
			 */
			private static final long serialVersionUID = 2337313230471977215L;

			protected ClusterPairSet(final IPairBuilder<ICluster, ICluster, IClusterPair> b) {
				super(b);
			}

			protected ClusterPairSet(final IPairBuilder<ICluster, ICluster, IClusterPair> b,
					final IClusterPair... pairs) {
				this(b);
				for (final IClusterPair o : pairs)
					this.add(o);
			}

			protected ClusterPairSet(final Collection<? extends IClusterPair> pairs,
					final IPairBuilder<ICluster, ICluster, IClusterPair> b) {
				super(pairs, b);
			}

			protected ClusterPairSet(final ClusterPairSet other) {
				super(other);
			}

			@Override
			public IClusterPair[] toArray() {
				return super.toArray(new IClusterPair[0]);
			}

			@Override
			public ClusterPairSet asSet() {
				return new ClusterPairSet(this);
			}

			@Override
			public ClusterPairSet copy() {
				return new ClusterPairSet(this);
			}

			@Override
			public ClusterPairList asList() {
				return new ClusterPairList(this, builder);
			}

			@Override
			public boolean addPairOf(ICluster object, ICluster otherObject) {
				return this.add(new ClusterPair(object, otherObject));
			}

			@Override
			public String toString() {
				return String.format("%s (%d)", "ObjectPairSet", this.size());
			}
		}

		/**
		 * @author Christian Wiwie
		 * 
		 * @since Oct 8, 2018
		 *
		 */
		class ClusterPairList extends PairList<ICluster, ICluster, IClusterPair> implements IClusterPairList {

			/**
			 * 
			 */
			private static final long serialVersionUID = -6866240078234975817L;

			protected ClusterPairList(final IPairBuilder<ICluster, ICluster, IClusterPair> b) {
				super(b);
			}

			protected ClusterPairList(final Collection<? extends IClusterPair> c,
					final IPairBuilder<ICluster, ICluster, IClusterPair> b) {
				super(c, b);
			}

			protected ClusterPairList(final ClusterPairList other) {
				super(other);
			}

			@Override
			public boolean addPairOf(ICluster object, ICluster otherObject) {
				return this.add(new ClusterPair(object, otherObject));
			}

			@Override
			public IClusterPairs getClusterPairs() {
				return this;
			}

			@Override
			public ClusterPairSet asSet() {
				return new ClusterPairSet(this, builder);
			}

			@Override
			public ClusterPairList copy() {
				return new ClusterPairList(this);
			}

			@Override
			public String toString() {
				return String.format("%s (%d)", "ClusterPairList", this.size());
			}
		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7892141241512169664L;

	/**
	 * @param el1
	 * @param el2
	 */
	public ClusterPair(final ICluster el1, final ICluster el2) {
		super(el1, el2);
	}

	@Override
	public boolean equals(final Object p) {
		if (p instanceof ClusterPair)
			return Objects.equals(this.first, ((ClusterPair) p).first)
					&& Objects.equals(this.second, ((ClusterPair) p).second);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getClass().getSimpleName(), this.first, this.second);
	}

	@Override
	public String toString() {
		return String.format("(%s,%s)", Objects.toString(this.first), Objects.toString(this.second));
	}
}
