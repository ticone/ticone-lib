package dk.sdu.imada.ticone.clustering;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.MyParallel;
import dk.sdu.imada.ticone.util.Utility;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

/**
 * Created by christian on 03-03-16.
 */
public class KMeansClusteringMethod extends ParallelizedClusteringMethod<ClusterObjectMapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2856696702128457131L;
	private final boolean printTimes = false;
	private ITimeSeriesObjectList timeSeriesDatas;
	private IntList indices;
	private final int maxIterations, nstart;

	private ICluster[] clustersPatterns;
	private IntList[] clusterObjectIds;

	KMeansClusteringMethod(final ISimilarityFunction similarityFunction, final IPrototypeBuilder prototypeFactory,
			final int maxIterations, final int nstart) throws IncompatibleSimilarityFunctionException {
		super(similarityFunction, prototypeFactory);
		this.maxIterations = maxIterations;
		this.nstart = nstart;
	}

	KMeansClusteringMethod(KMeansClusteringMethod other) {
		super(other);
		this.maxIterations = other.maxIterations;
		this.nstart = other.nstart;
	}

	@Override
	public IClusteringMethod<ClusterObjectMapping> copy() {
		return new KMeansClusteringMethod(this);
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	protected ClusterObjectMapping doFindClusters(final ITimeSeriesObjects timeSeriesDatas, final int numberOfClusters,
			final long seed) throws ClusterOperationException, InterruptedException {
		try {
			final Random random = new Random(seed);
			final IPrototypeBuilder prototypeBuilder = this.prototypeBuilder.copy();

			this.timeSeriesDatas = timeSeriesDatas.asList();
			this.clusterObjectIds = new IntList[numberOfClusters];

			this.indices = new IntArrayList();
			for (int i = 0; i < timeSeriesDatas.size(); i++) {
				this.indices.add(i);
			}
			if (numberOfClusters == 1) {
				try {
					return this.oneCluster(prototypeBuilder);
				} catch (final CreatePrototypeInstanceFactoryException e) {
					throw new ClusterOperationException(e);
				}
			}

			ISimilarityValue bestSimilarity = AbstractSimilarityValue.MIN;
			ClusterObjectMapping bestClustering = null;

			for (int n = 0; n < nstart; n++) {
				ClusterObjectMapping newClustering = this.findStartMedoids(this.timeSeriesDatas, numberOfClusters,
						prototypeBuilder, random.nextLong());

				long time = System.currentTimeMillis();
				this.assignObjectsToNearestCluster();
				ISimilarityValue prevSimilarity = this.adjustClustersParallel(prototypeBuilder);
				long now = System.currentTimeMillis();
				if (this.printTimes) {
					logger.debug("{}", now - time);
				}
				ISimilarityValue newSimilarity = AbstractSimilarityValue.MIN;
				int maxIterations = this.maxIterations;

				while (!prevSimilarity.equals(newSimilarity) && maxIterations > 0) {
					time = System.currentTimeMillis();
					prevSimilarity = newSimilarity;
					this.assignObjectsToNearestCluster();
					newSimilarity = this.adjustClustersParallel(prototypeBuilder);
					maxIterations--;
					now = System.currentTimeMillis();
					if (this.printTimes) {
						logger.debug("{}", now - time);
					}
				}
				newClustering = this.createMapping(prototypeBuilder);
				if (newSimilarity.greaterThan(bestSimilarity)) {
					bestSimilarity = newSimilarity;
					bestClustering = newClustering;
				}
			}
			return bestClustering;
		} catch (SimilarityCalculationException | CreatePrototypeInstanceFactoryException | PrototypeFactoryException
				| SimilarityValuesException | ClusterFactoryException | CreateClusterInstanceFactoryException
				| IncompatibleObjectTypeException | NoComparableSimilarityValuesException
				| IncompatibleSimilarityValueException | IncompatiblePrototypeException
				| DuplicateMappingForObjectException e) {
			throw new ClusterOperationException(e);
		}
	}

	/**
	 * Creates the clustering from the found clusters.
	 * 
	 * @return
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws IncompatibleObjectTypeException
	 * @throws IncompatiblePrototypeException
	 * @throws DuplicateMappingForObjectException
	 */
	private ClusterObjectMapping createMapping(final IPrototypeBuilder prototypeBuilder)
			throws SimilarityCalculationException, InterruptedException, ClusterFactoryException,
			CreateClusterInstanceFactoryException, IncompatibleObjectTypeException, IncompatiblePrototypeException,
			DuplicateMappingForObjectException {
		final ClusterObjectMapping clustering = new ClusterObjectMapping(timeSeriesDatas.asSet().copy(),
				prototypeBuilder);

		for (int i = 0; i < this.clusterObjectIds.length; i++) {
			if (this.clustersPatterns[i] == null)
				continue;
			final IPrototype prototype = this.clustersPatterns[i].getPrototype();

			final ICluster p = clustering.addCluster(prototype);

			this.addObjectsToMapping(this.clusterObjectIds[i], p, clustering);
		}
		return clustering;
	}

	private void addObjectsToMapping(final IntList indicesOfObjects, final ICluster pattern,
			final ClusterObjectMapping pom) throws SimilarityCalculationException, InterruptedException,
			IncompatibleObjectTypeException, DuplicateMappingForObjectException {
		for (final int index : indicesOfObjects) {
			final ITimeSeriesObject tsd = this.timeSeriesDatas.get(index);
			final ISimilarityValue similarity = this.similarityFunction
					.calculateSimilarity(new ObjectClusterPair(tsd, pattern));
			pom.addMapping(tsd, pattern, similarity);
		}
	}

	private ISimilarityValue adjustClustersParallel(final IPrototypeBuilder prototypeBuilder)
			throws InterruptedException, SimilarityCalculationException, CreatePrototypeInstanceFactoryException,
			PrototypeFactoryException, ClusterOperationException, SimilarityValuesException {
		final List<Integer> param = new ArrayList<>();
		for (int i = 0; i < this.clusterObjectIds.length; i++) {
			param.add(i);
		}
		final List<Future<ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair>>> for1 = new MyParallel<ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair>>(
				this.privateClusteringMethodThreadPool).For(param,
						// The operation to perform with each item
						new MyParallel.Operation<Integer, ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair>>() {
							@Override
							public ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> perform(
									final Integer param)
									throws SimilarityCalculationException, CreatePrototypeInstanceFactoryException,
									PrototypeFactoryException, InterruptedException, SimilarityValuesException,
									IncompatibleSimilarityValueException {
								return KMeansClusteringMethod.this.updateClusterPrototypes(param,
										prototypeBuilder.copy());
							}
						});

		if (!Utility.getProgress().getStatus()) {
			throw new InterruptedException("Stopped");
		}
		ISimilarityValue sum = null;
		for (int i = 0; i < for1.size(); i++) {
			try {
				final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = for1.get(i)
						.get();
				if (sims == null)
					continue;
				if (sum == null)
					sum = sims.sum();
				else
					sum.plus(sims.sum());
			} catch (final ExecutionException e) {
				if (e.getCause() instanceof SimilarityCalculationException)
					throw (SimilarityCalculationException) e.getCause();
				if (e.getCause() instanceof SimilarityValuesException)
					throw (SimilarityValuesException) e.getCause();
				if (e.getCause() instanceof CreatePrototypeInstanceFactoryException)
					throw (CreatePrototypeInstanceFactoryException) e.getCause();
				if (e.getCause() instanceof PrototypeFactoryException)
					throw (PrototypeFactoryException) e.getCause();
				if (e.getCause() != null)
					throw new ClusterOperationException(e.getCause());
				throw new ClusterOperationException(e);
			}
		}
		return sum;

	}

	private void assignObjectsToNearestCluster()
			throws InterruptedException, ClusterOperationException, SimilarityCalculationException,
			SimilarityValuesException, NoComparableSimilarityValuesException, IncompatibleSimilarityValueException {
		final List<ICluster> clustersNonNull = new ArrayList<>();
		Int2IntMap subToOrigIdx = new Int2IntOpenHashMap();
		for (int i = 0; i < clustersPatterns.length; i++)
			if (clustersPatterns[i] != null) {
				subToOrigIdx.put(clustersNonNull.size(), i);
				clustersNonNull.add(clustersPatterns[i]);
			}
		final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = this.similarityFunction
				.calculateSimilarities(KMeansClusteringMethod.this.timeSeriesDatas.toArray(),
						clustersNonNull.toArray(new ICluster[0]), new ObjectClusterPairsFactory(),
						ObjectType.OBJECT_CLUSTER_PAIR);
		if (!Utility.getProgress().getStatus()) {
			throw new InterruptedException("Stopped");
		}
		for (int i = 0; i < this.clusterObjectIds.length; i++) {
			this.clusterObjectIds[i] = new IntArrayList();
		}
		final int[] whichMaxForFirstObject = sims
				.whichMaxForFirstObject(IntStream.range(0, this.indices.size()).toArray());
		for (int i = 0; i < this.indices.size(); i++) {
			final int max = whichMaxForFirstObject[i];
			this.clusterObjectIds[subToOrigIdx.get(max)].add(i);
		}
	}

	private ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> updateClusterPrototypes(
			final int cluster, final IPrototypeBuilder prototypeFactory)
			throws InterruptedException, SimilarityCalculationException, CreatePrototypeInstanceFactoryException,
			PrototypeFactoryException, SimilarityValuesException, IncompatibleSimilarityValueException {
		final List<Integer> assignedObjectIds = this.clusterObjectIds[cluster];

		if (assignedObjectIds.isEmpty()) {
			this.clustersPatterns[cluster] = null;
			return null;
		}

		final ITimeSeriesObjects assignedObjects = new TimeSeriesObjectList();
		for (int i = 0; i < assignedObjectIds.size(); i++) {
			final int index = assignedObjectIds.get(i);
			final ITimeSeriesObject tsd = this.timeSeriesDatas.get(index);
			assignedObjects.add(tsd);
		}

		final ICluster p = this.clustersPatterns[cluster];

		if (!Utility.getProgress().getStatus()) {
			throw new InterruptedException("Stopped");
		}

		final IPrototype pattern = prototypeFactory.setObjects(assignedObjects).build();
		p.updatePrototype(pattern);

		final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = this.similarityFunction
				.calculateSimilarities(assignedObjects.toArray(), p.asSingletonList().toArray(),
						new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR);

		return sims;
	}

	private ClusterObjectMapping oneCluster(final IPrototypeBuilder prototypeBuilder)
			throws SimilarityCalculationException, CreatePrototypeInstanceFactoryException, PrototypeFactoryException,
			InterruptedException, ClusterFactoryException, CreateClusterInstanceFactoryException,
			IncompatibleObjectTypeException, SimilarityValuesException, IncompatibleSimilarityValueException,
			DuplicateMappingForObjectException {
		final ClusterObjectMapping clustering = new ClusterObjectMapping(timeSeriesDatas.asSet().copy(),
				prototypeBuilder);

		for (int i = 0; i < this.timeSeriesDatas.size(); i++) {
			this.clusterObjectIds[0].add(i);
		}
		final ICluster p = clustering.addCluster(this.timeSeriesDatas, similarityFunction);

		this.addObjectsToMapping(this.clusterObjectIds[0], p, clustering);
		return clustering;
	}

	/**
	 * Find the starting medoids, by selecting <tt>numberOfClusters</tt> random
	 * objects.
	 * 
	 * @param objects          the objects to choose medoids from.
	 * @param numberOfClusters the number of medoids wanted (number of clusters)
	 * @return
	 * @throws CreatePrototypeInstanceFactoryException
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws IncompatibleSimilarityValueException
	 * @throws SimilarityValuesException
	 * @throws SimilarityCalculationException
	 * @throws DuplicateMappingForObjectException
	 */
	private ClusterObjectMapping findStartMedoids(final ITimeSeriesObjectList objects, final int numberOfClusters,
			final IPrototypeBuilder prototypeBuilder, final long seed)
			throws CreatePrototypeInstanceFactoryException, PrototypeFactoryException, InterruptedException,
			ClusterFactoryException, CreateClusterInstanceFactoryException, SimilarityCalculationException,
			SimilarityValuesException, IncompatibleSimilarityValueException, DuplicateMappingForObjectException {
		final ClusterObjectMapping clustering = new ClusterObjectMapping(timeSeriesDatas.asSet().copy(),
				prototypeBuilder);
		this.clustersPatterns = new Cluster[numberOfClusters];
		final ITimeSeriesObjectList choices = objects.copy();
		final ITimeSeriesObjectList startObjects = new TimeSeriesObjectList();

		final Random random = new Random(seed);

		while (startObjects.size() < numberOfClusters && !choices.isEmpty()) {
			final int index = random.nextInt(choices.size());
			startObjects.add(choices.remove(index));
		}
		int i = 0;
		for (final ITimeSeriesObject o : startObjects) {
			this.clustersPatterns[i] = clustering.addCluster(o.asSingletonList(), similarityFunction);
			i++;
		}
		return clustering;
	}

	@Override
	public String toString() {
		return "K-Means";
	}

	/**
	 * @return the nstart
	 */
	public int getNstart() {
		return this.nstart;
	}

	/**
	 * @return the maxIterations
	 */
	public int getMaxIterations() {
		return this.maxIterations;
	}
}
