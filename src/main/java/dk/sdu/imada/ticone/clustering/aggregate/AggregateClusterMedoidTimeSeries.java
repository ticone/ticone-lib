package dk.sdu.imada.ticone.clustering.aggregate;

import java.util.Map;
import java.util.Objects;

import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesSomePairs;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.AggregationException;

/**
 * @author Christian Wiwie
 * @author Christian Nørskov
 * @since 01-12-15.
 */
public class AggregateClusterMedoidTimeSeries implements IAggregateClusterIntoTimeSeries {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3510955954536406206L;
	private ISimilarityFunction similarityFunction;

	public AggregateClusterMedoidTimeSeries(final ISimilarityFunction similarityFunction) {
		this.similarityFunction = similarityFunction;
	}

	@Override
	public AggregateClusterMedoidTimeSeries copy() {
		return new AggregateClusterMedoidTimeSeries(this.similarityFunction);
	}

	public void setSimilarityFunction(final ISimilarityFunction similarityFunction) {
		this.similarityFunction = similarityFunction;
	}

	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	@Override
	public ITimeSeries doAggregate(final ITimeSeriesObjects objects) throws AggregationException, InterruptedException {
		try {

			final Map<ITimeSeriesObject, ? extends ISimilarityValuesSomePairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>> sims = this.similarityFunction
					.calculateSimilarities(objects.toArray(), objects.toArray(), new ObjectPairsFactory(),
							ObjectType.OBJECT_PAIR)
					.filter((o1, o2, s) -> {
						try {
							return Double.isFinite(s.get());
						} catch (final SimilarityCalculationException e) {
							return false;
						}
					}).partitionByFirstObject();

			ISimilarityValue maxSim = AbstractSimilarityValue.MIN;
			ITimeSeriesObject maxObject = null;
			for (final ITimeSeriesObject o : sims.keySet()) {
				try {
					final ISimilarityValue sum = sims.get(o).sum();
					if (sum instanceof ISimilarityValue && maxSim.lessThan((ISimilarityValue) sum)) {
						maxObject = o;
						maxSim = (ISimilarityValue) sum;
					}
				} catch (final SimilarityCalculationException e1) {
				}
			}

			// TODO
			final ITimeSeries pattern = maxObject.getPreprocessedTimeSeriesList()[0];
			return pattern;
		} catch (SimilarityCalculationException | SimilarityValuesException | IncompatibleSimilarityValueException e) {
			throw new AggregationException(e);
		}
	}

	@Override
	public String toString() {
		return "Medoid Time Series";
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof AggregateClusterMedoidTimeSeries
				&& Objects.equals(this.similarityFunction, ((AggregateClusterMedoidTimeSeries) obj).similarityFunction);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getClass(), this.similarityFunction);
	}
}
