package dk.sdu.imada.ticone.clustering;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping.ClusterObjectMappingCopy;
import dk.sdu.imada.ticone.clustering.filter.BasicFilter;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.FeaturePvalue;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.feature.store.INewFeatureStoreListener;
import dk.sdu.imada.ticone.feature.store.NewFeatureStoreEvent;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.statistics.IPValueCalculationResult;
import dk.sdu.imada.ticone.statistics.IPValueResultStorageListener;
import dk.sdu.imada.ticone.statistics.IPvalue;
import dk.sdu.imada.ticone.statistics.PValueCalculationException;
import dk.sdu.imada.ticone.statistics.PValueResultStorageEvent;
import dk.sdu.imada.ticone.util.ClusterHistory;
import dk.sdu.imada.ticone.util.ClusterStatusMapping;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.ITimePointWeighting;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.Pair;

/**
 * @author Christian Wiwie
 * @author Christian Norskov
 * @since 3/24/15
 */
public class TiconeClusteringResult extends AbstractNamedTiconeResult
		implements ChangeListener, ITiconeClusteringResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7519999103416906395L;

	protected static int nextClusteringNumber = 1;

	protected int clusteringNumber;
	protected final long seed;
	protected final ILoadDataMethod loadDataMethod;
	protected final IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider;
	protected final ITimePointWeighting timePointWeighting;
	protected final IIdMapMethod idMapMethod;
	protected final IPreprocessingSummary preprocessingSummary;
	protected final int numberOfTimePoints;
	protected BasicClusteringProcess<TiconeClusteringResult> clusteringProcess;
	protected IShuffle permutateDataset;
	protected final ISimilarityFunction similarityFunction;
	protected final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder;
	protected ClusterStatusMapping clusterStatusMapping;
	protected final ITimeSeriesPreprocessor timeSeriesPreprocessor;
	protected final IPrototypeBuilder prototypeBuilder;
	protected IFilter<ICluster> clusterFilter;

	protected Map<Integer, IFeatureStore> iterationToFeatureStore;
	protected Map<Integer, IPValueCalculationResult> iterationToPValues;

	private IClusterHistory<ClusterObjectMapping> clusterHistory;

	ITimeSeriesObjects temporaryObjectsToBeClustered;
	ClusterObjectMapping temporaryClustering;

	private transient Set<INewFeatureStoreListener> newFeatureStoreListener;
	private transient Set<IClusteringIterationAddedListener> iterationAddedListener;
	private transient Set<IClusteringChangeListener> changeListener;
	private transient Set<IClusteringIterationDeletionListener> iterationDeletionListener;
	private transient Set<IPValueResultStorageListener> pvalueStorageListener;

	public TiconeClusteringResult(long seed, ILoadDataMethod loadDataMethod,
			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider, int numberOfTimePoints,
			ITimePointWeighting timePointWeighting, IIdMapMethod idMapMethod,
			IPreprocessingSummary preprocessingSummary, ISimilarityFunction similarityFunction,
			IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder,
			ITimeSeriesPreprocessor timeSeriesPreprocessor, IPrototypeBuilder prototypeBuilder) {
		super();
		this.seed = seed;
		this.loadDataMethod = loadDataMethod;
		this.initialClusteringProvider = initialClusteringProvider;
		this.numberOfTimePoints = numberOfTimePoints;
		this.timePointWeighting = timePointWeighting;
		this.idMapMethod = idMapMethod;
		this.preprocessingSummary = preprocessingSummary;
		this.similarityFunction = similarityFunction;
		this.clusteringMethodBuilder = clusteringMethodBuilder;
		this.timeSeriesPreprocessor = timeSeriesPreprocessor;
		this.prototypeBuilder = prototypeBuilder;

		this.clusteringNumber = nextClusteringNumber++;
		// version is set in the preprocessing task after preprocessing and
		// clustering is finished
		// ...
		this.clusterFilter = new BasicFilter<>();
		this.newFeatureStoreListener = new HashSet<>();
		this.iterationAddedListener = new HashSet<>();
		this.changeListener = new HashSet<>();
		this.iterationDeletionListener = new HashSet<>();
		this.pvalueStorageListener = new HashSet<>();

		this.iterationToFeatureStore = new TreeMap<>();
		this.iterationToPValues = new TreeMap<>();

		if (timeSeriesPreprocessor != null) {
			this.temporaryObjectsToBeClustered = timeSeriesPreprocessor.getObjects().copy();
			this.temporaryClustering = new ClusterObjectMapping(this.temporaryObjectsToBeClustered.asSet().copy(),
					prototypeBuilder);
		}
	}

	public TiconeClusteringResult(final TiconeClusteringResult clusteringResult) {
		super(clusteringResult);
		this.newFeatureStoreListener = new HashSet<>();
		this.iterationAddedListener = new HashSet<>();
		this.changeListener = new HashSet<>();
		this.iterationDeletionListener = new HashSet<>();

		this.seed = clusteringResult.seed;
		this.clusteringNumber = clusteringResult.clusteringNumber;
		this.loadDataMethod = clusteringResult.loadDataMethod;
		this.timePointWeighting = clusteringResult.timePointWeighting;
		this.idMapMethod = clusteringResult.idMapMethod;
		this.preprocessingSummary = clusteringResult.preprocessingSummary;
		this.numberOfTimePoints = clusteringResult.numberOfTimePoints;
		this.clusteringProcess = clusteringResult.clusteringProcess;
		this.permutateDataset = clusteringResult.permutateDataset;
		this.similarityFunction = clusteringResult.similarityFunction;
		this.initialClusteringProvider = clusteringResult.initialClusteringProvider;
		this.clusteringMethodBuilder = clusteringResult.clusteringMethodBuilder;
		this.clusterStatusMapping = clusteringResult.clusterStatusMapping;
		this.timeSeriesPreprocessor = clusteringResult.timeSeriesPreprocessor;
		this.creationDate = clusteringResult.creationDate;
		this.prototypeBuilder = clusteringResult.prototypeBuilder;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof TiconeClusteringResult))
			return false;
		final TiconeClusteringResult other = (TiconeClusteringResult) obj;
		return Objects.equals(this.changeListener, other.changeListener)
				&& this.clusteringNumber == other.clusteringNumber && Objects.equals(this.name, other.name)
				&& Objects.equals(this.loadDataMethod, other.loadDataMethod)
				&& Objects.equals(this.timePointWeighting, other.timePointWeighting)
				&& Objects.equals(this.idMapMethod, other.idMapMethod)
				&& Objects.equals(this.preprocessingSummary, other.preprocessingSummary)
				&& Objects.equals(this.prototypeBuilder, other.prototypeBuilder)
				&& this.numberOfTimePoints == other.numberOfTimePoints
				&& Objects.equals(this.clusteringProcess, other.clusteringProcess)
				&& Objects.equals(this.permutateDataset, other.permutateDataset)
				&& Objects.equals(this.similarityFunction, other.similarityFunction)
				&& Objects.equals(this.clusteringMethodBuilder, other.clusteringMethodBuilder)
				&& Objects.equals(this.clusterStatusMapping, other.clusterStatusMapping)
				&& Objects.equals(this.timeSeriesPreprocessor, other.timeSeriesPreprocessor);
	}

	public void setupPatternStatusMapping() {
		final IClusterObjectMapping patternObjectMapping = this.clusteringProcess.getLatestClustering();
		final ClusterStatusMapping patternStatusMapping = new ClusterStatusMapping(this.getFeatureStore());
		for (final ICluster pattern : patternObjectMapping.getClusters()) {
			patternStatusMapping.addCluster(pattern, pattern.getObjects(), false, false, false);
		}
		this.setPatternStatusMapping(patternStatusMapping);
	}

	@Override
	public ClusterStatusMapping getClusterStatusMapping() {
		return this.clusterStatusMapping;
	}

	@Override
	public IClusterObjectMapping getLatestClustering() {
		return this.getClusterHistory().getClusterObjectMapping();
	}

	@Override
	public IClusterObjectMapping getClusteringOfIteration(final int iteration) {
		IClusterHistory hist = this.getClusterHistory();

		while (hist != null && hist.getIterationNumber() > iteration) {
			hist = hist.getParent();
		}

		return hist.getClusterObjectMapping();
	}

	private void setPatternStatusMapping(final ClusterStatusMapping patternStatusMapping) {
		if (this.getClusterFilter() != null)
			this.getClusterFilter().removeFilterListener(this.clusterStatusMapping);
		this.clusterStatusMapping = patternStatusMapping;
		this.getClusterFilter().addFilterListener(this.clusterStatusMapping);
	}

	@Override
	public IClusterHistory<ClusterObjectMapping> getClusterHistory() {
		return this.clusterHistory;
	}

	@Override
	public void resetToIteration(final int iteration) throws InterruptedException {
		IClusterHistory<ClusterObjectMapping> h = this.getClusterHistory();
		while (h != null && h.getIterationNumber() > iteration) {
			final int deletedIteration = h.getIterationNumber();
			this.removeFeatureStore(deletedIteration);
			this.removeClusterPValues(deletedIteration);

			h = h.getParent();

			this.setClusterHistory(h);
			this.setClusterObjectMapping(h.getClusterObjectMapping().getCopy(true).copy);
			this.fireClusteringIterationDeleted(deletedIteration);
		}

		this.setupPatternStatusMapping();
		this.updateClusterFilter();

		this.fireClusteringChanged();
	}

	protected void saveCurrentStateToHistory(final String note) throws InterruptedException {
		final ClusterObjectMappingCopy copy = this.temporaryClustering.getCopy(true);
		// apply copied clusters to feature store
		int iterationNumber = this.getClusterHistory() != null ? this.getClusterHistory().getIterationNumber() + 1 : 1;

		final IFeatureStore featureStore = this.getFeatureStore(iterationNumber);
		final IFeatureStore newFeatureStore;
		if (featureStore == null)
			newFeatureStore = new BasicFeatureStore();
		else
			newFeatureStore = new BasicFeatureStore((BasicFeatureStore) featureStore, (IObjectWithFeatures c) -> {
				if (c instanceof ICluster)
					return copy.originalToCopy.get(c);
				return copy.copy;
			});
		this.setFeatureStore(iterationNumber, newFeatureStore);

		final ClusterHistory patternHistory = new ClusterHistory(this.getClusterHistory(),
				temporaryObjectsToBeClustered.copy(), copy.copy, note);
		this.setClusterHistory(patternHistory);
		this.setupPatternStatusMapping();
		this.updateClusterFilter();
		this.fireClusteringChanged();
		this.fireClusteringIterationAdded(iterationNumber);

	}

	protected void setClusterObjectMapping(ClusterObjectMapping clusterObjectMapping) {
		this.temporaryClustering = clusterObjectMapping;
	}

	/**
	 * @param currentHistory the currentHistory to set
	 */
	public void setClusterHistory(IClusterHistory<ClusterObjectMapping> currentHistory) {
		this.clusterHistory = currentHistory;
	}

	/**
	 * @return the prototypeFactory
	 */
	@Override
	public IPrototypeBuilder getPrototypeBuilder() {
		return this.prototypeBuilder;
	}

	public int getNumberOfTimePoints() {
		return this.numberOfTimePoints;
	}

	public void setClusteringProcess(final BasicClusteringProcess timeSeriesClusteringWithOverrepresentedPatterns) {
//		if (this.timeSeriesClusteringWithOverrepresentedPatterns != null) {
//			this.timeSeriesClusteringWithOverrepresentedPatterns.removeChangeListener(this);
//			this.timeSeriesClusteringWithOverrepresentedPatterns.removePValueCalculationListener(this);
//			this.timeSeriesClusteringWithOverrepresentedPatterns.removeClusteringIterationDeletionListener(this);
//		}
		this.clusteringProcess = timeSeriesClusteringWithOverrepresentedPatterns;
//		this.timeSeriesClusteringWithOverrepresentedPatterns.addClusteringChangeListener(this);
//		this.timeSeriesClusteringWithOverrepresentedPatterns.addPValueCalculationListener(this);
//		this.timeSeriesClusteringWithOverrepresentedPatterns.addClusteringIterationDeletionListener(this);
	}

	public IClusteringProcess<ClusterObjectMapping, ?> getClusteringProcess() {
		return this.clusteringProcess;
	}

	@Override
	public ITimeSeriesPreprocessor getTimeSeriesPreprocessor() {
		return this.timeSeriesPreprocessor;
	}

	public void setIPermutate(final IShuffle permutateDataset) {
		this.permutateDataset = permutateDataset;
	}

	@Override
	public IShuffle getPermutationMethod() {
		return this.permutateDataset;
	}

	public ITimeSeriesObjects getLatestObjects() {
		return getClusterHistory().getAllObjects();
	}

	/**
	 * @return the objects
	 */
	public ITimeSeriesObjects getObjectsOfIteration(final int iteration) {
		if (iteration == 0) {
			if (this.timeSeriesPreprocessor != null)
				return timeSeriesPreprocessor.getObjects();
			return null;
		}
		IClusterHistory hist = this.getClusterHistory();
		while (hist != null && hist.getIterationNumber() > iteration) {
			hist = hist.getParent();
		}
		return hist.getAllObjects();

	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		s.defaultReadObject();
		this.newFeatureStoreListener = new HashSet<>();
		this.iterationAddedListener = new HashSet<>();
		this.changeListener = new HashSet<>();
		this.pvalueStorageListener = new HashSet<>();
		this.iterationDeletionListener = new HashSet<>();
		this.getClusterFilter().addFilterListener(this.clusterStatusMapping);
	}

	/**
	 * @return the random
	 */
	@Override
	public long getSeed() {
		return this.seed;
	}

	@Override
	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	@Override
	public IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> getClusteringMethodBuilder() {
		return this.clusteringMethodBuilder;
	}

//	@Override
//	public void clusteringChanged(final ClusteringChangeEvent e) {
//		this.setupPatternStatusMapping();
//		this.updateClusterFilter();
//
//		if (e.getClustering() instanceof ITimeSeriesClusteringWithOverrepresentedPatterns) {
//			final ITimeSeriesClusteringWithOverrepresentedPatterns clustering = e.getClustering();
//			// if (clustering.getClusterPValues() != null) {
//			// this.pvalueCalculationResult = clustering.getClusterPValues();
//			// }
//			// forward event to listener of this object
//			this.fireStateChanged();
//		}
//	}
//
//	@Override
//	public void pvalueCalculationFinished(final PValueCalculationEvent event) {
//		if (event.getSource().equals(this.timeSeriesClusteringWithOverrepresentedPatterns)) {
//			final IClusterPValueCalculationResult clusterPValues = this.timeSeriesClusteringWithOverrepresentedPatterns
//					.getClusterPValues();
//			this.iterationToClusterPValues.put(clusterHistory.getIterationNumber(), clusterPValues);
//			clusterPValues.addChangeListener(this);
//
//			this.fireStateChanged();
//		}
//	}
//
//	@Override
//	public void pvalueCalculationStarted(final PValueCalculationEvent event) {
//	}

	public void setPvalueResult(final int iteration, final IPValueCalculationResult pvalsResult)
			throws PValueCalculationException, InterruptedException, IncompatibleFeatureAndObjectException {
		try {
			final IPValueCalculationResult old = this.iterationToPValues.put(iteration, pvalsResult);

			final IFeatureStore featureStore = this.getFeatureStore(iteration);

			final List<Pair<IObjectWithFeatures, IFeature<IPvalue>>> objectFeaturePairs = new ArrayList<>();
			final List<IFeatureValue<IPvalue>> values = new ArrayList<>();

			for (ObjectType<?> c : pvalsResult.providesValuesForObjectTypes()) {
				final FeaturePvalue cfPvalue = new FeaturePvalue(c);
				cfPvalue.setFeatureValueProvider(pvalsResult);
				for (final IObjectWithFeatures o : pvalsResult.getObjects(c)) {
					objectFeaturePairs.add(Pair.getPair(o, cfPvalue));
					values.add(cfPvalue.calculate(o));
				}
			}

			featureStore.setFeatureValues(objectFeaturePairs, values);

			this.firePValueResultStored(iteration, pvalsResult, old);
		} catch (IncorrectlyInitializedException | FeatureCalculationException
				| IncompatibleFeatureValueProviderException e1) {
			throw new PValueCalculationException(e1);
		}
	}

	/**
	 * If we have switched to another clustering, we need to update the
	 * corresponding stats in the cluster filter.
	 */
	protected void updateClusterFilter() {
		final IFilter<ICluster> clusterFilter = this.getClusterFilter();

		final IArithmeticFeature<?> feature = clusterFilter.getFeature();

		if (feature instanceof FeaturePvalue) {
			((FeaturePvalue) feature).setFeatureValueProvider(this.getPvalueCalculationResult());
		}

		// TODO: for now we deactivate the filter as we do not know whether the
		// cluster features are
		// calculated for the new clustering
		clusterFilter.setActive(false);
		clusterFilter.fireFilterChanged();
	}

	@Override
	public ILoadDataMethod getLoadDataMethod() {
		return this.loadDataMethod;
	}

	/**
	 * @return the initialClusteringProvider
	 */
	@Override
	public IInitialClusteringProvider<ClusterObjectMapping> getInitialClusteringProvider() {
		return this.initialClusteringProvider;
	}

	@Override
	public ITimePointWeighting getTimePointWeighting() {
		return this.timePointWeighting;
	}

	@Override
	public IIdMapMethod getIdMapMethod() {
		return this.idMapMethod;
	}

	@Override
	public List<Integer> getIterations() {
		return new ArrayList<>(this.iterationToFeatureStore.keySet());
	}

	@Override
	public IPreprocessingSummary getPreprocessingSummary() {
		return this.preprocessingSummary;
	}

	@Override
	public int getClusteringNumber() {
		return this.clusteringNumber;
	}

	public void setClusteringNumber(final int clusteringNumber) {
		this.clusteringNumber = clusteringNumber;
		this.name = String.format("Clustering %d", this.clusteringNumber);
	}

	public static int getNextClusteringNumber() {
		return nextClusteringNumber;
	}

	public static void setNextClusteringNumber(final int nextClusteringNumber) {
		TiconeClusteringResult.nextClusteringNumber = nextClusteringNumber;
	}

	/**
	 * @return the pvalueCalculationResult
	 */
	@Override
	public <O extends IObjectWithFeatures> IPValueCalculationResult getPvalueCalculationResult() {
		return this.clusteringProcess.getPValues();
	}

	/**
	 * @return the pvalueCalculationResult
	 */
	@Override
	public <O extends IObjectWithFeatures> IPValueCalculationResult getPvalueCalculationResult(final int iteration) {
		return this.clusteringProcess.getPValues(iteration);
	}

	@Override
	protected void initName() {
		this.name = String.format("Clustering %d", this.clusteringNumber);
	}

	@Override
	public boolean hasFeatureStore(final int iteration) {
		return this.iterationToFeatureStore.containsKey(iteration);
	}

	@Override
	public IFeatureStore getFeatureStore() {
		return this.getFeatureStore(this.clusterHistory.getIterationNumber());
	}

	@Override
	public IFeatureStore getFeatureStore(final int iteration) {
		return this.iterationToFeatureStore.get(iteration);
	}

	public void setFeatureStore(final IFeatureStore store) {
		this.setFeatureStore(this.clusterHistory.getIterationNumber(), store);
	}

	public void setFeatureStore(final int iteration, final IFeatureStore store) {
		final IFeatureStore newStore = new BasicFeatureStore();
		final IFeatureStore oldStore = this.iterationToFeatureStore.put(iteration, newStore);
		this.fireNewFeatureStore(new NewFeatureStoreEvent(this, oldStore, newStore));
	}

	/**
	 * @return the clusterFeatureStore
	 */
	@Override
	public void removeFeatureStore(final int iteration) {
		this.iterationToFeatureStore.remove(iteration);
	}

	/**
	 * @return the clusterFilter
	 */
	@Override
	public IFilter<ICluster> getClusterFilter() {
		return this.clusterFilter;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == this.iterationToPValues.get(clusterHistory.getIterationNumber()))
			fireStateChanged();
	}

	public void fireNewFeatureStore(final NewFeatureStoreEvent e) {
		for (final INewFeatureStoreListener l : this.getNewFeatureStoreListener())
			l.newFeatureStore(e);
	}

	/**
	 * @return the newFeatureStoreListener
	 */
	public Set<INewFeatureStoreListener> getNewFeatureStoreListener() {
		if (this.newFeatureStoreListener == null)
			this.newFeatureStoreListener = new HashSet<>();
		return this.newFeatureStoreListener;
	}

	@Override
	public void addNewFeatureStoreListener(final INewFeatureStoreListener listener) {
		this.getNewFeatureStoreListener().add(listener);
	}

	@Override
	public void removeNewFeatureStoreListener(final INewFeatureStoreListener listener) {
		this.getNewFeatureStoreListener().remove(listener);
	}

	protected void fireClusteringIterationAdded(final int iteration) {
		final ClusteringIterationAddedEvent event = new ClusteringIterationAddedEvent(this, iteration);
		for (final IClusteringIterationAddedListener listener : this.iterationAddedListener)
			listener.clusteringIterationAdded(event);
	}

	@Override
	public boolean removeClusteringIterationAddedListener(IClusteringIterationAddedListener listener) {
		return this.iterationAddedListener.remove(listener);
	}

	@Override
	public boolean addClusteringIterationAddedListener(IClusteringIterationAddedListener listener) {
		return this.iterationAddedListener.add(listener);
	}

	@Override
	public void removeClusterPValues(final int iteration) {
		this.iterationToPValues.remove(iteration);
		if (!hasFeatureStore(iteration))
			return;
		final IFeatureStore featureStore = this.getFeatureStore(iteration);
		for (IFeature<?> f : featureStore.featureSet()) {
			if (f instanceof FeaturePvalue)
				featureStore.removeFeature(f);
		}
	}

	@Override
	public boolean addClusteringChangeListener(final IClusteringChangeListener listener) {
		return this.changeListener.add(listener);
	}

	protected void fireClusteringChanged() {
		final ClusteringChangeEvent changeEvent = new ClusteringChangeEvent(this);
		for (final IClusteringChangeListener listener : this.changeListener)
			listener.clusteringChanged(changeEvent);
	}

	@Override
	public boolean removeChangeListener(final IClusteringChangeListener listener) {
		return this.changeListener.remove(listener);
	}

	@Override
	public boolean addClusteringIterationDeletionListener(final IClusteringIterationDeletionListener listener) {
		return this.iterationDeletionListener.add(listener);
	}

	protected void fireClusteringIterationDeleted(final int iteration) {
		final ClusteringIterationDeletedEvent event = new ClusteringIterationDeletedEvent(this, iteration);
		for (final IClusteringIterationDeletionListener listener : this.iterationDeletionListener)
			listener.clusteringIterationDeleted(event);
	}

	@Override
	public boolean removeClusteringIterationDeletionListener(final IClusteringIterationDeletionListener listener) {
		return this.iterationDeletionListener.remove(listener);
	}

	@Override
	public boolean addPValueResultStorageListener(IPValueResultStorageListener listener) {
		return this.pvalueStorageListener.add(listener);
	}

	@Override
	public boolean removePValueResultStorageListener(IPValueResultStorageListener listener) {
		return this.pvalueStorageListener.remove(listener);
	}

	protected void firePValueResultStored(final int iteration, final IPValueCalculationResult pvalueResult,
			final IPValueCalculationResult predecessor) {
		final PValueResultStorageEvent e = new PValueResultStorageEvent(this, iteration, pvalueResult, predecessor);
		for (final IPValueResultStorageListener listener : this.pvalueStorageListener)
			listener.pvalueCalculationResultStored(e);
	}

}