package dk.sdu.imada.ticone.clustering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityFunctionNotDefinedForInputException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.Utility;

/**
 * Created by christian on 05-03-16.
 */
public class CLARAClusteringMethod extends AbstractClusteringMethod<ClusterObjectMapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8529717484705549200L;

	private final int samples, sampleSize;

	private final PAMKClusteringMethodBuilder pamkBuilder;

	CLARAClusteringMethod(final ISimilarityFunction similarityFunction, final IPrototypeBuilder prototypeFactory,
			final int samples, final int sampleSize, final PAMKClusteringMethodBuilder pamkBuilder)
			throws IncompatibleSimilarityFunctionException {
		super(similarityFunction, prototypeFactory);
		this.samples = samples;
		this.sampleSize = sampleSize;
		this.pamkBuilder = pamkBuilder;
	}

	CLARAClusteringMethod(CLARAClusteringMethod other) {
		super(other);
		this.samples = other.samples;
		this.sampleSize = other.sampleSize;
		this.pamkBuilder = other.pamkBuilder.copy();
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction instanceof ISimilarityFunction && similarityFunction instanceof ISimilarityFunction;
	}

	@Override
	public IClusteringMethod<ClusterObjectMapping> copy() {
		return new CLARAClusteringMethod(this);
	}

	public static int recommendedSampleSize(final int n, final int k) {
		if (n <= 250)
			return n;
		return 40 + 2 * k;
	}

	@Override
	protected ClusterObjectMapping doFindClusters(final ITimeSeriesObjects timeSeriesDatas, final int numberOfClusters,
			final long seed) throws ClusterOperationException, InterruptedException {
		final long startTime = System.currentTimeMillis();
		try {
			final Random random = new Random(seed);
			int sampleSize = this.sampleSize;
			int samples = this.samples;
			if (sampleSize < 1)
				sampleSize = recommendedSampleSize(timeSeriesDatas.size(), numberOfClusters);
			if (sampleSize >= timeSeriesDatas.size()) {
				sampleSize = timeSeriesDatas.size();
				samples = 1;
			}
			final PAMKClusteringMethod pamk = pamkBuilder.build();
			final ITimeSeriesObject[] objectArray = timeSeriesDatas.toArray();
			similarities = similarityFunction.emptySimilarities(objectArray, objectArray, new ObjectPairsFactory(),
					ObjectType.OBJECT_PAIR);
			pamk.setSimilarities(similarities);
			ClusterObjectMapping bestClustering = null;
			ISimilarityValue bestSim = similarityFunction.missingValuePlaceholder();
			final Random pamkRandom = new Random(random.nextLong());
			for (int i = 0; i < samples; i++) {
				try {
					final ITimeSeriesObjects sampleData;
					final ITimeSeriesObjects nonSampleData;
					if (sampleSize < timeSeriesDatas.size()) {
						sampleData = this.findSampleData(timeSeriesDatas, sampleSize, random);
						nonSampleData = timeSeriesDatas.copy();
						nonSampleData.removeAll(sampleData);
					} else {
						sampleData = timeSeriesDatas.copy();
						nonSampleData = new TimeSeriesObjectList();
					}
					final ClusterObjectMapping sampleClustering = pamk.findClusters(sampleData,
							Math.min(numberOfClusters, sampleData.size()), pamkRandom.nextLong());
					Objects.requireNonNull(sampleClustering, "clustering received from PAMK was null");
					if (!Utility.getProgress().getStatus())
						throw new InterruptedException();

					sampleClustering.addObjectsToMostSimilarCluster(nonSampleData, this.getSimilarityFunction());

					ISimilarityValue totalSim = similarityFunction.missingValuePlaceholder();
					for (ICluster c : sampleClustering.getClusters()) {
						ISimilarityValue sum = similarityFunction.missingValuePlaceholder();
						for (ISimilarityValue v : c.getSimilarities().values())
							sum = sum.plus(v);
						totalSim = totalSim.plus(sum);
					}

					if (totalSim.greaterThan(bestSim)) {
						bestSim = totalSim;
						bestClustering = sampleClustering;
//						System.out.println("CLARA: (sample=" + i + ")\t" + bestSim.get());
					}
				} catch (final NoComparableSimilarityValuesException | NullPointerException e) {
					if (bestClustering == null && i == samples - 1)
						throw e;
					continue;
				} catch (final ClusterOperationException e) {
					if (!(e.getCause() instanceof SimilarityFunctionNotDefinedForInputException)
							|| (i == samples - 1 && bestClustering == null))
						throw e;
					// try again
				}
			}
			if (!Utility.getProgress().getStatus()) {
				throw new InterruptedException();
			}
			this.logger.debug(String.format("CLARA:\t%d\tN=%d\tk=%d\tsamples=%d\tsampleSize=%d",
					System.currentTimeMillis() - startTime, timeSeriesDatas.size(), numberOfClusters, samples,
					sampleSize));
			if (bestClustering == null)
				Objects.requireNonNull(bestClustering, "clustering received from PAMK was null");
//			System.out.println(bestClustering.asObjectPartition());
//			System.out.println(bestClustering.getUnassignedObjects());
			return bestClustering;
		} catch (SimilarityCalculationException | SimilarityValuesException | IncompatibleSimilarityValueException
				| ClusteringMethodFactoryException | CreateClusteringMethodInstanceFactoryException
				| NoComparableSimilarityValuesException | NullPointerException e) {
			throw new ClusterOperationException(e);
		}
	}

	/**
	 * @return the sampleSize
	 */
	public int getSampleSize() {
		return this.sampleSize;
	}

	/**
	 * @return the pamkMaxIterations
	 */
	public int getPamkMaxIterations() {
		Integer maxIterations = this.pamkBuilder.getSwapIterations();
		return maxIterations != null ? maxIterations : PAMKClusteringMethodBuilder.DEFAULT_SWAPPING_ITERATIONS;
	}

	/**
	 * @return the pamkNstart
	 */
	public int getPamkNstart() {
		Integer nstart = this.pamkBuilder.getNstart();
		return nstart != null ? nstart : PAMKClusteringMethodBuilder.DEFAULT_NSTART;
	}

	/**
	 * @return the samples
	 */
	public int getSamples() {
		return this.samples;
	}

	protected ITimeSeriesObjects findSampleData(final ITimeSeriesObjects timeSeriesDatas, final int sampleSize,
			final Random random) {
		final ITimeSeriesObjectList sampleData = new TimeSeriesObjectList(timeSeriesDatas);
		while (sampleData.size() > sampleSize) {
			final int nextIndex = random.nextInt(sampleData.size());
			sampleData.remove(nextIndex);
		}
		return sampleData;
	}

	@Override
	public String toString() {
		return "CLARA";
	}
}
