/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 28, 2019
 *
 */
public class CLARAClusteringMethodBuilder extends AbstractClusteringMethodBuilder<CLARAClusteringMethod> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3394321983356355187L;

	public static final int DEFAULT_SAMPLES = 5;

	public static final int DEFAULT_SAMPLE_SIZE = -1;

	private int samples, sampleSize;

	private PAMKClusteringMethodBuilder pamkBuilder;

	public CLARAClusteringMethodBuilder() {
		super();
		this.samples = DEFAULT_SAMPLES;
		this.sampleSize = DEFAULT_SAMPLE_SIZE;
	}

	public CLARAClusteringMethodBuilder(CLARAClusteringMethodBuilder other) {
		super(other);
		if (other.pamkBuilder != null) {
			this.pamkBuilder = other.pamkBuilder.copy();
			this.pamkBuilder.setPrototypeBuilder(prototypeBuilder);
		}
		this.samples = other.samples;
		this.sampleSize = other.sampleSize;
	}

	@Override
	public CLARAClusteringMethodBuilder copy() {
		return new CLARAClusteringMethodBuilder(this);
	}

	@Override
	public CLARAClusteringMethod copy(CLARAClusteringMethod clusteringMethod) {
		return new CLARAClusteringMethod(clusteringMethod);
	}

	/**
	 * @param samples the samples to set
	 */
	public CLARAClusteringMethodBuilder setSamples(int samples) {
		this.samples = samples;
		return this;
	}

	/**
	 * @param sampleSize the sampleSize to set
	 */
	public CLARAClusteringMethodBuilder setSampleSize(int sampleSize) {
		this.sampleSize = sampleSize;
		return this;
	}

	/**
	 * @param pamkBuilder the pamkBuilder to set
	 */
	public CLARAClusteringMethodBuilder setPamkBuilder(PAMKClusteringMethodBuilder pamkBuilder) {
		this.pamkBuilder = pamkBuilder;
		if (this.pamkBuilder != null) {
			this.pamkBuilder.setSimilarityFunction(similarityFunction);
			this.pamkBuilder.setPrototypeBuilder(prototypeBuilder);
		}
		return this;
	}
	
	/**
	 * @return the pamkBuilder
	 */
	public PAMKClusteringMethodBuilder getPamkBuilder() {
		return this.pamkBuilder;
	}

	@Override
	public IClusteringMethodBuilder<CLARAClusteringMethod> setSimilarityFunction(
			ISimilarityFunction similarityFunction) {
		super.setSimilarityFunction(similarityFunction);
		if (this.pamkBuilder != null)
			this.pamkBuilder.setSimilarityFunction(similarityFunction);
		return this;
	}

	@Override
	public IClusteringMethodBuilder<CLARAClusteringMethod> setPrototypeBuilder(IPrototypeBuilder prototypeBuilder) {
		super.setPrototypeBuilder(prototypeBuilder);
		if (this.pamkBuilder != null)
			this.pamkBuilder.setPrototypeBuilder(prototypeBuilder);
		return this;
	}

	@Override
	public CLARAClusteringMethod doBuild() throws ClusteringMethodFactoryException,
			CreateClusteringMethodInstanceFactoryException, InterruptedException {
		try {
			return new CLARAClusteringMethod(similarityFunction, prototypeBuilder, samples, sampleSize,
					pamkBuilder != null ? pamkBuilder
							: (PAMKClusteringMethodBuilder) new PAMKClusteringMethodBuilder()
									.setSimilarityFunction(similarityFunction).setPrototypeBuilder(prototypeBuilder));
		} catch (IncompatibleSimilarityFunctionException e) {
			throw new CreateClusteringMethodInstanceFactoryException(e);
		}
	}

	@Override
	protected void reset() {
	}
}
