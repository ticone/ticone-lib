package dk.sdu.imada.ticone.clustering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dk.sdu.imada.ticone.clustering.pair.ClusterPair.ClusterPairsFactory;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.MissingPrototype;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import it.unimi.dsi.fastutil.objects.ObjectCollection;

public class ClusterUtility {

	public static IClusterList getPatternSingleLinkage(final IClusterList clusterList,
			final ISimilarityFunction similarityFunction) throws SimilarityCalculationException, InterruptedException,
			SimilarityValuesException, IncompatibleSimilarityValueException {
		if (clusterList.isEmpty())
			return new ClusterList();

		final IClusterList clustersWithoutPrototype = new ClusterList(), clustersWithPrototype = new ClusterList();
		for (final ICluster c : clusterList)
			if (c.getPrototype() == null || c.getPrototype() instanceof MissingPrototype)
				clustersWithoutPrototype.add(c);
			else
				clustersWithPrototype.add(c);

		final ISimilarityValuesAllPairs<ICluster, ICluster, IClusterPair> similarities = similarityFunction
				.calculateSimilarities(clustersWithPrototype.toArray(), clustersWithPrototype.toArray(),
						new ClusterPairsFactory(), ObjectType.CLUSTER_PAIR);
		final ObjectCollection<int[]> sortedIndices = similarities.decreasingByValue();

		final Map<Integer, List<Integer>> clusterToList = new HashMap<>();
		int idx = 0;
		for (final ICluster c : clusterList) {
			final List<Integer> m = new ArrayList<>();
			final int objectListIndex = idx;
			m.add(objectListIndex);
			clusterToList.put(objectListIndex, m);
			idx++;
		}

		final Set<int[]> processedIndices = new HashSet<>();

		final Iterator<int[]> iterator = sortedIndices.iterator();
		while (clusterToList.size() > 1
				&& clusterToList.values().iterator().next().size() < clustersWithPrototype.size()
				&& iterator.hasNext()) {
			final int[] maxIdx = iterator.next();
			processedIndices.add(maxIdx);
			iterator.remove();

			// we have already processed this cluster pair in the other "orientation"
			// or: the pair is an identity pair
			if (processedIndices.contains(new int[] { maxIdx[1], maxIdx[0] })) {
				continue;
			}

			final List<Integer> list1 = clusterToList.get(maxIdx[0]);
			final List<Integer> list2 = clusterToList.get(maxIdx[1]);

			// both objects are already in the same connected component
			if (list1 == list2) {

			}
			// they are in different components -> link together
			else {
				final int cIdx1 = maxIdx[0];
				final int cIdx2 = maxIdx[1];
				final boolean leftHalfInComp1 = similarities.get(cIdx1, list2.get(0)).get() > similarities
						.get(cIdx1, list2.get(list2.size() - 1)).get();
				final boolean leftHalfInComp2 = similarities.get(cIdx2, list1.get(list1.size() - 1))
						.get() > similarities.get(cIdx2, list1.get(0)).get();

				final List<Integer> linkedComponent = new ArrayList<>();

				if (!leftHalfInComp1 && leftHalfInComp2) {
					// add first comp1, then comp2
					for (final int cIdx : list1)
						linkedComponent.add(cIdx);
					for (final int cIdx : list2)
						linkedComponent.add(cIdx);
				} else if (leftHalfInComp1 && !leftHalfInComp2) {
					// add first comp2, then comp1
					for (final int cIdx : list2)
						linkedComponent.add(cIdx);
					for (final int cIdx : list1)
						linkedComponent.add(cIdx);
				} else if (leftHalfInComp1 && leftHalfInComp2) {
					// reverse comp1; then add comp1, then comp2
					final List<Integer> revComp1 = new ArrayList<>(list1);
					Collections.reverse(revComp1);
					for (final int cIdx : revComp1)
						linkedComponent.add(cIdx);
					for (final int cIdx : list2)
						linkedComponent.add(cIdx);
				} else if (!leftHalfInComp1 && !leftHalfInComp2) {
					// reverse comp2; then add comp1, then comp2
					final List<Integer> revComp2 = new ArrayList<>(list2);
					Collections.reverse(revComp2);
					for (final int cIdx : list1)
						linkedComponent.add(cIdx);
					for (final int cIdx : revComp2)
						linkedComponent.add(cIdx);
				}

				// set all contained clusters to the same list
				for (final int cIdx : linkedComponent) {
					clusterToList.put(cIdx, linkedComponent);
				}
			}
		}

		final List<Integer> completeConnectedComponent = clusterToList.values().iterator().next();
		final ClusterList result = new ClusterList();
		for (final Integer i : completeConnectedComponent)
			result.add(clustersWithPrototype.get(i));
		// append empty clusters to end of list
		result.addAll(clustersWithoutPrototype);
		return result;
	}
}
