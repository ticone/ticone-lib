/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.BasicShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleResultWithMapping;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototype;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.util.Utility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since May 9, 2017
 *
 */
public class ShuffleClusteringByRandomlyAssigningObjectsToClusters extends AbstractShuffle
		implements IShuffleClustering {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1630280216065166164L;

	@Override
	public ObjectType<IClusterObjectMapping> supportedObjectType() {
		return ObjectType.CLUSTERING;
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public ShuffleClusteringByRandomlyAssigningObjectsToClusters copy() {
		final ShuffleClusteringByRandomlyAssigningObjectsToClusters s = new ShuffleClusteringByRandomlyAssigningObjectsToClusters();
		return s;
	}

	@Override
	public ShuffleClusteringByRandomlyAssigningObjectsToClusters newInstance() {
		return new ShuffleClusteringByRandomlyAssigningObjectsToClusters();
	}

	@Override
	public String getName() {
		return "Randomly assign objects to clusters";
	}

	@Override
	public boolean producesShuffleMappingFor(ObjectType<?> type) {
		return type == ObjectType.CLUSTER;
	}

	@Override
	public IShuffleResultWithMapping doShuffle(final IObjectWithFeatures object, final long seed)
			throws ShuffleException, ShuffleNotInitializedException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final IClusterObjectMapping clustering = supportedObjectType().getBaseType().cast(object);
		try {
			final Random random = new Random(seed);
			// randomly assign the objects to clusters of the same size
			final IClusterObjectMapping shuffledClustering = clustering.newInstance(clustering.getAllObjects().asSet());
			final Map<ICluster, ICluster> shuffledClusters = new HashMap<>();
			final IShuffleMapping shuffleMapping = new BasicShuffleMapping();
			for (final ICluster cluster : clustering.getClusters()) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				final ICluster shuffledCluster = shuffledClustering.addCluster(new MissingPrototype());
				shuffledClusters.put(cluster, shuffledCluster);
				shuffleMapping.put(cluster, shuffledCluster);
			}

			final ITimeSeriesObjectList remainingObjects = clustering.getAllObjects().copy().asList();
			for (final ICluster cluster : clustering.getClusters()) {
				final ICluster shuffledCluster = shuffledClusters.get(cluster);
				// randomly assign the same number of objects to the new cluster
				for (int o = 0; o < cluster.getObjects().size(); o++) {
					if (!Utility.getProgress().getStatus())
						throw new InterruptedException();
					final ITimeSeriesObject obj = remainingObjects.remove(random.nextInt(remainingObjects.size()));
					shuffledClustering.addMapping(obj, shuffledCluster, AbstractSimilarityValue.MAX);
				}
				// make sure that we keep all the clusters and return exactly
				// the same number of clusters as we got
				shuffledCluster.setKeep(true);

				shuffledClustering.updatePrototypes();
			}
			final IShuffleResultWithMapping result = new ShuffleResultWithMapping(clustering, shuffledClustering);
			result.setShuffleMapping(shuffleMapping);

			return result;
		} catch (CreatePrototypeInstanceFactoryException | PrototypeFactoryException | ClusterFactoryException
				| CreateClusterInstanceFactoryException | IncompatiblePrototypeException
				| DuplicateMappingForObjectException e) {
			throw new ShuffleException(e);
		}
	}
}
