/**
 * 
 */
package dk.sdu.imada.ticone.clustering.validity;

import dk.sdu.imada.ticone.clustering.ClusterObjectMappingNotFoundException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 5, 2019
 *
 */
public class SilhouetteValue implements IClusterValidityIndex {

	@Override
	public double getValidity(final IClusterObjectMapping clustering, final ISimilarityFunction similarityFunction)
			throws ValidityCalculationException, InterruptedException {
		try {
			if (clustering.getClusters().size() < 2)
				return Double.NaN;

			ITimeSeriesObject[] allObjects = clustering.getAssignedObjects().toArray();
			final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities = similarityFunction
					.calculateSimilarities(allObjects, allObjects, new ObjectPair.ObjectPairsFactory(),
							ObjectType.OBJECT_PAIR);

			double qualityValue = 0.0;

			int c = 0;
			for (ITimeSeriesObject item : allObjects) {
				double qualityOfDatum = getQualityOfDatum(similarities, clustering, item);
				if (!Double.isNaN(qualityOfDatum)) {
					qualityValue += qualityOfDatum;
					c++;
				}

			}
			qualityValue /= c;

			return qualityValue;
		} catch (SimilarityCalculationException | SimilarityValuesException | IncompatibleSimilarityValueException
				| NoComparableSimilarityValuesException | ClusterObjectMappingNotFoundException e) {
			throw new ValidityCalculationException(e);
		}
	}

	private double getQualityOfDatum(
			final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities,
			final IClusterObjectMapping clustering, final ITimeSeriesObject item)
			throws SimilarityCalculationException, SimilarityValuesException, NoComparableSimilarityValuesException,
			InterruptedException, ClusterObjectMappingNotFoundException {
		final ICluster ownCluster = clustering.getSimilarities(item).keySet().iterator().next();

		if (ownCluster.size() < 2)
			return Double.NaN;

		double a = getAverageDissimilarityToFuzzyCluster(similarities, item, ownCluster);
		double b = Double.MAX_VALUE;
		for (ICluster other : clustering.getClusters())
			if (other != ownCluster)
				b = Math.min(b, getAverageDissimilarityToFuzzyCluster(similarities, item, other));

		/*
		 * Avoid division by zero in extreme cases.
		 */
		return (a == b && a == 0 ? 0 : (b - a) / Math.max(a, b));
	}

	private double getAverageDissimilarityToFuzzyCluster(
			final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> similarities,
			final ITimeSeriesObject item, final ICluster cluster)
			throws SimilarityCalculationException, SimilarityValuesException, NoComparableSimilarityValuesException,
			InterruptedException, ClusterObjectMappingNotFoundException {
		double result = 0.0;

		final int object1Index = similarities.getObject1Index(item);

		boolean contained = false;
		for (ITimeSeriesObject otherItem : cluster.getObjects()) {
			if (otherItem == item) {
				contained = true;
				continue;
			}
			result += (similarities.max().get()
					- similarities.get(object1Index, similarities.getObject2Index(otherItem)).get());
		}
		if (contained)
			result /= cluster.size() - 1;
		else
			result /= cluster.size();

		return result;
	}

	@Override
	public String toString() {
		return "Silhouette Value";
	}
}
