package dk.sdu.imada.ticone.clustering.suggestclusters;

import dk.sdu.imada.ticone.clustering.ClusterFactoryException;
import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterOperationException;
import dk.sdu.imada.ticone.clustering.ClusteringMethodFactoryException;
import dk.sdu.imada.ticone.clustering.CreateClusterInstanceFactoryException;
import dk.sdu.imada.ticone.clustering.CreateClusteringMethodInstanceFactoryException;
import dk.sdu.imada.ticone.clustering.DuplicateMappingForObjectException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.TooFewObjectsClusteringException;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueStorageException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.similarity.TimeSeriesNotCompatibleException;
import dk.sdu.imada.ticone.util.ExtractData;

/**
 * This class is used to suggest new patterns, based on the least fitting
 * objects in the current clustering.
 *
 * @author Christian Nørskov
 */
public class SuggestClustersBasedOnLeastFitting implements ISuggestNewCluster {

	// private Map<Pattern, List<TimeSeriesData>> patternsDataToDelete;
	private final int numberOfPatterns;
	private final int percentLeastFitting;
	private final IClusteringMethodBuilder<? extends IClusteringMethod<? extends IClusterObjectMapping>> clusteringMethodBuilder;
	private final ISimilarityFunction similarityFunction;
	private long seed;

	public SuggestClustersBasedOnLeastFitting(final int numberOfPatterns, final int percentLeastFitting,
			final IClusteringMethodBuilder<? extends IClusteringMethod<? extends IClusterObjectMapping>> initialClusteringInterface,
			final ISimilarityFunction similarityFunction, final long seed) {
		this.numberOfPatterns = numberOfPatterns;
		this.percentLeastFitting = percentLeastFitting;
		this.clusteringMethodBuilder = initialClusteringInterface;
		this.similarityFunction = similarityFunction;
		this.seed = seed;
	}

	/**
	 *
	 * @param patternObjectMapping
	 * @return
	 * @throws SuggestClusterException
	 * @throws InterruptedException
	 * @throws TooFewObjectsClusteringException
	 * @throws TimeSeriesNotCompatibleException
	 */
	@Override
	public IClusterSuggestion suggestNewClusters(final IClusterObjectMapping patternObjectMapping)
			throws SuggestClusterException, InterruptedException {
		ITimeSeriesObjects objects;
		try {
			objects = ExtractData.getLeastFittingObjectsFromWholeDataset(patternObjectMapping, this.percentLeastFitting,
					this.similarityFunction);
			if (objects.isEmpty())
				throw new SuggestClusterException("We can only suggest a cluster based on at least 1 object.");
			// findObjects(patternObjectMapping, percentLeastFitting);

			final IClusterObjectMapping pom = this.clusteringMethodBuilder.build().findClusters(objects,
					this.numberOfPatterns, seed);
			final IClusterSuggestion suggestPatternsContainer = new ClusterSuggestion(patternObjectMapping, pom,
					objects);
			// patternObjectMapping.mergeMappings(pom);

			return suggestPatternsContainer;
		} catch (SimilarityCalculationException | ClusterOperationException | SimilarityValuesException
				| IncompatibleObjectTypeException | IncompatibleSimilarityValueException
				| ClusteringMethodFactoryException | CreateClusteringMethodInstanceFactoryException
				| IncompatibleSimilarityValueStorageException e) {
			throw new SuggestClusterException(e);
		}
	}

	/**
	 *
	 * @param clusteringTansition
	 * @return
	 * @throws SuggestClusterException
	 */
	@Override
	public IClusterObjectMapping applyNewClusters(final IClusterSuggestion clusteringTansition)
			throws SuggestClusterException {
		try {
			final IClusterObjectMapping oldMapping = clusteringTansition.getOldClusterObjectMapping();
			final IClusterObjectMapping newMapping = clusteringTansition.getNewClusterObjectMapping();
			// Map<Pattern, List<TimeSeriesData>> patternsDataToDelete =
			// clusteringTansition.getPatternsDataToDelete();
			final ITimeSeriesObjects objectsToDelete = clusteringTansition.getObjectsToDelete();

			this.removePatternsAndObjectsFromNewMapping(clusteringTansition);

			this.deleteObjectsFromOldMapping(objectsToDelete, oldMapping);

			oldMapping.mergeMappings(newMapping);

			return oldMapping;
		} catch (CreateClusterInstanceFactoryException | InterruptedException | ClusterFactoryException
				| DuplicateMappingForObjectException e) {
			throw new SuggestClusterException(e);
		}

	}

	private void removePatternsAndObjectsFromNewMapping(final IClusterSuggestion clusteringTransition) {
		final IClusterObjectMapping newMapping = clusteringTransition.getNewClusterObjectMapping();
		final ITimeSeriesObjects objectsToDelete = clusteringTransition.getObjectsToDelete();

		final IClusterList patternsToNotKeep = new ClusterList();
		for (final ICluster pattern : newMapping.getClusters()) {
			if (!clusteringTransition.getKeepStatusForPattern(pattern)) {
				patternsToNotKeep.add(pattern);
			}
		}

		for (final ICluster pattern : patternsToNotKeep) {
			final ITimeSeriesObjects objects = pattern.getObjects();
			objectsToDelete.removeAll(objects);
			newMapping.removeData(pattern.getClusterNumber(),
					dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);
		}
	}

	private void deleteObjectsFromOldMapping(final ITimeSeriesObjects objectsToDelete,
			final IClusterObjectMapping oldMapping) {
		for (final ICluster pattern : oldMapping.getClusters()) {
			oldMapping.removeObjectsFromCluster(pattern, objectsToDelete);
		}

	}

	/**
	 * Find the objects that is least fitting (globally).
	 * 
	 * @param patternObjectMapping the object mappings to find the objects in
	 * @param percentLeastFitting  the percent of objects that should be reclustered
	 * @return returns the list of least fitting objects.
	 */
	/*
	 * private List<TimeSeriesData> findObjects(IClusterObjectMapping
	 * patternObjectMapping, int percentLeastFitting) {
	 * 
	 * Iterator<Pattern> patternIterator = patternObjectMapping.patternIterator();
	 * List<TimeSeriesData> objects = new ArrayList<TimeSeriesData>();
	 * patternsDataToDelete = new HashMap<Pattern, List<TimeSeriesData>>(); while
	 * (patternIterator.hasNext()) { Pattern pattern = patternIterator.next();
	 * List<TimeSeriesData> objectsToDelete =
	 * ExtractData.getLeastFittingDataFromPattern(patternObjectMapping, pattern,
	 * percentLeastFitting); if (objectsToDelete == null) { continue; }
	 * objects.addAll(objectsToDelete); patternsDataToDelete.put(pattern,
	 * objectsToDelete); }
	 * 
	 * return objects; }
	 */
}
