package dk.sdu.imada.ticone.clustering.splitpattern;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import dk.sdu.imada.ticone.clustering.ClusterFactoryException;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.CreateClusterInstanceFactoryException;
import dk.sdu.imada.ticone.clustering.DuplicateMappingForObjectException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair;
import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.similarity.TimeSeriesNotCompatibleException;

/**
 * This class is used to split a given pattern, by taking the two least similar
 * objects, and using these as candidates for new patterns.
 *
 * @author Christian Wiwie
 * @author Christian Nørskov
 */
public class SplitClusterBasedOnTwoMostDissimilarObjects extends AbstractSplitCluster {

	private final ISimilarityFunction similarityFunction;
	private final IPrototypeBuilder prototypeBuilder;

	/**
	 *
	 * @param simFunc
	 * @param IDiscretizePattern
	 * @param clustering
	 */
	public SplitClusterBasedOnTwoMostDissimilarObjects(final ISimilarityFunction simFunc,
			final IPrototypeBuilder prototypeFactory) {
		super();
		this.similarityFunction = simFunc;
		this.prototypeBuilder = prototypeFactory;
	}

	/**
	 *
	 * @param pattern
	 * @return
	 * @throws SplitClusterException
	 * @throws InterruptedException
	 * @throws TimeSeriesNotCompatibleException
	 */
	@Override
	public SplitClusterContainer splitClusterInternal(final ICluster pattern)
			throws SplitClusterException, InterruptedException {
		final ITimeSeriesObjects timeSeriesDataArrayList = pattern.getObjects();

		final ClusterObjectMapping newPatternsPOM = new ClusterObjectMapping(timeSeriesDataArrayList.asSet().copy(),
				prototypeBuilder);

		// Deletes current pattern
		// patternObjectMapping.deleteData(pattern,
		// IClusterObjectMapping.DELETE_BOTH_PATTERN_AND_OBJECTS);

		ITimeSeriesObjects candidates;
		try {
			candidates = this.findMostDissimilarObjects(timeSeriesDataArrayList);

			final Map<ITimeSeriesObject, ITimeSeriesObjects> patterns = this.assignDataToNearest(candidates,
					timeSeriesDataArrayList);

			this.addPatternsToObjectMapping(newPatternsPOM, patterns);

			final SplitClusterContainer splitPatternContainer = new SplitClusterContainer(pattern, newPatternsPOM,
					candidates);

			this.setPatternParent(splitPatternContainer);

			return splitPatternContainer;
		} catch (SimilarityCalculationException | CreatePrototypeInstanceFactoryException | PrototypeFactoryException
				| SimilarityValuesException | NoComparableSimilarityValuesException
				| CreateClusterInstanceFactoryException | ClusterFactoryException | IncompatibleObjectTypeException
				| IncompatibleSimilarityValueException | DuplicateMappingForObjectException e) {
			throw new SplitClusterException(e);
		}
	}

	@Override
	public IClusterObjectMapping applyNewClusters(final ISplitClusterContainer splitPatternContainer)
			throws SplitClusterException {
		try {
			final ICluster pattern = splitPatternContainer.getOldPattern();

			this.clustering.removeData(pattern.getClusterNumber(),
					IClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);

			final IClusterObjectMapping newPatterns = splitPatternContainer.getNewClusters();

			this.clustering.mergeMappings(newPatterns);

			return this.clustering;
		} catch (CreateClusterInstanceFactoryException | InterruptedException | ClusterFactoryException
				| DuplicateMappingForObjectException e) {
			throw new SplitClusterException(e);
		}
	}

	private void setPatternParent(final ISplitClusterContainer splitPatternContainer) {
		final ICluster parent = splitPatternContainer.getOldPattern();

		for (final ICluster child : splitPatternContainer.getNewClusters().getClusters()) {
			child.setParent(parent);
		}
	}

	private ITimeSeriesObjects findMostDissimilarObjects(final ITimeSeriesObjects timeSeriesDataArrayList)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			NoComparableSimilarityValuesException, IncompatibleSimilarityValueException {
		IObjectPair pair;
		final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> sims = this.similarityFunction
				.calculateSimilarities(timeSeriesDataArrayList.toArray(), timeSeriesDataArrayList.toArray(),
						new ObjectPairsFactory(), ObjectType.OBJECT_PAIR);
		pair = sims.getPair(sims.whichMin());
		final ITimeSeriesObjects tempList = new TimeSeriesObjectList();
		tempList.add(pair.getFirst());
		tempList.add(pair.getSecond());
		return tempList;
	}

	private Map<ITimeSeriesObject, ITimeSeriesObjects> assignDataToNearest(final ITimeSeriesObjects candidates,
			final ITimeSeriesObjects timeSeriesDataArrayList)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			NoComparableSimilarityValuesException, IncompatibleSimilarityValueException {
		final Map<ITimeSeriesObject, ITimeSeriesObjects> patterns = new HashMap<>();

		// Creating the lists for the two candidates
		for (final ITimeSeriesObject timeSeriesData : candidates) {
			patterns.put(timeSeriesData, new TimeSeriesObjectList());
			patterns.get(timeSeriesData).add(timeSeriesData);
		}

		// Assigning to the nearest candidate
		for (final ITimeSeriesObject timeSeriesData : timeSeriesDataArrayList) {
			if (patterns.containsKey(timeSeriesData)) {
				continue;
			}
			IObjectPair pair;
			final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> sims = this.similarityFunction

					.calculateSimilarities(timeSeriesData.asSingletonList().toArray(), candidates.toArray(),
							new ObjectPairsFactory(), ObjectType.OBJECT_PAIR);
			pair = sims.getPair(sims.whichMax());
			final ITimeSeriesObject nearest = pair.getSecond();
			patterns.get(nearest).add(timeSeriesData);
		}

		return patterns;
	}

	private void addPatternsToObjectMapping(final ClusterObjectMapping patternObjectMapping,
			final Map<ITimeSeriesObject, ITimeSeriesObjects> newClusters) throws SimilarityCalculationException,
			CreatePrototypeInstanceFactoryException, PrototypeFactoryException, InterruptedException,
			CreateClusterInstanceFactoryException, ClusterFactoryException, IncompatibleObjectTypeException,
			SimilarityValuesException, IncompatibleSimilarityValueException, DuplicateMappingForObjectException {

		final Iterator<ITimeSeriesObject> clusterIterator = newClusters.keySet().iterator();

		while (clusterIterator.hasNext()) {
			final ITimeSeriesObject prototype = clusterIterator.next();
			final ITimeSeriesObjects objects = newClusters.get(prototype);
			final ICluster cluster = patternObjectMapping.addCluster(objects, similarityFunction);
			for (final ITimeSeriesObject object : objects) {
				final ISimilarityValue sim = this.similarityFunction
						.calculateSimilarity(new ObjectClusterPair(object, cluster));
				patternObjectMapping.addMapping(object, cluster, sim);
			}
		}
	}
}
