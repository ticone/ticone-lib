/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Random;

import dk.sdu.imada.ticone.clustering.prototype.permute.IShuffleTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ShuffleTimeSeries;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.permute.BasicShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.permute.ShuffleResultWithMapping;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototype;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.Utility;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public class ShuffleClusteringByShufflingPrototypeTimeSeries
		extends AbstractShuffleClusteringWithSimilarityReassignment {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1755871987744257117L;
	protected ShuffleTimeSeries shuffleTimeSeries;

	ShuffleClusteringByShufflingPrototypeTimeSeries(final ShuffleClusteringByShufflingPrototypeTimeSeries s) {
		super(s);
		this.shuffleTimeSeries = s.shuffleTimeSeries.copy();
	}

	public ShuffleClusteringByShufflingPrototypeTimeSeries(final ISimilarityFunction simFunc,
			final IPrototypeBuilder prototypeFactory, final ShuffleTimeSeries shufflePrototype)
			throws IncompatibleSimilarityFunctionException {
		super(simFunc);
		this.shuffleTimeSeries = shufflePrototype;
	}

	@Override
	public ObjectType<IClusterObjectMapping> supportedObjectType() {
		return ObjectType.CLUSTERING;
	}

	@Override
	public boolean validateParameters() {
		return true;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.shuffleTimeSeries == null)
			throw new ShuffleNotInitializedException("shufflePrototype");
		return super.validateInitialized();
	}

	/**
	 * @return the shuffleDataset
	 */
	public IShuffleTimeSeries getShuffleTimeSeries() {
		return this.shuffleTimeSeries;
	}

	@Override
	public ShuffleClusteringByShufflingPrototypeTimeSeries copy() {
		return new ShuffleClusteringByShufflingPrototypeTimeSeries(this);
	}

	@Override
	public ShuffleClusteringByShufflingPrototypeTimeSeries newInstance() {
		return new ShuffleClusteringByShufflingPrototypeTimeSeries(this);
	}

	@Override
	public String getName() {
		return "Shuffle prototype time series";
	}

	@Override
	public boolean producesShuffleMappingFor(final ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResultWithMapping doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final Random random = new Random(seed);
		final IClusterObjectMapping clustering = supportedObjectType().getBaseType().cast(object);
		final IClusterObjectMapping shuffledClustering = clustering.newInstance(clustering.getAllObjects().asSet());
		final IPrototypeBuilder prototypeBuilder = shuffledClustering.getPrototypeBuilder();
		// shuffle all prototypes
		try {
			final IShuffleMapping shuffleMapping = new BasicShuffleMapping();
			for (final ICluster cluster : clustering.getClusters()) {
				if (!Utility.getProgress().getStatus())
					throw new InterruptedException();
				IPrototype shuffledPrototype;
				try {
					final ITimeSeries timeSeries = PrototypeComponentType.TIME_SERIES
							.getComponent(cluster.getPrototype()).getTimeSeries();
					final IShuffleResult shuffleResult = this.shuffleTimeSeries.shuffle(timeSeries, random.nextLong());
					final ITimeSeries shuffledTS = timeSeries.getObjectType().getBaseType()
							.cast(shuffleResult.getShuffled());
					synchronized (prototypeBuilder) {
						shuffledPrototype = prototypeBuilder.copy(cluster.getPrototype(),
								PrototypeComponentType.TIME_SERIES.getComponentFactory(prototypeBuilder)
										.setTimeSeries(shuffledTS).build());
					}
				} catch (MissingPrototypeException | MissingPrototypeFactoryException e) {
					shuffledPrototype = new MissingPrototype();
				}
				final ICluster shuffledCluster = shuffledClustering.addCluster(shuffledPrototype);
				// make sure that we keep all the clusters and return exactly
				// the same number of clusters as we got
				shuffledCluster.setKeep(true);
				shuffleMapping.put(cluster, shuffledCluster);
			}
			try {
				shuffledClustering.removeDuplicatePrototypes(true);
				shuffledClustering.addObjectsToMostSimilarCluster(clustering.getAllObjects(), getSimilarityFunction());
				shuffledClustering.removeEmptyClusters(true);
				shuffledClustering.updatePrototypes();
				final IShuffleResultWithMapping result = new ShuffleResultWithMapping(clustering, shuffledClustering);
//				result.setShuffleMapping(shuffleMapping);
				return result;
			} catch (SimilarityCalculationException | SimilarityValuesException
					| NoComparableSimilarityValuesException e) {
				throw new ShuffleException(e);
			}
		} catch (IncompatiblePrototypeComponentException | PrototypeFactoryException
				| PrototypeComponentFactoryException | ClusterFactoryException | CreateInstanceFactoryException
				| IncompatibleSimilarityValueException | IncompatiblePrototypeException e) {
			throw new ShuffleException(e);
		}
	}
}
