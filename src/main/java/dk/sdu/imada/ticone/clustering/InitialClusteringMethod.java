package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

public class InitialClusteringMethod extends AbstractInitialClusteringProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7205178018374314482L;
	protected IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder;
	protected int numberOfClusters;
	private long seed;

	/**
	 * 
	 */
	public InitialClusteringMethod() {
		super();
	}

	public InitialClusteringMethod(
			final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder,
			final int numberOfClusters, final long seed) {
		super();
		this.clusteringMethodBuilder = clusteringMethodBuilder;
		this.numberOfClusters = numberOfClusters;
		this.seed = seed;
	}

	/**
	 * @param clusteringMethodBuilder the clusteringMethod to set
	 */
	public void setClusteringMethod(
			IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder) {
		this.clusteringMethodBuilder = clusteringMethodBuilder;
	}

	@Override
	public ClusterObjectMapping getInitialClustering(final ITimeSeriesObjects timeSeriesDatas)
			throws ClusterOperationException, InterruptedException {
		try {
			return this.clusteringMethodBuilder.build().findClusters(timeSeriesDatas, this.numberOfClusters, seed);
		} catch (ClusteringMethodFactoryException | CreateClusteringMethodInstanceFactoryException e) {
			throw new ClusterOperationException(e);
		}
	}

	@Override
	public String toString() {
		return "Use clustering method";
	}

	@Override
	public int getInitialNumberOfClusters() {
		return this.numberOfClusters;
	}

	/**
	 * @param numberOfClusters the numberOfClusters to set
	 */
	public void setInitialNumberOfClusters(int numberOfClusters) {
		this.numberOfClusters = numberOfClusters;
	}

	/**
	 * @param seed the seed to set
	 */
	public void setSeed(long seed) {
		this.seed = seed;
	}
}
