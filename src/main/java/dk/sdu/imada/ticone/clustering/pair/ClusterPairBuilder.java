/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IPairBuilder;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 15, 2018
 *
 */
public class ClusterPairBuilder extends AbstractBuilder<IClusterPair, FactoryException, CreateInstanceFactoryException>
		implements IPairBuilder<ICluster, ICluster, IClusterPair> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4810704713644673393L;
	protected transient ICluster first, second;

	@Override
	public ClusterPairBuilder copy() {
		return new ClusterPairBuilder().setFirst(this.first).setSecond(this.second);
	}

	@Override
	public IClusterPair build(ICluster first, ICluster second) {
		return new ClusterPair(first, second);
	}

	@Override
	protected IClusterPair doBuild() {
		return new ClusterPair(this.first, this.second);
	}

	@Override
	protected void reset() {
		first = null;
		second = null;
	}

	@Override
	public ClusterPairBuilder setFirst(final ICluster first) {
		this.first = first;
		return this;
	}

	@Override
	public ClusterPairBuilder setSecond(final ICluster second) {
		this.second = second;
		return this;
	}

}
