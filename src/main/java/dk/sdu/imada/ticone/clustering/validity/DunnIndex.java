/**
 * 
 */
package dk.sdu.imada.ticone.clustering.validity;

import java.io.Serializable;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 6, 2019
 *
 */
public class DunnIndex implements IClusterValidityIndex, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7926212090708417991L;

	@Override
	public double getValidity(final IClusterObjectMapping clustering, final ISimilarityFunction similarityFunction)
			throws ValidityCalculationException, InterruptedException {
		try {
			ICluster[] clusters = clustering.getClusters().toArray();
			if (clusters.length < 2)
				return Double.NaN;

			final double minSim = similarityFunction
					.calculateSimilarities(clustering.getObjectClusterPairs().asList(), ObjectType.OBJECT_CLUSTER_PAIR)
					.min().get();

			final ISimilarityValuesAllPairs<ICluster, ICluster, IClusterPair> sims = similarityFunction
					.calculateSimilarities(clusters, clusters, new ClusterPair.ClusterPairsFactory(),
							ObjectType.CLUSTER_PAIR);

			final double maxInterClusterSim = sims.filter((c1, c2, p) -> c1 != c2).max().get();

			return maxInterClusterSim / minSim;
		} catch (SimilarityCalculationException | SimilarityValuesException | NoComparableSimilarityValuesException
				| IncompatibleSimilarityValueException e) {
			throw new ValidityCalculationException(e);
		}
	}

	@Override
	public String toString() {
		return "Dunn Index";
	}
}
