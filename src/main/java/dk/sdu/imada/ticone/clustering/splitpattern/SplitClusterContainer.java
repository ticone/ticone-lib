package dk.sdu.imada.ticone.clustering.splitpattern;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * This class is used as a container for a splitted pattern, before it is
 * applied to the clustering.
 * <p>
 * It holds the old pattern, and the new patterns and the assignments (in a
 * <tt>IClusterObjectMapping</tt>).
 * </p>
 *
 * @author Christian Nørskov
 */
public class SplitClusterContainer implements ISplitClusterContainer {

	private final ICluster oldPattern;
	private final ClusterObjectMapping newPatterns;
	private final ITimeSeriesObjects candidates;

	public SplitClusterContainer(final ICluster oldPattern, final ClusterObjectMapping newPatterns,
			final ITimeSeriesObjects candidates) {
		this.oldPattern = oldPattern;
		this.newPatterns = newPatterns;
		this.candidates = candidates;
	}

	@Override
	public ICluster getOldPattern() {
		return this.oldPattern;
	}

	@Override
	public ClusterObjectMapping getNewClusters() {
		return this.newPatterns;
	}

	@Override
	public ITimeSeriesObjects getCandidates() {
		return this.candidates;
	}
}
