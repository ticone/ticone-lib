/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 15, 2019
 *
 */
public abstract class AbstractInitialClusteringProvider implements IInitialClusteringProvider<ClusterObjectMapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2162555382855502096L;

	@Override
	public abstract ClusterObjectMapping getInitialClustering(ITimeSeriesObjects timeSeriesDatas)
			throws ClusterOperationException, InterruptedException;

}
