package dk.sdu.imada.ticone.clustering;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;

import dk.sdu.imada.ticone.util.AbstractTiconeResult;
import dk.sdu.imada.ticone.util.INamedTiconeResult;
import dk.sdu.imada.ticone.util.ITiconeResultNameChangeListener;
import dk.sdu.imada.ticone.util.TiconeResultNameChangedEvent;

public abstract class AbstractNamedTiconeResult extends AbstractTiconeResult implements INamedTiconeResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5831137542225691710L;
	/**
	 * Clustering objects
	 */
	protected String name;

	protected transient Set<ITiconeResultNameChangeListener> nameChangeListener;

	public AbstractNamedTiconeResult() {
		super();
		this.nameChangeListener = new HashSet<>();
	}

	public AbstractNamedTiconeResult(final AbstractNamedTiconeResult result) {
		super(result);
		this.nameChangeListener = result.nameChangeListener;
		this.name = result.name;
	}

	protected Set<ITiconeResultNameChangeListener> getNameChangeListener() {
		// older deserialized results
		if (this.nameChangeListener == null)
			this.nameChangeListener = new HashSet<>();
		return this.nameChangeListener;
	}

	@Override
	public String getName() {
		if (this.name == null)
			this.initName();
		return this.name;
	}

	protected abstract void initName();

	@Override
	public void setName(final String name) {
		this.name = name;
		this.fireNameChanged();
	}

	@Override
	public void addNameChangeListener(final ITiconeResultNameChangeListener l) {
		this.getNameChangeListener().add(l);
	}

	protected void fireNameChanged() {
		final TiconeResultNameChangedEvent event = new TiconeResultNameChangedEvent(this, this.name);
		for (final ITiconeResultNameChangeListener l : this.getNameChangeListener()) {
			l.resultNameChanged(event);
		}
	}

	public void clearNameChangeListener() {
		if (this.nameChangeListener == null)
			this.nameChangeListener = new HashSet<>();
		else
			this.nameChangeListener.clear();
	}

	@Override
	public void destroy() {
		super.destroy();
		this.clearNameChangeListener();
	}

	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		nameChangeListener = new HashSet<>();
	}

	@Override
	public void removeNameChangeListener(ITiconeResultNameChangeListener listener) {
		this.nameChangeListener.remove(listener);
	}

}