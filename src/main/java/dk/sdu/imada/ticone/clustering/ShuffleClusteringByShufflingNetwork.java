/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ObjectPair;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.network.IShuffleNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.permute.BasicShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder.IPrototypeComponent;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.INetworkBasedSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 1, 2019
 *
 */
public class ShuffleClusteringByShufflingNetwork extends AbstractShuffleClusteringWithSimilarityReassignment {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4712272713870991402L;
	protected ITiconeNetwork<?, ?> network;
	protected IShuffleNetwork shuffleNetwork;
	protected IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder;
	protected boolean randomNumberClusters;

	public ShuffleClusteringByShufflingNetwork(final ShuffleClusteringByShufflingNetwork s) {
		super(s);
		this.clusteringMethodBuilder = s.clusteringMethodBuilder.copy();
		this.shuffleNetwork = s.shuffleNetwork.copy();
		this.randomNumberClusters = s.randomNumberClusters;
		this.network = s.network;
	}

	public ShuffleClusteringByShufflingNetwork(
			final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder,
			final IShuffleNetwork shuffleNetwork) throws IncompatibleSimilarityFunctionException {
		super(clusteringMethodBuilder.getSimilarityFunction());
		this.clusteringMethodBuilder = clusteringMethodBuilder;
		this.shuffleNetwork = shuffleNetwork;
	}

	@Override
	public ShuffleClusteringByShufflingNetwork copy() {
		return new ShuffleClusteringByShufflingNetwork(this);
	}

	@Override
	public boolean validateParameters() {
		// TODO: number of clusters?
		return true;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.shuffleNetwork == null)
			throw new ShuffleNotInitializedException("shuffleNetwork");
		if (this.network == null)
			throw new ShuffleNotInitializedException("network");
		if (this.clusteringMethodBuilder == null)
			throw new ShuffleNotInitializedException("clusteringMethod");
		return super.validateInitialized();
	}

	@Override
	public ShuffleClusteringByShufflingNetwork newInstance() {
		// TODO: do we need to deep clone some stuff?
		ShuffleClusteringByShufflingNetwork shuffleClusteringByShufflingDataset = new ShuffleClusteringByShufflingNetwork(
				this);
		shuffleClusteringByShufflingDataset.randomNumberClusters = this.randomNumberClusters;
		return shuffleClusteringByShufflingDataset;
	}

	public IShuffleNetwork getShuffleDataset() {
		return this.shuffleNetwork;
	}

	@Override
	public String getName() {
		return this.shuffleNetwork.getName();
	}

	@Override
	public boolean producesShuffleMappingFor(final ObjectType<?> type) {
		return false;
	}

	@Override
	public IShuffleResult doShuffle(IObjectWithFeatures object, long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException {
		final IClusterObjectMapping clustering = supportedObjectType().getBaseType().cast(object).copy();
		final IShuffleResult shuffleResult = this.shuffleNetwork.shuffle(network, seed);
		ITiconeNetwork<?, ?> shuffledNetwork = (ITiconeNetwork) shuffleResult.getShuffled();
		final ITimeSeriesObjectList mappedObjects = clustering.getAllObjects().mapToNetwork(shuffledNetwork);
		try {
			final Random random = new Random(seed);
			final IClusterList originalClusters = clustering.getClusters().copy().asList();
			int maxNumberClusters = mappedObjects.size();
			int numberClusters;
			if (!randomNumberClusters)
				numberClusters = originalClusters.size();
			else {
				numberClusters = (int) Math
						.abs(Math.round(random.nextGaussian() * maxNumberClusters * 0.05 + originalClusters.size()));
				numberClusters = Math.max(Math.min(maxNumberClusters, numberClusters), 1);
			}

			if (this.similarityFunction instanceof INetworkBasedSimilarityFunction) {
				((INetworkBasedSimilarityFunction) this.similarityFunction).setNetwork(shuffledNetwork);
				this.similarityFunction.initialize();
			}
			final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethodBuilder = this.clusteringMethodBuilder
					.copy();
			clusteringMethodBuilder.setSimilarityFunction(this.similarityFunction);

			if (clusteringMethodBuilder.getPrototypeBuilder().getPrototypeComponentFactories()
					.containsKey(PrototypeComponentType.NETWORK_LOCATION)) {
				IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?> nlPCB = clusteringMethodBuilder
						.getPrototypeBuilder().getPrototypeComponentFactories()
						.get(PrototypeComponentType.NETWORK_LOCATION);
				IAggregateCluster<?> aggregateCluster = nlPCB.getAggregationFunction();
				if (aggregateCluster instanceof AggregateClusterMedoidNode) {
					((AggregateClusterMedoidNode) aggregateCluster).setNetwork(shuffledNetwork);
					ITimeSeriesObject[] objects = mappedObjects.toArray();
					((AggregateClusterMedoidNode) aggregateCluster)
							.setSimilarities(this.similarityFunction.emptySimilarities(objects, objects,
									new ObjectPair.ObjectPairsFactory(), ObjectType.OBJECT_PAIR));
				}
			}

			final IClusteringMethod<ClusterObjectMapping> clusteringMethod = clusteringMethodBuilder.build();

			final ClusterObjectMapping shuffledClustering = clusteringMethod.findClusters(mappedObjects, numberClusters,
					seed);
			final Map<IObjectWithFeatures, IObjectWithFeatures> shuffledSupport = new HashMap<>();
			shuffledSupport.put(this.network, shuffledNetwork);
			return new BasicShuffleResult(clustering, shuffledClustering, shuffledSupport);
		} catch (final ClusterOperationException | ClusteringMethodFactoryException
				| CreateClusteringMethodInstanceFactoryException | IncompatibleSimilarityFunctionException e) {
			throw new ShuffleException(e);
		}
	}

	@Override
	public void cleanUpShuffle(IShuffleResult shuffleResult) {
		// remove mapping to random network
		if (shuffleResult.hasShuffledSupport() && shuffleResult.getShuffledSupport().containsKey(this.network)) {
			final IObjectWithFeatures object = shuffleResult.getOriginal();
			final IClusterObjectMapping clustering = supportedObjectType().getBaseType().cast(object);
			final ITimeSeriesObjectList allObjects = clustering.getAllObjects();
			allObjects.removeMappingToNetwork(
					(ITiconeNetwork<?, ?>) shuffleResult.getShuffledSupport().get(this.network));
		}
	}

	public IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> getClusteringMethodBuilder() {
		return this.clusteringMethodBuilder;
	}

	/**
	 * @param randomNumberClusters the randomNumberClusters to set
	 */
	public void setRandomNumberClusters(boolean randomNumberClusters) {
		this.randomNumberClusters = randomNumberClusters;
	}

	@Override
	public void setSimilarityFunction(ISimilarityFunction similarityFunction) {
		super.setSimilarityFunction(similarityFunction);
		if (clusteringMethodBuilder != null)
			clusteringMethodBuilder.setSimilarityFunction(similarityFunction);
	}

	/**
	 * @param network the network to set
	 */
	public void setNetwork(ITiconeNetwork<?, ?> network) {
		this.network = network;
	}

	/**
	 * @return the network
	 */
	public ITiconeNetwork<?, ?> getNetwork() {
		return this.network;
	}
}
