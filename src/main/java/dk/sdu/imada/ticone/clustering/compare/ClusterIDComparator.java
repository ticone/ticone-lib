package dk.sdu.imada.ticone.clustering.compare;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.FeatureComparisonException;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

public class ClusterIDComparator extends AbstractTiconeComparator {

	@Override
	public int compare(IObjectWithFeatures o1, IObjectWithFeatures o2) {
		if (!(o1 instanceof ICluster && o2 instanceof ICluster))
			throw new FeatureComparisonException("This comparator can only compare ICluster objects.");
		final int statusComp = super.compareStatus(o1, o2);
		if (statusComp != 0)
			return statusComp;

		if (this.decreasing && this.reverseComparator) {
			final ICluster tmp = (ICluster) o1;
			o1 = o2;
			o2 = tmp;
		}

		if (o1.getName().length() != o2.getName().length())
			return o1.getName().length() - o2.getName().length();
		return o1.getName().compareTo(o2.getName());
	}

	@Override
	public String toString() {
		return "Cluster ID";
	}
}