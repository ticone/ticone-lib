/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.AbstractShuffle;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public abstract class AbstractShuffleClusteringWithSimilarityReassignment extends AbstractShuffle
		implements IShuffleClustering {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6249207519451159283L;

	protected ISimilarityFunction similarityFunction;

	protected transient Logger logger = LoggerFactory.getLogger(this.getClass());

	public AbstractShuffleClusteringWithSimilarityReassignment(final ISimilarityFunction simFunc)
			throws IncompatibleSimilarityFunctionException {
		super();
		this.similarityFunction = simFunc;
	}

	public AbstractShuffleClusteringWithSimilarityReassignment(
			final AbstractShuffleClusteringWithSimilarityReassignment s) {
		super();

		this.similarityFunction = s.similarityFunction.copy();
	}

	@Override
	public ObjectType<IClusterObjectMapping> supportedObjectType() {
		return ObjectType.CLUSTERING;
	}

	@Override
	public boolean validateInitialized() throws ShuffleNotInitializedException {
		if (this.similarityFunction == null)
			throw new ShuffleNotInitializedException("similarityFunction");
		return true;
	}

	/**
	 * @return the similarityFunction
	 */
	public ISimilarityFunction getSimilarityFunction() {
		return this.similarityFunction;
	}

	/**
	 * @param similarityFunction the similarityFunction to set
	 */
	public void setSimilarityFunction(ISimilarityFunction similarityFunction) {
		this.similarityFunction = similarityFunction;
	}
}