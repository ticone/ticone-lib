/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;

import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.pair.ClusterPairBuilder;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPairs;
import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectProviderManager;
import dk.sdu.imada.ticone.util.ObjectSampleException;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 2, 2018
 *
 */
public class ClusterList extends ObjectArrayList<ICluster> implements IClusterList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3360457850416722092L;
	protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);
	protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

	/**
	 * 
	 */
	public ClusterList() {
		super();
	}

	public ClusterList(final ICluster... clusters) {
		super(clusters);
	}

	public ClusterList(final Collection<ICluster> clusters) {
		super(clusters);
	}

	@Override
	public ICluster[] toArray() {
		final ICluster[] res = new ICluster[this.size];
		System.arraycopy(this.a, 0, res, 0, this.size);
		return res;
	}

	@Override
	public ClusterSet asSet() {
		return new ClusterSet(new LinkedHashSet<>(this));
	}

	@Override
	public ClusterList copy() {
		return new ClusterList(this);
	}

	@Override
	public FeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public ObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}

	@Override
	public IClusterPairs getClusterPairs() {
		return new ClusterPair.ClusterPairsFactory().createList(this, this);
	}

	@Override
	public <T extends IObjectWithFeatures> List<T> sampleObjectsOfType(ObjectType<T> type, int sampleSize, long seed)
			throws IncompatibleObjectProviderException, ObjectSampleException {
		final Random random = new Random(seed);
		if (type == ObjectType.CLUSTER) {
			final int[] indices = random.ints(sampleSize, 0, this.size()).toArray();
			final List<ICluster> result = new ArrayList<>();
			for (int i : indices)
				result.add(this.get(i));
			return (List<T>) result;
		} else if (type == ObjectType.CLUSTER_PAIR) {
			final int[] indices1 = random.ints(sampleSize, 0, this.size()).toArray();
			final int[] indices2 = random.ints(sampleSize, 0, this.size()).toArray();
			final List<IClusterPair> result = new ArrayList<>();
			final ClusterPairBuilder clusterPairBuilder = new ClusterPairBuilder();
			for (int p = 0; p < sampleSize; p++)
				result.add(clusterPairBuilder.build(this.get(indices1[p]), this.get(indices2[p])));
			return (List<T>) result;
		}
		throw new ObjectSampleException();
	}
}
