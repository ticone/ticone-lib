/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.MyParallel;
import dk.sdu.imada.ticone.util.MyScheduledThreadPoolExecutor;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 5, 2018
 *
 */
public abstract class ParallelizedClusteringMethod<C extends IClusterObjectMapping>
		extends AbstractClusteringMethod<C> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5243683644331154345L;

	private final boolean isCopy;

	transient ScheduledThreadPoolExecutor privateClusteringMethodThreadPool;

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	public ParallelizedClusteringMethod(final ISimilarityFunction similarityFunction,
			final IPrototypeBuilder prototypeBuilder) throws IncompatibleSimilarityFunctionException {
		super(similarityFunction, prototypeBuilder);
		this.isCopy = false;
		this.initThreadPool();
	}

	ParallelizedClusteringMethod(ParallelizedClusteringMethod<C> other) {
		super(other);
		this.isCopy = true;
		this.privateClusteringMethodThreadPool = other.privateClusteringMethodThreadPool;
	}

	private void initThreadPool() {
		this.privateClusteringMethodThreadPool = new MyScheduledThreadPoolExecutor(MyParallel.DEFAULT_THREADS,
				new MyThreadFactory(this.getClass()));
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		s.defaultReadObject();
		initThreadPool();
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		this.privateClusteringMethodThreadPool = null;
//		if (this.privateClusteringMethodThreadPool != null && !isCopy)
//			this.privateClusteringMethodThreadPool.shutdown();
	}

	/**
	 * @return the privateClusteringMethodThreadPool
	 */
	protected ScheduledThreadPoolExecutor getPrivateClusteringMethodThreadPool() {
		return this.privateClusteringMethodThreadPool;
	}
}

class MyThreadFactory implements ThreadFactory {

	ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();

	private final Class<? extends ParallelizedClusteringMethod> c;

	public MyThreadFactory(final Class<? extends ParallelizedClusteringMethod> c) {
		this.c = c;
	}

	@Override
	public Thread newThread(Runnable r) {
		Thread t = defaultThreadFactory.newThread(r);

		t.setName(t.getName().replace("pool-", String.format("%s-pool-", c.getSimpleName())));

		return t;
	}
}
