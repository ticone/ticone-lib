package dk.sdu.imada.ticone.clustering.splitpattern;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;

/**
 * Created by christian on 8/24/15.
 */
public abstract class AbstractSplitCluster implements ISplitCluster {

	protected IClusterObjectMapping clustering;

	public AbstractSplitCluster() {
		super();
	}

	@Override
	public void setClustering(IClusterObjectMapping clustering) {
		this.clustering = clustering;
	}

	@Override
	public final SplitClusterContainer splitCluster(final ICluster cluster)
			throws SplitClusterException, InterruptedException {
		if (cluster.getObjects().size() < 2) {
			throw new SplitClusterException("Only cluster with at least 2 objects can be split.");
		}
		return this.splitClusterInternal(cluster);
	}

	protected abstract SplitClusterContainer splitClusterInternal(ICluster cluster)
			throws SplitClusterException, InterruptedException;

	@Override
	public abstract IClusterObjectMapping applyNewClusters(ISplitClusterContainer splitPatternContainer)
			throws SplitClusterException;
}
