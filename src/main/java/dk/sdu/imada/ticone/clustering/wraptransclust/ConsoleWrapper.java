package dk.sdu.imada.ticone.clustering.wraptransclust;

import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Logger;

/**
 * Created by christian on 21-03-16.
 */
import de.costmatrixcreation.main.Config;
import de.layclust.taskmanaging.InvalidInputFileException;
import de.layclust.taskmanaging.TaskConfig;
import de.layclust.taskmanaging.io.ArgsParseException;

public class ConsoleWrapper {
	private static Logger log = Logger.getLogger(ConsoleWrapper.class.getName());
	private String[] args = null;

	public ConsoleWrapper(final String[] args) throws InvalidInputFileException, ArgsParseException, IOException {
		this.args = args;
		log.fine("Available processors in system: " + Runtime.getRuntime().availableProcessors());
		this.parseArgsAndInitProgram();
	}

	private void parseArgsAndInitProgram() throws InvalidInputFileException, ArgsParseException, IOException {
		final boolean inputAndOutputGiven = this.findAndReadConfigIfGivenAndSetMode();
		this.initGivenParameters();
		final Logger logger = Logger.getLogger("");

		final Handler[] handler = logger.getHandlers();
		final Handler[] var7 = handler;
		final int var6 = handler.length;

		final ClusteringManagerTaskWrapper var8 = new ClusteringManagerTaskWrapper();
		var8.run();

	}

	private void initGivenParameters() throws ArgsParseException, IOException {
		int i = 0;
		String key = null;
		String value = null;

		while (i < this.args.length) {
			key = this.args[i].trim();
			if (key.equals("-verbose")) {
				value = "true";
				this.setParameter(key, value);
				++i;
			} else {
				value = this.args[i + 1].trim();

				this.setParameter(key, value);
				i += 2;
			}
		}

	}

	private void setParameter(final String key, final String value) throws ArgsParseException {
		if (key.equals("-i")) {
			TaskConfig.cmPath = value;
		} else if (key.equals("-o")) {

			TaskConfig.clustersPath = value;
		} else if (key.equals("-verbose")) {
			TaskConfig.verbose = Boolean.parseBoolean(value);
		} else {
			final int e;
			if (key.equals("-mode")) {
				TaskConfig.mode = Integer.parseInt(value);
			} else if (key.equals("-sim")) {
				Config.similarityFile = value;
			} else if (key.equals("-minT")) {
				TaskConfig.minThreshold = Double.parseDouble(value);
			} else if (key.equals("-maxT")) {
				TaskConfig.maxThreshold = Double.parseDouble(value);
			} else if (key.equals("-tss")) {
				TaskConfig.thresholdStepSize = Double.parseDouble(value);
			}
		}
	}

	private boolean findAndReadConfigIfGivenAndSetMode() throws InvalidInputFileException, ArgsParseException {
		boolean input = false;
		boolean output = false;
		final String configPath = "Default.conf";

		for (int ex = 0; ex < this.args.length; ++ex) {
			if (this.args[ex].trim().equals("-i")) {
				input = true;
				++ex;
			}

			if (this.args[ex].trim().equals("-o")) {
				output = true;
				++ex;
			}

			String configrb;
			if (this.args[ex].trim().equals("-mode")) {
				configrb = this.args[ex + 1].trim();

				TaskConfig.mode = 2;

				++ex;
			}

		}

		if (input && output) {
			return true;
		} else {
			return false;
		}
	}
}
