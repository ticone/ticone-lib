package dk.sdu.imada.ticone.clustering.wraptransclust;

import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

import de.layclust.iterativeclustering.IteratorThread;
import de.layclust.taskmanaging.ClusteringManager;
import de.layclust.taskmanaging.gui.TransClustGui;

/**
 * Created by christian on 21-03-16.
 */
public class ClusteringManagerTaskWrapper {
	private static Logger log = Logger.getLogger(ConsoleWrapper.class.getName());
	private Semaphore semaphore;
	private ClusteringManager clusterManager;
	private TransClustGui gui;

	public ClusteringManagerTaskWrapper() {
	}

	public void run() {
		try {
			log.info("-----------------------------------");
			log.info("Running ... TransClust v1.0");
			final Date e = new Date(System.currentTimeMillis());
			log.info(e.toString());
			log.info("-----------------------------------");
			final long var13;

			final IteratorThread info = new IteratorThread();
			info.start();
			// This is the only line added.... Other wise the method would end
			// before the thread stopped.
			info.join();
		} catch (final Exception var12) {
			log.severe("ERROR occured in run with the following message: " + var12.getMessage());
			var12.printStackTrace();
		}

	}
}
