/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Collection;
import java.util.LinkedHashSet;

import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPairs;
import dk.sdu.imada.ticone.feature.FeatureValueSampleManager;
import dk.sdu.imada.ticone.util.ObjectProviderManager;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 12, 2018
 *
 */
public class ClusterSet extends LinkedHashSet<ICluster> implements IClusterSet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4706328852242111873L;
	protected FeatureValueSampleManager featureValueSampleManager = new FeatureValueSampleManager(this);
	protected ObjectProviderManager objectProviderManager = new ObjectProviderManager(this);

	/**
	 * 
	 */
	public ClusterSet() {
		super();
	}

	public ClusterSet(final ICluster... clusters) {
		this();
		for (final ICluster o : clusters)
			this.add(o);
	}

	public ClusterSet(final Collection<ICluster> clusters) {
		this();
		this.addAll(clusters);
	}

	@Override
	public ICluster[] toArray() {
		return this.asList().toArray();
	}

	@Override
	public ClusterSet asSet() {
		return new ClusterSet(new LinkedHashSet<>(this));
	}

	@Override
	public ClusterSet copy() {
		return new ClusterSet(this);
	}

	@Override
	public ClusterList asList() {
		return new ClusterList(this);
	}

	@Override
	public FeatureValueSampleManager getFeatureValueSampleManager() {
		return this.featureValueSampleManager;
	}

	@Override
	public ObjectProviderManager getObjectProviderManager() {
		return this.objectProviderManager;
	}

	@Override
	public IClusterPairs getClusterPairs() {
		final ICluster[] array = this.toArray();
		return new ClusterPair.ClusterPairsFactory().createIterable(array, array);
	}
}
