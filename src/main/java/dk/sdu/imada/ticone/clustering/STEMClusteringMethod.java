package dk.sdu.imada.ticone.clustering;

import java.util.Arrays;

import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.Utility;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;

/**
 * Created by christian on 02-03-16.
 */
public class STEMClusteringMethod extends AbstractClusteringMethod<ClusterObjectMapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2044980634822900818L;
	private IClusterList allPatterns = null;
	private Long2ObjectMap<ISimilarityValue> similarityMap;
	private ClusterObjectMapping patternObjectMapping;
	private final int timepoints;
	private ITimeSeriesObjectList objects;

	STEMClusteringMethod(final ISimilarityFunction similarity, final IPrototypeBuilder prototypeFactory,
			final int timepoints) throws IncompatibleSimilarityFunctionException {
		super(similarity, prototypeFactory);
		this.similarityMap = new Long2ObjectOpenHashMap<>();
		this.timepoints = timepoints;
	}

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	STEMClusteringMethod(STEMClusteringMethod other) {
		super(other);
		this.similarityMap = new Long2ObjectOpenHashMap<>(other.similarityMap);
		this.timepoints = other.timepoints;
	}

	@Override
	protected boolean isValidSimilarityFunction(ISimilarityFunction similarityFunction) {
		return similarityFunction.providesValuesForObjectTypes()
				.containsAll(Arrays.asList(ObjectType.OBJECT_CLUSTER_PAIR, ObjectType.CLUSTER_PAIR));
	}

	@Override
	public IClusteringMethod<ClusterObjectMapping> copy() {
		return new STEMClusteringMethod(this);
	}

	@Override
	protected ClusterObjectMapping doFindClusters(final ITimeSeriesObjects objects, final int numberOfClusters,
			final long seed) throws ClusterOperationException, InterruptedException {
		this.objects = objects.asList();
		try {
			final IPrototypeBuilder prototypeBuilder = this.prototypeBuilder.copy();
			if (this.allPatterns == null) {
				this.allPatterns = this.generateAllPossiblePatterns(PrototypeComponentType.TIME_SERIES
						.getComponentFactory(prototypeBuilder).getDiscretizeFunction().getDiscretizationValues(),
						this.timepoints);
			}
			this.patternObjectMapping = new ClusterObjectMapping(objects.asSet().copy(), prototypeBuilder);
			final IClusterList patterns = this.selectPatterns(numberOfClusters, prototypeBuilder);

			final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> sims = this
					.getSimilarityFunction().calculateSimilarities(objects.toArray(), patterns.toArray(),
							new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR);

			final int[] whichMaxForFirstObject = sims.whichMaxForFirstObject(sims.firstObjectIndices());
			int p = 0;
			for (final int objectIdx : sims.firstObjectIndices()) {
				final int whichMax = whichMaxForFirstObject[p++];
				final ISimilarityValue bestSim = sims.maxForFirstObject(objectIdx);
				final ICluster bestCluster = sims.getObject2(whichMax);

				this.addMapping(bestCluster, sims.getObject1(objectIdx), bestSim);
			}
		} catch (SimilarityCalculationException | PrototypeFactoryException | PrototypeComponentFactoryException
				| IncompatiblePrototypeComponentException | MissingPrototypeException | MissingPrototypeFactoryException
				| SimilarityValuesException | NoComparableSimilarityValuesException | ClusterFactoryException
				| CreateInstanceFactoryException | IncompatibleObjectTypeException
				| IncompatibleSimilarityValueException | IncompatiblePrototypeException
				| DuplicateMappingForObjectException e) {
			throw new ClusterOperationException(e);
		}
		if (!Utility.getProgress().getStatus()) {
			throw new InterruptedException();
		}
		return this.patternObjectMapping;
	}

	private synchronized void addMapping(final ICluster pattern, final ITimeSeriesObject timeSeriesData,
			final ISimilarityValue similarity) throws DuplicateMappingForObjectException {
		this.patternObjectMapping.addMapping(timeSeriesData, pattern, similarity);
	}

	private IClusterList generateAllPossiblePatterns(final double[] values, final int timepoints)
			throws InterruptedException, PrototypeFactoryException, PrototypeComponentFactoryException,
			IncompatiblePrototypeComponentException, MissingPrototypeFactoryException, ClusterFactoryException,
			CreateClusterInstanceFactoryException, CreateInstanceFactoryException, IncompatiblePrototypeException {
		return this.generatePatterns(timepoints, values, prototypeBuilder);
	}

	public IClusterList generatePatterns(final int timepoints, final double[] values,
			final IPrototypeBuilder prototypeBuilder)
			throws InterruptedException, PrototypeFactoryException, PrototypeComponentFactoryException,
			IncompatiblePrototypeComponentException, MissingPrototypeFactoryException, ClusterFactoryException,
			CreateClusterInstanceFactoryException, CreateInstanceFactoryException, IncompatiblePrototypeException {
		final double[] pat = new double[timepoints];
		final ClusterObjectMapping patterns = new ClusterObjectMapping(objects.asSet().copy(), prototypeBuilder);
		this.generatePermutations(patterns, timepoints, 0, pat, values, prototypeBuilder);
		return patterns.clusters.asList();
	}

	private void generatePermutations(final ClusterObjectMapping patterns, final int timepoints, final int currentIndex,
			final double[] pattern, final double[] values, final IPrototypeBuilder prototypeBuilder)
			throws InterruptedException, PrototypeFactoryException, PrototypeComponentFactoryException,
			IncompatiblePrototypeComponentException, MissingPrototypeFactoryException, ClusterFactoryException,
			CreateClusterInstanceFactoryException, CreateInstanceFactoryException, IncompatiblePrototypeException {
		if (!Utility.getProgress().getStatus()) {
			logger.info("Number of patterns generated: " + patterns.getClusters().size());
			throw new InterruptedException("Stopped");
		}
		if (currentIndex == timepoints) {
			final double[] pat = new double[timepoints];
			for (int i = 0; i < timepoints; i++) {
				pat[i] = pattern[i];
			}
			patterns.addCluster(this.prototypeBuilder.setComponents(PrototypeComponentType.TIME_SERIES
					.getComponentFactory(this.prototypeBuilder).setTimeSeries(new TimeSeries(pat)).build()).build());
			return;
		}
		for (int i = 0; i < values.length; i++) {
			pattern[currentIndex] = values[i];
			this.generatePermutations(patterns, timepoints, currentIndex + 1, pattern, values, prototypeBuilder);
		}
	}

	private IClusterList selectPatterns(final int k, final IPrototypeBuilder prototypeBuilder)
			throws InterruptedException, SimilarityCalculationException, PrototypeFactoryException,
			PrototypeComponentFactoryException, IncompatiblePrototypeComponentException, MissingPrototypeException,
			MissingPrototypeFactoryException, ClusterFactoryException, CreateInstanceFactoryException,
			IncompatibleObjectTypeException, IncompatiblePrototypeException {
		this.similarityMap = new Long2ObjectOpenHashMap<>();
		final IClusterList tempPatterns = new ClusterList(this.allPatterns);
		final IClusterList selectedPatterns = new ClusterList();
		if (k > this.allPatterns.size()) {
			return this.allPatterns;
		}
		final int timepoints = PrototypeComponentType.TIME_SERIES.getComponent(this.allPatterns.get(0).getPrototype())
				.getTimeSeries().getNumberTimePoints();
		final double[] values = PrototypeComponentType.TIME_SERIES.getComponentFactory(prototypeBuilder)
				.getDiscretizeFunction().getDiscretizationValues();
		final double[] p1arr = new double[timepoints];
		for (int i = 0; i < timepoints; i++) {
			p1arr[i] = values[Math.max(values.length - 1 - i, 0)];
		}
		final IPrototype newPrototype = prototypeBuilder.setComponents(PrototypeComponentType.TIME_SERIES
				.getComponentFactory(prototypeBuilder).setTimeSeries(new TimeSeries(p1arr)).build()).build();
		ICluster p1 = this.patternObjectMapping.addCluster(newPrototype);
		selectedPatterns.add(p1);
		this.removeFirstPatternFromList(p1arr, tempPatterns);

		for (int i = 0; i < k - 1; i++) {
			if (!Utility.getProgress().getStatus()) {
				throw new InterruptedException("Stopped");
			}
			ISimilarityValue minSim = AbstractSimilarityValue.MAX;
			int bestIndex = -1;
			for (int j = 0; j < tempPatterns.size(); j++) {
				p1 = tempPatterns.get(j);
				ISimilarityValue sim = this.findMaximumSimilarity(selectedPatterns, p1);
				if (minSim.compareTo(sim) > 0) {
					minSim = sim;
					bestIndex = j;
				}
			}
			selectedPatterns.add(this.patternObjectMapping.addCluster(tempPatterns.get(bestIndex).getPrototype()));
			tempPatterns.remove(bestIndex);
		}
		return selectedPatterns;
	}

	private ISimilarityValue findMaximumSimilarity(final IClusterList selectedPatterns, final ICluster p1)
			throws InterruptedException, SimilarityCalculationException, IncompatibleObjectTypeException {
		ISimilarityValue maxSim = AbstractSimilarityValue.MIN;
		for (int i = 0; i < selectedPatterns.size(); i++) {
			if (!Utility.getProgress().getStatus()) {
				throw new InterruptedException("Stopped");
			}
			final ICluster p2 = selectedPatterns.get(i);
			final ISimilarityValue sim = this.findSimilarity(p1, p2);
			if (maxSim.compareTo(sim) < 0) {
				maxSim = sim;
			}
		}
		return maxSim;
	}

	private ISimilarityValue findSimilarity(final ICluster p1, final ICluster p2)
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		final long key = this.getKey(p1.getClusterNumber(), p2.getClusterNumber());
		if (this.similarityMap.containsKey(key)) {
			return this.similarityMap.get(key);
		} else {
			final ISimilarityValue similarity = this.getSimilarityFunction()
					.calculateSimilarity(new ClusterPair(p1, p2));
			this.similarityMap.put(key, similarity);
			return similarity;
		}
	}

	/**
	 * Generates the key for two objects.
	 * 
	 * @param obj1
	 * @param obj2
	 * @return returns the key for the two objects
	 */
	private long getKey(final int obj1, final int obj2) {
		int high, low;
		if (obj1 > obj2) {
			high = obj1;
			low = obj2;
		} else {
			high = obj2;
			low = obj1;
		}
		return (long) low << 32 | high & 0xFFFFFFFL;
	}

	private void removeFirstPatternFromList(final double[] p1, final IClusterList patternList)
			throws IncompatiblePrototypeComponentException {
		for (int i = 0; i < patternList.size(); i++) {
			try {
				final double[] p2 = PrototypeComponentType.TIME_SERIES.getComponent(patternList.get(i).getPrototype())
						.getTimeSeries().asArray();
				boolean identical = true;
				for (int j = 0; j < p2.length; j++) {
					if (p1[j] != p2[j]) {
						identical = false;
						break;
					}
				}
				if (identical) {
					patternList.remove(i);
					return;
				}
			} catch (final MissingPrototypeException e) {
				continue;
			}
		}
	}

	@Override
	public String toString() {
		return "STEM";
	}
}
