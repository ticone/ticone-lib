/**
 * 
 */
package dk.sdu.imada.ticone.clustering.mergeclusters;

import java.util.Arrays;

import dk.sdu.imada.ticone.clustering.DuplicateMappingForObjectException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;

/**
 * Merges a collection of clusters into one.
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 13, 2019
 *
 */
public class MergeSelectedClusters implements IMergeClusters {

	final protected IClusters clustersToMerge;

	final protected ISimilarityFunction similarityFunction;

	public MergeSelectedClusters(IClusters clustersToMerge, ISimilarityFunction similarityFunc) {
		super();
		this.clustersToMerge = clustersToMerge;
		this.similarityFunction = similarityFunc;
	}

	@Override
	public <C extends IClusterObjectMapping> ClusterMergeResult<C> doMerge(C clustering)
			throws ClusterMergeException, InterruptedException {
		try {
			final ITimeSeriesObjects newPatternsData = new TimeSeriesObjectList();

			for (final ICluster pattern : clustersToMerge) {
				newPatternsData.addAll(pattern.getObjects());
			}

			removeOldClusters(clustersToMerge, clustering);

			final ICluster pattern = clustering.addCluster(newPatternsData, similarityFunction);

			addMappings(pattern, newPatternsData, clustering);

			return new ClusterMergeResult<>(clustering, Arrays.asList(clustersToMerge));
		} catch (final SimilarityCalculationException | CreateInstanceFactoryException | FactoryException
				| IncompatibleObjectTypeException | SimilarityValuesException | IncompatibleSimilarityValueException
				| DuplicateMappingForObjectException e) {
			throw new ClusterMergeException(e);
		}
	}

	private void addMappings(final ICluster cluster, final ITimeSeriesObjects objects,
			final IClusterObjectMapping clustering) throws SimilarityCalculationException, InterruptedException,
			IncompatibleObjectTypeException, DuplicateMappingForObjectException {
		for (final ITimeSeriesObject object : objects) {
			final ISimilarityValue similarity = similarityFunction
					.calculateSimilarity(new ObjectClusterPair(object, cluster));
			clustering.addMapping(object, cluster, similarity);
		}
	}

	private void removeOldClusters(final IClusters clusters, final IClusterObjectMapping clustering) {
		for (final ICluster c : clusters) {
			clustering.removeData(c.getClusterNumber(),
					dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS);
		}

	}

}
