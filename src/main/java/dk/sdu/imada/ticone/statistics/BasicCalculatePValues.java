/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.swing.event.ChangeListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wiwie.wiutils.format.Formatter;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.ISimilarityFeature;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.FitnessCalculationException;
import dk.sdu.imada.ticone.fitness.FitnessScoreInitializationException;
import dk.sdu.imada.ticone.fitness.FitnessScoreNotInitializedException;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessValue;
import dk.sdu.imada.ticone.fitness.IncompatibleFitnessScoreAndObjectException;
import dk.sdu.imada.ticone.network.IShuffleNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.permute.IPermutable;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.permute.IShuffleMapping;
import dk.sdu.imada.ticone.permute.IShuffleResult;
import dk.sdu.imada.ticone.permute.IShuffleResultWithMapping;
import dk.sdu.imada.ticone.permute.IncompatibleShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleException;
import dk.sdu.imada.ticone.permute.ShuffleNotInitializedException;
import dk.sdu.imada.ticone.similarity.INetworkBasedSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.FitnessScorePValues;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.OriginalFitnessValues;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PValues;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PermutedFeatureValues;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PermutedFeatureValuesObjectSpecific;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PermutedFitnessValues;
import dk.sdu.imada.ticone.statistics.PValueCalculationResult.PermutedFitnessValuesObjectSpecific;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.Iterables;
import dk.sdu.imada.ticone.util.MyParallel;
import dk.sdu.imada.ticone.util.MyParallel.BatchCount;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;
import dk.sdu.imada.ticone.util.Utility;
import it.unimi.dsi.fastutil.ints.IntAVLTreeSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 19, 2017
 *
 */
public class BasicCalculatePValues implements ICalculatePValues {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1962378569253591040L;

	protected transient Logger logger;
	protected transient List<ChangeListener> listener;

	protected final List<ObjectType<?>> objectTypes;
	protected boolean isCancelled;
	protected IShuffle shuffle;
	protected IFeatureStore featureStore;
	protected Map<IFeatureValueProvider, Set<IFeatureWithValueProvider<?>>> featureValueProviders;
	protected int permutations;
	protected AtomicInteger finishedPermutations;
	protected boolean forceDisableObjectSpecificDistribution;
	protected boolean[] calculateObjectSpecificDistributions;

	protected List<IFitnessScore> fitnessScores;
	protected List<Boolean> fitnessScoreTwoSided;
	protected final List<IArithmeticFeature<? extends Comparable<?>>>[] conditionalFeatures;
	protected ICombinePValues combinePValues;

	protected boolean storePermutedFitnessValues;
	protected boolean storePermutedFeatureValues;
	protected boolean storePermutedObjectInFeatureValues;

	/**
	 * 
	 */
	public BasicCalculatePValues(final List<ObjectType<?>> objectTypes, final IShuffle shuffle,
			final List<IFitnessScore> fitnessScores,
			final List<IArithmeticFeature<? extends Comparable<?>>>[] conditionalFeatures,
			final List<Boolean> fitnessScoreTwoSided, final ICombinePValues combinePValues, final int permutations) {
		super();
		if (objectTypes.size() != conditionalFeatures.length)
			throw new IllegalArgumentException("Please specify conditional features for each object type");
		if (fitnessScores.size() != fitnessScoreTwoSided.size())
			throw new IllegalArgumentException("Please specify for each fitness score whether it is two or one sided.");
		this.objectTypes = objectTypes;
		this.setShuffle(shuffle);
		this.permutations = permutations;
		this.finishedPermutations = new AtomicInteger();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.listener = new ArrayList<>();

		this.combinePValues = combinePValues;
		this.fitnessScores = fitnessScores;
		this.conditionalFeatures = conditionalFeatures;
		this.fitnessScoreTwoSided = fitnessScoreTwoSided;
	}

	/**
	 * @param featureStore the featureStore to set
	 */
	@Override
	public void setFeatureStore(IFeatureStore featureStore) {
		this.featureStore = featureStore;
	}

	/**
	 * @param forceDisableObjectSpecificDistribution the
	 *                                               forceDisableObjectSpecificDistribution
	 *                                               to set
	 */
	@Override
	public void setForceDisableObjectSpecificDistribution(boolean forceDisableObjectSpecificDistribution) {
		this.forceDisableObjectSpecificDistribution = forceDisableObjectSpecificDistribution;
		if (this.calculateObjectSpecificDistributions != null)
			for (int i = 0; i < this.calculateObjectSpecificDistributions.length; i++)
				this.calculateObjectSpecificDistributions[i] = false;
	}

	/**
	 * @return the forceDisableObjectSpecificDistribution
	 */
	@Override
	public boolean isForceDisableObjectSpecificDistribution() {
		return this.forceDisableObjectSpecificDistribution;
	}

	/**
	 * @param storePermutedFitnessValues the storePermutedFitnessValues to set
	 */
	public void setStorePermutedFeatureValues(final boolean storePermutedFitnessValues) {
		this.storePermutedFeatureValues = storePermutedFitnessValues;
	}

	/**
	 * @param storePermutedFitnessValues the storePermutedFitnessValues to set
	 */
	public void setStorePermutedFitnessValues(final boolean storePermutedFitnessValues) {
		this.storePermutedFitnessValues = storePermutedFitnessValues;
	}

	/**
	 * @param storePermutedObjectInFeatureValues the
	 *                                           storePermutedObjectInFeatureValues
	 *                                           to set
	 */
	public void setStorePermutedObjectInFeatureValues(boolean storePermutedObjectInFeatureValues) {
		this.storePermutedObjectInFeatureValues = storePermutedObjectInFeatureValues;
	}

	@Override
	public boolean isCancelled() {
		return this.isCancelled;
	}

	@Override
	public void cancel() {
		this.isCancelled = true;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(this.getClass().equals(obj.getClass())))
			return false;
		final BasicCalculatePValues other = (BasicCalculatePValues) obj;
		// TODO
		return other.permutations == this.permutations && Objects.equals(this.fitnessScores, other.fitnessScores);
	}

	@Override
	public int hashCode() {
		// TODO
		return this.permutations + this.fitnessScores.hashCode();
	}

	@Override
	public void addChangeListener(final ChangeListener listener) {
		this.listener.add(listener);
	}

	public void clearChangeListener() {
		if (this.listener == null)
			this.listener = new ArrayList<>();
		else
			this.listener.clear();
	}

	@Override
	public void removeChangeListener(final ChangeListener listener) {
		this.listener.remove(listener);
	}

	protected void fireStateChanged(final int totalPermutations, final int finishedPermutations) {
		for (final ChangeListener l : this.listener)
			l.stateChanged(new PermutationTestChangeEvent(this, totalPermutations, finishedPermutations));
	}

	@Override
	public int getNumberPermutations() {
		return this.permutations;
	}

	/**
	 * @return the finishedPermutations
	 */
	@Override
	public int getNumberFinishedPermutations() {
		return this.finishedPermutations.get();
	}

	/**
	 * @return the shuffleClustering
	 */
	@Override
	public IShuffle getShuffle() {
		return this.shuffle;
	}

	/**
	 * @param shuffle the shuffle to set
	 */
	@Override
	public void setShuffle(IShuffle shuffle) {
		this.shuffle = shuffle;
		this.calculateObjectSpecificDistributions = new boolean[objectTypes.size()];
		if (this.forceDisableObjectSpecificDistribution)
			return;
		for (int i = 0; i < objectTypes.size(); i++)
			this.calculateObjectSpecificDistributions[i] = this.shuffle.producesShuffleMappingFor(objectTypes.get(i));
	}

	@Override
	public List<ObjectType<?>> getObjectTypes() {
		return this.objectTypes;
	}

	protected List<IObjectWithFeatures> getObjectsFromProvider(final IObjectProvider provider)
			throws IncompatibleObjectProviderException {
		final List<IObjectWithFeatures> result = new ArrayList<>();
		for (ObjectType<?> c : getObjectTypes())
			result.addAll(provider.getObjectsOfType(c));
		return result;
	}

	/**
	 * @param originalObject
	 * @param shuffledObject
	 * @param conditionalFeatures
	 * @return Whether the given shuffled object passes the conditional.
	 * @throws ToNumberConversionException
	 * @throws NotAnArithmeticFeatureValueException
	 */
	protected boolean pValueConditional(IObjectWithFeatures originalObject, IObjectWithFeatures shuffledObject,
			IFeatureStore featureValuesOriginal, IFeatureStore featureValuesShuffled,
			IArithmeticFeature<? extends Comparable<?>>[] conditionalFeatures)
			throws NotAnArithmeticFeatureValueException, ToNumberConversionException {
		if (this.conditionalFeatures == null)
			return true;
		for (IArithmeticFeature<? extends Comparable<?>> f : conditionalFeatures) {
			final IFeatureValue<? extends Comparable<?>> origValue = featureValuesOriginal
					.getFeatureValue(originalObject, f);
			final IFeatureValue<? extends Comparable<?>> shuffledValue = featureValuesShuffled
					.getFeatureValue(shuffledObject, f);
			if (Double.compare(shuffledValue.toNumber().doubleValue(), origValue.toNumber().doubleValue()) != 0)
				return false;
		}
		return true;
	}

	/**
	 * 
	 * Initialize required fields of features.
	 * 
	 * @param permutation
	 * @param fitnessScore
	 * @param shuffledObjectList
	 * @param shuffledFeatureValueProviders
	 * @param random
	 * @return
	 * @throws PValueCalculationException
	 * @throws InterruptedException
	 */
	protected IArithmeticFeature<? extends Comparable<?>>[] initializeFeaturesForPermutation(int permutation,
			IArithmeticFeature<? extends Comparable<?>>[] features, final IShuffleResult shuffleResult,
			IObjectProvider shuffledObjectList,
			final Map<CanShuffleFeatureValueProvider, IFeatureValueProvider> shuffledFeatureValueProviders,
			final long seed) throws PValueCalculationException, InterruptedException {
		try {
			for (IArithmeticFeature<? extends Comparable<?>> f : features) {
				f.setStoreObjectInValues(this.storePermutedObjectInFeatureValues);
				f.setStoreFeatureInValues(this.storePermutedFeatureValues);
				f.setUseCache(false);

				if (f instanceof ISimilarityFeature) {
					final ISimilarityFunction similarityFunction = ((ISimilarityFeature) f).getSimilarityFunction();
					if (similarityFunction instanceof INetworkBasedSimilarityFunction) {
						final ITiconeNetwork<?, ?> prevNetwork = ((INetworkBasedSimilarityFunction) similarityFunction)
								.getNetwork();
						if (shuffleResult.getOriginal() == prevNetwork)
							((INetworkBasedSimilarityFunction) similarityFunction)
									.setNetwork((ITiconeNetwork<?, ?>) shuffleResult.getShuffled());
						else if (shuffleResult.getShuffledSupport().containsKey(prevNetwork))
							((INetworkBasedSimilarityFunction) similarityFunction).setNetwork(
									(ITiconeNetwork<?, ?>) shuffleResult.getShuffledSupport().get(prevNetwork));
					}
				}
			}

			for (CanShuffleFeatureValueProvider shuffled : shuffledFeatureValueProviders.keySet()) {
				if (!shuffled.isTransitive()) {
					for (IArithmeticFeature<? extends Comparable<?>> f : features) {
						if (f instanceof IFeatureWithValueProvider) {
							IFeatureValueProvider featureValueProvider = ((IFeatureWithValueProvider) f)
									.getFeatureValueProvider();

							if (shuffled.provider == featureValueProvider)
								((IFeatureWithValueProvider) f)
										.setFeatureValueProvider(shuffledFeatureValueProviders.get(shuffled));
						}
					}
				} else {
					IFeatureValueProvider copy = shuffled.provider.copy();
					copy.updateDependency(shuffled.providerDependency, shuffledFeatureValueProviders.get(shuffled));

					for (IArithmeticFeature<? extends Comparable<?>> f : features) {
						if (f instanceof IFeatureWithValueProvider) {
							IFeatureValueProvider featureValueProvider = ((IFeatureWithValueProvider) f)
									.getFeatureValueProvider();

							if (shuffled.provider.equals(featureValueProvider)) {
								((IFeatureWithValueProvider) f).setFeatureValueProvider(copy);
							}
						}
					}
				}
			}
		} catch (IncompatibleFeatureValueProviderException e) {
			throw new PValueCalculationException(e);
		}

		return features;
	}

	protected IShuffle getShuffleForPermutation(final int permutation) {
		return this.shuffle.copy();
	}

	protected Map<IObjectWithFeatures, IFitnessValue> calculateOriginalObjectFitness(final IFitnessScore fitnessScore,
			final IObjectProvider provider)
			throws PValueCalculationException, InterruptedException, IncompatibleFitnessScoreAndObjectException,
			FitnessScoreInitializationException, IncompatibleObjectProviderException,
			IncompatibleFeatureAndObjectException, FeatureCalculationException, IncorrectlyInitializedException {
		Map<IObjectWithFeatures, IFitnessValue> originalFitnessValues = new HashMap<>();
		// original fitness scores
		try {
			fitnessScore.setObjectsAndFeatureStore(provider, this.featureStore);
			for (final IObjectWithFeatures c : provider.getObjectsOfType(fitnessScore.getObjectType())) {
				originalFitnessValues.put(c, fitnessScore.calculateFitness(c));
			}

			return originalFitnessValues;
		} catch (FitnessScoreNotInitializedException | FitnessScoreInitializationException | FitnessCalculationException
				| IncompatibleObjectProviderException e) {
			throw new PValueCalculationException(e);
		}
	}

	private static class CanShuffleFeatureValueProvider {
		final IFeatureValueProvider provider, providerDependency;

		public CanShuffleFeatureValueProvider(IFeatureValueProvider provider) {
			super();
			this.provider = provider;
			this.providerDependency = null;
		}

		public CanShuffleFeatureValueProvider(IFeatureValueProvider provider,
				IFeatureValueProvider providerDependency) {
			super();
			this.provider = provider;
			this.providerDependency = providerDependency;
		}

		/**
		 * @return the provider
		 */
		public IFeatureValueProvider getProvider() {
			return this.provider;
		}

		/**
		 * @return the providerDependency
		 */
		public IFeatureValueProvider getProviderDependency() {
			return this.providerDependency;
		}

		public boolean isTransitive() {
			return this.providerDependency != null;
		}

		@Override
		public String toString() {
			return isTransitive() ? String.format("%s -> %s", provider, providerDependency) : provider.toString();
		}
	}

	@Override
	public <PROVIDER extends IObjectProvider & IObjectWithFeatures & IPermutable> PValueCalculationResult calculatePValues(
			final PROVIDER objectProvider, final long masterSeed)
			throws InterruptedException, PValueCalculationException {
		try {
			if (this.featureStore == null)
				throw new PValueCalculationException("featureStore hasn't been initialized.");
			this.featureValueProviders = new HashMap<>();
			for (IFitnessScore fs : this.fitnessScores) {
				for (IFeature<?> f : fs.getFeatures()) {
					if (f instanceof IFeatureWithValueProvider) {
						IFeatureValueProvider featureValueProvider = ((IFeatureWithValueProvider) f)
								.getFeatureValueProvider();
						this.featureValueProviders.putIfAbsent(featureValueProvider, new HashSet<>());
						this.featureValueProviders.get(featureValueProvider).add((IFeatureWithValueProvider<?>) f);
					}
				}
			}

			final boolean canShuffleObjectProvider = this.shuffle.supportedObjectType() == objectProvider
					.getObjectType();
			// check that we have at least one feature for which we can shuffle its feature
			// value provider
			final Set<CanShuffleFeatureValueProvider> canShuffleFeatureValueProviders = new HashSet<>();
			for (IFeatureValueProvider p : this.featureValueProviders.keySet()) {
				final Set<IFeatureWithValueProvider<?>> features = this.featureValueProviders.get(p);
				if (features.isEmpty())
					continue;
				if (this.shuffle.supportedObjectType().getBaseType().isInstance(p))
					canShuffleFeatureValueProviders.add(new CanShuffleFeatureValueProvider(p));
				else
					for (IFeatureValueProvider dep : p.dependsOn()) {
						if (this.shuffle.supportedObjectType().getBaseType().isInstance(dep)) {
							canShuffleFeatureValueProviders.add(new CanShuffleFeatureValueProvider(p, dep));
							break;
						}
					}
			}

			if (!canShuffleObjectProvider && canShuffleFeatureValueProviders.isEmpty())
				throw new PValueCalculationException(new IncompatibleShuffleException(
						"Neither the object provider nor any of the involved features can be shuffled using the specified IShuffle."));

			if (canShuffleObjectProvider) {
				this.logger.debug("shuffling the following object provider: " + objectProvider);
			} else {
				this.logger
						.debug("shuffling the following feature value providers: " + canShuffleFeatureValueProviders);
			}

			logger.debug("Preparing data structures for storing permuted feature values and fitness scores ...");
			final List<ObjectType<?>> objectTypes = this.getObjectTypes();

			final List<IObjectWithFeatures>[] objectsPerObjectType = new List[objectTypes.size()];
			final List<IFitnessScore>[] fitnessScoresPerObjectType = new List[objectTypes.size()];
			final List<Boolean>[] fitnessScoreTwoSidedPerObjectType = new List[objectTypes.size()];

			int otc = 0;
			for (ObjectType<?> c : objectTypes) {
				objectsPerObjectType[otc] = new ArrayList<>(objectProvider.getObjectsOfType(c));
				final List<IFitnessScore> fitnessScores = new ArrayList<>();
				final List<Boolean> fitnessScoresTwoSided = new ArrayList<>();

				int fsc = 0;

				for (IFitnessScore fs : this.fitnessScores) {
					if (fs.getObjectType().equals(c)) {
						fitnessScores.add(fs);
						fitnessScoresTwoSided.add(this.fitnessScoreTwoSided.get(fsc));
					}
					fsc++;
				}

				if (fitnessScores.isEmpty())
					throw new IllegalArgumentException(
							String.format("No fitness score was provided for object type %s", c));

				fitnessScoresPerObjectType[otc] = fitnessScores;
				fitnessScoreTwoSidedPerObjectType[otc] = fitnessScoresTwoSided;
				otc++;
			}

			final List<? extends IObjectWithFeatures>[] objectArrays = new List[objectsPerObjectType.length];

			final int[][][] countLarger = new int[getObjectTypes().size()][][];
			final int[][][] countEqual = new int[getObjectTypes().size()][][];
			final int[][][] countSmaller = new int[getObjectTypes().size()][][];
			final int[][][] comparisons = new int[getObjectTypes().size()][][];

			final int[] numbersTotalFeatures = new int[objectTypes.size()];
			// object type X feature X permutation X object |-> feature value
			final List<IFeatureValue<? extends Comparable<?>>>[][][] newPermutedFeatureValues;
			final List<IFeatureValue<? extends Comparable<?>>>[][][] newFeatureValuesObjectSpecific;

			// object type X fitness score X permutation |-> fitness
			final List<IFitnessValue>[][][] newPermutedFitnessValues;
			final IFitnessValue[][][][] newFitnessValuesObjectSpecific;

			if (this.storePermutedFeatureValues) {
				newPermutedFeatureValues = new List[objectTypes.size()][][];
				newFeatureValuesObjectSpecific = new List[objectTypes.size()][][];
			} else {
				newPermutedFeatureValues = null;
				newFeatureValuesObjectSpecific = null;
			}

			if (this.storePermutedFitnessValues) {
				newFitnessValuesObjectSpecific = new IFitnessValue[objectTypes.size()][][][];
				newPermutedFitnessValues = new List[objectTypes.size()][][];
			} else {
				newPermutedFitnessValues = null;
				newFitnessValuesObjectSpecific = null;
			}

			final OriginalFitnessValues originalFitnessValues = new OriginalFitnessValues(fitnessScoresPerObjectType,
					objectsPerObjectType, objectTypes);

			otc = 0;
			for (ObjectType<?> c : objectTypes) {
				objectArrays[otc] = new ArrayList<>(objectsPerObjectType[otc]);

				countLarger[otc] = new int[objectsPerObjectType[otc].size()][fitnessScoresPerObjectType[otc].size()];
				countEqual[otc] = new int[objectsPerObjectType[otc].size()][fitnessScoresPerObjectType[otc].size()];
				countSmaller[otc] = new int[objectsPerObjectType[otc].size()][fitnessScoresPerObjectType[otc].size()];
				comparisons[otc] = new int[objectsPerObjectType[otc].size()][fitnessScoresPerObjectType[otc].size()];

				for (final IFitnessScore fs : fitnessScoresPerObjectType[otc])
					numbersTotalFeatures[otc] += fs.getFeatures().length;
				numbersTotalFeatures[otc] += this.conditionalFeatures[otc].size();

				if (this.storePermutedFeatureValues) {
					if (this.calculateObjectSpecificDistributions[otc]) {
						newFeatureValuesObjectSpecific[otc] = new List[objectArrays[otc]
								.size()][numbersTotalFeatures[otc]];
						for (int f = 0; f < newFeatureValuesObjectSpecific[otc].length; f++)
							for (int p = 0; p < newFeatureValuesObjectSpecific[otc][f].length; p++)
								newFeatureValuesObjectSpecific[otc][f][p] = new ArrayList<>();
					} else {
						newPermutedFeatureValues[otc] = new List[numbersTotalFeatures[otc]][this.permutations];
						for (int f = 0; f < newPermutedFeatureValues[otc].length; f++)
							for (int p = 0; p < newPermutedFeatureValues[otc][f].length; p++)
								newPermutedFeatureValues[otc][f][p] = new ArrayList<>();
					}
				}

				if (this.storePermutedFitnessValues) {
					if (this.calculateObjectSpecificDistributions[otc])
						newFitnessValuesObjectSpecific[otc] = new IFitnessValue[objectArrays[otc]
								.size()][fitnessScoresPerObjectType[otc].size()][this.permutations];
					else {
						newPermutedFitnessValues[otc] = new List[fitnessScoresPerObjectType[otc]
								.size()][this.permutations];
						for (int fs = 0; fs < newPermutedFitnessValues[otc].length; fs++)
							for (int p = 0; p < newPermutedFitnessValues[otc][fs].length; p++)
								newPermutedFitnessValues[otc][fs][p] = new ArrayList<>();
					}
				}

				otc++;
			}
			logger.debug("done");

			if (this.conditionalFeatures != null) {
				logger.debug("Calculating original conditional feature values ...");

				otc = 0;
				for (ObjectType<?> c : objectTypes) {
					for (IArithmeticFeature<? extends Comparable<?>> f : this.conditionalFeatures[otc]) {
						for (final IObjectWithFeatures object : objectProvider.getObjectsOfType(c)) {
							if (!this.featureStore.hasFeatureFor(f, object))
								this.featureStore.setFeatureValue(object, (IArithmeticFeature) f, f.calculate(object));
						}
					}
					otc++;
				}
				logger.debug("done");
			}

			logger.debug("Calculating original fitness scores ...");
			for (otc = 0; otc < objectTypes.size(); otc++) {
				for (final IFitnessScore fs : fitnessScoresPerObjectType[otc]) {
					final Map<IObjectWithFeatures, IFitnessValue> originalFitness = this
							.calculateOriginalObjectFitness(fs, objectProvider);
					for (IObjectWithFeatures o : originalFitness.keySet())
						originalFitnessValues.set(fs, o, originalFitness.get(o));
				}
			}
			logger.debug("done");

			final int numCores = Math.max(1, MyParallel.DEFAULT_THREADS);

			this.finishedPermutations = new AtomicInteger();

			final Iterable<Iterable<Integer>> batchifiedPermutations = Iterables
					.batchify(Iterables.range(this.permutations), new BatchCount(numCores, 1));

			final long[] permutationSeeds = new Random(masterSeed).longs(permutations).toArray();

			final IntSet failedPermutations = new IntAVLTreeSet();

			try {
				final List<Future<Void>> futures = new MyParallel<Void>(numCores, "calculate-pvalues-pool") {

					@Override
					protected boolean isOnExceptionShutdown(final Exception e) {
						if (e instanceof ShuffleNotInitializedException)
							return true;
						return super.isOnExceptionShutdown(e);
					}
				}.For(batchifiedPermutations, new MyParallel.Operation<Iterable<Integer>, Void>() {
					@Override
					public Void perform(final Iterable<Integer> perms)
							throws InterruptedException, PValueCalculationException, IncorrectlyInitializedException,
							FeatureCalculationException, FitnessScoreNotInitializedException,
							FitnessScoreInitializationException, NotAnArithmeticFeatureValueException,
							ToNumberConversionException, FitnessCalculationException,
							IncompatibleObjectProviderException, IncompatibleFitnessScoreAndObjectException,
							IncompatibleFeatureAndObjectException, ShuffleException, ShuffleNotInitializedException,
							IncompatibleShuffleException, IncompatibleMappingAndObjectTypeException {
						long startTime = System.currentTimeMillis();

						final int[][][] batchSmaller = new int[objectTypes.size()][][];
						final int[][][] batchLarger = new int[objectTypes.size()][][];
						final int[][][] batchEqual = new int[objectTypes.size()][][];
						final int[][][] batchComps = new int[objectTypes.size()][][];

						int otc = 0;
						for (ObjectType<?> c : objectTypes) {
							objectsPerObjectType[otc] = new ArrayList<>(objectProvider.getObjectsOfType(c));
							objectArrays[otc] = new ArrayList<>(objectsPerObjectType[otc]);

							batchLarger[otc] = new int[objectsPerObjectType[otc].size()][fitnessScoresPerObjectType[otc]
									.size()];
							batchEqual[otc] = new int[objectsPerObjectType[otc].size()][fitnessScoresPerObjectType[otc]
									.size()];
							batchSmaller[otc] = new int[objectsPerObjectType[otc]
									.size()][fitnessScoresPerObjectType[otc].size()];
							batchComps[otc] = new int[objectsPerObjectType[otc].size()][fitnessScoresPerObjectType[otc]
									.size()];
							otc++;
						}

						for (final int p : perms) {
							try {
								final long permutationSeed = permutationSeeds[p];

								if (!Utility.getProgress().getStatus())
									throw new InterruptedException();
								if (BasicCalculatePValues.this.isCancelled())
									throw new InterruptedException();

								final IShuffle shuffleCopy = BasicCalculatePValues.this.getShuffleForPermutation(p);

								final IShuffleMapping shuffleMapping;
								final IObjectProvider shuffledProvider;

								// if appropriate, we shuffle the IFeatureValueProviders here, and not for each
								// object type individually. This is because one provider may
								// provide for multiple object types and hence would be shuffled multiple times.
								final Map<CanShuffleFeatureValueProvider, IFeatureValueProvider> shuffledFeatureValueProviders = new HashMap<>();

								IShuffleResult shuffleResult = null;
								if (canShuffleObjectProvider) {
									shuffleResult = shuffleCopy.shuffle(objectProvider, permutationSeed);
									if (shuffleResult instanceof IShuffleResultWithMapping
											&& ((IShuffleResultWithMapping) shuffleResult).hasShuffleMapping())
										shuffleMapping = ((IShuffleResultWithMapping) shuffleResult)
												.getShuffleMapping();
									else
										shuffleMapping = null;
									shuffledProvider = (IObjectProvider) shuffleResult.getShuffled();
								} else {
									if (shuffleCopy instanceof IShuffleNetwork
											&& objectProvider.isProvidingObjectsOfType(ObjectType.CLUSTER))
										shuffledProvider = objectProvider.copy();
									else
										shuffledProvider = objectProvider;
									// TODO: shuffling only first for now
									for (CanShuffleFeatureValueProvider canShuffle : canShuffleFeatureValueProviders) {
										IFeatureValueProvider fvP = canShuffle.isTransitive()
												? canShuffle.providerDependency
												: canShuffle.provider;
										shuffleResult = shuffleCopy.shuffle((IObjectWithFeatures) fvP, permutationSeed);
										final IFeatureValueProvider shuffled = (IFeatureValueProvider) shuffleResult
												.getShuffled();
										shuffledFeatureValueProviders.put(canShuffle, shuffled);
										break;
									}
									shuffleMapping = null;
								}

								for (otc = 0; otc < objectTypes.size(); otc++) {
									final List<IObjectWithFeatures> shuffledObjects = new ArrayList<>(
											shuffledProvider.getObjectsOfType(objectTypes.get(otc)));

									// for each fitness score
									int fs = 0;
									int featureCounterOffset = 0;
									for (IFitnessScore fitnessScore : fitnessScoresPerObjectType[otc]) {
										// this also copies all features including their caches, avoiding any potential
										// caching issues.
										fitnessScore = fitnessScore.copy();
										final IArithmeticFeature<? extends Comparable<?>>[] features = BasicCalculatePValues.this
												.initializeFeaturesForPermutation(p, fitnessScore.getFeatures(),
														shuffleResult, shuffledProvider, shuffledFeatureValueProviders,
														permutationSeed);

										final IArithmeticFeature<? extends Comparable<?>>[] conditionalFeatures = BasicCalculatePValues.this
												.initializeFeaturesForPermutation(p,
														BasicCalculatePValues.this.conditionalFeatures[otc].stream()
																.map(f -> (IArithmeticFeature<? extends Comparable<?>>) f
																		.copy())
																.toArray(IArithmeticFeature[]::new),
														shuffleResult, shuffledProvider, shuffledFeatureValueProviders,
														permutationSeed);

										final IFeatureStore featureValuesShuffled = new BasicFeatureStore();

										// calculate features of shuffled objects
										int co = 0;
										for (final IObjectWithFeatures shuffledObject : shuffledObjects) {
											int fCounter = 0;
											for (final IArithmeticFeature<? extends Comparable<?>> f : features) {
												IFeatureValue<? extends Comparable<?>> value = f
														.calculate(shuffledObject);
												featureValuesShuffled.setFeatureValue(shuffledObject,
														(IArithmeticFeature) f, (IFeatureValue) value);

												if (BasicCalculatePValues.this.storePermutedFeatureValues) {
													if (BasicCalculatePValues.this.calculateObjectSpecificDistributions[otc])
														synchronized (newFeatureValuesObjectSpecific[otc][co][featureCounterOffset
																+ fCounter]) {
															newFeatureValuesObjectSpecific[otc][co][featureCounterOffset
																	+ fCounter].add(value);
														}
													else {
														synchronized (newPermutedFeatureValues[otc][featureCounterOffset
																+ fCounter][p]) {
															newPermutedFeatureValues[otc][featureCounterOffset
																	+ fCounter][p].add(co, value);
														}
													}
												}

												fCounter++;
											}

											if (conditionalFeatures != null) {
												for (IArithmeticFeature<? extends Comparable<?>> f : conditionalFeatures) {
													try {
														IFeatureValue<? extends Comparable<?>> value = f
																.calculate(shuffledObject);
														featureValuesShuffled.setFeatureValue(shuffledObject,
																(IArithmeticFeature) f, (IFeatureValue) value);

														if (BasicCalculatePValues.this.storePermutedFeatureValues) {
															if (BasicCalculatePValues.this.calculateObjectSpecificDistributions[otc])
																synchronized (newFeatureValuesObjectSpecific[otc][co][featureCounterOffset
																		+ fCounter]) {
																	newFeatureValuesObjectSpecific[otc][co][featureCounterOffset
																			+ fCounter].add(value);
																}
															else {
																synchronized (newPermutedFeatureValues[otc][featureCounterOffset
																		+ fCounter][p]) {
																	newPermutedFeatureValues[otc][featureCounterOffset
																			+ fCounter][p].add(value);
																}
															}
														}
													} catch (FeatureCalculationException
															| IncorrectlyInitializedException e) {
														throw new PValueCalculationException(e);
													}
													fCounter++;
												}
											}

											co++;
										}

										fitnessScore.setObjectsAndFeatureStore(shuffledProvider, featureValuesShuffled);

										co = 0;
										for (final IObjectWithFeatures shuffledObject : shuffledObjects) {
											// calculate fitness of shuffled object
											final IFitnessValue shuffledFitness = fitnessScore
													.calculateFitness(shuffledObject);

											if (BasicCalculatePValues.this.storePermutedFitnessValues) {
												if (BasicCalculatePValues.this.calculateObjectSpecificDistributions[otc])
													try {
														newFitnessValuesObjectSpecific[otc][co][fs][p] = shuffledFitness;
													} catch (final ArrayIndexOutOfBoundsException e) {
														e.printStackTrace();
														throw e;
													}
												else {
													newPermutedFitnessValues[otc][fs][p].add(shuffledFitness);
												}
											}

											int o = 0;
											for (final IObjectWithFeatures originalObject : objectArrays[otc]) {
												// if we calculate object specific
												// distributions we only compare it against
												// its shuffled counterpart
												if (!BasicCalculatePValues.this.calculateObjectSpecificDistributions[otc]
														|| shuffleMapping == null || (shuffleMapping.get(originalObject)
																.equals(shuffledObject))) {
													if (pValueConditional(originalObject, shuffledObject, featureStore,
															featureValuesShuffled, conditionalFeatures)) {
														try {
															final IFitnessValue originalFitness = originalFitnessValues
																	.get(otc, fs, o);
															final int compareResult = fitnessScore
																	.compare(shuffledFitness, originalFitness);
															if (compareResult < 0)
																batchSmaller[otc][o][fs]++;
															else if (compareResult > 0)
																batchLarger[otc][o][fs]++;
															else
																batchEqual[otc][o][fs]++;
															batchComps[otc][o][fs]++;
														} catch (final Exception e) {
															e.printStackTrace();
														}
													}
												}
												o++;
											}
											co++;

										}
										fs++;
										featureCounterOffset += fitnessScore.getFeatures().length;
									}
								}
								if (shuffleResult != null)
									shuffleCopy.cleanUpShuffle(shuffleResult);
								BasicCalculatePValues.this.fireStateChanged(BasicCalculatePValues.this.permutations,
										BasicCalculatePValues.this.finishedPermutations.incrementAndGet());
							} catch (ShuffleException e) {
								synchronized (failedPermutations) {
									failedPermutations.add(p);
								}
							}
						} // end p

						synchronized (countSmaller) {
							for (otc = 0; otc < objectTypes.size(); otc++)
								for (int o = 0; o < batchSmaller[otc].length; o++)
									for (int fs = 0; fs < batchSmaller[otc][o].length; fs++)
										countSmaller[otc][o][fs] += batchSmaller[otc][o][fs];
						}

						synchronized (countLarger) {
							for (otc = 0; otc < objectTypes.size(); otc++)
								for (int o = 0; o < batchLarger[otc].length; o++)
									for (int fs = 0; fs < batchLarger[otc][o].length; fs++)
										countLarger[otc][o][fs] += batchLarger[otc][o][fs];
						}

						synchronized (countEqual) {
							for (otc = 0; otc < objectTypes.size(); otc++)
								for (int o = 0; o < batchEqual[otc].length; o++)
									for (int fs = 0; fs < batchEqual[otc][o].length; fs++)
										countEqual[otc][o][fs] += batchEqual[otc][o][fs];
						}

						synchronized (comparisons) {
							for (otc = 0; otc < objectTypes.size(); otc++)
								for (int o = 0; o < batchComps[otc].length; o++)
									for (int fs = 0; fs < batchComps[otc][o].length; fs++)
										comparisons[otc][o][fs] += batchComps[otc][o][fs];
						}

						logger.debug(
								"batch time: " + Formatter.formatMsToDuration(System.currentTimeMillis() - startTime));
						return null;
					}
				});
				for (Future<Void> f : futures) {
					f.get();
				}
			} catch (final InterruptedException e) {
				throw e;
			} catch (final Throwable e) {
				if (e.getCause() instanceof InterruptedException)
					throw (InterruptedException) e.getCause();
				throw new PValueCalculationException(e);
			}

			final FitnessScorePValues pvals = new FitnessScorePValues(fitnessScoresPerObjectType, objectsPerObjectType,
					objectTypes);

			for (otc = 0; otc < objectTypes.size(); otc++) {
				int o = 0;
				for (final IObjectWithFeatures c : objectArrays[otc]) {

					int j = 0;
					for (final IFitnessScore fs : fitnessScoresPerObjectType[otc]) {
						int totalObservations = comparisons[otc][o][j];
						int betterObservations;

						if (this.fitnessScoreTwoSided.get(j))
							betterObservations = Math.min(countLarger[otc][o][j], countSmaller[otc][o][j]) * 2
									+ countEqual[otc][o][j];
						else
							betterObservations = countEqual[otc][o][j] + countLarger[otc][o][j];
						pvals.set(fs, c, new EmpiricalPvalue(totalObservations, betterObservations));
						j++;
					}
					o++;
				}
			}

			// combine p-values
			final PValues result = new PValues(objectsPerObjectType, objectTypes);

			for (otc = 0; otc < objectTypes.size(); otc++) {
				for (final IObjectWithFeatures c : objectsPerObjectType[otc]) {
					final int noPvalues = fitnessScoresPerObjectType[otc].size();
					if (noPvalues > 1)
						result.set(c, this.combinePValues.combine(c, fitnessScoresPerObjectType[otc].stream()
								.map(fs -> pvals.get(fs, c)).collect(Collectors.toList())));
					else if (noPvalues == 1)
						// reduce to the single entry
						result.set(c, pvals.get(fitnessScoresPerObjectType[otc].iterator().next(), c));
				}
			}

			PermutedFeatureValues permutedFeatureValues = null;
			PermutedFeatureValuesObjectSpecific permutedFeatureValuesObjectSpecific = null;
			PermutedFitnessValues permutedFitnessValues = null;
			PermutedFitnessValuesObjectSpecific permutedFitnessValuesObjectSpecific = null;

			if (this.storePermutedFeatureValues) {
				permutedFeatureValuesObjectSpecific = new PermutedFeatureValuesObjectSpecific(objectsPerObjectType,
						objectTypes);
				permutedFeatureValues = new PermutedFeatureValues(objectTypes);

				for (otc = 0; otc < objectTypes.size(); otc++) {
					if (this.calculateObjectSpecificDistributions[otc]) {
						// store feature values of permuted objects
						for (int oc = 0; oc < newFeatureValuesObjectSpecific[otc].length; oc++) {
							int f = 0;
							for (int fs = 0; fs < fitnessScoresPerObjectType[otc].size(); fs++)
								for (final IArithmeticFeature<? extends Comparable<?>> feature : fitnessScoresPerObjectType[otc]
										.get(fs).getFeatures()) {
									permutedFeatureValuesObjectSpecific.set(otc, oc, feature,
											newFeatureValuesObjectSpecific[otc][oc][f]);

									f++;
								}
							if (this.conditionalFeatures != null) {
								for (final IArithmeticFeature<? extends Comparable<?>> feature : this.conditionalFeatures[otc]) {
									final List<IFeatureValue<? extends Comparable<?>>> l = new ArrayList<>();
									l.addAll(newFeatureValuesObjectSpecific[otc][oc][f]);
									permutedFeatureValuesObjectSpecific.set(otc, oc, feature, l);
									f++;
								}
							}
						}
					} else {
						// store feature values of permuted objects
						int f = 0;
						for (int fs = 0; fs < fitnessScoresPerObjectType[otc].size(); fs++) {
							for (final IArithmeticFeature<? extends Comparable<?>> feature : fitnessScoresPerObjectType[otc]
									.get(fs).getFeatures()) {
								final List<IFeatureValue<? extends Comparable<?>>> l = new ArrayList<>();
								for (int p = 0; p < this.permutations; p++)
									l.addAll(newPermutedFeatureValues[otc][f][p]);
								permutedFeatureValues.set(otc, feature, l);
								f++;
							}
						}
						if (this.conditionalFeatures != null) {
							for (final IArithmeticFeature<? extends Comparable<?>> feature : this.conditionalFeatures[otc]) {
								final List<IFeatureValue<? extends Comparable<?>>> l = new ArrayList<>();
								for (int p = 0; p < this.permutations; p++)
									l.addAll(newPermutedFeatureValues[otc][f][p]);
								permutedFeatureValues.set(otc, feature, l);
								f++;
							}
						}
					}
				}
			}

			if (this.storePermutedFitnessValues) {
				permutedFitnessValuesObjectSpecific = new PermutedFitnessValuesObjectSpecific(permutations,
						fitnessScoresPerObjectType, objectsPerObjectType, objectTypes);
				permutedFitnessValues = new PermutedFitnessValues(fitnessScoresPerObjectType, objectsPerObjectType,
						objectTypes);

				for (otc = 0; otc < objectTypes.size(); otc++) {
					if (this.calculateObjectSpecificDistributions[otc]) {
						// store fitness values of permuted objects
						for (int oc = 0; oc < newFeatureValuesObjectSpecific.length; oc++) {
							for (int fs = 0; fs < fitnessScoresPerObjectType[otc].size(); fs++)
								permutedFitnessValuesObjectSpecific.set(otc, oc, fs,
										newFitnessValuesObjectSpecific[otc][oc][fs]);
						}
					} else {
						// store fitness values of permuted objects
						for (int fs = 0; fs < fitnessScoresPerObjectType[otc].size(); fs++) {

							final List<IFitnessValue> l = new ArrayList<>();
							for (int p = 0; p < this.permutations; p++)
								l.addAll(newPermutedFitnessValues[otc][fs][p]);

							permutedFitnessValues.set(otc, fs, l);
						}
					}
				}
			}

			return new PValueCalculationResult(objectTypes, this, masterSeed, this.featureStore, objectsPerObjectType,
					fitnessScoresPerObjectType, calculateObjectSpecificDistributions, conditionalFeatures,
					originalFitnessValues, pvals, result, permutedFeatureValues, permutedFeatureValuesObjectSpecific,
					permutedFitnessValues, permutedFitnessValuesObjectSpecific, failedPermutations);

		} catch (FeatureCalculationException | IncorrectlyInitializedException | IncompatibleObjectProviderException
				| IncompatibleFeatureAndObjectException | IncompatibleFitnessScoreAndObjectException |

				FitnessScoreInitializationException e) {
			throw new PValueCalculationException(e);
		}
	}

	/**
	 * @return the conditionalFeatures
	 */
	@Override
	public List<IArithmeticFeature<? extends Comparable<?>>>[] getConditionalFeatures() {
		return this.conditionalFeatures;
	}

	@Override
	public Set<IArithmeticFeature<? extends Comparable<?>>> getFeatures() {
		final Set<IArithmeticFeature<? extends Comparable<?>>> features = new HashSet<>();
		for (final IFitnessScore fs : this.fitnessScores)
			for (final IArithmeticFeature<? extends Comparable<?>> f : fs.getFeatures())
				features.add(f);
		return features;
	}

	/**
	 * @return the featureStore
	 */
	@Override
	public IFeatureStore getFeatureStore() {
		return this.featureStore;
	}

	/**
	 * @return the combineClusterPValues
	 */
	@Override
	public final ICombinePValues getCombinePValues() {
		return this.combinePValues;
	}

	@Override
	public final List<IFitnessScore> getFitnessScores() {
		return this.fitnessScores;
	}

	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.listener = new ArrayList<>();
	}
}