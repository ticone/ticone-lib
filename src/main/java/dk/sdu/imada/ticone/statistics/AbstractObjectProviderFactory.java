/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IObjectProviderFactory;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 19, 2018
 *
 */
public abstract class AbstractObjectProviderFactory<S extends Object, P extends IObjectProvider>
		implements IObjectProviderFactory<S, P> {

	protected long seed;

	/**
	 * 
	 */
	public AbstractObjectProviderFactory(final long seed) {
		super();
		this.seed = seed;
	}

}