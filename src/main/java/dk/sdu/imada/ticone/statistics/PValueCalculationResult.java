/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dk.sdu.imada.ticone.feature.FeaturePvalue;
import dk.sdu.imada.ticone.feature.FeaturePvalueValue;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessValue;
import dk.sdu.imada.ticone.util.AbstractTiconeResult;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.Iterables;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterable;
import it.unimi.dsi.fastutil.objects.ObjectList;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 20, 2017
 *
 */
public class PValueCalculationResult extends AbstractTiconeResult implements IPValueCalculationResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6823187171107409262L;

	protected ICalculatePValues calculatePValues;
	protected IFeatureStore featureStore;

	private final Map<ObjectType<?>, Integer> objectTypeToIndex;
	private final Map<IFitnessScore, Integer>[] fitnessScoreToIndexPerObjectType;
	private final Map<IArithmeticFeature<? extends Comparable<?>>, Integer>[] conditionalFeaturesPerObjectType;
	private final Map<IObjectWithFeatures, Integer>[] objectToIndexPerObjectType;
	private final boolean[] hasObjectSpecificDistributions;
	private final long seed;
	private final IntSet failedPermutations;

	FitnessScorePValues fitnessScorePValues;
	PValues pValues;
	PermutedFeatureValues permutedFeatureValues;
	PermutedFeatureValuesObjectSpecific permutedFeatureValuesObjectSpecific;
	PermutedFitnessValues permutedFitnessValues;
	PermutedFitnessValuesObjectSpecific permutedFitnessValuesObjectSpecific;
	OriginalFitnessValues originalFitnessValues;

	private static <O> Map<O, Integer>[] arrayOfListsToArrayOfIndexMaps(final List<O>[] arrayOfLists) {
		final Map<O, Integer>[] result = new Map[arrayOfLists.length];
		for (int arrIdx = 0; arrIdx < arrayOfLists.length; arrIdx++) {
			result[arrIdx] = new HashMap<>();
			for (int i = 0; i < arrayOfLists[arrIdx].size(); i++)
				result[arrIdx].put(arrayOfLists[arrIdx].get(i), i);
		}
		return result;
	}

	private static <O> Map<O, Integer> listToIndexMap(final List<O> list) {
		final Map<O, Integer> result = new HashMap<>();
		for (int i = 0; i < list.size(); i++)
			result.put(list.get(i), i);
		return result;
	}

	static class FitnessScorePValues implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1665447533808486668L;
		private final IPvalue[][][] fitnessScorePValues;
		private final Map<IFitnessScore, Integer>[] fitnessScoresPerObjectType;
		private final Map<IObjectWithFeatures, Integer>[] objectToIndexPerObjectType;
		private final Map<ObjectType<?>, Integer> objectTypeToIndex;

		FitnessScorePValues(final List<IFitnessScore>[] fitnessScoresPerObjectType,
				final List<IObjectWithFeatures>[] objectToIndexPerObjectType,
				final List<ObjectType<?>> objectTypeToIndex) {
			super();

			this.objectTypeToIndex = listToIndexMap(objectTypeToIndex);
			this.objectToIndexPerObjectType = arrayOfListsToArrayOfIndexMaps(objectToIndexPerObjectType);
			this.fitnessScoresPerObjectType = arrayOfListsToArrayOfIndexMaps(fitnessScoresPerObjectType);

			this.fitnessScorePValues = new IPvalue[objectTypeToIndex.size()][][];
			for (int otIdx = 0; otIdx < this.fitnessScorePValues.length; otIdx++) {
				this.fitnessScorePValues[otIdx] = new IPvalue[objectToIndexPerObjectType[otIdx]
						.size()][fitnessScoresPerObjectType[otIdx].size()];
			}
		}

		void set(final IFitnessScore fitnessScore, final IObjectWithFeatures object, IPvalue val) {
			final int otIdx = objectTypeToIndex.get(object.getObjectType());
			this.fitnessScorePValues[otIdx][objectToIndexPerObjectType[otIdx]
					.get(object)][fitnessScoresPerObjectType[otIdx].get(fitnessScore)] = val;
		}

		IPvalue get(final IFitnessScore fitnessScore, final IObjectWithFeatures object) {
			final int otIdx = objectTypeToIndex.get(object.getObjectType());
			return this.fitnessScorePValues[otIdx][objectToIndexPerObjectType[otIdx]
					.get(object)][fitnessScoresPerObjectType[otIdx].get(fitnessScore)];
		}
	}

	static class PValues implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5693040124632449993L;
		private final IPvalue[][] pValues;
		private final Map<IObjectWithFeatures, Integer>[] objectToIndexPerObjectType;
		private final Map<ObjectType<?>, Integer> objectTypeToIndex;

		public PValues(final List<IObjectWithFeatures>[] objectToIndexPerObjectType,
				final List<ObjectType<?>> objectTypeToIndex) {
			super();

			this.objectTypeToIndex = listToIndexMap(objectTypeToIndex);
			this.objectToIndexPerObjectType = arrayOfListsToArrayOfIndexMaps(objectToIndexPerObjectType);

			this.pValues = new IPvalue[this.objectTypeToIndex.size()][];
			for (int otIdx = 0; otIdx < this.pValues.length; otIdx++)
				this.pValues[otIdx] = new IPvalue[objectToIndexPerObjectType[otIdx].size()];
		}

		void set(final IObjectWithFeatures object, IPvalue val) {
			final int otIdx = objectTypeToIndex.get(object.getObjectType());
			this.pValues[otIdx][objectToIndexPerObjectType[otIdx].get(object)] = val;
		}

		IPvalue get(final IObjectWithFeatures object) {
			final int otIdx = objectTypeToIndex.get(object.getObjectType());
			return this.pValues[otIdx][objectToIndexPerObjectType[otIdx].get(object)];
		}

		ObjectList<IPvalue> get(final ObjectType<?> type) {
			final int otIdx = objectTypeToIndex.get(type);
			return ObjectArrayList.wrap(this.pValues[otIdx]);
		}

		public <O extends IObjectWithFeatures> Set<O> keySet(final ObjectType<O> type) {
			final int otIdx = objectTypeToIndex.get(type);
			return (Set<O>) this.objectToIndexPerObjectType[otIdx].keySet();
		}

		public Collection<IPvalue> values(final ObjectType<?> type) {
			final int otIdx = objectTypeToIndex.get(type);
			return ObjectArrayList.wrap(this.pValues[otIdx]);
		}
	}

	static class PermutedFeatureValues implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3029615773147294792L;
		private final Map<IArithmeticFeature<? extends Comparable<?>>, List<IFeatureValue<? extends Comparable<?>>>>[] permutedFeatureValues;
		private final Map<ObjectType<?>, Integer> objectTypeToIndex;

		public PermutedFeatureValues(final List<ObjectType<?>> objectTypeToIndex) {
			super();

			this.objectTypeToIndex = listToIndexMap(objectTypeToIndex);

			this.permutedFeatureValues = new Map[objectTypeToIndex.size()];
			for (int otIdx = 0; otIdx < this.permutedFeatureValues.length; otIdx++)
				this.permutedFeatureValues[otIdx] = new HashMap<>();
		}

		void set(final ObjectType<?> objectType, final IArithmeticFeature<? extends Comparable<?>> feature,
				List<IFeatureValue<? extends Comparable<?>>> vals) {
			final int otIdx = objectTypeToIndex.get(objectType);
			permutedFeatureValues[otIdx].put(feature, vals);
		}

		<V> List<IFeatureValue<V>> get(final IArithmeticFeature<V> feature) {
			final int otIdx = objectTypeToIndex.get(feature.supportedObjectType());
			return (List) permutedFeatureValues[otIdx].get(feature);
		}

		Map<IArithmeticFeature<? extends Comparable<?>>, List<IFeatureValue<? extends Comparable<?>>>> get(
				final ObjectType<?> objectType) {
			final int otIdx = objectTypeToIndex.get(objectType);
			return permutedFeatureValues[otIdx];
		}

		void set(final int otIdx, final IArithmeticFeature<? extends Comparable<?>> feature,
				List<IFeatureValue<? extends Comparable<?>>> vals) {
			permutedFeatureValues[otIdx].put(feature, vals);
		}

		Map<IArithmeticFeature<? extends Comparable<?>>, List<IFeatureValue<? extends Comparable<?>>>> get(
				final int otIdx) {
			return permutedFeatureValues[otIdx];
		}
	}

	static class PermutedFeatureValuesObjectSpecific implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6194222565161725918L;
		private final Map<IArithmeticFeature<? extends Comparable<?>>, List<IFeatureValue<? extends Comparable<?>>>>[][] permutedFeatureValuesObjectSpecific;
		private final Map<IObjectWithFeatures, Integer>[] objectToIndexPerObjectType;
		private final Map<ObjectType<?>, Integer> objectTypeToIndex;

		public PermutedFeatureValuesObjectSpecific(final List<IObjectWithFeatures>[] objectToIndexPerObjectType,
				final List<ObjectType<?>> objectTypeToIndex) {
			super();

			this.objectTypeToIndex = listToIndexMap(objectTypeToIndex);
			this.objectToIndexPerObjectType = arrayOfListsToArrayOfIndexMaps(objectToIndexPerObjectType);

			this.permutedFeatureValuesObjectSpecific = new Map[this.objectTypeToIndex.size()][];
			for (int otIdx = 0; otIdx < this.permutedFeatureValuesObjectSpecific.length; otIdx++) {
				this.permutedFeatureValuesObjectSpecific[otIdx] = new Map[this.objectToIndexPerObjectType[otIdx]
						.size()];
				for (int oIdx = 0; oIdx < this.permutedFeatureValuesObjectSpecific[otIdx].length; oIdx++)
					this.permutedFeatureValuesObjectSpecific[otIdx][oIdx] = new HashMap<>();
			}
		}

		void set(final IObjectWithFeatures object, IArithmeticFeature<? extends Comparable<?>> feature,
				final List<IFeatureValue<? extends Comparable<?>>> vals) {
			final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
			final int otIdx = objectTypeToIndex.get(objectType);
			permutedFeatureValuesObjectSpecific[otIdx][objectToIndexPerObjectType[otIdx].get(object)].put(feature,
					vals);
		}

		List<IFeatureValue<? extends Comparable<?>>> get(final ObjectType<?> objectType, final IFeature<?> feature) {
			final int otIdx = objectTypeToIndex.get(objectType);
			final List<IFeatureValue<? extends Comparable<?>>> result = new ObjectArrayList<>();
			for (int oIdx = 0; oIdx < permutedFeatureValuesObjectSpecific[otIdx].length; oIdx++)
				if (permutedFeatureValuesObjectSpecific[otIdx][oIdx].containsKey(feature))
					result.addAll(permutedFeatureValuesObjectSpecific[otIdx][oIdx].get(feature));
			return result;
		}

		Map<IArithmeticFeature<? extends Comparable<?>>, List<IFeatureValue<? extends Comparable<?>>>> get(
				final IObjectWithFeatures object) {
			final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
			final int otIdx = objectTypeToIndex.get(objectType);
			return permutedFeatureValuesObjectSpecific[otIdx][objectToIndexPerObjectType[otIdx].get(object)];
		}

		<V> List<IFeatureValue<V>> get(final IArithmeticFeature<V> feature) {
			final ObjectType<? extends IObjectWithFeatures> objectType = feature.supportedObjectType();
			final int otIdx = objectTypeToIndex.get(objectType);
			final List<IFeatureValue<V>> result = new ObjectArrayList<>();
			for (int oIdx = 0; oIdx < permutedFeatureValuesObjectSpecific[otIdx].length; oIdx++)
				if (permutedFeatureValuesObjectSpecific[otIdx][oIdx].containsKey(feature))
					result.addAll((List) permutedFeatureValuesObjectSpecific[otIdx][oIdx].get(feature));
			return result;
		}

		void set(final int otIdx, final int oIdx, IArithmeticFeature<? extends Comparable<?>> feature,
				final List<IFeatureValue<? extends Comparable<?>>> vals) {
			permutedFeatureValuesObjectSpecific[otIdx][oIdx].put(feature, vals);
		}

		Map<IArithmeticFeature<? extends Comparable<?>>, List<IFeatureValue<? extends Comparable<?>>>> get(
				final int otIdx, final int oIdx) {
			return permutedFeatureValuesObjectSpecific[otIdx][oIdx];
		}

	}

	static class PermutedFitnessValues implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -5548004199015470593L;
		private final List<IFitnessValue>[][] permutedFitnessValues;
		private final Map<IFitnessScore, Integer>[] fitnessScoresPerObjectType;
		private final Map<ObjectType<?>, Integer> objectTypeToIndex;

		PermutedFitnessValues(final List<IFitnessScore>[] fitnessScoresPerObjectType,
				final List<IObjectWithFeatures>[] objectToIndexPerObjectType,
				final List<ObjectType<?>> objectTypeToIndex) {
			super();

			this.objectTypeToIndex = listToIndexMap(objectTypeToIndex);
			this.fitnessScoresPerObjectType = arrayOfListsToArrayOfIndexMaps(fitnessScoresPerObjectType);

			this.permutedFitnessValues = new List[this.objectTypeToIndex.size()][];
			for (int otIdx = 0; otIdx < this.permutedFitnessValues.length; otIdx++) {
				this.permutedFitnessValues[otIdx] = new List[fitnessScoresPerObjectType[otIdx].size()];
				for (int oIdx = 0; oIdx < this.permutedFitnessValues[otIdx].length; oIdx++)
					this.permutedFitnessValues[otIdx][oIdx] = new ArrayList<>();
			}
		}

		void set(final ObjectType<?> objectType, final IFitnessScore fitnessScore, final List<IFitnessValue> vals) {
			final int otIdx = objectTypeToIndex.get(objectType);
			permutedFitnessValues[otIdx][fitnessScoresPerObjectType[otIdx].get(fitnessScore)] = vals;
		}

		List<IFitnessValue> get(final ObjectType<?> objectType, final IFitnessScore fitnessScore) {
			final int otIdx = objectTypeToIndex.get(objectType);
			return permutedFitnessValues[otIdx][fitnessScoresPerObjectType[otIdx].get(fitnessScore)];
		}

		void set(final int otIdx, final int fsIdx, final List<IFitnessValue> vals) {
			permutedFitnessValues[otIdx][fsIdx] = vals;
		}

		List<IFitnessValue> get(final int otIdx, final int fsIdx) {
			return permutedFitnessValues[otIdx][fsIdx];
		}

		public Iterable<List<IFitnessValue>> values(final ObjectType<?> type) {
			final int otIdx = objectTypeToIndex.get(type);
			return new Iterable<List<IFitnessValue>>() {

				@Override
				public Iterator<List<IFitnessValue>> iterator() {
					return new Iterator<List<IFitnessValue>>() {
						int i = -1;

						@Override
						public boolean hasNext() {
							return i + 1 < permutedFitnessValues[otIdx].length;
						}

						@Override
						public List<IFitnessValue> next() {
							return permutedFitnessValues[otIdx][++i];
						}
					};
				}
			};
		}
	}

	static class PermutedFitnessValuesObjectSpecific implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 783273069756313659L;
		private final IFitnessValue[][][][] permutedFitnessValuesObjectSpecific;
		private final Map<IFitnessScore, Integer>[] fitnessScoresPerObjectType;
		private final Map<IObjectWithFeatures, Integer>[] objectToIndexPerObjectType;
		private final Map<ObjectType<?>, Integer> objectTypeToIndex;

		PermutedFitnessValuesObjectSpecific(final int permutations,
				final List<IFitnessScore>[] fitnessScoresPerObjectType,
				final List<IObjectWithFeatures>[] objectToIndexPerObjectType,
				final List<ObjectType<?>> objectTypeToIndex) {
			super();

			this.objectTypeToIndex = listToIndexMap(objectTypeToIndex);
			this.objectToIndexPerObjectType = arrayOfListsToArrayOfIndexMaps(objectToIndexPerObjectType);
			this.fitnessScoresPerObjectType = arrayOfListsToArrayOfIndexMaps(fitnessScoresPerObjectType);

			this.permutedFitnessValuesObjectSpecific = new IFitnessValue[this.objectTypeToIndex.size()][][][];
			for (int otIdx = 0; otIdx < this.permutedFitnessValuesObjectSpecific.length; otIdx++) {
				this.permutedFitnessValuesObjectSpecific[otIdx] = new IFitnessValue[this.objectToIndexPerObjectType[otIdx]
						.size()][][];
				for (int oIdx = 0; oIdx < this.permutedFitnessValuesObjectSpecific[otIdx].length; oIdx++)
					this.permutedFitnessValuesObjectSpecific[otIdx][oIdx] = new IFitnessValue[this.fitnessScoresPerObjectType[otIdx]
							.size()][];
			}
		}

		void set(final IFitnessScore fitnessScore, final IObjectWithFeatures object, final IFitnessValue[] val) {
			final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
			final int otIdx = objectTypeToIndex.get(objectType);
			permutedFitnessValuesObjectSpecific[otIdx][objectToIndexPerObjectType[otIdx]
					.get(object)][fitnessScoresPerObjectType[otIdx].get(fitnessScore)] = val;
		}

		IFitnessValue[] get(final IFitnessScore fitnessScore, final IObjectWithFeatures object) {
			final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
			final int otIdx = objectTypeToIndex.get(objectType);
			return permutedFitnessValuesObjectSpecific[otIdx][objectToIndexPerObjectType[otIdx]
					.get(object)][fitnessScoresPerObjectType[otIdx].get(fitnessScore)];
		}

		void set(final int otIdx, final int oIdx, final int fsIdx, final IFitnessValue[] val) {
			permutedFitnessValuesObjectSpecific[otIdx][oIdx][fsIdx] = val;
		}

		IFitnessValue[] get(final int otIdx, final int oIdx, final int fsIdx) {
			return permutedFitnessValuesObjectSpecific[otIdx][oIdx][fsIdx];
		}
	}

	static class OriginalFitnessValues implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8606607672897173911L;
		private final IFitnessValue[][][] originalFitnessValues;
		private final Map<IFitnessScore, Integer>[] fitnessScoresPerObjectType;
		private final Map<IObjectWithFeatures, Integer>[] objectToIndexPerObjectType;
		private final Map<ObjectType<?>, Integer> objectTypeToIndex;

		OriginalFitnessValues(final List<IFitnessScore>[] fitnessScoresPerObjectType,
				final List<IObjectWithFeatures>[] objectToIndexPerObjectType,
				final List<ObjectType<?>> objectTypeToIndex) {
			super();

			this.objectTypeToIndex = listToIndexMap(objectTypeToIndex);
			this.objectToIndexPerObjectType = arrayOfListsToArrayOfIndexMaps(objectToIndexPerObjectType);
			this.fitnessScoresPerObjectType = arrayOfListsToArrayOfIndexMaps(fitnessScoresPerObjectType);

			this.originalFitnessValues = new IFitnessValue[objectTypeToIndex.size()][][];
			for (int otIdx = 0; otIdx < this.originalFitnessValues.length; otIdx++) {
				this.originalFitnessValues[otIdx] = new IFitnessValue[objectToIndexPerObjectType[otIdx]
						.size()][fitnessScoresPerObjectType[otIdx].size()];
			}
		}

		void set(final IFitnessScore fitnessScore, final IObjectWithFeatures object, IFitnessValue val) {
			final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
			final int otIdx = objectTypeToIndex.get(objectType);
			set(otIdx, fitnessScoresPerObjectType[otIdx].get(fitnessScore),
					objectToIndexPerObjectType[otIdx].get(object), val);
		}

		IFitnessValue get(final IFitnessScore fitnessScore, final IObjectWithFeatures object) {
			final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
			final int otIdx = objectTypeToIndex.get(objectType);
			return get(otIdx, fitnessScoresPerObjectType[otIdx].get(fitnessScore),
					objectToIndexPerObjectType[otIdx].get(object));
		}

		void set(final int otIdx, final int fsIdx, final int oIdx, IFitnessValue val) {
			originalFitnessValues[otIdx][oIdx][fsIdx] = val;
		}

		IFitnessValue get(final int otIdx, final int fsIdx, final int oIdx) {
			return originalFitnessValues[otIdx][oIdx][fsIdx];
		}

		public ObjectIterable<IFitnessValue> values(final ObjectType<?> type) {
			final int otIdx = objectTypeToIndex.get(type);
			return Iterables.iterable(this.originalFitnessValues[otIdx]);
		}
	}

	/**
	 * 
	 */
	public PValueCalculationResult(final List<ObjectType<?>> objectTypes, final ICalculatePValues calculatePValues,
			final long seed, final IFeatureStore featureStore, final List<IObjectWithFeatures>[] objectsPerObjectType,
			final List<IFitnessScore>[] fitnessScoresPerObjectType,
			final boolean[] calculateObjectSpecificDistributions,
			final List<IArithmeticFeature<? extends Comparable<?>>>[] conditionalFeaturesPerObjectType,
			final OriginalFitnessValues originalFitnessValues, final FitnessScorePValues fitnessScorePValues,
			final PValues pValues, final PermutedFeatureValues permutedFeatureValues,
			final PermutedFeatureValuesObjectSpecific permutedFeatureValuesObjectSpecific,
			final PermutedFitnessValues permutedFitnessValues,
			final PermutedFitnessValuesObjectSpecific permutedFitnessValuesObjectSpecific,
			final IntSet failedPermutations) {
		super();

		this.objectTypeToIndex = new LinkedHashMap<>();
		this.seed = seed;
		this.failedPermutations = failedPermutations;
		this.objectToIndexPerObjectType = new Map[objectTypes.size()];
		this.fitnessScoreToIndexPerObjectType = new Map[objectTypes.size()];
		this.conditionalFeaturesPerObjectType = new Map[objectTypes.size()];
		this.hasObjectSpecificDistributions = calculateObjectSpecificDistributions;
		int otIdx = 0;
		for (ObjectType<?> c : objectTypes) {
			this.objectTypeToIndex.put(c, otIdx);

			this.objectToIndexPerObjectType[otIdx] = new LinkedHashMap<>();
			int oIdx = 0;
			for (IObjectWithFeatures o : objectsPerObjectType[otIdx])
				this.objectToIndexPerObjectType[otIdx].put(o, oIdx++);

			this.fitnessScoreToIndexPerObjectType[otIdx] = new LinkedHashMap<>();
			int fsIdx = 0;
			for (IFitnessScore fs : fitnessScoresPerObjectType[otIdx])
				this.fitnessScoreToIndexPerObjectType[otIdx].put(fs, fsIdx++);

			this.conditionalFeaturesPerObjectType[otIdx] = new LinkedHashMap<>();
			int fIdx = 0;
			for (IArithmeticFeature<? extends Comparable<?>> f : conditionalFeaturesPerObjectType[otIdx])
				this.conditionalFeaturesPerObjectType[otIdx].put(f, fIdx++);
			otIdx++;
		}
		this.changeListener = new HashSet<>();
		this.calculatePValues = calculatePValues;
		this.featureStore = featureStore;
		this.originalFitnessValues = originalFitnessValues;
		this.fitnessScorePValues = fitnessScorePValues;
		this.pValues = pValues;
		this.permutedFeatureValues = permutedFeatureValues;
		this.permutedFitnessValues = permutedFitnessValues;
		this.permutedFeatureValuesObjectSpecific = permutedFeatureValuesObjectSpecific;
		this.permutedFitnessValuesObjectSpecific = permutedFitnessValuesObjectSpecific;
	}

	@Override
	public IFeatureValueProvider copy() throws InterruptedException {
		// TODO
		return null;
	}

	@Override
	public List<ObjectType<?>> providesValuesForObjectTypes() {
		return new ArrayList<>(this.objectTypeToIndex.keySet());
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof PValueCalculationResult))
			return false;

		final PValueCalculationResult other = (PValueCalculationResult) obj;
		return this.calculatePValues.equals(other.calculatePValues) && this.featureStore.equals(other.featureStore)
				&& this.fitnessScorePValues.equals(other.fitnessScorePValues) && this.pValues.equals(other.pValues);
	}

	@Override
	public boolean hasObjectSpecificPermutedFeatureValues(final ObjectType<?> type) {
		return this.hasObjectSpecificDistributions[this.objectTypeToIndex.get(type)];
	}

	/**
	 * @param calculatePValues the calculatePValues to set
	 */
	@Override
	public void setCalculatePValues(final ICalculatePValues calculatePValues) {
		this.calculatePValues = calculatePValues;
	}

	/**
	 * 
	 * @param featureStore
	 */
	@Override
	public void setFeatureStore(final IFeatureStore featureStore) {
		this.featureStore = featureStore;
	}

	/**
	 * @return the calculatePValues
	 */
	@Override
	public ICalculatePValues getCalculatePValues() {
		return this.calculatePValues;
	}

	/**
	 * @return the conditionalFeaturesPerObjectType
	 */
	@Override
	public List<IArithmeticFeature<? extends Comparable<?>>> getConditionalFeaturesPerObjectType(
			final ObjectType<?> objectType) {
		return new ArrayList<>(this.conditionalFeaturesPerObjectType[this.objectTypeToIndex.get(objectType)].keySet());
	}

	/**
	 * @return the clusterFeaturePValues
	 * @throws IncompatibleFeatureValueProviderException
	 */
	@Override
	public IPvalue getPValue(final IFitnessScore fitnessScore, final IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException {
		final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
		if (!(this.providesValuesForObjectTypes().contains(objectType)))
			throw new IncompatibleFeatureValueProviderException(this, objectType);
		return this.fitnessScorePValues.get(fitnessScore, object);
	}

	/**
	 * @return the clusterFeatureValues
	 */
	@Override
	public IFeatureStore getFeatureStore() {
		return this.featureStore;
	}

	@Override
	public <V> IFeatureValue<V> getFeatureValue(IFeature<V> feature, IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException, UnknownObjectFeatureValueProviderException,
			IncompatibleFeatureAndObjectException {
		final IFeatureValue<V> value = IPValueCalculationResult.super.getFeatureValue(feature, object);
		final IPvalue pval = this.pValues.get(object);
		if (value instanceof FeaturePvalueValue) {
			for (IFitnessScore fs : getFitnessScores(object.getObjectType())) {
				((FeaturePvalueValue) value).setFitnessValue(fs, originalFitnessValues.get(fs, object));

				if (pval instanceof EmpiricalPvalue) {
					((FeaturePvalueValue) value).setTotalObservations(((EmpiricalPvalue) pval).getTotalObservations());
					((FeaturePvalueValue) value)
							.setBetterObservations(((EmpiricalPvalue) pval).getBetterObservations());
				}
			}
		}
		return value;
	}

	@Override
	public <O extends IObjectWithFeatures> List<O> getObjects(ObjectType<O> type)
			throws IncompatibleFeatureValueProviderException {
		if (!containsPvaluesForObjectType(type))
			throw new IncompatibleFeatureValueProviderException(this, type);
		return new ArrayList<>(
				(Collection<? extends O>) this.objectToIndexPerObjectType[this.objectTypeToIndex.get(type)].keySet());
	}

	@Override
	public List<IFitnessScore> getFitnessScores(ObjectType<? extends IObjectWithFeatures> type) {
		return new ArrayList<>(fitnessScoreToIndexPerObjectType[this.objectTypeToIndex.get(type)].keySet());
	}

	@Override
	public List<IArithmeticFeature<? extends Comparable<?>>> getFeatures(
			ObjectType<? extends IObjectWithFeatures> type) {
		final List<IArithmeticFeature<? extends Comparable<?>>> result = new ArrayList<>();
		for (IFitnessScore fs : this.getFitnessScores(type))
			for (IArithmeticFeature<? extends Comparable<?>> f : fs.getFeatures())
				result.add(f);
		result.addAll(getConditionalFeaturesPerObjectType(type));
		return result;
	}

	@Override
	public IPvalue getPValue(final IObjectWithFeatures key) {
		return this.pValues.get(key);
	}

	@Override
	public ObjectList<IPvalue> getPValues(final ObjectType<?> type) throws IncompatibleFeatureValueProviderException {
		if (!containsPvaluesForObjectType(type))
			throw new IncompatibleFeatureValueProviderException(this, type);
		return this.pValues.get(type);
	}

	@Override
	public boolean hasPermutedFeatureValues() {
		return this.permutedFeatureValues != null;
	}

	@Override
	public long getSeed() {
		return this.seed;
	}

	/**
	 * @return the failedPermutations
	 */
	public IntSet getFailedPermutations() {
		return this.failedPermutations;
	}

	/**
	 * @return the permutedFitnessValues
	 * @throws IncompatibleFeatureValueProviderException
	 */
	@Override
	public <V> List<IFeatureValue<V>> getPermutedFeatureValues(final ObjectType<?> objectType,
			final IArithmeticFeature<V> feature) throws IncompatibleFeatureValueProviderException {
		if (!(this.providesValuesForObjectTypes().contains(objectType)))
			throw new IncompatibleFeatureValueProviderException(this, objectType);

		if (this.hasObjectSpecificPermutedFeatureValues(objectType)) {
			if (this.permutedFeatureValuesObjectSpecific == null)
				return null;
			return (List) this.permutedFeatureValuesObjectSpecific.get(objectType, feature);
		} else {
			if (this.permutedFeatureValues == null)
				return null;
			return (List) this.permutedFeatureValues.get(objectType).get(feature);
		}
	}

	/**
	 * @return the permutedFeatureValuesObjectSpecific
	 * @throws IncompatibleFeatureValueProviderException
	 */
	@Override
	public <V> List<IFeatureValue<V>> getPermutedFeatureValuesObjectSpecific(final IObjectWithFeatures object,
			final IArithmeticFeature<V> feature) throws IncompatibleFeatureValueProviderException {
		final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
		if (!(this.providesValuesForObjectTypes().contains(objectType)))
			throw new IncompatibleFeatureValueProviderException(this, objectType);
		if (this.permutedFeatureValuesObjectSpecific == null)
			return null;
		return (List) this.permutedFeatureValuesObjectSpecific.get(object).get(feature);
	}

	@Override
	public final IFitnessValue getOriginalFitness(final IFitnessScore fitnessScore, final IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException {
		final ObjectType<? extends IObjectWithFeatures> objectType = object.getObjectType();
		if (!(this.providesValuesForObjectTypes().contains(objectType)))
			throw new IncompatibleFeatureValueProviderException(this, objectType);
		return this.originalFitnessValues.get(fitnessScore, object);
	}

	@Override
	public void clearPermutedFeatureValues() {
		this.permutedFeatureValues = null;
		this.fireStateChanged();
	}

	@Override
	public void clearPermutedFeatureValuesObjectSpecific() {
		this.permutedFeatureValuesObjectSpecific = null;
		this.fireStateChanged();
	}

	@Override
	public void clearPermutedFitnessValues() {
		this.permutedFitnessValues = null;
		this.fireStateChanged();
	}

	@Override
	public void clearPermutedFitnessValuesObjectSpecific() {
		this.permutedFitnessValuesObjectSpecific = null;
		this.fireStateChanged();
	}

	@Override
	public void clearOriginalFitness() {
		this.originalFitnessValues = null;
		this.fireStateChanged();
	}

	@Override
	public Collection<? extends IFeatureWithValueProvider<?>> featuresProvidedValuesFor(final ObjectType<?> objectType)
			throws IncompatibleFeatureValueProviderException {
		if (!providesValuesForObjectTypes().contains(objectType))
			throw new IncompatibleFeatureValueProviderException(this, objectType);
		return Arrays.asList(new FeaturePvalue(objectType));
	}
}
