/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.feature.FeatureComparisonException;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
public class Quantiles {
	/**
	 * 
	 * R type 2 quantile method.
	 * 
	 * @param values
	 * @param quantiles
	 * @return
	 * @throws ToNumberConversionException
	 * @throws NotAnArithmeticFeatureValueException
	 */
	public static double[] calculate(final List<? extends IFeatureValue<?>> values, final double[] quantiles)
			throws NotAnArithmeticFeatureValueException, ToNumberConversionException {
		ensureValidQuantiles(quantiles);

		Collections.sort(values, new Comparator<IFeatureValue<?>>() {

			@Override
			public int compare(IFeatureValue<?> o1, IFeatureValue<?> o2) {
				try {
					return Double.compare(o1.toNumber().doubleValue(), o2.toNumber().doubleValue());
				} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException e) {
					throw new FeatureComparisonException(e);
				}
			}
		});
		final int n = values.size();

		final double[] cdf = new double[quantiles.length];
		for (int i = 0; i < quantiles.length; i++) {
			final double p = quantiles[i];
			if (p == 0.0)
				cdf[i] = values.get(0).toNumber().doubleValue();
			else if (p == 1.0)
				cdf[i] = values.get(values.size() - 1).toNumber().doubleValue();
			else {
				final double h = n * p + 0.5;
				final int x1 = (int) Math.ceil(h - 0.5) - 1;
				final int x2 = (int) Math.floor(h + 0.5) - 1;

				cdf[i] = ArraysExt.mean(values.get(x1).toNumber().doubleValue(),
						values.get(x2).toNumber().doubleValue());
			}
		}
		return cdf;
	}

	private static void ensureValidQuantiles(final double[] quantiles) {
		for (final double q : quantiles)
			if (q < 0.0 || q > 1.0)
				throw new IllegalArgumentException("Quantiles need to be within [0.0,1.0]");
	}
}
