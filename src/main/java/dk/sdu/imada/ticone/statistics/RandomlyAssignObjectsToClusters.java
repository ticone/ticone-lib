/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import dk.sdu.imada.ticone.clustering.ClusterFactoryException;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.CreateClusterInstanceFactoryException;
import dk.sdu.imada.ticone.clustering.DuplicateMappingForObjectException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 19, 2018
 *
 */
public class RandomlyAssignObjectsToClusters
		extends AbstractObjectProviderFactory<ITimeSeriesObjects, ClusterObjectMapping> {

	protected IPrototypeBuilder prototypeFactory;

	private ISimilarityFunction similarityFunction;
	protected int numberClusters;

	/**
	 * 
	 */
	public RandomlyAssignObjectsToClusters(final long seed, final IPrototypeBuilder prototypeFactory,
			final ISimilarityFunction similarityFunction, final int numberClusters) {
		super(seed);
		this.prototypeFactory = prototypeFactory;
		this.numberClusters = numberClusters;
		this.similarityFunction = similarityFunction;
	}

	@Override
	public ClusterObjectMapping generateFrom(final ITimeSeriesObjects template) throws PrototypeFactoryException,
			CreatePrototypeInstanceFactoryException, InterruptedException, ClusterFactoryException,
			CreateClusterInstanceFactoryException, IncompatiblePrototypeException, SimilarityCalculationException,
			SimilarityValuesException, IncompatibleSimilarityValueException, DuplicateMappingForObjectException {
		final ITimeSeriesObjectList objectList = template.asList();

		final Random random = new Random(seed);

		final int[] objectToClusterId = random.ints(objectList.size(), 0, this.numberClusters).toArray();
		final Map<Integer, ITimeSeriesObjects> clusterIdToObjects = new HashMap<>();
		for (int objId = 0; objId < objectToClusterId.length; objId++) {
			final int cId = objectToClusterId[objId];
			clusterIdToObjects.putIfAbsent(cId, new TimeSeriesObjectList());
			clusterIdToObjects.get(cId).add(objectList.get(objId));
		}

		final ClusterObjectMapping clustering = new ClusterObjectMapping(template.asSet().copy(), prototypeFactory);
		for (final int clusterId : clusterIdToObjects.keySet()) {
			final ITimeSeriesObjects clusterObjects = clusterIdToObjects.get(clusterId);
			final ICluster newCluster = clustering.addCluster(clusterObjects, similarityFunction);
			// make sure that we keep all the clusters and return exactly
			// the same number of clusters as were specified
			newCluster.setKeep(true);
		}
		return clustering;
	}

}
