/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.Collections;
import java.util.List;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 19, 2017
 *
 */
public class BonferroniPValues extends AbstractCombinePValues {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7872186684133995109L;

	@Override
	public IPvalue combine(final IObjectWithFeatures cluster, final List<IPvalue> pvalues) {
		return new BasicPvalue(Math.min(pvalues.size() * Collections.min(pvalues).getDouble(), 1.0));
	}

	@Override
	public String getName() {
		return "Bonferroni";
	}
}
