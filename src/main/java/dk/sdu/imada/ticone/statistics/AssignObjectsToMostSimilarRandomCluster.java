/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;

import dk.sdu.imada.ticone.clustering.ClusterFactoryException;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.CreateClusterInstanceFactoryException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.data.MaxValueTimeSeriesFeature;
import dk.sdu.imada.ticone.data.MinValueTimeSeriesFeature;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.network.INetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 12, 2019
 *
 */
public class AssignObjectsToMostSimilarRandomCluster
		extends AbstractObjectProviderFactory<ITimeSeriesObjects, ClusterObjectMapping> {

	protected IPrototypeBuilder prototypeFactory;

	protected int numberClusters;

	protected ISimilarityFunction similarityFunction;

	protected ITiconeNetwork<ITiconeNetworkNode, ?> network;

	private final MinValueTimeSeriesFeature minF = new MinValueTimeSeriesFeature();
	private final MaxValueTimeSeriesFeature maxF = new MaxValueTimeSeriesFeature();

	/**
	 * 
	 */
	public AssignObjectsToMostSimilarRandomCluster(final long seed, final IPrototypeBuilder prototypeFactory,
			final int numberClusters, final ISimilarityFunction similarityFunction,
			final ITiconeNetwork<ITiconeNetworkNode, ?> network) {
		super(seed);
		this.prototypeFactory = prototypeFactory;
		this.numberClusters = numberClusters;
		this.similarityFunction = similarityFunction;
		this.network = network;
	}

	@Override
	public ClusterObjectMapping generateFrom(final ITimeSeriesObjects template)
			throws PrototypeFactoryException, CreatePrototypeInstanceFactoryException, InterruptedException,
			ClusterFactoryException, CreateClusterInstanceFactoryException {
		try {
			final Random random = new Random(seed);
			final ITimeSeriesObjectList objectList = template.asList();
			int numberTimepoints = objectList.get(0).getOriginalTimeSeriesList()[0].getNumberTimePoints();
			double minValue = Double.POSITIVE_INFINITY, maxValue = Double.NEGATIVE_INFINITY;
			for (ITimeSeriesObject o : objectList) {
				for (ITimeSeries ts : o.getOriginalTimeSeriesList()) {
					minValue = Math.min(minValue, minF.calculate(ts).getValue().doubleValue());
					maxValue = Math.max(maxValue, maxF.calculate(ts).getValue().doubleValue());
				}
			}

			final ClusterObjectMapping clustering = new ClusterObjectMapping(objectList.asSet().copy(),
					prototypeFactory);
			final ITimeSeriesPrototypeComponentBuilder componentFactoryTimeSeries = PrototypeComponentType.TIME_SERIES
					.getComponentFactory(this.prototypeFactory);
			final INetworkLocationPrototypeComponentBuilder componentFactoryNetwork = PrototypeComponentType.NETWORK_LOCATION
					.getComponentFactory(this.prototypeFactory);

			// random assignment of objects to clusters; needed to assign a random network
			// location to prototypes
			int[] randomClusterAssignment = random.ints(objectList.size(), 0, numberClusters).toArray();

			for (int c = 0; c < numberClusters; c++) {
				componentFactoryTimeSeries
						.setTimeSeries(random.doubles(numberTimepoints, minValue, maxValue).toArray());
				final Collection<INetworkMappedTimeSeriesObject> nodes = new HashSet<>();
				for (int o = 0; o < randomClusterAssignment.length; o++) {
					if (randomClusterAssignment[o] == c) {
						final ITimeSeriesObject object = objectList.get(o);
						nodes.add(object.mapToNetworkNode(network, network.getNode(object.getName())));
					}
				}
				componentFactoryNetwork.setNetworkLocation(nodes);

				final ICluster newCluster = clustering.addCluster(this.prototypeFactory.setObjects(objectList).build());
				// make sure that we keep all the clusters and return exactly
				// the same number of clusters as were specified
				newCluster.setKeep(true);
			}

			clustering.addObjectsToMostSimilarCluster(objectList, similarityFunction);
			return clustering;
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeFactoryException e) {
			throw new PrototypeFactoryException(e);
		} catch (SimilarityCalculationException | SimilarityValuesException | NoComparableSimilarityValuesException
				| IncompatibleSimilarityValueException | IncompatiblePrototypeException | FeatureCalculationException
				| IncompatibleFeatureAndObjectException | IncorrectlyInitializedException e) {
			throw new CreateClusterInstanceFactoryException(e);
		}
	}

}
