/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.List;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 7, 2018
 *
 */
public class FeatureValueSampler implements IFeatureValueSampler {

	@Override
	public <V> IFeatureValueSample<V> sampleFrom(final IFeature<V> feature, final IObjectProvider objectProvider,
			final int sampleSize, final long seed) throws FeatureValueSampleException {
		if (feature instanceof IFeatureWithValueProvider)
			if (((IFeatureWithValueProvider) feature).getFeatureValueProvider() == null)
				throw new FeatureValueSampleException("Feature requires a value provider, but is not set.");
		try {
			if (!objectProvider.isProvidingObjectsOfType(feature.supportedObjectType()))
				throw new IncompatibleObjectProviderException(objectProvider, feature.supportedObjectType());

			final IFeatureValueSample<V> result = new FeatureValueSample<>(feature);

			final List<? extends IObjectWithFeatures> objects = objectProvider
					.sampleObjectsOfType(feature.supportedObjectType(), sampleSize, seed);

			for (IObjectWithFeatures object : objects)
				result.setValue(object, feature.calculate(object));
			return result;
		} catch (final Exception e) {
			throw new FeatureValueSampleException(e);
		}
	}

	@Override
	public <V> IFeatureValueSample<V> sampleFrom(final IFeatureWithValueProvider<V> feature,
			final IObjectProvider objectProvider, final IFeatureValueProvider featureValueProvider,
			final int sampleSize, final long seed) throws FeatureValueSampleException {
		try {
			feature.setFeatureValueProvider(featureValueProvider);
		} catch (IncompatibleFeatureValueProviderException e) {
			throw new FeatureValueSampleException(e);
		}
		return this.sampleFrom(feature, objectProvider, sampleSize, seed);
	}

}
