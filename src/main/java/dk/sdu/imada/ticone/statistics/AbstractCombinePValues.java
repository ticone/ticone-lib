/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 21, 2017
 *
 */
public abstract class AbstractCombinePValues implements ICombinePValues {

	/**
	 * 
	 */
	private static final long serialVersionUID = 99509282647819083L;

	@Override
	public String toString() {
		return this.getName();
	}

}