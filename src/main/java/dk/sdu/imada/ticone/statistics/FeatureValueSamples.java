/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 15, 2018
 *
 */
public class FeatureValueSamples implements IFeatureValueSamples {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6496168692205407755L;

	private transient Logger logger;
	protected IObjectProvider provider;

	protected Map<IFeature<?>, IFeatureValueSample<?>> samples;

	/**
	 * 
	 */
	public FeatureValueSamples(final IObjectProvider provider) {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.provider = provider;
		this.samples = new HashMap<>();
	}

	/**
	 * @return the provider
	 */
	@Override
	public IObjectProvider getProvider() {
		return this.provider;
	}

	@Override
	public boolean contains(final IFeature<?> feature, final int sampleSize) {
		return this.samples.containsKey(feature) && this.samples.get(feature).size() >= sampleSize;
	}

	@Override
	public <O extends IObjectWithFeatures, V> IFeatureValueSample<V> get(final IFeature<V> feature,
			final int sampleSize, final long seed) throws FeatureValueSampleException {
		if (this.contains(feature, sampleSize))
			return (IFeatureValueSample<V>) this.samples.get(feature);

		logger.info(String.format(
				"No feature value sample found with sample size >=%d for provider '%s' and feature '%s' - generating ...",
				sampleSize, this.provider, feature));
		final IFeatureValueSample<V> sample = new FeatureValueSampler().sampleFrom(feature, this.getProvider(),
				sampleSize, seed);
		logger.info(" done");
		this.samples.put(feature, sample);
		return sample;
	}

	@Override
	public <O extends IObjectWithFeatures, V> IFeatureValueSample<? extends V> get(
			final IFeatureWithValueProvider<? extends V> feature, final IFeatureValueProvider featureValueProvider,
			final int sampleSize, final long seed) throws FeatureValueSampleException {
		if (this.contains(feature, sampleSize))
			return (IFeatureValueSample<V>) this.samples.get(feature);

		logger.info(String.format(
				"No feature value sample found with sample size >=%d for provider '%s' and feature '%s' - generating ...",
				sampleSize, this.provider, feature));
		final IFeatureValueSample<? extends V> sample = new FeatureValueSampler().sampleFrom(feature,
				this.getProvider(), featureValueProvider, sampleSize, seed);
		logger.info(" done");
		this.samples.put(feature, sample);
		return sample;
	}

	private void readObject(final ObjectInputStream in) throws ClassNotFoundException, IOException {
		in.defaultReadObject();
		this.logger = LoggerFactory.getLogger(this.getClass());
	}
}
