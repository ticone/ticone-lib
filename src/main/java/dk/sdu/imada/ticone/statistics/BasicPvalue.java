/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 29, 2019
 *
 */
public class BasicPvalue implements IPvalue, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7682311040850287362L;
	protected final double pvalue;

	public BasicPvalue(double pvalue) {
		super();
		this.pvalue = pvalue;
	}

	@Override
	public double getDouble() {
		return this.pvalue;
	}

	@Override
	public String toString() {
		return Double.toString(pvalue);
	}
}
