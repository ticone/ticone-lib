/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.List;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 19, 2017
 *
 */
public class MultiplyPValues extends AbstractCombinePValues {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2361576351414046960L;
	protected double pseudoCount;

	/**
	 * 
	 */
	public MultiplyPValues() {
		this(0.00001);
	}

	/**
	 * 
	 */
	public MultiplyPValues(final double pseudoCount) {
		super();
		this.pseudoCount = pseudoCount;
	}

	/**
	 * @param pseudoCount the pseudoCount to set
	 */
	public void setPseudoCount(final double pseudoCount) {
		this.pseudoCount = pseudoCount;
	}

	/**
	 * @return the pseudoCount
	 */
	public double getPseudoCount() {
		return this.pseudoCount;
	}

	@Override
	public IPvalue combine(final IObjectWithFeatures cluster, final List<IPvalue> pvalues) {
		double result = 1.0;
		for (final IPvalue p : pvalues) {
			if (p.getDouble() == 0.0)
				result *= this.pseudoCount;
			else
				result *= p.getDouble();
		}
		return new BasicPvalue(result);
	}

	@Override
	public String getName() {
		return "Multiply";
	}
}
