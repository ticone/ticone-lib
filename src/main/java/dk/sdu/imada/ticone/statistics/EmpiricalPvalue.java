/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 29, 2019
 *
 */
public class EmpiricalPvalue extends BasicPvalue {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1368717236739988701L;

	protected final long totalObservations, betterObservations;

	public EmpiricalPvalue(long totalObservations, long betterObservations) {
		super(totalObservations > 0 ? (double) (1 + betterObservations) / (1 + totalObservations) : Double.NaN);
		this.totalObservations = totalObservations > 0 ? totalObservations + 1 : totalObservations;
		this.betterObservations = totalObservations > 0 ? betterObservations + 1 : betterObservations;
	}

	/**
	 * @return the totalObservations
	 */
	public long getTotalObservations() {
		return this.totalObservations;
	}

	/**
	 * @return the betterObservations
	 */
	public long getBetterObservations() {
		return this.betterObservations;
	}

	@Override
	public String toString() {
		return String.format("%.5f (%d/%d)", this.pvalue, this.betterObservations, this.totalObservations);
	}
}
