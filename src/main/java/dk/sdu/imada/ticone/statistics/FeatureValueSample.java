/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import de.wiwie.wiutils.utils.Pair;
import dk.sdu.imada.ticone.feature.BasicFeatureStore;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 9, 2018
 *
 */
public class FeatureValueSample<R> implements IFeatureValueSample<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 175199267205184653L;

	public static <R> FeatureValueSample<R> fromObservations(final IFeature<R> feature,
			final Pair<IObjectWithFeatures, IFeatureValue<R>>... observations) {
		final FeatureValueSample<R> sample = new FeatureValueSample<>(feature);
		for (final Pair<IObjectWithFeatures, IFeatureValue<R>> p : observations)
			sample.setValue(p.getFirst(), p.getSecond());
		return sample;
	}

	protected final IFeatureStore featureStore;

	protected final IFeature<R> feature;

	protected final List<IObjectWithFeatures> sample;

	/**
	 * 
	 */
	public FeatureValueSample(final IFeature<R> feature) {
		super();
		this.featureStore = new BasicFeatureStore();
		this.feature = feature;
		this.sample = new ArrayList<>();
	}

	/**
	 * @return the feature
	 */
	public IFeature<R> getFeature() {
		return this.feature;
	}

	@Override
	public IFeatureStore getFeatureStore() {
		return this.featureStore;
	}

	/**
	 * @return the sample
	 */
	@Override
	public List<IObjectWithFeatures> getObjects() {
		return this.sample;
	}

	@Override
	public List<IFeatureValue<R>> getValues() {
		return this.sample.stream().map(o -> this.featureStore.getFeatureValue(o, this.feature))
				.collect(Collectors.toList());
	}

	@Override
	public void setValue(final IObjectWithFeatures object, final IFeatureValue<R> value) {
		this.featureStore.setFeatureValue(object, this.feature, value);
		this.sample.add(object);
	}

	@Override
	public IFeatureValue<R> getValue(final IObjectWithFeatures object) {
		return this.featureStore.getFeatureValue(object, this.feature);
	}

	@Override
	public Iterator<IFeatureValueSample.Entry<R>> iterator() {
		return new Iterator<IFeatureValueSample.Entry<R>>() {
			protected int pos = 0;

			@Override
			public boolean hasNext() {
				return this.pos < FeatureValueSample.this.sample.size();
			}

			@Override
			public IFeatureValueSample.Entry<R> next() {
				final IObjectWithFeatures object = FeatureValueSample.this.sample.get(this.pos++);
				return new Entry(object, FeatureValueSample.this.getValue(object));
			}
		};
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		for (final IObjectWithFeatures o : this.sample) {
			sb.append(o);
			sb.append(":");
			sb.append(this.getValue(o));
			sb.append("\t");
		}
		return sb.toString();
	}

	public class Entry implements IFeatureValueSample.Entry<R> {

		protected final IObjectWithFeatures object;

		protected final IFeatureValue<R> value;

		Entry(final IObjectWithFeatures object, final IFeatureValue<R> value) {
			super();
			this.object = object;
			this.value = value;
		}

		@Override
		public IObjectWithFeatures getObject() {
			return this.object;
		}

		@Override
		public IFeatureValue<R> getValue() {
			return this.value;
		}

	}
}
