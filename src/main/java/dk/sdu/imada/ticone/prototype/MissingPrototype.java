/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import java.util.Collection;
import java.util.Collections;

import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder.IPrototypeComponent;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 6, 2018
 *
 */
public class MissingPrototype implements IPrototype {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5803679241177652330L;

	@Override
	public boolean hasPrototypeComponent(final PrototypeComponentType componentType) {
		return false;
	}

	@Override
	public IPrototypeComponent<?> getPrototypeComponent(final PrototypeComponentType componentType)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		throw new MissingPrototypeException();
	}

	@Override
	public Collection<PrototypeComponentType> getPrototypeComponentTypes() {
		return Collections.emptyList();
	}

	@Override
	public Collection<? extends IPrototypeComponent<?>> getPrototypeComponents() {
		return Collections.emptyList();
	}

	@Override
	public IPrototypeComponent<?> addPrototypeComponent(final IPrototypeComponent<?> component) {
		throw null;
	}

	@Override
	public IPrototype copy() {
		return this;
	}

}
