/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder.IPrototypeComponent;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 1, 2018
 *
 */
public class PrototypeBuilder
		extends AbstractBuilder<IPrototype, PrototypeFactoryException, CreatePrototypeInstanceFactoryException>
		implements IPrototypeBuilder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8795273562524760264L;

	protected static Map<PrototypeComponentType, IPrototypeComponent<?>> toMap(
			final IPrototypeComponent<?>... components) {
		final Map<PrototypeComponentType, IPrototypeComponent<?>> result = new HashMap<>();
		for (final IPrototypeComponent<?> c : components)
			result.put(c.getType(), c);
		return result;
	}

	protected Map<PrototypeComponentType, IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?>> prototypeComponentFactories;

	protected transient Map<PrototypeComponentType, IPrototypeComponent<?>> components;
	protected transient Map<PrototypeComponentType, IPrototypeComponent<?>> overrideComponents;

	protected transient ITimeSeriesObjects objects;

	@Override
	public Map<PrototypeComponentType, IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?>> getPrototypeComponentFactories() {
		if (this.prototypeComponentFactories == null)
			return Collections.emptyMap();
		return this.prototypeComponentFactories;
	}

	@Override
	public IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?> getPrototypeComponentFactory(
			final PrototypeComponentType type) throws IncompatiblePrototypeComponentException {
		if (this.prototypeComponentFactories == null || !this.prototypeComponentFactories.containsKey(type))
			throw new IncompatiblePrototypeComponentException();
		return this.prototypeComponentFactories.get(type);
	}

	/**
	 * @param prototypeComponentFactories the prototypeComponentFactories to set
	 */
	public PrototypeBuilder addPrototypeComponentFactory(
			final IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?> prototypeComponentFactory) {
		if (this.prototypeComponentFactories == null)
			this.prototypeComponentFactories = new LinkedHashMap<>();
		this.prototypeComponentFactories.put(prototypeComponentFactory.getType(), prototypeComponentFactory);
		return this;
	}

	/**
	 * @param prototypeComponentFactories the prototypeComponentFactories to set
	 */
	@Override
	@SafeVarargs
	public final PrototypeBuilder setPrototypeComponentBuilders(
			final IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?>... prototypeComponentFactories) {
		this.prototypeComponentFactories = new HashMap<>();
		for (final IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?> c : prototypeComponentFactories)
			this.prototypeComponentFactories.put(c.getType(), c);
		return this;
	}

	/**
	 * @return the components
	 */
	@Override
	public Map<PrototypeComponentType, IPrototypeComponent<?>> getComponents() {
		return this.components;
	}

	@Override
	public PrototypeBuilder setComponents(final IPrototypeComponent<?> component,
			final IPrototypeComponent<?>... components) {
		this.components = new LinkedHashMap<>();
		this.components.put(component.getType(), component);
		this.components.putAll(toMap(components));
		return this;
	}

	@Override
	public Map<PrototypeComponentType, IPrototypeComponent<?>> getOverrideComponents() {
		return this.overrideComponents;
	}

	/**
	 * @param overrideComponents the overrideComponents to set
	 * @return
	 */
	@Override
	public PrototypeBuilder setOverrideComponents(
			final Map<PrototypeComponentType, IPrototypeComponent<?>> overrideComponents) {
		this.overrideComponents = overrideComponents;
		return this;
	}

	@Override
	public PrototypeBuilder setObjects(final ITimeSeriesObjects objects) {
		this.objects = objects;
		return this;
	}

	@Override
	public ITimeSeriesObjects getObjects() {
		return this.objects;
	}

	@Override
	public PrototypeBuilder copy() {
		final PrototypeBuilder f = new PrototypeBuilder();
		if (this.components != null)
			f.components = new HashMap<>(this.components);
		f.objects = this.objects;
		if (this.overrideComponents != null)
			f.overrideComponents = new HashMap<>(this.overrideComponents);
		if (this.prototypeComponentFactories != null) {
			f.prototypeComponentFactories = new HashMap<>();
			this.prototypeComponentFactories.forEach((t, cf) -> {
				if (cf != null)
					f.prototypeComponentFactories.put(t, cf.copy());
				else
					f.prototypeComponentFactories.put(t, cf);
			});
		}
		return f;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this.prototypeComponentFactories == null) ? 0 : this.prototypeComponentFactories.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrototypeBuilder other = (PrototypeBuilder) obj;
		if (this.prototypeComponentFactories == null) {
			if (other.prototypeComponentFactories != null)
				return false;
		} else if (!this.prototypeComponentFactories.equals(other.prototypeComponentFactories))
			return false;
		return true;
	}

	@Override
	protected Prototype doBuild() throws CreatePrototypeInstanceFactoryException, InterruptedException {
		if (this.components != null) {
			if (this.components.size() == 1
					&& this.components.keySet().iterator().next() == PrototypeComponentType.TIME_SERIES) {
				final Prototype tsPrototype = new TimeSeriesPrototype();
				tsPrototype.addPrototypeComponent(this.components.values().iterator().next());
				return tsPrototype;
			} else if (this.components.size() == 2 && this.components.keySet().equals(new HashSet<>(
					Arrays.asList(PrototypeComponentType.TIME_SERIES, PrototypeComponentType.NETWORK_LOCATION)))) {
				final Prototype tsPrototype = new TimeSeriesAndNetworkLocationPrototype();
				tsPrototype.addPrototypeComponent(this.components.get(PrototypeComponentType.TIME_SERIES));
				tsPrototype.addPrototypeComponent(this.components.get(PrototypeComponentType.NETWORK_LOCATION));
				return tsPrototype;
			} else {
				return new Prototype(this.components);
			}
		} else if (this.prototypeComponentFactories != null) {
			try {
				final Prototype prototype;
				if (this.prototypeComponentFactories.size() == 1 && this.prototypeComponentFactories.keySet().iterator()
						.next() == PrototypeComponentType.TIME_SERIES) {
					prototype = new TimeSeriesPrototype();
				} else if (this.prototypeComponentFactories.size() == 2 && this.prototypeComponentFactories.keySet()
						.equals(new HashSet<>(Arrays.asList(PrototypeComponentType.TIME_SERIES,
								PrototypeComponentType.NETWORK_LOCATION)))) {
					prototype = new TimeSeriesAndNetworkLocationPrototype();
				} else {
					prototype = new Prototype();
				}
				final Map<PrototypeComponentType, IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?>> componentFactories = this.prototypeComponentFactories;
				for (final PrototypeComponentType t : componentFactories.keySet())
					if (this.overrideComponents != null && this.overrideComponents.containsKey(t))
						prototype.addPrototypeComponent(this.overrideComponents.get(t));
					else if (this.objects != null)
						prototype.addPrototypeComponent(componentFactories.get(t).setObjects(this.objects).build());
					else
						prototype.addPrototypeComponent(componentFactories.get(t).build());

				return prototype;
			} catch (final InterruptedException e) {
				throw e;
			} catch (final Exception e) {
				throw new CreatePrototypeInstanceFactoryException(e);
			}

		}
		throw new CreatePrototypeInstanceFactoryException(
				"Neither prototype components nor prototype component builders are set.");
	}

	@Override
	protected void reset() {
		objects = null;
		overrideComponents = null;
	}

	@Override
	public IPrototype copy(final IPrototype template, final IPrototypeComponent<?>... overrideComponents)
			throws PrototypeFactoryException {
		try {
			final Map<PrototypeComponentType, IPrototypeComponent<?>> map = toMap(overrideComponents);
			final Prototype prototype = new Prototype();
			for (final PrototypeComponentType t : template.getPrototypeComponentTypes()) {
				if (map.containsKey(t))
					prototype.addPrototypeComponent(map.get(t));
				else
					prototype.addPrototypeComponent(template.getPrototypeComponent(t));
			}
			return prototype;
		} catch (final Exception e) {
			throw new PrototypeFactoryException(e);
		}
	}

	@Override
	public boolean producesComparablePrototypesWith(final IPrototypeBuilder prototypeFactory) {
		final Set<PrototypeComponentType> effectiveComponentTypes1 = new HashSet<>(),
				effectiveComponentTypes2 = new HashSet<>();

		if (this.components != null)
			effectiveComponentTypes1.addAll(this.components.keySet());
		if (prototypeFactory.getComponents() != null)
			effectiveComponentTypes2.addAll(prototypeFactory.getComponents().keySet());

		if (this.prototypeComponentFactories != null)
			effectiveComponentTypes1.addAll(this.prototypeComponentFactories.keySet());
		if (prototypeFactory.getPrototypeComponentFactories() != null)
			effectiveComponentTypes2.addAll(prototypeFactory.getPrototypeComponentFactories().keySet());

		return effectiveComponentTypes1.equals(effectiveComponentTypes2);
	}

	@Override
	public boolean producesComparablePrototypesTo(final IPrototype prototype) {
		final Set<PrototypeComponentType> effectiveComponentTypes1 = new HashSet<>(),
				effectiveComponentTypes2 = new HashSet<>();

		if (this.components != null)
			effectiveComponentTypes1.addAll(this.components.keySet());
		if (this.prototypeComponentFactories != null)
			effectiveComponentTypes1.addAll(this.prototypeComponentFactories.keySet());

		if (prototype.getPrototypeComponentTypes() != null)
			effectiveComponentTypes2.addAll(prototype.getPrototypeComponentTypes());

		return effectiveComponentTypes1.equals(effectiveComponentTypes2);
	}
}

class TimeSeriesPrototype extends Prototype {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7005228790531555898L;

	TimeSeriesPrototype() {
		super();
		this.components.add(null);
		this.componentTypes.put(PrototypeComponentType.TIME_SERIES, 0);
	}

	@Override
	public IPrototypeComponent<?> addPrototypeComponent(IPrototypeComponent<?> component) {
		if (component.getType() == PrototypeComponentType.TIME_SERIES)
			return super.setPrototypeComponent(0, component);
		return null;
	}

	@Override
	public IPrototypeComponent<?> getPrototypeComponent(PrototypeComponentType componentType)
			throws IncompatiblePrototypeComponentException {
		if (componentType == PrototypeComponentType.TIME_SERIES)
			return getPrototypeComponent(0);
		throw new IncompatiblePrototypeComponentException();
	}

	@Override
	public boolean hasPrototypeComponent(PrototypeComponentType componentType) {
		return componentType == PrototypeComponentType.TIME_SERIES;
	}

	@Override
	public IPrototype copy() {
		final TimeSeriesPrototype timeSeriesPrototype = new TimeSeriesPrototype();
		timeSeriesPrototype.addPrototypeComponent(components.get(0));
		return timeSeriesPrototype;
	}
}

class TimeSeriesAndNetworkLocationPrototype extends Prototype {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7005228790531555898L;

	TimeSeriesAndNetworkLocationPrototype() {
		super();
		this.components.add(null);
		this.components.add(null);
		this.componentTypes.put(PrototypeComponentType.TIME_SERIES, 0);
		this.componentTypes.put(PrototypeComponentType.NETWORK_LOCATION, 1);
	}

	@Override
	public IPrototypeComponent<?> addPrototypeComponent(IPrototypeComponent<?> component) {
		if (component.getType() == PrototypeComponentType.TIME_SERIES)
			return super.setPrototypeComponent(0, component);
		else if (component.getType() == PrototypeComponentType.NETWORK_LOCATION)
			return super.setPrototypeComponent(1, component);
		return null;
	}

	@Override
	public IPrototypeComponent<?> getPrototypeComponent(PrototypeComponentType componentType)
			throws IncompatiblePrototypeComponentException {
		if (componentType == PrototypeComponentType.TIME_SERIES)
			return getPrototypeComponent(0);
		else if (componentType == PrototypeComponentType.NETWORK_LOCATION)
			return getPrototypeComponent(1);
		throw new IncompatiblePrototypeComponentException();
	}

	@Override
	public boolean hasPrototypeComponent(PrototypeComponentType componentType) {
		return componentType == PrototypeComponentType.TIME_SERIES
				|| componentType == PrototypeComponentType.NETWORK_LOCATION;
	}

	@Override
	public IPrototype copy() {
		final TimeSeriesAndNetworkLocationPrototype timeSeriesPrototype = new TimeSeriesAndNetworkLocationPrototype();
		timeSeriesPrototype.addPrototypeComponent(components.get(0).copy());
		timeSeriesPrototype.addPrototypeComponent(components.get(1).copy());
		return timeSeriesPrototype;
	}
}

class Prototype implements IPrototype {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1891021564793838826L;
	protected final Map<PrototypeComponentType, Integer> componentTypes;
	protected final List<IPrototypeComponent<?>> components;

	/**
	 * 
	 */
	Prototype() {
		this(new HashMap<>());
	}

	Prototype(final Map<PrototypeComponentType, IPrototypeComponent<?>> components) {
		super();
		// avoid resizing if possible
		this.components = new ArrayList<>(components.size());
		// avoid rehashing if possible
		this.componentTypes = new HashMap<>(components.size() * 2);
		int i = 0;
		for (PrototypeComponentType t : components.keySet()) {
			final IPrototypeComponent<?> c = components.get(t);
			this.components.set(i, c);
			this.componentTypes.put(t, i++);
		}
	}

	Prototype(final List<PrototypeComponentType> componentTypes, final List<IPrototypeComponent<?>> components)
			throws IncompatiblePrototypeComponentException {
		super();
		this.components = components;
		this.componentTypes = new HashMap<>();
		if (!(components.size() == componentTypes.size()))
			throw new IncompatiblePrototypeComponentException();

		for (int i = 0; i < components.size(); i++) {
			if (components.get(i) != null && !(components.get(i).getType().equals(componentTypes.get(i))))
				throw new IncompatiblePrototypeComponentException();
			this.componentTypes.put(componentTypes.get(i), i);
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof Prototype)
			return Objects.equals(this.components, ((Prototype) obj).components);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.components);
	}

	@Override
	public String toString() {
		return String.format("Prototype: %s", this.components);
	}

	@Override
	public IPrototype copy() {
		try {
			return new Prototype(this.components.stream().map(c -> c.getType()).collect(Collectors.toList()),
					this.components.stream().map(c -> c.copy()).collect(Collectors.toList()));
		} catch (IncompatiblePrototypeComponentException e) {
			// shouldn't happen
			return null;
		}
	}

	@Override
	public boolean hasPrototypeComponent(final PrototypeComponentType componentType) {
		return this.componentTypes.containsKey(componentType);
	}

	@Override
	public IPrototypeComponent<?> getPrototypeComponent(final PrototypeComponentType componentType)
			throws IncompatiblePrototypeComponentException {
		final Integer idx = this.componentTypes.getOrDefault(componentType, null);
		if (idx == null)
			throw new IncompatiblePrototypeComponentException();
		return this.components.get(idx);
	}

	public IPrototypeComponent<?> getPrototypeComponent(final int idx) {
		return this.components.get(idx);
	}

	@Override
	public Collection<? extends IPrototypeComponent<?>> getPrototypeComponents() {
		return this.components;
	}

	@Override
	public Collection<PrototypeComponentType> getPrototypeComponentTypes() {
		return this.componentTypes.keySet();
	}

	@Override
	public IPrototypeComponent<?> addPrototypeComponent(final IPrototypeComponent<?> component) {
		Integer idx = this.componentTypes.getOrDefault(component.getType(), null);
		if (idx == null) {
			// insert component of new component type
			idx = this.components.size();
			this.components.add(component);
			this.componentTypes.put(component.getType(), idx);
			return null;
		} else {
			// replace old component of same type
			return setPrototypeComponent(idx, component);
		}
	}

	protected IPrototypeComponent<?> setPrototypeComponent(final int idx, final IPrototypeComponent<?> component) {
		return this.components.set(idx, component);
	}

}
